package com.haohan.cloud.scm.api.bill.vo;

import cn.hutool.core.bean.BeanUtil;
import com.haohan.cloud.scm.api.bill.dto.OrderInfoDTO;
import com.haohan.cloud.scm.api.bill.entity.PayableBill;
import com.haohan.cloud.scm.api.bill.entity.ReceivableBill;
import com.haohan.cloud.scm.api.constant.enums.common.ReviewStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import com.haohan.cloud.scm.api.constant.enums.saleb.BillTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author dy
 * @date 2019/11/26
 */
@Data
@ApiModel("账单详情-带明细")
@NoArgsConstructor
public class BillInfoVO implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 账单编号 (应收、应付)
     */
    private String billSn;
    /**
     * 账单审核状态: 1.待审核2.审核不通过3.审核通过
     */
    private ReviewStatusEnum reviewStatus;
    /**
     * 账单类型: 1订单应收 2退款应收 3采购应付 4退款应付
     */
    private BillTypeEnum billType;
    /**
     * 结算单编号
     */
    private String settlementSn;
    /**
     * 结算状态 是否支付 0否1是
     */
    private YesNoEnum settlementStatus;

    // 订单相关

    @ApiModelProperty(value = "订单详情")
    private OrderInfoDTO orderInfo;

    // 金额相关

    @ApiModelProperty(value = "预付标志", notes = "是否预付账单")
    private YesNoEnum advanceFlag;

    @ApiModelProperty(value = "预付金额")
    private BigDecimal advanceAmount;

    @ApiModelProperty(value = "账单金额", notes = "用于结算的金额")
    private BigDecimal billAmount;

    @ApiModelProperty(value = "订单金额")
    private BigDecimal orderAmount;

    @ApiModelProperty(value = "备注信息")
    protected String remarks;

    // 扩展

    @ApiModelProperty(value = "已支付金额")
    private BigDecimal payAmount;

    // 补充

    @ApiModelProperty(value = "预付账单是否支付标志")
    private YesNoEnum advancePayFlag;

    @ApiModelProperty(value = "是否汇总结算标志")
    private YesNoEnum settlementSummaryFlag;

    @ApiModelProperty(value = "汇总结算单编号")
    private String summarySettlementSn;


    public BillInfoVO(ReceivableBill bill) {
        if (null != bill) {
            BeanUtil.copyProperties(bill, this);
        }
    }

    public BillInfoVO(PayableBill bill) {
        if (null != bill) {
            BeanUtil.copyProperties(bill, this);
        }
    }

    /**
     * 计算已支付金额
     */
    public void computePayAmount() {
        BigDecimal billAmount = null == this.billAmount ? BigDecimal.ZERO : this.billAmount;
        billAmount = settlementStatus == YesNoEnum.yes ? billAmount : BigDecimal.ZERO;

        BigDecimal advanceAmount = null == this.advanceAmount ? BigDecimal.ZERO : this.advanceAmount;
        advanceAmount = advancePayFlag == YesNoEnum.yes ? advanceAmount : BigDecimal.ZERO;

        this.payAmount = billAmount.add(advanceAmount);
    }
}
