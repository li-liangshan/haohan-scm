/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.manage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.manage.entity.MerchantEmployee;

/**
 * 员工管理
 *
 * @author haohan
 * @date 2019-05-13 17:37:52
 */
public interface MerchantEmployeeService extends IService<MerchantEmployee> {

}
