/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.opc.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.opc.entity.TradeMatch;
import com.haohan.cloud.scm.api.opc.req.TradeMatchReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 交易匹配内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "TradeMatchFeignService", value = ScmServiceName.SCM_BIZ_OPC)
public interface TradeMatchFeignService {


    /**
     * 通过id查询交易匹配
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/TradeMatch/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 交易匹配 列表信息
     * @param tradeMatchReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/TradeMatch/fetchTradeMatchPage")
    R getTradeMatchPage(@RequestBody TradeMatchReq tradeMatchReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 交易匹配 列表信息
     * @param tradeMatchReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/TradeMatch/fetchTradeMatchList")
    R getTradeMatchList(@RequestBody TradeMatchReq tradeMatchReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增交易匹配
     * @param tradeMatch 交易匹配
     * @return R
     */
    @PostMapping("/api/feign/TradeMatch/add")
    R save(@RequestBody TradeMatch tradeMatch, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改交易匹配
     * @param tradeMatch 交易匹配
     * @return R
     */
    @PostMapping("/api/feign/TradeMatch/update")
    R updateById(@RequestBody TradeMatch tradeMatch, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除交易匹配
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/TradeMatch/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/TradeMatch/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/TradeMatch/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param tradeMatchReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/TradeMatch/countByTradeMatchReq")
    R countByTradeMatchReq(@RequestBody TradeMatchReq tradeMatchReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param tradeMatchReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/TradeMatch/getOneByTradeMatchReq")
    R getOneByTradeMatchReq(@RequestBody TradeMatchReq tradeMatchReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param tradeMatchList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/TradeMatch/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<TradeMatch> tradeMatchList, @RequestHeader(SecurityConstants.FROM) String from);


}
