package com.haohan.cloud.scm.product.core;

import com.haohan.cloud.scm.api.opc.entity.SummaryOrder;
import com.haohan.cloud.scm.api.product.entity.ProductionTask;
import com.haohan.cloud.scm.api.product.entity.ShipOrder;
import com.haohan.cloud.scm.api.product.req.AddShipReq;
import com.haohan.cloud.scm.api.product.req.ArriveBatchReq;
import com.haohan.cloud.scm.api.product.req.ProductDeliveryReq;

/**
 * @author xwx
 * @date 2019/5/27
 */
public interface IShipOrderService {
  /**
   * 生成送货单
   * @param req
   * @return
   */
  ShipOrder addShipOrder (AddShipReq req);

  /**
   * 库存满足时  运营 发起送货
   * @param req
   * @return
   */
  Boolean initiateDelivery(ProductDeliveryReq req);

  /**
   * 创建生产任务
   * @param order,userId
   * @return
   */
  ProductionTask addProductionTask(SummaryOrder order,String userId);
  /**
   * 创建出库单明细
   * @param task
   * @return
   */
  Boolean addExitWarehouseDetail(ProductionTask task);

    /**
     * 开始配送 改变配送单状态 修改B订单状态
     *
     * @param shipOrder
     */
    void deliveryBegin(ShipOrder shipOrder);

    /**
     * 配送单 送达  修改配送单状态
     * @param shipOrder 必须 pmId/shipId
     */
    void arrive(ShipOrder shipOrder);

    /**
     * 配送单批量送达
     * @param buyOrder
     */
    String arriveBatch(ArriveBatchReq buyOrder);
}
