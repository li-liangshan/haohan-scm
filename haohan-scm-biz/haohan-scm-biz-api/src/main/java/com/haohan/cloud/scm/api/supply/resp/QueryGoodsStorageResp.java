package com.haohan.cloud.scm.api.supply.resp;

import com.haohan.cloud.scm.api.goods.dto.GoodsModelDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author cx
 * @date 2019/8/20
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class QueryGoodsStorageResp extends GoodsModelDTO {

    private String agreementId;
}
