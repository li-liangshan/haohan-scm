package com.haohan.cloud.scm.api.opc.resp;

import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import com.haohan.cloud.scm.api.constant.enums.purchase.PayTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.saleb.BillTypeEnum;
import com.haohan.cloud.scm.api.saleb.entity.BuyOrderDetail;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author xwx
 * @date 2019/7/17
 */
@Data
public class QueryBuyerPaymentDetailResp {
    /**
     * 结算记录编号
     */
    private String settlementId;
    /**
     * 采购商名称
     */
    private String buyerName;
    /**
     * 创建时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
     * 状态 是否结算 0否1是
     */
    private YesNoEnum status;
    /**
     * 付款人
     */
    private String companyOperator;
    /**
     * 付款金额
     */
    private BigDecimal settlementAmount;
    /**
     * 账单类型: 1订单应收 2退款应收
     */
    private BillTypeEnum billType;
    /**
     * 付款方式:1对公转账2现金支付3在线支付4承兑汇票
     */
    private PayTypeEnum payType;
    /**
     * 原订单号
     */
    private String buyId;
    /**
     * 订单明细
     */
    List<BuyOrderDetail> list;

}
