/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.wms.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.wms.entity.ExitWarehouse;
import com.haohan.cloud.scm.api.wms.req.ExitWarehouseReq;
import com.haohan.cloud.scm.wms.service.ExitWarehouseService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 出库单
 *
 * @author haohan
 * @date 2019-05-29 13:22:22
 */
@RestController
@AllArgsConstructor
@RequestMapping("/exitwarehouse" )
@Api(value = "exitwarehouse", tags = "exitwarehouse管理")
public class ExitWarehouseController {

    private final ExitWarehouseService exitWarehouseService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param exitWarehouse 出库单
     * @return
     */
    @GetMapping("/page" )
    public R getExitWarehousePage(Page page, ExitWarehouse exitWarehouse) {
        return new R<>(exitWarehouseService.page(page, Wrappers.query(exitWarehouse)));
    }


    /**
     * 通过id查询出库单
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(exitWarehouseService.getById(id));
    }

    /**
     * 新增出库单
     * @param exitWarehouse 出库单
     * @return R
     */
    @SysLog("新增出库单" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_exitwarehouse_add')" )
    public R save(@RequestBody ExitWarehouse exitWarehouse) {
        return new R<>(exitWarehouseService.save(exitWarehouse));
    }

    /**
     * 修改出库单
     * @param exitWarehouse 出库单
     * @return R
     */
    @SysLog("修改出库单" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_exitwarehouse_edit')" )
    public R updateById(@RequestBody ExitWarehouse exitWarehouse) {
        return new R<>(exitWarehouseService.updateById(exitWarehouse));
    }

    /**
     * 通过id删除出库单
     * @param id id
     * @return R
     */
    @SysLog("删除出库单" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_exitwarehouse_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(exitWarehouseService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除出库单")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_exitwarehouse_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(exitWarehouseService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询出库单")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(exitWarehouseService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param exitWarehouseReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询出库单总记录}")
    @PostMapping("/countByExitWarehouseReq")
    public R countByExitWarehouseReq(@RequestBody ExitWarehouseReq exitWarehouseReq) {

        return new R<>(exitWarehouseService.count(Wrappers.query(exitWarehouseReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param exitWarehouseReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据exitWarehouseReq查询一条货位信息表")
    @PostMapping("/getOneByExitWarehouseReq")
    public R getOneByExitWarehouseReq(@RequestBody ExitWarehouseReq exitWarehouseReq) {

        return new R<>(exitWarehouseService.getOne(Wrappers.query(exitWarehouseReq), false));
    }


    /**
     * 批量修改OR插入
     * @param exitWarehouseList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_exitwarehouse_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<ExitWarehouse> exitWarehouseList) {

        return new R<>(exitWarehouseService.saveOrUpdateBatch(exitWarehouseList));
    }


}
