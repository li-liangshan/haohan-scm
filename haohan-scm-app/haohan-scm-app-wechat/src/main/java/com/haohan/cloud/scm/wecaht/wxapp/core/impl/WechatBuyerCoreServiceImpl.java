package com.haohan.cloud.scm.wecaht.wxapp.core.impl;

import com.haohan.cloud.scm.api.saleb.entity.Buyer;
import com.haohan.cloud.scm.api.saleb.req.QueryBuyerStatusReq;
import com.haohan.cloud.scm.api.saleb.vo.BuyerPayVO;
import com.haohan.cloud.scm.api.wechat.req.WxBuyerStatusReq;
import com.haohan.cloud.scm.wecaht.wxapp.core.WechatBuyerCoreService;
import com.haohan.cloud.scm.wecaht.wxapp.utils.ScmWechatUtils;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author dy
 * @date 2020/5/26
 */
@Service
@AllArgsConstructor
public class WechatBuyerCoreServiceImpl implements WechatBuyerCoreService {

    private final ScmWechatUtils scmWechatUtils;

    @Override
    public boolean buyerStatus(WxBuyerStatusReq req) {
        // uid 验证
        Buyer buyer = scmWechatUtils.fetchBuyerByUid(req.getUid());
        QueryBuyerStatusReq query = new QueryBuyerStatusReq();
        query.setBuyerId(buyer.getId());
        return scmWechatUtils.queryBuyerStatus(query);
    }

    @Override
    public BuyerPayVO buyerPayInfo(WxBuyerStatusReq req) {
        // uid 验证
        Buyer buyer = scmWechatUtils.fetchBuyerByUid(req.getUid());
        QueryBuyerStatusReq query = new QueryBuyerStatusReq();
        query.setBuyerId(buyer.getId());
        return scmWechatUtils.queryBuyerPayInfo(query);
    }
}
