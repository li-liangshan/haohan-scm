package com.haohan.cloud.scm.api.constant.converter.purchase;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.purchase.PurchaseOrderTypeEnum;

/**
 * @author xwx
 * @date 2019/6/3
 */
public class PurchaseOrderTypeEnumConverterUtil implements Converter<PurchaseOrderTypeEnum> {
    @Override
    public PurchaseOrderTypeEnum convert(Object o, PurchaseOrderTypeEnum purchaseOrderTypeEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return PurchaseOrderTypeEnum.getByType(o.toString());
    }
}
