/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.haohan.cloud.scm.api.message.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * 预警发送记录表
 *
 * @author haohan
 * @date 2019-05-13 18:23:45
 */
@Data
@TableName("scm_ews_warning_send")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "预警发送记录表")
public class WarningSend extends Model<WarningSend> {
private static final long serialVersionUID = 1L;

    /**
   * 主键
   */
    @TableId
    private String id;
    /**
   * 平台商家id
   */
    private String pmId;
    /**
   * 预警记录编号
   */
    private String warningSn;
    /**
   * 查看时间
   */
    private LocalDateTime lookTime;
    /**
   * 消息状态:1待发送2已发送3已查看
   */
    private String messageStatus;
    /**
   * 接收人uid
   */
    private String receiverUid;
    /**
   * 接收人名称
   */
    private String receiverName;
    /**
   * 发送时间
   */
    private LocalDateTime sendTime;
    /**
   * 创建者
   */
    private String createBy;
    /**
   * 创建时间
   */
    private LocalDateTime createDate;
    /**
   * 更新者
   */
    private String updateBy;
    /**
   * 更新时间
   */
    private LocalDateTime updateDate;
    /**
   * 备注信息
   */
    private String remarks;
    /**
   * 删除标记
   */
    private String delFlag;
    /**
   * 租户id
   */
    private Integer tenantId;
  
}
