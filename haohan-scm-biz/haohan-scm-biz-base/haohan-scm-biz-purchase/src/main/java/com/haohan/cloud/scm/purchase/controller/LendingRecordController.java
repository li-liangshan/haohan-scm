/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.purchase.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.purchase.entity.LendingRecord;
import com.haohan.cloud.scm.api.purchase.req.LendingRecordReq;
import com.haohan.cloud.scm.purchase.service.LendingRecordService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 放款申请记录
 *
 * @author haohan
 * @date 2019-05-29 13:35:06
 */
@RestController
@AllArgsConstructor
@RequestMapping("/lendingrecord" )
@Api(value = "lendingrecord", tags = "lendingrecord管理")
public class LendingRecordController {

    private final LendingRecordService lendingRecordService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param lendingRecord 放款申请记录
     * @return
     */
    @GetMapping("/page" )
    public R getLendingRecordPage(Page page, LendingRecord lendingRecord) {
        return new R<>(lendingRecordService.page(page, Wrappers.query(lendingRecord)));
    }


    /**
     * 通过id查询放款申请记录
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(lendingRecordService.getById(id));
    }

    /**
     * 新增放款申请记录
     * @param lendingRecord 放款申请记录
     * @return R
     */
    @SysLog("新增放款申请记录" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('purchase_lendingrecord_add')" )
    public R save(@RequestBody LendingRecord lendingRecord) {
        return new R<>(lendingRecordService.save(lendingRecord));
    }

    /**
     * 修改放款申请记录
     * @param lendingRecord 放款申请记录
     * @return R
     */
    @SysLog("修改放款申请记录" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('purchase_lendingrecord_edit')" )
    public R updateById(@RequestBody LendingRecord lendingRecord) {
        return new R<>(lendingRecordService.updateById(lendingRecord));
    }

    /**
     * 通过id删除放款申请记录
     * @param id id
     * @return R
     */
    @SysLog("删除放款申请记录" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('purchase_lendingrecord_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(lendingRecordService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除放款申请记录")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('purchase_lendingrecord_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(lendingRecordService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询放款申请记录")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(lendingRecordService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param lendingRecordReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询放款申请记录总记录}")
    @PostMapping("/countByLendingRecordReq")
    public R countByLendingRecordReq(@RequestBody LendingRecordReq lendingRecordReq) {

        return new R<>(lendingRecordService.count(Wrappers.query(lendingRecordReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param lendingRecordReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据lendingRecordReq查询一条货位信息表")
    @PostMapping("/getOneByLendingRecordReq")
    public R getOneByLendingRecordReq(@RequestBody LendingRecordReq lendingRecordReq) {

        return new R<>(lendingRecordService.getOne(Wrappers.query(lendingRecordReq), false));
    }


    /**
     * 批量修改OR插入
     * @param lendingRecordList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('purchase_lendingrecord_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<LendingRecord> lendingRecordList) {

        return new R<>(lendingRecordService.saveOrUpdateBatch(lendingRecordList));
    }


}
