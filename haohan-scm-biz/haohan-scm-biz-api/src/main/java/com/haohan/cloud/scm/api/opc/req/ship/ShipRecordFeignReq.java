package com.haohan.cloud.scm.api.opc.req.ship;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author dy
 * @date 2020/1/6
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ShipRecordFeignReq extends QueryShipRecordReq {
    private static final long serialVersionUID = 1L;

    /**
     * 分页参数 当前页
     */
    private long current = 1;
    /**
     * 分页参数 每页显示条数
     */
    private long size = 10;

}
