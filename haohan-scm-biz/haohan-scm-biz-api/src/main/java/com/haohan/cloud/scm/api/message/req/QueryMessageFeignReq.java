package com.haohan.cloud.scm.api.message.req;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author dy
 * @date 2020/1/14
 */
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class QueryMessageFeignReq extends QueryMessageReq {
    private static final long serialVersionUID = 1L;

    /**
     * 分页参数 当前页
     */
    private long current = 1;
    /**
     * 分页参数 每页显示条数
     */
    private long size = 10;

    public QueryMessageFeignReq(Page page) {
        this.current = page.getCurrent();
        this.size = page.getSize();
    }
}
