/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.tms.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.tms.entity.TruckManage;
import com.haohan.cloud.scm.api.tms.req.TruckManageReq;
import com.haohan.cloud.scm.tms.service.TruckManageService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 车辆管理
 *
 * @author haohan
 * @date 2019-06-05 09:26:20
 */
@RestController
@AllArgsConstructor
@RequestMapping("/truckmanage" )
@Api(value = "truckmanage", tags = "truckmanage管理")
public class TruckManageController {

    private final  TruckManageService truckManageService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param truckManage 车辆管理
     * @return
     */
    @GetMapping("/page" )
    public R getTruckManagePage(Page page, TruckManage truckManage) {
        return new R<>(truckManageService.page(page, Wrappers.query(truckManage)));
    }


    /**
     * 通过id查询车辆管理
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(truckManageService.getById(id));
    }

    /**
     * 新增车辆管理
     * @param truckManage 车辆管理
     * @return R
     */
    @SysLog("新增车辆管理" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('tms_truckmanage_add')" )
    public R save(@RequestBody TruckManage truckManage) {
        return new R<>(truckManageService.save(truckManage));
    }

    /**
     * 修改车辆管理
     * @param truckManage 车辆管理
     * @return R
     */
    @SysLog("修改车辆管理" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('tms_truckmanage_edit')" )
    public R updateById(@RequestBody TruckManage truckManage) {
        return new R<>(truckManageService.updateById(truckManage));
    }

    /**
     * 通过id删除车辆管理
     * @param id id
     * @return R
     */
    @SysLog("删除车辆管理" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('tms_truckmanage_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(truckManageService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除车辆管理")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('tms_truckmanage_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(truckManageService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询车辆管理")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(truckManageService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param truckManageReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询车辆管理总记录}")
    @PostMapping("/countByTruckManageReq")
    public R countByTruckManageReq(@RequestBody TruckManageReq truckManageReq) {

        return new R<>(truckManageService.count(Wrappers.query(truckManageReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param truckManageReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据truckManageReq查询一条货位信息表")
    @PostMapping("/getOneByTruckManageReq")
    public R getOneByTruckManageReq(@RequestBody TruckManageReq truckManageReq) {

        return new R<>(truckManageService.getOne(Wrappers.query(truckManageReq), false));
    }


    /**
     * 批量修改OR插入
     * @param truckManageList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('tms_truckmanage_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<TruckManage> truckManageList) {

        return new R<>(truckManageService.saveOrUpdateBatch(truckManageList));
    }


}
