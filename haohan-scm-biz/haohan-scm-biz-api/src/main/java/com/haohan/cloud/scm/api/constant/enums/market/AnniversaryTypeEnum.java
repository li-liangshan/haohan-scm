package com.haohan.cloud.scm.api.constant.enums.market;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xwx
 * @date 2019/5/30
 */
@Getter
@AllArgsConstructor
public enum AnniversaryTypeEnum implements IBaseEnum {
    /**
     * 纪念日类型:1.公司2.联系人
     */
    company("1", "公司"),
    linkman("2", "联系人");

    private static final Map<String, AnniversaryTypeEnum> MAP = new HashMap<>(8);

    static {
        for (AnniversaryTypeEnum e : AnniversaryTypeEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static AnniversaryTypeEnum getByType(String type) {
        return MAP.get(type);
    }

    /**
     * 类型
     */
    @EnumValue
    @JsonValue
    private String type;
    /**
     * 描述
     */

    private String desc;
}
