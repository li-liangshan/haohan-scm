/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.wms.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.wms.entity.WarehouseAllot;
import com.haohan.cloud.scm.api.wms.req.WarehouseAllotReq;
import com.haohan.cloud.scm.wms.service.WarehouseAllotService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 库存调拨记录
 *
 * @author haohan
 * @date 2019-05-29 13:23:27
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/WarehouseAllot")
@Api(value = "warehouseallot", tags = "warehouseallot内部接口服务")
public class WarehouseAllotFeignApiCtrl {

    private final WarehouseAllotService warehouseAllotService;


    /**
     * 通过id查询库存调拨记录
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(warehouseAllotService.getById(id));
    }


    /**
     * 分页查询 库存调拨记录 列表信息
     * @param warehouseAllotReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchWarehouseAllotPage")
    public R getWarehouseAllotPage(@RequestBody WarehouseAllotReq warehouseAllotReq) {
        Page page = new Page(warehouseAllotReq.getPageNo(), warehouseAllotReq.getPageSize());
        WarehouseAllot warehouseAllot =new WarehouseAllot();
        BeanUtil.copyProperties(warehouseAllotReq, warehouseAllot);

        return new R<>(warehouseAllotService.page(page, Wrappers.query(warehouseAllot)));
    }


    /**
     * 全量查询 库存调拨记录 列表信息
     * @param warehouseAllotReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchWarehouseAllotList")
    public R getWarehouseAllotList(@RequestBody WarehouseAllotReq warehouseAllotReq) {
        WarehouseAllot warehouseAllot =new WarehouseAllot();
        BeanUtil.copyProperties(warehouseAllotReq, warehouseAllot);

        return new R<>(warehouseAllotService.list(Wrappers.query(warehouseAllot)));
    }


    /**
     * 新增库存调拨记录
     * @param warehouseAllot 库存调拨记录
     * @return R
     */
    @Inner
    @SysLog("新增库存调拨记录")
    @PostMapping("/add")
    public R save(@RequestBody WarehouseAllot warehouseAllot) {
        return new R<>(warehouseAllotService.save(warehouseAllot));
    }

    /**
     * 修改库存调拨记录
     * @param warehouseAllot 库存调拨记录
     * @return R
     */
    @Inner
    @SysLog("修改库存调拨记录")
    @PostMapping("/update")
    public R updateById(@RequestBody WarehouseAllot warehouseAllot) {
        return new R<>(warehouseAllotService.updateById(warehouseAllot));
    }

    /**
     * 通过id删除库存调拨记录
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除库存调拨记录")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(warehouseAllotService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除库存调拨记录")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(warehouseAllotService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询库存调拨记录")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(warehouseAllotService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param warehouseAllotReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询库存调拨记录总记录}")
    @PostMapping("/countByWarehouseAllotReq")
    public R countByWarehouseAllotReq(@RequestBody WarehouseAllotReq warehouseAllotReq) {

        return new R<>(warehouseAllotService.count(Wrappers.query(warehouseAllotReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param warehouseAllotReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据warehouseAllotReq查询一条货位信息表")
    @PostMapping("/getOneByWarehouseAllotReq")
    public R getOneByWarehouseAllotReq(@RequestBody WarehouseAllotReq warehouseAllotReq) {

        return new R<>(warehouseAllotService.getOne(Wrappers.query(warehouseAllotReq), false));
    }


    /**
     * 批量修改OR插入
     * @param warehouseAllotList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<WarehouseAllot> warehouseAllotList) {

        return new R<>(warehouseAllotService.saveOrUpdateBatch(warehouseAllotList));
    }

}
