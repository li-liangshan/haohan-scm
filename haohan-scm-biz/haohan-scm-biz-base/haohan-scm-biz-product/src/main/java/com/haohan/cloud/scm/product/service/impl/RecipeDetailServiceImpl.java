/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.product.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.product.entity.RecipeDetail;
import com.haohan.cloud.scm.product.mapper.RecipeDetailMapper;
import com.haohan.cloud.scm.product.service.RecipeDetailService;
import org.springframework.stereotype.Service;

/**
 * 商品加工配方明细表
 *
 * @author haohan
 * @date 2019-05-13 18:15:38
 */
@Service
public class RecipeDetailServiceImpl extends ServiceImpl<RecipeDetailMapper, RecipeDetail> implements RecipeDetailService {

}
