package com.haohan.cloud.scm.manage.core;

import com.haohan.cloud.scm.api.manage.dto.ShopExtDTO;
import com.haohan.cloud.scm.api.manage.req.shop.ShopEditReq;
import com.haohan.cloud.scm.api.manage.vo.MerchantShopVO;

/**
 * @author dy
 * @date 2019/9/12
 */
public interface ShopManageCoreService {

    /**
     * 获取店铺详情 带图片
     *
     * @param id
     * @return
     */
    ShopExtDTO fetchShopInfo(String id);

    /**
     * 获取平台商家的采购配送店铺
     * (目前在采购单商品导入时使用)
     *
     * @param pmId
     * @return
     */
    ShopExtDTO fetchPurchaseShop(String pmId);

    /**
     * 获取商家店铺
     *
     * @param merchantId
     * @return
     */
    MerchantShopVO fetchMerchantShop(String merchantId);

    /**
     * 查询租户默认店铺
     *
     * @return
     */
    ShopExtDTO fetchDefaultShop();

    /**
     * 删除店铺，有对应商品时不可删除
     *
     * @param id
     * @return
     */
    boolean deleteShop(String id);

    /**
     * 新增
     * @param req
     * @return
     */
    boolean addShop(ShopEditReq req);

    /**
     * 修改
     * @param req
     * @return
     */
    boolean modifyShop(ShopEditReq req);

}
