/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.product.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.product.entity.ProductLossRecord;
import com.haohan.cloud.scm.api.product.req.ProductInfoInventoryReq;
import com.haohan.cloud.scm.api.product.req.ProductLossRecordReq;
import com.haohan.cloud.scm.product.core.IProductLossRecordService;
import com.haohan.cloud.scm.product.service.ProductLossRecordService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 货品损耗记录表
 *
 * @author haohan
 * @date 2019-05-29 14:45:38
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/ProductLossRecord")
@Api(value = "productlossrecord", tags = "productlossrecord内部接口服务")
public class ProductLossRecordFeignApiCtrl {

    private final ProductLossRecordService productLossRecordService;

    private final IProductLossRecordService iProductLossRecordService;
    /**
     * 通过id查询货品损耗记录表
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(productLossRecordService.getById(id));
    }


    /**
     * 分页查询 货品损耗记录表 列表信息
     * @param productLossRecordReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchProductLossRecordPage")
    public R getProductLossRecordPage(@RequestBody ProductLossRecordReq productLossRecordReq) {
        Page page = new Page(productLossRecordReq.getPageNo(), productLossRecordReq.getPageSize());
        ProductLossRecord productLossRecord =new ProductLossRecord();
        BeanUtil.copyProperties(productLossRecordReq, productLossRecord);

        return new R<>(productLossRecordService.page(page, Wrappers.query(productLossRecord)));
    }


    /**
     * 全量查询 货品损耗记录表 列表信息
     * @param productLossRecordReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchProductLossRecordList")
    public R getProductLossRecordList(@RequestBody ProductLossRecordReq productLossRecordReq) {
        ProductLossRecord productLossRecord =new ProductLossRecord();
        BeanUtil.copyProperties(productLossRecordReq, productLossRecord);

        return new R<>(productLossRecordService.list(Wrappers.query(productLossRecord)));
    }


    /**
     * 新增货品损耗记录表
     * @param productLossRecord 货品损耗记录表
     * @return R
     */
    @Inner
    @SysLog("新增货品损耗记录表")
    @PostMapping("/add")
    public R save(@RequestBody ProductLossRecord productLossRecord) {
        return new R<>(productLossRecordService.save(productLossRecord));
    }

    /**
     * 修改货品损耗记录表
     * @param productLossRecord 货品损耗记录表
     * @return R
     */
    @Inner
    @SysLog("修改货品损耗记录表")
    @PostMapping("/update")
    public R updateById(@RequestBody ProductLossRecord productLossRecord) {
        return new R<>(productLossRecordService.updateById(productLossRecord));
    }

    /**
     * 通过id删除货品损耗记录表
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除货品损耗记录表")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(productLossRecordService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除货品损耗记录表")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(productLossRecordService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询货品损耗记录表")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(productLossRecordService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param productLossRecordReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询货品损耗记录表总记录}")
    @PostMapping("/countByProductLossRecordReq")
    public R countByProductLossRecordReq(@RequestBody ProductLossRecordReq productLossRecordReq) {

        return new R<>(productLossRecordService.count(Wrappers.query(productLossRecordReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param productLossRecordReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据productLossRecordReq查询一条货位信息表")
    @PostMapping("/getOneByProductLossRecordReq")
    public R getOneByProductLossRecordReq(@RequestBody ProductLossRecordReq productLossRecordReq) {

        return new R<>(productLossRecordService.getOne(Wrappers.query(productLossRecordReq), false));
    }


    /**
     * 批量修改OR插入
     * @param productLossRecordList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<ProductLossRecord> productLossRecordList) {

        return new R<>(productLossRecordService.saveOrUpdateBatch(productLossRecordList));
    }

    @Inner
    @PostMapping("/addLossRecord")
    @ApiOperation(value = "新增报损记录")
    public R addLossRecord(@RequestBody ProductInfoInventoryReq req){
        return R.ok(iProductLossRecordService.addLossRecord(req));
    }
}
