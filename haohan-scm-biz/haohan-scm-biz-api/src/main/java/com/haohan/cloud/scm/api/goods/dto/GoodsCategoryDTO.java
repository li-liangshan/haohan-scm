package com.haohan.cloud.scm.api.goods.dto;

import com.haohan.cloud.scm.api.common.tree.TreeUtil;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import com.haohan.cloud.scm.api.goods.entity.GoodsCategory;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @author dy
 * @date 2019/10/16
 */
@Data
@NoArgsConstructor
public class GoodsCategoryDTO {

    // 主要属性
    /**
     * 主键
     */
    private String id;
    /**
     * 店铺ID
     */
    private String shopId;
    /**
     * 商家ID
     */
    private String merchantId;
    /**
     * 父级编号
     */
    private String parentId;
    /**
     * 所有父级编号
     */
    private String parentIds;
    /**
     * 名称
     */
    private String name;
    /**
     * logo地址
     */
    private String logo;
    /**
     * 排序
     */
    private BigDecimal sort;
    /**
     * 分类类型 修改为是否展示 0否 1是
     */
    private YesNoEnum categoryType;
    /**
     * 是否上架  0否 1是
     */
    private YesNoEnum status;
    /**
     * 描述
     */
    private String description;

    // 其他属性
    /**
     * 行业名称
     */
    private String industry;
    /**
     * 关键词
     */
    private String keywords;
    /**
     * 分类编号  原系统 第三方分类编号   即速分类ID
     */
    private String categorySn;
    /**
     * 父分类编号/即速父级ID
     */
    private String parentSn;
    /**
     * 商品类型 0实体商品，1虚拟商品
     */
    private String goodsType;
    /**
     * 商品分类通用编号
     */
    private String generalCategorySn;
    /**
     * 备注信息
     */
    private String remarks;

    // 扩展属性
    /**
     * 父级分类名称
     */
    private String parentName;
    /**
     * 所有父级名称
     */
    private String parentNameAll;
    /**
     * 父级分类
     */
    private GoodsCategoryDTO parentCategory;

    public GoodsCategoryDTO(GoodsCategory category) {
        this.id = category.getId();
        this.shopId = category.getShopId();
        this.merchantId = category.getMerchantId();
        this.parentId = category.getParentId();
        this.parentIds = category.getParentIds();
        this.name = category.getName();
        this.logo = category.getLogo();
        this.sort = category.getSort();
        this.categoryType = category.getCategoryType();
        this.status = category.getStatus();

        this.description = category.getDescription();
        this.industry = category.getIndustry();
        this.keywords = category.getKeywords();
        this.categorySn = category.getCategorySn();
        this.parentSn = category.getParentSn();
        this.goodsType = category.getGoodsType();
        this.generalCategorySn = category.getGeneralCategorySn();
        this.remarks = category.getRemarks();
    }

    public GoodsCategory transTo() {
        GoodsCategory category = new GoodsCategory();
        category.setId(this.id);
        category.setShopId(this.shopId);
        category.setMerchantId(this.merchantId);

        // 所有父级分类id 保存时设置
        category.setParentId(null == parentCategory ? TreeUtil.ROOT : parentCategory.getId());
        category.setName(this.name);
        category.setLogo(this.logo);
        category.setSort(this.sort);
        category.setCategoryType(this.categoryType);
        category.setStatus(this.status);

        category.setDescription(this.description);
        category.setIndustry(this.industry);
        category.setKeywords(this.keywords);
        category.setCategorySn(this.categorySn);
        category.setParentSn(this.parentSn);
        category.setGoodsType(this.goodsType);
        category.setGeneralCategorySn(this.generalCategorySn);
        category.setRemarks(this.remarks);
        return category;
    }
}
