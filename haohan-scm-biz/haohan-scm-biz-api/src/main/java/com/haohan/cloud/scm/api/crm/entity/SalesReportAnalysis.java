/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.crm.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Digits;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 客户销量上报数据统计
 *
 * @author haohan
 * @date 2020-01-16 12:08:35
 */
@Data
@TableName("crm_sales_report_analysis")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "客户销量上报数据统计")
public class SalesReportAnalysis extends Model<SalesReportAnalysis> {
    private static final long serialVersionUID = 1L;


    @Length(max = 64, message = "主键长度最大64字符")
    @ApiModelProperty(value = "主键")
    @TableId(type = IdType.INPUT)
    private String id;

    @Length(max = 64, message = "客户编码长度最大64字符")
    @ApiModelProperty(value = "客户编码")
    private String customerSn;

    @Length(max = 64, message = "客户名称长度最大64字符")
    @ApiModelProperty(value = "客户名称")
    private String customerName;

    @Length(max = 255, message = "市场人员(所有上报员工名称)长度最大255字符")
    @ApiModelProperty(value = "市场人员(所有上报员工名称)")
    private String employeeNames;

    @Length(max = 64, message = "月份(yyyy-MM)长度最大64字符")
    @ApiModelProperty(value = "月份(yyyy-MM)")
    private String month;

    @Digits(integer = 10, fraction = 2, message = "销售金额的整数位最大10位, 小数位最大2位")
    @ApiModelProperty(value = "销售金额")
    private BigDecimal totalAmount;

    @Digits(integer = 10, fraction = 2, message = "赠品总金额的整数位最大10位, 小数位最大2位")
    @ApiModelProperty(value = "赠品总金额")
    private BigDecimal giftAmount;

    @Length(max = 1000, message = "赠品内容(赠送商品及数量)长度最大1000字符")
    @ApiModelProperty(value = "赠品内容(赠送商品及数量)")
    private String giftContent;

    @Length(max = 64, message = "创建者长度最大64字符")
    @ApiModelProperty(value = "创建者")
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;

    @Length(max = 64, message = "更新者长度最大64字符")
    @ApiModelProperty(value = "更新者")
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;

    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.UPDATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateDate;

    @Length(max = 255, message = "备注信息长度最大255字符")
    @ApiModelProperty(value = "备注信息")
    private String remarks;

    @Length(max = 1, message = "删除标记长度最大1字符")
    @ApiModelProperty(value = "删除标记")
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;

    @ApiModelProperty(value = "租户id")
    private Integer tenantId;

}
