/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.opc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haohan.cloud.scm.api.opc.entity.SupplierPayment;

/**
 * 供应商货款统计
 *
 * @author haohan
 * @date 2019-05-13 17:45:06
 */
public interface SupplierPaymentMapper extends BaseMapper<SupplierPayment> {

}
