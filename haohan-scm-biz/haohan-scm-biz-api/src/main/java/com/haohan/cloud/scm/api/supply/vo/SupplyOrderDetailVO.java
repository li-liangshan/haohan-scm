package com.haohan.cloud.scm.api.supply.vo;

import cn.hutool.core.bean.BeanUtil;
import com.haohan.cloud.scm.api.constant.enums.crm.PayStatusEnum;
import com.haohan.cloud.scm.api.supply.entity.SupplyOrderDetail;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Digits;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author dy
 * @date 2020/4/25
 *
 */
@Data
@NoArgsConstructor
public class SupplyOrderDetailVO implements Serializable {
    private static final long serialVersionUID = 1L;

    @Length(max = 64, message = "主键长度最大64字符")
    @ApiModelProperty(value = "主键")
    private String id;

    @Length(max = 64, message = "供应订单编号长度最大64字符")
    @ApiModelProperty(value = "供应订单编号")
    private String supplySn;

    @Length(max = 64, message = "供应订单明细编号长度最大64字符")
    @ApiModelProperty(value = "供应订单明细编号")
    private String supplyDetailSn;

    @Length(max = 64, message = "供应商id长度最大64字符")
    @ApiModelProperty(value = "供应商id")
    private String supplierId;

    @Length(max = 64, message = "商品id长度最大64字符")
    @ApiModelProperty(value = "商品id")
    private String goodsId;

    @Length(max = 64, message = "商品规格ID长度最大64字符")
    @ApiModelProperty(value = "商品规格ID")
    private String goodsModelId;

    @Length(max = 64, message = "商品规格编号长度最大64字符")
    @ApiModelProperty(value = "商品规格编号")
    private String goodsModelSn;

    @Length(max = 500, message = "商品图片长度最大500字符")
    @ApiModelProperty(value = "商品图片")
    private String goodsImg;

    @Length(max = 64, message = "商品名称长度最大64字符")
    @ApiModelProperty(value = "商品名称")
    private String goodsName;

    @Length(max = 64, message = "商品规格名称长度最大64字符")
    @ApiModelProperty(value = "商品规格名称")
    private String modelName;

    @Digits(integer = 10, fraction = 2, message = "采购数量的整数位最大10位, 小数位最大2位")
    @ApiModelProperty(value = "采购数量")
    private BigDecimal goodsNum;

    @Digits(integer = 10, fraction = 2, message = "成交价格的整数位最大10位, 小数位最大2位")
    @ApiModelProperty(value = "成交价格")
    private BigDecimal dealPrice;

    @Length(max = 64, message = "单位长度最大64字符")
    @ApiModelProperty(value = "单位")
    private String unit;

    @Digits(integer = 10, fraction = 2, message = "金额的整数位最大10位, 小数位最大2位")
    @ApiModelProperty(value = "金额")
    private BigDecimal amount;

    @ApiModelProperty(value = "支付状态")
    private PayStatusEnum payStatus;

    public SupplyOrderDetailVO(SupplyOrderDetail detail) {
        BeanUtil.copyProperties(detail, this);
        this.payStatus = PayStatusEnum.wait;
    }

}
