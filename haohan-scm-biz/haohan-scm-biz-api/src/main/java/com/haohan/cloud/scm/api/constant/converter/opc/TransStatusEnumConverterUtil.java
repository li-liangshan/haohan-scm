package com.haohan.cloud.scm.api.constant.converter.opc;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.opc.TransStatusEnum;

/**
 * @author xwx
 * @date 2019/6/4
 */
public class TransStatusEnumConverterUtil implements Converter<TransStatusEnum> {
    @Override
    public TransStatusEnum convert(Object o, TransStatusEnum transStatusEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return TransStatusEnum.getByType(o.toString());
    }
}
