/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.crm.feign;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.crm.entity.CustomerLinkman;
import com.haohan.cloud.scm.api.crm.req.CustomerLinkmanReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 客户联系人内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "CustomerLinkmanFeignService", value = ScmServiceName.SCM_CRM)
public interface CustomerLinkmanFeignService {


    /**
     * 通过id查询客户联系人
     *
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/CustomerLinkman/{id}", method = RequestMethod.GET)
    R<CustomerLinkman> getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 客户联系人 列表信息
     *
     * @param customerLinkmanReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/CustomerLinkman/fetchCustomerLinkmanPage")
    R<Page<CustomerLinkman>> getCustomerLinkmanPage(@RequestBody CustomerLinkmanReq customerLinkmanReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 客户联系人 列表信息
     *
     * @param customerLinkmanReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/CustomerLinkman/fetchCustomerLinkmanList")
    R<List<CustomerLinkman>> getCustomerLinkmanList(@RequestBody CustomerLinkmanReq customerLinkmanReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增客户联系人
     *
     * @param customerLinkman 客户联系人
     * @return R
     */
    @PostMapping("/api/feign/CustomerLinkman/add")
    R<Boolean> save(@RequestBody CustomerLinkman customerLinkman, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改客户联系人
     *
     * @param customerLinkman 客户联系人
     * @return R
     */
    @PostMapping("/api/feign/CustomerLinkman/update")
    R<Boolean> updateById(@RequestBody CustomerLinkman customerLinkman, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除客户联系人
     *
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/CustomerLinkman/delete/{id}")
    R<Boolean> removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/CustomerLinkman/batchDelete")
    R<Boolean> removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/CustomerLinkman/listByIds")
    R<List<CustomerLinkman>> listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param customerLinkmanReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/CustomerLinkman/countByCustomerLinkmanReq")
    R<Integer> countByCustomerLinkmanReq(@RequestBody CustomerLinkmanReq customerLinkmanReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param customerLinkmanReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/CustomerLinkman/getOneByCustomerLinkmanReq")
    R<CustomerLinkman> getOneByCustomerLinkmanReq(@RequestBody CustomerLinkmanReq customerLinkmanReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param customerLinkmanList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/CustomerLinkman/saveOrUpdateBatch")
    R<Boolean> saveOrUpdateBatch(@RequestBody List<CustomerLinkman> customerLinkmanList, @RequestHeader(SecurityConstants.FROM) String from);


}
