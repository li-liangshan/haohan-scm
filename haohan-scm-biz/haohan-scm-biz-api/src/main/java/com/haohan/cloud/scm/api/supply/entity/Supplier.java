/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.haohan.cloud.scm.api.supply.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.haohan.cloud.scm.api.constant.enums.common.UseStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import com.haohan.cloud.scm.api.constant.enums.saleb.PayPeriodEnum;
import com.haohan.cloud.scm.api.constant.enums.supply.GradeTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.supply.PdsSupplierTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

/**
 * 供应商
 *
 * @author haohan
 * @date 2019-05-13 17:44:16
 */
@Data
@TableName("scm_sms_supplier")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "供应商")
public class Supplier extends Model<Supplier> {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 平台商家ID (原系统使用)
     */
    private String pmId;
    /**
     * 通行证ID
     */
    private String passportId;

    /**
     * 用户id
     */
    private String userId;
    /**
     * 商家ID
     */
    private String merchantId;
    /**
     * 商家名称
     */
    private String merchantName;
    /**
     * 供应商全称
     */
    private String supplierName;
    /**
     * 供应商简称
     */
    private String shortName;
    /**
     * 联系人
     */
    private String contact;
    /**
     * 联系电话
     */
    private String telephone;
    /**
     * 微信  (未使用)
     */
    private String wechatId;
    /**
     * 操作员 (未使用)
     */
    private String operator;
    /**
     * 账期:1.月结2.周结3.日结
     */
    private PayPeriodEnum payPeriod;
    /**
     * 账期日：  设置账期对应的结算日，使用数字，最大2位
     */
    private String payDay;
    /**
     * 供应商地址
     */
    private String address;
    /**
     * 标签
     */
    private String tags;
    /**
     * 是否启用
     */
    private UseStatusEnum status;
    /**
     * 供应商类型
     */
    private PdsSupplierTypeEnum supplierType;
    /**
     * 是否开启消息推送
     */
    private YesNoEnum needPush;
    /**
     * 排序值
     */
    private String sort;
    /**
     * 评级:1星-5星
     */
    private GradeTypeEnum supplierLevel;
    /**
     * 创建者
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
     * 更新者
     */
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateDate;
    /**
     * 备注信息
     */
    private String remarks;
    /**
     * 删除标记
     */
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;
    /**
     * 租户id
     */
    private Integer tenantId;
    /**
     * 区域地址
     */
    private String area;
    /**
     * 经度
     */
    private String longitude;
    /**
     * 纬度
     */
    private String latitude;

}
