package com.haohan.cloud.framework.dto.api;

import com.haohan.cloud.framework.utils.JacksonUtils;

import java.io.Serializable;
/**
 * 请求返回数据父类
 * @author zhaokuner
 *
 */
public class ApiResolveRespData implements Serializable {

	private static final long serialVersionUID = -1290046396752366884L;
	private int totalRows;
	private String pageMark;


	public String toJson(){
		return JacksonUtils.toJson(this);
	}

	public int getTotalRows() {
		return totalRows;
	}
	public void setTotalRows(int totalRows) {
		this.totalRows = totalRows;
	}
	public String getPageMark() {
		return pageMark;
	}
	public void setPageMark(String pageMark) {
		this.pageMark = pageMark;
	}

}
