/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.product.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.constant.NumberPrefixConstant;
import com.haohan.cloud.scm.api.product.dto.SortingOrderDetailDTO;
import com.haohan.cloud.scm.api.product.entity.SortingOrderDetail;
import com.haohan.cloud.scm.common.tools.util.ScmIncrementUtil;
import com.haohan.cloud.scm.product.mapper.SortingOrderDetailMapper;
import com.haohan.cloud.scm.product.service.SortingOrderDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

/**
 * 分拣单明细
 *
 * @author haohan
 * @date 2019-05-13 18:15:00
 */
@Service
public class SortingOrderDetailServiceImpl extends ServiceImpl<SortingOrderDetailMapper, SortingOrderDetail> implements SortingOrderDetailService {
    @Autowired
    @Lazy
    private ScmIncrementUtil scmIncrementUtil;
    @Autowired
    @Lazy
    private SortingOrderDetailMapper sortingOrderDetailMapper;

    @Override
    public boolean save(SortingOrderDetail entity) {
        if (StrUtil.isEmpty(entity.getSortingDetailSn())) {
            String sn = scmIncrementUtil.inrcSnByClass(SortingOrderDetail.class, NumberPrefixConstant.SORTING_ORDER_DETAIL_SN_PRE);
            entity.setSortingDetailSn(sn);
        }
        return super.save(entity);
    }

    /**
     * 分页 查询分拣单明细列表
     * @param req
     * @return
     */
    @Override
    public IPage<SortingOrderDetailDTO> queryDetailList(Page page, SortingOrderDetailDTO req) {
        return sortingOrderDetailMapper.querySortingOrderDetail(page, req.getPmId(), req.getBuyerId(),
                req.getDeliveryDate(), req.getDeliverySeq(),
                req.getGoodsName(), req.getSortingStatus(), req.getGoodsModelId());
    }
}
