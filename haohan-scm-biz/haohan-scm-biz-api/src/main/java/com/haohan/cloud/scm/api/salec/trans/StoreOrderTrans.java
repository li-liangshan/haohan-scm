package com.haohan.cloud.scm.api.salec.trans;

import com.haohan.cloud.scm.api.constant.enums.common.UseStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import com.haohan.cloud.scm.api.constant.enums.saleb.BuyerTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.saleb.PayPeriodEnum;
import com.haohan.cloud.scm.api.saleb.req.buyer.BuyerFeignReq;
import com.haohan.cloud.scm.api.salec.entity.StoreOrder;
import lombok.experimental.UtilityClass;

/**
 * @author dy
 * @date 2020/6/18
 */
@UtilityClass
public class StoreOrderTrans {


    public BuyerFeignReq buyerInit(StoreOrder order) {
        String name = order.getRealName();
        BuyerFeignReq buyer = new BuyerFeignReq();
        buyer.setMerchantName(name);
        buyer.setBuyerName(name);
        buyer.setShortName(name);
        buyer.setContact(name);
        buyer.setTelephone(order.getUserPhone());
        buyer.setAddress(order.getUserAddress());
        buyer.setPayPeriod(PayPeriodEnum.day);
        buyer.setPayDay("1");
        buyer.setStatus(UseStatusEnum.enabled);
        buyer.setBuyerType(BuyerTypeEnum.employee);
        buyer.setNeedPush(YesNoEnum.no);
        buyer.setSort("100");
        buyer.setNeedConfirmation(YesNoEnum.yes);
        buyer.setNeedPay(YesNoEnum.yes);
        buyer.setRemarks("商城用户转换来");
        return buyer;
    }
}
