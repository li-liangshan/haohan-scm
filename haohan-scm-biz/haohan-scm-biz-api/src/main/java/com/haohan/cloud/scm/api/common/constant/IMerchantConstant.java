package com.haohan.cloud.scm.api.common.constant;

import com.haohan.cloud.framework.utils.EnumUtils;

/**
 * Created by zgw on 2017/12/25.
 */
public interface IMerchantConstant {

    String sessionName = "haohanshop-token";

    String commonStatus = "common_status";


    long LIMIT_FILE_SIZE = 2 * 1024 * 1024;


    enum MerchantFilesType {
        ShopPhotos("00", "门店照片"),
        MerchantPhotos("01", "商户照片"),
        ProtocolFiles("02", "协议文件"),
        productPhotos("03", "商品照片"),
        cateringLicense("04", "餐饮许可证"),
        license("05", "营业执照"),
        bankCardPhotos("06", "结算卡或对公账户"),
        idCardPhotos("07", "身份证正反面"),
        billPhotos("08", "账单图片");

        private String groupNum;
        private String groupName;

        MerchantFilesType(String groupNum, String groupName) {
            this.groupNum = groupNum;
            this.groupName = groupName;
            EnumUtils.put(getClass().getName() + groupNum, this);
        }

        public MerchantFilesType fetchMerchantFiles(String groupNum) {

            return (MerchantFilesType) EnumUtils.getEnumByCode(getClass(), groupNum);
        }

        public String getGroupNum() {
            return groupNum;
        }

        public void setGroupNum(String groupNum) {
            this.groupNum = groupNum;
        }

        public String getGroupName() {
            return groupName;
        }

        public void setGroupName(String groupName) {
            this.groupName = groupName;
        }
    }


    enum MerchantAppStatus {
        submit_fail("-1", "提交失败"),
        wx_check_fail("-2", "微信审核失败"),
        wait_config("0", "待配置"),
        publish_test_environment("1", "发布测试环境"),
        submit_wx_check("2", "提交微信审核"),
        wx_checking("3", "微信审核中"),
        wx_check_success("4", "微信审核成功"),
        publish_online("5", "发布上线"),
        online_success("6", "上线成功");

        private String code;
        private String desc;

        MerchantAppStatus(String code, String desc) {
            this.code = code;
            this.desc = desc;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }
    }

    // 商家的店铺类型
    enum PdsType {
        general("0", "普通商家"), platform("1", "平台商家");

        private String type;
        private String desc;

        PdsType(String type, String desc) {
            this.type = type;
            this.desc = desc;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }
    }

    String merchantFilesGroupNum = "merchant_files"; //商户资料组编号

}
