package com.haohan.cloud.scm.purchase.core;

import com.haohan.cloud.scm.api.opc.entity.SummaryOrder;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseOrder;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseOrderDetail;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseReport;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseTask;

import java.util.List;

/**
 * @author dy
 * @date 2019/5/29
 */
public interface PurchaseCoreSevice {

    // 根据 汇总单(采购选项为:单品) 创建 采购部采购单明细
    PurchaseOrderDetail addOrderDetailSingle(SummaryOrder summaryOrder);

    // 根据 汇总单(采购选项为:原材料) 创建 采购部采购单明细
    PurchaseOrderDetail addOrderDetailRecipe(SummaryOrder summaryOrder);

    // 新增采购部采购单明细(采购计划)
    PurchaseOrderDetail addOrderDetailPlan(PurchaseOrderDetail purchaseOrderDetail);

    // 新增采购部采购单明细(采购需求)
    PurchaseOrderDetail addOrderDetailNeeds(PurchaseOrderDetail purchaseOrderDetail);

    // 新增采购部采购单(合并采购单明细)
    PurchaseOrder createPurchaseOrder(List<PurchaseOrderDetail> list);

    // 新增 采购部采购任务(审核)
    PurchaseTask addaAuditTask(PurchaseTask purchaseTask);

    // 审核通过 新增 采购部采购任务(分配)
    PurchaseTask addAppointTask(PurchaseTask purchaseTask);

    // 指派采购员 新增 采购部采购任务(采购)
    PurchaseTask addPurchaseTask(PurchaseTask purchaseTask);

    // 采购任务 状态变更/修改
    PurchaseTask modifyPurchaseTask(PurchaseTask purchaseTask);

    // 执行采购 确定供应商/价格
    PurchaseOrderDetail confirmPurchase(PurchaseOrderDetail purchaseOrderDetail);

    // 发起采购汇报 (日常汇报)
    PurchaseReport addDailyReport(PurchaseReport purchaseReport);

    // 发起采购汇报 (订单异常汇报)
    PurchaseReport addOrderReport(PurchaseReport purchaseReport);

//    // 采购员申请交货
//    boolean applicationDelivery(PurchaseOrderDetail purchaseOrderDetail);

}