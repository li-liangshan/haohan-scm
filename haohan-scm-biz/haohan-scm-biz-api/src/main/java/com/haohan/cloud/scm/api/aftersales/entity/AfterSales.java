/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.haohan.cloud.scm.api.aftersales.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.haohan.cloud.scm.api.constant.enums.aftersales.AfterOrderTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.aftersales.AfterSalesStatusEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 售后单
 *
 * @author haohan
 * @date 2019-05-13 20:29:17
 */
@Data
@TableName("scm_ass_after_sales")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "售后单")
public class AfterSales extends Model<AfterSales> {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 平台商家id
     */
    private String pmId;
    /**
     * 售后单编号
     */
    private String afterSalesSn;
    /**
     * 售后金额
     */
    private BigDecimal afterSalesAmount;
    /**
     * 联系人uid
     */
    private String linkManUid;
    /**
     * 联系人名称
     */
    private String linkManName;
    /**
     * 联系人电话
     */
    private String linkManTelephone;
    /**
     * 售后订单类型:1.供应商报价单2.B客户采购单3.C客户零售单
     */
    private AfterOrderTypeEnum afterOrderType;
    /**
     * 供应商报价单编号
     */
    private String offerOrderId;
    /**
     * B客户采购单编号
     */
    private String buyId;
    /**
     * C客户零售单编号
     */
    private String goodsOrderId;
    /**
     * 处理结果
     */
    private String resultDesc;
    /**
     * 售后状态:1已上报2处理中3待确认4已完成
     */
    private AfterSalesStatusEnum afterSalesStatus;
    /**
     * 申请时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime applyTime;
    /**
     * 处理时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime actionTime;
    /**
     * 完成时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime finishTime;
    /**
     * 创建者
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
     * 更新者
     */
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateDate;
    /**
     * 备注信息
     */
    private String remarks;
    /**
     * 删除标记
     */
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;
    /**
     * 租户id
     */
    private Integer tenantId;

}
