package com.haohan.cloud.scm.api.wms.resp;

import com.haohan.cloud.scm.api.constant.enums.product.*;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author xwx
 * @date 2019/7/9
 */
@Data
public class EnterWarehouseDetailResp {
    //入库单明细信息
    private String id;
    /**
     * 平台商家id
     */
    private String pmId;
    /**
     * 入库单编号
     */
    private String enterWarehouseSn;
    /**
     * 入库单明细编号
     */
    private String enterWarehouseDetailSn;
    /**
     * 仓库编号
     */
    private String warehouseSn;
    /**
     * 货架编号
     */
    private String shelfSn;
    /**
     * 货位编号
     */
    private String cellSn;
    /**
     * 托盘编号
     */
    private String palletSn;
    /**
     * 汇总单编号
     */
    private String summaryOrderId;
    /**
     * 采购单明细编号
     */
    private String purchaseDetailSn;
    /**
     * 入库商品规格id
     */
    private String goodsModelId;
    /**
     * 入库货品编号
     */
    private String productSn;
    /**
     * 入库货品数量
     */
    private BigDecimal productNumber;
    /**
     * 货品单位
     */
    private String unit;
    /**
     * 入库申请人
     */
    private String applicantId;
    /**
     * 申请人名称
     */
    private String applicantName;
    /**
     * 入库申请时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime applyTime;
    /**
     * 验收人
     */
    private String auditorId;
    /**
     * 验收人名称
     */
    private String auditorName;
    /**
     * 验收时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime auditTime;
    /**
     * 入库状态:1待处理2验收中3已验收4.售后待处理
     */
    private EnterStatusEnum enterStatus;
    /**
     * 货品处理类型:1.入库存储2.转配送
     */
    private StorageTypeEnum storageType;

    //货品信息
    /**
     * 商品id,spu
     */
    private String goodsId;
    /**
     * 货品名称
     */
    private String productName;

    /**
     * 货品状态1.正常2.损耗3.分装
     */
    private ProductStatusEnum productStatus;
    /**
     * 货品质量1.正常2.残次品3.废品
     */
    private ProductQialityEnum productQuality;
    /**
     * 货品位置状态:1.采购部2.生产部3.物流部4.门店5.客户
     */
    private ProductPlaceStatusEnum productPlaceStatus;
    /**
     * 货品类型:1.采购货品2.供应货品3.加工货品
     */
    private ProductTypeEnum productType;
    /**
     * 供应商id
     */
    private String supplierId;
    /**
     * 供应商名称
     */
    private String supplierName;
    /**
     * 采购价格/成本价
     */
    private BigDecimal purchasePrice;
    /**
     * 初始采购时间/生产日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime purchaseTime;
    /**
     * 货品有效期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime validityTime;
    /**
     * 加工操作类型:0不加工1去皮2.拆分3配方
     */
    private ProcessingTypeEnum processingType;
    /**
     * 来源货品编号
     */
    private String sourceProductSn;
    /**
     * 货品描述
     */
    private String productDesc;

}
