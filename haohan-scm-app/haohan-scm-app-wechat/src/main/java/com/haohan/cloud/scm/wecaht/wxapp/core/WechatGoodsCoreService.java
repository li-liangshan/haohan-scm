package com.haohan.cloud.scm.wecaht.wxapp.core;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.haohan.cloud.scm.api.goods.dto.GoodsCategoryTree;
import com.haohan.cloud.scm.api.goods.vo.GoodsVO;
import com.haohan.cloud.scm.api.wechat.req.WxGoodsCollectionsReq;
import com.haohan.cloud.scm.api.wechat.req.WxGoodsQueryReq;

import java.util.List;

/**
 * @author dy
 * @date 2020/5/23
 */
public interface WechatGoodsCoreService {


    /**
     * 查询商品详情
     *
     * @param req
     * @return
     */
    GoodsVO fetchInfo(WxGoodsQueryReq req);

    /**
     * 分页查询商品详情列表 带收藏状态
     *
     * @param req
     * @return
     */
    IPage<GoodsVO> findPage(WxGoodsQueryReq req);

    /**
     * 商品分类树查询
     *
     * @param req
     * @return
     */
    List<GoodsCategoryTree> findCategoryTree(WxGoodsQueryReq req);

    /**
     * 商品收藏列表(详情)
     *
     * @param req
     * @return
     */
    IPage<GoodsVO> findCollectionsPage(WxGoodsQueryReq req);

    /**
     * 新增商品收藏
     *
     * @param req
     * @return
     */
    boolean addCollections(WxGoodsCollectionsReq req);

    /**
     * 取消商品收藏
     *
     * @param req
     * @return
     */
    boolean removeCollections(WxGoodsCollectionsReq req);
}
