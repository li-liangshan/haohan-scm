package com.haohan.cloud.scm.api.constant.converter.saleb;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.saleb.BillTypeEnum;

/**
 * @author dy
 * @date 2019/6/15
 */
public class BillTypeEnumConverterUtil implements Converter<BillTypeEnum> {

    @Override
    public BillTypeEnum convert(Object o, BillTypeEnum e) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return BillTypeEnum.getByType(o.toString());
    }

}
