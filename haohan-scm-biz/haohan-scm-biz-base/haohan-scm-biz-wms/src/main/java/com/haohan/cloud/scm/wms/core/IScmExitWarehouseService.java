package com.haohan.cloud.scm.wms.core;

import com.haohan.cloud.scm.api.wms.entity.ExitWarehouseDetail;
import com.haohan.cloud.scm.api.wms.req.*;
import com.haohan.cloud.scm.api.wms.resp.ExitWarehouseDetailResp;
import com.haohan.cloud.scm.api.wms.resp.QueryWaitExitResp;

import java.util.List;

/**
 * @author xwx
 * @date 2019/6/13
 */
public interface IScmExitWarehouseService {
    /**
     *  新增出库单 根据出库单明细
     * @param req
     * @return
     */
    Boolean createExitWarehouse(CreateExitWarehouseReq req);


    /**
     * 创建出库单明细(锁库存) 状态:待出货
     * @param detail
     * @return
     */
    ExitWarehouseDetail createExitWarehouseDetail(ExitWarehouseDetail detail);

    /**
     * 确认出库单明细 实出数量
     * @param req 必需 id/ productNumber
     * @return
     */
    ExitWarehouseDetail confirmExitWarehouseDetail(DetailConfirmRealExitNumReq req);


    /**
     * 同一商品 出库 实出数量确认
     * @param req
     * @return
     */
    Boolean confirmRealExitNum(ConfirmRealExitNumReq req);


    /**
     *  查询待出库列表
     * @param detail
     * @return
     */
    QueryWaitExitResp queryWaitList(ExitWarehouseDetail detail);

    /**
     * 查询出库单明细（包含货品信息）根据出库单号
     *
     * @param req
     * @return
     */
    List<ExitWarehouseDetailResp> queryExitWarehouseDetail(QueryExitWarehouseDetailReq req);

    /**
     * 新增出库单（出库单明细）
     * @param req
     * @return
     */
    Boolean addExitWarehouse(AddExitWarehouseReq req);

    /**
     * 编辑出库单（修改出库单明细）
     * @param req
     * @return
     */
    Boolean redactExitWarehouse(RedactExitWarehouseReq req);

}
