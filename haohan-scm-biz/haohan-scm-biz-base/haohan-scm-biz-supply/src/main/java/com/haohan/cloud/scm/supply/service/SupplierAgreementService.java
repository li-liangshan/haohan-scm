/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.supply.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.supply.entity.SupplierAgreement;

/**
 * 协议供应商品记录
 *
 * @author haohan
 * @date 2019-05-13 17:44:58
 */
public interface SupplierAgreementService extends IService<SupplierAgreement> {

}
