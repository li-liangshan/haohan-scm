/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.manage.feign;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.manage.entity.UPassport;
import com.haohan.cloud.scm.api.manage.req.UPassportReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 用户通行证内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "uPassportFeignService", value = ScmServiceName.SCM_MANAGE)
public interface UPassportFeignService {


    /**
     * 通过id查询用户通行证
     *
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/uPassport/{id}", method = RequestMethod.GET)
    R<UPassport> getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 用户通行证 列表信息
     *
     * @param uPassportReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/uPassport/fetchUPassportPage")
    R<IPage<UPassport>> getUPassportPage(@RequestBody UPassportReq uPassportReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 用户通行证 列表信息
     *
     * @param uPassport 请求对象
     * @return
     */
    @PostMapping("/api/feign/uPassport/fetchUPassportList")
    R<List<UPassport>> getUPassportList(@RequestBody UPassport uPassport, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增用户通行证
     *
     * @param uPassport 用户通行证
     * @return R
     */
    @PostMapping("/api/feign/uPassport/save")
    R<Boolean> save(@RequestBody UPassport uPassport, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增用户通行证 初始设置/校验
     *
     * @param uPassport 用户通行证
     * @return R
     */
    @PostMapping("/api/feign/uPassport/add")
    R<UPassport> add(@RequestBody UPassport uPassport, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改用户通行证
     *
     * @param uPassport 用户通行证
     * @return R
     */
    @PostMapping("/api/feign/uPassport/update")
    R<Boolean> updateById(@RequestBody UPassport uPassport, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除用户通行证
     *
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/uPassport/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/uPassport/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/uPassport/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param uPassportReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/uPassport/countByUPassportReq")
    R countByUPassportReq(@RequestBody UPassportReq uPassportReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param uPassportReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/uPassport/getOneByUPassportReq")
    R<UPassport> getOneByUPassportReq(@RequestBody UPassportReq uPassportReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param uPassportList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/uPassport/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<UPassport> uPassportList, @RequestHeader(SecurityConstants.FROM) String from);


}
