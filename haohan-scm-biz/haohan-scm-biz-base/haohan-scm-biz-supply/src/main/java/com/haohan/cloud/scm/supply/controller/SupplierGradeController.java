/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.supply.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.supply.entity.SupplierGrade;
import com.haohan.cloud.scm.api.supply.req.SupplierGradeReq;
import com.haohan.cloud.scm.supply.service.SupplierGradeService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 供应商评级记录
 *
 * @author haohan
 * @date 2019-05-29 13:13:07
 */
@RestController
@AllArgsConstructor
@RequestMapping("/suppliergrade" )
@Api(value = "suppliergrade", tags = "suppliergrade管理")
public class SupplierGradeController {

    private final SupplierGradeService supplierGradeService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param supplierGrade 供应商评级记录
     * @return
     */
    @GetMapping("/page" )
    public R getSupplierGradePage(Page page, SupplierGrade supplierGrade) {
        return new R<>(supplierGradeService.page(page, Wrappers.query(supplierGrade)));
    }


    /**
     * 通过id查询供应商评级记录
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(supplierGradeService.getById(id));
    }

    /**
     * 新增供应商评级记录
     * @param supplierGrade 供应商评级记录
     * @return R
     */
    @SysLog("新增供应商评级记录" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('supply_suppliergrade_add')" )
    public R save(@RequestBody SupplierGrade supplierGrade) {
        return new R<>(supplierGradeService.save(supplierGrade));
    }

    /**
     * 修改供应商评级记录
     * @param supplierGrade 供应商评级记录
     * @return R
     */
    @SysLog("修改供应商评级记录" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('supply_suppliergrade_edit')" )
    public R updateById(@RequestBody SupplierGrade supplierGrade) {
        return new R<>(supplierGradeService.updateById(supplierGrade));
    }

    /**
     * 通过id删除供应商评级记录
     * @param id id
     * @return R
     */
    @SysLog("删除供应商评级记录" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('supply_suppliergrade_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(supplierGradeService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除供应商评级记录")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('supply_suppliergrade_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(supplierGradeService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询供应商评级记录")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(supplierGradeService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param supplierGradeReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询供应商评级记录总记录}")
    @PostMapping("/countBySupplierGradeReq")
    public R countBySupplierGradeReq(@RequestBody SupplierGradeReq supplierGradeReq) {

        return new R<>(supplierGradeService.count(Wrappers.query(supplierGradeReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param supplierGradeReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据supplierGradeReq查询一条货位信息表")
    @PostMapping("/getOneBySupplierGradeReq")
    public R getOneBySupplierGradeReq(@RequestBody SupplierGradeReq supplierGradeReq) {

        return new R<>(supplierGradeService.getOne(Wrappers.query(supplierGradeReq), false));
    }


    /**
     * 批量修改OR插入
     * @param supplierGradeList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('supply_suppliergrade_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<SupplierGrade> supplierGradeList) {

        return new R<>(supplierGradeService.saveOrUpdateBatch(supplierGradeList));
    }


}
