/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.message.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.message.entity.WechatMessageTemplate;
import com.haohan.cloud.scm.api.message.req.WechatMessageTemplateReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 消息模板内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "WechatMessageTemplateFeignService", value = ScmServiceName.SCM_BIZ_MSG)
public interface WechatMessageTemplateFeignService {


    /**
     * 通过id查询消息模板
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/WechatMessageTemplate/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 消息模板 列表信息
     * @param wechatMessageTemplateReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/WechatMessageTemplate/fetchWechatMessageTemplatePage")
    R getWechatMessageTemplatePage(@RequestBody WechatMessageTemplateReq wechatMessageTemplateReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 消息模板 列表信息
     * @param wechatMessageTemplateReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/WechatMessageTemplate/fetchWechatMessageTemplateList")
    R getWechatMessageTemplateList(@RequestBody WechatMessageTemplateReq wechatMessageTemplateReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增消息模板
     * @param wechatMessageTemplate 消息模板
     * @return R
     */
    @PostMapping("/api/feign/WechatMessageTemplate/add")
    R save(@RequestBody WechatMessageTemplate wechatMessageTemplate, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改消息模板
     * @param wechatMessageTemplate 消息模板
     * @return R
     */
    @PostMapping("/api/feign/WechatMessageTemplate/update")
    R updateById(@RequestBody WechatMessageTemplate wechatMessageTemplate, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除消息模板
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/WechatMessageTemplate/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/WechatMessageTemplate/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/WechatMessageTemplate/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param wechatMessageTemplateReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/WechatMessageTemplate/countByWechatMessageTemplateReq")
    R countByWechatMessageTemplateReq(@RequestBody WechatMessageTemplateReq wechatMessageTemplateReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param wechatMessageTemplateReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/WechatMessageTemplate/getOneByWechatMessageTemplateReq")
    R getOneByWechatMessageTemplateReq(@RequestBody WechatMessageTemplateReq wechatMessageTemplateReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param wechatMessageTemplateList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/WechatMessageTemplate/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<WechatMessageTemplate> wechatMessageTemplateList, @RequestHeader(SecurityConstants.FROM) String from);


}
