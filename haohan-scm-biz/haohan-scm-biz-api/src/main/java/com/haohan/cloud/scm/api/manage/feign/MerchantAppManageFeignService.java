/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.manage.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.manage.entity.MerchantAppManage;
import com.haohan.cloud.scm.api.manage.req.MerchantAppManageReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 商家应用管理内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "MerchantAppManageFeignService", value = ScmServiceName.SCM_MANAGE)
public interface MerchantAppManageFeignService {


    /**
     * 通过id查询商家应用管理
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/MerchantAppManage/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 商家应用管理 列表信息
     * @param merchantAppManageReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/MerchantAppManage/fetchMerchantAppManagePage")
    R getMerchantAppManagePage(@RequestBody MerchantAppManageReq merchantAppManageReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 商家应用管理 列表信息
     * @param merchantAppManageReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/MerchantAppManage/fetchMerchantAppManageList")
    R getMerchantAppManageList(@RequestBody MerchantAppManageReq merchantAppManageReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增商家应用管理
     * @param merchantAppManage 商家应用管理
     * @return R
     */
    @PostMapping("/api/feign/MerchantAppManage/add")
    R save(@RequestBody MerchantAppManage merchantAppManage, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改商家应用管理
     * @param merchantAppManage 商家应用管理
     * @return R
     */
    @PostMapping("/api/feign/MerchantAppManage/update")
    R updateById(@RequestBody MerchantAppManage merchantAppManage, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除商家应用管理
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/MerchantAppManage/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/MerchantAppManage/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/MerchantAppManage/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param merchantAppManageReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/MerchantAppManage/countByMerchantAppManageReq")
    R countByMerchantAppManageReq(@RequestBody MerchantAppManageReq merchantAppManageReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param merchantAppManageReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/MerchantAppManage/getOneByMerchantAppManageReq")
    R getOneByMerchantAppManageReq(@RequestBody MerchantAppManageReq merchantAppManageReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param merchantAppManageList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/MerchantAppManage/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<MerchantAppManage> merchantAppManageList, @RequestHeader(SecurityConstants.FROM) String from);


}
