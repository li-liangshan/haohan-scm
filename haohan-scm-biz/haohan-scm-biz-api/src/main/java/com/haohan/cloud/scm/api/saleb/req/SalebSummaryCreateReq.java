package com.haohan.cloud.scm.api.saleb.req;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.haohan.cloud.scm.api.constant.enums.opc.PdsSummaryStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.PurchaseOptionEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.SummaryStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @author dy
 * @date 2019/6/4
 */
@Data
@ApiModel(description = "商品需求汇总 创建汇总单")
public class SalebSummaryCreateReq {
    /**
     * 平台商家id
     */
    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台商家id",required = true)
    private String pmId;
    /**
     * 送货批次
     */
    @NotBlank(message = "buySeq不能为空")
    @ApiModelProperty(value = "送货批次",required = true)
    private String buySeq;
    /**
     * 商品规格id
     */
    @NotBlank(message = "goodsModelId不能为空")
    @ApiModelProperty(value = "商品规格id",required = true)
    private String goodsModelId;
    /**
     * 送货日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @NotNull(message = "deliveryTime不能为空")
    @ApiModelProperty(value = "送货日期",required = true)
    private LocalDate deliveryTime;
    /**
     * 调配类型:1.采购2.库存
     */
    @NotBlank(message = "allocateType不能为空")
    @ApiModelProperty(value = "调配类型:1.采购2.库存",required = true)
    private String allocateType;
    /**
     * 汇总单号
     */
    private String summaryOrderId;
    /**
     * 商品分类id
     */
    private String goodsCategoryId;
    /**
     * 商品图片
     */
    private String goodsImg;
    /**
     * 商品名称
     */
    private String goodsName;
    /**
     * 市场价
     */
    private BigDecimal marketPrice;
    /**
     * 平台报价
     */
    private BigDecimal platformPrice;
    /**
     * 采购均价
     */
    private BigDecimal buyAvgPrice;
    /**
     * 供应均价
     */
    private BigDecimal supplyAvgPrice;
    /**
     * 实际采购数量
     */
    private BigDecimal realBuyNum;
    /**
     * 需求采购数量
     */
    private BigDecimal needBuyNum;
    /**
     * 最小供应量
     */
    private Integer limitSupplyNum;
    /**
     * 商家数量
     */
    private Integer buyerNum;
    /**
     * 采购日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime buyTime;
    /**
     * 单位
     */
    private String unit;
    /**
     * 状态
     */
    private PdsSummaryStatusEnum status;
    /**
     * 是否生成交易单
     */
    private YesNoEnum isGenTrade;
    /**
     * 备注信息
     */
    private String remarks;
    /**
     * 删除标记
     */
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;
    /**
     * 汇总单状态:1.待处理2.采购中3.配送中.4已完成5.已关闭
     */
    private SummaryStatusEnum summaryStatus;
    /**
     * 汇总时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime summaryTime;
    /**
     * 完成时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime finishTime;
    /**
     * 商品ID(spu)
     */
    private String goodsId;

    /**
     * 商品规格(名称)
     */
    private String goodsModel;
    /**
     * 采购选项:1.单品2.原材料
     */
    private PurchaseOptionEnum purchaseOption;
    /**
     * 租户id
     */
    private Integer tenantId;
    /**
     * 商品库存
     */
    private Integer goodsStorage;

}
