/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.opc.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.opc.entity.PlatformEmployee;
import com.haohan.cloud.scm.opc.mapper.PlatformEmployeeMapper;
import com.haohan.cloud.scm.opc.service.PlatformEmployeeService;
import org.springframework.stereotype.Service;

/**
 * 平台员工管理
 *
 * @author haohan
 * @date 2019-05-13 20:33:57
 */
@Service
public class PlatformEmployeeServiceImpl extends ServiceImpl<PlatformEmployeeMapper, PlatformEmployee> implements PlatformEmployeeService {

}
