package com.haohan.cloud.scm.aftersales.core;

import com.haohan.cloud.scm.api.aftersales.entity.ReturnOrder;
import com.haohan.cloud.scm.api.aftersales.req.AddBuyReturnOrderReq;
import com.haohan.cloud.scm.api.aftersales.req.AddReturnOrderByOfferReq;
import com.haohan.cloud.scm.api.aftersales.req.AddReturnOrderReq;
import com.haohan.cloud.scm.api.aftersales.req.EditReturnOrderReq;
import com.haohan.cloud.scm.api.aftersales.resp.QueryReturnOrderDetailResp;
import com.haohan.cloud.scm.api.aftersales.resp.ReturnOrderResp;

import java.util.List;

/**
 * @author cx
 * @date 2019/7/31
 */
public interface ScmReturnOrderService {

    /**
     * 查询退货单应退金额
     * @param list
     * @return
     */
    List<ReturnOrderResp> queryReturnOrderAmount(List list);

    /**
     * 新增退货单
     * @param req
     * @return
     */
    Boolean addReturnOrder(AddReturnOrderReq req);

    /**
     * 添加b客户退货单
     * @param req
     * @return
     */
    Boolean buyOrderReturn(AddBuyReturnOrderReq req);

    /**
     * 查询退货单详情
     * @param order
     * @return
     */
    QueryReturnOrderDetailResp queryReturnOrderDetail(ReturnOrder order);

    /**
     * 编辑退货单
     * @param req
     * @return
     */
    Boolean editReturnOrder(EditReturnOrderReq req);


    /**
     * 审核入库单
     * @param req
     * @return
     */
    Boolean auditPassOrder(EditReturnOrderReq req);
    /**
     * 退货货入库
     * @param req
     * @return
     */
    Boolean auditPassReturnOrder(EditReturnOrderReq req);

    /**
     * 根据报价单创建退货单
     * @param req
     * @return
     */
    Boolean addReturnOrderByOfferOrder(AddReturnOrderByOfferReq req);

    /**
     * 退货并生成库存
     * @param req
     * @return
     */
    Boolean passReturnOrderAddStock(EditReturnOrderReq req);

    /**
     * 确认收货
     * @param req
     * @return
     */
    Boolean receiptGoods(EditReturnOrderReq req);
}
