package com.haohan.cloud.scm.api.purchase.resp;

import com.haohan.cloud.scm.api.constant.enums.purchase.LendingStatusEnum;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseOrderDetail;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author cx
 * @date 2019/6/20
 */
@Data
@ApiModel(description = "查询请款记录详情小程序需求")
public class PurchaseQueryLendingResp {

    @ApiModelProperty(value = "采购明细信息")
    private PurchaseOrderDetail detail;

    @ApiModelProperty(value = "请款人名称")
    private String applicantName;

    @ApiModelProperty(value = "申请金额")
    private BigDecimal applyAmount;

    @ApiModelProperty(value = "申请时间")
    private LocalDateTime applyTime;

    @ApiModelProperty(value = "申请内容")
    private String applicantContent;

    @ApiModelProperty(value = "请款状态:1.已申请2.通过初审3.待复审4.审核未通过5.待放款6.已放款")
    private LendingStatusEnum lendingStatus;
}
