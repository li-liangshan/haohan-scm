package com.haohan.cloud.framework.validator;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Valid {

	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.METHOD)
	public static @interface StringNullable {
		String[] value();
	}

	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.METHOD)
	public static @interface StringNotNull {
		String[] value();
	}

	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.METHOD)
	public static @interface IntegerNullable {
		String[] value();
	}

	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.METHOD)
	public static @interface IntegerNotNull {
		String[] value();
	}

	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.METHOD)
	public static @interface ByteNullable {
		String[] value();
	}

	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.METHOD)
	public static @interface ByteNotNull {
		String[] value();
	}

	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.METHOD)
	public static @interface LongNullable {
		String[] value();
	}

	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.METHOD)
	public static @interface LongNotNull {
		String[] value();
	}

	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.METHOD)
	public static @interface DoubleNullable {
		String[] value();
	}

	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.METHOD)
	public static @interface DoubleNotNull {
		String[] value();
	}

	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.METHOD)
	public static @interface BooleanNullable {
		String[] value();
	}

	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.METHOD)
	public static @interface BooleanNotNull {
		String[] value();
	}

	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.METHOD)
	public static @interface FloatNullable {
		String[] value();
	}

	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.METHOD)
	public static @interface FloatNotNull {
		String[] value();
	}

	/** 字符串格式：yyyy-MM-dd HH:mm:ss 转为字符串 yyyyMMddHHmmss */
	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.METHOD)
	public static @interface DateStringNullable {
		String[] value();
	}

	/** 字符串格式：yyyy-MM-dd HH:mm:ss 转为字符串 yyyyMMddHHmmss */
	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.METHOD)
	public static @interface DateStringNotNull {
		String[] value();
	}

	/** 字符串格式：yyyy-MM-dd HH:mm:ss 转为Long格式 yyyyMMddHHmmss */
	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.METHOD)
	public static @interface DateNullable {
		String[] value();
	}

	/** 字符串格式：yyyy-MM-dd HH:mm:ss 转为Long格式 yyyyMMddHHmmss */
	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.METHOD)
	public static @interface DateNotNull {
		String[] value();
	}

	/** 字符串格式：yyyy-MM-dd[ HH:mm:ss ] 转为Long格式 yyyyMMdd240000 */
	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.METHOD)
	public static @interface MaxDateNullable {
		String[] value();
	}

	/** 字符串格式：yyyy-MM-dd[ HH:mm:ss ] 转为Long格式 yyyyMMdd240000 */
	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.METHOD)
	public static @interface MaxDateNotNull {
		String[] value();
	}

	/** 字符串格式：yyyy-MM-dd[ HH:mm:ss ] 转为Long格式 yyyyMMdd000000 */
	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.METHOD)
	public static @interface MinDateNullable {
		String[] value();
	}

	/** 字符串格式：yyyy-MM-dd[ HH:mm:ss ] 转为Long格式 yyyyMMdd000000 */
	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.METHOD)
	public static @interface MinDateNotNull {
		String[] value();
	}

}
