/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.goods.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.goods.entity.DeliveryRules;
import com.haohan.cloud.scm.api.goods.req.DeliveryRulesReq;
import com.haohan.cloud.scm.goods.service.DeliveryRulesService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 配送规则
 *
 * @author haohan
 * @date 2019-05-29 14:24:52
 */
@RestController
@AllArgsConstructor
@RequestMapping("/deliveryrules" )
@Api(value = "deliveryrules", tags = "deliveryrules管理")
public class DeliveryRulesController {

    private final DeliveryRulesService deliveryRulesService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param deliveryRules 配送规则
     * @return
     */
    @GetMapping("/page" )
    public R getDeliveryRulesPage(Page page, DeliveryRules deliveryRules) {
        return new R<>(deliveryRulesService.page(page, Wrappers.query(deliveryRules)));
    }


    /**
     * 通过id查询配送规则
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(deliveryRulesService.getById(id));
    }

    /**
     * 新增配送规则
     * @param deliveryRules 配送规则
     * @return R
     */
    @SysLog("新增配送规则" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_deliveryrules_add')" )
    public R save(@RequestBody DeliveryRules deliveryRules) {
        return new R<>(deliveryRulesService.save(deliveryRules));
    }

    /**
     * 修改配送规则
     * @param deliveryRules 配送规则
     * @return R
     */
    @SysLog("修改配送规则" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_deliveryrules_edit')" )
    public R updateById(@RequestBody DeliveryRules deliveryRules) {
        return new R<>(deliveryRulesService.updateById(deliveryRules));
    }

    /**
     * 通过id删除配送规则
     * @param id id
     * @return R
     */
    @SysLog("删除配送规则" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_deliveryrules_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(deliveryRulesService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除配送规则")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_deliveryrules_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(deliveryRulesService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询配送规则")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(deliveryRulesService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param deliveryRulesReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询配送规则总记录}")
    @PostMapping("/countByDeliveryRulesReq")
    public R countByDeliveryRulesReq(@RequestBody DeliveryRulesReq deliveryRulesReq) {

        return new R<>(deliveryRulesService.count(Wrappers.query(deliveryRulesReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param deliveryRulesReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据deliveryRulesReq查询一条货位信息表")
    @PostMapping("/getOneByDeliveryRulesReq")
    public R getOneByDeliveryRulesReq(@RequestBody DeliveryRulesReq deliveryRulesReq) {

        return new R<>(deliveryRulesService.getOne(Wrappers.query(deliveryRulesReq), false));
    }


    /**
     * 批量修改OR插入
     * @param deliveryRulesList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_deliveryrules_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<DeliveryRules> deliveryRulesList) {

        return new R<>(deliveryRulesService.saveOrUpdateBatch(deliveryRulesList));
    }


}
