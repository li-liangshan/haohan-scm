/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.purchase.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseOrder;
import com.haohan.cloud.scm.api.purchase.req.PurchaseOrderReq;
import com.haohan.cloud.scm.purchase.service.PurchaseOrderService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 采购部采购单
 *
 * @author haohan
 * @date 2019-05-29 13:34:55
 */
@RestController
@AllArgsConstructor
@RequestMapping("/purchaseorder" )
@Api(value = "purchaseorder", tags = "purchaseorder管理")
public class PurchaseOrderController {

    private final PurchaseOrderService purchaseOrderService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param purchaseOrder 采购部采购单
     * @return
     */
    @GetMapping("/page" )
    public R getPurchaseOrderPage(Page page, PurchaseOrder purchaseOrder) {
        return new R<>(purchaseOrderService.page(page, Wrappers.query(purchaseOrder)));
    }


    /**
     * 通过id查询采购部采购单
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(purchaseOrderService.getById(id));
    }

    /**
     * 新增采购部采购单
     * @param purchaseOrder 采购部采购单
     * @return R
     */
    @SysLog("新增采购部采购单" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('purchase_purchaseorder_add')" )
    public R save(@RequestBody PurchaseOrder purchaseOrder) {
        return new R<>(purchaseOrderService.save(purchaseOrder));
    }

    /**
     * 修改采购部采购单
     * @param purchaseOrder 采购部采购单
     * @return R
     */
    @SysLog("修改采购部采购单" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('purchase_purchaseorder_edit')" )
    public R updateById(@RequestBody PurchaseOrder purchaseOrder) {
        return new R<>(purchaseOrderService.updateById(purchaseOrder));
    }

    /**
     * 通过id删除采购部采购单
     * @param id id
     * @return R
     */
    @SysLog("删除采购部采购单" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('purchase_purchaseorder_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(purchaseOrderService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除采购部采购单")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('purchase_purchaseorder_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(purchaseOrderService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(purchaseOrderService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param purchaseOrderReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/countByPurchaseOrderReq")
    public R countByPurchaseOrderReq(@RequestBody PurchaseOrderReq purchaseOrderReq) {

        return new R<>(purchaseOrderService.count(Wrappers.query(purchaseOrderReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param purchaseOrderReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/getOneByPurchaseOrderReq")
    public R getOneByPurchaseOrderReq(@RequestBody PurchaseOrderReq purchaseOrderReq) {

        return new R<>(purchaseOrderService.getOne(Wrappers.query(purchaseOrderReq), false));
    }


    /**
     * 批量修改OR插入
     * @param purchaseOrderList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('purchase_purchaseorder_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<PurchaseOrder> purchaseOrderList) {

        return new R<>(purchaseOrderService.saveOrUpdateBatch(purchaseOrderList));
    }


}
