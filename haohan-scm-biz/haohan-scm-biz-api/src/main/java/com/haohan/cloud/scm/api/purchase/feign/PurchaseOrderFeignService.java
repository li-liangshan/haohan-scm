/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.purchase.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.purchase.dto.PurchaseTodayDTO;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseOrder;
import com.haohan.cloud.scm.api.purchase.req.*;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 采购部采购单内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "PurchaseOrderFeignService", value = ScmServiceName.SCM_BIZ_PURCHASE)
public interface PurchaseOrderFeignService {


    /**
     * 通过id查询采购部采购单
     *
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/PurchaseOrder/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 采购部采购单 列表信息
     *
     * @param purchaseOrderReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/PurchaseOrder/fetchPurchaseOrderPage")
    R getPurchaseOrderPage(@RequestBody PurchaseOrderReq purchaseOrderReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 采购部采购单 列表信息
     *
     * @param purchaseOrderReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/PurchaseOrder/fetchPurchaseOrderList")
    R getPurchaseOrderList(@RequestBody PurchaseOrderReq purchaseOrderReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增采购部采购单
     *
     * @param purchaseOrder 采购部采购单
     * @return R
     */
    @PostMapping("/api/feign/PurchaseOrder/add")
    R save(@RequestBody PurchaseOrder purchaseOrder, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改采购部采购单
     *
     * @param purchaseOrder 采购部采购单
     * @return R
     */
    @PostMapping("/api/feign/PurchaseOrder/update")
    R updateById(@RequestBody PurchaseOrder purchaseOrder, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除采购部采购单
     *
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/PurchaseOrder/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/PurchaseOrder/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/PurchaseOrder/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param purchaseOrderReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/PurchaseOrder/countByPurchaseOrderReq")
    R countByPurchaseOrderReq(@RequestBody PurchaseOrderReq purchaseOrderReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param purchaseOrderReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/PurchaseOrder/getOneByPurchaseOrderReq")
    R getOneByPurchaseOrderReq(@RequestBody PurchaseOrderReq purchaseOrderReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param purchaseOrderList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/PurchaseOrder/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<PurchaseOrder> purchaseOrderList, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 根据purchaseOrderDetailSn添加PurchaseOrder
     *
     * @param req
     * @return R
     */
    @PostMapping("/api/feign/PurchaseOrder/addPurchaseOrder")
    R addPurchaseOrder(@RequestBody AddPurchaseOrderReq req, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 查询今日订单总数
     *
     * @param req
     * @param from
     * @return
     */
    @PostMapping("/api/feign/PurchaseOrder/queryTodayPurchaseCount")
    R queryTodayPurchaseCount(@RequestBody CountPurchaseOrderReq req, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 查询今日采购订单总额
     *
     * @param from
     * @return
     */
    @PostMapping("/api/feign/PurchaseOrder/queryTodayAmount")
    R queryTodayAmount(@RequestBody PurchaseTodayDTO dto, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 查询已入库采购单
     *
     * @param dto
     * @param from
     * @return
     */
    @PostMapping("/api/feign/PurchaseOrder/queryEnterPurchase")
    R queryEnterPurchase(@RequestBody PurchaseTodayDTO dto, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 查询已入库商品种类
     *
     * @param dto
     * @param from
     * @return
     */
    @PostMapping("/api/feign/PurchaseOrder/queryEnterGoodsCate")
    R queryEnterGoodsCate(@RequestBody PurchaseTodayDTO dto, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 查询采购状态数量
     *
     * @param pmId
     * @param from
     * @return
     */
    @PostMapping("/api/feign/PurchaseOrder/queryPurchaseStatus/{pmId}")
    R queryPurchaseStatus(@PathVariable("pmId") String pmId, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 查询采购商品分类统计
     *
     * @param pmId
     * @param from
     * @return
     */
    @PostMapping("/api/feign/PurchaseOrder/queryPurchaseGoodsCateCount/{pmId}")
    R queryPurchaseGoodsCateCount(@PathVariable("pmId") String pmId, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 总监 查询采购任务列表(带采购商品信息)
     * @param req
     * @param from
     * @return
     */
    @PostMapping("/api/feign/PurchaseOrder/queryTaskListByDirector")
    R queryTaskListByDirector(@RequestBody PurchasePageReq req,@RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 查询采购任务详情(带采购商品信息)
     * @param req
     * @param from
     * @return
     */
    @PostMapping("/api/feign/PurchaseOrder/queryPurchaseTask")
    R queryPurchaseTask(@RequestBody @Validated QueryPurchaseTaskReq req,@RequestHeader(SecurityConstants.FROM)String from);

    /**
     * 经理 执行采购任务(分配) 指定采购员
     * @param req
     * @param from
     * @return
     */
    @PostMapping("/api/feign/PurchaseOrder/appointTask")
    R appointTask(@RequestBody AppointTaskReq req, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 总监 执行采购任务(审核) 批量通过采购任务
     * @param req
     * @param from
     * @return
     */
    @PostMapping("/api/feign/PurchaseOrder/auditTaskBatch")
    R auditTaskBatch(@RequestBody AuditTaskBatchReq req,@RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 采购单明细 供应商确定(小程序:单品采购)
     * @param req
     * @param from
     * @return
     */
    @PostMapping("/api/feign/PurchaseOrder/singlePurchase")
    R singlePurchase(@RequestBody SinglePurchaseReq req,@RequestHeader(SecurityConstants.FROM)String from);

    /**
     * 采购单明细 供应商确定(小程序:竞价采购)
     * @param req
     * @param from
     * @return
     */
    @PostMapping("/api/feign/PurchaseOrder/selectOrderSupplier")
    R selectOrderSupplier(@RequestBody ChoiceSupplierReq req,@RequestHeader(SecurityConstants.FROM)String from);

    /**
     * 采购单明细 供应商确定(协议供应)
     * @param req
     * @param from
     * @return
     */
    @PostMapping("/api/feign/PurchaseOrder/agreementPurchase")
    R agreementPurchase(@RequestBody AgreementPurchaseReq req,@RequestHeader(SecurityConstants.FROM)String from);

    /**
     * 采购员工(经理/采购员) 查询采购任务列表(带采购商品信息)
     * @param req
     * @param from
     * @return
     */
    @PostMapping("/api/feign/PurchaseOrder/queryPurchaseTaskList")
    R queryPurchaseTaskList(@RequestBody PurchasePageReq req,@RequestHeader(SecurityConstants.FROM)String from);

    /**
     * 采购部  总监发起采购计划
     * @param req
     * @param from
     * @return
     */
    @PostMapping("/api/feign/PurchaseOrder/initiatePurchasePlanByDirector")
    R initiatePurchasePlanByDirector(@RequestBody InitiatePurchasePlanReq req,@RequestHeader(SecurityConstants.FROM) String from);

}

