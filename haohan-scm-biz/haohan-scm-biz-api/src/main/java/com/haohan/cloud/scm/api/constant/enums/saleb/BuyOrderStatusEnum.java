package com.haohan.cloud.scm.api.constant.enums.saleb;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dy
 * @date 2019/5/27
 * 采购单状态：1.已下单2.待确认3.成交4.取消  5.待发货 6.待收货
 */
@Getter
@AllArgsConstructor
public enum BuyOrderStatusEnum implements IBaseEnum {
    /**
     * 采购单状态:已下单
     */
    submit("1", "已下单"),
    /**
     * 采购单状态:待确认
     */
    wait("2", "待确认"),
    /**
     * 采购单状态:成交
     */
    success("3", "成交"),
    /**
     * 采购单状态:取消
     */
    cancel("4", "取消"),
    /**
     * 采购单状态:待发货
     */
    delivery("5", "待发货"),
    /**
     * 采购单状态:待收货
     */
    arrive("6", "待收货");

    private static final Map<String, BuyOrderStatusEnum> MAP = new HashMap<>(8);

    static {
        for (BuyOrderStatusEnum e : BuyOrderStatusEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static BuyOrderStatusEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

    /**
     * 类型
     */
    @EnumValue
    @JsonValue
    private String type;
    /**
     * 描述
     */
    private String desc;

}
