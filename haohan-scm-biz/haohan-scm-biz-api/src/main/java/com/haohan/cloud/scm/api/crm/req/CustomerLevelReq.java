/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.crm.req;

import com.haohan.cloud.scm.api.crm.entity.CustomerLevel;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 客户组织结构
 *
 * @author haohan
 * @date 2019-08-30 11:45:36
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "客户组织结构")
public class CustomerLevelReq extends CustomerLevel {

    private long pageSize;
    private long pageNo;


}
