package com.haohan.cloud.scm.api.constant.converter.saleb;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.saleb.DetailSummaryFlagEnum;

/**
 * @author dy
 * @date 2019/6/1
 */
public class DetailSummaryFlagEnumConverterUtil implements Converter<DetailSummaryFlagEnum> {

    @Override
    public DetailSummaryFlagEnum convert(Object o, DetailSummaryFlagEnum e) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return DetailSummaryFlagEnum.getByType(o.toString());
    }

}
