package com.haohan.cloud.scm.api.constant.enums.crm;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dy
 * @date 2019/8/31
 */
@Getter
@AllArgsConstructor
public enum SalesGoodsTypeEnum implements IBaseEnum {

    /**
     * 商品销售类型:1.普通2.特价 3.赠品
     * 修改 原促销品 名称为 特价
     */
    normal("1", "普通"),
    promotion("2", "特价"),
    gift("3", "赠品");

    private static final Map<String, SalesGoodsTypeEnum> MAP = new HashMap<>(8);
    private static final Map<String, SalesGoodsTypeEnum> DESC_MAP = new HashMap<>(8);

    static {
        for (SalesGoodsTypeEnum e : SalesGoodsTypeEnum.values()) {
            MAP.put(e.getType(), e);
            DESC_MAP.put(e.getDesc(), e);
        }
    }

    @JsonCreator
    public static SalesGoodsTypeEnum getByType(String type) {
        return MAP.get(type);
    }

    public static SalesGoodsTypeEnum getByDesc(String desc) {
        return DESC_MAP.get(desc);
    }

    @EnumValue
    @JsonValue
    private String type;
    private String desc;
}
