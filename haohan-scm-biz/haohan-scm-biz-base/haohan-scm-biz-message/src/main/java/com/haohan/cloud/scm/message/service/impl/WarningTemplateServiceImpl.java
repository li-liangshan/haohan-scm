/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.message.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.constant.NumberPrefixConstant;
import com.haohan.cloud.scm.api.message.entity.WarningTemplate;
import com.haohan.cloud.scm.common.tools.util.ScmIncrementUtil;
import com.haohan.cloud.scm.message.mapper.WarningTemplateMapper;
import com.haohan.cloud.scm.message.service.WarningTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

/**
 * 预警信息模板
 *
 * @author haohan
 * @date 2019-05-13 18:23:26
 */
@Service
public class WarningTemplateServiceImpl extends ServiceImpl<WarningTemplateMapper, WarningTemplate> implements WarningTemplateService {

    @Autowired
    @Lazy
    private ScmIncrementUtil scmIncrementUtil;

    @Override
    public boolean save(WarningTemplate entity) {
        if (StrUtil.isEmpty(entity.getTemplateSn())) {
            String sn = scmIncrementUtil.inrcSnByClass(WarningTemplate.class, NumberPrefixConstant.WARNING_TEMPLATE_SN_PRE);
            entity.setTemplateSn(sn);
        }
        return super.save(entity);
    }

}
