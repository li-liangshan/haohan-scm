/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.supply.req;

import com.haohan.cloud.scm.api.supply.entity.SupplierGoods;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;


/**
 * 供应商货物表
 *
 * @author haohan
 * @date 2019-05-29 13:13:14
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "供应商货物表")
public class SupplierGoodsReq extends SupplierGoods {

    private long pageSize=10;
    private long pageNo=1;
    @NotBlank(message = "uId不能为空")
    private String uid;
    @NotBlank(message = "pmId不能为空")
    private String pmId;



}
