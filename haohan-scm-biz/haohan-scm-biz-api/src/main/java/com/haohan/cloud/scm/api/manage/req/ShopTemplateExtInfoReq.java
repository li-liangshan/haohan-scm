/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.manage.req;

import com.haohan.cloud.scm.api.manage.entity.ShopTemplateExtInfo;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 店铺模板扩展信息
 *
 * @author haohan
 * @date 2019-05-28 20:38:16
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "店铺模板扩展信息")
public class ShopTemplateExtInfoReq extends ShopTemplateExtInfo {

    private long pageSize;
    private long pageNo;




}
