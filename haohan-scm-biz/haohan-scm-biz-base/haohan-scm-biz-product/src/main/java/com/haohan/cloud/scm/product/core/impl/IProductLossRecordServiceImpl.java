package com.haohan.cloud.scm.product.core.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.product.dto.ProcessingResultDTO;
import com.haohan.cloud.scm.api.product.dto.ProcessingSourceDTO;
import com.haohan.cloud.scm.api.product.entity.ProductInfo;
import com.haohan.cloud.scm.api.product.entity.ProductLossRecord;
import com.haohan.cloud.scm.api.product.req.ProductInfoInventoryReq;
import com.haohan.cloud.scm.api.product.resp.LossRecordResp;
import com.haohan.cloud.scm.common.tools.exception.EmptyDataException;
import com.haohan.cloud.scm.common.tools.exception.ErrorDataException;
import com.haohan.cloud.scm.product.core.IProductInfoService;
import com.haohan.cloud.scm.product.core.IProductLossRecordService;
import com.haohan.cloud.scm.product.service.ProductInfoService;
import com.haohan.cloud.scm.product.service.ProductLossRecordService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @author cx
 * @date 2019/8/13
 */

@Service
@AllArgsConstructor
public class IProductLossRecordServiceImpl implements IProductLossRecordService {


    private final IProductInfoService iProductInfoService;

    private final ProductLossRecordService productLossRecordService;

    private final ProductInfoService productInfoService;
    /**
     * 新增报损记录
     * @param info
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public Boolean addLossRecord(ProductInfoInventoryReq info) {
        ProcessingSourceDTO dto = new ProcessingSourceDTO();
        dto.setSourceProductSn(info.getProductSn());
        dto.setSubProductNum(info.getAfterNumber());
        ProcessingResultDTO res = iProductInfoService.productPeeling(dto);
        ProductLossRecord record = new ProductLossRecord();
        record.setProductSn(res.getSourceProduct().getSourceProductSn());
        record.setAfterProductSn(res.getSubProduct().getProductSn());
        record.setLossProductSn(res.getLossProduct().getProductSn());
        record.setLossNumber(res.getLossProduct().getProductNumber());
        record.setLossType(info.getLossType());
        record.setUnit(info.getUnit());
        record.setOriginalNumber(res.getSourceProduct().getProductNumber());
        record.setResultNumber(res.getSubProduct().getProductNumber());
        record.setRecordTime(LocalDateTime.now());
        if(!productLossRecordService.save(record)){
            throw new EmptyDataException();
        }
        if(!productInfoService.save(res.getSubProduct()) ||
                !productInfoService.save(res.getLossProduct()) ||
                    !productInfoService.updateById(res.getSourceProduct())){
            throw new ErrorDataException();
        }
        return true;
    }

    /**
     * 编辑报损记录
     * @param record
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public Boolean editLossRecord(ProductLossRecord record) {
        ProductInfo info = new ProductInfo();
        info.setProductSn(record.getLossProductSn());
        ProductInfo loss = productInfoService.getOne(Wrappers.query(info));
        info.setProductSn(record.getAfterProductSn());
        ProductInfo after = productInfoService.getOne(Wrappers.query(info));
        if(null == loss || null == after ){
            throw new EmptyDataException();
        }
        after.setProductNumber(record.getResultNumber());
        loss.setProductNumber(record.getOriginalNumber().subtract(record.getResultNumber()));
        if(!productInfoService.updateById(after) || !productInfoService.updateById(loss) || !productLossRecordService.updateById(record)){
            throw new ErrorDataException();
        }
        return true;
    }

    /**
     * 查询损耗记录
     * @param page
     * @param record
     * @return
     */
    @Override
    public IPage getLossRecordPage(Page page, ProductLossRecord record) {
        IPage p = productLossRecordService.page(page, Wrappers.query(record));
        if(p.getRecords().isEmpty()){
            throw new EmptyDataException();
        }
        List<LossRecordResp> list = new ArrayList<>();
        p.getRecords().forEach(r -> {
            ProductLossRecord rec = BeanUtil.toBean(r, ProductLossRecord.class);
            LossRecordResp res = new LossRecordResp();
            BeanUtil.copyProperties(rec,res);
            ProductInfo info = new ProductInfo();
            info.setProductSn(rec.getAfterProductSn());
            ProductInfo one = productInfoService.getOne(Wrappers.query(info));
            if(one != null){
                res.setProdcutName(one.getProductName());
            }
        });
        p.setRecords(list);
        return p;
    }
}
