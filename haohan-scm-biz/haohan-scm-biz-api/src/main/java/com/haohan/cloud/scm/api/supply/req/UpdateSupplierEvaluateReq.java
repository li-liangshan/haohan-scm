package com.haohan.cloud.scm.api.supply.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author xwx
 * @date 2019/6/17
 */
@Data
@ApiModel(description = "修改供应商评价")
public class UpdateSupplierEvaluateReq {
    @NotBlank(message = "pmId不为空")
    @ApiModelProperty(value = "平台id" , required = true)
    private String pmId;

    @NotBlank(message = "uid不为空")
    @ApiModelProperty(value = "采购员id" , required = true)
    private String uid;

    @NotBlank(message = "id不为空")
    @ApiModelProperty(value = "供应商评价主键" , required = true)
    private String id;

    @ApiModelProperty(value = "评价得分")
    private String score;

    @ApiModelProperty(value = "评价说明")
    private String evaluateDesc;

}
