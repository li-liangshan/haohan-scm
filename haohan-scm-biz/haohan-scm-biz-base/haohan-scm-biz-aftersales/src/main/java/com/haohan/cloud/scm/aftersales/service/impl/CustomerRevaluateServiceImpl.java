/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.aftersales.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.aftersales.mapper.CustomerRevaluateMapper;
import com.haohan.cloud.scm.aftersales.service.CustomerRevaluateService;
import com.haohan.cloud.scm.api.aftersales.entity.CustomerRevaluate;
import org.springframework.stereotype.Service;

/**
 * 客户服务评分
 *
 * @author haohan
 * @date 2019-05-13 20:13:24
 */
@Service
public class CustomerRevaluateServiceImpl extends ServiceImpl<CustomerRevaluateMapper, CustomerRevaluate> implements CustomerRevaluateService {

}
