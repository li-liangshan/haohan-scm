/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.supply.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.supply.dto.SupplySqlDTO;
import com.haohan.cloud.scm.api.supply.entity.Supplier;
import com.haohan.cloud.scm.supply.mapper.SupplierMapper;
import com.haohan.cloud.scm.supply.service.SupplierService;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 供应商
 *
 * @author haohan
 * @date 2019-05-13 17:44:16
 */
@Service
public class SupplierServiceImpl extends ServiceImpl<SupplierMapper, Supplier> implements SupplierService {

    @Override
    public Set<String> findSupplyMerchantList(SupplySqlDTO query) {
        return baseMapper.findMerchantList(query).stream()
                .filter(Objects::nonNull).collect(Collectors.toSet());
    }

    @Override
    public Supplier fetchByTelephone(String telephone) {
        if (StrUtil.isEmpty(telephone)) {
            return null;
        }
        return baseMapper.selectList(Wrappers.<Supplier>query().lambda()
                .eq(Supplier::getTelephone, telephone)
        ).stream()
                .findFirst().orElse(null);
    }

    @Override
    public Supplier fetchByName(String supplierName) {
        if (StrUtil.isEmpty(supplierName)) {
            return null;
        }
        return baseMapper.selectList(Wrappers.<Supplier>query().lambda()
                .eq(Supplier::getSupplierName, supplierName)
        ).stream()
                .findFirst().orElse(null);
    }
}
