package com.haohan.cloud.scm.api.opc.resp;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.haohan.cloud.scm.api.constant.enums.opc.*;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @author cx
 * @date 2019/5/31
 */
@Data
public class TemporarySummaryOrderResp{

    /**
     * 汇总单主键id
     */
    private String id;
  /**
   * 平台商家id
   */
  private String pmId;
  /**
   * 汇总单号
   */
  private String summaryOrderId;
  /**
   * 商品分类id
   */
  private String goodsCategoryId;
  /**
   * 商品图片
   */
  private String goodsImg;
  /**
   * 商品名称
   */
  private String goodsName;
  /**
   * 市场价
   */
  private BigDecimal marketPrice;
  /**
   * 平台报价
   */
  private BigDecimal platformPrice;
  /**
   * 采购均价
   */
  private BigDecimal buyAvgPrice;
  /**
   * 供应均价
   */
  private BigDecimal supplyAvgPrice;
  /**
   * 实际采购数量
   */
  private BigDecimal realBuyNum;
  /**
   * 需求采购数量
   */
  private BigDecimal needBuyNum;
  /**
   * 最小供应量
   */
  private Integer limitSupplyNum;
  /**
   * 商家数量
   */
  private Integer buyerNum;
  /**
   * 采购日期
   */
  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private LocalDateTime buyTime;
  /**
   * 送货日期
   */
  @DateTimeFormat(pattern = "yyyy-MM-dd")
  private LocalDate deliveryTime;
  /**
   * 单位
   */
  private String unit;
  /**
   * 状态
   */
  private PdsSummaryStatusEnum status;
  /**
   * 是否生成交易单
   */
  private YesNoEnum isGenTrade;
  /**
   * 采购批次
   */
  private BuySeqEnum buySeq;
  /**
   * 备注信息
   */
  private String remarks;
  /**
   * 删除标记
   */
  @TableLogic
  @TableField(fill = FieldFill.INSERT)
  private String delFlag;
  /**
   * 汇总单状态:1.待处理2.采购中3.配送中.4已完成5.已关闭
   */
  private SummaryStatusEnum summaryStatus;
  /**
   * 调配类型:1.采购2.库存
   */
  private AllocateTypeEnum allocateType;
  /**
   * 汇总时间
   */
  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private LocalDateTime summaryTime;
  /**
   * 完成时间
   */
  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private LocalDateTime finishTime;
  /**
   * 商品ID(spu)
   */
  private String goodsId;
  /**
   * 商品规格id
   */
  private String goodsModelId;
  /**
   * 商品规格(名称)
   */
  private String goodsModel;
  /**
   * 采购选项:1.单品2.原材料
   */
  private PurchaseOptionEnum purchaseOption;
  /**
   * 租户id
   */
  private Integer tenantId;
  /**
   * 商品库存
   */
  private BigDecimal goodsStorage;

    /**
     * 订单数量
     */
    private Integer orderNum;
    /**
     * 损耗率
     */
    private BigDecimal lossRate;
}
