package com.haohan.cloud.scm.api.purchase.req;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 *
 * @author cx
 * @date 2019/7/17
 */

@Data
public class OrderDetailReq {

    @NotBlank(message = "pmId不能为空")
    private String pmId;

    @NotBlank(message = "purchaseSn不能为空")
    private String purchaseSn;
}
