/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.supply.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.constant.NumberPrefixConstant;
import com.haohan.cloud.scm.api.supply.entity.SupplyOrder;
import com.haohan.cloud.scm.common.tools.util.ScmIncrementUtil;
import com.haohan.cloud.scm.supply.mapper.SupplyOrderMapper;
import com.haohan.cloud.scm.supply.service.SupplyOrderService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * 供应订单
 *
 * @author haohan
 * @date 2019-09-21 14:58:37
 */
@Service
@AllArgsConstructor
public class SupplyOrderServiceImpl extends ServiceImpl<SupplyOrderMapper, SupplyOrder> implements SupplyOrderService {

    private final ScmIncrementUtil scmIncrementUtil;

    @Override
    public boolean save(SupplyOrder entity) {
        if (StrUtil.isEmpty(entity.getSupplySn())) {
            String sn = scmIncrementUtil.inrcSnByClass(SupplyOrder.class, NumberPrefixConstant.SUPPLY_ORDER_SN_PRE);
            entity.setSupplySn(sn);
        }
        return super.save(entity);
    }

    @Override
    public SupplyOrder fetchBySn(String supplySn) {
        return baseMapper.selectList(Wrappers.<SupplyOrder>query().lambda()
                .eq(SupplyOrder::getSupplySn, supplySn)
        ).stream()
                .findFirst().orElse(null);
    }
}
