package com.haohan.cloud.scm.api.constant.enums.manage;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dy
 * @date 2019/6/10
 */
@Getter
@AllArgsConstructor
public enum PdsRegFromEnum implements IBaseEnum {
    /**
     * 注册类型 0 ：Web端；1： 移动端； 2： Wap端
     */
    web("0", "Web端"),
    mobile("1", "移动端"),
    wap("2", "Wap端");

    private static final Map<String, PdsRegFromEnum> MAP = new HashMap<>(8);

    static {
        for (PdsRegFromEnum e : PdsRegFromEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static PdsRegFromEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

    @EnumValue
    @JsonValue
    private String type;
    private String desc;
}
