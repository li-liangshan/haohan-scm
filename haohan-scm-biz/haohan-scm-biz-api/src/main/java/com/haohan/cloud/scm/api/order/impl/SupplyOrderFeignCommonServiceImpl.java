package com.haohan.cloud.scm.api.order.impl;

import com.haohan.cloud.scm.api.bill.dto.OrderInfoDTO;
import com.haohan.cloud.scm.api.constant.enums.crm.PayStatusEnum;
import com.haohan.cloud.scm.api.order.OrderCommonService;
import com.haohan.cloud.scm.api.supply.feign.SupplyOrderFeignService;
import com.haohan.cloud.scm.common.tools.exception.ErrorDataException;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author dy
 * @date 2020/3/6
 */
@Service
@AllArgsConstructor
public class SupplyOrderFeignCommonServiceImpl implements OrderCommonService {

    private final SupplyOrderFeignService supplyOrderFeignService;

    @Override
    public OrderInfoDTO fetchOrderInfo(String orderSn) {
        R<OrderInfoDTO> r = supplyOrderFeignService.fetchOrderIfo(orderSn, SecurityConstants.FROM_IN);
        if (RUtil.isFailed(r) || null == r.getData()) {
            throw new ErrorDataException("查询供应单详情失败");
        }
        return r.getData();
    }

    @Override
    public boolean updateOrderSettlement(String orderSn, PayStatusEnum status) {
        // 目前订单无支付状态
        return true;
    }

    @Override
    public void orderCompleteShip(String orderSn) {
        // todo 修改供应订单发货状态:待收货
    }
}
