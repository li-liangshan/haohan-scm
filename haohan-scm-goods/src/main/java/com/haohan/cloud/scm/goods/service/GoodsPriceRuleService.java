/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.goods.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.goods.entity.GoodsPriceRule;

/**
 * 定价规则/市场价/销售价/VIP价格
 *
 * @author haohan
 * @date 2019-05-13 18:46:24
 */
public interface GoodsPriceRuleService extends IService<GoodsPriceRule> {

    GoodsPriceRule fetchByGoodsId(String goodsId);
}
