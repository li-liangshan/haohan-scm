/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.goods.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.goods.entity.Goods;
import com.haohan.cloud.scm.api.goods.req.GoodsReq;
import com.haohan.cloud.scm.api.goods.req.UploadGoodsImageReq;
import com.haohan.cloud.scm.goods.service.GoodsService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 商品
 *
 * @author haohan
 * @date 2019-05-29 14:25:11
 */
@RestController
@AllArgsConstructor
@RequestMapping("/goods" )
@Api(value = "goods", tags = "goods管理")
public class GoodsController {

    private final GoodsService goodsService;
    /**
     * 分页查询
     * @param page 分页对象
     * @param goods 商品
     * @return
     */
    @GetMapping("/page" )
    public R getGoodsPage(Page page, Goods goods) {
        return new R<>(goodsService.page(page, Wrappers.query(goods)));
    }


    /**
     * 通过id查询商品
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(goodsService.getById(id));
    }

    /**
     * 新增商品
     * @param goods 商品
     * @return R
     */
    @SysLog("新增商品" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_goods_add')" )
    public R save(@RequestBody Goods goods) {
        return R.failed("不支持");
    }

    /**
     * 修改商品
     * @param goods 商品
     * @return R
     */
    @SysLog("修改商品" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_goods_edit')" )
    public R updateById(@RequestBody Goods goods) {
        return R.failed("不支持");
    }

    /**
     * 通过id删除商品
     * @param id id
     * @return R
     */
    @SysLog("删除商品" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_goods_del')" )
    public R removeById(@PathVariable String id) {
        return R.failed("不支持");
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除商品")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_goods_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return R.failed("不支持");
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询商品")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(goodsService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param goodsReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询商品总记录}")
    @PostMapping("/countByGoodsReq")
    public R countByGoodsReq(@RequestBody GoodsReq goodsReq) {

        return new R<>(goodsService.count(Wrappers.query(goodsReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param goodsReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据goodsReq查询一条货位信息表")
    @PostMapping("/getOneByGoodsReq")
    public R getOneByGoodsReq(@RequestBody GoodsReq goodsReq) {

        return new R<>(goodsService.getOne(Wrappers.query(goodsReq), false));
    }


    /**
     * 批量修改OR插入
     * @param goodsList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_goods_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<Goods> goodsList) {

        return R.failed("不支持");
    }

    /**
     * 查询c端同步商品信息   (废弃)
     * @return
     */
    @GetMapping("/queryAllGoodsInfo")
    public R queryAllGoodsInfo(){
        return R.failed("不支持");
//        return new R<>(iScmGoodsService.getGoodsInfoDTO());
    }

    /**
     * 查询商品轮播图   (废弃)
     * @param goodsSn
     * @return
     */
    @GetMapping("/getSliderImage/{goodsSn}")
    public R getSliderImage(@PathVariable("goodsSn") String goodsSn){
        return R.failed("不支持");
//        return new R<>(iScmGoodsService.getSliderImage(goodsSn));
    }

    /**
     * 上传轮播图   (废弃)
     * @param req
     * @return
     */
    @PostMapping("/uploadSliderImages")
    public R uploadSliderImages(@RequestBody UploadGoodsImageReq req){
        return R.failed("不支持");
//        return new R<>(iScmGoodsService.uploadSliderImages(req));
    }
}
