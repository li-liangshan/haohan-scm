/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.wms.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.wms.entity.ShelfManagement;
import com.haohan.cloud.scm.api.wms.req.ShelfManagementReq;
import com.haohan.cloud.scm.wms.service.ShelfManagementService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 货品上下架记录
 *
 * @author haohan
 * @date 2019-05-29 13:22:57
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/ShelfManagement")
@Api(value = "shelfmanagement", tags = "shelfmanagement内部接口服务")
public class ShelfManagementFeignApiCtrl {

    private final ShelfManagementService shelfManagementService;


    /**
     * 通过id查询货品上下架记录
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(shelfManagementService.getById(id));
    }


    /**
     * 分页查询 货品上下架记录 列表信息
     * @param shelfManagementReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchShelfManagementPage")
    public R getShelfManagementPage(@RequestBody ShelfManagementReq shelfManagementReq) {
        Page page = new Page(shelfManagementReq.getPageNo(), shelfManagementReq.getPageSize());
        ShelfManagement shelfManagement =new ShelfManagement();
        BeanUtil.copyProperties(shelfManagementReq, shelfManagement);

        return new R<>(shelfManagementService.page(page, Wrappers.query(shelfManagement)));
    }


    /**
     * 全量查询 货品上下架记录 列表信息
     * @param shelfManagementReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchShelfManagementList")
    public R getShelfManagementList(@RequestBody ShelfManagementReq shelfManagementReq) {
        ShelfManagement shelfManagement =new ShelfManagement();
        BeanUtil.copyProperties(shelfManagementReq, shelfManagement);

        return new R<>(shelfManagementService.list(Wrappers.query(shelfManagement)));
    }


    /**
     * 新增货品上下架记录
     * @param shelfManagement 货品上下架记录
     * @return R
     */
    @Inner
    @SysLog("新增货品上下架记录")
    @PostMapping("/add")
    public R save(@RequestBody ShelfManagement shelfManagement) {
        return new R<>(shelfManagementService.save(shelfManagement));
    }

    /**
     * 修改货品上下架记录
     * @param shelfManagement 货品上下架记录
     * @return R
     */
    @Inner
    @SysLog("修改货品上下架记录")
    @PostMapping("/update")
    public R updateById(@RequestBody ShelfManagement shelfManagement) {
        return new R<>(shelfManagementService.updateById(shelfManagement));
    }

    /**
     * 通过id删除货品上下架记录
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除货品上下架记录")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(shelfManagementService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除货品上下架记录")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(shelfManagementService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询货品上下架记录")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(shelfManagementService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param shelfManagementReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询货品上下架记录总记录}")
    @PostMapping("/countByShelfManagementReq")
    public R countByShelfManagementReq(@RequestBody ShelfManagementReq shelfManagementReq) {

        return new R<>(shelfManagementService.count(Wrappers.query(shelfManagementReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param shelfManagementReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据shelfManagementReq查询一条货位信息表")
    @PostMapping("/getOneByShelfManagementReq")
    public R getOneByShelfManagementReq(@RequestBody ShelfManagementReq shelfManagementReq) {

        return new R<>(shelfManagementService.getOne(Wrappers.query(shelfManagementReq), false));
    }


    /**
     * 批量修改OR插入
     * @param shelfManagementList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<ShelfManagement> shelfManagementList) {

        return new R<>(shelfManagementService.saveOrUpdateBatch(shelfManagementList));
    }

}
