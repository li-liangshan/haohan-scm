/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.manage.req;

import com.haohan.cloud.scm.api.manage.entity.ShopTemplate;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 店铺模板
 *
 * @author haohan
 * @date 2019-05-28 20:38:07
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "店铺模板")
public class ShopTemplateReq extends ShopTemplate {

    private long pageSize;
    private long pageNo;




}
