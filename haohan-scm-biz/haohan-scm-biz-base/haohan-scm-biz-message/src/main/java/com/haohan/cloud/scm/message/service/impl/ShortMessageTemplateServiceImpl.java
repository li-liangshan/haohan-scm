/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.message.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.message.entity.ShortMessageTemplate;
import com.haohan.cloud.scm.message.mapper.ShortMessageTemplateMapper;
import com.haohan.cloud.scm.message.service.ShortMessageTemplateService;
import org.springframework.stereotype.Service;

/**
 * 短信模板
 *
 * @author haohan
 * @date 2019-05-13 18:34:19
 */
@Service
public class ShortMessageTemplateServiceImpl extends ServiceImpl<ShortMessageTemplateMapper, ShortMessageTemplate> implements ShortMessageTemplateService {

}
