package com.haohan.cloud.scm.api.constant.enums.aftersales;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

@Getter
@AllArgsConstructor
public enum AfterOrderTypeEnum {
  productorder("1","供应商报价单"),
  salebpurchaseorder("2","B客户采购单"),
  salecretailorder("3","C客户零售单");

    private static final Map<String, AfterOrderTypeEnum> MAP = new HashMap<>(8);

    static {
        for (AfterOrderTypeEnum e : AfterOrderTypeEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static AfterOrderTypeEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

  @EnumValue
  @JsonValue
  private String type;

  private String desc;
}
