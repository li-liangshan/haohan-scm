package com.haohan.cloud.scm.api.aftersales.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author xwx
 * @date 2019/7/23
 */
@Data
@ApiModel(description = "查询售后单详情")
public class QueryAfterSalesDetailReq {
    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台商家ID" , required = true)
    private String pmId;

    @NotBlank(message = "id不能为空")
    @ApiModelProperty(value = "售后单id",required = true)
    private String id;

    @ApiModelProperty(value = "通行证id",required = true)
    private String uid;
}
