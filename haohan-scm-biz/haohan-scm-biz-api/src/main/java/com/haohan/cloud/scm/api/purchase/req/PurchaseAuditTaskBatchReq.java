package com.haohan.cloud.scm.api.purchase.req;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @author dy
 * @date 2019/5/17
 */
@Data
public class PurchaseAuditTaskBatchReq {

    @NotBlank(message = "pmId不能为空")
    private String pmId;

    private List<PurchaseAuditTaskReq> list;

}
