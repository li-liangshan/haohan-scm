package com.haohan.cloud.scm.wms.api.ctrl;

import com.haohan.cloud.scm.api.purchase.req.PurchaseOrderDetailReq;
import com.haohan.cloud.scm.wms.core.IEntryWareHouseService;
import com.pig4cloud.pigx.common.core.util.R;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 *
 *
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/wms/order")
@Api(value = "ApiWmsOrder", tags = "仓储订单接口")
public class WmsOrderApiCtrl {


  @Autowired
  private IEntryWareHouseService entryWareHouseService;

  /**
   * 单个采购货品申请入库
   * 通过采购单明细ID创建入库单
   * @param podDetailId
   * @return
   */
  @PostMapping("/applyEntryWareHouseOne")
  public R createInOrderByPodId(@RequestBody String podDetailId) {

    //获取

    return   entryWareHouseService.applyEntryWareHouseByPodId(podDetailId);
  }

  /**
   *
   * 单个采购货品申请入库
   * @param orderReq
   * @return
   */
  @PostMapping("/createInOrderByPurchaseOrderDetail")
  public R<String> createInOrderByPod(@Validated PurchaseOrderDetailReq orderReq) {

    //获取


    return new R();
  }


  /**
   *
   * 根据采购单申请入库
   * @param purchaseSn
   * @return
   */
  @PostMapping("/createInOrderByPurchaseSn")
  public R<String> createInOrderByPsn(@RequestParam String purchaseSn) {

    //获取


    return new R();
  }







}
