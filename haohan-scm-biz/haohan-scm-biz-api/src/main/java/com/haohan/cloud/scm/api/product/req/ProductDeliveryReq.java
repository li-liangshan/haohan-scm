package com.haohan.cloud.scm.api.product.req;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 运营 发起送货
 *
 * @author cx
 * @date 2019-05-31 11:33:22
 */
@Data
public class ProductDeliveryReq {

  @NotBlank(message = "pmId不能为空")
  private String pmId;

  @NotBlank(message = "id不能为空")
  private String id;

  @NotBlank(message = "汇总单号不能为空")
  private String summaryOrderId;

  private String userId;
}
