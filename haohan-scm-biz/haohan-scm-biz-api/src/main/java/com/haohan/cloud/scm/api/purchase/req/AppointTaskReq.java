package com.haohan.cloud.scm.api.purchase.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

/**
 * @author cx
 * @date 2019/6/10
 */
@Data
@ApiModel(description = "经理 执行采购任务(分配) 指定采购员")
public class AppointTaskReq {

    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台商家id", required = true)
    private String pmId;

    @NotBlank(message = "id不能为空")
    @ApiModelProperty(value = "采购任务id", required = true)
    private String id;

    @NotBlank(message = "employeeId不能为空")
    @ApiModelProperty(value = "采购员工id", required = true)
    private String employeeId;

    @ApiModelProperty(value = "通行证id")
    private String uid;

    @ApiModelProperty(value = "任务执行备注")
    private String actionDesc;

    @ApiModelProperty(value = "任务内容说明")
    private String content;

    @ApiModelProperty(value = "任务截止时间")
    private LocalDateTime deadlineTime;
}
