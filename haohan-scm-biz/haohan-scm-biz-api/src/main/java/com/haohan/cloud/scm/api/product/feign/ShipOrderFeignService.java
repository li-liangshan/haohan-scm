/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.product.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.product.entity.ShipOrder;
import com.haohan.cloud.scm.api.product.req.ShipOrderReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 送货单内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "ShipOrderFeignService", value = ScmServiceName.SCM_BIZ_PRODUCT)
public interface ShipOrderFeignService {


    /**
     * 通过id查询送货单
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/ShipOrder/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 送货单 列表信息
     * @param shipOrderReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/ShipOrder/fetchShipOrderPage")
    R getShipOrderPage(@RequestBody ShipOrderReq shipOrderReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 送货单 列表信息
     * @param shipOrderReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/ShipOrder/fetchShipOrderList")
    R getShipOrderList(@RequestBody ShipOrderReq shipOrderReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增送货单
     * @param shipOrder 送货单
     * @return R
     */
    @PostMapping("/api/feign/ShipOrder/add")
    R save(@RequestBody ShipOrder shipOrder, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改送货单
     * @param shipOrder 送货单
     * @return R
     */
    @PostMapping("/api/feign/ShipOrder/update")
    R updateById(@RequestBody ShipOrder shipOrder, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除送货单
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/ShipOrder/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/ShipOrder/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/ShipOrder/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param shipOrderReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/ShipOrder/countByShipOrderReq")
    R countByShipOrderReq(@RequestBody ShipOrderReq shipOrderReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param shipOrderReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/ShipOrder/getOneByShipOrderReq")
    R getOneByShipOrderReq(@RequestBody ShipOrderReq shipOrderReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param shipOrderList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/ShipOrder/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<ShipOrder> shipOrderList, @RequestHeader(SecurityConstants.FROM) String from);


}
