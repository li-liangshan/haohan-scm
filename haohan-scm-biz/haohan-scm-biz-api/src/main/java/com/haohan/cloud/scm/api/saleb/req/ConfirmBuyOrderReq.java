package com.haohan.cloud.scm.api.saleb.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author dy
 * @date 2019/6/14
 */
@Data
@ApiModel("确认采购单的所有明细已成交")
public class ConfirmBuyOrderReq {

    @ApiModelProperty(value = "采购单编号", required = true)
    @NotBlank(message = "buyId不能为空")
    private String buyId;

}
