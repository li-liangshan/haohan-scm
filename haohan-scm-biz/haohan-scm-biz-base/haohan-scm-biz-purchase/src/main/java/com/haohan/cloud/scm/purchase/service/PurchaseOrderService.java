/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.purchase.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.purchase.dto.PurchaseCateCountDTO;
import com.haohan.cloud.scm.api.purchase.dto.PurchaseTodayDTO;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseOrder;
import com.haohan.cloud.scm.api.purchase.req.CountPurchaseOrderReq;

import java.math.BigDecimal;
import java.util.List;

/**
 * 采购部采购单
 *
 * @author haohan
 * @date 2019-05-13 18:48:11
 */
public interface PurchaseOrderService extends IService<PurchaseOrder> {
    /**
     * 查询今日采购单数
     * @param req
     * @return
     */
    Integer countPurchaseOrderNum(CountPurchaseOrderReq req);

    /**
     * 查询今日采购总额
     * @param dto
     * @return
     */
    BigDecimal queryTodayAmount(PurchaseTodayDTO dto);

    /**
     * 查询入库采购单
     * @param dto
     * @return
     */
    Integer queryEnterPurchase(PurchaseTodayDTO dto);

    /**
     * 查询已入库商品种类
     * @return
     */
    Integer queryEnterGoodsCate(PurchaseTodayDTO dto);

    /**
     * 查询采购商品分类统计
     * @param pmId
     * @return
     */
    List<PurchaseCateCountDTO> queryPurchaseGoodsCateCount(String pmId);
}

