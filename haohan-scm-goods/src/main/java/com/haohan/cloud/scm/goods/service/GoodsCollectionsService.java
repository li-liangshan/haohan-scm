/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.goods.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import com.haohan.cloud.scm.api.goods.dto.GoodsSqlDTO;
import com.haohan.cloud.scm.api.goods.entity.Goods;
import com.haohan.cloud.scm.api.goods.entity.GoodsCollections;

/**
 * 收藏商品
 *
 * @author haohan
 * @date 2019-05-13 20:37:43
 */
public interface GoodsCollectionsService extends IService<GoodsCollections> {

    YesNoEnum fetchStatus(String uid, String goodsId);

    GoodsCollections fetchCollections(String uid, String goodsId);

    /**
     * goods联查 goodsCollections
     *
     * @param query
     * @return
     */
    IPage<Goods> findPage(GoodsSqlDTO query);
}
