/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.goods.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haohan.cloud.scm.api.goods.entity.ProductAttrValue;

/**
 * 商品库属性值
 *
 * @author haohan
 * @date 2019-05-13 19:06:21
 */
public interface ProductAttrValueMapper extends BaseMapper<ProductAttrValue> {

}
