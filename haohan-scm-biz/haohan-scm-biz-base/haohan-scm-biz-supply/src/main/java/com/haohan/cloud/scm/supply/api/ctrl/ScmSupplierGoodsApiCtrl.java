package com.haohan.cloud.scm.supply.api.ctrl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.supply.entity.SupplierGoods;
import com.haohan.cloud.scm.api.supply.req.AddSupplierGoodsReq;
import com.haohan.cloud.scm.api.supply.req.SupplierGoodsDetailReq;
import com.haohan.cloud.scm.api.supply.req.SupplierGoodsListReq;
import com.haohan.cloud.scm.api.supply.req.UpdateSupplierGoodsReq;
import com.haohan.cloud.scm.supply.core.IScmSupplierGoodsService;
import com.haohan.cloud.scm.supply.service.SupplierGoodsService;
import com.haohan.cloud.scm.supply.utils.ScmSupplyUtils;
import com.pig4cloud.pigx.common.core.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @author xwx
 * @date 2019/5/17
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/supplierGoods")
@Api(value = "ApiScmSupplierGoods", tags = "scmSupplierGoods供应商货物管理(给采购部的接口)api")
public class ScmSupplierGoodsApiCtrl {

    @Autowired
    @Lazy
    private IScmSupplierGoodsService scmSupplierGoodsService;
    @Autowired
    @Lazy
    private SupplierGoodsService supplierGoodsService;
    @Autowired
    @Lazy
    private ScmSupplyUtils scmSupplyUtils;


    /**
     * 查询供应商 售卖商品信息列表
     * @param req   必须：pmId/uId/supplierId
     * @return
     */
    @GetMapping("/queryList")
    @ApiOperation(value = "查询供应商 售卖商品信息列表")
    public R querySupplierGoodsList(@Validated SupplierGoodsListReq req) {
        if (StrUtil.isEmpty(req.getUid())) {
            //判断供应商是否被该采购员工管理
            scmSupplyUtils.fetchByBuyerUid(req.getUid(), req.getSupplierId());
        }
        Page page = scmSupplierGoodsService.querySupplierGoodsList(req);
        return new R<>(page);
    }

    /**
     * 查询供应商 售卖商品信息详情
     * @param req  必须：pmId/uId/supplierId/id
     * @return
     */
    @GetMapping("/queryDetail")
    @ApiOperation(value = "查询供应商 售卖商品信息详情")
    public R querySupplierGoodsDetail(@Validated SupplierGoodsDetailReq req){
        if (StrUtil.isEmpty(req.getUid())) {
            //判断供应商是否被该采购员工管理
            scmSupplyUtils.fetchByBuyerUid(req.getUid(), req.getSupplierId());
        }
        return new R<>(scmSupplierGoodsService.querySupplierGoodsDetail(req));
    }

    /**
     * 修改供应商 售卖商品
     * @param req   必须：pmId/uid/id
     * @return
     */
    @PostMapping("/update")
    @ApiOperation(value = "修改供应商 售卖商品-小程序")
    public R updateSupplierGoods(@RequestBody @Validated UpdateSupplierGoodsReq req) {
        //查询供应商 售卖商品
        SupplierGoods supplierGoods = new SupplierGoods();
        supplierGoods.setPmId(req.getPmId());
        supplierGoods.setSupplierId(req.getId());
        SupplierGoods one = supplierGoodsService.getOne(Wrappers.query(supplierGoods));
        //判断供应商是否被该采购员工管理
        scmSupplyUtils.fetchByBuyerUid(req.getUid(),one.getSupplierId());
        //修改供应商 售卖商品
        SupplierGoods goods = new SupplierGoods();
        goods.setId(one.getId());
        goods.setStatus(req.getStatus());
        goods.setSupplyType(req.getSupplyType());
        return new R<>(supplierGoodsService.updateById(goods));
    }

    /**
     * 新增供应商 售卖商品
     * @param req  必须：pmId/goodsModelId/supplierId/uid
     * @return
     */
    @PostMapping("/add")
    @ApiOperation(value = "新增供应商 售卖商品-小程序",notes = "根据商品规格id 查询供应商 售卖商品,查到就修改，没查到就新增")
    public R addSupplierGoods( @Validated AddSupplierGoodsReq req){

        return new R<>(scmSupplierGoodsService.addSupplierGoods(req));
    }


}
