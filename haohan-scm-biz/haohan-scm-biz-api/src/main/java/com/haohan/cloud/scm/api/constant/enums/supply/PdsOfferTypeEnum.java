package com.haohan.cloud.scm.api.constant.enums.supply;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xwx
 * @date 2019/5/20
 * 报价单类型
 */
@Getter
@AllArgsConstructor
public enum PdsOfferTypeEnum implements IBaseEnum {
    /**
     * 报价单类型: 平台报价
     */
    platformOffer("1", "平台报价"),
    /**
     * 报价单类型: 市场报价
     */
    bazaarOffer("2", "市场报价"),
    /**
     * 报价单类型: 指定报价
     */
    appointOffer("3", "指定报价"),
    /**
     * 报价单类型: 货源报价
     */
    supplierOffer("4", "货源报价");

  private static final Map<String, PdsOfferTypeEnum> MAP = new HashMap<>(8);

    static {
        for (PdsOfferTypeEnum e : PdsOfferTypeEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static PdsOfferTypeEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

    /**
     * 类型
     */
    @EnumValue
    @JsonValue
    private String type;
    /**
     * 描述
     */
    private String desc;
}
