/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.supply.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.supply.entity.SupplierGoods;
import com.haohan.cloud.scm.api.supply.req.*;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 供应商货物表内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "SupplierGoodsFeignService", value = ScmServiceName.SCM_BIZ_SUPPLY)
public interface SupplierGoodsFeignService {


    /**
     * 通过id查询供应商货物表
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/SupplierGoods/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 供应商货物表 列表信息
     * @param supplierGoodsReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/SupplierGoods/fetchSupplierGoodsPage")
    R getSupplierGoodsPage(@RequestBody SupplierGoodsReq supplierGoodsReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 供应商货物表 列表信息
     * @param supplierGoodsReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/SupplierGoods/fetchSupplierGoodsList")
    R getSupplierGoodsList(@RequestBody SupplierGoodsReq supplierGoodsReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增供应商货物表
     * @param supplierGoods 供应商货物表
     * @return R
     */
    @PostMapping("/api/feign/SupplierGoods/add")
    R save(@RequestBody SupplierGoods supplierGoods, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改供应商货物表
     * @param supplierGoods 供应商货物表
     * @return R
     */
    @PostMapping("/api/feign/SupplierGoods/update")
    R updateById(@RequestBody SupplierGoods supplierGoods, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除供应商货物表
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/SupplierGoods/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/SupplierGoods/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/SupplierGoods/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param supplierGoodsReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/SupplierGoods/countBySupplierGoodsReq")
    R countBySupplierGoodsReq(@RequestBody SupplierGoodsReq supplierGoodsReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param supplierGoodsReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/SupplierGoods/getOneBySupplierGoodsReq")
    R getOneBySupplierGoodsReq(@RequestBody SupplierGoodsReq supplierGoodsReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     * @param supplierGoodsList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/SupplierGoods/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<SupplierGoods> supplierGoodsList, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 查询供应商品库存信息列表
     * @param req
     * @param from
     * @return
     */
    @PostMapping("/api/feign/SupplierGoods/querySupplyGoodsRepertoryList")
    R querySupplyGoodsRepertoryList(@RequestBody @Validated QueryGoodsRepertoryListReq req,@RequestHeader(SecurityConstants.FROM)String from);

    /**
     * 查询供应商 售卖商品信息列表
     * @param req
     * @param from
     * @return
     */
    @PostMapping("/api/feign/SupplierGoods/queryList")
    R querySupplierGoodsList(@RequestBody SupplierGoodsListReq req,@RequestHeader(SecurityConstants.FROM)String from);

    /**
     * 查询供应商 售卖商品信息详情
     * @param req
     * @param from
     * @return
     */
    @PostMapping("/api/feign/SupplierGoods/queryDetail")
    R querySupplierGoodsDetail(@RequestBody SupplierGoodsDetailReq req, @RequestHeader(SecurityConstants.FROM)String from);

    /**
     * 查询供应商品库存详情
     * @param req
     * @param from
     * @return
     */
    @PostMapping("/api/feign/SupplierGoods/querySupplyGoodsRepertoryDetail")
    R querySupplyGoodsRepertoryDetail(@RequestBody QueryGoodsRepertoryDetailReq req, @RequestHeader(SecurityConstants.FROM)String from);

    /**
     * 删除商品的供应关联关系
     *
     * @param req goodsId
     * @return
     */
    @PostMapping("/api/feign/SupplierGoods/deleteRelationByGoods")
    R<Boolean> deleteRelationByGoods(@RequestBody SupplierGoodsReq req, @RequestHeader(SecurityConstants.FROM)String from);

}
