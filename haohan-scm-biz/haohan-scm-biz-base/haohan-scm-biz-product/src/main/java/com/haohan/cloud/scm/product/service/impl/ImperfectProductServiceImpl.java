/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.product.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.product.entity.ImperfectProduct;
import com.haohan.cloud.scm.product.mapper.ImperfectProductMapper;
import com.haohan.cloud.scm.product.service.ImperfectProductService;
import org.springframework.stereotype.Service;

/**
 * 残次货品处理记录
 *
 * @author haohan
 * @date 2019-05-13 18:20:47
 */
@Service
public class ImperfectProductServiceImpl extends ServiceImpl<ImperfectProductMapper, ImperfectProduct> implements ImperfectProductService {

}
