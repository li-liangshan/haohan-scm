package com.haohan.cloud.scm.api.opc.req.payment;

import com.haohan.cloud.scm.api.constant.enums.common.ReviewStatusEnum;
import com.haohan.cloud.scm.api.opc.entity.BuyerPayment;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @author dy
 * @date 2019/8/6
 */
@Data
@ApiModel(description = "审核应收账单")
public class ReviewPaymentReq {

    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台商家id", required = true)
    private String pmId;

    @NotBlank(message = "buyerPaymentId不能为空")
    @ApiModelProperty(value = "应收账单编号", required = true)
    private String buyerPaymentId;

    @NotNull(message = "buyerPayment不能为空")
    @ApiModelProperty(value = "应收货款", required = true)
    private BigDecimal buyerPayment;

    @NotNull(message = "reviewStatus不能为空")
    @ApiModelProperty(value = "账单审核状态", required = true)
    private ReviewStatusEnum reviewStatus;

    @ApiModelProperty(value = "备注信息")
    private String remarks;


    public BuyerPayment transTo() {
        BuyerPayment payment = new BuyerPayment();
        payment.setPmId(this.pmId);
        payment.setBuyerPaymentId(this.buyerPaymentId);
        payment.setBuyerPayment(this.buyerPayment);
        payment.setReviewStatus(this.reviewStatus);
        payment.setRemarks(this.remarks);
        return payment;
    }
}
