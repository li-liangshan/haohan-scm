package com.haohan.cloud.scm.api.saleb.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.opc.entity.SummaryOrder;
import com.haohan.cloud.scm.api.saleb.req.BuyOrderReq;
import com.haohan.cloud.scm.api.saleb.req.SalebBuyOrderDetailQueryReq;
import com.haohan.cloud.scm.api.saleb.req.SalebSummaryGoodsNumReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

/**
 * @author dy
 * @date 2019/5/27
 */
@FeignClient(contextId = "SalebFeignService", value = ScmServiceName.SCM_BIZ_SALEB)
public interface SalebFeignService {

    /**
     * 查询 客户采购单
     * 通过 id buyId
     *
     * @param buyOrder
     * @param from
     * @return
     */
    @PostMapping("/api/feign/saleb/buyOrder/queryOne")
    R queryOneBuyOrder(@RequestBody BuyOrderReq buyOrder, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 查询 采购单明细 关联采购单
     * 通过 id sn 等
     *
     * @param req
     * @param from
     * @return
     */
    @PostMapping("/api/feign/saleb/buyOrderDetail/queryOne")
    R queryOneBuyOrderDetail(@RequestBody SalebBuyOrderDetailQueryReq req, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 查询 采购单明细列表 关联采购单
     * 采购单 参数: buySeq/deliveryTime
     * 明细 参数: summaryFlag/status/buyId/buyDetailSn/pmId/id
     *
     * @param req
     * @param from
     * @return
     */
    @PostMapping("/api/feign/saleb/buyOrderDetail/queryList")
    R queryListBuyOrderDetail(@RequestBody SalebBuyOrderDetailQueryReq req, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 查询 汇总的商品下单数量
     * 通过 商品规格id  送货日期 批次
     *
     * @param req
     * @param from
     * @return
     */
    @PostMapping("/api/feign/saleb/buyOrderDetail/summaryGoodsNum")
    R<SummaryOrder> summaryGoodsNum(@RequestBody SalebSummaryGoodsNumReq req, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 查询 汇总临时商品下单列表
     *
     * @param req
     * @param from
     * @return
     */
    @PostMapping("/api/feign/saleb/buyOrderDetail/summaryGoodsNumList")
    R summaryGoodsNumList(@RequestBody SalebSummaryGoodsNumReq req, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 确认 采购单的所有明细已成交 修改采购单状态为已成交
     *
     * @param buyId
     * @param from
     * @return
     */
    @PostMapping("/api/feign/saleb/buyOrder/confirmBuyOrder")
    R<Boolean> confirmBuyOrder(String buyId, @RequestHeader(SecurityConstants.FROM) String from);

}
