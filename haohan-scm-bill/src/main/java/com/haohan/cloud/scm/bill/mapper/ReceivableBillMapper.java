package com.haohan.cloud.scm.bill.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haohan.cloud.scm.api.bill.dto.BillInfoDTO;
import com.haohan.cloud.scm.api.bill.dto.BillSqlDTO;
import com.haohan.cloud.scm.api.bill.entity.ReceivableBill;

import java.util.List;

/**
 * @author dy
 * @date 2019/11/27
 */
public interface ReceivableBillMapper extends BaseMapper<ReceivableBill> {

    /**
     * 账单统计 总数量、总金额
     *
     * @param params
     * @return total、billAmount
     */
    BillInfoDTO countBill(BillSqlDTO params);

    /**
     * 按下单客户统计 账单金额
     *
     * @param params reviewStatus、billType、settlementStatus、customerIdSet
     * @return customerId、customerName、billAmount
     */
    List<BillInfoDTO> countBillByCustomer(BillSqlDTO params);

}
