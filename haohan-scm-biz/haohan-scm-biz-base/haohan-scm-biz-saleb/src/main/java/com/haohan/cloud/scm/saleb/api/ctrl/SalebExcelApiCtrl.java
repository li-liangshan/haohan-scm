package com.haohan.cloud.scm.saleb.api.ctrl;

import com.haohan.cloud.scm.api.saleb.dto.BuyOrderImport;
import com.haohan.cloud.scm.api.saleb.req.BuyOrderImportDetailReq;
import com.haohan.cloud.scm.api.saleb.req.BuyOrderImportOneDetailReq;
import com.haohan.cloud.scm.api.saleb.req.BuyOrderImportOneReq;
import com.haohan.cloud.scm.api.saleb.req.BuyOrderImportTableReq;
import com.haohan.cloud.scm.api.saleb.trans.BuyOrderImportTrans;
import com.haohan.cloud.scm.common.tools.util.ExcelUtils;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.haohan.cloud.scm.saleb.core.BuyOrderImportCoreService;
import com.pig4cloud.pigx.common.core.util.R;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;

/**
 * @author dy
 * @date 2019/9/12
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/saleb/excel")
@Slf4j
public class SalebExcelApiCtrl {

    private final BuyOrderImportCoreService buyOrderImportCoreService;

    /**
     * 一个B客户订单 table导入 新增明细
     *
     * @param req 传入 BuyId
     * @return
     */
    @PostMapping("/importDetail")
    public R<String> importDetail(@RequestBody @Valid BuyOrderImportDetailReq req) {
        return buyOrderImportCoreService.addBuyOrderDetail(req.getList(), req.getBuyId());
    }


    /**
     * 按表格导入 B客户订单 可多个订单
     *
     * @return
     */
    @PostMapping("/importTable")
    public R<String> importTable(@RequestBody @Valid BuyOrderImportTableReq req) {
        return buyOrderImportCoreService.saveBuyOrder(req.getList());
    }

    /**
     * 按Excel导入 B客户订单 可多个订单
     *
     * @param file
     * @return
     */
    @PostMapping("/import")
    public R<String> importExcel(@RequestParam(value = "file", required = false) MultipartFile file) {
        R<List<BuyOrderImport>> r = transExcel(file);
        if (!RUtil.isSuccess(r)) {
            return R.failed(r.getMsg());
        }
        return buyOrderImportCoreService.saveBuyOrder(r.getData());
    }

    /**
     * 导入为一个 B客户订单
     *
     * @param req 传入buyer
     * @return
     */
    @PostMapping("/importOne")
    public R<String> importExcelOne(@Validated BuyOrderImportOneReq req) {
        R<List<BuyOrderImport>> r = transExcel(req.getFile());
        if (!RUtil.isSuccess(r)) {
            return R.failed(r.getMsg());
        }
        return buyOrderImportCoreService.saveOneBuyOrder(r.getData(), req);
    }

    /**
     * 一个B客户订单 excel导入 新增明细
     *
     * @param req 传入 BuyId
     * @return
     */
    @PostMapping("/importOneDetail")
    public R<String> importOneDetail(@Validated BuyOrderImportOneDetailReq req) {
        R<List<BuyOrderImport>> r = transExcel(req.getFile());
        if (!RUtil.isSuccess(r)) {
            return R.failed(r.getMsg());
        }
        return buyOrderImportCoreService.addBuyOrderDetail(r.getData(), req.getBuyId());
    }

    /**
     * excel文件读取
     *
     * @param file
     * @return
     */
    private R<List<BuyOrderImport>> transExcel(MultipartFile file) {
        return ExcelUtils.transExcel(file, BuyOrderImportTrans.HEADER_MAP, BuyOrderImport.class);
    }


}
