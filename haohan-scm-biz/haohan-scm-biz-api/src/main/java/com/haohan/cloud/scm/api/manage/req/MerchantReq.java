/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.manage.req;

import com.haohan.cloud.scm.api.manage.entity.Merchant;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 商家信息
 *
 * @author haohan
 * @date 2019-05-28 20:34:51
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "商家信息")
public class MerchantReq extends Merchant {

    private long pageSize;
    private long pageNo;




}
