/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.manage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.manage.entity.PhotoGroupManage;

/**
 * 图片组管理
 *
 * @author haohan
 * @date 2019-05-13 17:32:43
 */
public interface PhotoGroupManageService extends IService<PhotoGroupManage> {

    /**
     * 根据图片组编号查询
     * @param groupNum
     * @return
     */
    PhotoGroupManage getByGroupNum(String groupNum);
}
