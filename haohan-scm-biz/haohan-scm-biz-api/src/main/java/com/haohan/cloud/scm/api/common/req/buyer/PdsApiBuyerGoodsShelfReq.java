package com.haohan.cloud.scm.api.common.req.buyer;


import com.haohan.cloud.scm.api.common.req.PdsBuyerGoodsReq;

import java.io.Serializable;
import java.util.List;

/**
 * @author shenyu
 * @create 2018/12/12
 */
public class PdsApiBuyerGoodsShelfReq implements Serializable {
    private List<PdsBuyerGoodsReq> goodsReqList;

    public List<PdsBuyerGoodsReq> getGoodsReqList() {
        return goodsReqList;
    }

    public void setGoodsReqList(List<PdsBuyerGoodsReq> goodsReqList) {
        this.goodsReqList = goodsReqList;
    }
}
