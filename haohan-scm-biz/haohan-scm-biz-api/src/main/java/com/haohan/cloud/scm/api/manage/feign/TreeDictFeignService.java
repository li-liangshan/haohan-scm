/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.manage.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.manage.entity.TreeDict;
import com.haohan.cloud.scm.api.manage.req.TreeDictReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 树形字典内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "TreeDictFeignService", value = ScmServiceName.SCM_MANAGE)
public interface TreeDictFeignService {


    /**
     * 通过id查询树形字典
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/TreeDict/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 树形字典 列表信息
     * @param treeDictReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/TreeDict/fetchTreeDictPage")
    R getTreeDictPage(@RequestBody TreeDictReq treeDictReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 树形字典 列表信息
     * @param treeDictReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/TreeDict/fetchTreeDictList")
    R getTreeDictList(@RequestBody TreeDictReq treeDictReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增树形字典
     * @param treeDict 树形字典
     * @return R
     */
    @PostMapping("/api/feign/TreeDict/add")
    R save(@RequestBody TreeDict treeDict, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改树形字典
     * @param treeDict 树形字典
     * @return R
     */
    @PostMapping("/api/feign/TreeDict/update")
    R updateById(@RequestBody TreeDict treeDict, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除树形字典
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/TreeDict/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/TreeDict/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/TreeDict/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param treeDictReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/TreeDict/countByTreeDictReq")
    R countByTreeDictReq(@RequestBody TreeDictReq treeDictReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param treeDictReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/TreeDict/getOneByTreeDictReq")
    R getOneByTreeDictReq(@RequestBody TreeDictReq treeDictReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param treeDictList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/TreeDict/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<TreeDict> treeDictList, @RequestHeader(SecurityConstants.FROM) String from);


}
