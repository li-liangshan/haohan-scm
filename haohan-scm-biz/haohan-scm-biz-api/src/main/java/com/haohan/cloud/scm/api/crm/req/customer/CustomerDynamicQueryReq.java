package com.haohan.cloud.scm.api.crm.req.customer;

import com.haohan.cloud.scm.api.constant.enums.crm.DynamicTypeEnum;
import com.haohan.cloud.scm.api.crm.entity.CustomerDynamicState;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

/**
 * @author dy
 * @date 2019/12/10
 */
@Data
public class CustomerDynamicQueryReq {

    /**
     * 动态编号
     */
    @ApiModelProperty(value = "动态编号")
    private String stateSn;
    /**
     * 客户编码
     */
    @ApiModelProperty(value = "客户编码")
    private String customerSn;
    /**
     * 来源编号
     */
    @ApiModelProperty(value = "来源编号")
    private String sourceSn;
    /**
     * 动态类型 (1.销售订单 2.销量上报 3.库存上报 )
     */
    @ApiModelProperty(value = "动态类型")
    private DynamicTypeEnum stateType;
    /**
     * 联系电话 上报人
     */
    @ApiModelProperty(value = "联系电话")
    private String telephone;
    /**
     * 业务上报人名称
     */
    @ApiModelProperty(value = "业务上报人名称")
    private String reportMan;


    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间-开时时间")
    private LocalDate startDate;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间-结束时间")
    private LocalDate endDate;

    public CustomerDynamicState transTo() {
        CustomerDynamicState dynamic = new CustomerDynamicState();
        // eq参数
        dynamic.setStateSn(this.stateSn);
        dynamic.setCustomerSn(this.customerSn);
        dynamic.setSourceSn(this.sourceSn);
        dynamic.setStateType(this.stateType);
        return dynamic;
    }
}
