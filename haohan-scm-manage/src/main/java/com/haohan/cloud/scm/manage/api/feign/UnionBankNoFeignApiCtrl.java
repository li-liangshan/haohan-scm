/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.manage.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.manage.entity.UnionBankNo;
import com.haohan.cloud.scm.api.manage.req.UnionBankNoReq;
import com.haohan.cloud.scm.manage.service.UnionBankNoService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 联行号
 *
 * @author haohan
 * @date 2019-05-29 14:29:10
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/UnionBankNo")
@Api(value = "unionbankno", tags = "unionbankno内部接口服务")
public class UnionBankNoFeignApiCtrl {

    private final UnionBankNoService unionBankNoService;


    /**
     * 通过id查询联行号
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(unionBankNoService.getById(id));
    }


    /**
     * 分页查询 联行号 列表信息
     * @param unionBankNoReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchUnionBankNoPage")
    public R getUnionBankNoPage(@RequestBody UnionBankNoReq unionBankNoReq) {
        Page page = new Page(unionBankNoReq.getPageNo(), unionBankNoReq.getPageSize());
        UnionBankNo unionBankNo =new UnionBankNo();
        BeanUtil.copyProperties(unionBankNoReq, unionBankNo);

        return new R<>(unionBankNoService.page(page, Wrappers.query(unionBankNo)));
    }


    /**
     * 全量查询 联行号 列表信息
     * @param unionBankNoReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchUnionBankNoList")
    public R getUnionBankNoList(@RequestBody UnionBankNoReq unionBankNoReq) {
        UnionBankNo unionBankNo =new UnionBankNo();
        BeanUtil.copyProperties(unionBankNoReq, unionBankNo);

        return new R<>(unionBankNoService.list(Wrappers.query(unionBankNo)));
    }


    /**
     * 新增联行号
     * @param unionBankNo 联行号
     * @return R
     */
    @Inner
    @SysLog("新增联行号")
    @PostMapping("/add")
    public R save(@RequestBody UnionBankNo unionBankNo) {
        return new R<>(unionBankNoService.save(unionBankNo));
    }

    /**
     * 修改联行号
     * @param unionBankNo 联行号
     * @return R
     */
    @Inner
    @SysLog("修改联行号")
    @PostMapping("/update")
    public R updateById(@RequestBody UnionBankNo unionBankNo) {
        return new R<>(unionBankNoService.updateById(unionBankNo));
    }

    /**
     * 通过id删除联行号
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除联行号")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(unionBankNoService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除联行号")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(unionBankNoService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询联行号")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(unionBankNoService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param unionBankNoReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询联行号总记录}")
    @PostMapping("/countByUnionBankNoReq")
    public R countByUnionBankNoReq(@RequestBody UnionBankNoReq unionBankNoReq) {

        return new R<>(unionBankNoService.count(Wrappers.query(unionBankNoReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param unionBankNoReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据unionBankNoReq查询一条货位信息表")
    @PostMapping("/getOneByUnionBankNoReq")
    public R getOneByUnionBankNoReq(@RequestBody UnionBankNoReq unionBankNoReq) {

        return new R<>(unionBankNoService.getOne(Wrappers.query(unionBankNoReq), false));
    }


    /**
     * 批量修改OR插入
     * @param unionBankNoList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<UnionBankNo> unionBankNoList) {

        return new R<>(unionBankNoService.saveOrUpdateBatch(unionBankNoList));
    }

}
