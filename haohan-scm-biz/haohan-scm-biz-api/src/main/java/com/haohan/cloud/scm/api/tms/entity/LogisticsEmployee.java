/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.tms.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;


/**
 * 物流部员工管理
 *
 * @author haohan
 * @date 2019-06-05 09:26:11
 */
@Data
@TableName("scm_lms_logistics_employee")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "物流部员工管理")
public class LogisticsEmployee extends Model<LogisticsEmployee> {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 平台商家id
     */
    private String pmId;
    /**
     * 通行证ID
     */
    private String passportId;
    /**
     * 用户id
     */
    private String userId;
    /**
     * 名称
     */
    private String name;
    /**
     * 联系电话
     */
    private String telephone;
    /**
     * 物流员工类型:1物流经理2司机3第三方物流
     */
    private String logisticsEmployeeType;
    /**
     * 启用状态:0.未启用1.启用
     */
    private String useStatus;
    /**
     * 创建者
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
     * 更新者
     */
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;
    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.UPDATE)
    @ApiModelProperty(value = "更新时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateDate;
    /**
     * 备注信息
     */
    private String remarks;
    /**
     * 删除标记
     */
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;
    /**
     * 租户id
     */
    private Integer tenantId;

}
