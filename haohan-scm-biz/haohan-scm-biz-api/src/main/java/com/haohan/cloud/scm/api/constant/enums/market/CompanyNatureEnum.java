package com.haohan.cloud.scm.api.constant.enums.market;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xwx
 * @date 2019/5/30
 */
@Getter
@AllArgsConstructor
public enum CompanyNatureEnum implements IBaseEnum {
    /**
     * 公司性质:1.个体2.民营3.国有4.其他
     */
    individual("1", "个体"),
    Private("2", "民营"),
    stateOwned("3", "国有"),
    rests("4", "其他");

    private static final Map<String, CompanyNatureEnum> MAP = new HashMap<>(8);
    private static final Map<String, CompanyNatureEnum> DESC_MAP = new HashMap<>(8);

    static {
        for (CompanyNatureEnum e : CompanyNatureEnum.values()) {
            MAP.put(e.getType(), e);
            DESC_MAP.put(e.getDesc(), e);
        }
    }

    @JsonCreator
    public static CompanyNatureEnum getByType(String type) {
        return MAP.get(type);
    }

    public static CompanyNatureEnum getByDesc(String desc) {
        return DESC_MAP.get(desc);
    }

    /**
     * 类型
     */
    @EnumValue
    @JsonValue
    private String type;
    /**
     * 描述
     */

    private String desc;
}
