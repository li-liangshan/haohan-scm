/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.aftersales.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.aftersales.service.SalesReturnService;
import com.haohan.cloud.scm.api.aftersales.entity.SalesReturn;
import com.haohan.cloud.scm.api.aftersales.req.SalesReturnReq;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 售后退货记录
 *
 * @author haohan
 * @date 2019-05-30 10:28:49
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/SalesReturn")
@Api(value = "salesreturn", tags = "salesreturn内部接口服务")
public class SalesReturnFeignApiCtrl {

    private final SalesReturnService salesReturnService;


    /**
     * 通过id查询售后退货记录
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(salesReturnService.getById(id));
    }


    /**
     * 分页查询 售后退货记录 列表信息
     * @param salesReturnReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchSalesReturnPage")
    public R getSalesReturnPage(@RequestBody SalesReturnReq salesReturnReq) {
        Page page = new Page(salesReturnReq.getPageNo(), salesReturnReq.getPageSize());
        SalesReturn salesReturn =new SalesReturn();
        BeanUtil.copyProperties(salesReturnReq, salesReturn);

        return new R<>(salesReturnService.page(page, Wrappers.query(salesReturn)));
    }


    /**
     * 全量查询 售后退货记录 列表信息
     * @param salesReturnReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchSalesReturnList")
    public R getSalesReturnList(@RequestBody SalesReturnReq salesReturnReq) {
        SalesReturn salesReturn =new SalesReturn();
        BeanUtil.copyProperties(salesReturnReq, salesReturn);

        return new R<>(salesReturnService.list(Wrappers.query(salesReturn)));
    }


    /**
     * 新增售后退货记录
     * @param salesReturn 售后退货记录
     * @return R
     */
    @Inner
    @SysLog("新增售后退货记录")
    @PostMapping("/add")
    public R save(@RequestBody SalesReturn salesReturn) {
        return new R<>(salesReturnService.save(salesReturn));
    }

    /**
     * 修改售后退货记录
     * @param salesReturn 售后退货记录
     * @return R
     */
    @Inner
    @SysLog("修改售后退货记录")
    @PostMapping("/update")
    public R updateById(@RequestBody SalesReturn salesReturn) {
        return new R<>(salesReturnService.updateById(salesReturn));
    }

    /**
     * 通过id删除售后退货记录
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除售后退货记录")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(salesReturnService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除售后退货记录")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(salesReturnService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询售后退货记录")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(salesReturnService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param salesReturnReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询售后退货记录总记录}")
    @PostMapping("/countBySalesReturnReq")
    public R countBySalesReturnReq(@RequestBody SalesReturnReq salesReturnReq) {

        return new R<>(salesReturnService.count(Wrappers.query(salesReturnReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param salesReturnReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据salesReturnReq查询一条货位信息表")
    @PostMapping("/getOneBySalesReturnReq")
    public R getOneBySalesReturnReq(@RequestBody SalesReturnReq salesReturnReq) {

        return new R<>(salesReturnService.getOne(Wrappers.query(salesReturnReq), false));
    }


    /**
     * 批量修改OR插入
     * @param salesReturnList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<SalesReturn> salesReturnList) {

        return new R<>(salesReturnService.saveOrUpdateBatch(salesReturnList));
    }

}
