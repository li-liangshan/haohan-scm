/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.tms.req;

import com.haohan.cloud.scm.api.tms.entity.RouteManage;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 路线管理
 *
 * @author haohan
 * @date 2019-06-05 09:25:40
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "路线管理")
public class RouteManageReq extends RouteManage {

    private long pageSize;
    private long pageNo;




}
