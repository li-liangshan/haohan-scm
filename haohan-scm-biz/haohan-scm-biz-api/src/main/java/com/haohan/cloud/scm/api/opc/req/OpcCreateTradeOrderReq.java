package com.haohan.cloud.scm.api.opc.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author dy
 * @date 2019/5/27
 */
@Data
@ApiModel(description = "分拣匹配交易单")
public class OpcCreateTradeOrderReq {

    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台id",required = true)
    private String pmId;

    @NotBlank(message = "buyDetailSn不能为空")
    @ApiModelProperty(value = "采购明细编号",required = true)
    private String buyDetailSn;
    
    @NotBlank(message = "productSn不能为空")
    @ApiModelProperty(value = "货品编号",required = true)
    private String productSn;

}
