/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.salec.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;

/**
 * @author haohan
 * @date 2020-06-20 14:52:18
 */
@Data
@TableName("eb_store_product_description")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "")
public class StoreProductDescription extends Model<StoreProductDescription> {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "商品ID")
    private Integer productId;

    @Length(max = 65535, message = "商品详情长度最大65535字符")
    @ApiModelProperty(value = "商品详情")
    private String description;

    @ApiModelProperty(value = "商品类型")
    private Integer type;

    @ApiModelProperty(value = "租户id")
    private Integer tenantId;

}
