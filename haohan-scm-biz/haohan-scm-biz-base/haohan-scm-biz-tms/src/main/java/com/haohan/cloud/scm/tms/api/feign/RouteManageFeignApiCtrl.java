/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.tms.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.tms.entity.RouteManage;
import com.haohan.cloud.scm.api.tms.req.RouteManageReq;
import com.haohan.cloud.scm.tms.service.RouteManageService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 路线管理
 *
 * @author haohan
 * @date 2019-06-05 09:25:40
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/RouteManage")
@Api(value = "routemanage", tags = "routemanage内部接口服务")
public class RouteManageFeignApiCtrl {

    private final RouteManageService routeManageService;


    /**
     * 通过id查询路线管理
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(routeManageService.getById(id));
    }


    /**
     * 分页查询 路线管理 列表信息
     * @param routeManageReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchRouteManagePage")
    public R getRouteManagePage(@RequestBody RouteManageReq routeManageReq) {
        Page page = new Page(routeManageReq.getPageNo(), routeManageReq.getPageSize());
        RouteManage routeManage =new RouteManage();
        BeanUtil.copyProperties(routeManageReq, routeManage);

        return new R<>(routeManageService.page(page, Wrappers.query(routeManage)));
    }


    /**
     * 全量查询 路线管理 列表信息
     * @param routeManageReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchRouteManageList")
    public R getRouteManageList(@RequestBody RouteManageReq routeManageReq) {
        RouteManage routeManage =new RouteManage();
        BeanUtil.copyProperties(routeManageReq, routeManage);

        return new R<>(routeManageService.list(Wrappers.query(routeManage)));
    }


    /**
     * 新增路线管理
     * @param routeManage 路线管理
     * @return R
     */
    @Inner
    @SysLog("新增路线管理")
    @PostMapping("/add")
    public R save(@RequestBody RouteManage routeManage) {
        return new R<>(routeManageService.save(routeManage));
    }

    /**
     * 修改路线管理
     * @param routeManage 路线管理
     * @return R
     */
    @Inner
    @SysLog("修改路线管理")
    @PostMapping("/update")
    public R updateById(@RequestBody RouteManage routeManage) {
        return new R<>(routeManageService.updateById(routeManage));
    }

    /**
     * 通过id删除路线管理
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除路线管理")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(routeManageService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除路线管理")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(routeManageService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询路线管理")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(routeManageService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param routeManageReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询路线管理总记录}")
    @PostMapping("/countByRouteManageReq")
    public R countByRouteManageReq(@RequestBody RouteManageReq routeManageReq) {

        return new R<>(routeManageService.count(Wrappers.query(routeManageReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param routeManageReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据routeManageReq查询一条货位信息表")
    @PostMapping("/getOneByRouteManageReq")
    public R getOneByRouteManageReq(@RequestBody RouteManageReq routeManageReq) {

        return new R<>(routeManageService.getOne(Wrappers.query(routeManageReq), false));
    }


    /**
     * 批量修改OR插入
     * @param routeManageList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<RouteManage> routeManageList) {

        return new R<>(routeManageService.saveOrUpdateBatch(routeManageList));
    }

}
