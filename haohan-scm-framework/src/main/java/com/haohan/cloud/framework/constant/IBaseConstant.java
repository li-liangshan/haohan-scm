package com.haohan.cloud.framework.constant;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * 基本常亮类
 * @author zhaokuner
 *
 */
public interface IBaseConstant {


	Charset DEFAULT_CHARSET = StandardCharsets.UTF_8;

}
