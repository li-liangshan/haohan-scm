package com.haohan.cloud.scm.api.wms.req;

import com.haohan.cloud.scm.api.constant.enums.product.ExitTypeEnum;
import com.haohan.cloud.scm.api.wms.entity.ExitWarehouse;
import com.haohan.cloud.scm.api.wms.resp.ExitWarehouseDetailResp;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author xwx
 * @date 2019/7/17
 */
@Data
@ApiModel(description = "编辑出库单（修改出库单明细）")
@EqualsAndHashCode(callSuper = true)
public class RedactExitWarehouseReq extends ExitWarehouse {
    private List<ExitWarehouseDetailResp> list;

    private List<GoodsModelDTONumReq> addList;

    private List<ExitWarehouseDetailResp> delList;

    private ExitTypeEnum exitType;

}
