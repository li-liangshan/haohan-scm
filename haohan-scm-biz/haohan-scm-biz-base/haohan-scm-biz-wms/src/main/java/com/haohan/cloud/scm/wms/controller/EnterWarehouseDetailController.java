/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.wms.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.wms.entity.EnterWarehouseDetail;
import com.haohan.cloud.scm.api.wms.req.EnterWarehouseDetailReq;
import com.haohan.cloud.scm.wms.service.EnterWarehouseDetailService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 入库单明细
 *
 * @author haohan
 * @date 2019-05-29 13:22:08
 */
@RestController
@AllArgsConstructor
@RequestMapping("/enterwarehousedetail" )
@Api(value = "enterwarehousedetail", tags = "enterwarehousedetail管理")
public class EnterWarehouseDetailController {

    private final EnterWarehouseDetailService enterWarehouseDetailService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param enterWarehouseDetail 入库单明细
     * @return
     */
    @GetMapping("/page" )
    public R getEnterWarehouseDetailPage(Page page, EnterWarehouseDetail enterWarehouseDetail) {
        return new R<>(enterWarehouseDetailService.page(page, Wrappers.query(enterWarehouseDetail)));
    }


    /**
     * 通过id查询入库单明细
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(enterWarehouseDetailService.getById(id));
    }

    /**
     * 新增入库单明细
     * @param enterWarehouseDetail 入库单明细
     * @return R
     */
    @SysLog("新增入库单明细" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_enterwarehousedetail_add')" )
    public R save(@RequestBody EnterWarehouseDetail enterWarehouseDetail) {
        return new R<>(enterWarehouseDetailService.save(enterWarehouseDetail));
    }

    /**
     * 修改入库单明细
     * @param enterWarehouseDetail 入库单明细
     * @return R
     */
    @SysLog("修改入库单明细" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_enterwarehousedetail_edit')" )
    public R updateById(@RequestBody EnterWarehouseDetail enterWarehouseDetail) {
        return new R<>(enterWarehouseDetailService.updateById(enterWarehouseDetail));
    }

    /**
     * 通过id删除入库单明细
     * @param id id
     * @return R
     */
    @SysLog("删除入库单明细" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_enterwarehousedetail_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(enterWarehouseDetailService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除入库单明细")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_enterwarehousedetail_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(enterWarehouseDetailService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询入库单明细")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(enterWarehouseDetailService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param enterWarehouseDetailReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询入库单明细总记录}")
    @PostMapping("/countByEnterWarehouseDetailReq")
    public R countByEnterWarehouseDetailReq(@RequestBody EnterWarehouseDetailReq enterWarehouseDetailReq) {

        return new R<>(enterWarehouseDetailService.count(Wrappers.query(enterWarehouseDetailReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param enterWarehouseDetailReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据enterWarehouseDetailReq查询一条货位信息表")
    @PostMapping("/getOneByEnterWarehouseDetailReq")
    public R getOneByEnterWarehouseDetailReq(@RequestBody EnterWarehouseDetailReq enterWarehouseDetailReq) {

        return new R<>(enterWarehouseDetailService.getOne(Wrappers.query(enterWarehouseDetailReq), false));
    }


    /**
     * 批量修改OR插入
     * @param enterWarehouseDetailList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_enterwarehousedetail_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<EnterWarehouseDetail> enterWarehouseDetailList) {

        return new R<>(enterWarehouseDetailService.saveOrUpdateBatch(enterWarehouseDetailList));
    }


}
