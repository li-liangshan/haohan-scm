/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.wms.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.wms.entity.ShelfManagement;
import com.haohan.cloud.scm.wms.mapper.ShelfManagementMapper;
import com.haohan.cloud.scm.wms.service.ShelfManagementService;
import org.springframework.stereotype.Service;

/**
 * 货品上下架记录
 *
 * @author haohan
 * @date 2019-05-13 21:29:43
 */
@Service
public class ShelfManagementServiceImpl extends ServiceImpl<ShelfManagementMapper, ShelfManagement> implements ShelfManagementService {

}
