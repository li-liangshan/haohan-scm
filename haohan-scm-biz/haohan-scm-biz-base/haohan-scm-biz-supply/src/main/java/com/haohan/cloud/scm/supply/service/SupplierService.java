/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.supply.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.supply.dto.SupplySqlDTO;
import com.haohan.cloud.scm.api.supply.entity.Supplier;

import java.util.Set;

/**
 * 供应商
 *
 * @author haohan
 * @date 2019-05-13 17:44:16
 */
public interface SupplierService extends IService<Supplier> {

    Set<String> findSupplyMerchantList(SupplySqlDTO query);

    Supplier fetchByTelephone(String telephone);

    Supplier fetchByName(String supplierName);
}
