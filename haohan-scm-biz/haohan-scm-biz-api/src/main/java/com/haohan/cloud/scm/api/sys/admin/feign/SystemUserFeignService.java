package com.haohan.cloud.scm.api.sys.admin.feign;

import com.pig4cloud.pigx.admin.api.dto.UserDTO;
import com.pig4cloud.pigx.admin.api.dto.UserInfo;
import com.pig4cloud.pigx.admin.api.entity.SysUser;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.constant.ServiceNameConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @program: haohan-fresh-scm
 * @description: 系统用户内部接口服务
 * @author: Simon
 * @create: 2019-06-05
 **/
@FeignClient(contextId = "systemUserFeignService", value = ServiceNameConstants.UPMS_SERVICE)
public interface SystemUserFeignService {
    /**
     * 通过用户名查询用户、角色信息
     *
     * @param username 用户名
     * @param from     调用标志
     * @return R
     */
    @GetMapping("/user/info/{username}")
    R<UserInfo> info(@PathVariable("username") String username
            , @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 通过社交账号或手机号查询用户、角色信息
     *
     * @param inStr appid@code
     * @param from  调用标志
     * @return
     */
    @GetMapping("/social/info/{inStr}")
    R<UserInfo> social(@PathVariable("inStr") String inStr
            , @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 查询上级部门的用户信息
     *
     * @param username 用户名
     * @return R
     */
    @GetMapping("/user/ancestor/{username}")
    R<List<SysUser>> ancestorUsers(@PathVariable("username") String username);

    /**
     * 通过ID 查询用户全部信息
     *
     * @param id
     * @param from
     * @return 用户信息
     */
    @GetMapping("/api/feign/user/info/{id}")
    R<UserInfo> infoById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 更新用户信息
     * 修改所有信息，修改用户角色
     *
     * @param userDto id
     * @param from
     * @return
     */
    @PostMapping("/api/feign/user/update")
    R<Boolean> updateUser(@RequestBody UserDTO userDto, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 用户密码修改
     * 验证原密码 修改密码、手机、头像
     *
     * @param userDto id必须  password /newpassword1/ username/ lockFlag/deptId/phone/avatar
     * @param from
     * @return
     */
    @PostMapping("/api/feign/user/edit")
    R<Boolean> updateUserInfoById(@RequestBody UserDTO userDto, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 通过phone查询用户id
     *
     * @param phone
     * @param from
     * @return 系统用户id
     */
    @GetMapping("/api/feign/user/queryUserId/{phone}")
    R<String> queryUserId(@PathVariable("phone") String phone, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 添加用户
     *
     * @param userDto
     * @param from
     * @return
     */
    @PostMapping("/api/feign/user/addUser")
    R<UserDTO> addUser(@RequestBody UserDTO userDto, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 删除用户
     *
     * @param userDto 必须userId
     * @param from
     * @return
     */
    @PostMapping("/api/feign/user/delete")
    R<Boolean> deleteUser(@RequestBody UserDTO userDto, @RequestHeader(SecurityConstants.FROM) String from);


}
