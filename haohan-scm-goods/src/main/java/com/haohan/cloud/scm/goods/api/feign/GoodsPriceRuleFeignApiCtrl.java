/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.goods.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.goods.entity.GoodsPriceRule;
import com.haohan.cloud.scm.api.goods.req.GoodsPriceRuleReq;
import com.haohan.cloud.scm.goods.service.GoodsPriceRuleService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 定价规则/市场价/销售价/VIP价格
 *
 * @author haohan
 * @date 2019-05-29 14:25:54
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/GoodsPriceRule")
@Api(value = "goodspricerule", tags = "goodspricerule内部接口服务")
public class GoodsPriceRuleFeignApiCtrl {

    private final GoodsPriceRuleService goodsPriceRuleService;


    /**
     * 通过id查询定价规则/市场价/销售价/VIP价格
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(goodsPriceRuleService.getById(id));
    }


    /**
     * 分页查询 定价规则/市场价/销售价/VIP价格 列表信息
     * @param goodsPriceRuleReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchGoodsPriceRulePage")
    public R getGoodsPriceRulePage(@RequestBody GoodsPriceRuleReq goodsPriceRuleReq) {
        Page page = new Page(goodsPriceRuleReq.getPageNo(), goodsPriceRuleReq.getPageSize());
        GoodsPriceRule goodsPriceRule =new GoodsPriceRule();
        BeanUtil.copyProperties(goodsPriceRuleReq, goodsPriceRule);

        return new R<>(goodsPriceRuleService.page(page, Wrappers.query(goodsPriceRule)));
    }


    /**
     * 全量查询 定价规则/市场价/销售价/VIP价格 列表信息
     * @param goodsPriceRuleReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchGoodsPriceRuleList")
    public R getGoodsPriceRuleList(@RequestBody GoodsPriceRuleReq goodsPriceRuleReq) {
        GoodsPriceRule goodsPriceRule =new GoodsPriceRule();
        BeanUtil.copyProperties(goodsPriceRuleReq, goodsPriceRule);

        return new R<>(goodsPriceRuleService.list(Wrappers.query(goodsPriceRule)));
    }


    /**
     * 新增定价规则/市场价/销售价/VIP价格
     * @param goodsPriceRule 定价规则/市场价/销售价/VIP价格
     * @return R
     */
    @Inner
    @SysLog("新增定价规则/市场价/销售价/VIP价格")
    @PostMapping("/add")
    public R save(@RequestBody GoodsPriceRule goodsPriceRule) {
        return new R<>(goodsPriceRuleService.save(goodsPriceRule));
    }

    /**
     * 修改定价规则/市场价/销售价/VIP价格
     * @param goodsPriceRule 定价规则/市场价/销售价/VIP价格
     * @return R
     */
    @Inner
    @SysLog("修改定价规则/市场价/销售价/VIP价格")
    @PostMapping("/update")
    public R updateById(@RequestBody GoodsPriceRule goodsPriceRule) {
        return new R<>(goodsPriceRuleService.updateById(goodsPriceRule));
    }

    /**
     * 通过id删除定价规则/市场价/销售价/VIP价格
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除定价规则/市场价/销售价/VIP价格")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(goodsPriceRuleService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除定价规则/市场价/销售价/VIP价格")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(goodsPriceRuleService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询定价规则/市场价/销售价/VIP价格")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(goodsPriceRuleService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param goodsPriceRuleReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询定价规则/市场价/销售价/VIP价格总记录}")
    @PostMapping("/countByGoodsPriceRuleReq")
    public R countByGoodsPriceRuleReq(@RequestBody GoodsPriceRuleReq goodsPriceRuleReq) {

        return new R<>(goodsPriceRuleService.count(Wrappers.query(goodsPriceRuleReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param goodsPriceRuleReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据goodsPriceRuleReq查询一条货位信息表")
    @PostMapping("/getOneByGoodsPriceRuleReq")
    public R getOneByGoodsPriceRuleReq(@RequestBody GoodsPriceRuleReq goodsPriceRuleReq) {

        return new R<>(goodsPriceRuleService.getOne(Wrappers.query(goodsPriceRuleReq), false));
    }


    /**
     * 批量修改OR插入
     * @param goodsPriceRuleList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<GoodsPriceRule> goodsPriceRuleList) {

        return new R<>(goodsPriceRuleService.saveOrUpdateBatch(goodsPriceRuleList));
    }

}
