/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.supply.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.opc.entity.SupplierPayment;
import com.haohan.cloud.scm.api.opc.req.payment.SupplierPaymentReq;
import com.haohan.cloud.scm.api.supply.req.QueryPaymentDetailReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 供应商货款统计内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "SupplierPaymentFeignService", value = ScmServiceName.SCM_BIZ_SUPPLY)
public interface SupplierPaymentFeignService {


//    /**
//     * 通过id查询供应商货款统计
//     * @param id id
//     * @return R
//     */
//    @RequestMapping(value = "/api/feign/SupplierPayment/{id}", method = RequestMethod.GET)
//    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 供应商货款统计 列表信息
     * @param supplierPaymentReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/SupplierPayment/fetchSupplierPaymentPage")
    R getSupplierPaymentPage(@RequestBody SupplierPaymentReq supplierPaymentReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 供应商货款统计 列表信息
     * @param supplierPaymentReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/SupplierPayment/fetchSupplierPaymentList")
    R getSupplierPaymentList(@RequestBody SupplierPaymentReq supplierPaymentReq, @RequestHeader(SecurityConstants.FROM) String from);

//    /**
//     * 新增供应商货款统计
//     * @param supplierPayment 供应商货款统计
//     * @return R
//     */
//    @PostMapping("/api/feign/SupplierPayment/add")
//    R save(@RequestBody SupplierPayment supplierPayment, @RequestHeader(SecurityConstants.FROM) String from);


//    /**
//     * 修改供应商货款统计
//     * @param supplierPayment 供应商货款统计
//     * @return R
//     */
//    @PostMapping("/api/feign/SupplierPayment/update")
//    R updateById(@RequestBody SupplierPayment supplierPayment, @RequestHeader(SecurityConstants.FROM) String from);


//    /**
//     * 通过id删除供应商货款统计
//     * @param id id
//     * @return R
//     */
//    @PostMapping("/api/feign/SupplierPayment/delete/{id}")
//    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

//    /**
//   * 删除（根据ID 批量删除)
//   *
//   * @param idList 主键ID列表
//   * @return R
//   */
//    @PostMapping("/api/feign/SupplierPayment/batchDelete")
//    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);

//
//    /**
//     * 批量查询（根据IDS）
//     *
//     * @param idList 主键ID列表
//     * @return R
//     */
//    @PostMapping("/api/feign/SupplierPayment/listByIds")
//    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


//    /**
//     * 根据 Wrapper 条件，查询总记录数
//     *
//     * @param supplierPaymentReq 实体对象,可以为空
//     * @return R
//     */
//    @PostMapping("/api/feign/SupplierPayment/countBySupplierPaymentReq")
//    R countBySupplierPaymentReq(@RequestBody SupplierPaymentReq supplierPaymentReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param supplierPaymentReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/SupplierPayment/getOneBySupplierPaymentReq")
    R getOneBySupplierPaymentReq(@RequestBody SupplierPaymentReq supplierPaymentReq, @RequestHeader(SecurityConstants.FROM) String from);

//
//    /**
//     * 批量修改OR插入
//     *
//     * @param supplierPaymentList 实体对象集合 大小不超过1000条数据
//     * @return R
//     */
//    @PostMapping("/api/feign/SupplierPayment/saveOrUpdateBatch")
//    R saveOrUpdateBatch(@RequestBody List<SupplierPayment> supplierPaymentList, @RequestHeader(SecurityConstants.FROM) String from);

//    /**
//     * 查询供应商货款详情
//     * @param req
//     * @param from
//     * @return
//     */
//    @PostMapping("/api/feign/SupplierPayment/queryPayment")
//    R querySupplierPayment(@Validated QueryPaymentDetailReq req,@RequestHeader(SecurityConstants.FROM)String from);

//    /**
//     * 查询应付账单 是否为确认状态,及对应金额
//     * @param supplierPayment 必需 pmId / supplierPaymentId
//     * @return 不为确认状态时 返回null
//     */
//    @PostMapping("/api/feign/SupplierPayment/checkPayment")
//    R<SupplierPayment> checkPayment(SupplierPayment supplierPayment,@RequestHeader(SecurityConstants.FROM)String from);
}
