package com.haohan.cloud.scm.api.constant.enums.market;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xwx
 * @date 2019/5/30
 */
@Getter
@AllArgsConstructor
public enum SalesStageEnum {
    /**
     * 销售阶段1.初期沟通2.立项评估3.商务谈判4.谈成结束5.未成结束
     */
    preliminaryStage("1","初期沟通"),
    projectEvaluation("2","立项评估"),
    businessNegotiation("3","商务谈判"),
    negotiated("4","谈成结束"),
    notInto("5","未成结束");

    private static final Map<String, SalesStageEnum> MAP = new HashMap<>(8);

    static {
        for (SalesStageEnum e : SalesStageEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static SalesStageEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

    /**
     * 类型
     */
    @EnumValue
    @JsonValue
    private String type;
    /**
     * 描述
     */

    private String desc;
}
