/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.manage.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.manage.entity.OpenplatformManage;
import com.haohan.cloud.scm.api.manage.req.OpenplatformManageReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 开放平台应用资料管理内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "OpenplatformManageFeignService", value = ScmServiceName.SCM_MANAGE)
public interface OpenplatformManageFeignService {


    /**
     * 通过id查询开放平台应用资料管理
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/OpenplatformManage/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 开放平台应用资料管理 列表信息
     * @param openplatformManageReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/OpenplatformManage/fetchOpenplatformManagePage")
    R getOpenplatformManagePage(@RequestBody OpenplatformManageReq openplatformManageReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 开放平台应用资料管理 列表信息
     * @param openplatformManageReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/OpenplatformManage/fetchOpenplatformManageList")
    R getOpenplatformManageList(@RequestBody OpenplatformManageReq openplatformManageReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增开放平台应用资料管理
     * @param openplatformManage 开放平台应用资料管理
     * @return R
     */
    @PostMapping("/api/feign/OpenplatformManage/add")
    R save(@RequestBody OpenplatformManage openplatformManage, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改开放平台应用资料管理
     * @param openplatformManage 开放平台应用资料管理
     * @return R
     */
    @PostMapping("/api/feign/OpenplatformManage/update")
    R updateById(@RequestBody OpenplatformManage openplatformManage, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除开放平台应用资料管理
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/OpenplatformManage/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/OpenplatformManage/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/OpenplatformManage/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param openplatformManageReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/OpenplatformManage/countByOpenplatformManageReq")
    R countByOpenplatformManageReq(@RequestBody OpenplatformManageReq openplatformManageReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param openplatformManageReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/OpenplatformManage/getOneByOpenplatformManageReq")
    R getOneByOpenplatformManageReq(@RequestBody OpenplatformManageReq openplatformManageReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param openplatformManageList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/OpenplatformManage/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<OpenplatformManage> openplatformManageList, @RequestHeader(SecurityConstants.FROM) String from);


}
