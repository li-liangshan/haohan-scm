/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.manage.req;

import com.haohan.cloud.scm.api.manage.entity.BusinessNote;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 商务留言
 *
 * @author haohan
 * @date 2019-05-28 20:34:24
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "商务留言")
public class BusinessNoteReq extends BusinessNote {

    private long pageSize;
    private long pageNo;




}
