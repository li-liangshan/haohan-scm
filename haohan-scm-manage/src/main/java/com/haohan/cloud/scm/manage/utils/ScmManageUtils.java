package com.haohan.cloud.scm.manage.utils;

import com.haohan.cloud.scm.api.bill.feign.SettlementFeignService;
import com.haohan.cloud.scm.api.bill.req.SettlementFeignReq;
import com.haohan.cloud.scm.api.crm.feign.CustomerMerchantRelationFeignService;
import com.haohan.cloud.scm.api.crm.req.CustomerMerchantRelationReq;
import com.haohan.cloud.scm.api.goods.feign.GoodsFeignService;
import com.haohan.cloud.scm.api.goods.req.GoodsReq;
import com.haohan.cloud.scm.api.opc.feign.ShipRecordFeignService;
import com.haohan.cloud.scm.api.opc.req.ShipRecordReq;
import com.haohan.cloud.scm.api.saleb.feign.BuyerFeignService;
import com.haohan.cloud.scm.api.saleb.req.BuyerReq;
import com.haohan.cloud.scm.api.supply.feign.SupplierFeignService;
import com.haohan.cloud.scm.api.supply.req.SupplierReq;
import com.haohan.cloud.scm.api.sys.admin.feign.SystemDictFeignService;
import com.haohan.cloud.scm.api.sys.admin.feign.SystemUserFeignService;
import com.haohan.cloud.scm.common.tools.exception.ErrorDataException;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.pig4cloud.pigx.admin.api.entity.SysDictItem;
import com.pig4cloud.pigx.admin.api.entity.SysTenant;
import com.pig4cloud.pigx.admin.api.feign.RemoteTenantService;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author dy
 * @date 2019/7/24
 */
@Component
@AllArgsConstructor
public class ScmManageUtils {

    private final SystemUserFeignService systemUserFeignService;
    private final SystemDictFeignService systemDictFeignService;
    private final RemoteTenantService remoteTenantService;
    private final CustomerMerchantRelationFeignService customerMerchantRelationFeignService;
    private final BuyerFeignService buyerFeignService;
    private final SupplierFeignService supplierFeignService;
    private final GoodsFeignService goodsFeignService;
    private final ShipRecordFeignService shipRecordFeignService;
    private final SettlementFeignService settlementFeignService;

    /**
     * 查询全部有效租户
     *
     * @return
     */
    public List<SysTenant> fetchTenantList() {
        R<List<SysTenant>> resp = remoteTenantService.list(SecurityConstants.FROM_IN);
        if (!RUtil.isSuccess(resp) || null == resp.getData()) {
            throw new ErrorDataException("找不到有效租户");
        }
        return resp.getData();
    }

    /**
     * 查询 字典项
     *
     * @param type
     * @return
     */
    public List<SysDictItem> fetchDictByType(String type) {
        R<List<SysDictItem>> resp = systemDictFeignService.fetchDictByType(type, SecurityConstants.FROM_IN);
        if (!RUtil.isSuccess(resp) || null == resp.getData()) {
            throw new ErrorDataException("找不到字典项");
        }
        return resp.getData();
    }

    /**
     * 查询 用户id
     *
     * @param telephone
     * @return
     */
    public String fetchUserId(String telephone) {
        R<String> resp = systemUserFeignService.queryUserId(telephone, SecurityConstants.FROM_IN);
        if (!RUtil.isSuccess(resp) || null == resp.getData()) {
            throw new ErrorDataException("找不到用户");
        }
        return resp.getData();
    }

    public int countCustomerNum(String merchantId) {
        CustomerMerchantRelationReq req = new CustomerMerchantRelationReq();
        req.setMerchantId(merchantId);
        R<Integer> r = customerMerchantRelationFeignService.countByCustomerMerchantRelationReq(req, SecurityConstants.FROM_IN);
        if (RUtil.isFailed(r) || null == r.getData()) {
            throw new ErrorDataException("查询关联客户数失败");
        }
        return r.getData();
    }

    public void updateCustomerMerchant(String merchantId, String merchantName) {
        CustomerMerchantRelationReq req = new CustomerMerchantRelationReq();
        req.setMerchantId(merchantId);
        req.setMerchantName(merchantName);
        R<Boolean> r = customerMerchantRelationFeignService.updateMerchantName(req, SecurityConstants.FROM_IN);
        if (RUtil.isFailed(r) || null == r.getData()) {
            throw new ErrorDataException("修改客户相关商家时失败");
        }
    }

    public int countBuyerNum(String merchantId) {
        BuyerReq req = new BuyerReq();
        req.setMerchantId(merchantId);
        R<Integer> r = buyerFeignService.countByBuyerReq(req, SecurityConstants.FROM_IN);
        if (RUtil.isFailed(r) || null == r.getData()) {
            throw new ErrorDataException("查询关联客户数失败");
        }
        return r.getData();
    }

    public void updateBuyerMerchant(String merchantId, String merchantName) {
        BuyerReq req = new BuyerReq();
        req.setMerchantId(merchantId);
        req.setMerchantName(merchantName);
        R<Boolean> r = buyerFeignService.updateMerchantName(req, SecurityConstants.FROM_IN);
        if (RUtil.isFailed(r)) {
            throw new ErrorDataException("采购商商家名称修改时失败:" + r.getMsg());
        }

    }

    public int countSupplierNum(String merchantId) {
        SupplierReq req = new SupplierReq();
        req.setMerchantId(merchantId);
        R<Integer> r = supplierFeignService.countBySupplierReq(req, SecurityConstants.FROM_IN);
        if (RUtil.isFailed(r) || null == r.getData()) {
            throw new ErrorDataException("查询关联客户数失败");
        }
        return r.getData();
    }

    public void updateSupplierMerchant(String merchantId, String merchantName) {
        SupplierReq req = new SupplierReq();
        req.setMerchantId(merchantId);
        req.setMerchantName(merchantName);
        R<Boolean> r = supplierFeignService.updateMerchantName(req, SecurityConstants.FROM_IN);
        if (RUtil.isFailed(r)) {
            throw new ErrorDataException("供应商商家名称修改时失败:" + r.getMsg());
        }
    }

    public int countGoodsNum(String id) {
        GoodsReq req = new GoodsReq();
        req.setShopId(id);
        R<Integer> r = goodsFeignService.countByGoodsReq(req, SecurityConstants.FROM_IN);
        if (RUtil.isFailed(r) || null == r.getData()) {
            throw new ErrorDataException("查询关联商品数失败");
        }
        return r.getData();
    }

    public void updateShipRecordMerchant(String merchantId, String merchantName) {
        ShipRecordReq req = new ShipRecordReq();
        req.setPmId(merchantId);
        req.setPmName(merchantName);
        R<Boolean> r = shipRecordFeignService.updateMerchantName(req, SecurityConstants.FROM_IN);
        if (RUtil.isFailed(r)) {
            throw new ErrorDataException("发货记录商家名称修改时失败:" + r.getMsg());
        }
    }

    public void updateBillMerchant(String merchantId, String merchantName) {
        SettlementFeignReq req = new SettlementFeignReq();
        req.setCompanyId(merchantId);
        req.setCompanyName(merchantName);
        R<Boolean> r = settlementFeignService.updateMerchantName(req, SecurityConstants.FROM_IN);
        if (RUtil.isFailed(r)) {
            throw new ErrorDataException("账单相关商家名称修改时失败:" + r.getMsg());
        }
    }

}
