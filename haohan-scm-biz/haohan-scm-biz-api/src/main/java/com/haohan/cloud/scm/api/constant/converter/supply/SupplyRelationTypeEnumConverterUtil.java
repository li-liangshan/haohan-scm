package com.haohan.cloud.scm.api.constant.converter.supply;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.supply.SupplyTypeEnum;

/**
 * @author dy
 * @date 2020/4/17
 */
public class SupplyRelationTypeEnumConverterUtil implements Converter<SupplyTypeEnum> {
    @Override
    public SupplyTypeEnum convert(Object obj, SupplyTypeEnum supplyTypeEnum) throws IllegalArgumentException {
        return null == obj ? null : SupplyTypeEnum.getByType(obj.toString());
    }
}