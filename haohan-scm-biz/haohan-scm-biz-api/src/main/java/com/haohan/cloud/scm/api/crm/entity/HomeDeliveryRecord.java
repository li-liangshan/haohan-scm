/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.crm.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.haohan.cloud.scm.api.constant.enums.crm.DeliveryGoodsStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.crm.DeliveryGoodsTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.crm.MemberTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.crm.PaymentTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 送货上门服务记录表
 *
 * @author haohan
 * @date 2019-12-10 18:11:09
 */
@Data
@TableName("crm_home_delivery_record")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "送货上门服务记录表")
public class HomeDeliveryRecord extends Model<HomeDeliveryRecord> {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 客户编码
     */
    @ApiModelProperty(value = "客户编码")
    private String customerSn;
    /**
     * 客户名称
     */
    @ApiModelProperty(value = "客户名称")
    private String customerName;
    /**
     * 联系人id
     */
    @ApiModelProperty(value = "联系人id")
    private String linkmanId;
    /**
     * 联系人名称
     */
    @ApiModelProperty(value = "联系人名称")
    private String linkmanName;
    /**
     * 配件类型 1普通件 2急件
     */
    @ApiModelProperty(value = "配件类型 1普通件 2急件")
    private DeliveryGoodsTypeEnum goodsType;
    /**
     * 件数
     */
    @ApiModelProperty(value = "件数")
    private Integer goodsNum;
    /**
     * 重量 , 单位kg
     */
    @ApiModelProperty(value = "重量 , 单位kg")
    private BigDecimal weight;
    /**
     * 预约配送时间
     */
    @ApiModelProperty(value = "预约配送时间")
    private LocalDateTime appointmentTime;
    /**
     * 配送时间
     */
    @ApiModelProperty(value = "配送时间")
    private LocalDateTime deliveryTime;
    /**
     * 配送物品 （自定义标签）
     */
    @ApiModelProperty(value = "配送物品 （自定义标签）")
    private String goodsTags;
    /**
     * 配送状态 1未送，2已送，3异常
     */
    @ApiModelProperty(value = "配送状态 1未送，2已送，3异常")
    private DeliveryGoodsStatusEnum deliveryStatus;
    /**
     * 会员类型 1普通，2年卡
     */
    @ApiModelProperty(value = "会员类型（1普通，2年卡）")
    private MemberTypeEnum memberType;
    /**
     * 付款方式 1.现金，2.微信转账，3.线上支付
     */
    @ApiModelProperty(value = "付款方式 1.现金，2.微信转账，3.线上支付")
    private PaymentTypeEnum paymentType;
    /**
     * 付款金额（数字, 单位元）
     */
    @ApiModelProperty(value = "付款金额（数字, 单位元）")
    private BigDecimal amount;
    /**
     * 其他服务 （自定义标签）
     */
    @ApiModelProperty(value = "其他服务 （自定义标签）")
    private String otherServices;
    /**
     * 补充说明
     */
    @ApiModelProperty(value = "补充说明")
    private String description;
    /**
     * 订单号
     */
    @ApiModelProperty(value = "订单号")
    private String orderSn;
    /**
     * 订单类型
     */
    @ApiModelProperty(value = "订单类型")
    private String orderTye;
    /**
     * 第三方订单号
     */
    @ApiModelProperty(value = "第三方订单号")
    private String thirdOrderSn;
    /**
     * 创建者
     */
    @ApiModelProperty(value = "创建者")
    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
     * 更新者
     */
    @ApiModelProperty(value = "更新者")
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.UPDATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateDate;
    /**
     * 备注信息
     */
    @ApiModelProperty(value = "备注信息")
    private String remarks;
    /**
     * 删除标记
     */
    @ApiModelProperty(value = "删除标记")
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;
    /**
     * 租户id
     */
    @ApiModelProperty(value = "租户id")
    private Integer tenantId;

}
