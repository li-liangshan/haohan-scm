package com.haohan.cloud.scm.common.tools.http;

/**
 * Created by zgw on 2019/5/22.
 */
public enum  MethodType {
  POST,DELETE,PUT,GET,JSON
}
