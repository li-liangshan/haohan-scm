package com.haohan.cloud.scm.goods.api.ctrl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.goods.dto.GoodsCategoryTree;
import com.haohan.cloud.scm.api.goods.dto.GoodsExtDTO;
import com.haohan.cloud.scm.api.goods.dto.GoodsModelDTO;
import com.haohan.cloud.scm.api.goods.entity.Goods;
import com.haohan.cloud.scm.api.goods.req.manage.*;
import com.haohan.cloud.scm.api.goods.vo.GoodsModelVO;
import com.haohan.cloud.scm.api.goods.vo.GoodsVO;
import com.haohan.cloud.scm.common.tools.constant.FirstGroup;
import com.haohan.cloud.scm.common.tools.constant.SecondGroup;
import com.haohan.cloud.scm.common.tools.constant.ThreeGroup;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.haohan.cloud.scm.goods.core.GoodsCoreService;
import com.haohan.cloud.scm.goods.core.GoodsModelCoreService;
import com.haohan.cloud.scm.goods.service.GoodsCategoryService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author dy
 * @date 2019/11/2
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/goods/manage")
@Api(value = "ApiGoodsManage", tags = "GoodsManage商品管理")
public class GoodsInfoManageApiCtrl {

    private final GoodsModelCoreService goodsModelCoreService;
    private final GoodsCoreService goodsCoreService;
    private final GoodsCategoryService goodsCategoryService;

    @GetMapping("/fetchList")
    @ApiOperation(value = "查询商品列表")
    public R<IPage<GoodsExtDTO>> fetchPage(Page<Goods> page, @Validated GoodsListReq req) {
        return RUtil.success(goodsCoreService.findExtPage(page, req.transTo()));
    }

    @GetMapping("/fetchInfoPage")
    @ApiOperation(value = "查询商品列表,带详情")
    public R<IPage<GoodsVO>> fetchPageGoodsVO(Page<Goods> page, @Validated GoodsListReq req) {
        int maxSize = 50;
        if (page.getSize() > maxSize) {
            page.setSize(maxSize);
        }
        return RUtil.success(goodsCoreService.findInfoPage(page, req));
    }

    @GetMapping("/fetchInfo")
    @ApiOperation(value = "查询商品详情")
    public R<GoodsVO> fetchInfo(@Valid GoodsFetchInfoReq req) {
        if (StrUtil.isNotBlank(req.getGoodsId())) {
            return RUtil.success(goodsCoreService.fetchGoodsInfoById(req.getGoodsId(), req.transTo()));
        } else if (StrUtil.isBlank(req.getGoodsSn())) {
            return R.failed("缺少参数goodsId或goodsSn");
        }
        return RUtil.success(goodsCoreService.fetchInfo(req.getGoodsSn(), req.transTo()));
    }

    /**
     * app使用中
     *
     * @param page
     * @param req
     * @return
     */
    @GetMapping("/fetchModelList")
    @ApiOperation(value = "查询商品规格列表")
    public R<IPage<GoodsModelDTO>> fetchModelPage(Page<GoodsModelDTO> page, @Valid FetchModelListReq req) {
        return RUtil.success(goodsModelCoreService.fetchModelPage(page, req));
    }

    /**
     * 后台使用中
     *
     * @param page
     * @param req
     * @return
     */
    @GetMapping("/fetchModelInfoPage")
    @ApiOperation(value = "查询商品规格列表(带类型名称)")
    public R<IPage<GoodsModelVO>> fetchModelInfoPage(Page page, @Valid FetchModelListReq req) {
        return RUtil.success(goodsModelCoreService.fetchModelInfoPage(page, req));
    }

    @GetMapping(value = "/categoryTree")
    @ApiOperation(value = "查询分类树", notes = "查询分类树, 默认不查询全部")
    public R<List<GoodsCategoryTree>> queryCategoryTree(String shopId, boolean allShow) {
        if (StrUtil.isEmpty(shopId)) {
            return R.failed("店铺id不能为空");
        }
        return RUtil.success(goodsCategoryService.selectTree(shopId, allShow));
    }

    /**
     * 新增商品
     *
     * @param req 商品
     * @return R
     */
    @SysLog("新增商品")
    @PostMapping("/info")
    @PreAuthorize("@pms.hasPermission('scm_goods_add')")
    public R<Boolean> addGoods(@RequestBody @Valid EditGoodsReq req) {
        req.setGoodsId(null);
        return RUtil.success(goodsCoreService.addGoods(req));
    }

    /**
     * 修改商品
     *
     * @param req 商品
     * @return R
     */
    @SysLog("修改商品")
    @PutMapping("/info")
    @PreAuthorize("@pms.hasPermission('scm_goods_edit')")
    public R<Boolean> modifyGoods(@RequestBody @Valid EditGoodsReq req) {
        if (StrUtil.isEmpty(req.getGoodsId())) {
            return R.failed("goodsId不能为空");
        }
        return RUtil.success(goodsCoreService.modifyGoods(req));
    }

    /**
     * 通过id删除商品
     *
     * @param goodsId id
     * @return R
     */
    @SysLog("删除商品")
    @DeleteMapping("/{goodsId}")
    @PreAuthorize("@pms.hasPermission('scm_goods_del')")
    public R<Boolean> deleteGoods(@PathVariable String goodsId) {
        return RUtil.success(goodsCoreService.deleteGoods(goodsId));
    }

    @SysLog("商品上下架修改")
    @PutMapping("/marketable")
    @PreAuthorize("@pms.hasPermission('scm_goods_edit')")
    public R<Boolean> changeMarketable(@Validated(FirstGroup.class) GoodsChangeReq req) {
        return RUtil.success(goodsCoreService.changeMarketable(req.getGoodsId(), req.getMarketable()));
    }

    @SysLog("商品排序(权重)修改")
    @PutMapping("/sort")
    @PreAuthorize("@pms.hasPermission('scm_goods_edit')")
    public R<Boolean> changeSort(@Validated(SecondGroup.class) GoodsChangeReq req) {
        return RUtil.success(goodsCoreService.changeSort(req.getGoodsId(), req.getSort()));
    }

    @SysLog("商品库存修改")
    @PutMapping("/storage")
    @PreAuthorize("@pms.hasPermission('scm_goods_edit')")
    public R<Boolean> changeStorage(@Validated(ThreeGroup.class) GoodsChangeReq req) {
        return RUtil.success(goodsCoreService.changeStorage(req.getGoodsId(), req.getStorage()));
    }

    @SysLog("商品规格库存修改")
    @PutMapping("/model/storage")
    @PreAuthorize("@pms.hasPermission('scm_goods_edit')")
    public R<Boolean> changeModelStorage(@Validated GoodsModelChangeReq req) {
        return RUtil.success(goodsModelCoreService.changeStorage(req.getModelId(), req.getStorage()));
    }

}
