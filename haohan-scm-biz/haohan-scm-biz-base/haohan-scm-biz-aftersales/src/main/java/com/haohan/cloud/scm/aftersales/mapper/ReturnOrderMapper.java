/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.aftersales.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haohan.cloud.scm.api.aftersales.entity.ReturnOrder;

/**
 * 退货单
 *
 * @author haohan
 * @date 2019-07-30 16:09:37
 */
public interface ReturnOrderMapper extends BaseMapper<ReturnOrder> {

}
