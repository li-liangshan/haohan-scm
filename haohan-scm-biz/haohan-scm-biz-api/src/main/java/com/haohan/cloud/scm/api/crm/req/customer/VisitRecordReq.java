package com.haohan.cloud.scm.api.crm.req.customer;

import com.haohan.cloud.scm.api.constant.enums.crm.VisitStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.crm.VisitStepEnum;
import com.haohan.cloud.scm.api.constant.enums.market.CustomerStatusEnum;
import com.haohan.cloud.scm.api.crm.entity.CustomerVisit;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @author dy
 * @date 2019/10/29
 */
@Data
public class VisitRecordReq {

    @NotBlank(message = "客户拜访记录id不能为空")
    @Length(max = 32, message = "客户拜访记录id长度最大32字符")
    @ApiModelProperty(value = "主键")
    private String id;

    @ApiModelProperty(value = "进展阶段:1初次拜访2了解交流3.深入跟进4达成合作5商务往来")
    private VisitStepEnum visitStep;

    @ApiModelProperty(value = "客户状态:0.无1潜在2.有意向3成交4失败")
    private CustomerStatusEnum customerStatus;

    @ApiModelProperty(value = "拜访状态: 1.待拜访 2.拜访中 3.已拜访 4.未完成", notes = "只修改状态为 已拜访、未完成")
    private VisitStatusEnum visitStatus;

    @Length(max = 255, message = "拜访内容长度最大255字符")
    @ApiModelProperty(value = "拜访内容")
    private String visitContent;

    @Length(max = 255, message = "拜访总结长度最大255字符")
    @ApiModelProperty(value = "拜访总结")
    private String summaryContent;

    @ApiModelProperty(value = "拜访图片列表")
    private List<String> photoList;

    public CustomerVisit transTo() {
        CustomerVisit visit = new CustomerVisit();
        visit.setId(this.id);
        visit.setVisitStep(this.visitStep);
        visit.setCustomerStatus(this.customerStatus);
        if (this.visitStatus == VisitStatusEnum.finish || this.visitStatus == VisitStatusEnum.failed) {
            visit.setVisitStatus(this.visitStatus);
        }
        visit.setSummaryContent(this.summaryContent);
        visit.setVisitContent(this.visitContent);
        return visit;
    }

}
