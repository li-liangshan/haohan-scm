/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.goods.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.goods.entity.GoodsPromotion;
import com.haohan.cloud.scm.api.goods.req.GoodsPromotionReq;
import com.haohan.cloud.scm.goods.service.GoodsPromotionService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 商品促销库
 *
 * @author haohan
 * @date 2019-08-30 11:45:09
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/GoodsPromotion")
@Api(value = "goodspromotion", tags = "goodspromotion-商品促销库内部接口服务}")
public class GoodsPromotionFeignApiCtrl {

    private final GoodsPromotionService goodsPromotionService;


    /**
     * 通过id查询商品促销库
     *
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    @ApiOperation(value = "通过id查询商品促销库")
    public R getById(@PathVariable("id") String id) {
        return new R<>(goodsPromotionService.getById(id));
    }


    /**
     * 分页查询 商品促销库 列表信息
     *
     * @param goodsPromotionReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchGoodsPromotionPage")
    @ApiOperation(value = "分页查询 商品促销库 列表信息")
    public R getGoodsPromotionPage(@RequestBody GoodsPromotionReq goodsPromotionReq) {
        Page page = new Page(goodsPromotionReq.getPageNo(), goodsPromotionReq.getPageSize());
        GoodsPromotion goodsPromotion = new GoodsPromotion();
        BeanUtil.copyProperties(goodsPromotionReq, goodsPromotion);

        return new R<>(goodsPromotionService.page(page, Wrappers.query(goodsPromotion)));
    }


    /**
     * 全量查询 商品促销库 列表信息
     *
     * @param goodsPromotionReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchGoodsPromotionList")
    @ApiOperation(value = "全量查询 商品促销库 列表信息")
    public R getGoodsPromotionList(@RequestBody GoodsPromotionReq goodsPromotionReq) {
        GoodsPromotion goodsPromotion = new GoodsPromotion();
        BeanUtil.copyProperties(goodsPromotionReq, goodsPromotion);

        return new R<>(goodsPromotionService.list(Wrappers.query(goodsPromotion)));
    }


    /**
     * 新增商品促销库
     *
     * @param goodsPromotion 商品促销库
     * @return R
     */
    @Inner
    @SysLog("新增商品促销库")
    @PostMapping("/add")
    @ApiOperation(value = "新增商品促销库")
    public R save(@RequestBody GoodsPromotion goodsPromotion) {
        return new R<>(goodsPromotionService.save(goodsPromotion));
    }

    /**
     * 修改商品促销库
     *
     * @param goodsPromotion 商品促销库
     * @return R
     */
    @Inner
    @SysLog("修改商品促销库")
    @PostMapping("/update")
    @ApiOperation(value = "修改商品促销库")
    public R updateById(@RequestBody GoodsPromotion goodsPromotion) {
        return new R<>(goodsPromotionService.updateById(goodsPromotion));
    }

    /**
     * 通过id删除商品促销库
     *
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除商品促销库")
    @PostMapping("/delete/{id}")
    @ApiOperation(value = "通过id删除商品促销库")
    public R removeById(@PathVariable String id) {
        return new R<>(goodsPromotionService.removeById(id));
    }

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @return R
     */
    @Inner
    @SysLog("批量删除商品促销库")
    @PostMapping("/batchDelete")
    @ApiOperation(value = "通过IDS批量删除商品促销库")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(goodsPromotionService.removeByIds(idList));
    }


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @Inner
    @SysLog("根据IDS批量查询商品促销库")
    @PostMapping("/listByIds")
    @ApiOperation(value = "根据IDS批量查询商品促销库")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(goodsPromotionService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param goodsPromotionReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询商品促销库总记录")
    @PostMapping("/countByGoodsPromotionReq")
    @ApiOperation(value = "查询商品促销库总记录")
    public R countByGoodsPromotionReq(@RequestBody GoodsPromotionReq goodsPromotionReq) {

        return new R<>(goodsPromotionService.count(Wrappers.query(goodsPromotionReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     *
     * @param goodsPromotionReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据goodsPromotionReq查询一条商品促销库信息")
    @PostMapping("/getOneByGoodsPromotionReq")
    @ApiOperation(value = "根据goodsPromotionReq查询一条商品促销库信息")
    public R getOneByGoodsPromotionReq(@RequestBody GoodsPromotionReq goodsPromotionReq) {

        return new R<>(goodsPromotionService.getOne(Wrappers.query(goodsPromotionReq), false));
    }


    /**
     * 批量修改OR插入商品促销库
     *
     * @param goodsPromotionList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入商品促销库数据")
    @PostMapping("/saveOrUpdateBatch")
    @ApiOperation(value = "批量修改OR插入商品促销库数据")
    public R saveOrUpdateBatch(@RequestBody List<GoodsPromotion> goodsPromotionList) {

        return new R<>(goodsPromotionService.saveOrUpdateBatch(goodsPromotionList));
    }

}
