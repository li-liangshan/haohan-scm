package com.haohan.cloud.scm.purchase.core;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.purchase.entity.MarketRecord;
import com.haohan.cloud.scm.api.purchase.req.PurchaseAnswerPurchaseReportReq;
import com.haohan.cloud.scm.api.purchase.req.PurchaseModifyMarketPriceReq;
import com.haohan.cloud.scm.api.purchase.req.PurchaseModifyPurchaseReportReq;

/**
 * @author dy
 * @date 2019/5/17
 */
public interface IScmPurchaseMarketReportService {

     /**
     * 修改市场行情
     *
     * @param req
     * @return
     */
    Boolean modifyMarketPrice(PurchaseModifyMarketPriceReq req);


    /**
     * 修改 采购汇报
     *
     * @param req
     * @return
     */
    Boolean modifyPurchaseReport(PurchaseModifyPurchaseReportReq req);

    /**
     * 回复采购汇报
     *
     * @param req
     * @return
     */
    Boolean answerPurchaseReport(PurchaseAnswerPurchaseReportReq req);

    /**
     * 查询市场行情列表
     * @param page
     * @param marketRecord
     * @return
     */
    IPage queryMarketRecordsList(Page page, MarketRecord marketRecord);
}
