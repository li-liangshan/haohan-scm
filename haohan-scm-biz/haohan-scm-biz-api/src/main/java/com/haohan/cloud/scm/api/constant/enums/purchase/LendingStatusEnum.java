package com.haohan.cloud.scm.api.constant.enums.purchase;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xwx
 * @date 2019/5/30
 */
@Getter
@AllArgsConstructor
public enum LendingStatusEnum implements IBaseEnum {

    /**
     * 请款状态:1.已申请2.通过初审3.待复审4.审核未通过5.待放款6.已放款
     */
    haveApplied("1","已申请"),
    shortList("2","通过初审"),
    recheck("3","待复审"),
    AuditFailed("4","审核未通过"),
    stayLoan("5","待放款"),
    alreadyLoan("6","已放款");

    private static final Map<String, LendingStatusEnum> MAP = new HashMap<>(8);

    static {
        for (LendingStatusEnum e : LendingStatusEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static LendingStatusEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

    /**
     * 类型
     */
    @EnumValue
    @JsonValue
    private String type;
    /**
     * 描述
     */
    private String desc;
}
