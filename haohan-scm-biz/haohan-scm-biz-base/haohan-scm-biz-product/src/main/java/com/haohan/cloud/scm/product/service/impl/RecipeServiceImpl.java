/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.product.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.constant.NumberPrefixConstant;
import com.haohan.cloud.scm.api.product.entity.Recipe;
import com.haohan.cloud.scm.common.tools.util.ScmIncrementUtil;
import com.haohan.cloud.scm.product.mapper.RecipeMapper;
import com.haohan.cloud.scm.product.service.RecipeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

/**
 * 商品加工配方表
 *
 * @author haohan
 * @date 2019-05-13 18:15:51
 */
@Service
public class RecipeServiceImpl extends ServiceImpl<RecipeMapper, Recipe> implements RecipeService {
    @Autowired
    @Lazy
    private ScmIncrementUtil scmIncrementUtil;

    @Override
    public boolean save(Recipe entity) {
        if (StrUtil.isEmpty(entity.getRecipeSn())) {
            String sn = scmIncrementUtil.inrcSnByClass(Recipe.class, NumberPrefixConstant.RECIPE_SN_PRE);
            entity.setRecipeSn(sn);
        }
        return super.save(entity);
    }

}
