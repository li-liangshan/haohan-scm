/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.manage.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.manage.entity.Industry;
import com.haohan.cloud.scm.api.manage.req.IndustryReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 行业类型内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "IndustryFeignService", value = ScmServiceName.SCM_MANAGE)
public interface IndustryFeignService {


    /**
     * 通过id查询行业类型
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/Industry/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 行业类型 列表信息
     * @param industryReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/Industry/fetchIndustryPage")
    R getIndustryPage(@RequestBody IndustryReq industryReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 行业类型 列表信息
     * @param industryReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/Industry/fetchIndustryList")
    R getIndustryList(@RequestBody IndustryReq industryReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增行业类型
     * @param industry 行业类型
     * @return R
     */
    @PostMapping("/api/feign/Industry/add")
    R save(@RequestBody Industry industry, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改行业类型
     * @param industry 行业类型
     * @return R
     */
    @PostMapping("/api/feign/Industry/update")
    R updateById(@RequestBody Industry industry, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除行业类型
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/Industry/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/Industry/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/Industry/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param industryReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/Industry/countByIndustryReq")
    R countByIndustryReq(@RequestBody IndustryReq industryReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param industryReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/Industry/getOneByIndustryReq")
    R getOneByIndustryReq(@RequestBody IndustryReq industryReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param industryList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/Industry/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<Industry> industryList, @RequestHeader(SecurityConstants.FROM) String from);


}
