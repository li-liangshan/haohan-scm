/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.message.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.message.entity.ShortMessageRecord;
import com.haohan.cloud.scm.api.message.req.ShortMessageRecordReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 短信消息记录表内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "ShortMessageRecordFeignService", value = ScmServiceName.SCM_BIZ_MSG)
public interface ShortMessageRecordFeignService {


    /**
     * 通过id查询短信消息记录表
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/ShortMessageRecord/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 短信消息记录表 列表信息
     * @param shortMessageRecordReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/ShortMessageRecord/fetchShortMessageRecordPage")
    R getShortMessageRecordPage(@RequestBody ShortMessageRecordReq shortMessageRecordReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 短信消息记录表 列表信息
     * @param shortMessageRecordReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/ShortMessageRecord/fetchShortMessageRecordList")
    R getShortMessageRecordList(@RequestBody ShortMessageRecordReq shortMessageRecordReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增短信消息记录表
     * @param shortMessageRecord 短信消息记录表
     * @return R
     */
    @PostMapping("/api/feign/ShortMessageRecord/add")
    R save(@RequestBody ShortMessageRecord shortMessageRecord, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改短信消息记录表
     * @param shortMessageRecord 短信消息记录表
     * @return R
     */
    @PostMapping("/api/feign/ShortMessageRecord/update")
    R updateById(@RequestBody ShortMessageRecord shortMessageRecord, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除短信消息记录表
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/ShortMessageRecord/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/ShortMessageRecord/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/ShortMessageRecord/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param shortMessageRecordReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/ShortMessageRecord/countByShortMessageRecordReq")
    R countByShortMessageRecordReq(@RequestBody ShortMessageRecordReq shortMessageRecordReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param shortMessageRecordReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/ShortMessageRecord/getOneByShortMessageRecordReq")
    R getOneByShortMessageRecordReq(@RequestBody ShortMessageRecordReq shortMessageRecordReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param shortMessageRecordList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/ShortMessageRecord/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<ShortMessageRecord> shortMessageRecordList, @RequestHeader(SecurityConstants.FROM) String from);


}
