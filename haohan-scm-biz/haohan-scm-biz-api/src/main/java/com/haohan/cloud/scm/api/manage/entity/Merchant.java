/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.haohan.cloud.scm.api.manage.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.haohan.cloud.scm.api.constant.enums.manage.MerchantPdsTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.manage.MerchantStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

/**
 * 商家信息
 *
 * @author haohan
 * @date 2019-05-13 17:37:44
 */
@Data
@TableName("scm_merchant")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "商家信息")
public class Merchant extends Model<Merchant> {
    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 归属用户
     */
    private String manageUser;
    /**
     * 名称
     */
    private String merchantName;
    /**
     * 区域ID
     */
    private String areaId;
    /**
     * 商家地址
     */
    private String address;
    /**
     * 联系人
     */
    private String contact;
    /**
     * 电话
     */
    private String telephone;
    /**
     * 行业名称
     */
    private String industry;
    /**
     * 规模
     */
    private String scale;
    /**
     * 业务介绍
     */
    private String bizDesc;
    /**
     * 业务专管员
     */
    private String bizUser;
    /**
     * 状态  原系统使用 2启用  0 待审核 -1 停用
     */
    private MerchantStatusEnum status;
    /**
     * 渠道编号
     */
    private String partnerNum;
    /**
     * 商家类型  商家的店铺类型  0:餐饮   1:零售  暂未使用需修改
     */
    private String merchantType;
    /**
     * 产品类型  暂未使用需完善
     */
    private String prodType;
    /**
     * 是否自动分单
     */
    private YesNoEnum isAutomaticOrder;
    /**
     * 采购配送商家类型
     */
    private MerchantPdsTypeEnum pdsType;
    /**
     * 产品线类型  暂未使用需完善
     */
    private String productLine;
    /**
     * 归属商家id
     */
    private String parentId;
    /**
     * 创建者
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
     * 更新者
     */
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateDate;
    /**
     * 备注信息
     */
    private String remarks;
    /**
     * 删除标记
     */
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;
    /**
     * 租户id
     */
    private Integer tenantId;

}
