package com.haohan.cloud.scm.api.purchase.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author dy
 * @date 2019/5/17
 */
@Data
@ApiModel(description = "增加市场行情需求")
public class PurchaseAddMarketPriceReq {

    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "商家id",required = true)
    private String pmId;

//    @NotBlank(message = "uId不能为空")
    @ApiModelProperty(value = "用户ID",required = true)
    private String uId;

    @ApiModelProperty(value = "商品规格id")
    private String goodsModelId;

    @ApiModelProperty(value = "商品图片")
    private String goodsImg;

    @ApiModelProperty(value = "商品名称")
    private String goodsName;

    @ApiModelProperty(value = "规格名称")
    private String modelName;

    @ApiModelProperty(value = "单位")
    private String unit;

    @ApiModelProperty(value = "市场价")
    private BigDecimal marketPrice;

    @ApiModelProperty(value = "记录时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime recordTime;

    @ApiModelProperty(value = "发起人id")
    private String initiatorId;

    @ApiModelProperty(value = "供应商id")
    private String supplierId;

    @ApiModelProperty(value = "供应商姓名")
    private String supplierName;

    @ApiModelProperty(value = "备注信息")
    private String remarks;

    @ApiModelProperty(value = "商品分类id")
    private String goodsCategoryId;

}
