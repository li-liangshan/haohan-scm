package com.haohan.cloud.scm.wms.core;

import com.haohan.cloud.scm.api.wms.entity.WarehouseAllot;
import com.haohan.cloud.scm.api.wms.req.AddWarehouseAllotReq;
import com.haohan.cloud.scm.api.wms.req.EditWarehouseAllotReq;
import com.haohan.cloud.scm.api.wms.resp.WarehouseAllotDetailResp;

/**
 * @author cx
 * @date 2019/8/10
 */
public interface IScmWarehouseAllotService {

    /**
     * 查看调拨详情
     * @param allot
     * @return
     */
    WarehouseAllotDetailResp queryWarehouseAllotDetails(WarehouseAllot allot);

    /**
     * 新增调拨记录
     * @param req
     * @return
     */
    Boolean addWarehouseAllot(AddWarehouseAllotReq req);

    /**
     * 编辑调拨记录
     * @param req
     * @return
     */
    Boolean editWarehouseAllot(EditWarehouseAllotReq req);
}
