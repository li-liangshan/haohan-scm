/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.purchase.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.constant.NumberPrefixConstant;
import com.haohan.cloud.scm.api.purchase.entity.LendingRecord;
import com.haohan.cloud.scm.common.tools.util.ScmIncrementUtil;
import com.haohan.cloud.scm.purchase.mapper.LendingRecordMapper;
import com.haohan.cloud.scm.purchase.service.LendingRecordService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * 放款申请记录
 *
 * @author haohan
 * @date 2019-05-13 18:48:27
 */
@Service
@AllArgsConstructor
public class LendingRecordServiceImpl extends ServiceImpl<LendingRecordMapper, LendingRecord> implements LendingRecordService {

    private final ScmIncrementUtil scmIncrementUtil;

    @Override
    public boolean save(LendingRecord entity) {
        if (StrUtil.isEmpty(entity.getLendingSn())) {
            String sn = scmIncrementUtil.inrcSnByClass(LendingRecord.class, NumberPrefixConstant.LENDING_RECORD_SN_PRE);
            entity.setLendingSn(sn);
        }
        return super.save(entity);
    }

}
