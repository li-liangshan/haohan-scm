package com.haohan.cloud.scm.api.constant.enums.salec;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author cx
 * @date 2019/7/16
 */

@Getter
@AllArgsConstructor
public enum ChannelEnum {

    /**
     * 等级:1级-5级
     */

    wxmp("0","微信公众号"),
    wxapp("1","微信小程序"),
    pc("2","PC端");

    private static final Map<String, ChannelEnum> MAP = new HashMap<>(8);

    static {
        for (ChannelEnum e : ChannelEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static ChannelEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

    /**
     * 类型
     */
    @EnumValue
    @JsonValue
    private String type;
    /**
     * 描述
     */

    private String desc;
}
