package com.haohan.cloud.scm.api.crm.req.report;

import com.haohan.cloud.scm.api.constant.enums.crm.SalesGoodsTypeEnum;
import com.haohan.cloud.scm.api.crm.entity.DataReportDetail;
import com.haohan.cloud.scm.api.crm.trans.CrmReportTrans;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @author dy
 * @date 2019/9/28
 */
@Data
public class DataDetailAddReq {

    @NotBlank(message = "商品规格id不能为空")
    @Length(min = 0, max = 32, message = "商品规格id长度在0至32之间")
    @ApiModelProperty(value = "商品规格id", required = true)
    private String goodsModelId;

    @NotNull(message = "批发价不能为空")
    @Digits(integer = 8, fraction = 2, message = "批发价的整数位最大8位, 小数位最大2位")
    @Range(min = 0, max = 1000000, message = "批发价在0至1000000之间")
    @ApiModelProperty(value = "批发价", required = true)
    private BigDecimal tradePrice;

    @NotNull(message = "数量不能为空")
    @Digits(integer = 8, fraction = 2, message = "数量的整数位最大8位, 小数位最大2位")
    @Range(min = 0, max = 1000000, message = "数量在0至1000000之间")
    @ApiModelProperty(value = "数量", required = true)
    private BigDecimal goodsNum;

    @Length(min = 0, max = 32, message = "保质期长度在0至32之间")
    @ApiModelProperty(value = "保质期")
    private String expiration;

    @ApiModelProperty(value = "生产日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime productTime;

    @ApiModelProperty(value = "到期日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime maturityTime;

    @Length(min = 0, max = 32, message = "备注信息长度在0至32之间")
    @ApiModelProperty(value = "备注信息")
    private String remarks;

    /**
     * 修改后 无需传入 根据价格判断类型
     */
    @ApiModelProperty(value = "商品销售类型:1.普通2.促销品3.赠品")
    private SalesGoodsTypeEnum salesGoodsType;

    @Digits(integer = 8, fraction = 2, message = "赠品数量的整数位最大8位, 小数位最大2位")
    @ApiModelProperty(value = "赠品数量")
    private BigDecimal giftNum;

    public DataReportDetail transTo() {
        DataReportDetail detail = new DataReportDetail();
        detail.setTradePrice(this.tradePrice);
        detail.setGoodsNum(this.goodsNum);
        detail.setExpiration(this.expiration);
        detail.setProductTime(this.productTime);
        detail.setMaturityTime(this.maturityTime);
        detail.setRemarks(this.remarks);
        detail.setSalesGoodsType(null == this.salesGoodsType ? SalesGoodsTypeEnum.normal : this.salesGoodsType);
        CrmReportTrans.defaultDetailInfo(detail, LocalDate.now());
        return detail;
    }
}
