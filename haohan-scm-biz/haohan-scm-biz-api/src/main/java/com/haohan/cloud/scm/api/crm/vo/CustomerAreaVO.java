package com.haohan.cloud.scm.api.crm.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.haohan.cloud.scm.api.constant.enums.market.AreaTypeEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author dy
 * @date 2019/9/25
 */
@Data
@AllArgsConstructor
public class CustomerAreaVO {

    /**
     * 地区名称
     */
    @JsonProperty("name")
    private String areaName;

    /**
     * 地区等级
     */
    private AreaTypeEnum level;
    /**
     * 客户数量
     */
    @JsonProperty("value")
    private Integer num;

    @ApiModelProperty(value = "地址定位", notes = "地址定位 (经度，纬度), 用于地图上定位点")
    private String position;

}
