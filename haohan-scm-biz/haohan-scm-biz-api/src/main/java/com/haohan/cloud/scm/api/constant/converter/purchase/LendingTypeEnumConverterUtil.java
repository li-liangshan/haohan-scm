package com.haohan.cloud.scm.api.constant.converter.purchase;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.purchase.LendingTypeEnum;

/**
 * @author xwx
 * @date 2019/6/3
 */
public class LendingTypeEnumConverterUtil implements Converter<LendingTypeEnum> {
    @Override
    public LendingTypeEnum convert(Object o, LendingTypeEnum lendingTypeEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return LendingTypeEnum.getByType(o.toString());
    }
}
