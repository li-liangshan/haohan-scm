/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.manage.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.manage.entity.OpenplatformManage;
import com.haohan.cloud.scm.api.manage.req.OpenplatformManageReq;
import com.haohan.cloud.scm.manage.service.OpenplatformManageService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 开放平台应用资料管理
 *
 * @author haohan
 * @date 2019-05-29 14:27:27
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/OpenplatformManage")
@Api(value = "openplatformmanage", tags = "openplatformmanage内部接口服务")
public class OpenplatformManageFeignApiCtrl {

    private final OpenplatformManageService openplatformManageService;


    /**
     * 通过id查询开放平台应用资料管理
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(openplatformManageService.getById(id));
    }


    /**
     * 分页查询 开放平台应用资料管理 列表信息
     * @param openplatformManageReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchOpenplatformManagePage")
    public R getOpenplatformManagePage(@RequestBody OpenplatformManageReq openplatformManageReq) {
        Page page = new Page(openplatformManageReq.getPageNo(), openplatformManageReq.getPageSize());
        OpenplatformManage openplatformManage =new OpenplatformManage();
        BeanUtil.copyProperties(openplatformManageReq, openplatformManage);

        return new R<>(openplatformManageService.page(page, Wrappers.query(openplatformManage)));
    }


    /**
     * 全量查询 开放平台应用资料管理 列表信息
     * @param openplatformManageReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchOpenplatformManageList")
    public R getOpenplatformManageList(@RequestBody OpenplatformManageReq openplatformManageReq) {
        OpenplatformManage openplatformManage =new OpenplatformManage();
        BeanUtil.copyProperties(openplatformManageReq, openplatformManage);

        return new R<>(openplatformManageService.list(Wrappers.query(openplatformManage)));
    }


    /**
     * 新增开放平台应用资料管理
     * @param openplatformManage 开放平台应用资料管理
     * @return R
     */
    @Inner
    @SysLog("新增开放平台应用资料管理")
    @PostMapping("/add")
    public R save(@RequestBody OpenplatformManage openplatformManage) {
        return new R<>(openplatformManageService.save(openplatformManage));
    }

    /**
     * 修改开放平台应用资料管理
     * @param openplatformManage 开放平台应用资料管理
     * @return R
     */
    @Inner
    @SysLog("修改开放平台应用资料管理")
    @PostMapping("/update")
    public R updateById(@RequestBody OpenplatformManage openplatformManage) {
        return new R<>(openplatformManageService.updateById(openplatformManage));
    }

    /**
     * 通过id删除开放平台应用资料管理
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除开放平台应用资料管理")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(openplatformManageService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除开放平台应用资料管理")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(openplatformManageService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询开放平台应用资料管理")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(openplatformManageService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param openplatformManageReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询开放平台应用资料管理总记录}")
    @PostMapping("/countByOpenplatformManageReq")
    public R countByOpenplatformManageReq(@RequestBody OpenplatformManageReq openplatformManageReq) {

        return new R<>(openplatformManageService.count(Wrappers.query(openplatformManageReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param openplatformManageReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据openplatformManageReq查询一条货位信息表")
    @PostMapping("/getOneByOpenplatformManageReq")
    public R getOneByOpenplatformManageReq(@RequestBody OpenplatformManageReq openplatformManageReq) {

        return new R<>(openplatformManageService.getOne(Wrappers.query(openplatformManageReq), false));
    }


    /**
     * 批量修改OR插入
     * @param openplatformManageList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<OpenplatformManage> openplatformManageList) {

        return new R<>(openplatformManageService.saveOrUpdateBatch(openplatformManageList));
    }

}
