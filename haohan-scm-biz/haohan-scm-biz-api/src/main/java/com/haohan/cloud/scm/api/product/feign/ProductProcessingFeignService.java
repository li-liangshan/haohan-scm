/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.product.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.product.entity.ProductProcessing;
import com.haohan.cloud.scm.api.product.req.ProductProcessingReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 货品加工记录表内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "ProductProcessingFeignService", value = ScmServiceName.SCM_BIZ_PRODUCT)
public interface ProductProcessingFeignService {


    /**
     * 通过id查询货品加工记录表
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/ProductProcessing/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 货品加工记录表 列表信息
     * @param productProcessingReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/ProductProcessing/fetchProductProcessingPage")
    R getProductProcessingPage(@RequestBody ProductProcessingReq productProcessingReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 货品加工记录表 列表信息
     * @param productProcessingReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/ProductProcessing/fetchProductProcessingList")
    R getProductProcessingList(@RequestBody ProductProcessingReq productProcessingReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增货品加工记录表
     * @param productProcessing 货品加工记录表
     * @return R
     */
    @PostMapping("/api/feign/ProductProcessing/add")
    R save(@RequestBody ProductProcessing productProcessing, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改货品加工记录表
     * @param productProcessing 货品加工记录表
     * @return R
     */
    @PostMapping("/api/feign/ProductProcessing/update")
    R updateById(@RequestBody ProductProcessing productProcessing, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除货品加工记录表
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/ProductProcessing/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/ProductProcessing/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/ProductProcessing/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param productProcessingReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/ProductProcessing/countByProductProcessingReq")
    R countByProductProcessingReq(@RequestBody ProductProcessingReq productProcessingReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param productProcessingReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/ProductProcessing/getOneByProductProcessingReq")
    R getOneByProductProcessingReq(@RequestBody ProductProcessingReq productProcessingReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param productProcessingList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/ProductProcessing/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<ProductProcessing> productProcessingList, @RequestHeader(SecurityConstants.FROM) String from);


}
