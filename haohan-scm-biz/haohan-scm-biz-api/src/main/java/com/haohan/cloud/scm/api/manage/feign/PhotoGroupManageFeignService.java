/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.manage.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.manage.dto.PhotoGroupDTO;
import com.haohan.cloud.scm.api.manage.entity.PhotoGroupManage;
import com.haohan.cloud.scm.api.manage.req.PhotoGroupManageReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 图片组管理内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "PhotoGroupManageFeignService", value = ScmServiceName.SCM_MANAGE)
public interface PhotoGroupManageFeignService {


    /**
     * 通过id查询图片组管理
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/PhotoGroupManage/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 图片组管理 列表信息
     * @param photoGroupManageReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/PhotoGroupManage/fetchPhotoGroupManagePage")
    R getPhotoGroupManagePage(@RequestBody PhotoGroupManageReq photoGroupManageReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 图片组管理 列表信息
     * @param photoGroupManageReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/PhotoGroupManage/fetchPhotoGroupManageList")
    R getPhotoGroupManageList(@RequestBody PhotoGroupManageReq photoGroupManageReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增图片组管理
     * @param photoGroupManage 图片组管理
     * @return R
     */
    @PostMapping("/api/feign/PhotoGroupManage/add")
    R save(@RequestBody PhotoGroupManage photoGroupManage, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改图片组管理
     * @param photoGroupManage 图片组管理
     * @return R
     */
    @PostMapping("/api/feign/PhotoGroupManage/update")
    R updateById(@RequestBody PhotoGroupManage photoGroupManage, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除图片组管理
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/PhotoGroupManage/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/PhotoGroupManage/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/PhotoGroupManage/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param photoGroupManageReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/PhotoGroupManage/countByPhotoGroupManageReq")
    R countByPhotoGroupManageReq(@RequestBody PhotoGroupManageReq photoGroupManageReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param photoGroupManageReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/PhotoGroupManage/getOneByPhotoGroupManageReq")
    R getOneByPhotoGroupManageReq(@RequestBody PhotoGroupManageReq photoGroupManageReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param photoGroupManageList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/PhotoGroupManage/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<PhotoGroupManage> photoGroupManageList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 保存或修改图片组图片
     * 修改时：需groupNum / photoList
     * 新增时 需 图片组名称/ 类别标签 / merchantId
     * @param photoGroupDTO
     * @param from
     * @return
     */
    @PostMapping("/api/feign/PhotoGroupManage/savePhotoGroup")
    R<PhotoGroupManage> savePhotoGroup(@RequestBody PhotoGroupDTO photoGroupDTO, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 查询图片组图片
     * @param groupNum
     * @param from
     * @return
     */
    @GetMapping("/api/feign/PhotoGroupManage/fetchByGroupNum")
    R<PhotoGroupDTO> fetchByGroupNum(@RequestParam("groupNum") String groupNum, @RequestHeader(SecurityConstants.FROM) String from);

}
