package com.haohan.cloud.scm.api.constant.converter.manage;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.manage.PdsRegFromEnum;

/**
 * @author dy
 * @date 2019/9/12
 */
public class PdsRegFromEnumConverterUtil implements Converter<PdsRegFromEnum> {
    @Override
    public PdsRegFromEnum convert(Object o, PdsRegFromEnum e) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return PdsRegFromEnum.getByType(o.toString());
    }
}
