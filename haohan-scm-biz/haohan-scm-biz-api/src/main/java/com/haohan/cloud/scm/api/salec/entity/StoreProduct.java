/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.salec.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Digits;
import java.math.BigDecimal;

/**
 * 商品表
 *
 * @author haohan
 * @date 2020-05-21 18:42:57
 */
@Data
@TableName("eb_store_product")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "商品表")
public class StoreProduct extends Model<StoreProduct> {
    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "商品id")
    @TableId(type = IdType.INPUT)
    private Integer id;

    @ApiModelProperty(value = "商户Id(0为总后台管理员创建,不为0的时候是商户后台创建)")
    private Integer merId;

    @ApiModelProperty(value = "店铺id")
    private String shopId;

    @Length(max = 256, message = "商品图片长度最大256字符")
    @ApiModelProperty(value = "商品图片")
    private String image;

    @Length(max = 2000, message = "轮播图长度最大2000字符")
    @ApiModelProperty(value = "轮播图")
    private String sliderImage;

    @Length(max = 128, message = "商品名称长度最大128字符")
    @ApiModelProperty(value = "商品名称")
    private String storeName;

    @Length(max = 256, message = "商品简介长度最大256字符")
    @ApiModelProperty(value = "商品简介")
    private String storeInfo;

    @Length(max = 256, message = "关键字长度最大256字符")
    @ApiModelProperty(value = "关键字")
    private String keyword;

    @Length(max = 15, message = "商品条码（一维码）长度最大15字符")
    @ApiModelProperty(value = "商品条码（一维码）")
    private String barCode;

    @Length(max = 64, message = "分类id长度最大64字符")
    @ApiModelProperty(value = "分类id")
    private String cateId;

    @Digits(integer = 8, fraction = 2, message = "商品价格的整数位最大8位, 小数位最大2位")
    @ApiModelProperty(value = "商品价格")
    private BigDecimal price;

    @Digits(integer = 8, fraction = 2, message = "会员价格的整数位最大8位, 小数位最大2位")
    @ApiModelProperty(value = "会员价格")
    private BigDecimal vipPrice;

    @Digits(integer = 8, fraction = 2, message = "市场价的整数位最大8位, 小数位最大2位")
    @ApiModelProperty(value = "市场价", notes = "商品原价即虚拟价")
    private BigDecimal otPrice;

    @Digits(integer = 8, fraction = 2, message = "邮费的整数位最大8位, 小数位最大2位")
    @ApiModelProperty(value = "邮费")
    private BigDecimal postage;

    @Length(max = 32, message = "单位名长度最大32字符")
    @ApiModelProperty(value = "单位名")
    private String unitName;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "销量")
    private Integer sales;

    @ApiModelProperty(value = "库存")
    private Integer stock;

    @ApiModelProperty(value = "状态（0：未上架，1：上架）")
    private Integer isShow;

    @ApiModelProperty(value = "是否热卖")
    private Integer isHot;

    @ApiModelProperty(value = "是否优惠")
    private Integer isBenefit;

    @ApiModelProperty(value = "是否精品")
    private Integer isBest;

    @ApiModelProperty(value = "是否新品")
    private Integer isNew;

    @ApiModelProperty(value = "添加时间")
    private Integer addTime;

    @ApiModelProperty(value = "是否包邮")
    private Integer isPostage;

    @ApiModelProperty(value = "是否删除")
    private Integer isDel;

    @ApiModelProperty(value = "商户是否代理 0不可代理1可代理")
    private Integer merUse;

    @Digits(integer = 8, fraction = 2, message = "获得积分的整数位最大8位, 小数位最大2位")
    @ApiModelProperty(value = "获得积分")
    private BigDecimal giveIntegral;

    @Digits(integer = 8, fraction = 2, message = "成本价的整数位最大8位, 小数位最大2位")
    @ApiModelProperty(value = "成本价")
    private BigDecimal cost;

    @ApiModelProperty(value = "秒杀状态 0 未开启 1已开启")
    private Integer isSeckill;

    @ApiModelProperty(value = "砍价状态 0未开启 1开启")
    private Integer isBargain;

    @ApiModelProperty(value = "是否优品推荐")
    private Integer isGood;

    @ApiModelProperty(value = "是否单独分佣")
    private Integer isSub;

    @ApiModelProperty(value = "虚拟销量")
    private Integer ficti;

    @ApiModelProperty(value = "浏览量")
    private Integer browse;

    @Length(max = 64, message = "商品二维码地址(用户小程序海报)长度最大64字符")
    @ApiModelProperty(value = "商品二维码地址(用户小程序海报)")
    private String codePath;

    @Length(max = 255, message = "淘宝京东1688类型长度最大255字符")
    @ApiModelProperty(value = "淘宝京东1688类型")
    private String soureLink;

    @Length(max = 200, message = "主图视频链接长度最大200字符")
    @ApiModelProperty(value = "主图视频链接")
    private String videoLink;

    @ApiModelProperty(value = "运费模板ID")
    private Integer tempId;

    @ApiModelProperty(value = "规格 0单 1多")
    private Integer specType;

    @Length(max = 255, message = "活动显示排序1=秒杀，2=砍价，3=拼团长度最大255字符")
    @ApiModelProperty(value = "活动显示排序1=秒杀，2=砍价，3=拼团")
    private String activity;

    @ApiModelProperty(value = "租户id")
    private Integer tenantId;

}
