/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.purchase.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.purchase.entity.DealRecord;
import com.haohan.cloud.scm.purchase.mapper.DealRecordMapper;
import com.haohan.cloud.scm.purchase.service.DealRecordService;
import org.springframework.stereotype.Service;

/**
 * 账户扣减记录
 *
 * @author haohan
 * @date 2019-05-13 18:52:25
 */
@Service
public class DealRecordServiceImpl extends ServiceImpl<DealRecordMapper, DealRecord> implements DealRecordService {

}
