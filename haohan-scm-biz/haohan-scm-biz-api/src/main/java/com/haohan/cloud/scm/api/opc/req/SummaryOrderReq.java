/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.opc.req;

import com.haohan.cloud.scm.api.opc.entity.SummaryOrder;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 采购单汇总
 *
 * @author haohan
 * @date 2019-05-30 10:21:03
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "采购单汇总")
public class SummaryOrderReq extends SummaryOrder {

    private long pageSize;
    private long pageNo;




}
