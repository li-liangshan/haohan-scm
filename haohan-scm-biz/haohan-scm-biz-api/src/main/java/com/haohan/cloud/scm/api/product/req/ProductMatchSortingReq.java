package com.haohan.cloud.scm.api.product.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @author dy
 * @date 2019/7/18
 */
@Data
@ApiModel("根据新入库货品 匹配分拣")
public class ProductMatchSortingReq {

    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value="平台商家id", required = true)
    private String pmId;

    @NotNull(message = "realBuyNum不能为空")
    @ApiModelProperty(value="货品入库数量", required = true)
    private BigDecimal realBuyNum;

    @NotBlank(message = "purchaseDetailSn不能为空")
    @ApiModelProperty(value="采购明细编号", required = true)
    private String purchaseDetailSn;

}
