/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.aftersales.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.aftersales.mapper.ReturnOrderDetailMapper;
import com.haohan.cloud.scm.aftersales.service.ReturnOrderDetailService;
import com.haohan.cloud.scm.api.aftersales.entity.ReturnOrderDetail;
import com.haohan.cloud.scm.api.constant.NumberPrefixConstant;
import com.haohan.cloud.scm.common.tools.util.ScmIncrementUtil;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * 退货单明细
 *
 * @author haohan
 * @date 2019-07-30 15:55:55
 */
@Service
@AllArgsConstructor
public class ReturnOrderDetailServiceImpl extends ServiceImpl<ReturnOrderDetailMapper, ReturnOrderDetail> implements ReturnOrderDetailService {

    private final ScmIncrementUtil scmIncrementUtil;

    @Override
    public boolean save(ReturnOrderDetail entity) {
        if (StrUtil.isEmpty(entity.getReturnDetailSn())) {
            String sn = scmIncrementUtil.inrcSnByClass(ReturnOrderDetail.class, NumberPrefixConstant.RETURN_DETAIL_SN_PRE);
            entity.setReturnDetailSn(sn);
        }
        return super.save(entity);
    }

}
