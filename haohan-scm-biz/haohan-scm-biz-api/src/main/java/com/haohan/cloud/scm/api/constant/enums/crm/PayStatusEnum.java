package com.haohan.cloud.scm.api.constant.enums.crm;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dy
 * @date 2019/9/27
 */
@Getter
@AllArgsConstructor
public enum PayStatusEnum implements IBaseEnum {

    /**
     * 订单支付状态:0未付,1已付,2部分支付
     */
    wait("0", "未付"),
    success("1", "已付"),
    part("2", "部分支付");

    private static final Map<String, PayStatusEnum> MAP = new HashMap<>(8);

    static {
        for (PayStatusEnum e : PayStatusEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static PayStatusEnum getByType(String type) {
        return MAP.get(type);
    }

    @EnumValue
    @JsonValue
    private String type;
    private String desc;
}
