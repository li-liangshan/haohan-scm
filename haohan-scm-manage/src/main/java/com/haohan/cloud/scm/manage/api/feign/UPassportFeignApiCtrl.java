/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.manage.api.feign;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.manage.entity.UPassport;
import com.haohan.cloud.scm.api.manage.req.UPassportReq;
import com.haohan.cloud.scm.api.manage.trans.UpassportTrans;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.haohan.cloud.scm.manage.service.UPassportService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.data.tenant.TenantContextHolder;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 用户通行证
 *
 * @author haohan
 * @date 2019-05-29 14:29:24
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/uPassport")
@Api(value = "upassport", tags = "upassport内部接口服务")
public class UPassportFeignApiCtrl {

    private final UPassportService uPassportService;


    /**
     * 通过id查询用户通行证
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R<UPassport> getById(@PathVariable("id") String id) {
        // 处理tenantId, 目前多租户共用
        Integer tenantId = TenantContextHolder.getTenantId();
        TenantContextHolder.setTenantId(null);
        UPassport u = uPassportService.getById(id);
        TenantContextHolder.setTenantId(tenantId);
        return RUtil.success(u);
    }


    /**
     * 分页查询 用户通行证 列表信息
     * @param uPassportReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchUPassportPage")
    public R<IPage<UPassport>> getUPassportPage(@RequestBody UPassportReq uPassportReq) {
        Page page = new Page(uPassportReq.getPageNo(), uPassportReq.getPageSize());
        UPassport uPassport =new UPassport();
        BeanUtil.copyProperties(uPassportReq, uPassport);

        return new R<>(uPassportService.page(page, Wrappers.query(uPassport)));
    }


    /**
     * 全量查询 用户通行证 列表信息
     * @param uPassport 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchUPassportList")
    public R<List<UPassport>> getUPassportList(@RequestBody UPassport uPassport) {
        return new R<>(uPassportService.list(Wrappers.query(uPassport)));
    }


    /**
     * 新增用户通行证
     * @param uPassport 用户通行证
     * @return R
     */
    @Inner
    @SysLog("新增用户通行证")
    @PostMapping("/save")
    public R<Boolean> save(@RequestBody UPassport uPassport) {
        return new R<>(uPassportService.save(uPassport));
    }

    /**
     * 新增用户通行证 初始设置/校验
     * @param uPassport 用户通行证
     * @return R
     */
    @Inner
    @SysLog("新增用户通行证")
    @PostMapping("/add")
    public R<UPassport> add(@RequestBody UPassport uPassport) {
        R r = RUtil.error();
        if(StrUtil.isEmpty(uPassport.getTelephone())|| StrUtil.isEmpty(uPassport.getLoginName())){
            r.setMsg("缺少手机号和名称");
            return r;
        }
        UpassportTrans.initUpassport(uPassport);
        //  同一手机号不重复创建 uPassport
        UPassport exist = uPassportService.fetchByTelephone(uPassport.getTelephone());
        if(null == exist){
            uPassport.setTenantId(null);
            if(uPassportService.save(uPassport)){
                return RUtil.success(uPassport);
            }
            r.setMsg("uPassport保存失败");
        }else{
            UPassport update = new UPassport();
            update.setId(exist.getId());
            update.setPassword(uPassport.getPassword());
            update.setLoginName(uPassport.getLoginName());
            update.setAvatar(uPassport.getAvatar());
            if(uPassportService.updateById(update)){
                return RUtil.success(exist);
            }
            r.setMsg("uPassport修改失败");
        }
        return r;
    }

    /**
     * 修改用户通行证
     * @param uPassport 用户通行证
     * @return R
     */
    @Inner
    @SysLog("修改用户通行证")
    @PostMapping("/update")
    public R<Boolean> updateById(@RequestBody UPassport uPassport) {
        return new R<>(uPassportService.updateById(uPassport));
    }

    /**
     * 通过id删除用户通行证
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除用户通行证")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(uPassportService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除用户通行证")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(uPassportService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询用户通行证")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(uPassportService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param uPassportReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询用户通行证总记录}")
    @PostMapping("/countByUPassportReq")
    public R countByUPassportReq(@RequestBody UPassportReq uPassportReq) {

        return new R<>(uPassportService.count(Wrappers.query(uPassportReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param uPassportReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据uPassportReq查询一条货位信息表")
    @PostMapping("/getOneByUPassportReq")
    public R<UPassport> getOneByUPassportReq(@RequestBody UPassportReq uPassportReq) {

        return new R<>(uPassportService.getOne(Wrappers.query(uPassportReq), false));
    }


    /**
     * 批量修改OR插入
     * @param uPassportList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<UPassport> uPassportList) {

        return new R<>(uPassportService.saveOrUpdateBatch(uPassportList));
    }

}
