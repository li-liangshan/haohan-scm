/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.message.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.message.entity.WechatUserMsgevent;
import com.haohan.cloud.scm.api.message.req.WechatUserMsgeventReq;
import com.haohan.cloud.scm.message.service.WechatUserMsgeventService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 微信用户消息事件
 *
 * @author haohan
 * @date 2019-05-29 13:50:08
 */
@RestController
@AllArgsConstructor
@RequestMapping("/wechatusermsgevent" )
@Api(value = "wechatusermsgevent", tags = "wechatusermsgevent管理")
public class WechatUserMsgeventController {

    private final WechatUserMsgeventService wechatUserMsgeventService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param wechatUserMsgevent 微信用户消息事件
     * @return
     */
    @GetMapping("/page" )
    public R getWechatUserMsgeventPage(Page page, WechatUserMsgevent wechatUserMsgevent) {
        return new R<>(wechatUserMsgeventService.page(page, Wrappers.query(wechatUserMsgevent)));
    }


    /**
     * 通过id查询微信用户消息事件
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(wechatUserMsgeventService.getById(id));
    }

    /**
     * 新增微信用户消息事件
     * @param wechatUserMsgevent 微信用户消息事件
     * @return R
     */
    @SysLog("新增微信用户消息事件" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_wechatusermsgevent_add')" )
    public R save(@RequestBody WechatUserMsgevent wechatUserMsgevent) {
        return new R<>(wechatUserMsgeventService.save(wechatUserMsgevent));
    }

    /**
     * 修改微信用户消息事件
     * @param wechatUserMsgevent 微信用户消息事件
     * @return R
     */
    @SysLog("修改微信用户消息事件" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_wechatusermsgevent_edit')" )
    public R updateById(@RequestBody WechatUserMsgevent wechatUserMsgevent) {
        return new R<>(wechatUserMsgeventService.updateById(wechatUserMsgevent));
    }

    /**
     * 通过id删除微信用户消息事件
     * @param id id
     * @return R
     */
    @SysLog("删除微信用户消息事件" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_wechatusermsgevent_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(wechatUserMsgeventService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除微信用户消息事件")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_wechatusermsgevent_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(wechatUserMsgeventService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询微信用户消息事件")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(wechatUserMsgeventService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param wechatUserMsgeventReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询微信用户消息事件总记录}")
    @PostMapping("/countByWechatUserMsgeventReq")
    public R countByWechatUserMsgeventReq(@RequestBody WechatUserMsgeventReq wechatUserMsgeventReq) {

        return new R<>(wechatUserMsgeventService.count(Wrappers.query(wechatUserMsgeventReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param wechatUserMsgeventReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据wechatUserMsgeventReq查询一条货位信息表")
    @PostMapping("/getOneByWechatUserMsgeventReq")
    public R getOneByWechatUserMsgeventReq(@RequestBody WechatUserMsgeventReq wechatUserMsgeventReq) {

        return new R<>(wechatUserMsgeventService.getOne(Wrappers.query(wechatUserMsgeventReq), false));
    }


    /**
     * 批量修改OR插入
     * @param wechatUserMsgeventList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_wechatusermsgevent_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<WechatUserMsgevent> wechatUserMsgeventList) {

        return new R<>(wechatUserMsgeventService.saveOrUpdateBatch(wechatUserMsgeventList));
    }


}
