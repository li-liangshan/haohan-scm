package com.haohan.cloud.scm.api.wms.req;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author xwx
 * @date 2019/7/16
 */
@Data
public class ParamReq {
    /**
     * 商品规格id
     */
    private String goodsModelId;
    /**
     * 入库货品数量
     */
    private BigDecimal productNumber;
    /**
     * 入库单明细编号
     */
    private String enterWarehouseDetailSn;

}
