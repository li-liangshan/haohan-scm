package com.haohan.cloud.framework.enums;

public interface SuperEnum {
	public int getCode() ;

    public void setCode(int code) ;

    public String getDescription() ;

    public void setDescription(String description) ;
}
