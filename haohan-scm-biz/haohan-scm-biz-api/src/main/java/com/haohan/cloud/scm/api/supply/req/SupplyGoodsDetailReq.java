package com.haohan.cloud.scm.api.supply.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author xwx
 * @date 2019/7/26
 */
@Data
@ApiModel(description = "查询货源信息详情")
public class SupplyGoodsDetailReq {
    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台id",required = true)
    private String pmId;

    @NotBlank(message = "uid不能为空")
    @ApiModelProperty(value = "供应商uid",required = true)
    private String uid;

    @NotBlank(message = "id不能为空")
    @ApiModelProperty(value = "报价单id",required = true)
    private String id;
}
