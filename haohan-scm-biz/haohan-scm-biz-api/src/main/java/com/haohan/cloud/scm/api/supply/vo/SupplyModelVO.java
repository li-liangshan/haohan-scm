package com.haohan.cloud.scm.api.supply.vo;

import cn.hutool.core.bean.BeanUtil;
import com.haohan.cloud.scm.api.constant.enums.supply.SupplyRelationTypeEnum;
import com.haohan.cloud.scm.api.goods.dto.GoodsModelDTO;
import com.haohan.cloud.scm.api.goods.vo.GoodsModelVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author dy
 * @date 2020/4/18
 * 供应商品规格 对 sku
 */
@Data
@NoArgsConstructor
public class SupplyModelVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 规格id
     */
    private String modelId;
    /**
     * 商品id
     */
    private String goodsId;
    /**
     * 规格类型名称
     */
    private String typeName;
    /**
     * 规格名称
     */
    private String modelName;
    /**
     * 商品名称
     */
    private String goodsName;
    /**
     * 规格价格
     */
    private BigDecimal modelPrice;
    /**
     * 规格单位
     */
    private String modelUnit;
    /**
     * 规格库存
     */
    private BigDecimal modelStorage;
    /**
     * 规格商品图片地址
     */
    private String modelUrl;

    /**
     * 扫码购编码
     */
    private String modelCode;
    /**
     * 商品规格唯一编号
     */
    private String goodsModelSn;

    // 扩展属性

    @ApiModelProperty(value = "关联类型", notes = "关联类型: 0.未关联 1.关联商家 2.关联供应人")
    private SupplyRelationTypeEnum relationType;

    public SupplyModelVO(GoodsModelVO model) {
        BeanUtil.copyProperties(model, this);
        this.relationType = SupplyRelationTypeEnum.no;
    }

    public SupplyModelVO(GoodsModelDTO model) {
        this.modelId = model.getId();
        this.goodsId = model.getGoodsId();
//        this.typeName
        this.modelName = model.getModelName();
        this.goodsName = model.getGoodsName();
        this.modelPrice = model.getModelPrice();
        this.modelUnit = model.getModelUnit();
        this.modelStorage = model.getModelStorage();
        this.modelUrl = model.getModelUrl();
        this.modelCode = model.getModelCode();
        this.goodsModelSn = model.getGoodsModelSn();
    }
}
