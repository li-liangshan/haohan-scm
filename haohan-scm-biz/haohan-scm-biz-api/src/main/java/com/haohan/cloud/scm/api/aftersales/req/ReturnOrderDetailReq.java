package com.haohan.cloud.scm.api.aftersales.req;

import com.haohan.cloud.scm.api.aftersales.entity.ReturnOrderDetail;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author cx
 * @date 2019/7/30
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "退货单详情")
public class ReturnOrderDetailReq extends ReturnOrderDetail {

    private long pageSize;
    private long pageNo;




}

