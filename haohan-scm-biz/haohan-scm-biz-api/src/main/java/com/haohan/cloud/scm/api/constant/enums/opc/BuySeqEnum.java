package com.haohan.cloud.scm.api.constant.enums.opc;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dy
 * @date 2019/5/31
 */
@Getter
@AllArgsConstructor
public enum BuySeqEnum implements IBaseEnum {

    /**
     * 送货批次:0第一批次1第二批次
     */
    first("0", "第一批次"),
    second("1", "第二批次");

    private static final Map<String, BuySeqEnum> MAP = new HashMap<>(8);

    static {
        for (BuySeqEnum e : BuySeqEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static BuySeqEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

    @EnumValue
    @JsonValue
    private String type;
    private String desc;
}
