package com.haohan.cloud.scm.api.manage.trans;

import cn.hutool.core.collection.CollUtil;
import com.haohan.cloud.scm.api.constant.enums.manage.MerchantStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.manage.OssTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.manage.PhotoTypeEnum;
import com.haohan.cloud.scm.api.manage.dto.PhotoGroupDTO;
import com.haohan.cloud.scm.api.manage.entity.PhotoGallery;
import com.haohan.cloud.scm.api.manage.vo.PhotoVO;
import lombok.experimental.UtilityClass;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author dy
 * @date 2019/10/18
 */
@UtilityClass
public class PhotoTrans {

    /**
     * 图片id列表转换
     *
     * @param list
     * @return
     */
    public List<PhotoGallery> transPhotoList(List<String> list) {
        if (CollUtil.isEmpty(list)) {
            return new ArrayList<>(10);
        }
        return list.stream()
                .map(item -> {
                    PhotoGallery photo = new PhotoGallery();
                    photo.setId(item);
                    return photo;
                })
                .collect(Collectors.toList());
    }

    /**
     * 转为 图片地址列表
     *
     * @param list
     * @return
     */
    public List<PhotoVO> photoListTrans(List<PhotoGallery> list) {
        if (CollUtil.isEmpty(list)) {
            return new ArrayList<>(10);
        }
        return list.stream()
                .map(PhotoVO::new)
                .collect(Collectors.toList());
    }

    /**
     * 上传图片信息初始设置
     *
     * @return
     */
    public PhotoGallery initPhotoUpload(MultipartFile file, String url, PhotoTypeEnum type) {
        PhotoGallery photo = new PhotoGallery();
        photo.setPicName(file.getOriginalFilename());
        photo.setPicUrl(url);
        photo.setPicType(type);
        photo.setPicSize(String.valueOf(file.getSize()));
        photo.setPicFrom("scm");
        photo.setOssType(OssTypeEnum.aliyun);
        photo.setStatus(MerchantStatusEnum.enabled);
        return photo;
    }


    /**
     * 设置图片组属性 用于新增或修改
     *
     * @param groupName
     * @param groupNum
     * @param type
     * @param photoList id列表
     * @return
     */
    public PhotoGroupDTO initToEdit(String groupName, String groupNum, PhotoTypeEnum type, List<String> photoList) {
        return initToEdit(groupName, groupNum, type, "", transPhotoList(photoList));
    }

    /**
     * 设置图片组属性 用于新增或修改
     *
     * @param photoList
     * @param groupName
     * @param groupNum
     * @param type
     * @return
     */
    public PhotoGroupDTO initToEdit(List<PhotoGallery> photoList, String groupName, String groupNum, PhotoTypeEnum type) {
        return initToEdit(groupName, groupNum, type, "", photoList);
    }

    public PhotoGroupDTO initToEdit(String groupName, String groupNum, PhotoTypeEnum type, String merchantId, List<PhotoGallery> photoList) {
        PhotoGroupDTO photo = new PhotoGroupDTO();
        photo.setMerchantId(merchantId);
        photo.setGroupName(groupName);
        photo.setCategoryTag(type.getDesc());
        photo.setPhotoList(photoList);
        photo.setGroupNum(groupNum);
        return photo;
    }

}
