/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.goods.req;

import com.haohan.cloud.scm.api.goods.entity.StandardProductUnit;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 标准商品库
 *
 * @author haohan
 * @date 2019-05-28 19:59:08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "标准商品库")
public class StandardProductUnitReq extends StandardProductUnit {

    private long pageSize;
    private long pageNo;




}
