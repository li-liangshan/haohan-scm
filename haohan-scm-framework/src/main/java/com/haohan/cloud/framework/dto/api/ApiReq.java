package com.haohan.cloud.framework.dto.api;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.haohan.cloud.framework.dto.Device;
import com.haohan.cloud.framework.dto.Header;
import com.haohan.cloud.framework.dto.Proxy;
import com.haohan.cloud.framework.entity.BaseReq;
import com.haohan.cloud.framework.utils.JacksonUtils;
import org.apache.commons.lang.StringUtils;

import java.io.Serializable;

/**
 * 请求封装父类
 *
 * @author zhaokuner
 *
 */
public class ApiReq extends BaseReq implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 747501983476874249L;

	public static String PARAMS_DATA = "data";
	@JsonIgnore
	private Header header;
	@JsonIgnore
	private Device device;
	@JsonIgnore
	private Proxy proxy;
	@JsonIgnore
	private String data;
	/**
	 * paraseData data时 是否成功
	 */
	@JsonIgnore
	private boolean initSuccess = true;
	/**时间戳（毫秒）*/
	protected Long timestamp = System.currentTimeMillis();

	/**
	 * 通讯秘钥 sendMsg时加密用
	 */
	@JsonIgnore
	private String sercertKey;
	/**
	 * 请求目标serviceId
	 */
	@JsonIgnore
	private String targetServiceId;

	public static <T extends ApiReq> T paraseData(ApiReq apiReq , Class<T> clazz) {

		T result = null;

		if (apiReq != null && StringUtils.isNotBlank(apiReq.getData())) {
			result = JacksonUtils.readValue(apiReq.getData(), clazz);
		}
		if(null == result){
			try {
				result = clazz.newInstance();
			} catch (InstantiationException | IllegalAccessException e) {
				e.printStackTrace();
			}
			result.setInitSuccess(false);
		}
		result.setData(apiReq.getData());

		if (apiReq != null) {
				result.setHeader(apiReq.getHeader());
				result.setDevice(apiReq.getDevice());
				result.setProxy(apiReq.getProxy());
		}

		return result;
	}

	public String toDataJson() {
		if ((null == data || data.isEmpty())) {
			this.setData(JacksonUtils.toJson(this));
		}
		return data;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public Header getHeader() {
		return header;
	}

	public void setHeader(Header header) {
		this.header = header;
	}

	public Device getDevice() {
		return device;
	}

	public void setDevice(Device device) {
		this.device = device;
	}

	public Proxy getProxy() {
		return proxy;
	}

	public void setProxy(Proxy proxy) {
		this.proxy = proxy;
	}
	/**
	 * 通讯秘钥 sendMsg时加密用
	 */
	public String getSercertKey() {
		return sercertKey;
	}
	/**
	 * 通讯秘钥 sendMsg时加密用
	 */
	public void setSercertKey(String sercertKey) {
		this.sercertKey = sercertKey;
	}


	public Long getTimestamp() {
		return timestamp;
	}


	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}
	/**
	 * 数据留存，不用于数据传输
	 * @return
	 */
	public String toJson() {
		StringBuilder sb = new StringBuilder();
		sb.append("{")
		.append("\"header\"").append(":").append(JacksonUtils.toJson(this.header)).append(",")
		.append("\"proxy\"").append(":").append(JacksonUtils.toJson(this.proxy)).append(",")
		.append("\"device\"").append(":").append(JacksonUtils.toJson(this.device)).append(",")
		.append("\"data\"").append(":").append(JacksonUtils.toJson(this.data)).append(",")
		.append("\"sercertKey\"").append(":").append(JacksonUtils.toJson(StringUtils.isBlank(this.sercertKey)?"":"******")).append(",")
		.append("\"targetServiceId\"").append(":").append(JacksonUtils.toJson(this.targetServiceId)).append(",");
		String thisJson = JacksonUtils.toJson(this);
		if(StringUtils.isNotBlank(thisJson)){
			thisJson = thisJson.substring(1, thisJson.length()-1);
			if(thisJson.isEmpty()){
				sb.deleteCharAt(sb.length()-1);
			}else{
				sb.append(thisJson);
			}
		}
		sb.append("}");
		return sb.toString();
	}

	@Override
	public String toString() {
		return toJson();
	}
	/**
	 * paraseData data时 是否成功
	 * @return
	 */
	public boolean isInitSuccess() {
		return initSuccess;
	}
	/**
	 * paraseData data时 是否成功
	 * @return
	 */
	public void setInitSuccess(boolean initSuccess) {
		this.initSuccess = initSuccess;
	}
	/**
	 * 请求目标serviceId
	 */
	public String getTargetServiceId() {
		return targetServiceId;
	}
	/**
	 * 请求目标serviceId
	 */
	public void setTargetServiceId(String targetServiceId) {
		this.targetServiceId = targetServiceId;
	}

}
