/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.product.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.product.dto.SortingOrderDetailDTO;
import com.haohan.cloud.scm.api.product.entity.SortingOrderDetail;

/**
 * 分拣单明细
 *
 * @author haohan
 * @date 2019-05-13 18:15:00
 */
public interface SortingOrderDetailService extends IService<SortingOrderDetail> {

    /**
     * 查询分拣单明细列表
     * @param req
     * @return
     */
    IPage<SortingOrderDetailDTO> queryDetailList(Page page, SortingOrderDetailDTO req);
}
