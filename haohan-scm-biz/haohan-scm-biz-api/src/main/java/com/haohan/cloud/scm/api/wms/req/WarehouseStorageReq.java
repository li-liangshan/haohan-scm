package com.haohan.cloud.scm.api.wms.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

/**
 * @author xwx
 * @date 2019/6/18
 */
@Data
@ApiModel(description = "入库存储")
public class WarehouseStorageReq {
    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台id",required = true)
    private String pmId;

    @NotBlank(message = "warehouseSn不能为空")
    @ApiModelProperty(value = "仓库编号",required = true)
    private String warehouseSn;

    @NotEmpty(message = "ids采购单明细编号的数组不能为空")
    @ApiModelProperty(value = "采购单明细编号的数组" , required = true)
    private String[] ids;

    @ApiModelProperty(value = "入库批次号")
    private String batchNumber;

    @ApiModelProperty(value = "采购单编号")
    private String purchaseSn;

    @ApiModelProperty(value = "入库申请人id")
    private String applicantId;

    @ApiModelProperty(value = "入库申请人名称")
    private String applicantName;
}
