package com.haohan.cloud.scm.product.core.impl;

import cn.hutool.core.bean.BeanUtil;
import com.haohan.cloud.scm.api.constant.enums.product.LossTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.product.ProductionTypeEnum;
import com.haohan.cloud.scm.api.goods.dto.GoodsModelDTO;
import com.haohan.cloud.scm.api.manage.entity.UPassport;
import com.haohan.cloud.scm.api.opc.entity.SummaryOrder;
import com.haohan.cloud.scm.api.product.SingleRealLossDTO;
import com.haohan.cloud.scm.api.product.entity.ProductionTask;
import com.haohan.cloud.scm.api.product.req.CreateDeliveryTaskReq;
import com.haohan.cloud.scm.api.product.req.CreateProcessingTaskReq;
import com.haohan.cloud.scm.api.product.resp.SingleRealLossResp;
import com.haohan.cloud.scm.api.product.trans.ProductionTaskTrans;
import com.haohan.cloud.scm.common.tools.exception.ErrorDataException;
import com.haohan.cloud.scm.product.core.IProductionTaskService;
import com.haohan.cloud.scm.product.service.ProductLossRecordService;
import com.haohan.cloud.scm.product.service.ProductionTaskService;
import com.haohan.cloud.scm.product.utils.ScmProductUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

/**
 * @author cx
 * @date 2019/6/14
 */
@Service
public class IProductionTaskServiceImpl implements IProductionTaskService {

    @Autowired
    @Lazy
    private ProductionTaskService productionTaskService;

    @Autowired
    @Lazy
    private ScmProductUtils scmProductUtils;

    @Autowired
    @Lazy
    private ProductLossRecordService productLossRecordService;
    /**
     * 新增 生产部任务(任务类别: 加工)  根据 汇总商品需求/预计需求
     *
     * @param req
     * @return
     */
    @Override
    public ProductionTask createProductProcessingTask(CreateProcessingTaskReq req) {
        //查询商品信息
        GoodsModelDTO goodsModel = scmProductUtils.queryGoodsModelById(req.getGoodsModelId());
        //查询发起人
        UPassport initiator = scmProductUtils.queryUPassportById(req.getInitiatorId());
        //查询执行人
        UPassport transactor = scmProductUtils.queryUPassportById(req.getTransactorId());
        //创建生产任务
        ProductionTask task = new ProductionTask();
        BeanUtil.copyProperties(req, task);
        ProductionTaskTrans.goodsModelToProductionTaskTrans(task, goodsModel);
        task.setInitiatorName(initiator.getLoginName());
        task.setTransactorName(transactor.getLoginName());
        task.setProductionType(ProductionTypeEnum.handle);
        if (!productionTaskService.save(task)) {
            throw new ErrorDataException();
        }
        return task;
    }

    /**
     * 新增 生产部任务(任务类别: 配送) 根据 汇总商品需求
     *
     * @param req
     * @return
     */
    @Override
    public ProductionTask createProductDeliveryTask(CreateDeliveryTaskReq req) {
        //查询汇总单
        SummaryOrder order = scmProductUtils.querySummaryOrderById(req.getSummaryOrderId(), req.getPmId());
        //查询商品信息
        GoodsModelDTO goodsModel = scmProductUtils.queryGoodsModelById(order.getGoodsModelId());
        //查询发起人
        UPassport initiator = scmProductUtils.queryUPassportById(req.getInitiatorId());
        //查询执行人
        UPassport transactor = scmProductUtils.queryUPassportById(req.getTransactorId());
        //创建生产任务,注入生产任务数据
        ProductionTask task = new ProductionTask();
        BeanUtil.copyProperties(req, task);
        ProductionTaskTrans.summaryOrderToProductionTaskTrans(task, order);
        task.setGoodsModelName(goodsModel.getModelName());
        task.setInitiatorName(initiator.getLoginName());
        task.setTransactorName(transactor.getLoginName());
        task.setProductionType(ProductionTypeEnum.delivery);
        //保存生产任务
        if (!productionTaskService.save(task)) {
            throw new ErrorDataException();
        }
        return task;
    }

    /**
     * 查询统计 单品实际损耗率
     *
     * @param dto
     * @return
     */
    @Override
    public SingleRealLossResp querySingleRealLoss(SingleRealLossDTO dto) {
        SingleRealLossResp resp = new SingleRealLossResp();
        resp.setGoodsModelId(dto.getGoodsModelId());
        resp.setPmId(dto.getPmId());
        resp.setLossType(LossTypeEnum.getByType(dto.getLossType()));
        resp.setLossRate(productLossRecordService.queryLossRate(dto));
        return resp;
    }
}
