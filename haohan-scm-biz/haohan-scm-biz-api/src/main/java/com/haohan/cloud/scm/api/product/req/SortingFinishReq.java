package com.haohan.cloud.scm.api.product.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author dy
 * @date 2019/8/11
 */
@Data
@ApiModel(description = "单个分拣单,确认完成")
public class SortingFinishReq {

    /**
     * 平台商家id
     */
    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台商家id", required = true)
    private String pmId;

    /**
     * 分拣单编号
     */
    @NotBlank(message = "sortingOrderSn不能为空")
    @ApiModelProperty(value = "分拣单编号", required = true)
    private String sortingOrderSn;



}
