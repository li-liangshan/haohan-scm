package com.haohan.cloud.scm.api.crm.req.customer;

import cn.hutool.core.bean.BeanUtil;
import com.haohan.cloud.scm.api.constant.ScmCommonConstant;
import com.haohan.cloud.scm.api.constant.enums.common.TaskStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.crm.VisitStepEnum;
import com.haohan.cloud.scm.api.constant.enums.market.CustomerStatusEnum;
import com.haohan.cloud.scm.api.crm.entity.CustomerVisit;
import com.haohan.cloud.scm.common.tools.constant.FirstGroup;
import com.haohan.cloud.scm.common.tools.constant.SecondGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author dy
 * @date 2019/9/26
 */
@Data
public class EditCustomerVisitReq {

    /**
     * 主键
     */
    @NotBlank(message = "id不能为空")
    @ApiModelProperty(value = "主键")
    @Length(min = 0, max = 32, message = "主键长度在0至32之间")
    private String id;
    /**
     * 客户编号
     */
    @NotBlank(message = "客户编号不能为空", groups = {FirstGroup.class})
    @ApiModelProperty(value = "客户编号")
    @Length(min = 0, max = 32, message = "客户编号长度在0至32之间")
    private String customerSn;
    /**
     * 拜访地址
     */
    @ApiModelProperty(value = "拜访地址")
    @Length(min = 0, max = 200, message = "拜访地址长度在0至200之间")
    private String visitAddress;
    /**
     * 拜访联系人id
     */
    @ApiModelProperty(value = "拜访联系人id")
    @Length(min = 0, max = 32, message = "拜访联系人id长度在0至32之间")
    private String linkmanId;
    /**
     * 拜访日期
     */
    @ApiModelProperty(value = "拜访日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate visitDate;
    /**
     * 拜访内容
     */
    @ApiModelProperty(value = "拜访内容")
    @Length(min = 0, max = 255, message = "拜访内容长度在0至255之间")
    private String visitContent;
    /**
     * 进展阶段:1初次拜访2了解交流3.深入跟进4达成合作5商务往来
     */
    @ApiModelProperty(value = "进展阶段:1初次拜访2了解交流3.深入跟进4达成合作5商务往来")
    private VisitStepEnum visitStep;
    /**
     * 拜访图片组编号
     */
    @ApiModelProperty(value = "拜访图片组编号")
    @Length(min = 0, max = 32, message = "拜访图片组编号长度在0至32之间")
    private String photoNum;
    /**
     * 拜访员工id
     */
    @ApiModelProperty(value = "拜访员工id")
    @Length(min = 0, max = 32, message = "拜访员工id长度在0至32之间")
    private String employeeId;
    /**
     * 下次回访时间
     */
    @ApiModelProperty(value = "下次回访时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime nextVisitTime;
    /**
     * 客户状态:0.无1潜在2.有意向3成交4失败
     */
    @ApiModelProperty(value = "客户状态:0.无1潜在2.有意向3成交4失败")
    private CustomerStatusEnum customerStatus;
    /**
     * 执行状态:1.待处理2.执行中3.已完成4.未完成
     */
    @ApiModelProperty(value = "执行状态:1.待处理2.执行中3.已完成4.未完成")
    private TaskStatusEnum visitStatus;
    /**
     * 拜访总结
     */
    @NotBlank(message = "客户编号不能为空", groups = {SecondGroup.class})
    @ApiModelProperty(value = "拜访总结")
    @Length(min = 0, max = 255, message = "拜访总结长度在0至255之间")
    private String summaryContent;
    /**
     * 抵达时间
     */
    @ApiModelProperty(value = "抵达时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime arrivalTime;
    /**
     * 离开时间
     */
    @ApiModelProperty(value = "离开时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime departureTime;
    /**
     * 抵达地址定位
     */
    @Pattern(regexp = ScmCommonConstant.POSITION_PATTEN, message = "抵达地址定位, 经纬度格式有误, 经度正负180度之间, 纬度正负90度之间, 弧度数表示")
    @ApiModelProperty(value = "抵达地址定位")
    private String arrivalPosition;
    /**
     * 离开地址定位
     */
    @Pattern(regexp = ScmCommonConstant.POSITION_PATTEN, message = "离开地址定位, 经纬度格式有误, 经度正负180度之间, 纬度正负90度之间, 弧度数表示")
    @ApiModelProperty(value = "离开地址定位")
    private String departurePosition;

    @ApiModelProperty(value = "备注信息")
    @Length(min = 0, max = 255, message = "备注信息长度在0至255之间")
    private String remarks;

    @ApiModelProperty(value = "拜访图片列表")
    private List<String> photoList;

    public CustomerVisit transTo() {
        CustomerVisit visit = new CustomerVisit();
        BeanUtil.copyProperties(this, visit);
        return visit;
    }
}
