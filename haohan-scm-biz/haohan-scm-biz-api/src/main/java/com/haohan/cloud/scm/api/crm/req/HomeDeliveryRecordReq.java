/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.crm.req;

import com.haohan.cloud.scm.api.crm.entity.HomeDeliveryRecord;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 送货上门服务记录表
 *
 * @author haohan
 * @date 2019-12-10 18:11:09
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "送货上门服务记录表")
public class HomeDeliveryRecordReq extends HomeDeliveryRecord {

    private long pageSize;
    private long pageNo;


}
