package com.haohan.cloud.scm.api.constant.enums.market;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dy
 * @date 2019/9/26
 */
@Getter
@AllArgsConstructor
public enum MarketTypeEnum implements IBaseEnum {

    /**
     * 市场类型： 1.一级市场 2.二级市场
     */
    one("1", "一级市场"),
    two("2", "二级市场");

    private static final Map<String, MarketTypeEnum> MAP = new HashMap<>(8);
    private static final Map<String, MarketTypeEnum> DESC_MAP = new HashMap<>(8);

    static {
        for (MarketTypeEnum e : MarketTypeEnum.values()) {
            MAP.put(e.getType(), e);
            DESC_MAP.put(e.getDesc(), e);
        }
    }

    @JsonCreator
    public static MarketTypeEnum getByType(String type) {
        return MAP.get(type);
    }

    public static MarketTypeEnum getByDesc(String desc) {
        return DESC_MAP.get(desc);
    }

    @EnumValue
    @JsonValue
    private String type;
    private String desc;
}
