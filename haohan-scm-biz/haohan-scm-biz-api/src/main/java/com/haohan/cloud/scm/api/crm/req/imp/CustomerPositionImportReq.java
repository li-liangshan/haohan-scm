package com.haohan.cloud.scm.api.crm.req.imp;

import com.haohan.cloud.scm.api.crm.dto.imp.CustomerPositionImport;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @author dy
 * @date 2019/10/11
 */
@Data
public class CustomerPositionImportReq {

    @NotEmpty(message = "客户经纬度列表不能为空")
    private List<CustomerPositionImport> positionList;

}
