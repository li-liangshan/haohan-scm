package com.haohan.cloud.scm.api.crm.vo;

import com.haohan.cloud.scm.api.constant.enums.common.UseStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.market.CompanyNatureEnum;
import com.haohan.cloud.scm.api.constant.enums.market.CustomerTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.supply.GradeTypeEnum;
import com.haohan.cloud.scm.api.crm.entity.Customer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author dy
 * @date 2019/12/9
 */
@Data
@NoArgsConstructor
public class CustomerIgnoreVO implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 客户编号
     */
    @ApiModelProperty(value = "客户编号")
    private String customerSn;
    /**
     * 客户名称
     */
    @ApiModelProperty(value = "客户名称")
    private String customerName;
    /**
     * 销售区域编号
     */
    @ApiModelProperty(value = "销售区域")
    private String areaSn;

    @ApiModelProperty(value = "市场编码")
    private String marketSn;
    /**
     * 客户联系人名称
     */
    @ApiModelProperty(value = "客户联系人")
    private String contact;
    /**
     * 联系人手机
     */
    @ApiModelProperty(value = "联系手机")
    private String telephone;
    /**
     * 座机电话
     */
    @ApiModelProperty(value = "座机电话")
    private String phoneNumber;
    /**
     * 省
     */
    @ApiModelProperty(value = "省")
    private String province;
    /**
     * 市
     */
    @ApiModelProperty(value = "市")
    private String city;
    /**
     * 区
     */
    @ApiModelProperty(value = "区")
    private String district;
    /**
     * 街道、乡镇
     */
    @ApiModelProperty(value = "街道、乡镇")
    private String street;
    /**
     * 详细地址
     */
    @ApiModelProperty(value = "详细地址")
    private String address;
    /**
     * 客户地址定位 (经度，纬度)
     */
    @ApiModelProperty(value = "客户地址定位", notes = "客户地址定位 (经度，纬度)")
    private String position;
    /**
     * 客户标签
     */
    @ApiModelProperty(value = "客户标签")
    private String tags;
    /**
     * 收录时间
     */
    @ApiModelProperty(value = "收录时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime initTime;
    /**
     * 客户状态   0.未启用 1.启用 2.待审核
     */
    @ApiModelProperty(value = "客户状态")
    private UseStatusEnum status;
    /**
     * 客户类型:1经销商2门店
     */
    @ApiModelProperty(value = "客户类型", notes = "客户类型:1经销商2门店")
    private CustomerTypeEnum customerType;
    /**
     * 客户负责人id
     */
    @ApiModelProperty(value = "客户负责人id")
    private String directorId;
    /**
     * 公司性质:1.个体2.民营3.国有4.其他
     */
    @ApiModelProperty(value = "公司性质", notes = "公司性质:1.个体2.民营3.国有4.其他")
    private CompanyNatureEnum companyNature;
    /**
     * 客户级别:1星-5星
     */
    @ApiModelProperty(value = "客户级别", notes = "客户级别:1星-5星")
    private GradeTypeEnum customerLevel;

    public CustomerIgnoreVO(Customer customer) {
        this.customerSn = customer.getCustomerSn();
        this.customerName = customer.getCustomerName();
        this.areaSn = customer.getAreaSn();
        this.marketSn = customer.getMarketSn();
        this.contact = customer.getContact();
        this.telephone = customer.getTelephone();
        this.phoneNumber = customer.getPhoneNumber();
        this.province = customer.getProvince();
        this.city = customer.getCity();
        this.district = customer.getDistrict();
        this.street = customer.getStreet();
        this.address = customer.getAddress();
        this.position = customer.getPosition();
        this.tags = customer.getTags();
        this.initTime = customer.getInitTime();
        this.status = customer.getStatus();
        this.customerType = customer.getCustomerType();
        this.directorId = customer.getDirectorId();
        this.companyNature = customer.getCompanyNature();
        this.customerLevel = customer.getCustomerLevel();
    }

//      "id",
//      "customerSn",
//      "customerName",
//      "areaSn",
//      "marketSn",
//      "contact",
//      "telephone",
//      "phoneNumber",
//      "province",
//      "city",
//      "district",
//      "street",
//      "address",
//      "position",
//      "tags",
//      "postcode",
//      "fax",
//      "website",
//      "serviceTime",
//      "photoGroupNum",
//      "operateArea",
//      "shopSale",
//      "bizDesc",
//      "bizLicense",
//      "licenseName",
//      "licenseGroupNum",
//      "registrationNum",
//      "registrationDate",
//      "regName",
//      "legalName",
//      "regAddress",
//      "initTime",
//      "status",
//      "customerType",
//      "directorId",
//      "companyNature",
//      "customerLevel",
//      "createBy",
//      "createDate",
//      "updateBy",
//      "updateDate",
//      "remarks",
//      "delFlag",
//      "tenantId"


}
