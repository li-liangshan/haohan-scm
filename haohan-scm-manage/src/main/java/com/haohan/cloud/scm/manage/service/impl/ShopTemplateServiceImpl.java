/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.manage.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.manage.entity.ShopTemplate;
import com.haohan.cloud.scm.manage.mapper.ShopTemplateMapper;
import com.haohan.cloud.scm.manage.service.ShopTemplateService;
import org.springframework.stereotype.Service;

/**
 * 店铺模板
 *
 * @author haohan
 * @date 2019-05-13 17:45:13
 */
@Service
public class ShopTemplateServiceImpl extends ServiceImpl<ShopTemplateMapper, ShopTemplate> implements ShopTemplateService {

}
