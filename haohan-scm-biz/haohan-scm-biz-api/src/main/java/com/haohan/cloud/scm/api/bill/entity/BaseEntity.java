package com.haohan.cloud.scm.api.bill.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

/**
 * @author dy
 * @date 2019/12/6
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "实体基础属性")
public abstract class BaseEntity<T extends Model> extends Model<T> {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @Length(max = 32, message = "备注信息最大长度为32字符")
    @TableId(type = IdType.INPUT)
    protected String id;
    /**
     * 备注信息
     */
    @Length(max = 255, message = "备注信息最大长度为255字符")
    protected String remarks;

    /**
     * 创建者
     */
    @TableField(fill = FieldFill.INSERT)
    protected String createBy;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    protected LocalDateTime createDate;
    /**
     * 更新者
     */
    @TableField(fill = FieldFill.UPDATE)
    protected String updateBy;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.UPDATE)
    protected LocalDateTime updateDate;
    /**
     * 删除标记
     */
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    protected String delFlag;
    /**
     * 租户id
     */
    protected Integer tenantId;
}
