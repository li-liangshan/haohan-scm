/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.manage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.manage.entity.PhotoGallery;
import com.haohan.cloud.scm.api.manage.entity.PhotoManage;

import java.util.List;

/**
 * 图片管理
 *
 * @author haohan
 * @date 2019-05-13 17:32:37
 */
public interface PhotoManageService extends IService<PhotoManage> {
    /**
     * 根据图片组编号查询图片管理列表
     * @param groupNum
     * @return
     */
    List<PhotoManage> findListByNum(String groupNum);

    /**
     * 根据图片组编号和图片资源 新增 图片管理
     * @param groupNum
     * @param photoGallery
     * @return
     */
    boolean addPhotoManage(String groupNum, PhotoGallery photoGallery);

}
