package com.haohan.cloud.scm.api.crm.vo.analysis;

import com.haohan.cloud.scm.api.crm.vo.CustomerTypeVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author dy
 * @date 2019/11/6
 */
@Data
@Api("客户数按类型统计")
public class CustomerTypeCountVO {

    @ApiModelProperty(value = "客户负责人id")
    private String directorId;

    @ApiModelProperty("客户总数")
    private Integer total;

    @ApiModelProperty("客户类型列表")
    private List<CustomerTypeVO> list;

}
