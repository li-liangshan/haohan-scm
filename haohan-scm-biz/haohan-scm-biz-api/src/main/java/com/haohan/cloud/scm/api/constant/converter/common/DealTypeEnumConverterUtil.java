package com.haohan.cloud.scm.api.constant.converter.common;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.common.DealTypeEnum;

/**
 * @author xwx
 * @date 2019/6/5
 */
public class DealTypeEnumConverterUtil implements Converter<DealTypeEnum> {
    @Override
    public DealTypeEnum convert(Object o, DealTypeEnum dealTypeEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return DealTypeEnum.getByType(o.toString());
    }
}
