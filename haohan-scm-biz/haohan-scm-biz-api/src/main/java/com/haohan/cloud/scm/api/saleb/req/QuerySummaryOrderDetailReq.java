package com.haohan.cloud.scm.api.saleb.req;

import com.haohan.cloud.scm.api.constant.enums.opc.BuySeqEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import java.time.LocalDate;

/**
 * @author cx
 * @date 2019/7/12
 */
@Data
public class QuerySummaryOrderDetailReq {

    @NotBlank(message = "pmId不能为空")
    private String pmId;

    @NotBlank(message = "goodsModelId不能为空")
    private String goodsModelId;

    private YesNoEnum summaryFlag;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate deliveryTime;

    private BuySeqEnum buySeq;
}
