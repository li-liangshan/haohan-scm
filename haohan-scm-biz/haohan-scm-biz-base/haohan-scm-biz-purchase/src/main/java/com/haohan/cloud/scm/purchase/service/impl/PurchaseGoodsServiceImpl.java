/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.purchase.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseGoods;
import com.haohan.cloud.scm.purchase.mapper.PurchaseGoodsMapper;
import com.haohan.cloud.scm.purchase.service.PurchaseGoodsService;
import org.springframework.stereotype.Service;

/**
 * 采购员工管理商品
 *
 * @author haohan
 * @date 2019-05-13 19:06:17
 */
@Service
public class PurchaseGoodsServiceImpl extends ServiceImpl<PurchaseGoodsMapper, PurchaseGoods> implements PurchaseGoodsService {

}
