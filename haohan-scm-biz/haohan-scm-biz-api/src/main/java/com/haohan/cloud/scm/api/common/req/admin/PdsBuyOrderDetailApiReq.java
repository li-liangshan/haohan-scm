package com.haohan.cloud.scm.api.common.req.admin;

import javax.validation.constraints.NotBlank;
import java.util.Date;

/**
 * @author shenyu
 * @create 2019/2/14
 */
public class PdsBuyOrderDetailApiReq extends PdsBaseApiReq {
    @NotBlank(message = "missing param detailId")
    private String detailId;
    private Date deliveryDate;
    private String buySeq;
    @NotBlank(message = "missing param buyId")
    private String buyId;


    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getBuySeq() {
        return buySeq;
    }

    public void setBuySeq(String buySeq) {
        this.buySeq = buySeq;
    }

    public String getBuyId() {
        return buyId;
    }

    public void setBuyId(String buyId) {
        this.buyId = buyId;
    }

    public String getDetailId() {
        return detailId;
    }

    public void setDetailId(String detailId) {
        this.detailId = detailId;
    }
}
