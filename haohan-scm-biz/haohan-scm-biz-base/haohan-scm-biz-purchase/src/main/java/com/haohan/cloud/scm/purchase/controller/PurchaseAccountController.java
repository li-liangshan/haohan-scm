/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.purchase.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseAccount;
import com.haohan.cloud.scm.api.purchase.req.PurchaseAccountReq;
import com.haohan.cloud.scm.purchase.service.PurchaseAccountService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 采购员工账户
 *
 * @author haohan
 * @date 2019-05-29 13:34:39
 */
@RestController
@AllArgsConstructor
@RequestMapping("/purchaseaccount" )
@Api(value = "purchaseaccount", tags = "purchaseaccount管理")
public class PurchaseAccountController {

    private final PurchaseAccountService purchaseAccountService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param purchaseAccount 采购员工账户
     * @return
     */
    @GetMapping("/page" )
    public R getPurchaseAccountPage(Page page, PurchaseAccount purchaseAccount) {
        return new R<>(purchaseAccountService.page(page, Wrappers.query(purchaseAccount)));
    }


    /**
     * 通过id查询采购员工账户
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(purchaseAccountService.getById(id));
    }

    /**
     * 新增采购员工账户
     * @param purchaseAccount 采购员工账户
     * @return R
     */
    @SysLog("新增采购员工账户" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('purchase_purchaseaccount_add')" )
    public R save(@RequestBody PurchaseAccount purchaseAccount) {
        return new R<>(purchaseAccountService.save(purchaseAccount));
    }

    /**
     * 修改采购员工账户
     * @param purchaseAccount 采购员工账户
     * @return R
     */
    @SysLog("修改采购员工账户" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('purchase_purchaseaccount_edit')" )
    public R updateById(@RequestBody PurchaseAccount purchaseAccount) {
        return new R<>(purchaseAccountService.updateById(purchaseAccount));
    }

    /**
     * 通过id删除采购员工账户
     * @param id id
     * @return R
     */
    @SysLog("删除采购员工账户" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('purchase_purchaseaccount_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(purchaseAccountService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除采购员工账户")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('purchase_purchaseaccount_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(purchaseAccountService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询采购员工账户")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(purchaseAccountService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param purchaseAccountReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询采购员工账户总记录}")
    @PostMapping("/countByPurchaseAccountReq")
    public R countByPurchaseAccountReq(@RequestBody PurchaseAccountReq purchaseAccountReq) {

        return new R<>(purchaseAccountService.count(Wrappers.query(purchaseAccountReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param purchaseAccountReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据purchaseAccountReq查询一条货位信息表")
    @PostMapping("/getOneByPurchaseAccountReq")
    public R getOneByPurchaseAccountReq(@RequestBody PurchaseAccountReq purchaseAccountReq) {

        return new R<>(purchaseAccountService.getOne(Wrappers.query(purchaseAccountReq), false));
    }


    /**
     * 批量修改OR插入
     * @param purchaseAccountList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('purchase_purchaseaccount_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<PurchaseAccount> purchaseAccountList) {

        return new R<>(purchaseAccountService.saveOrUpdateBatch(purchaseAccountList));
    }


}
