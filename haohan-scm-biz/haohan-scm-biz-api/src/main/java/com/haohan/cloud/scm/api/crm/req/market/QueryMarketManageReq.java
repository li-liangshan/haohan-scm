package com.haohan.cloud.scm.api.crm.req.market;

import com.haohan.cloud.scm.api.constant.enums.market.MarketTypeEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

/**
 * @author dy
 * @date 2019/12/20
 */
@Data
public class QueryMarketManageReq {

    @ApiModelProperty(value = "市场类型")
    private MarketTypeEnum marketType;

    // 需处理参数

    @ApiModelProperty(value = "区域编码")
    @Length(min = 0, max = 32, message = "区域编码长度在0至32之间")
    private String areaSn;

    @ApiModelProperty(value = "市场名称")
    @Length(min = 0, max = 32, message = "市场名称长度在0至32之间")
    private String marketName;


    @ApiModelProperty(value = "标签")
    @Length(min = 0, max = 10, message = "标签长度在0至10之间")
    private String tags;

    @ApiModelProperty(value = "电话")
    @Length(min = 0, max = 20, message = "电话长度在0至20之间")
    private String phone;

    @ApiModelProperty(value = "省")
    @Length(min = 0, max = 32, message = "省长度在0至32之间")
    private String province;

    @ApiModelProperty(value = "市")
    @Length(min = 0, max = 32, message = "市长度在0至32之间")
    private String city;

    @ApiModelProperty(value = "区")
    @Length(min = 0, max = 32, message = "区长度在0至32之间")
    private String district;

    @Length(max = 32, message = "员工ID最大长度32字符")
    @ApiModelProperty(value = "app登陆员工")
    private String employeeId;

}
