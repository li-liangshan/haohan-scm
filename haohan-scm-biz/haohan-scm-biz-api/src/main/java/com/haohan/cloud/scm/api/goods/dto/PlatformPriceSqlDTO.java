package com.haohan.cloud.scm.api.goods.dto;

import com.haohan.cloud.scm.api.common.dto.SelfSqlDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.List;

/**
 * @author dy
 * @date 2020/6/5
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class PlatformPriceSqlDTO  extends SelfSqlDTO {
    private static final long serialVersionUID = 1L;

    /**
     * 定价的平台商家
     */
    @ApiModelProperty(value = "商家id")
    private String pricingPmId;

    /**
     * 使用定价的商家
     */
    @ApiModelProperty(value = "商家id")
    private String merchantId;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "查询时间")
    private LocalDate queryDate;

    @ApiModelProperty(value = "商品id")
    private String goodsId;

    @ApiModelProperty(value = "规格id")
    private String modelId;

    @ApiModelProperty(value = "查询商品列表")
    private List<String> goodsIds;


}
