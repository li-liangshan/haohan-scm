package com.haohan.cloud.scm.api.constant.enums.product;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

@Getter
@AllArgsConstructor
public enum ExitTypeEnum {

    delivery("1", "配送"),
    handle("2", "加工");

    private static final Map<String, ExitTypeEnum> MAP = new HashMap<>(8);

    static {
        for (ExitTypeEnum e : ExitTypeEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static ExitTypeEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

    @EnumValue
    @JsonValue
    private String type;

    private String desc;
}
