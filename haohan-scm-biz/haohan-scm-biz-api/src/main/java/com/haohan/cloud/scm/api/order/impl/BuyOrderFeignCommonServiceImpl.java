package com.haohan.cloud.scm.api.order.impl;

import com.haohan.cloud.scm.api.bill.dto.OrderInfoDTO;
import com.haohan.cloud.scm.api.constant.enums.crm.PayStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.saleb.BuyOrderStatusEnum;
import com.haohan.cloud.scm.api.order.OrderCommonService;
import com.haohan.cloud.scm.api.saleb.feign.BuyOrderFeignService;
import com.haohan.cloud.scm.api.saleb.req.BuyOrderFeignReq;
import com.haohan.cloud.scm.common.tools.exception.ErrorDataException;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author dy
 * @date 2020/3/6
 */
@Service
@AllArgsConstructor
public class BuyOrderFeignCommonServiceImpl implements OrderCommonService {

    private final BuyOrderFeignService buyOrderFeignService;

    @Override
    public OrderInfoDTO fetchOrderInfo(String orderSn) {
        R<OrderInfoDTO> r = buyOrderFeignService.fetchOrderIfo(orderSn, SecurityConstants.FROM_IN);
        if (RUtil.isFailed(r) || null == r.getData()) {
            throw new ErrorDataException("查询采购单详情失败");
        }
        return r.getData();
    }

    /**
     * 订单结算状态修改
     *
     * @param orderSn
     * @param status
     * @return
     */
    @Override
    public boolean updateOrderSettlement(String orderSn, PayStatusEnum status) {
        BuyOrderFeignReq req = new BuyOrderFeignReq();
        req.setBuyOrderSn(orderSn);
        req.setPayStatus(status);
        R<Boolean> r = buyOrderFeignService.updateOrderSettlement(req, SecurityConstants.FROM_IN);
        if (RUtil.isFailed(r) || null == r.getData()) {
            return false;
        }
        return r.getData();
    }

    @Override
    public void orderCompleteShip(String orderSn) {
        // 修改采购单发货状态:待收货
        BuyOrderFeignReq req = new BuyOrderFeignReq();
        req.setBuyOrderSn(orderSn);
        req.setStatus(BuyOrderStatusEnum.arrive);
        R<Boolean> r = buyOrderFeignService.updateOrderStatus(req, SecurityConstants.FROM_IN);
        if (RUtil.isFailed(r)) {
            throw new ErrorDataException("采购订单完成发货时修改状态失败:" + r.getMsg());
        }
    }
}
