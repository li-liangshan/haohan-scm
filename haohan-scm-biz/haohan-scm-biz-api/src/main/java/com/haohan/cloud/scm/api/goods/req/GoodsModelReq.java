/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.goods.req;

import com.haohan.cloud.scm.api.goods.entity.GoodsModel;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 商品规格
 *
 * @author haohan
 * @date 2019-05-28 19:54:41
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "商品规格")
public class GoodsModelReq extends GoodsModel {

    private long pageSize;
    private long pageNo;




}
