package com.haohan.cloud.scm.api.opc.req.payment;

import com.haohan.cloud.scm.api.constant.enums.common.ReviewStatusEnum;
import com.haohan.cloud.scm.api.opc.entity.SupplierPayment;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @author dy
 * @date 2019/8/6
 */
@Data
@ApiModel(description = "审核应付账单")
public class ReviewPaymentPayableReq {

    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台商家id", required = true)
    private String pmId;

    @NotBlank(message = "supplierPaymentId不能为空")
    @ApiModelProperty(value = "应付账单编号", required = true)
    private String supplierPaymentId;

    @NotNull(message = "supplierPayment不能为空")
    @ApiModelProperty(value = "应付货款", required = true)
    private BigDecimal supplierPayment;

    @NotNull(message = "reviewStatus不能为空")
    @ApiModelProperty(value = "账单审核状态", required = true)
    private ReviewStatusEnum reviewStatus;

    @ApiModelProperty(value = "备注信息")
    private String remarks;


    public SupplierPayment transTo() {
        SupplierPayment payment = new SupplierPayment();
        payment.setPmId(this.pmId);
        payment.setSupplierPaymentId(this.supplierPaymentId);
        payment.setSupplierPayment(this.supplierPayment);
        payment.setReviewStatus(this.reviewStatus);
        payment.setRemarks(this.remarks);
        return payment;
    }
}
