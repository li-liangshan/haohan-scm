package com.haohan.cloud.scm.product.core;

import com.haohan.cloud.scm.api.product.SingleRealLossDTO;
import com.haohan.cloud.scm.api.product.entity.ProductInfo;
import com.haohan.cloud.scm.api.product.entity.ProductionTask;
import com.haohan.cloud.scm.api.product.entity.SortingOrder;
import com.haohan.cloud.scm.api.product.entity.SortingOrderDetail;
import com.haohan.cloud.scm.api.product.req.CreateDeliveryTaskReq;
import com.haohan.cloud.scm.api.product.req.CreateProcessingTaskReq;
import com.haohan.cloud.scm.api.product.resp.SingleRealLossResp;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseOrderDetail;
import com.haohan.cloud.scm.api.saleb.entity.BuyOrderDetail;

import java.util.List;

/**
 * @author dy
 * @date 2019/5/29
 */
public interface ProductCoreService {

    // 新增货品信息 根据采购单明细
    ProductInfo createProductByPurchaseDetail(PurchaseOrderDetail purchaseOrderDetail);

    // 货品报损/去皮 (拆分为2个 一个正常 一个损耗) 会生成货品耗损记录和加工记录

    // 货品分装 (拆分为多个正常 可有一个耗损)  会生成货品加工记录

    // 货品组合 (按配方 合并为一个 原始货品)   会生成货品加工记录



    // 新增货品分拣明细 根据 客户订单明细
    SortingOrderDetail addSortingDetail(BuyOrderDetail buyOrderDetail);

    // 新增货品分拣单  根据分拣明细
    SortingOrder createSortingOrder(List<SortingOrderDetail> list);

    // 确认货品分拣 数量
    SortingOrderDetail confirmSorting(SortingOrderDetail detail, ProductInfo productInfo);

    //TODO

    /**
     * 新增 生产部任务(任务类别: 加工)  根据 汇总商品需求/预计需求
     *
     * @param req
     * @return
     */
    ProductionTask createProductProcessingTask(CreateProcessingTaskReq req);

    /**
     * 新增 生产部任务(任务类别: 配送) 根据 汇总商品需求
     *
     * @param req
     * @return
     */
    ProductionTask createProductDeliveryTask(CreateDeliveryTaskReq req);

    /**
     * 查询统计 单品实际损耗率
     *
     * @param dto
     * @return
     */
    SingleRealLossResp querySingleRealLoss(SingleRealLossDTO dto);


}
