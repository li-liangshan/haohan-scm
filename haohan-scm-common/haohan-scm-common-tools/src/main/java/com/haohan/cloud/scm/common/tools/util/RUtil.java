package com.haohan.cloud.scm.common.tools.util;

import com.pig4cloud.pigx.common.core.constant.CommonConstants;
import com.pig4cloud.pigx.common.core.util.R;
import lombok.experimental.UtilityClass;

/**
 * @author dy
 * @date 2019/5/16
 */
@UtilityClass
public class RUtil {

    public static <T> R<T> error(String msg) {
        return R.failed(msg);
    }

    public static <T> R<T> error() {
        return R.failed("failed");
    }

    public static <T> R<T> success(T data) {
        return R.ok(data, "success");
    }

    public static boolean isSuccess(R r) {
        return r.getCode() == CommonConstants.SUCCESS;
    }

    public static boolean isFailed(R r) {
        return r.getCode() != CommonConstants.SUCCESS;
    }

}
