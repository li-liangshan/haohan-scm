package com.haohan.cloud.scm.api.purchase.resp;

import com.haohan.cloud.scm.api.constant.enums.common.TaskStatusEnum;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseOrderDetail;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author dy
 * @date 2019/6/12
 */
@Data
@ApiModel(description = "总监查询当前任务列表")
public class PurchaseTaskListByDirectorResp {

    /**
     * 任务状态1.待处理2.执行中3.已完成4.未完成
     */
    @ApiModelProperty(value = "任务状态1.待处理2.执行中3.已完成4.未完成")
    private TaskStatusEnum taskStatus;
    /**
     * 任务截止时间
     */
    @ApiModelProperty(value = "任务截止时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime deadlineTime;

    @ApiModelProperty(value = "任务采购商品明细列表")
    private List<PurchaseOrderDetail> detailList;

}
