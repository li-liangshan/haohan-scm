/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.message.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.message.entity.InMailRecord;
import com.haohan.cloud.scm.api.message.req.InMailRecordReq;
import com.haohan.cloud.scm.message.service.InMailRecordService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 站内信记录表
 *
 * @author haohan
 * @date 2019-05-29 13:47:41
 */
@RestController
@AllArgsConstructor
@RequestMapping("/inmailrecord" )
@Api(value = "inmailrecord", tags = "inmailrecord管理")
public class InMailRecordController {

    private final InMailRecordService inMailRecordService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param inMailRecord 站内信记录表
     * @return
     */
    @GetMapping("/page" )
    public R getInMailRecordPage(Page page, InMailRecord inMailRecord) {
        return new R<>(inMailRecordService.page(page, Wrappers.query(inMailRecord)));
    }


    /**
     * 通过id查询站内信记录表
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(inMailRecordService.getById(id));
    }

    /**
     * 新增站内信记录表
     * @param inMailRecord 站内信记录表
     * @return R
     */
    @SysLog("新增站内信记录表" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_inmailrecord_add')" )
    public R save(@RequestBody InMailRecord inMailRecord) {
        return new R<>(inMailRecordService.save(inMailRecord));
    }

    /**
     * 修改站内信记录表
     * @param inMailRecord 站内信记录表
     * @return R
     */
    @SysLog("修改站内信记录表" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_inmailrecord_edit')" )
    public R updateById(@RequestBody InMailRecord inMailRecord) {
        return new R<>(inMailRecordService.updateById(inMailRecord));
    }

    /**
     * 通过id删除站内信记录表
     * @param id id
     * @return R
     */
    @SysLog("删除站内信记录表" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_inmailrecord_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(inMailRecordService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除站内信记录表")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_inmailrecord_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(inMailRecordService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询站内信记录表")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(inMailRecordService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param inMailRecordReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询站内信记录表总记录}")
    @PostMapping("/countByInMailRecordReq")
    public R countByInMailRecordReq(@RequestBody InMailRecordReq inMailRecordReq) {

        return new R<>(inMailRecordService.count(Wrappers.query(inMailRecordReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param inMailRecordReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据inMailRecordReq查询一条货位信息表")
    @PostMapping("/getOneByInMailRecordReq")
    public R getOneByInMailRecordReq(@RequestBody InMailRecordReq inMailRecordReq) {

        return new R<>(inMailRecordService.getOne(Wrappers.query(inMailRecordReq), false));
    }


    /**
     * 批量修改OR插入
     * @param inMailRecordList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_inmailrecord_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<InMailRecord> inMailRecordList) {

        return new R<>(inMailRecordService.saveOrUpdateBatch(inMailRecordList));
    }


}
