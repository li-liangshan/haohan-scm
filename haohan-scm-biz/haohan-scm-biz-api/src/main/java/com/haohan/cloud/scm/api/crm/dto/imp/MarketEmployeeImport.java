package com.haohan.cloud.scm.api.crm.dto.imp;

import com.haohan.cloud.scm.api.constant.enums.common.UseStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.market.MarketEmployeeTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.market.SexEnum;
import com.haohan.cloud.scm.api.crm.entity.MarketEmployee;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author dy
 * @date 2019/10/11
 */
@Data
public class MarketEmployeeImport {

    @NotBlank(message = "员工姓名不能为空")
    @Length(min = 1, max = 20, message = "员工姓名的字符长度必须在1至20之间")
    @ApiModelProperty(value = "员工姓名")
    private String name;

    @Length(min = 1, max = 20, message = "手机的字符长度必须在1至20之间")
    @ApiModelProperty(value = "手机")
    private String telephone;

    @ApiModelProperty(value = "员工类型:1市场总监2区域经理3业务经理4.社区合伙人")
    private MarketEmployeeTypeEnum employeeType;

    @ApiModelProperty(value = "性别:1男2女")
    private SexEnum sex;

    @Length(min = 0, max = 20, message = "部门的字符长度必须在0至20之间")
    @ApiModelProperty(value = "部门")
    private String department;

    @Length(min = 0, max = 20, message = "职位的字符长度必须在0至20之间")
    @ApiModelProperty(value = "职位")
    private String post;

    @Length(min = 0, max = 64, message = "备注信息的字符长度必须在0至64之间")
    @ApiModelProperty(value = "备注信息")
    private String remarks;


    public MarketEmployee transTo() {
        MarketEmployee employee = new MarketEmployee();
        employee.setName(this.name);
        employee.setTelephone(this.telephone);
        employee.setEmployeeType(this.employeeType);
        employee.setSex(this.sex);
        employee.setPost(this.post);
        employee.setDeptId(this.department);
        // 默认启用
        employee.setUseStatus(UseStatusEnum.enabled);
        return employee;
    }
}
