package com.haohan.cloud.scm.api.product.req;
import com.haohan.cloud.scm.api.constant.enums.product.DeliverySeqEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

/**
 * @author cx
 * @date 2019/6/13
 */
@Data
@ApiModel(description =
 "创建配送生产任务需求")
public class CreateDeliveryTaskReq {

    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台商家Id" , required = true)
    private String pmId;

    @NotBlank(message = "summaryOrderId不能为空")
    @ApiModelProperty(value = "汇总单id" , required = true)
    private String summaryOrderId;

    @ApiModelProperty(value = "执行人id" )
    private String transactorId;

    @ApiModelProperty(value = "发起人id")
    private String initiatorId;

    @ApiModelProperty(value = "任务内容说明")
    private String taskContent;

    @ApiModelProperty(value = "送货批次:0.第一批1.第二批")
    private DeliverySeqEnum deliverySeq;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "任务截止时间", example = "2000-01-01 12:00:00")
    private LocalDateTime deadlineTime;
}
