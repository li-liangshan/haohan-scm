package com.haohan.cloud.scm.api.wms.req;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author xwx
 * @date 2019/7/17
 */
@Data
public class ExitWarehouseParam {
    /**
     * 商品规格id
     */
    private String goodsModelId;
    /**
     * 出库货品数量
     */
    private BigDecimal productNumber;
    /**
     * 出库单明细编号
     */
    private String exitWarehouseDetailSn;
}
