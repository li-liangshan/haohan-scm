/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.crm.req;

import com.haohan.cloud.scm.api.crm.entity.DataReport;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 数据汇报
 *
 * @author haohan
 * @date 2019-08-30 11:46:08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "数据汇报")
public class DataReportReq extends DataReport {

    private long pageSize;
    private long pageNo;


}
