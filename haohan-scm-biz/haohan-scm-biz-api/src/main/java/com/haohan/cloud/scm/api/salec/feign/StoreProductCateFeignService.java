/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.salec.feign;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.salec.entity.StoreProductCate;
import com.haohan.cloud.scm.api.salec.req.StoreProductCateReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 产品分类辅助表内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "StoreProductCateFeignService", value = ScmServiceName.SCM_BIZ_SALEC)
public interface StoreProductCateFeignService {


  /**
   * 通过id查询产品分类辅助表
   *
   * @param id id
   * @return R
   */
  @RequestMapping(value = "/api/feign/StoreProductCate/{id}", method = RequestMethod.GET)
  R<StoreProductCate> getById(@PathVariable("id") Integer id, @RequestHeader(SecurityConstants.FROM) String from);


  /**
   * 分页查询 产品分类辅助表 列表信息
   *
   * @param storeProductCateReq 请求对象
   * @return
   */
  @PostMapping("/api/feign/StoreProductCate/fetchStoreProductCatePage")
  R<Page<StoreProductCate>> getStoreProductCatePage(@RequestBody StoreProductCateReq storeProductCateReq, @RequestHeader(SecurityConstants.FROM) String from);


  /**
   * 全量查询 产品分类辅助表 列表信息
   *
   * @param storeProductCateReq 请求对象
   * @return
   */
  @PostMapping("/api/feign/StoreProductCate/fetchStoreProductCateList")
  R<List<StoreProductCate>> getStoreProductCateList(@RequestBody StoreProductCateReq storeProductCateReq, @RequestHeader(SecurityConstants.FROM) String from);

  /**
   * 新增产品分类辅助表
   *
   * @param storeProductCate 产品分类辅助表
   * @return R
   */
  @PostMapping("/api/feign/StoreProductCate/add")
  R<Boolean> save(@RequestBody StoreProductCate storeProductCate, @RequestHeader(SecurityConstants.FROM) String from);


  /**
   * 修改产品分类辅助表
   *
   * @param storeProductCate 产品分类辅助表
   * @return R
   */
  @PostMapping("/api/feign/StoreProductCate/update")
  R<Boolean> updateById(@RequestBody StoreProductCate storeProductCate, @RequestHeader(SecurityConstants.FROM) String from);


  /**
   * 通过id删除产品分类辅助表
   *
   * @param id id
   * @return R
   */
  @PostMapping("/api/feign/StoreProductCate/delete/{id}")
  R<Boolean> removeById(@PathVariable("id") Integer id, @RequestHeader(SecurityConstants.FROM) String from);

  /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
  @PostMapping("/api/feign/StoreProductCate/batchDelete")
  R<Boolean> removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


  /**
   * 批量查询（根据IDS）
   *
   * @param idList 主键ID列表
   * @return R
   */
  @PostMapping("/api/feign/StoreProductCate/listByIds")
  R<List<StoreProductCate>> listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


  /**
   * 根据 Wrapper 条件，查询总记录数
   *
   * @param storeProductCateReq 实体对象,可以为空
   * @return R
   */
  @PostMapping("/api/feign/StoreProductCate/countByStoreProductCateReq")
  R<Integer> countByStoreProductCateReq(@RequestBody StoreProductCateReq storeProductCateReq, @RequestHeader(SecurityConstants.FROM) String from);


  /**
   * 根据对象条件，查询一条记录
   *
   * @param storeProductCateReq 实体对象,可以为空
   * @return R
   */
  @PostMapping("/api/feign/StoreProductCate/getOneByStoreProductCateReq")
  R<StoreProductCate> getOneByStoreProductCateReq(@RequestBody StoreProductCateReq storeProductCateReq, @RequestHeader(SecurityConstants.FROM) String from);


  /**
   * 批量修改OR插入
   *
   * @param storeProductCateList 实体对象集合 大小不超过1000条数据
   * @return R
   */
  @PostMapping("/api/feign/StoreProductCate/saveOrUpdateBatch")
  R<Boolean> saveOrUpdateBatch(@RequestBody List<StoreProductCate> storeProductCateList, @RequestHeader(SecurityConstants.FROM) String from);


}
