/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.crm.req;

import com.haohan.cloud.scm.api.crm.entity.Customer;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 客户基础信息
 *
 * @author haohan
 * @date 2019-08-30 11:45:29
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "客户基础信息")
public class CustomerReq extends Customer {

    private long pageSize;
    private long pageNo;


}
