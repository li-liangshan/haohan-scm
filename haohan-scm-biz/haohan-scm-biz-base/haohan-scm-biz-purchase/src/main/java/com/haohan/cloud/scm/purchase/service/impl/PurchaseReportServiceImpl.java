/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.purchase.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseReport;
import com.haohan.cloud.scm.purchase.mapper.PurchaseReportMapper;
import com.haohan.cloud.scm.purchase.service.PurchaseReportService;
import org.springframework.stereotype.Service;

/**
 * 采购汇报
 *
 * @author haohan
 * @date 2019-05-13 18:52:53
 */
@Service
public class PurchaseReportServiceImpl extends ServiceImpl<PurchaseReportMapper, PurchaseReport> implements PurchaseReportService {

}
