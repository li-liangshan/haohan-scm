/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.supply.req;

import com.haohan.cloud.scm.api.supply.entity.SupplierEvaluate;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 供应商评价记录
 *
 * @author haohan
 * @date 2019-05-29 13:13:19
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "供应商评价记录")
public class SupplierEvaluateReq extends SupplierEvaluate {

    private long pageSize=10;
    private long pageNo=1;

}
