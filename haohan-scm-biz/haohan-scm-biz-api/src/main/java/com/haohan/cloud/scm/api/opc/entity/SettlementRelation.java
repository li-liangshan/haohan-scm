/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.opc.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.haohan.cloud.scm.api.constant.enums.common.SettlementTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

/**
 * 结算单账单关系表   已废弃，使用bill模块
 *
 * @author haohan
 * @date 2019-09-18 17:36:02
 */
@Data
@TableName("scm_settlement_relation")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "结算单账单关系表")
public class SettlementRelation extends Model<SettlementRelation> {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 结算记录id
     */
    @ApiModelProperty(value = "结算记录id")
    private String settlementId;
    /**
     * 结算记录编号
     */
    @ApiModelProperty(value = "结算记录编号")
    private String settlementSn;
    /**
     * 账单id
     */
    @ApiModelProperty(value = "账单id")
    private String paymentId;
    /**
     * 账单编号
     */
    @ApiModelProperty(value = "账单编号")
    private String paymentSn;
    /**
     * 结算账单类型:1.应收 2.应付
     */
    @ApiModelProperty(value = "结算账单类型:1.应收 2.应付")
    private SettlementTypeEnum settlementType;
    /**
     * 创建者
     */
    @ApiModelProperty(value = "创建者")
    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
     * 更新者
     */
    @ApiModelProperty(value = "更新者")
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.UPDATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateDate;
    /**
     * 备注信息
     */
    @ApiModelProperty(value = "备注信息")
    private String remarks;
    /**
     * 删除标记
     */
    @ApiModelProperty(value = "删除标记")
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;
    /**
     * 租户id
     */
    @ApiModelProperty(value = "租户id")
    private Integer tenantId;

}
