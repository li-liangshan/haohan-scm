/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.manage.req;

import com.haohan.cloud.scm.api.manage.entity.MerchantEmployee;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 员工管理
 *
 * @author haohan
 * @date 2019-05-28 20:35:25
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "员工管理")
public class MerchantEmployeeReq extends MerchantEmployee {

    private long pageSize;
    private long pageNo;




}
