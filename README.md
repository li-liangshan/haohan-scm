# haohan-scm

#### 介绍
基于pig微服务架构打造 供应链系统，采购配送系统。为客户提供仓储管理、订单管理、打单、货源采购、分拣、配送等系统功能。
<br/>
演示地址： http://pds.haohanwork.com  <br/>
租户：北京分公司  <br/>
账号密码：pds/123456  scm/123456

#### 系统截图
![系统截图](image/sys-01.png)
![系统截图](image/sys-02.png)
![系统截图](image/sys-03.png)
#### 软件架构
采用 J2EE 技术体系，基于Spring Cloud微服务框架进行封装，平台设计灵活可扩展、可移植、可应对高并发需求。同时兼顾本地化、私有云、公有云部署，支持SaaS模式应用。
开发框架：平台底层应用的基础服务，是一个微服务系统运行所必要的组件服务。平台提供较好的兼容性，可根据需要选择不同的基础组件，如注册中心、配置中心、分布式事务等。
辅助开发包：主要针对开发人员进行技术开发支持，提供一系列通用的开发工具包，定义了基础工具类，如配置、缓存、路由、发号器等工具，减少开发人员重复造轮子，帮助提高代码编写效率。
通用服务：主要指平台中已包含的开发业务系统所需要的基础服务，如分布式调度、消息、权限、文档、支付管理等，能快速适配各产品线、各业务系统的通用基础功能需求，帮助提高开发效率。

 **技术栈：** <br/>
开发框架：Spring Boot 2.4 <br/>
微服务框架：Spring Cloud 2020<br/>
安全框架：Spring Security + Spring OAuth 2.0<br/>
任务调度：Quartz 、 XXL-JOB<br/>
持久层框架：MyBatis Plus<br/>
数据库连接池：Druid<br/>
服务注册与发现: Nacos<br/>
客户端负载均衡：Ribbon<br/>
熔断组件：Sentinel<br/>
网关组件：Spring Cloud Gateway<br/>
日志管理：Logback<br/>


#### 安装教程

1.  安装redis、mysql、rocketmq 
2.  导入数据库脚本
3.  启动微服务
4.  启动应用，shell目录下，startScm.sh

详情加客服微信了解：<br/>
![输入图片说明](https://images.gitee.com/uploads/images/2020/0805/143525_be83aa45_7837037.png "Wechat-logo.png")


#### 使用说明
1. 已开源的代码，授权协议采用 AGPL v3 + Apache Licence v2 进行发行。
2. 您可以免费使用、修改和衍生代码，但不允许修改后和衍生的代码做为闭源软件发布。
3. 修改后和衍生的代码必须也按照AGPL协议进行流通，对修改后和衍生的代码必须向社会公开。
4. 如果您修改了代码，需要在被修改的文件中进行说明，并遵守代码格式规范，帮助他人更好的理解您的用意。
5. 在延伸的代码中（修改和有源代码衍生的代码中）需要带有原来代码中的协议、版权声明和其他原作者规定。
6. 您可以应用于商业软件，但必须遵循以上条款原则（请协助改进本作品）。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 补充说明

联系客服获取完整sql，指导安装。 开源版本不支持商业使用，商业版需单独授权。更多了解 [浩瀚云供应链](http://www.haohanscm.com/demo)

