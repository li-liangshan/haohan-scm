package com.haohan.cloud.scm.api.constant.enums.crm;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dy
 * @date 2019/12/10
 */
@Getter
@AllArgsConstructor
public enum DeliveryGoodsTypeEnum implements IBaseEnum {

    /**
     * 配件类型 1普通件 2急件
     */
    normal("1", "普通件"),
    urgent("2", "急件");

    private static final Map<String, DeliveryGoodsTypeEnum> MAP = new HashMap<>(8);

    static {
        for (DeliveryGoodsTypeEnum e : DeliveryGoodsTypeEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static DeliveryGoodsTypeEnum getByType(String type) {
        return MAP.get(type);
    }

    @EnumValue
    @JsonValue
    private String type;
    private String desc;
}
