/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.manage.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.manage.entity.PhotoGallery;
import com.haohan.cloud.scm.manage.mapper.PhotoGalleryMapper;
import com.haohan.cloud.scm.manage.service.PhotoGalleryService;
import org.springframework.stereotype.Service;

/**
 * 资源图片库
 *
 * @author haohan
 * @date 2019-05-13 17:32:48
 */
@Service
public class PhotoGalleryServiceImpl extends ServiceImpl<PhotoGalleryMapper, PhotoGallery> implements PhotoGalleryService {

}
