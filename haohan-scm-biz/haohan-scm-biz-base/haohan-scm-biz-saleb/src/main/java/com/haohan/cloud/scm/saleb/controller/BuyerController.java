/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.saleb.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.saleb.entity.Buyer;
import com.haohan.cloud.scm.api.saleb.req.buyer.BuyerEditReq;
import com.haohan.cloud.scm.api.saleb.req.buyer.BuyerQueryReq;
import com.haohan.cloud.scm.api.saleb.vo.BuyerVO;
import com.haohan.cloud.scm.common.tools.constant.FirstGroup;
import com.haohan.cloud.scm.common.tools.constant.SecondGroup;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.haohan.cloud.scm.saleb.core.BuyerCoreService;
import com.haohan.cloud.scm.saleb.service.BuyerService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


/**
 * 采购商
 *
 * @author haohan
 * @date 2019-05-29 13:28:25
 */
@RestController
@AllArgsConstructor
@RequestMapping("/buyer" )
@Api(value = "buyer", tags = "buyer管理")
public class BuyerController {

    private final BuyerService buyerService;
    private final BuyerCoreService buyerCoreService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param req 采购商
     * @return
     */
    @GetMapping("/page" )
    public R<IPage<BuyerVO>> getBuyerPage(Page<Buyer> page, BuyerQueryReq req) {
        return RUtil.success(buyerCoreService.findPage(page, req));
    }


    /**
     * 通过id查询采购商
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(buyerService.getById(id));
    }

    /**
     * 新增采购商
     * @param req 采购商
     * @return R
     */
    @SysLog("新增采购商" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_buyer_add')" )
    public R<Boolean> save(@RequestBody @Validated(FirstGroup.class) BuyerEditReq req) {
        return RUtil.success(buyerCoreService.addBuyer(req));
    }

    /**
     * 修改采购商
     * @param req 采购商
     * @return R
     */
    @SysLog("修改采购商" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_buyer_edit')" )
    public R<Boolean> updateById(@RequestBody @Validated(SecondGroup.class) BuyerEditReq req) {
        return RUtil.success(buyerCoreService.modifyBuyer(req));
    }

    /**
     * 通过id删除采购商
     * @param id id
     * @return R
     */
    @SysLog("删除采购商" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_buyer_del')" )
    public R<Boolean> removeById(@PathVariable String id) {
        return RUtil.success(buyerCoreService.deleteBuyer(id));
    }

}
