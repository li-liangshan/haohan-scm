/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.wms.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.constant.NumberPrefixConstant;
import com.haohan.cloud.scm.api.wms.entity.WarehouseInventory;
import com.haohan.cloud.scm.common.tools.util.ScmIncrementUtil;
import com.haohan.cloud.scm.wms.mapper.WarehouseInventoryMapper;
import com.haohan.cloud.scm.wms.service.WarehouseInventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

/**
 * 库存盘点
 *
 * @author haohan
 * @date 2019-05-13 21:32:04
 */
@Service
public class WarehouseInventoryServiceImpl extends ServiceImpl<WarehouseInventoryMapper, WarehouseInventory> implements WarehouseInventoryService {

    @Autowired
    @Lazy
    private ScmIncrementUtil scmIncrementUtil;

    @Override
    public boolean save(WarehouseInventory entity) {
        if (StrUtil.isEmpty(entity.getWarehouseInventorySn())) {
            String sn = scmIncrementUtil.inrcSnByClass(WarehouseInventory.class, NumberPrefixConstant.WAREHOUSE_INVENTORY_SN_PRE);
            entity.setWarehouseInventorySn(sn);
        }
        return super.save(entity);
    }

}
