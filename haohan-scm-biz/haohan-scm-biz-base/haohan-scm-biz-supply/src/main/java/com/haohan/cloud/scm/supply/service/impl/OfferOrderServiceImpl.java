/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.supply.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.constant.NumberPrefixConstant;
import com.haohan.cloud.scm.api.constant.enums.supply.PdsOfferStatusEnum;
import com.haohan.cloud.scm.api.supply.entity.OfferOrder;
import com.haohan.cloud.scm.common.tools.util.ScmIncrementUtil;
import com.haohan.cloud.scm.supply.mapper.OfferOrderMapper;
import com.haohan.cloud.scm.supply.service.OfferOrderService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 报价单
 *
 * @author haohan
 * @date 2019-05-13 17:14:26
 */
@Service
@AllArgsConstructor
public class OfferOrderServiceImpl extends ServiceImpl<OfferOrderMapper, OfferOrder> implements OfferOrderService {

    private final ScmIncrementUtil scmIncrementUtil;

    @Override
    public boolean save(OfferOrder entity) {
        if (StrUtil.isEmpty(entity.getOfferOrderId())) {
            String sn = scmIncrementUtil.inrcSnByClass(OfferOrder.class, NumberPrefixConstant.OFFER_ORDER_SN_PRE);
            entity.setOfferOrderId(sn);
        }
        return retBool(baseMapper.insert(entity));
    }

    /**
     * 根据采购单明细编号列表  查询 已中标的报价单
     *
     * @param buyDetailSnList
     * @return
     */
    @Override
    public List<OfferOrder> findListByBuyDetailSn(List<String> buyDetailSnList) {
        if (buyDetailSnList.isEmpty()) {
            return new ArrayList<>();
        }
        return baseMapper.selectList(Wrappers.<OfferOrder>query().lambda()
                .in(OfferOrder::getBuyDetailSn, buyDetailSnList)
                .eq(OfferOrder::getStatus, PdsOfferStatusEnum.bidding)
        );
    }
}
