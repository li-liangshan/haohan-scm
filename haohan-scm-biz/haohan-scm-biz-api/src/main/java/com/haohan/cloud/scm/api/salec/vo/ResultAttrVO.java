package com.haohan.cloud.scm.api.salec.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author dy
 * @date 2020/6/9
 * StoreProductAttrResult result json串中使用
 * 商品类型属性
 */
@Data
public class ResultAttrVO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "属性名", notes = "商品类型名称")
    private String value;

    @ApiModelProperty(value = "未知")
    private String detailValue;

    @ApiModelProperty(value = "未知")
    private Boolean attrHidden;

    @ApiModelProperty(value = "商品类型下规格名称")
    private List<String> detail;


}
