package com.haohan.cloud.scm.api.constant.enums.purchase;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xwx
 * @date 2019/5/30
 */
@Getter
@AllArgsConstructor
public enum BiddingStatusEnum {

    /**
     * 中标状态:0.不开标1.已中标2.落标
     */
    noBidding("0","不开标"),
    alreadyBidding("1","已中标"),
    dropBidding("2","落标");

    private static final Map<String, BiddingStatusEnum> MAP = new HashMap<>(8);

    static {
        for (BiddingStatusEnum e : BiddingStatusEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static BiddingStatusEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

    /**
     * 类型
     */
    @EnumValue
    @JsonValue
    private String type;
    /**
     * 描述
     */

    private String desc;
}
