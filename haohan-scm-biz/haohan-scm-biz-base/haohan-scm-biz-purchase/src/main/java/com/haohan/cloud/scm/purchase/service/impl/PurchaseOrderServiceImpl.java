/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.purchase.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.constant.NumberPrefixConstant;
import com.haohan.cloud.scm.api.purchase.dto.PurchaseCateCountDTO;
import com.haohan.cloud.scm.api.purchase.dto.PurchaseTodayDTO;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseOrder;
import com.haohan.cloud.scm.api.purchase.req.CountPurchaseOrderReq;
import com.haohan.cloud.scm.common.tools.util.ScmIncrementUtil;
import com.haohan.cloud.scm.purchase.mapper.PurchaseOrderMapper;
import com.haohan.cloud.scm.purchase.service.PurchaseOrderService;
import com.haohan.cloud.scm.purchase.utils.ScmPurchaseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;

/**
 * 采购部采购单
 *
 * @author haohan
 * @date 2019-05-13 18:48:11
 */
@Service
public class PurchaseOrderServiceImpl extends ServiceImpl<PurchaseOrderMapper, PurchaseOrder> implements PurchaseOrderService {
    @Autowired
    @Lazy
    private PurchaseOrderMapper purchaseOrderMapper;
    @Autowired
    @Lazy
    private ScmIncrementUtil scmIncrementUtil;
    @Autowired
    @Lazy
    private ScmPurchaseUtils scmPurchaseUtils;
    @Override
    public boolean save(PurchaseOrder entity) {
        if (StrUtil.isEmpty(entity.getPurchaseSn())) {
            String sn = scmIncrementUtil.inrcSnByClass(PurchaseOrder.class, NumberPrefixConstant.PURCHASE_SN_PRE);
            entity.setPurchaseSn(sn);
        }
        return super.save(entity);
    }

    /**
     * 查询今日采购单数量
     * @param req
     * @return
     */
    @Override
    public Integer countPurchaseOrderNum(CountPurchaseOrderReq req) {
        Integer integer = purchaseOrderMapper.countPurchaseOrderNum(req);
        return integer;
    }

    /**
     * 查询今日采购总额
     * @param dto
     * @return
     */
    @Override
    public BigDecimal queryTodayAmount(PurchaseTodayDTO dto) {
        return purchaseOrderMapper.queryTodayAmount(dto);
    }

    /**
     * 查询已入库采购单
     * @param dto
     * @return
     */
    @Override
    public Integer queryEnterPurchase(PurchaseTodayDTO dto) {
        dto.setEnd(scmPurchaseUtils.todayEnd());
        dto.setStart(scmPurchaseUtils.todayStart());
        dto.setStatus("3");
        return purchaseOrderMapper.queryEnterPurchase(dto);
    }

    /**
     * 查询已入库商品种类
     * @return
     */
    @Override
    public Integer queryEnterGoodsCate(PurchaseTodayDTO dto) {
        if(dto.getStart() == null){
            dto.setStart(scmPurchaseUtils.todayStart().toString());
        }
        if(dto.getEnd() == null) {
            dto.setEnd(scmPurchaseUtils.todayEnd());
        }
        List<String> string = purchaseOrderMapper.quryEnterGoodsCate(dto);
        HashSet<String> cate = new HashSet<>();
        for(String s : string){
            cate.add(s);
        }
        return cate.size();
    }

    /**
     * 查询采购商品分类统计
     * @param pmId
     * @return
     */
    @Override
    public List<PurchaseCateCountDTO> queryPurchaseGoodsCateCount(String pmId) {
        return purchaseOrderMapper.queryPurchaseGoodsCateCount(pmId);
    }


}
