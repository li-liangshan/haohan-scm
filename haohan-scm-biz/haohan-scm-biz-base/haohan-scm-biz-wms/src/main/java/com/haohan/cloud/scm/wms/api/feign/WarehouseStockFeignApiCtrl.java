/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.wms.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.wms.entity.WarehouseStock;
import com.haohan.cloud.scm.api.wms.req.WarehouseStockReq;
import com.haohan.cloud.scm.wms.service.WarehouseStockService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 仓库库存
 *
 * @author haohan
 * @date 2019-08-22 16:12:12
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/warehouseStock")
@Api(value = "warehousestock", tags = "warehousestock-仓库库存内部接口服务}")
public class WarehouseStockFeignApiCtrl {

    private final WarehouseStockService WarehouseStockService;


    /**
     * 通过id查询仓库库存
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    @ApiOperation(value = "通过id查询仓库库存")
    public R getById(@PathVariable("id") String id) {
        return new R<>(WarehouseStockService.getById(id));
    }


    /**
     * 分页查询 仓库库存 列表信息
     * @param WarehouseStockReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchWarehouseStockPage")
    @ApiOperation(value = "分页查询 仓库库存 列表信息")
    public R getWarehouseStockPage(@RequestBody WarehouseStockReq WarehouseStockReq) {
        Page page = new Page(WarehouseStockReq.getPageNo(), WarehouseStockReq.getPageSize());
        WarehouseStock WarehouseStock =new WarehouseStock();
        BeanUtil.copyProperties(WarehouseStockReq, WarehouseStock);

        return new R<>(WarehouseStockService.page(page, Wrappers.query(WarehouseStock)));
    }


    /**
     * 全量查询 仓库库存 列表信息
     * @param WarehouseStockReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchWarehouseStockList")
    @ApiOperation(value = "全量查询 仓库库存 列表信息")
    public R getWarehouseStockList(@RequestBody WarehouseStockReq WarehouseStockReq) {
        WarehouseStock WarehouseStock =new WarehouseStock();
        BeanUtil.copyProperties(WarehouseStockReq, WarehouseStock);

        return new R<>(WarehouseStockService.list(Wrappers.query(WarehouseStock)));
    }


    /**
     * 新增仓库库存
     * @param WarehouseStock 仓库库存
     * @return R
     */
    @Inner
    @SysLog("新增仓库库存")
    @PostMapping("/add")
    @ApiOperation(value = "新增仓库库存")
    public R save(@RequestBody WarehouseStock WarehouseStock) {
        return new R<>(WarehouseStockService.save(WarehouseStock));
    }

    /**
     * 修改仓库库存
     * @param WarehouseStock 仓库库存
     * @return R
     */
    @Inner
    @SysLog("修改仓库库存")
    @PostMapping("/update")
    @ApiOperation(value = "修改仓库库存")
    public R updateById(@RequestBody WarehouseStock WarehouseStock) {
        return new R<>(WarehouseStockService.updateById(WarehouseStock));
    }

    /**
     * 通过id删除仓库库存
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除仓库库存")
    @PostMapping("/delete/{id}")
    @ApiOperation(value = "通过id删除仓库库存")
    public R removeById(@PathVariable String id) {
        return new R<>(WarehouseStockService.removeById(id));
    }

    /**
     * 删除（根据ID 批量删除)
     * @param idList 主键ID列表
     * @return R
     */
    @Inner
    @SysLog("批量删除仓库库存")
    @PostMapping("/batchDelete")
    @ApiOperation(value = "通过IDS批量删除仓库库存")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(WarehouseStockService.removeByIds(idList));
    }


    /**
     * 批量查询（根据IDS）
     * @param idList 主键ID列表
     * @return R
     */
    @Inner
    @SysLog("根据IDS批量查询仓库库存")
    @PostMapping("/listByIds")
    @ApiOperation(value = "根据IDS批量查询仓库库存")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(WarehouseStockService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param WarehouseStockReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询仓库库存总记录")
    @PostMapping("/countByWarehouseStockReq")
    @ApiOperation(value = "查询仓库库存总记录")
    public R countByWarehouseStockReq(@RequestBody WarehouseStockReq WarehouseStockReq) {

        return new R<>(WarehouseStockService.count(Wrappers.query(WarehouseStockReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param WarehouseStockReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据WarehouseStockReq查询一条仓库库存信息")
    @PostMapping("/getOneByWarehouseStockReq")
    @ApiOperation(value = "根据WarehouseStockReq查询一条仓库库存信息")
    public R getOneByWarehouseStockReq(@RequestBody WarehouseStockReq WarehouseStockReq) {

        return new R<>(WarehouseStockService.getOne(Wrappers.query(WarehouseStockReq), false));
    }


    /**
     * 批量修改OR插入仓库库存
     * @param WarehouseStockList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入仓库库存数据")
    @PostMapping("/saveOrUpdateBatch")
    @ApiOperation(value = "批量修改OR插入仓库库存数据")
    public R saveOrUpdateBatch(@RequestBody List<WarehouseStock> WarehouseStockList) {

        return new R<>(WarehouseStockService.saveOrUpdateBatch(WarehouseStockList));
    }

}

