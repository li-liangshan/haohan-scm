package com.haohan.cloud.scm.api.crm.dto;

import com.haohan.cloud.scm.api.constant.enums.crm.DataReportStatusEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @author dy
 * @date 2019/9/30
 */
@Data
public class ReportStockDTO {

    @ApiModelProperty(value = "客户编码")
    private String customerSn;

    @ApiModelProperty(value = "客户名称")
    private String customerName;

    @ApiModelProperty(value = "上报人id(员工)")
    private String reportManId;

    @ApiModelProperty(value = "上报人名称")
    private String reportMan;

    @ApiModelProperty(value = "上报日期/销售日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate reportDate;

    @ApiModelProperty(value = "上报状态0待确认1确认")
    private DataReportStatusEnum reportStatus;

    @ApiModelProperty(value = "上报编号")
    private String reportSn;

    @ApiModelProperty(value = "商品规格id")
    private String goodsModelId;


    @ApiModelProperty(value = "商品名称")
    private String goodsName;

    @ApiModelProperty(value = "规格属性")
    private String modelAttr;

    @ApiModelProperty(value = "单位")
    private String goodsUnit;

    @ApiModelProperty(value = "商品图片")
    private String goodsImg;

    @ApiModelProperty(value = "商品规格编号")
    private String modelSn;

    @ApiModelProperty(value = "条形码")
    private String modelCode;


    @ApiModelProperty(value = "保质期")
    private String expiration;

    @ApiModelProperty(value = "生产日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime productTime;

    @ApiModelProperty(value = "到期日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime maturityTime;

    @ApiModelProperty(value = "批发价")
    private BigDecimal tradePrice;

    @ApiModelProperty(value = "数量")
    private BigDecimal goodsNum;

    @ApiModelProperty(value = "金额")
    private BigDecimal amount;

}
