package com.haohan.cloud.scm.api.purchase.feign;

import com.haohan.cloud.scm.api.common.req.PdsBuyerGoodsReq;
import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseOrderDetail;
import com.haohan.cloud.scm.api.purchase.req.*;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * @author dy
 * @date 2019/5/16
 */
@FeignClient(contextId = "purchaseFeignService", value = ScmServiceName.SCM_BIZ_PURCHASE)
public interface PurchaseFeignService {


  /**
   * 通过id查询采购员工管理
   *
   * @param id
   * @return R
   */
  @GetMapping("/api/feign/test/{id}")
  R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

  @PostMapping("/api/feign/test/list")
  R callTest(@RequestBody PdsBuyerGoodsReq goodsReq, @RequestHeader(SecurityConstants.FROM) String from);

  /**
   * 分页调用
   *
   * @param purchaseEmployee
   * @param from
   * @return
   */
  @PostMapping("/api/feign/test/page2")
  R getPage(@RequestBody PurchaseEmployeeReq purchaseEmployee, @RequestHeader(SecurityConstants.FROM) String from);


  /**
   * 账户扣减记录 分页查询
   *
   * @param req
   * @return
   */
  @PostMapping("/dealRecord/page")
  R getDealRecordPage(@RequestBody DealRecordReq req, @RequestHeader(SecurityConstants.FROM) String from);

  /**
   * 通过id查询账户扣减记录
   *
   * @param id id
   * @return R
   */
  @PostMapping("/dealRecord/{id}")
  R getDealRecordById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

  /**
   * 放款申请记录 分页查询
   *
   * @param req
   * @return
   */
  @PostMapping("/lendingRecord/page")
  R getLendingRecordPage(@RequestBody LendingRecordReq req, @RequestHeader(SecurityConstants.FROM) String from);

  /**
   * 通过id查询放款申请记录
   *
   * @param id id
   * @return R
   */
  @PostMapping("/lendingRecord/{id}")
  R getLendingRecordById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

  /**
   * 市场行情价格记录 分页查询
   *
   * @param req
   * @return
   */
  @PostMapping("/marketRecord/page")
  R getMarketRecordPage(@RequestBody MarketRecordReq req, @RequestHeader(SecurityConstants.FROM) String from);

  /**
   * 通过id查询市场行情价格记录
   *
   * @param id id
   * @return R
   */
  @PostMapping("/marketRecord/{id}")
  R getMarketRecordById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

  /**
   * 采购员工账户 分页查询
   *
   * @param req
   * @return
   */
  @PostMapping("/purchaseAccount/page")
  R getPurchaseAccountPage(@RequestBody PurchaseAccountReq req, @RequestHeader(SecurityConstants.FROM) String from);

  /**
   * 通过id查询采购员工账户
   *
   * @param id id
   * @return R
   */
  @PostMapping("/purchaseAccount/{id}")
  R getPurchaseAccountById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

  /**
   * 查询采购部采购单明细
   * @param purchaseOrderDetail
   * @param from
   * @return
   */
  @PostMapping("/api/feign/purchase/purchaseOrderDetail/queryOne")
  R queryPurchaseOrderDetail(@RequestBody PurchaseOrderDetail purchaseOrderDetail, @RequestHeader(SecurityConstants.FROM) String from);




}

