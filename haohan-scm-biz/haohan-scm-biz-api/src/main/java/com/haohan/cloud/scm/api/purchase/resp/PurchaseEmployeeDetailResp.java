package com.haohan.cloud.scm.api.purchase.resp;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author dy
 * @date 2019/5/16
 */
@Data
public class PurchaseEmployeeDetailResp {
    /**
     * 主键
     */
    private String id;
    /**
     * 名称
     */
    private String name;
    /**
     * 联系电话
     */
    private String telephone;
    /**
     * 采购员工类型:1采购总监2采购经理3采购员
     */
    private String purchaseEmployeeType;
    /**
     * 启用状态:0.未启用1.启用
     */
    private String useStatus;

    /**
     * 账户余额
     */
    private BigDecimal accountBalance;

    /**
     * 账户状态1.使用中2.已冻结3.已停用
     */
    private String accountStatus;

}
