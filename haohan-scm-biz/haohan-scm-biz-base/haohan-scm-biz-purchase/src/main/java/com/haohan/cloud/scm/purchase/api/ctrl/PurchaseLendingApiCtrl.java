package com.haohan.cloud.scm.purchase.api.ctrl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.purchase.req.PurchaseAddLendingReq;
import com.haohan.cloud.scm.api.purchase.req.PurchaseAuditLendingReq;
import com.haohan.cloud.scm.api.purchase.req.PurchaseQueryLendingListReq;
import com.haohan.cloud.scm.api.purchase.req.PurchaseQueryLendingReq;
import com.haohan.cloud.scm.api.purchase.resp.PurchaseQueryLendingResp;
import com.haohan.cloud.scm.purchase.core.IScmPurchaseLendingService;
import com.pig4cloud.pigx.common.core.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @author dy
 * @date 2019/5/16
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/purchaseLending")
@Api(value = "ApiPurchaseLending", tags = "purchaseLending采购请款")
public class PurchaseLendingApiCtrl {

    @Autowired
    @Lazy
    private IScmPurchaseLendingService lendingService;

    /**
     * 查询请款记录列表
     *
     * @param req
     * @return
     */
    @GetMapping("/queryLendingList")
    @ApiOperation(value = "请款记录列表",notes = "请款记录列表——小程序")
    public R<Page> queryLendingList(@Validated PurchaseQueryLendingListReq req) {
        return new R(lendingService.queryLendingList(req));
    }

    @PostMapping("/queryLendingRecord")
    @ApiOperation(value = "查询请款记录详情",notes = "查询请款记录详情——小程序")
    public R<PurchaseQueryLendingResp> queryLendingRecord(@Validated PurchaseQueryLendingReq req){
        return new R(lendingService.queryLendingRecord(req));
    }
    /**
     * 新增请款记录
     *
     * @param req
     * @return
     */
    @PostMapping("/addLending")
    public R<Boolean> addLending(@Validated PurchaseAddLendingReq req) {
      return new R(lendingService.addLending(req));
    }
    /**
     * 请款审核
     *
     * @param req
     * @return
     */
    @PostMapping("/auditLending")
    @ApiModelProperty(value = "请款审核", notes = "审核请款——小程序")
    public R<Boolean> auditLending(@RequestBody @Validated PurchaseAuditLendingReq req) {
        return new R(lendingService.auditLending(req));
    }

}
