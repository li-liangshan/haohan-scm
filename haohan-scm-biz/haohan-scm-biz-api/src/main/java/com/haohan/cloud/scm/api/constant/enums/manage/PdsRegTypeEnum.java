package com.haohan.cloud.scm.api.constant.enums.manage;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dy
 * @date 2019/6/10
 */
@Getter
@AllArgsConstructor
public enum PdsRegTypeEnum implements IBaseEnum {
    /**
     * 注册方式 0：login_name；1：telephone；2：email 3：QQ；4：sinaweibo； 5：weixin
     */
    name("0", "login_name"),
    telephone("1", "telephone"),
    email("2", "email"),
    qq("3", "QQ"),
    weibo("4", "sinaweibo"),
    weixin("5", "weixin");

    private static final Map<String, PdsRegTypeEnum> MAP = new HashMap<>(8);

    static {
        for (PdsRegTypeEnum e : PdsRegTypeEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static PdsRegTypeEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

    @EnumValue
    @JsonValue
    private String type;
    private String desc;
}
