/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.purchase.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseTask;
import com.haohan.cloud.scm.api.purchase.req.AuditTaskReq;
import com.haohan.cloud.scm.api.purchase.req.PurchaseTaskReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 采购任务内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "PurchaseTaskFeignService", value = ScmServiceName.SCM_BIZ_PURCHASE)
public interface PurchaseTaskFeignService {


    /**
     * 通过id查询采购任务
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/PurchaseTask/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 采购任务 列表信息
     * @param purchaseTaskReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/PurchaseTask/fetchPurchaseTaskPage")
    R getPurchaseTaskPage(@RequestBody PurchaseTaskReq purchaseTaskReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 采购任务 列表信息
     * @param purchaseTaskReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/PurchaseTask/fetchPurchaseTaskList")
    R getPurchaseTaskList(@RequestBody PurchaseTaskReq purchaseTaskReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增采购任务
     * @param purchaseTask 采购任务
     * @return R
     */
    @PostMapping("/api/feign/PurchaseTask/add")
    R save(@RequestBody PurchaseTask purchaseTask, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改采购任务
     * @param purchaseTask 采购任务
     * @return R
     */
    @PostMapping("/api/feign/PurchaseTask/update")
    R updateById(@RequestBody PurchaseTask purchaseTask, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除采购任务
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/PurchaseTask/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/PurchaseTask/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/PurchaseTask/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param purchaseTaskReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/PurchaseTask/countByPurchaseTaskReq")
    R countByPurchaseTaskReq(@RequestBody PurchaseTaskReq purchaseTaskReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param purchaseTaskReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/PurchaseTask/getOneByPurchaseTaskReq")
    R getOneByPurchaseTaskReq(@RequestBody PurchaseTaskReq purchaseTaskReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param purchaseTaskList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/PurchaseTask/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<PurchaseTask> purchaseTaskList, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 总监 执行采购任务(审核) 修改采购单明细
     * @param req
     * @param from
     * @return
     */
    @PostMapping("/api/feign/PurchaseTask/auditTask")
    R auditTask(@RequestBody AuditTaskReq req, @RequestHeader(SecurityConstants.FROM) String from);


}
