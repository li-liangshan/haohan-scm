/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.pay.req;

import com.haohan.cloud.scm.api.pay.entity.OrderPayRecord;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 支付订单
 *
 * @author haohan
 * @date 2019-07-25 21:31:10
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "支付订单")
public class OrderPayRecordReq extends OrderPayRecord {

    private long pageSize;
    private long pageNo;




}
