/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.opc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haohan.cloud.scm.api.opc.entity.ShipRecordDetail;

/**
 * 发货记录明细
 *
 * @author haohan
 * @date 2020-01-06 14:28:22
 */
public interface ShipRecordDetailMapper extends BaseMapper<ShipRecordDetail> {

}
