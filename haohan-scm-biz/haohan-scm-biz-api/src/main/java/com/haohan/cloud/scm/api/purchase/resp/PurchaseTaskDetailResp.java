package com.haohan.cloud.scm.api.purchase.resp;

import com.haohan.cloud.scm.api.constant.enums.common.TaskStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.purchase.MethodTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.purchase.PayTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.purchase.ReceiveTypeEnum;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author cx
 * @date 2019/6/6
 */
@Data
public class PurchaseTaskDetailResp {

  /**
   * 采购任务编号
   */
  private String id;
  /**
   * 平台商家Id
   */
  private String pmId;
  /**
   * 采购方式
   */
  private MethodTypeEnum methodType;
  /**
   * 商品分类名称
   */
  private String queryPurchaseTask;
  /**
   * 商品名称
   */
  private String goodsName;
  /**
   * 需求数量
   */
  private BigDecimal needBuyNum;
  /**
   * 商品规格名称
   */
  private String goodsModelName;
  /**
   * 商品图片地址
   */
  private String goodsImg;
  /**
   * 采购价格
   */
  private BigDecimal buyPrice;
  /**
   * 任务状态
   */
  private TaskStatusEnum purchaseStatus;
  /**
   * 任务截止时间
   */
  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private LocalDateTime deadlineTime;
  /**
   * 任务操作时间
   */
  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private LocalDateTime actionTime;
  /**
   * 付款方式
   */
  private PayTypeEnum payType;
  /**
   * 取货方式
   */
  private ReceiveTypeEnum receiveType;
  /**
   * 采购经理
   */
  private String managerName;

  private String goodsCategoryName;
}
