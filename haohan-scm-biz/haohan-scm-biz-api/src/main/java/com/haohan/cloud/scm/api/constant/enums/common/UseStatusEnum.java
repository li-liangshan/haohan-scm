package com.haohan.cloud.scm.api.constant.enums.common;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dy
 * @date 2019/5/18
 * 启用状态 0.未启用 1.启用 2.待审核
 */
@Getter
@AllArgsConstructor
public enum UseStatusEnum implements IBaseEnum {
    /**
     * 启用状态:未启用
     */
    disabled("0", "未启用"),
    /**
     * 启用状态:启用
     */
    enabled("1", "启用"),
    /**
     * 待审核
     */
    stayAudit("2","待审核");

    private static final Map<String, UseStatusEnum> MAP = new HashMap<>(8);

    static {
        for (UseStatusEnum e : UseStatusEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static UseStatusEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

    /**
     * 类型
     */
    @EnumValue
    @JsonValue
    private String type;
    /**
     * 描述
     */

    private String desc;

}
