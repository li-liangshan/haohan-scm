package com.haohan.cloud.scm.api.salec.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Map;

/**
 * @author dy
 * @date 2020/6/9
 */
@Data
public class ResultAttrValueVO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "商品规格名称1")
    private String value1;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(value = "商品规格名称2")
    private String value2;

    @ApiModelProperty(value = "此商品的属性信息", notes = "商品类型名称:规格名称")
    private Map<String,String> detail;

    @ApiModelProperty(value = "商品图片")
    private String pic;

    @ApiModelProperty(value = "销售价")
    private BigDecimal price;

    @ApiModelProperty(value = "成本价")
    private BigDecimal cost;

    @JsonProperty(value = "ot_price")
    @ApiModelProperty(value = "原价")
    private BigDecimal otPrice;

    @ApiModelProperty(value = "属性对应的库存")
    private Integer stock;

    @JsonProperty(value = "bar_code")
    @ApiModelProperty(value = "商品条码")
    private String barCode;

    @ApiModelProperty(value = "重量")
    private BigDecimal weight;

    @ApiModelProperty(value = "体积")
    private BigDecimal volume;

    @ApiModelProperty(value = "一级返佣")
    private BigDecimal brokerage;

    @JsonProperty(value = "brokerage_two")
    @ApiModelProperty(value = "二级返佣")
    private BigDecimal brokerageTwo;


}
