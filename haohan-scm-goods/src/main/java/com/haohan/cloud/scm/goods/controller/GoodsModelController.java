/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.goods.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.goods.entity.GoodsModel;
import com.haohan.cloud.scm.api.goods.req.GoodsModelReq;
import com.haohan.cloud.scm.goods.core.IScmGoodsService;
import com.haohan.cloud.scm.goods.service.GoodsModelService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 商品规格
 *
 * @author haohan
 * @date 2019-05-29 14:25:39
 */
@RestController
@AllArgsConstructor
@RequestMapping("/goodsmodel" )
@Api(value = "goodsmodel", tags = "goodsmodel管理")
public class GoodsModelController {

    private final GoodsModelService goodsModelService;

    private final IScmGoodsService scmGoodsService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param goodsModel 商品规格
     * @return
     */
    @GetMapping("/page" )
    public R getGoodsModelPage(Page page, GoodsModel goodsModel) {
        return new R<>(goodsModelService.page(page, Wrappers.query(goodsModel)));
    }


    /**
     * 通过id查询商品规格
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(goodsModelService.getById(id));
    }

    /**
     * 新增商品规格
     * @param goodsModel 商品规格
     * @return R
     */
    @SysLog("新增商品规格" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_goodsmodel_add')" )
    public R save(@RequestBody GoodsModel goodsModel) {
        return R.failed("不支持");
    }

    /**
     * 修改商品规格
     * @param goodsModel 商品规格
     * @return R
     */
    @SysLog("修改商品规格" )
    @PutMapping
    public R updateById(@RequestBody GoodsModel goodsModel) {
        return R.failed("不支持");
    }

    /**
     * 通过id删除商品规格
     * @param id id
     * @return R
     */
    @SysLog("删除商品规格" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_goodsmodel_del')" )
    public R removeById(@PathVariable String id) {
        return R.failed("不支持");
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除商品规格")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_goodsmodel_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return R.failed("不支持");
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询商品规格")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(goodsModelService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param goodsModelReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询商品规格总记录}")
    @PostMapping("/countByGoodsModelReq")
    public R countByGoodsModelReq(@RequestBody GoodsModelReq goodsModelReq) {

        return new R<>(goodsModelService.count(Wrappers.query(goodsModelReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param goodsModelReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据goodsModelReq查询一条货位信息表")
    @PostMapping("/getOneByGoodsModelReq")
    public R getOneByGoodsModelReq(@RequestBody GoodsModelReq goodsModelReq) {

        return new R<>(goodsModelService.getOne(Wrappers.query(goodsModelReq), false));
    }


    /**
     * 批量修改OR插入
     * @param goodsModelList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_goodsmodel_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<GoodsModel> goodsModelList) {

        return R.failed("不支持");
    }

    @GetMapping("/queryGoodsStorageWarn")
    @ApiOperation(value = "查询库存预警")
    public R queryStorageWarn(Page page,GoodsModel model){
        return new R<>(scmGoodsService.queryStorageWarn(page,model));
    }

    @GetMapping("/queryGoodsModelDTOList")
    @ApiOperation(value = "查询商品规格信息列表")
    public R queryGoodsModelDTOList(GoodsModel model){
        return new R<>(scmGoodsService.queryGoodsModelDTOList(model));
    }
}
