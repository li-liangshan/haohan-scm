package com.haohan.cloud.scm.api.crm.vo.analysis;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author dy
 * @date 2019/11/9
 */
@Data
@Api("员工单日数据统计")
@NoArgsConstructor
public class EmployeeDayStatisticsVO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "拜访数")
    private Integer visitNum;

    @ApiModelProperty(value = "新增客户数")
    private Integer customerNum;

    @ApiModelProperty(value = "新增销售订单数")
    private Integer orderNum;

    @ApiModelProperty(value = "日报数")
    private Integer workReportNum;

    @ApiModelProperty(value = "销量上报数")
    private Integer salesReportNum;

    @ApiModelProperty(value = "库存上报数")
    private Integer stockReportNum;

    @ApiModelProperty(value = "竞品上报数")
    private Integer competitionReportNum;

    public EmployeeDayStatisticsVO(EmployeeDailyCountVO dailyCount) {
        this.visitNum = dailyCount.getVisitNum();
        this.customerNum = dailyCount.getCustomerNum();
        this.orderNum = dailyCount.getSalesOrderNum();
        this.salesReportNum = dailyCount.getSalesReportNum();
        this.stockReportNum = dailyCount.getStockReportNum();
        this.competitionReportNum = dailyCount.getCompetitionReportNum();
    }
}
