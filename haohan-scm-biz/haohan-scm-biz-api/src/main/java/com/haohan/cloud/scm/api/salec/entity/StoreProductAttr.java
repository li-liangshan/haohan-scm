/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.salec.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;

/**
 * 商品属性表
 *
 * @author haohan
 * @date 2020-05-21 18:43:29
 */
@Data
@TableName("eb_store_product_attr")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "商品属性表")
public class StoreProductAttr extends Model<StoreProductAttr> {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "商品ID")
    private Integer productId;

    @Length(max = 32, message = "属性名长度最大32字符")
    @ApiModelProperty(value = "属性名", notes = "规格类型名称")
    private String attrName;

    @Length(max = 256, message = "属性值长度最大256字符")
    @ApiModelProperty(value = "属性值", notes = "规格名称列表, 逗号连接")
    private String attrValues;

    @ApiModelProperty(value = "活动类型 0=商品，1=秒杀，2=砍价，3=拼团")
    private Integer type;

    @ApiModelProperty(value = "租户id")
    private Integer tenantId;

}
