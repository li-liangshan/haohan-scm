package com.haohan.cloud.scm.api.opc.dto;

import com.haohan.cloud.scm.api.constant.enums.common.SettlementTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import com.haohan.cloud.scm.api.constant.enums.purchase.PayTypeEnum;
import com.haohan.cloud.scm.api.manage.entity.PhotoGallery;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author dy
 * @date 2019/9/11
 */
@Data
public class SettlementRecordDTO {

    /**
     * 结算记录编号
     */
    private String settlementSn;
    /**
     * 结算类型: 应收/应付
     */
    private SettlementTypeEnum settlementType;
    /**
     * 结算金额
     */
    private BigDecimal settlementAmount;
    /**
     * 结算开始日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime settlementBeginDate;
    /**
     * 结算结束日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime settlementEndDate;
    /**
     * 付款时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime payDate;
    /**
     * 付款方式:1对公转账2现金支付3在线支付4承兑汇票
     */
    private PayTypeEnum payType;
    /**
     * 结算公司类型:采购商/供应商 (未启用)
     */
    private String companyType;
    /**
     * 结算公司id: merchantId
     */
    private String companyId;
    /**
     * 结算公司名称
     */
    private String companyName;
    /**
     * 结款人名称
     */
    private String companyOperator;
    /**
     * 结算说明
     */
    private String settlementDesc;
    /**
     * 是否结算:0否1是
     */
    private YesNoEnum status;
    /**
     * 备注信息
     */
    private String remarks;

    /**
     * 结算凭证 图片组编号
     */
    private String groupNum;
    /**
     * 结算凭证 图片列表
     */
    private List<PhotoGallery> photoList;

    private List<BillPaymentInfoDTO> billList;

}
