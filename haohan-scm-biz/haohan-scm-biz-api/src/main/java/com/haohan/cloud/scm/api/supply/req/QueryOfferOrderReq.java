package com.haohan.cloud.scm.api.supply.req;

import com.haohan.cloud.scm.api.constant.enums.supply.PdsOfferStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.supply.PdsOfferTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author xwx
 * @date 2019/7/25
 */
@Data
@ApiModel(description = "查询报价单列表")
public class QueryOfferOrderReq {
    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台id",required = true)
    private String pmId;

    @NotBlank(message = "uid不能为空")
    @ApiModelProperty(value = "采购员uid",required = true)
    private String uid;

    @ApiModelProperty(value = "报价类型:1平台报价2市场报价3指定报价4货源报价")
    private PdsOfferTypeEnum offerType;

    @ApiModelProperty(value = "报价单状态1待报价2已报价3中标4未中标")
    private PdsOfferStatusEnum status;

    @ApiModelProperty(value = "供应商id")
    private String supplierId;

}
