/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.product.req;

import com.haohan.cloud.scm.api.product.entity.ProductInfo;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 货品信息记录表
 *
 * @author haohan
 * @date 2019-05-28 20:51:23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "货品信息记录表")
public class ProductInfoReq extends ProductInfo {

    private long pageSize;
    private long pageNo;

    private String warehouseSn;
}
