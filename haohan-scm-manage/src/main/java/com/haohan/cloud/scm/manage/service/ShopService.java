/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.manage.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.manage.dto.ShopExtDTO;
import com.haohan.cloud.scm.api.manage.dto.ShopSqlDTO;
import com.haohan.cloud.scm.api.manage.entity.Shop;

import java.util.List;

/**
 * 店铺
 *
 * @author haohan
 * @date 2019-05-13 17:39:04
 */
public interface ShopService extends IService<Shop> {

    Shop fetchPdsShop(String merchantId);

    List<Shop> findEnableList(String merchantId);

    /**
     * 分页查询 联查商家
     *
     * @param query
     * @return
     */
    IPage<ShopExtDTO> findPage(ShopSqlDTO query);
}
