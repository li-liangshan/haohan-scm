package com.haohan.cloud.scm.api.wms.resp;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author xwx
 * @date 2019/7/15
 */
@Data
public class ExitWarehouseDetailResp {
    //出库单明细

    /**
     * 出库货品数量
     */
    private BigDecimal productNumber;
    /**
     * 货品单位
     */
    private String unit;
    //货品信息
    /**
     * 商品id,spu
     */
    private String goodsId;
    /**
     * 货品名称
     */
    private String productName;
    /**
     * 供应商名称
     */
    private String supplierName;
    /**
     * 采购价格/成本价
     */
    private BigDecimal purchasePrice;
    /**
     * 出库明细编号
     */
    private String exitWarehouseDetailSn;

    private String goodsModelId;
}
