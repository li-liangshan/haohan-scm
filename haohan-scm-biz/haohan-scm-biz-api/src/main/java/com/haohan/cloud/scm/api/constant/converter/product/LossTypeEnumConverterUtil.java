package com.haohan.cloud.scm.api.constant.converter.product;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.product.LossTypeEnum;

/**
 * @author xwx
 * @date 2019/6/4
 */
public class LossTypeEnumConverterUtil implements Converter<LossTypeEnum> {
    @Override
    public LossTypeEnum convert(Object o, LossTypeEnum lossTypeEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return LossTypeEnum.getByType(o.toString());
    }
}
