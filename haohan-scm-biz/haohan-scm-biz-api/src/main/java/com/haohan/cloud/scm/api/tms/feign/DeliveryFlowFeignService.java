/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.tms.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.tms.entity.DeliveryFlow;
import com.haohan.cloud.scm.api.tms.req.DeliveryFlowReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 物流配送内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "DeliveryFlowFeignService", value = ScmServiceName.SCM_BIZ_WMS)
public interface DeliveryFlowFeignService {


    /**
     * 通过id查询物流配送
     *
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/DeliveryFlow/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 物流配送 列表信息
     *
     * @param deliveryFlowReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/DeliveryFlow/fetchDeliveryFlowPage")
    R getDeliveryFlowPage(@RequestBody DeliveryFlowReq deliveryFlowReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 物流配送 列表信息
     *
     * @param deliveryFlowReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/DeliveryFlow/fetchDeliveryFlowList")
    R getDeliveryFlowList(@RequestBody DeliveryFlowReq deliveryFlowReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增物流配送
     *
     * @param deliveryFlow 物流配送
     * @return R
     */
    @PostMapping("/api/feign/DeliveryFlow/add")
    R save(@RequestBody DeliveryFlow deliveryFlow, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改物流配送
     *
     * @param deliveryFlow 物流配送
     * @return R
     */
    @PostMapping("/api/feign/DeliveryFlow/update")
    R updateById(@RequestBody DeliveryFlow deliveryFlow, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除物流配送
     *
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/DeliveryFlow/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/DeliveryFlow/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/DeliveryFlow/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param deliveryFlowReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/DeliveryFlow/countByDeliveryFlowReq")
    R countByDeliveryFlowReq(@RequestBody DeliveryFlowReq deliveryFlowReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param deliveryFlowReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/DeliveryFlow/getOneByDeliveryFlowReq")
    R getOneByDeliveryFlowReq(@RequestBody DeliveryFlowReq deliveryFlowReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param deliveryFlowList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/DeliveryFlow/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<DeliveryFlow> deliveryFlowList, @RequestHeader(SecurityConstants.FROM) String from);


}
