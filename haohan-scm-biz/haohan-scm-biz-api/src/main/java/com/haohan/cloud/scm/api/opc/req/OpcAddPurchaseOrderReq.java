package com.haohan.cloud.scm.api.opc.req;

import com.haohan.cloud.scm.api.constant.enums.purchase.MethodTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @author cx
 * @date 2019/6/19
 */
@Data
@ApiModel(description = "运营生成采购单需求")
public class OpcAddPurchaseOrderReq {

    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台商家id",required = true)
    private String pmId;

    @NotBlank(message = "summaryOrderId不能为空")
    @ApiModelProperty(value = "汇总单号",required = true)
    private List<String> summaryOrderId;

    @NotBlank(message = "methodType不能为空")
    @ApiModelProperty(value = "采购方式1.竞价采购2.单品采购3.协议供应",required = true)
    private MethodTypeEnum methodType;


}
