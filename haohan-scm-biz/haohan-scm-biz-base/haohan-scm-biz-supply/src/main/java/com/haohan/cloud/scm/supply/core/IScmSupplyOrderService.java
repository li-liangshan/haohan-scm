package com.haohan.cloud.scm.supply.core;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.goods.entity.GoodsModel;
import com.haohan.cloud.scm.api.supply.entity.OfferOrder;
import com.haohan.cloud.scm.api.supply.req.*;


/**
 * @author  xwx
 * @date 2019/5/15.
 */
public interface IScmSupplyOrderService {

    /**
     * 查询供应商品库存信息列表
     * @param req
     * @return
     */
    Page querySupplyGoodsRepertoryList(QueryGoodsRepertoryListReq req);

    /**
     * 查询供应商品库存详情
     * @param req
     * @return
     */
    GoodsModel querySupplyGoodsRepertory(QueryGoodsRepertoryDetailReq req);

    /**
     * 新增货源报价
     * @param req
     * @return
     */
    OfferOrder addSupplierOffer(OfferOrderReq req);

    /**
     * 新增供应商报价（竞价采购）
     * @param req
     * @return
     */
    OfferOrder addOfferBySupplier(OfferOrderReq req);

    /**
     * 采购员挑选供应商  (新增报价单  单品采购)
     * @param req
     * @return
     */
    OfferOrder addOfferByPurchase(OfferOrderReq req);

    /**
     * 根据供应协议设置供应商 (新增报价单 协议供应)
     * @param req
     * @return
     */
    OfferOrder addOfferByAgreement(OfferOrderReq req);


    /**
     * 初始创建报价单(不包括货源报价)
     * @param req
     *       必需参数: supplierId / supplyPrice / offerType / goodsModelId
     * @return
     */
    OfferOrder addOfferOrder(AddOfferOrderReq req);

    /**
     * 查询供应商货源（加筛选条件）
     * @param req
     * @return
     */
    Page<OfferOrder> filterOfferOrder(FilterOfferOrderReq req);

    /**
     * 供应商报价
     * @param req
     * @return
     */
    Boolean affirmOffer(AffirmOfferReq req);

    /**
     * 修改报价单
     * @param req
     * @return
     */
    Boolean updateOfferOrder(UpdateOfferOrderReq req);
}
