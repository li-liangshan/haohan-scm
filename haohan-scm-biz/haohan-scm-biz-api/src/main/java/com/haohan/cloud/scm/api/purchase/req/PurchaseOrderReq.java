/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.purchase.req;

import com.haohan.cloud.scm.api.purchase.entity.PurchaseOrder;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 采购部采购单
 *
 * @author haohan
 * @date 2019-05-29 13:34:55
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "采购部采购单")
public class PurchaseOrderReq extends PurchaseOrder {

    private long pageSize;
    private long pageNo;




}
