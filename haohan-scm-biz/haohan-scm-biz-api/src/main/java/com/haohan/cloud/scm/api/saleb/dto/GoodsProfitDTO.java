package com.haohan.cloud.scm.api.saleb.dto;

import lombok.Data;

/**
 * @author cx
 * @date 2019/7/11
 */
@Data
public class GoodsProfitDTO {

    private String name;

    private Integer value;
}
