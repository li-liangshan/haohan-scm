package com.haohan.cloud.scm.api.manage.vo;

import cn.hutool.core.util.StrUtil;
import com.haohan.cloud.scm.api.constant.enums.manage.MerchantStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.manage.ShopLevelEnum;
import com.haohan.cloud.scm.api.manage.dto.ShopExtDTO;
import com.haohan.cloud.scm.api.manage.entity.Shop;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @author dy
 * @date 2020/3/30
 */
@Data
@NoArgsConstructor
public class ShopVO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "店铺id")
    private String shopId;

    @ApiModelProperty(value = "店铺名称")
    private String name;

    @ApiModelProperty(value = "店铺地址")
    private String address;

    @ApiModelProperty(value = "店铺负责人名称")
    private String manager;

    @ApiModelProperty(value = "店铺电话")
    private String telephone;

    @ApiModelProperty(value = "地址定位 (经度，纬度)")
    private String position;

    @ApiModelProperty(value = "商家id")
    private String merchantId;

    @ApiModelProperty(value = "商家名称")
    private String merchantName;

    @ApiModelProperty(value = "营业时间")
    private String onlineTime;

    @ApiModelProperty(value = "店铺服务")
    private String shopService;

    @ApiModelProperty(value = "店铺介绍")
    private String shopDesc;

    @ApiModelProperty(value = "店铺启用状态", notes = "原系统使用字典 2启用  0 待审核 -1 停用")
    private MerchantStatusEnum status;

    @ApiModelProperty(value = "店铺等级", notes = "原系统使用字典 0 总店, 1 分店 2采购配送店")
    private ShopLevelEnum shopLevel;

    @ApiModelProperty(value = "行业名称")
    private String industry;

    @ApiModelProperty(value = "备注")
    private String remarks;

    @ApiModelProperty(value = "更新时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateDate;

    @ApiModelProperty(value = "轮播图列表")
    private List<PhotoVO> photoList;

    @ApiModelProperty(value = "店铺收款码列表")
    private List<PhotoVO> payCodeList;

    @ApiModelProperty(value = "店铺二维码列表")
    private List<PhotoVO> qrcodeList;

    @ApiModelProperty(value = "店铺Logo列表")
    private List<PhotoVO> shopLogoList;

    public ShopVO(ShopExtDTO shop) {
        this.copy(shop);
        this.merchantName = shop.getMerchantName();

        this.photoList = shop.getPhotoList() == null ? new ArrayList<>() : shop.getPhotoList();
        this.payCodeList = shop.getPayCodeList() == null ? new ArrayList<>() : shop.getPayCodeList();
        this.qrcodeList = shop.getQrcodeList() == null ? new ArrayList<>() : shop.getQrcodeList();
        this.shopLogoList = shop.getShopLogoList() == null ? new ArrayList<>() : shop.getShopLogoList();
    }

    public ShopVO(Shop shop) {
        this.copy(shop);
    }

    private void copy(Shop shop) {
        this.shopId = shop.getId();
        this.name = shop.getName();
        this.address = shop.getAddress();
        this.manager = shop.getManager();
        this.telephone = shop.getTelephone();
        if (StrUtil.isNotEmpty(shop.getMapLongitude()) && StrUtil.isNotEmpty(shop.getMapLatitude())) {
            this.position = shop.getMapLongitude().concat(StrUtil.COMMA).concat(shop.getMapLatitude());
        }
        this.merchantId = shop.getMerchantId();
        // 缺 merchantName
        this.onlineTime = shop.getOnlineTime();
        this.shopService = shop.getShopService();
        this.shopDesc = shop.getShopDesc();
        this.status = shop.getStatus();
        this.shopLevel = shop.getShopLevel();
        this.industry = shop.getIndustry();
        this.remarks = shop.getRemarks();
        this.updateDate = shop.getUpdateDate();
    }
}
