/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.crm.req;

import com.haohan.cloud.scm.api.crm.entity.SalesReportAnalysisDetail;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 客户销量上报数据统计明细
 *
 * @author haohan
 * @date 2020-01-16 12:08:43
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "客户销量上报数据统计明细")
public class SalesReportAnalysisDetailReq extends SalesReportAnalysisDetail {

    private long pageSize;
    private long pageNo;

}
