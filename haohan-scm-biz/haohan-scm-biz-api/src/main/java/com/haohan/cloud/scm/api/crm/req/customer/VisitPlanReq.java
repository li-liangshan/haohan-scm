package com.haohan.cloud.scm.api.crm.req.customer;

import com.haohan.cloud.scm.api.constant.enums.crm.VisitStepEnum;
import com.haohan.cloud.scm.api.crm.entity.CustomerVisit;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

/**
 * @author dy
 * @date 2019/10/29
 */
@Data
public class VisitPlanReq {

    @NotBlank(message = "客户编号不能为空")
    @Length(max = 32, message = "客户编号长度最大32字符")
    @ApiModelProperty(value = "客户编号")
    private String customerSn;

    @NotBlank(message = "拜访地址不能为空")
    @Length(max = 64, message = "拜访地址长度最大64字符")
    @ApiModelProperty(value = "拜访地址")
    private String visitAddress;

    @NotBlank(message = "拜访联系人id不能为空")
    @Length(max = 32, message = "拜访联系人id长度最大32字符")
    @ApiModelProperty(value = "拜访联系人id")
    private String linkmanId;

    @NotNull(message = "拜访日期不能为空")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "拜访日期")
    private LocalDate visitDate;

    @NotBlank(message = "拜访内容不能为空")
    @Length(max = 255, message = "拜访内容长度最大255字符")
    @ApiModelProperty(value = "拜访内容")
    private String visitContent;

    @NotBlank(message = "拜访员工id不能为空")
    @Length(max = 32, message = "拜访员工id长度最大32字符")
    @ApiModelProperty(value = "拜访员工id")
    private String employeeId;

    @ApiModelProperty(value = "进展阶段:1初次拜访2了解交流3.深入跟进4达成合作5商务往来")
    private VisitStepEnum visitStep;


    public CustomerVisit transTo() {
        CustomerVisit visit = new CustomerVisit();
        visit.setCustomerSn(this.customerSn);
        visit.setVisitAddress(this.visitAddress);
        visit.setLinkmanId(this.linkmanId);
        visit.setVisitDate(this.visitDate);
        visit.setVisitContent(this.visitContent);
        visit.setEmployeeId(this.employeeId);
        if (null == this.visitStep) {
            this.visitStep = VisitStepEnum.first;
        }
        visit.setVisitStep(this.visitStep);
        return visit;
    }
}
