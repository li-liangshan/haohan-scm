package com.haohan.cloud.scm.api.aftersales.trans;

import com.haohan.cloud.scm.api.aftersales.resp.QueryReturnOrderDetailResp;
import com.haohan.cloud.scm.api.bill.dto.OrderDetailDTO;
import com.haohan.cloud.scm.api.bill.dto.OrderInfoDTO;
import com.haohan.cloud.scm.api.constant.enums.aftersales.ReturnStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.bill.OrderStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.bill.OrderTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.crm.PayStatusEnum;
import lombok.Data;
import lombok.experimental.UtilityClass;

import java.util.stream.Collectors;

/**
 * @author dy
 * @date 2019/11/28
 */
@Data
@UtilityClass
public class AfterSalesTrans {

    public static OrderInfoDTO transToOrderInfo(QueryReturnOrderDetailResp order) {
        if(null == order){
            return null;
        }
        // 缺 pmName、merchantId、merchantName
        OrderInfoDTO orderInfo = new OrderInfoDTO();
        orderInfo.setOrderSn(order.getReturnSn());
        orderInfo.setOrderType(OrderTypeEnum.back);
        // todo 暂默认未付
        orderInfo.setPayStatus(PayStatusEnum.wait);
        // 订单状态 暂只设置 取消和已下单
        orderInfo.setOrderStatus(order.getReturnStatus()== ReturnStatusEnum.failed? OrderStatusEnum.cancel:OrderStatusEnum.submit);
        orderInfo.setPmId(order.getPmId());
        orderInfo.setCustomerId(order.getReturnCustomerId());
        orderInfo.setCustomerName(order.getReturnCustomer());
        orderInfo.setDealDate(order.getApplyTime().toLocalDate());
        orderInfo.setTotalAmount(order.getTotalAmount());
        orderInfo.setSumAmount(order.getSumAmount());
        orderInfo.setOtherAmount(order.getOtherAmount());
        // 明细
        orderInfo.setDetailList(order.getDetailList().stream()
                .map(item->{
                    OrderDetailDTO detail = new OrderDetailDTO();
                    // 缺 goodsId、marketPrice
                    detail.setGoodsModelId(item.getGoodsModelId());
                    detail.setGoodsName(item.getGoodsName());
                    detail.setModelName(item.getModelName());
                    detail.setUnit(item.getUnit());
                    detail.setGoodsImg(item.getGoodsImg());
                    detail.setDealPrice(item.getGoodsPrice());
                    detail.setGoodsNum(item.getGoodsNum());
                    detail.setAmount(item.getGoodsAmount());
                    return detail;
                }).collect(Collectors.toList()));
        return orderInfo;
    }
}
