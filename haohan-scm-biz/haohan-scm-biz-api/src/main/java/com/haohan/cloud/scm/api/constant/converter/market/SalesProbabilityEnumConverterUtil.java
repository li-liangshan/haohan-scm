package com.haohan.cloud.scm.api.constant.converter.market;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.market.SalesProbabilityEnum;

/**
 * @author xwx
 * @date 2019/6/5
 */
public class SalesProbabilityEnumConverterUtil implements Converter<SalesProbabilityEnum> {
    @Override
    public SalesProbabilityEnum convert(Object o, SalesProbabilityEnum salesProbabilityEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return SalesProbabilityEnum.getByType(o.toString());
    }
}
