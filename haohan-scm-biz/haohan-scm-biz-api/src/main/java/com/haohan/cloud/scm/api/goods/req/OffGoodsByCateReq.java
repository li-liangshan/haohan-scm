package com.haohan.cloud.scm.api.goods.req;

import lombok.Data;

/**
 * @author cx
 * @date 2019/8/20
 */

@Data
public class OffGoodsByCateReq {

    private String id;

    /**
     * true上架
     * false下架
     */
    private Boolean flag;
}
