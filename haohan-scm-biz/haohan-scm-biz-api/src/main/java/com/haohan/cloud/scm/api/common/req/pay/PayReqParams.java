package com.haohan.cloud.scm.api.common.req.pay;

import lombok.Data;

import java.io.Serializable;

/**
 * @program: haohan-fresh-scm
 * @description: 支付请求参数
 * @author: Simon
 * @create: 2019-07-25
 **/
@Data
public class PayReqParams implements Serializable {

  /**
   * Appkey 应用Key
   */
  private String appkey;


  /**
   * DES加密后的参数值
   */
  private String params;

  public PayReqParams(String appKey) {
    this.appkey = appKey;
  }


}
