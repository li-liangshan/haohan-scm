package com.haohan.cloud.scm.wms.core.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.haohan.cloud.scm.api.constant.NumberPrefixConstant;
import com.haohan.cloud.scm.api.product.req.ProductInfoInventoryReq;
import com.haohan.cloud.scm.api.wms.entity.WarehouseInventory;
import com.haohan.cloud.scm.api.wms.entity.WarehouseInventoryDetail;
import com.haohan.cloud.scm.api.wms.req.AddWarehouseInventoryReq;
import com.haohan.cloud.scm.api.wms.req.EditInventoryReq;
import com.haohan.cloud.scm.api.wms.resp.WarehouseInventoryDetailResp;
import com.haohan.cloud.scm.common.tools.exception.EmptyDataException;
import com.haohan.cloud.scm.common.tools.exception.ErrorDataException;
import com.haohan.cloud.scm.common.tools.util.ScmIncrementUtil;
import com.haohan.cloud.scm.wms.core.IScmWarehouseInventoryService;
import com.haohan.cloud.scm.wms.service.WarehouseInventoryDetailService;
import com.haohan.cloud.scm.wms.service.WarehouseInventoryService;
import com.haohan.cloud.scm.wms.service.WarehouseService;
import com.haohan.cloud.scm.wms.utils.ScmWmsUtils;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author cx
 * @date 2019/8/10
 */

@Service
@AllArgsConstructor
public class ScmWarehouseInventoryServiceImpl implements IScmWarehouseInventoryService {

    private final WarehouseInventoryService warehouseInventoryService;

    private final WarehouseInventoryDetailService warehouseInventoryDetailService;

    private final ScmIncrementUtil scmIncrementUtil;

    private final WarehouseService warehouseService;

    private final ScmWmsUtils scmWmsUtils;

    /**
     * 查询盘点详情
     * @param inventory
     * @return
     */
    @Override
    public WarehouseInventoryDetailResp queryInventoryDetail(WarehouseInventory inventory) {
        WarehouseInventory a = warehouseInventoryService.getOne(Wrappers.query(inventory));
        if(ObjectUtil.isNull(a)){
            throw new EmptyDataException();
        }
        QueryWrapper<WarehouseInventoryDetail> query = new QueryWrapper<>();
        query.lambda()
                .eq(WarehouseInventoryDetail::getPmId,a.getPmId())
                .eq(WarehouseInventoryDetail::getWarehouseInventorySn,a.getWarehouseInventorySn());
        List<WarehouseInventoryDetail> list = warehouseInventoryDetailService.list(query);
        if(list.isEmpty()){
            throw new EmptyDataException();
        }
        WarehouseInventoryDetailResp res = new WarehouseInventoryDetailResp();
        BeanUtil.copyProperties(a,res);
        res.setList(list);
        return res;
    }

    /**
     * 新增盘点记录
     * @param req
     * @return
     */
    @Override
    public Boolean addWarehouseInventory(AddWarehouseInventoryReq req) {
        WarehouseInventory inventory = new WarehouseInventory();
        BeanUtil.copyProperties(req,inventory);
        String sn = scmIncrementUtil.inrcSnByClass(WarehouseInventory.class, NumberPrefixConstant.WAREHOUSE_INVENTORY_SN_PRE);
        inventory.setWarehouseInventorySn(sn);
//        Warehouse q = new Warehouse();
//        q.setPmId(req.getPmId());
//        q.setWarehouseSn(req.getWarehouseSn());
//        Warehouse in = warehouseService.getOne(Wrappers.query(q));
//        if(ObjectUtil.isNull(in)){
//            throw new EmptyDataException();
//        }
        inventory.setOperateTime(LocalDateTime.now());
        BigDecimal on = BigDecimal.ZERO;
        BigDecimal rn = BigDecimal.ZERO;
        for(ProductInfoInventoryReq info:req.getInfos()){
            WarehouseInventoryDetail detail = new WarehouseInventoryDetail();
            detail.setPmId(req.getPmId());
            detail.setWarehouseInventorySn(sn);
            detail.setOperateTime(LocalDateTime.now());
            detail.setOriginalNumber(info.getProductNumber());
            on = on.add(info.getProductNumber());
            detail.setProductName(info.getProductName());
            detail.setProductSn(info.getProductSn());
            detail.setResultNumber(info.getAfterNumber());
            rn = rn.add(info.getAfterNumber());
            detail.setUnit(info.getUnit());
//            detail.setWarehouseSn(in.getWarehouseSn());
            if(!warehouseInventoryDetailService.save(detail)){
                throw new ErrorDataException();
            }
        }
        inventory.setOriginalNumber(on);
        inventory.setResultNumber(rn);
        if(!warehouseInventoryService.save(inventory)){
            throw new ErrorDataException();
        }
        return true;
    }



    /**
     * 编辑盘点记录
     * @param req
     * @return
     */
    @Override
    public Boolean editWarehouseInventory(EditInventoryReq req) {
        WarehouseInventory inventory = new WarehouseInventory();
        BeanUtil.copyProperties(req,inventory);
        inventory.setOriginalNumber(BigDecimal.ZERO);
        inventory.setResultNumber(BigDecimal.ZERO);
        req.getList().forEach(list ->{
            if(!warehouseInventoryDetailService.updateById(list)){
                throw new ErrorDataException();
            }
            inventory.setOriginalNumber(inventory.getOriginalNumber().add(list.getOriginalNumber()));
            inventory.setResultNumber(inventory.getResultNumber().add(list.getResultNumber()));
        });
        if(!warehouseInventoryService.updateById(inventory)){
            throw new ErrorDataException();
        }
        if(!req.getDelDetails().isEmpty()){
            req.getDelDetails().forEach(del -> warehouseInventoryDetailService.removeById(del.getId()));
        }
        if(!req.getAddDetails().isEmpty()){
           req.getAddDetails().forEach(info ->{
                WarehouseInventoryDetail detail = new WarehouseInventoryDetail();
                detail.setPmId(req.getPmId());
                detail.setWarehouseInventorySn(inventory.getWarehouseInventorySn());
                detail.setOperateTime(LocalDateTime.now());
                detail.setOriginalNumber(info.getProductNumber());
                detail.setProductName(info.getProductName());
                detail.setProductSn(info.getProductSn());
                detail.setResultNumber(info.getAfterNumber());
                detail.setUnit(info.getUnit());
                detail.setWarehouseSn(inventory.getWarehouseSn());
                if(!warehouseInventoryDetailService.save(detail)){
                    throw new ErrorDataException();
                }
            });
        }
        return true;
    }
}
