/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.aftersales.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.aftersales.service.CustomerRevaluateService;
import com.haohan.cloud.scm.api.aftersales.entity.CustomerRevaluate;
import com.haohan.cloud.scm.api.aftersales.req.CustomerRevaluateReq;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 客户服务评分
 *
 * @author haohan
 * @date 2019-05-30 10:27:30
 */
@RestController
@AllArgsConstructor
@RequestMapping("/customerrevaluate" )
@Api(value = "customerrevaluate", tags = "customerrevaluate管理")
public class CustomerRevaluateController {

    private final CustomerRevaluateService customerRevaluateService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param customerRevaluate 客户服务评分
     * @return
     */
    @GetMapping("/page" )
    public R getCustomerRevaluatePage(Page page, CustomerRevaluate customerRevaluate) {
        return new R<>(customerRevaluateService.page(page, Wrappers.query(customerRevaluate)));
    }


    /**
     * 通过id查询客户服务评分
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(customerRevaluateService.getById(id));
    }

    /**
     * 新增客户服务评分
     * @param customerRevaluate 客户服务评分
     * @return R
     */
    @SysLog("新增客户服务评分" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_customerrevaluate_add')" )
    public R save(@RequestBody CustomerRevaluate customerRevaluate) {
        return new R<>(customerRevaluateService.save(customerRevaluate));
    }

    /**
     * 修改客户服务评分
     * @param customerRevaluate 客户服务评分
     * @return R
     */
    @SysLog("修改客户服务评分" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_customerrevaluate_edit')" )
    public R updateById(@RequestBody CustomerRevaluate customerRevaluate) {
        return new R<>(customerRevaluateService.updateById(customerRevaluate));
    }

    /**
     * 通过id删除客户服务评分
     * @param id id
     * @return R
     */
    @SysLog("删除客户服务评分" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_customerrevaluate_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(customerRevaluateService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除客户服务评分")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_customerrevaluate_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(customerRevaluateService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询客户服务评分")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(customerRevaluateService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param customerRevaluateReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询客户服务评分总记录}")
    @PostMapping("/countByCustomerRevaluateReq")
    public R countByCustomerRevaluateReq(@RequestBody CustomerRevaluateReq customerRevaluateReq) {

        return new R<>(customerRevaluateService.count(Wrappers.query(customerRevaluateReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param customerRevaluateReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据customerRevaluateReq查询一条货位信息表")
    @PostMapping("/getOneByCustomerRevaluateReq")
    public R getOneByCustomerRevaluateReq(@RequestBody CustomerRevaluateReq customerRevaluateReq) {

        return new R<>(customerRevaluateService.getOne(Wrappers.query(customerRevaluateReq), false));
    }


    /**
     * 批量修改OR插入
     * @param customerRevaluateList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_customerrevaluate_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<CustomerRevaluate> customerRevaluateList) {

        return new R<>(customerRevaluateService.saveOrUpdateBatch(customerRevaluateList));
    }


}
