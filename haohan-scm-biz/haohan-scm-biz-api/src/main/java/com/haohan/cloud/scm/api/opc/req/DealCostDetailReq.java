/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.opc.req;

import com.haohan.cloud.scm.api.opc.entity.DealCostDetail;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 交易成本明细
 *
 * @author haohan
 * @date 2019-05-30 10:17:46
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "交易成本明细")
public class DealCostDetailReq extends DealCostDetail {

    private long pageSize;
    private long pageNo;




}
