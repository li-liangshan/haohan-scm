/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.manage.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.manage.entity.TreeDict;
import com.haohan.cloud.scm.api.manage.req.TreeDictReq;
import com.haohan.cloud.scm.manage.service.TreeDictService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 树形字典
 *
 * @author haohan
 * @date 2019-05-29 14:28:58
 */
@RestController
@AllArgsConstructor
@RequestMapping("/treedict" )
@Api(value = "treedict", tags = "treedict管理")
public class TreeDictController {

    private final TreeDictService treeDictService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param treeDict 树形字典
     * @return
     */
    @GetMapping("/page" )
    public R getTreeDictPage(Page page, TreeDict treeDict) {
        return new R<>(treeDictService.page(page, Wrappers.query(treeDict)));
    }


    /**
     * 通过id查询树形字典
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(treeDictService.getById(id));
    }

    /**
     * 新增树形字典
     * @param treeDict 树形字典
     * @return R
     */
    @SysLog("新增树形字典" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_treedict_add')" )
    public R save(@RequestBody TreeDict treeDict) {
        return new R<>(treeDictService.save(treeDict));
    }

    /**
     * 修改树形字典
     * @param treeDict 树形字典
     * @return R
     */
    @SysLog("修改树形字典" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_treedict_edit')" )
    public R updateById(@RequestBody TreeDict treeDict) {
        return new R<>(treeDictService.updateById(treeDict));
    }

    /**
     * 通过id删除树形字典
     * @param id id
     * @return R
     */
    @SysLog("删除树形字典" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_treedict_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(treeDictService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除树形字典")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_treedict_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(treeDictService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询树形字典")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(treeDictService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param treeDictReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询树形字典总记录}")
    @PostMapping("/countByTreeDictReq")
    public R countByTreeDictReq(@RequestBody TreeDictReq treeDictReq) {

        return new R<>(treeDictService.count(Wrappers.query(treeDictReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param treeDictReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据treeDictReq查询一条货位信息表")
    @PostMapping("/getOneByTreeDictReq")
    public R getOneByTreeDictReq(@RequestBody TreeDictReq treeDictReq) {

        return new R<>(treeDictService.getOne(Wrappers.query(treeDictReq), false));
    }


    /**
     * 批量修改OR插入
     * @param treeDictList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_treedict_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<TreeDict> treeDictList) {

        return new R<>(treeDictService.saveOrUpdateBatch(treeDictList));
    }


}
