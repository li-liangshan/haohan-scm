/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.aftersales.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haohan.cloud.scm.api.aftersales.entity.SalesReturn;

/**
 * 售后退货记录
 *
 * @author haohan
 * @date 2019-05-13 20:29:00
 */
public interface SalesReturnMapper extends BaseMapper<SalesReturn> {

}
