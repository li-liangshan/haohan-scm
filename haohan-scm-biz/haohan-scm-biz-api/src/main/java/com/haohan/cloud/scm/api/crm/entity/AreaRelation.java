/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.crm.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;


/**
 * 区域关联关系
 *
 * @author haohan
 * @date 2019-12-21 15:35:01
 */
@Data
@TableName("crm_area_relation")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "区域关联关系")
public class AreaRelation extends Model<AreaRelation> {
    private static final long serialVersionUID = 1L;


    @Length(max = 64, message = "主键长度最大64字符")
    @ApiModelProperty(value = "主键")
    @TableId(type = IdType.INPUT)
    private String id;

    @Length(max = 64, message = "区域编号长度最大64字符")
    @ApiModelProperty(value = "区域编号")
    private String areaSn;

    @Length(max = 64, message = "关联id长度最大64字符")
    @ApiModelProperty(value = "关联id")
    private String relationId;

    @Length(max = 64, message = "关联类名长度最大64字符")
    @ApiModelProperty(value = "关联类名")
    private String relationClass;

    @Length(max = 64, message = "创建者长度最大64字符")
    @ApiModelProperty(value = "创建者")
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;

    @Length(max = 64, message = "更新者长度最大64字符")
    @ApiModelProperty(value = "更新者")
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;

    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.UPDATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateDate;

    @Length(max = 255, message = "备注信息长度最大255字符")
    @ApiModelProperty(value = "备注信息")
    private String remarks;

    @Length(max = 1, message = "删除标记长度最大1字符")
    @ApiModelProperty(value = "删除标记")
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;

    @ApiModelProperty(value = "租户id")
    private Integer tenantId;

}
