/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.goods.req;

import com.haohan.cloud.scm.api.goods.entity.ServiceSelection;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 服务选项
 *
 * @author haohan
 * @date 2019-05-28 19:58:28
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "服务选项")
public class ServiceSelectionReq extends ServiceSelection {

    private long pageSize;
    private long pageNo;




}
