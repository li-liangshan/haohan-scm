package com.haohan.cloud.scm.api.constant;

/**
 * 自增编号 前缀 常量
 *
 * @author dy
 * @date 2019/5/17
 * afterSales 售后模块 下都以 A 结尾
 * saleB 模块 下都以 B 结尾
 * opc 运营平台 下 都以 C 结尾
 * tms  物流 都以 D 结尾
 * photo  图片资源  都以 G 结尾
 * ms 消息系统 下都以 M 结尾
 * purchase 采购部 以O 结尾
 * product 生产部 下都以 P 结尾
 * crm  客户关系 都以 R 结尾
 * goods 商品 下都以 S 结尾
 * market 市场部 下都以 T 结尾
 * wms 仓储系统 下都以 W 结尾
 * supply 供应 下 都以 Y 结尾
 * bill 账单 下 都以 Z 结尾
 */
public interface NumberPrefixConstant {

// purchase 采购部 以O 结尾

    /**
     * 采购部采购单
     */
    String PURCHASE_SN_PRE = "PO";

    /**
     * 采购部采购单明细
     */
    String PURCHASE_DETAIL_SN_PRE = "PDO";

    /**
     * 采购部采购单明细
     */
    String LENDING_RECORD_SN_PRE = "LO";

// afterSales 售后模块 下都以 A 结尾

    /**
     * 售后单
     */
    String AFTER_SALES_SN_PRE = "AA";

    /**
     * 退货单编号
     */
    String RETURN_ORDER_SN_PRE = "RA";

    /**
     * 退货单明细编号
     */
    String RETURN_DETAIL_SN_PRE = "RDA";


// market 市场部 下都以 T 结尾

    /**
     * 销售合同
     */
    String SALES_CONTRACT_SN_PRE = "SCT";

    /**
     * 市场部任务
     */
    String MARKET_TASK_SN_PRE = "MTT";

// ms 消息系统 下都以 M 结尾

    /**
     * 站内信记录
     */
    String IN_MAIL_RECORD_SN_PRE = "IM";

    /**
     * 短信消息记录
     */
    String SHORT_MESSAGE_RECORD_SN_PRE = "SM";

    /**
     * 微信消息记录
     */
    String WECHAT_MESSAGE_RECORD_SN_PRE = "WM";

    /**
     * 预警记录表
     */
    String WARNING_RECORD_SN_PRE = "RM";
    /**
     * 预警信息模板
     */
    String WARNING_TEMPLATE_SN_PRE = "RTM";

// wms 仓储系统 下都以 W 结尾

    /**
     * 仓库信息表
     */
    String WAREHOUSE_SN_PRE = "WSW";

    /**
     * 货架信息表
     */
    String SHELF_SN_PRE = "SSW";

    /**
     * 货位信息表
     */
    String CELL_SN_PRE = "CSW";
    /**
     * 入库单明细
     */
    String ENTER_WAREHOUSE_DETAIL_SN_PRE = "EDW";

    /**
     * 入库单
     */
    String ENTER_WAREHOUSE_SN_PRE = "EWW";

    /**
     * 出库单明细
     */
    String EXIT_WAREHOUSE_DETAIL_SN_PRE = "XDW";

    /**
     * 出库单
     */
    String EXIT_WAREHOUSE_SN_PRE = "XWW";

    /**
     * 托盘信息表
     */
    String PALLET_SN_PRE = "PW";

    /**
     * 暂存点信息表
     */
    String STORAGE_PLACE_SN_PRE = "SPW";

    /**
     * 库存调拨明细记录
     */
    String WAREHOUSE_ALLOT_DETAIL_SN_PRE = "ADW";

    /**
     * 库存调拨记录
     */
    String WAREHOUSE_ALLOT_SN_PRE = "AW";

    /**
     * 库存盘点明细
     */
    String WAREHOUSE_INVENTORY_DETAIL_SN_PRE = "IDW";

    /**
     * 库存盘点
     */
    String WAREHOUSE_INVENTORY_SN_PRE = "IDW";

// product 生产部 下都以 P 结尾

    /**
     * 货品信息记录表
     */
    String PRODUCT_INFO_SN_PRE = "PP";

    /**
     * 生产任务表
     */
    String PRODUCTION_TASK_SN_PRE = "TP";

    /**
     * 货品损耗记录表
     */
    String PRODUCT_LOSS_RECORD_SN_PRE = "LP";

    /**
     * 货品加工记录表
     */
    String PRODUCT_PROCESSING_SN_PRE = "PPP";

    /**
     * 商品加工配方表
     */
    String RECIPE_SN_PRE = "RP";

    /**
     * 送货明细
     */
    String SHIP_ORDER_DETAIL_SN_PRE = "SDP";

    /**
     * 送货单
     */
    String SHIP_ORDER_SN_PRE = "SP";

    /**
     * 分拣单明细
     */
    String SORTING_ORDER_DETAIL_SN_PRE = "ODP";

    /**
     * 分拣单
     */
    String SORTING_ORDER_SN_PRE = "OP";

//  opc 运营平台 下 都以 C 结尾

    /**
     * 发货记录
     */
    String SHIP_RECORD_SN_PRE = "PC";

    /**
     * 成本管控
     */
    String COST_CONTROL_SN_PRE = "CC";

    /**
     * 结算记录
     */
    String SETTLEMENT_RECORD_SN_PRE = "SC";

    /**
     * 汇总单
     */
    String SUMMARY_ORDER_SN_PRE = "SOC";

    /**
     * 交易订单
     */
    String TRADE_ORDER_SN_PRE = "TC";

// saleB 模块 下都以 B 结尾

    /**
     * 采购商货款统计
     */
    String BUYER_PAYMENT_SN_PRE = "PB";

    /**
     * 采购单明细
     */
    String BUY_ORDER_DETAIL_SN_PRE = "ODB";

    /**
     * 采购单
     */
    String BUY_ORDER_SN_PRE = "OB";

// supply 供应 下 都以 Y 结尾

    /**
     * 供应单号
     */
    String SUPPLY_ORDER_SN_PRE = "SY";
    /**
     * 供应单号
     */
    String SUPPLY_ORDER_DETAIL_SN_PRE = "SDY";

    /**
     * 报价单号
     */
    String OFFER_ORDER_SN_PRE = "OY";

    /**
     * 供应商货款
     */
    String SUPPLIER_PAYMENT_SN_PRE = "PY";

// photo  图片资源  都以 G 结尾
    /**
     * 图片组编号
     */
    String PHOTO_GROUP_SN_PR = "GGS";

// tms  物流 都以 D 结尾
    /**
     * 物流配送
     */
    String DELIVERY_FLOW_SN_PR = "FD";

//  crm  客户关系 都以 R 结尾
    /**
     * 区域编号
     */
    String AREA_SN_PR = "AR";
    /**
     * 客户编号
     */
    String CUSTOMER_SN_PR = "CR";
    /**
     * 业务动态编号
     */
    String DYNAMIC_SN_PR = "DR";
    /**
     * 汇报编号
     */
    String REPORT_SN_PR = "RR";
    /**
     * 市场编号
     */
    String MARKET_SN_PR = "MR";
    /**
     * 销售合同编号
     */
    String CONTRACT_SN_PR = "SCR";
    /**
     * 销售订单编号
     */
    String SALES_ORDER_SN_PR = "OR";
    /**
     * 销售订单明细编号
     */
    String SALES_DETAIL_SN_PR = "ODR";
    /**
     * 工作汇报编号
     */
    String WORK_REPORT_SN_PR = "WR";

//    goods 商品 下都以 S 结尾
    /**
     * 零售商品规格编号 中间分隔符
     */
    String GOODS_MODEL_SN_INFIX = "M";
    /**
     * 零售商品编号
     */
    String GOODS_SN_PR = "GS";

    // bill 账单 下 都以 Z 结尾
    /**
     * 结算记录
     */
    String SETTLEMENT_ACCOUNT_SN_PRE = "SZ";
    /**
     * 应收账单
     */
    String RECEIVABLE_BILL_SN_PRE = "RZ";
    /**
     * 应付账单
     */
    String PAYABLE_BILL_SN_PRE = "PZ";

}
