/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.goods.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.goods.entity.GoodsGifts;
import com.haohan.cloud.scm.goods.mapper.GoodsGiftsMapper;
import com.haohan.cloud.scm.goods.service.GoodsGiftsService;
import org.springframework.stereotype.Service;

/**
 * 赠品
 *
 * @author haohan
 * @date 2019-05-13 18:46:37
 */
@Service
public class GoodsGiftsServiceImpl extends ServiceImpl<GoodsGiftsMapper, GoodsGifts> implements GoodsGiftsService {

}
