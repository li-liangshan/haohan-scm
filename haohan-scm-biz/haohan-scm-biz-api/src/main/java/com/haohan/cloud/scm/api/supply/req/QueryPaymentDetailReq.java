package com.haohan.cloud.scm.api.supply.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author xwx
 * @date 2019/6/17
 */
@Data
@ApiModel(description = "查询供应商货款详情")
public class QueryPaymentDetailReq {

    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台id",required = true)
    private String pmId;

    @ApiModelProperty(value = "供应商货款主键",required = true)
    private String id;

    @NotBlank(message = "uid不能为空")
    @ApiModelProperty(value = "供应商uid",required = true)
    private String uid;

    private String offerOrderId;

}
