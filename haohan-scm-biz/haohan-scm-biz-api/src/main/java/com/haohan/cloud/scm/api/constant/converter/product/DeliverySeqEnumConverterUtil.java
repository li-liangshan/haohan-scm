package com.haohan.cloud.scm.api.constant.converter.product;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.product.DeliverySeqEnum;

/**
 * @author xwx
 * @date 2019/6/4
 */
public class DeliverySeqEnumConverterUtil implements Converter<DeliverySeqEnum> {
    @Override
    public DeliverySeqEnum convert(Object o, DeliverySeqEnum deliverySeqEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return DeliverySeqEnum.getByType(o.toString());
    }
}
