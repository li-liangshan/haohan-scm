package com.haohan.cloud.scm.api.constant.converter.purchase;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.purchase.ReportStatusEnum;

/**
 * @author xwx
 * @date 2019/6/3
 */
public class ReportStatusEnumConverterUtil implements Converter<ReportStatusEnum> {
    @Override
    public ReportStatusEnum convert(Object o, ReportStatusEnum reportStatusEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return ReportStatusEnum.getByType(o.toString());
    }
}
