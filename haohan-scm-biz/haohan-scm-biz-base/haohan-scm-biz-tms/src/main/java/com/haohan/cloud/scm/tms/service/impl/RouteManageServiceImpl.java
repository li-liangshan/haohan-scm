/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.tms.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.tms.entity.RouteManage;
import com.haohan.cloud.scm.tms.mapper.RouteManageMapper;
import com.haohan.cloud.scm.tms.service.RouteManageService;
import org.springframework.stereotype.Service;

/**
 * 路线管理
 *
 * @author haohan
 * @date 2019-06-05 09:25:40
 */
@Service
public class RouteManageServiceImpl extends ServiceImpl<RouteManageMapper, RouteManage> implements RouteManageService {

}
