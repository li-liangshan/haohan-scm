package com.haohan.cloud.scm.api.crm.req.analysis;

import com.haohan.cloud.scm.api.constant.enums.market.MarketEmployeeTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.market.SexEnum;
import com.haohan.cloud.scm.api.crm.entity.MarketEmployee;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

/**
 * @author dy
 * @date 2019/11/12
 */
@Data
@Api("查询员工日报数汇总")
public class EmployeeReportSummaryReq {

    @Length(max = 32, message = "员工名称字符长度在0至32之间")
    @ApiModelProperty(value = "员工名称")
    private String employeeName;

    @ApiModelProperty(value = "市场部员工类型:1市场总监2区域经理3业务经理4.社区合伙人")
    private MarketEmployeeTypeEnum employeeType;

    @ApiModelProperty(value = "性别:1男2女")
    private SexEnum sex;

    @Length(max = 32, message = "职位字符长度在0至32之间")
    @ApiModelProperty(value = "职位")
    private String post;

    @Length(max = 32, message = "部门名称字符长度在0至32之间")
    @ApiModelProperty(value = "部门名称")
    private String department;

    @ApiModelProperty(value = "查询开始日期", notes = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate startDate;

    @ApiModelProperty(value = "查询结束日期", notes = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate endDate;


    public MarketEmployee transTo() {
        MarketEmployee employee = new MarketEmployee();
        employee.setName(this.employeeName);
        employee.setEmployeeType(this.employeeType);
        employee.setSex(this.sex);
        employee.setPost(this.post);
        employee.setDeptId(department);
        return employee;
    }
}
