package com.haohan.cloud.scm.api.constant.enums.purchase;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xwx
 * @date 2019/5/30
 */
@Getter
@AllArgsConstructor
public enum ActionTypeEnum {
    /**
     * 采购执行状态 1.正常2.异常
     */
    normal("1","正常"),
    abnormal("2","异常");

    private static final Map<String, ActionTypeEnum> MAP = new HashMap<>(8);

    static {
        for (ActionTypeEnum e : ActionTypeEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static ActionTypeEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }
    /**
     * 类型
     */
    @EnumValue
    @JsonValue
    private String type;
    /**
     * 描述
     */

    private String desc;
}
