package com.haohan.cloud.scm.api.purchase.trans;

import com.haohan.cloud.scm.api.constant.enums.purchase.PurchaseStatusEnum;
import com.haohan.cloud.scm.api.goods.dto.GoodsModelDTO;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseOrderDetail;

/**
 * @author cx
 * @date 2019/6/11
 */
public class PurchaseInitPlanTrans {
  /**
   * 将商品规格信息和商品信息注入采购明细
   * @param dto
   * @return
   */
  public static PurchaseOrderDetail initDetail(GoodsModelDTO dto){
    PurchaseOrderDetail detail = new PurchaseOrderDetail();
    detail.setPurchaseStatus(PurchaseStatusEnum.pending);
    detail.setGoodsName(dto.getGoodsName());
    detail.setGoodsImg(dto.getModelUrl());
    detail.setGoodsCategoryName(dto.getCategoryName());
    detail.setGoodsCategoryId(dto.getGoodsCategoryId());
    detail.setUnit(dto.getModelUnit());
    detail.setModelName(dto.getModelName());
    detail.setGoodsId(dto.getGoodsId());
    return detail;
  }

}
