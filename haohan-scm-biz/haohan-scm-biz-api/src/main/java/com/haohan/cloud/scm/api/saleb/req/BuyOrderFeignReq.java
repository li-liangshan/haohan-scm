package com.haohan.cloud.scm.api.saleb.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author dy
 * @date 2020/5/16
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class BuyOrderFeignReq extends QueryBuyOrderReq {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "分页参数 当前页")
    private long current = 1;

    @ApiModelProperty(value = "分页参数 每页显示条数")
    private long size = 10;


}
