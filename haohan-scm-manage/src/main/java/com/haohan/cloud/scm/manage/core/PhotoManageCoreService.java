package com.haohan.cloud.scm.manage.core;

import com.haohan.cloud.scm.api.common.entity.resp.UploadPhotoResp;
import com.haohan.cloud.scm.api.constant.enums.manage.PhotoTypeEnum;
import com.haohan.cloud.scm.api.manage.dto.PhotoGroupDTO;
import com.haohan.cloud.scm.api.manage.entity.PhotoGroupManage;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author dy
 * @date 2019/9/11
 */
public interface PhotoManageCoreService {

    /**
     * 查询图片组图片信息
     *
     * @param groupNum
     * @return
     */
    PhotoGroupDTO fetchByGroupNum(String groupNum);

    /**
     * 新增图片组图片
     *
     * @param group
     * @return
     */
    PhotoGroupManage addPhotoGroup(PhotoGroupDTO group);

    /**
     * 图片组修改图片
     *
     * @param group
     * @return
     */
    void modifyPhotoGroup(PhotoGroupDTO group);

    /**
     * 编辑
     *
     * @param photoGroupDTO
     * @return
     */
    PhotoGroupManage editPhotoGroup(PhotoGroupDTO photoGroupDTO);

    /**
     * 图片上传
     *
     * @param file
     * @param type
     * @param suffix
     * @return
     */
    UploadPhotoResp uploadPhoto(MultipartFile file, PhotoTypeEnum type, String suffix);
}
