package com.haohan.cloud.scm.api.goods.dto;

import com.haohan.cloud.scm.api.common.dto.SelfSqlDTO;
import com.haohan.cloud.scm.api.constant.enums.goods.GoodsFromTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.goods.GoodsStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.goods.GoodsTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import com.haohan.cloud.scm.api.goods.entity.Goods;
import com.haohan.cloud.scm.api.goods.req.manage.GoodsListReq;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author dy
 * @date 2020/4/23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class GoodsSqlDTO extends SelfSqlDTO {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("商家id")
    private String merchantId;

    @ApiModelProperty("店铺id")
    private String shopId;

    @ApiModelProperty("商品id")
    private String goodsId;

    @ApiModelProperty("商品规格id")
    private String modelId;

    @ApiModelProperty("商品分类id")
    private String categoryId;

    @ApiModelProperty("商品规格sn")
    private String modelSn;

    @ApiModelProperty("商品sn")
    private String goodsSn;

    @ApiModelProperty("商品名称")
    private String goodsName;

    @ApiModelProperty("商品规格名称")
    private String modelName;

    @ApiModelProperty("商品分类名称")
    private String categoryName;

    @ApiModelProperty("商品规格单位")
    private String modelUnit;

    @ApiModelProperty("商品规格扫码购编码")
    private String modelCode;

    @ApiModelProperty("是否上架")
    private Integer isMarketable;

    @ApiModelProperty("售卖规则标记")
    private YesNoEnum saleRule;

    @ApiModelProperty("服务选项标记")
    private YesNoEnum serviceSelection;

    @ApiModelProperty("赠品标记")
    private YesNoEnum goodsGift;

    @ApiModelProperty("商品规格标记")
    private YesNoEnum goodsModel;

    @ApiModelProperty("商品状态(出售中/仓库中/已售罄)")
    private GoodsStatusEnum goodsStatus;

    @ApiModelProperty("商品来源 0.小店平台 1.即速应用")
    private GoodsFromTypeEnum goodsFrom;

    @ApiModelProperty("商品类型")
    private GoodsTypeEnum goodsType;

    @ApiModelProperty("商品扫码购编码")
    private String scanCode;

    @ApiModelProperty("是否c端销售")
    private YesNoEnum salecFlag;

    @ApiModelProperty("商品规格id列表")
    private String modelIds;

    @ApiModelProperty("分类id列表")
    private String categoryIds;

    @ApiModelProperty(value = "通行证id")
    private String uid;

    // 字典处理(自定义sql使用)

    public String getSaleRule() {
        return null == this.saleRule ? null : this.saleRule.getType();
    }

    public String getServiceSelection() {
        return null == this.serviceSelection ? null : this.serviceSelection.getType();
    }

    public String getGoodsGift() {
        return null == this.goodsGift ? null : this.goodsGift.getType();
    }

    public String getGoodsModel() {
        return null == this.goodsModel ? null : this.goodsModel.getType();
    }

    public String getGoodsStatus() {
        return null == this.goodsStatus ? null : this.goodsStatus.getType();
    }

    public String getGoodsFrom() {
        return null == this.goodsFrom ? null : this.goodsFrom.getType();
    }

    public String getGoodsType() {
        return null == this.goodsType ? null : this.goodsType.getType();
    }

    public String getSalecFlag() {
        return null == this.salecFlag ? null : this.salecFlag.getType();
    }


    public GoodsSqlDTO(Goods goods) {
        this.merchantId = goods.getMerchantId();
        this.shopId = goods.getShopId();
        this.goodsId = goods.getId();
        // 分类id单独处理
        this.goodsSn = goods.getGoodsSn();
        this.goodsName = goods.getGoodsName();
        this.saleRule = goods.getSaleRule();
        this.isMarketable = goods.getIsMarketable();
        this.serviceSelection = goods.getServiceSelection();
        this.goodsGift = goods.getGoodsGift();
        this.goodsModel = goods.getGoodsModel();
        this.goodsStatus = goods.getGoodsStatus();
        this.goodsFrom = goods.getGoodsFrom();
        this.goodsType = goods.getGoodsType();
        this.scanCode = goods.getScanCode();
        this.salecFlag = goods.getSalecFlag();
    }

    public GoodsSqlDTO(GoodsListReq req) {
        this.shopId = req.getShopId();
        this.goodsId = req.getGoodsId();
        this.goodsName = req.getGoodsName();
        this.goodsStatus = req.getGoodsStatus();
        this.goodsType = req.getGoodsType();
        this.salecFlag = req.getSalecFlag();
        if (null != req.getMarketableFlag()) {
            this.isMarketable = req.getMarketableFlag() == YesNoEnum.yes ? 1 : 0;
        }
        // 分类id单独处理
        this.goodsSn = req.getGoodsSn();
        this.scanCode = req.getScanCode();
    }
}
