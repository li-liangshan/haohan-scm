package com.haohan.cloud.scm.api.crm.req.report;

import com.haohan.cloud.scm.api.constant.ScmCommonConstant;
import com.haohan.cloud.scm.api.constant.enums.crm.DataReportEnum;
import com.haohan.cloud.scm.api.constant.enums.crm.DataReportStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.crm.SalesGoodsTypeEnum;
import com.haohan.cloud.scm.api.crm.dto.DataReportDTO;
import com.haohan.cloud.scm.api.crm.entity.DataReport;
import com.haohan.cloud.scm.api.crm.entity.DataReportDetail;
import com.haohan.cloud.scm.common.tools.constant.SingleGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

/**
 * @author dy
 * @date 2020/4/22
 * SingleGroup 用于修改时的编号
 */
@Data
public class CompetitionReportEditReq {

    @NotBlank(message = "上报编号不能为空", groups = {SingleGroup.class})
    @Length(min = 0, max = 32, message = "上报编号长度在0至32之间", groups = {SingleGroup.class})
    @ApiModelProperty(value = "上报编号")
    private String reportSn;

    @NotBlank(message = "客户编码不能为空")
    @Length(max = 32, message = "客户编码的最大长度为32字符")
    @ApiModelProperty(value = "客户编码")
    private String customerSn;

    @Pattern(regexp = ScmCommonConstant.POSITION_PATTEN, message = "位置定位, 经纬度格式有误, 经度正负180度之间, 纬度正负90度之间, 弧度数表示")
    @ApiModelProperty(value = "上报位置定位")
    private String reportLocation;

    @Length(max = 64, message = "上报位置地址的最大长度为64字符")
    @ApiModelProperty(value = "上报位置地址")
    private String reportAddress;

    @NotBlank(message = "上报人id不能为空")
    @Length(max = 32, message = "上报人id的最大长度为32字符")
    @ApiModelProperty(value = "上报人id(员工)")
    private String reportManId;

    @ApiModelProperty(value = "上报日期/销售日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate reportDate;

    @NotBlank(message = "竞品名称不能为空")
    @Length(max = 32, message = "竞品名称的最大长度为32字符")
    @ApiModelProperty(value = "竞品名称")
    private String goodsName;

    @NotBlank(message = "竞品单位不能为空")
    @Length(max = 10, message = "竞品单位的最大长度为32字符")
    @ApiModelProperty(value = "竞品单位")
    private String goodsUnit;

    @NotNull(message = "竞品数量不能为空")
    @Digits(integer = 8, fraction = 2, message = "竞品数量的整数位最大8位, 小数位最大2位")
    @Range(max = 1000000, message = "竞品数量在0至1000000之间")
    @ApiModelProperty(value = "竞品数量")
    private BigDecimal goodsNum;

    @NotNull(message = "竞品价格不能为空")
    @Digits(integer = 8, fraction = 2, message = "竞品价格的整数位最大8位, 小数位最大2位")
    @Range(max = 1000000, message = "竞品价格在0至1000000之间")
    @ApiModelProperty(value = "竞品价格")
    private BigDecimal tradePrice;

    @Length(max = 255, message = "备注信息的最大长度为255字符")
    @ApiModelProperty(value = "备注信息")
    private String remarks;

    @ApiModelProperty(value = "图片列表")
    private List<String> photoList;

    public DataReportDTO transToReport() {
        DataReportDTO report = new DataReportDTO();
        report.setReportType(DataReportEnum.competition);
        report.setCustomerSn(this.customerSn);
        report.setReportLocation(this.reportLocation);
        report.setReportAddress(this.reportAddress);
        report.setReportManId(this.reportManId);
        report.setReportDate(null == this.reportDate ? LocalDate.now() : this.reportDate);
//        report.setOperatorId(this.operatorId);
        report.setRemarks(this.remarks);
        return report;
    }

    /**
     * 无 id 及图片
     *
     * @return
     */
    public DataReportDetail transToDetail() {
        DataReportDetail detail = new DataReportDetail();
        detail.setGoodsName(this.goodsName);
        detail.setGoodsUnit(this.goodsUnit);
        detail.setTradePrice(this.tradePrice);
        detail.setGoodsNum(this.goodsNum);
        detail.setSalesGoodsType(SalesGoodsTypeEnum.normal);
        return detail;
    }

    /**
     * 修改时使用
     *
     * @param reportId
     * @return
     */
    public DataReport transTo(String reportId) {
        DataReport report = new DataReport();
        report.setId(reportId);
        report.setCustomerSn(this.customerSn);
        report.setReportLocation(this.reportLocation);
        report.setReportAddress(this.reportAddress);
        report.setReportManId(this.reportManId);
        if (null != this.reportDate) {
            report.setReportDate(this.reportDate);
        }
        // 修改后的上报都为待确认
        report.setReportStatus(DataReportStatusEnum.wait);
        report.setRemarks(this.remarks);
        return report;
    }

}
