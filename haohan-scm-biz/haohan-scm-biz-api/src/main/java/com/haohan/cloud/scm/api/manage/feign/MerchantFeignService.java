/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.manage.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.manage.entity.Merchant;
import com.haohan.cloud.scm.api.manage.req.MerchantFeignReq;
import com.haohan.cloud.scm.api.manage.req.MerchantReq;
import com.haohan.cloud.scm.api.manage.vo.MerchantVO;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 商家信息内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "MerchantFeignService", value = ScmServiceName.SCM_MANAGE)
public interface MerchantFeignService {


    /**
     * 通过id查询商家信息
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/Merchant/{id}", method = RequestMethod.GET)
    R<Merchant> getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 商家信息 列表信息
     * @param merchantReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/Merchant/fetchMerchantPage")
    R getMerchantPage(@RequestBody MerchantReq merchantReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 商家信息 列表信息
     * @param merchantReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/Merchant/fetchMerchantList")
    R getMerchantList(@RequestBody MerchantReq merchantReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增商家信息
     * @param merchant 商家信息
     * @return R
     */
    @PostMapping("/api/feign/Merchant/add")
    R<Merchant> save(@RequestBody Merchant merchant, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改商家信息
     * @param merchant 商家信息
     * @return R
     */
    @PostMapping("/api/feign/Merchant/update")
    R updateById(@RequestBody Merchant merchant, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除商家信息
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/Merchant/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/Merchant/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/Merchant/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param merchantReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/Merchant/countByMerchantReq")
    R countByMerchantReq(@RequestBody MerchantReq merchantReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param merchantReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/Merchant/getOneByMerchantReq")
    R<Merchant> getOneByMerchantReq(@RequestBody MerchantReq merchantReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param merchantList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/Merchant/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<Merchant> merchantList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 查询租户的平台商家
     *
     * @param from
     * @return
     */
    @GetMapping("/api/feign/Merchant/fetchPlatformMerchant")
    R<Merchant> fetchPlatformMerchant(@RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 创建平台的商家
     * @param merchant
     * @param from
     * @return
     */
    @PostMapping("/api/feign/Merchant/createWithPlatform")
    R<Merchant> createMerchantWithPlatform(@RequestBody Merchant merchant, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 根据merchantId 集合和 启用状态 查询列表
     *
     * @param req
     * @param from
     * @return
     */
    @PostMapping("/api/feign/Merchant/findMerchantList")
    R<List<MerchantVO>> findMerchantList(@RequestBody MerchantFeignReq req, @RequestHeader(SecurityConstants.FROM) String from);
}
