/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.supply.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.supply.entity.SupplierGrade;

/**
 * 供应商评级记录
 *
 * @author haohan
 * @date 2019-05-13 17:45:14
 */
public interface SupplierGradeService extends IService<SupplierGrade> {

}
