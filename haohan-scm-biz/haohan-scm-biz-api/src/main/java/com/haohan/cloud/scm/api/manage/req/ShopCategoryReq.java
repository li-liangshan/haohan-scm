/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.manage.req;

import com.haohan.cloud.scm.api.manage.entity.ShopCategory;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 店铺分类
 *
 * @author haohan
 * @date 2019-05-28 20:37:52
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "店铺分类")
public class ShopCategoryReq extends ShopCategory {

    private long pageSize;
    private long pageNo;




}
