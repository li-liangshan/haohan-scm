package com.haohan.cloud.scm.api.opc.req;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

/**
 * @author dy
 * @date 2019/5/27
 */
@Data
public class OpcSummaryOrderQueryReq {

    /**
     * 主键
     */
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 平台商家id
     */
    private String pmId;
    /**
     * 汇总单号
     */
    private String summaryOrderId;
    /**
     * 商品分类id
     */
    private String goodsCategoryId;
    /**
     * 采购日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime buyTime;
    /**
     * 送货日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime deliveryTime;
    /**
     * 状态
     */
    private String status;
    /**
     * 是否生成交易单
     */
    private String isGenTrade;
    /**
     * 采购批次
     */
    private String buySeq;
    /**
     * 汇总单状态:1.待处理2.采购中3.配送中.4已完成5.已关闭
     */
    private String summaryStatus;
    /**
     * 调配类型:1.采购2.库存
     */
    private String allocateType;
    /**
     * 汇总时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime summaryTime;
    /**
     * 完成时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime finishTime;
    /**
     * 商品ID(spu)
     */
    private String goodsId;
    /**
     * 商品规格id
     */
    private String goodsModelId;
    /**
     * 租户id
     */
    private Integer tenantId;
}
