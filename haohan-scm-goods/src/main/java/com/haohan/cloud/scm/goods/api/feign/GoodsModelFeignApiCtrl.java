/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.goods.api.feign;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.goods.dto.GoodsModelDTO;
import com.haohan.cloud.scm.api.goods.dto.GoodsSqlDTO;
import com.haohan.cloud.scm.api.goods.dto.GoodsStorageRankDTO;
import com.haohan.cloud.scm.api.goods.entity.GoodsModel;
import com.haohan.cloud.scm.api.goods.req.GoodsModelFeignReq;
import com.haohan.cloud.scm.api.goods.req.GoodsModelReq;
import com.haohan.cloud.scm.api.goods.vo.GoodsModelVO;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.haohan.cloud.scm.goods.core.GoodsModelCoreService;
import com.haohan.cloud.scm.goods.core.IScmGoodsService;
import com.haohan.cloud.scm.goods.service.GoodsModelService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 商品规格
 *
 * @author haohan
 * @date 2019-05-29 14:25:39
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/GoodsModel")
@Api(value = "goodsmodel", tags = "goodsmodel内部接口服务")
public class GoodsModelFeignApiCtrl {

    private final GoodsModelService goodsModelService;
    private final IScmGoodsService scmGoodsService;
    private final GoodsModelCoreService goodsModelCoreService;

    /**
     * 通过id查询商品规格 (不带平台定价)
     *
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(goodsModelService.getById(id));
    }

    /**
     * 修改商品规格
     *
     * @param goodsModel 商品规格
     * @return R
     */
    @Inner
    @SysLog("修改商品规格")
    @PostMapping("/update")
    public R updateById(@RequestBody GoodsModel goodsModel) {
        return new R<>(goodsModelService.updateById(goodsModel));
    }

    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param goodsModelReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @PostMapping("/countByGoodsModelReq")
    public R countByGoodsModelReq(@RequestBody GoodsModelReq goodsModelReq) {

        return new R<>(goodsModelService.count(Wrappers.query(goodsModelReq)));
    }


    /**
     * 根据对象条件，查询一条记录  (不带平台定价)
     *
     * @param goodsModelReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @PostMapping("/getOneByGoodsModelReq")
    public R getOneByGoodsModelReq(@RequestBody GoodsModelReq goodsModelReq) {

        return new R<>(goodsModelService.getOne(Wrappers.query(goodsModelReq), false));
    }

    /**
     * 通过id查询商品规格   (不带平台定价)
     * 关联 goods / goodsCategory
     * 返回带 商品名称/ 商品分类名称
     *
     * @param id id
     * @return R GoodsModelDTO
     */
    @Inner
    @GetMapping("/info/{id}")
    public R<GoodsModelDTO> getInfoById(@PathVariable("id") String id) {
        GoodsModelDTO goods = goodsModelService.getInfoById(id);
        R<GoodsModelDTO> resp = new R<>();
        resp.setData(goods);
        if (null == goods) {
            resp.setMsg("找不到对应id");
        }
        return resp;
    }

    /**
     * 根据sn查询商品规格信息  (不带平台定价)
     *
     * @param modelSn
     * @return
     */
    @Inner
    @GetMapping("/infoBySn/{modelSn}")
    public R<GoodsModelDTO> getInfoBySn(@PathVariable("modelSn") String modelSn) {
        return RUtil.success(goodsModelService.getInfoBySn(modelSn));
    }

    /**
     * 查询商品规格信息  根据商品名称 规格名称 规格单位   (使用平台定价)
     * 优先 规格名称匹配 无时 匹配 规格单位
     * 都无 或多条匹配时 返回null
     *
     * @param query 必须shopId goodsName
     * @return
     */
    @Inner
    @PostMapping("/fetchGoodsModelByName")
    public R<GoodsModelDTO> fetchGoodsModelByName(@RequestBody GoodsModelDTO query) {
        return RUtil.success(goodsModelCoreService.fetchGoodsModelByName(query));
    }

    /**
     * 查询商品库存排行
     *
     * @return
     */
    @Inner
    @GetMapping("/getStorageRank")
    public R<List<GoodsStorageRankDTO>> getStorageRank() {
        return new R<>(goodsModelService.getStorageRank());
    }

    /**
     * 查询商品库存预警数量
     *
     * @return
     */
    @Inner
    @GetMapping("/queryStorageWarn")
    public R<Integer> queryStorageWarn() {
        return new R<>(goodsModelService.queryStorageWarn());
    }

    @Inner
    @PostMapping("/queryGoodsStorageWarn")
    @ApiOperation(value = "查询库存预警")
    public R queryStorageWarn(Page page, GoodsModel model) {
        return new R<>(scmGoodsService.queryStorageWarn(page, model));
    }

    /**
     * 名称查询, 若无对应商品则新增 商品(下架状态, 分类为  未确认商品-> 导入商品)
     * (使用平台定价)
     *
     * @param goodsModelDTO goodsName / modelName /unit /price / shopId
     * @return
     */
    @Inner
    @PostMapping("/fetchModelByNameOrAdd")
    @ApiOperation(value = "根据名称查询商品规格")
    public R<GoodsModelDTO> fetchModelByNameOrAdd(@RequestBody GoodsModelDTO goodsModelDTO) {
        return R.ok(goodsModelCoreService.fetchModelByNameOrAdd(goodsModelDTO));
    }

    /**
     * 查询分类及子分类 的商品规格id列表
     *
     * @param categoryId
     * @return
     */
    @Inner
    @GetMapping("/fetchListByCategory/{categoryId}")
    public R<List<String>> fetchModelListByCategory(@PathVariable("categoryId") String categoryId) {
        return R.ok(goodsModelService.fetchModelListByCategory(categoryId));
    }

    /**
     * 根据 moderId 带 merchantId/shopId
     * (不带平台定价)
     *
     * @param req
     * @return 可null
     */
    @Inner
    @PostMapping("/fetchInfo")
    @ApiOperation(value = "根据id查询商品规格")
    public R<GoodsModelDTO> fetchInfo(@RequestBody GoodsModelFeignReq req) {
        if (StrUtil.isEmpty(req.getModelId())) {
            return R.failed("缺少modelId");
        }
        GoodsSqlDTO query = new GoodsSqlDTO();
        query.setModelId(req.getModelId());
        query.setMerchantId(req.getMerchantId());
        query.setShopId(req.getShopId());
        return R.ok(goodsModelService.getInfo(query));
    }

    /**
     * 根据 modelIdSet 查询规格列表  (不带平台定价)
     *
     * @param req modelIdSet
     * @return 可null
     */
    @Inner
    @PostMapping("/findGoodsModelList")
    @ApiOperation(value = "根据id查询商品规格")
    public R<List<GoodsModelDTO>> findGoodsModelList(@RequestBody GoodsModelFeignReq req) {
        if (CollUtil.isEmpty(req.getModelIdSet())) {
            return R.failed("缺少modelIdSet");
        }
        GoodsSqlDTO query = new GoodsSqlDTO();
        query.setModelIds(CollUtil.join(req.getModelIdSet(), StrUtil.COMMA));
        return R.ok(goodsModelService.findGoodsModelList(query));
    }

    @Inner
    @SysLog("规格库存扣减")
    @PostMapping("/modelStorageSubtract")
    @ApiOperation(value = "规格库存扣减")
    public R<Boolean> modelStorageSubtract(@RequestBody GoodsModelFeignReq req) {
        if (CollUtil.isEmpty(req.getModelList())) {
            return R.failed("缺少modelList");
        }
        return R.ok(goodsModelCoreService.modelStorageSubtractBatch(req.getModelList()));
    }

    /**
     * 根据id查询商品规格信息  (使用平台定价)
     * 无typeName
     *
     * @param req 必须 modelId, 可选buyerId/pricingMerchantId, pricingDate
     * @return
     */
    @Inner
    @PostMapping("/fetchModelVO")
    @ApiOperation(value = "根据id查询商品规格")
    public R<GoodsModelVO> fetchModelVO(@RequestBody GoodsModelFeignReq req) {
        if (StrUtil.isEmpty(req.getModelId())) {
            return R.failed("缺少modelId");
        }
        return RUtil.success(goodsModelCoreService.fetchModelVO(req.getModelId(), req.transToPricingReq()));
    }

}
