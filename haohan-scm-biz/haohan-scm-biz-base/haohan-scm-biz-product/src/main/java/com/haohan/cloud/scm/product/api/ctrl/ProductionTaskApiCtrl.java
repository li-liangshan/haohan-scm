package com.haohan.cloud.scm.product.api.ctrl;

import com.haohan.cloud.scm.api.product.SingleRealLossDTO;
import com.haohan.cloud.scm.api.product.entity.ProductionTask;
import com.haohan.cloud.scm.api.product.req.CreateDeliveryTaskReq;
import com.haohan.cloud.scm.api.product.req.CreateProcessingTaskReq;
import com.haohan.cloud.scm.api.product.resp.SingleRealLossResp;
import com.haohan.cloud.scm.product.core.IProductionTaskService;
import com.pig4cloud.pigx.common.core.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author cx
 * @date 2019/6/13
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/product/productionTask")
@Api(value = "ApiProductionTask", tags = "productionTask 生产任务操作api")
public class ProductionTaskApiCtrl {

    @Autowired
    @Lazy
    private IProductionTaskService iProductionTaskService;

    /**
     * 新增 生产部任务(任务类别: 加工)  根据 汇总商品需求/预计需求
     *
     * @param req
     * @return
     */
    @PostMapping("/createProductProcessingTask")
    @ApiOperation(value = "生成生产任务", notes = "新增 生产部任务(任务类别: 加工)  根据 汇总商品需求/预计需求")
    public R<ProductionTask> createProductProcessingTask(@Validated CreateProcessingTaskReq req) {
        return new R(iProductionTaskService.createProductProcessingTask(req));
    }

    /**
     * 新增 生产部任务(任务类别: 配送) 根据 汇总商品需求
     *
     * @param req
     * @return
     */
    @PostMapping("/createProductDeliveryTask")
    @ApiOperation(value = "生成生产任务", notes = "新增 生产部任务(任务类别: 配送)  根据 汇总商品需求")
    public R<ProductionTask> createProductDeliveryTask(@RequestBody @Validated CreateDeliveryTaskReq req) {
        return new R(iProductionTaskService.createProductDeliveryTask(req));
    }

    /**
     * 查询统计 单品实际损耗率
     *
     * @param dto
     * @return
     */
    @PostMapping("/querySingelRealLoss")
    @ApiOperation(value = "查询损耗率", notes = "查询统计 单品实际损耗率")
    public R<SingleRealLossResp> querySingelRealLoss(@Validated SingleRealLossDTO dto) {
        return new R(iProductionTaskService.querySingleRealLoss(dto));
    }
}
