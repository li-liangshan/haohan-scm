/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.manage.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.manage.entity.Industry;
import com.haohan.cloud.scm.api.manage.req.IndustryReq;
import com.haohan.cloud.scm.manage.service.IndustryService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 行业类型
 *
 * @author haohan
 * @date 2019-05-29 14:26:03
 */
@RestController
@AllArgsConstructor
@RequestMapping("/industry" )
@Api(value = "industry", tags = "industry管理")
public class IndustryController {

    private final IndustryService industryService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param industry 行业类型
     * @return
     */
    @GetMapping("/page" )
    public R getIndustryPage(Page page, Industry industry) {
        return new R<>(industryService.page(page, Wrappers.query(industry)));
    }


    /**
     * 通过id查询行业类型
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(industryService.getById(id));
    }

    /**
     * 新增行业类型
     * @param industry 行业类型
     * @return R
     */
    @SysLog("新增行业类型" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_industry_add')" )
    public R save(@RequestBody Industry industry) {
        return new R<>(industryService.save(industry));
    }

    /**
     * 修改行业类型
     * @param industry 行业类型
     * @return R
     */
    @SysLog("修改行业类型" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_industry_edit')" )
    public R updateById(@RequestBody Industry industry) {
        return new R<>(industryService.updateById(industry));
    }

    /**
     * 通过id删除行业类型
     * @param id id
     * @return R
     */
    @SysLog("删除行业类型" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_industry_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(industryService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除行业类型")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_industry_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(industryService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询行业类型")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(industryService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param industryReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询行业类型总记录}")
    @PostMapping("/countByIndustryReq")
    public R countByIndustryReq(@RequestBody IndustryReq industryReq) {

        return new R<>(industryService.count(Wrappers.query(industryReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param industryReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据industryReq查询一条货位信息表")
    @PostMapping("/getOneByIndustryReq")
    public R getOneByIndustryReq(@RequestBody IndustryReq industryReq) {

        return new R<>(industryService.getOne(Wrappers.query(industryReq), false));
    }


    /**
     * 批量修改OR插入
     * @param industryList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_industry_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<Industry> industryList) {

        return new R<>(industryService.saveOrUpdateBatch(industryList));
    }


}
