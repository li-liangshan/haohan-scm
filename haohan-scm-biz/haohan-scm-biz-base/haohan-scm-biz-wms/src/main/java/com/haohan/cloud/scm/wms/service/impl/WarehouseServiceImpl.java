/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.wms.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.constant.NumberPrefixConstant;
import com.haohan.cloud.scm.api.wms.entity.Warehouse;
import com.haohan.cloud.scm.common.tools.util.ScmIncrementUtil;
import com.haohan.cloud.scm.wms.mapper.WarehouseMapper;
import com.haohan.cloud.scm.wms.service.WarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

/**
 * 仓库信息表
 *
 * @author haohan
 * @date 2019-05-13 21:32:17
 */
@Service
public class WarehouseServiceImpl extends ServiceImpl<WarehouseMapper, Warehouse> implements WarehouseService {

    @Autowired
    @Lazy
    private ScmIncrementUtil scmIncrementUtil;

    @Override
    public boolean save(Warehouse entity) {
        if (StrUtil.isEmpty(entity.getWarehouseSn())) {
            String sn = scmIncrementUtil.inrcSnByClass(Warehouse.class, NumberPrefixConstant.WAREHOUSE_SN_PRE);
            entity.setWarehouseSn(sn);
        }
        return super.save(entity);
    }

}
