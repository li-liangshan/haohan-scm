package com.haohan.cloud.scm.saleb.api.ctrl;

import cn.hutool.core.util.StrUtil;
import com.haohan.cloud.scm.api.manage.entity.UPassport;
import com.haohan.cloud.scm.api.saleb.req.CountBuyerNumReq;
import com.haohan.cloud.scm.api.saleb.req.QueryBuyerStatusReq;
import com.haohan.cloud.scm.api.saleb.vo.BuyerVO;
import com.haohan.cloud.scm.common.tools.exception.ErrorDataException;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.haohan.cloud.scm.saleb.core.BuyerCoreService;
import com.haohan.cloud.scm.saleb.service.BuyerService;
import com.haohan.cloud.scm.saleb.utils.ScmSaleBUtils;
import com.pig4cloud.pigx.common.core.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author cx
 * @date 2019/7/9
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/saleb/buyer")
@Api(value = "ApiSalebBuyer", tags = "buyer相关操作")
public class SalebBuyerApiCtrl {
    private final ScmSaleBUtils scmSaleBUtils;
    private final BuyerService buyerService;
    private final BuyerCoreService buyerCoreService;

    /**
     * 查询通行证是否存在
     *
     * @param telephone
     * @return
     */
    @GetMapping("/queryUPassport")
    public R<UPassport> queryUPassport(String telephone) {
        return new R<>(scmSaleBUtils.queryUPassport(telephone));
    }

    /**
     * 查询用户是否存在
     *
     * @param telephone
     * @return
     */
    @GetMapping("/queryUser")
    public R<String> queryUser(String telephone) {
        return new R<>(scmSaleBUtils.queryUser(telephone));
    }

    /**
     * 查询B客户今日新增数
     *
     * @param req
     * @return
     */
    @PostMapping("/countBuyerNum")
    public R countBuyerNum(CountBuyerNumReq req) {

        return new R<>(buyerService.countBuyerNum(req));
    }

    /**
     * 查询采购商是否可下单
     *
     * @param req
     * @return
     */
    @GetMapping("/queryStatus")
    public R<Boolean> buyerStatus(@Validated QueryBuyerStatusReq req) {
        Integer num = buyerCoreService.countBuyerBill(req.getPmId(), req.getBuyerId());
        return RUtil.success(null != num && num == 0);
    }


    @GetMapping("/info")
    @ApiOperation(value = "获取采购商的详情")
    public R<BuyerVO> info(String buyerId) {
        if (StrUtil.isEmpty(buyerId)) {
            throw new ErrorDataException("缺少参数buyerId");
        }
        return RUtil.success(buyerCoreService.fetchInfoWithRole(buyerId));
    }
}
