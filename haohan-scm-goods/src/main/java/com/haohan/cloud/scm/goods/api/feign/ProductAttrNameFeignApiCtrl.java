/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.goods.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.goods.entity.ProductAttrName;
import com.haohan.cloud.scm.api.goods.req.ProductAttrNameReq;
import com.haohan.cloud.scm.goods.service.ProductAttrNameService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 商品库属性名
 *
 * @author haohan
 * @date 2019-05-29 14:26:18
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/ProductAttrName")
@Api(value = "productattrname", tags = "productattrname内部接口服务")
public class ProductAttrNameFeignApiCtrl {

    private final ProductAttrNameService productAttrNameService;


    /**
     * 通过id查询商品库属性名
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(productAttrNameService.getById(id));
    }


    /**
     * 分页查询 商品库属性名 列表信息
     * @param productAttrNameReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchProductAttrNamePage")
    public R getProductAttrNamePage(@RequestBody ProductAttrNameReq productAttrNameReq) {
        Page page = new Page(productAttrNameReq.getPageNo(), productAttrNameReq.getPageSize());
        ProductAttrName productAttrName =new ProductAttrName();
        BeanUtil.copyProperties(productAttrNameReq, productAttrName);

        return new R<>(productAttrNameService.page(page, Wrappers.query(productAttrName)));
    }


    /**
     * 全量查询 商品库属性名 列表信息
     * @param productAttrNameReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchProductAttrNameList")
    public R getProductAttrNameList(@RequestBody ProductAttrNameReq productAttrNameReq) {
        ProductAttrName productAttrName =new ProductAttrName();
        BeanUtil.copyProperties(productAttrNameReq, productAttrName);

        return new R<>(productAttrNameService.list(Wrappers.query(productAttrName)));
    }


    /**
     * 新增商品库属性名
     * @param productAttrName 商品库属性名
     * @return R
     */
    @Inner
    @SysLog("新增商品库属性名")
    @PostMapping("/add")
    public R save(@RequestBody ProductAttrName productAttrName) {
        return new R<>(productAttrNameService.save(productAttrName));
    }

    /**
     * 修改商品库属性名
     * @param productAttrName 商品库属性名
     * @return R
     */
    @Inner
    @SysLog("修改商品库属性名")
    @PostMapping("/update")
    public R updateById(@RequestBody ProductAttrName productAttrName) {
        return new R<>(productAttrNameService.updateById(productAttrName));
    }

    /**
     * 通过id删除商品库属性名
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除商品库属性名")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(productAttrNameService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除商品库属性名")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(productAttrNameService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询商品库属性名")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(productAttrNameService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param productAttrNameReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询商品库属性名总记录}")
    @PostMapping("/countByProductAttrNameReq")
    public R countByProductAttrNameReq(@RequestBody ProductAttrNameReq productAttrNameReq) {

        return new R<>(productAttrNameService.count(Wrappers.query(productAttrNameReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param productAttrNameReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据productAttrNameReq查询一条货位信息表")
    @PostMapping("/getOneByProductAttrNameReq")
    public R getOneByProductAttrNameReq(@RequestBody ProductAttrNameReq productAttrNameReq) {

        return new R<>(productAttrNameService.getOne(Wrappers.query(productAttrNameReq), false));
    }


    /**
     * 批量修改OR插入
     * @param productAttrNameList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<ProductAttrName> productAttrNameList) {

        return new R<>(productAttrNameService.saveOrUpdateBatch(productAttrNameList));
    }

}
