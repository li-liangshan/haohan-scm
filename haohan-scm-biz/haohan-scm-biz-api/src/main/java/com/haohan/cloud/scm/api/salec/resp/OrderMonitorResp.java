package com.haohan.cloud.scm.api.salec.resp;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @program: haohan-fresh-scm
 * @description: 订单实时监控返回
 * @author: Simon
 * @create: 2019-07-10
 **/
@Data
@AllArgsConstructor
@JsonRootName("data")
public class OrderMonitorResp implements Serializable {

    public OrderMonitorResp() { }


    /**
     * 用户ID-通行证
     */
    private String uid;
    /**
     *用户名称
     */
    private String name;

    /**
     *用户标识Id
     */
    private String userUnicode;

    /**
     *客户类型
     * C 端客户
     * B 端客户
     */
    private String userType;

    //用户头像
    private String headerImg;
    /**
     * b端下单时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime orderBTime;
    /**
     * c端下单时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Integer orderCTime;

  /**
   * 下单时间
   */
  private String orderTime;
  /**
   * 订单金额
   */
  private BigDecimal orderAmt;
  /**
   * 订单数量
   */
  private Integer goodsNum;
  /**
   * 经度
   */
  private String lng;
  /**
   * 维度
   */
  private String lat;

  /**
   * 订单号
   */
  private String orderId;

  /**
   * 订单状态
   */
  private String orderStatus;

  /**
   * 下单渠道
   *  wxmp 微信公众号
   *  wxapp 微信小程序
   *  pc 端
   */
    private String channel;

    private String content;


}
