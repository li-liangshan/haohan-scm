package com.haohan.cloud.scm.wecaht.wxapp.api;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.haohan.cloud.scm.api.bill.dto.BillInfoDTO;
import com.haohan.cloud.scm.api.constant.enums.saleb.BillTypeEnum;
import com.haohan.cloud.scm.api.goods.vo.GoodsVO;
import com.haohan.cloud.scm.api.saleb.vo.BuyOrderVO;
import com.haohan.cloud.scm.api.wechat.req.WxAdvanceBillReq;
import com.haohan.cloud.scm.api.wechat.req.WxBuyOrderEditReq;
import com.haohan.cloud.scm.api.wechat.req.WxBuyOrderReq;
import com.haohan.cloud.scm.api.wechat.req.WxGoodsQueryReq;
import com.haohan.cloud.scm.common.tools.constant.FirstGroup;
import com.haohan.cloud.scm.common.tools.constant.SecondGroup;
import com.haohan.cloud.scm.common.tools.constant.SingleGroup;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.haohan.cloud.scm.wecaht.wxapp.core.WechatBillCoreService;
import com.haohan.cloud.scm.wecaht.wxapp.core.WechatBuyOrderCoreService;
import com.pig4cloud.pigx.common.core.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @author dy
 * @date 2020/5/20
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/wechat/buyOrder")
@Api(value = "BuyOrderApiCtrl", tags = "微信小程序buyOrder")
public class WechatBuyOrderApiCtrl {

    private final WechatBillCoreService wechatBillCoreService;
    private final WechatBuyOrderCoreService wechatBuyOrderCoreService;

    @GetMapping("/page")
    public R<IPage<BuyOrderVO>> getBuyOrderPage(@Validated WxBuyOrderReq req) {
        return RUtil.success(wechatBuyOrderCoreService.findPage(req));
    }

    @GetMapping("/info")
    @ApiOperation(value = "查询订单详情")
    public R<BuyOrderVO> fetchInfo(@Validated(SingleGroup.class) WxBuyOrderReq req) {
        return RUtil.success(wechatBuyOrderCoreService.fetchInfo(req));
    }

    /**
     * 新增采购订单
     *
     * @param req
     * @return
     */
    @PostMapping("/info")
    public R<BuyOrderVO> addBuyOrder(@RequestBody @Validated(FirstGroup.class) WxBuyOrderEditReq req) {
        return RUtil.success(wechatBuyOrderCoreService.addBuyOrder(req));
    }
    /**
     * 修改采购订单
     *
     * @param req
     * @return
     */
    @PutMapping("/info")
    public R<Boolean> modifyBuyOrder(@RequestBody @Validated(SecondGroup.class) WxBuyOrderEditReq req) {
        return RUtil.success(wechatBuyOrderCoreService.modifyBuyOrder(req));
    }

    @DeleteMapping("/info")
    public R<Boolean> cancelBuyOrder(@Validated(SingleGroup.class) WxBuyOrderReq req) {
        return RUtil.success(wechatBuyOrderCoreService.cancelBuyOrder(req));
    }

    /**
     * 采购商 经常购买商品详情列表 带收藏状态
     *
     * @param req
     * @return
     */
    @GetMapping("/oftenPage")
    @ApiOperation(value = "采购商 经常购买商品详情列表 带收藏状态 ")
    public R<IPage<GoodsVO>> findOftenPage(@Validated WxGoodsQueryReq req) {
        return RUtil.success(wechatBuyOrderCoreService.findOftenPage(req));
    }

    /**
     * 根据采购订单创建预付应收账单
     * 可设置预付金额
     *
     * @param req
     * @return
     */
    @PostMapping("/advanceBill")
    @ApiOperation(value = "采购订单创建预付应收账单")
    public R<BillInfoDTO> createBuyOrderAdvanceBill(@Validated WxAdvanceBillReq req) {
        req.setBillType(BillTypeEnum.order);
        return RUtil.success(wechatBillCoreService.createReceivableBill(req));
    }
}
