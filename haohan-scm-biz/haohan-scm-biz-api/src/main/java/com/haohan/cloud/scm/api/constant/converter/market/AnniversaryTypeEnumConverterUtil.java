package com.haohan.cloud.scm.api.constant.converter.market;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.market.AnniversaryTypeEnum;

/**
 * @author xwx
 * @date 2019/6/5
 */
public class AnniversaryTypeEnumConverterUtil implements Converter<AnniversaryTypeEnum> {
    @Override
    public AnniversaryTypeEnum convert(Object o, AnniversaryTypeEnum anniversaryTypeEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return AnniversaryTypeEnum.getByType(o.toString());
    }
}
