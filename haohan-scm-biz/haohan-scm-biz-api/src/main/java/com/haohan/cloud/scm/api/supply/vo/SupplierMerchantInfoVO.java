package com.haohan.cloud.scm.api.supply.vo;

import com.haohan.cloud.scm.api.manage.entity.Shop;
import com.haohan.cloud.scm.api.manage.vo.ShopVO;
import com.haohan.cloud.scm.api.supply.entity.Supplier;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @author dy
 * @date 2020/4/16
 */
@Data
@NoArgsConstructor
public class SupplierMerchantInfoVO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "商家id")
    private String merchantId;

    @ApiModelProperty(value = "商家名称")
    private String merchantName;

    @ApiModelProperty(value = "店铺id", notes = "采购配送店铺")
    private String shopId;

    @ApiModelProperty(value = "店铺名称")
    private String shopName;

    @ApiModelProperty("店铺列表")
    private List<ShopVO> shopList;

    public SupplierMerchantInfoVO(Supplier supplier, Shop shop) {
        this.merchantId = supplier.getMerchantId();
        this.merchantName = supplier.getMerchantName();
        this.shopId = shop.getId();
        this.shopName = shop.getName();
    }
}
