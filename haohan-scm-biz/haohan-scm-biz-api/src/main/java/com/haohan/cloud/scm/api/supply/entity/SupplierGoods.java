/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.haohan.cloud.scm.api.supply.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.haohan.cloud.scm.api.constant.enums.common.UseStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.supply.SupplyTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

/**
 * 供应商货物表
 *
 * @author haohan
 * @date 2019-05-13 17:44:41
 */
@Data
@TableName("scm_sms_supplier_goods")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "供应商货物表")
public class SupplierGoods extends Model<SupplierGoods> {
    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 平台商家id (不使用)
     */
    private String pmId;
    /**
     * 供应商id  (暂不使用,都关联到商家上)
     */
    private String supplierId;
    /**
     * 供应商商家id
     */
    private String supplierMerchantId;
    /**
     * 商品id;spu  (平台)
     */
    private String goodsId;
    /**
     * 商品规格id;sku  (平台)
     */
    private String goodsModelId;
    /**
     * 供应商商品id
     */
    private String supplyGoodsId;
    /**
     * 供应商商品规格id;sku
     */
    @TableField(value = "supplier_model_id")
    private String supplyModelId;
    /**
     * 状态  启用状态 0.未启用 1.启用 2.待审核
     */
    private UseStatusEnum status;
    /**
     * 供应类型:0普通零售1协议零售2协议总价  (scm流程使用)
     */
    private SupplyTypeEnum supplyType;
    /**
     * 创建者
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createDate;
    /**
     * 更新者
     */
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateDate;
    /**
     * 备注信息
     */
    private String remarks;
    /**
     * 删除标记
     */
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;
    /**
     * 租户id
     */
    private Integer tenantId;

}
