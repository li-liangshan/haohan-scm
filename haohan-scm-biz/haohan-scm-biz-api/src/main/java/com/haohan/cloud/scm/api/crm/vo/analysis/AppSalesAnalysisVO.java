package com.haohan.cloud.scm.api.crm.vo.analysis;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author dy
 * @date 2019/11/7
 */
@Data
public class AppSalesAnalysisVO {

    @ApiModelProperty("今日")
    private SalesAnalysisVO today;

    @ApiModelProperty("昨日")
    private SalesAnalysisVO yesterday;

    @ApiModelProperty("本月")
    private SalesAnalysisVO currentMonth;

    @ApiModelProperty("上月")
    private SalesAnalysisVO lastMonth;

}
