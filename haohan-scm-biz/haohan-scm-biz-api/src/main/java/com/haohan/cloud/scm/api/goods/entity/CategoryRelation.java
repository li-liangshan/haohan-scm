/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.goods.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;

/**
 * 分类关系表
 *
 * @author haohan
 * @date 2020-04-08 17:47:17
 */
@Data
@TableName("scm_cms_category_relation")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "分类关系表")
public class CategoryRelation extends Model<CategoryRelation> {
    private static final long serialVersionUID = 1L;

    @Length(max = 64, message = "主键长度最大64字符")
    @ApiModelProperty(value = "主键")
    @TableId(type = IdType.INPUT)
    private String id;

    @Length(max = 64, message = "祖先节点长度最大64字符")
    @ApiModelProperty(value = "祖先节点")
    private String ancestor;

    @Length(max = 64, message = "后代节点长度最大64字符")
    @ApiModelProperty(value = "后代节点")
    private String descendant;

    @Length(max = 64, message = "类名长度最大64字符")
    @ApiModelProperty(value = "类名")
    private String className;

    @ApiModelProperty(value = "租户ID")
    private Integer tenantId;

}
