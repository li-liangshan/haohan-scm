package com.haohan.cloud.scm.api.crm.dto.analysis;

import com.haohan.cloud.scm.api.common.dto.SelfSqlDTO;
import com.haohan.cloud.scm.api.constant.enums.crm.DataReportEnum;
import com.haohan.cloud.scm.api.crm.entity.SalesReportAnalysisDetail;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Digits;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author dy
 * @date 2020/1/16
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel("销量上报客户统计")
public class SalesReportAnalysisDTO extends SelfSqlDTO {
    private static final long serialVersionUID = 1L;
    // 查询

    @ApiModelProperty(value = "上报类型  1.销量 ")
    private String reportType;

    @ApiModelProperty(value = "客户编码")
    private String customerSn;

    @ApiModelProperty(value = "排除的上报状态")
    private String excludeStatus;

    // 结果

    @Length(max = 64, message = "客户名称长度最大64字符")
    @ApiModelProperty(value = "客户名称")
    private String customerName;

    @Length(max = 255, message = "市场人员(所有上报员工名称)长度最大255字符")
    @ApiModelProperty(value = "市场人员(所有上报员工名称)")
    private String employeeNames;

    @Length(max = 64, message = "月份(yyyy-MM)长度最大64字符")
    @ApiModelProperty(value = "月份(yyyy-MM)")
    private String month;

    @Digits(integer = 10, fraction = 2, message = "销售金额的整数位最大10位, 小数位最大2位")
    @ApiModelProperty(value = "销售金额")
    private BigDecimal totalAmount;

    @Digits(integer = 10, fraction = 2, message = "赠品总金额的整数位最大10位, 小数位最大2位")
    @ApiModelProperty(value = "赠品总金额")
    private BigDecimal giftAmount;

    @Length(max = 1000, message = "赠品内容(赠送商品及数量)长度最大1000字符")
    @ApiModelProperty(value = "赠品内容(赠送商品及数量)")
    private String giftContent;

    @ApiModelProperty(value = "销量上报统计明细列表")
    private List<SalesReportAnalysisDetail> detailList;

    public SalesReportAnalysisDTO() {
        super();
        this.reportType = DataReportEnum.sales.getType();
    }

    public SalesReportAnalysisDTO(String customerSn) {
        super();
        this.reportType = DataReportEnum.sales.getType();
        this.customerSn = customerSn;
    }
}
