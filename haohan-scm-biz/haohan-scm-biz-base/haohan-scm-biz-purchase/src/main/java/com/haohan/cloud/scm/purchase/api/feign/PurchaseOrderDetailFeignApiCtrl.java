/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.purchase.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.opc.entity.SummaryOrder;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseOrderDetail;
import com.haohan.cloud.scm.api.purchase.req.CompleteTaskReq;
import com.haohan.cloud.scm.api.purchase.req.PurchaseOrderDetailReq;
import com.haohan.cloud.scm.purchase.core.IScmPurchaseOrderService;
import com.haohan.cloud.scm.purchase.service.PurchaseOrderDetailService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 采购部采购单明细
 *
 * @author haohan
 * @date 2019-05-29 13:35:25
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/PurchaseOrderDetail")
@Api(value = "purchaseorderdetail", tags = "purchaseorderdetail内部接口服务")
public class PurchaseOrderDetailFeignApiCtrl {

    private final PurchaseOrderDetailService purchaseOrderDetailService;

    private final IScmPurchaseOrderService iScmPurchaseOrderService;


    /**
     * 通过id查询采购部采购单明细
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(purchaseOrderDetailService.getById(id));
    }


    /**
     * 分页查询 采购部采购单明细 列表信息
     * @param purchaseOrderDetailReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchPurchaseOrderDetailPage")
    public R getPurchaseOrderDetailPage(@RequestBody PurchaseOrderDetailReq purchaseOrderDetailReq) {
        Page page = new Page(purchaseOrderDetailReq.getPageNo(), purchaseOrderDetailReq.getPageSize());
        PurchaseOrderDetail purchaseOrderDetail =new PurchaseOrderDetail();
        BeanUtil.copyProperties(purchaseOrderDetailReq, purchaseOrderDetail);

        return new R<>(purchaseOrderDetailService.page(page, Wrappers.query(purchaseOrderDetail)));
    }


    /**
     * 全量查询 采购部采购单明细 列表信息
     * @param purchaseOrderDetailReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchPurchaseOrderDetailList")
    public R getPurchaseOrderDetailList(@RequestBody PurchaseOrderDetailReq purchaseOrderDetailReq) {
        PurchaseOrderDetail purchaseOrderDetail =new PurchaseOrderDetail();
        BeanUtil.copyProperties(purchaseOrderDetailReq, purchaseOrderDetail);

        return new R<>(purchaseOrderDetailService.list(Wrappers.query(purchaseOrderDetail)));
    }


    /**
     * 新增采购部采购单明细
     * @param purchaseOrderDetail 采购部采购单明细
     * @return R
     */
    @Inner
    @SysLog("新增采购部采购单明细")
    @PostMapping("/add")
    public R save(@RequestBody PurchaseOrderDetail purchaseOrderDetail) {
        return new R<>(purchaseOrderDetailService.save(purchaseOrderDetail));
    }

    /**
     * 修改采购部采购单明细
     * @param purchaseOrderDetail 采购部采购单明细
     * @return R
     */
    @Inner
    @SysLog("修改采购部采购单明细")
    @PostMapping("/update")
    public R updateById(@RequestBody PurchaseOrderDetail purchaseOrderDetail) {
        return new R<>(purchaseOrderDetailService.updateById(purchaseOrderDetail));
    }

    /**
     * 通过id删除采购部采购单明细
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除采购部采购单明细")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(purchaseOrderDetailService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除采购部采购单明细")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(purchaseOrderDetailService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询采购部采购单明细")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(purchaseOrderDetailService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param purchaseOrderDetailReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询采购部采购单明细总记录}")
    @PostMapping("/countByPurchaseOrderDetailReq")
    public R countByPurchaseOrderDetailReq(@RequestBody PurchaseOrderDetailReq purchaseOrderDetailReq) {

        return new R<>(purchaseOrderDetailService.count(Wrappers.query(purchaseOrderDetailReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param purchaseOrderDetailReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据purchaseOrderDetailReq查询一条货位信息表")
    @PostMapping("/getOneByPurchaseOrderDetailReq")
    public R getOneByPurchaseOrderDetailReq(@RequestBody PurchaseOrderDetailReq purchaseOrderDetailReq) {

        return new R<>(purchaseOrderDetailService.getOne(Wrappers.query(purchaseOrderDetailReq), false));
    }


    /**
     * 批量修改OR插入
     * @param purchaseOrderDetailList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<PurchaseOrderDetail> purchaseOrderDetailList) {

        return new R<>(purchaseOrderDetailService.saveOrUpdateBatch(purchaseOrderDetailList));
    }

    /**
     * 根据汇总单生成采购明细
     * @param summaryOrder
     * @return
     */
    @Inner
    @SysLog("根据汇总单生成采购单明细")
    @PostMapping("/addPurchaseOrderDetail")
    public R addPurchaseOrderDetail(@RequestBody SummaryOrder summaryOrder){
        return new R<>(iScmPurchaseOrderService.addPurchaseOrderDetail(summaryOrder));
    }

    /**
     * 确认揽货
     * @param req
     * @return
     */
    @Inner
    @PostMapping("/deliveryTask")
    public R deliveryTask(@RequestBody CompleteTaskReq req){
        return new R(iScmPurchaseOrderService.deliveryTask(req));
    }

}
