/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.message.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.message.entity.ShortMessageTemplate;
import com.haohan.cloud.scm.api.message.req.ShortMessageTemplateReq;
import com.haohan.cloud.scm.message.service.ShortMessageTemplateService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 短信模板
 *
 * @author haohan
 * @date 2019-05-29 13:48:18
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/ShortMessageTemplate")
@Api(value = "shortmessagetemplate", tags = "shortmessagetemplate内部接口服务")
public class ShortMessageTemplateFeignApiCtrl {

    private final ShortMessageTemplateService shortMessageTemplateService;


    /**
     * 通过id查询短信模板
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(shortMessageTemplateService.getById(id));
    }


    /**
     * 分页查询 短信模板 列表信息
     * @param shortMessageTemplateReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchShortMessageTemplatePage")
    public R getShortMessageTemplatePage(@RequestBody ShortMessageTemplateReq shortMessageTemplateReq) {
        Page page = new Page(shortMessageTemplateReq.getPageNo(), shortMessageTemplateReq.getPageSize());
        ShortMessageTemplate shortMessageTemplate =new ShortMessageTemplate();
        BeanUtil.copyProperties(shortMessageTemplateReq, shortMessageTemplate);

        return new R<>(shortMessageTemplateService.page(page, Wrappers.query(shortMessageTemplate)));
    }


    /**
     * 全量查询 短信模板 列表信息
     * @param shortMessageTemplateReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchShortMessageTemplateList")
    public R getShortMessageTemplateList(@RequestBody ShortMessageTemplateReq shortMessageTemplateReq) {
        ShortMessageTemplate shortMessageTemplate =new ShortMessageTemplate();
        BeanUtil.copyProperties(shortMessageTemplateReq, shortMessageTemplate);

        return new R<>(shortMessageTemplateService.list(Wrappers.query(shortMessageTemplate)));
    }


    /**
     * 新增短信模板
     * @param shortMessageTemplate 短信模板
     * @return R
     */
    @Inner
    @SysLog("新增短信模板")
    @PostMapping("/add")
    public R save(@RequestBody ShortMessageTemplate shortMessageTemplate) {
        return new R<>(shortMessageTemplateService.save(shortMessageTemplate));
    }

    /**
     * 修改短信模板
     * @param shortMessageTemplate 短信模板
     * @return R
     */
    @Inner
    @SysLog("修改短信模板")
    @PostMapping("/update")
    public R updateById(@RequestBody ShortMessageTemplate shortMessageTemplate) {
        return new R<>(shortMessageTemplateService.updateById(shortMessageTemplate));
    }

    /**
     * 通过id删除短信模板
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除短信模板")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(shortMessageTemplateService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除短信模板")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(shortMessageTemplateService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询短信模板")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(shortMessageTemplateService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param shortMessageTemplateReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询短信模板总记录}")
    @PostMapping("/countByShortMessageTemplateReq")
    public R countByShortMessageTemplateReq(@RequestBody ShortMessageTemplateReq shortMessageTemplateReq) {

        return new R<>(shortMessageTemplateService.count(Wrappers.query(shortMessageTemplateReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param shortMessageTemplateReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据shortMessageTemplateReq查询一条货位信息表")
    @PostMapping("/getOneByShortMessageTemplateReq")
    public R getOneByShortMessageTemplateReq(@RequestBody ShortMessageTemplateReq shortMessageTemplateReq) {

        return new R<>(shortMessageTemplateService.getOne(Wrappers.query(shortMessageTemplateReq), false));
    }


    /**
     * 批量修改OR插入
     * @param shortMessageTemplateList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<ShortMessageTemplate> shortMessageTemplateList) {

        return new R<>(shortMessageTemplateService.saveOrUpdateBatch(shortMessageTemplateList));
    }

}
