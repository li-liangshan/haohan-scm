/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.wms.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.wms.entity.EnterWarehouseDetail;
import com.haohan.cloud.scm.api.wms.req.EnterWarehouseDetailReq;
import com.haohan.cloud.scm.wms.service.EnterWarehouseDetailService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 入库单明细
 *
 * @author haohan
 * @date 2019-05-29 13:22:08
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/EnterWarehouseDetail")
@Api(value = "enterwarehousedetail", tags = "enterwarehousedetail内部接口服务")
public class EnterWarehouseDetailFeignApiCtrl {

    private final EnterWarehouseDetailService enterWarehouseDetailService;


    /**
     * 通过id查询入库单明细
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(enterWarehouseDetailService.getById(id));
    }


    /**
     * 分页查询 入库单明细 列表信息
     * @param enterWarehouseDetailReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchEnterWarehouseDetailPage")
    public R getEnterWarehouseDetailPage(@RequestBody EnterWarehouseDetailReq enterWarehouseDetailReq) {
        Page page = new Page(enterWarehouseDetailReq.getPageNo(), enterWarehouseDetailReq.getPageSize());
        EnterWarehouseDetail enterWarehouseDetail =new EnterWarehouseDetail();
        BeanUtil.copyProperties(enterWarehouseDetailReq, enterWarehouseDetail);

        return new R<>(enterWarehouseDetailService.page(page, Wrappers.query(enterWarehouseDetail)));
    }


    /**
     * 全量查询 入库单明细 列表信息
     * @param enterWarehouseDetailReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchEnterWarehouseDetailList")
    public R getEnterWarehouseDetailList(@RequestBody EnterWarehouseDetailReq enterWarehouseDetailReq) {
        EnterWarehouseDetail enterWarehouseDetail =new EnterWarehouseDetail();
        BeanUtil.copyProperties(enterWarehouseDetailReq, enterWarehouseDetail);

        return new R<>(enterWarehouseDetailService.list(Wrappers.query(enterWarehouseDetail)));
    }


    /**
     * 新增入库单明细
     * @param enterWarehouseDetail 入库单明细
     * @return R
     */
    @Inner
    @SysLog("新增入库单明细")
    @PostMapping("/add")
    public R save(@RequestBody EnterWarehouseDetail enterWarehouseDetail) {
        return new R<>(enterWarehouseDetailService.save(enterWarehouseDetail));
    }

    /**
     * 修改入库单明细
     * @param enterWarehouseDetail 入库单明细
     * @return R
     */
    @Inner
    @SysLog("修改入库单明细")
    @PostMapping("/update")
    public R updateById(@RequestBody EnterWarehouseDetail enterWarehouseDetail) {
        return new R<>(enterWarehouseDetailService.updateById(enterWarehouseDetail));
    }

    /**
     * 通过id删除入库单明细
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除入库单明细")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(enterWarehouseDetailService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除入库单明细")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(enterWarehouseDetailService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询入库单明细")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(enterWarehouseDetailService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param enterWarehouseDetailReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询入库单明细总记录}")
    @PostMapping("/countByEnterWarehouseDetailReq")
    public R countByEnterWarehouseDetailReq(@RequestBody EnterWarehouseDetailReq enterWarehouseDetailReq) {

        return new R<>(enterWarehouseDetailService.count(Wrappers.query(enterWarehouseDetailReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param enterWarehouseDetailReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据enterWarehouseDetailReq查询一条货位信息表")
    @PostMapping("/getOneByEnterWarehouseDetailReq")
    public R getOneByEnterWarehouseDetailReq(@RequestBody EnterWarehouseDetailReq enterWarehouseDetailReq) {

        return new R<>(enterWarehouseDetailService.getOne(Wrappers.query(enterWarehouseDetailReq), false));
    }


    /**
     * 批量修改OR插入
     * @param enterWarehouseDetailList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<EnterWarehouseDetail> enterWarehouseDetailList) {

        return new R<>(enterWarehouseDetailService.saveOrUpdateBatch(enterWarehouseDetailList));
    }

}
