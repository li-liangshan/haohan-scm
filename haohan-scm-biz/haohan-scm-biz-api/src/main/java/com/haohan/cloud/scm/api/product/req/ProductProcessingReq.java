/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.product.req;

import com.haohan.cloud.scm.api.product.entity.ProductProcessing;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 货品加工记录表
 *
 * @author haohan
 * @date 2019-05-28 20:52:45
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "货品加工记录表")
public class ProductProcessingReq extends ProductProcessing {

    private long pageSize;
    private long pageNo;




}
