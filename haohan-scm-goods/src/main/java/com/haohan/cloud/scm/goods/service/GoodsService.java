/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.goods.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.goods.dto.GoodsExtDTO;
import com.haohan.cloud.scm.api.goods.dto.GoodsSqlDTO;
import com.haohan.cloud.scm.api.goods.entity.Goods;

/**
 * 商品
 *
 * @author haohan
 * @date 2019-05-13 18:46:46
 */
public interface GoodsService extends IService<Goods> {

    void flushGoodsCacheById(String goodsId);

    Goods fetchByName(String shopId, String goodsName);

    Goods fetchBySn(String goodsSn);

    IPage<GoodsExtDTO> findPage(GoodsSqlDTO query);
}
