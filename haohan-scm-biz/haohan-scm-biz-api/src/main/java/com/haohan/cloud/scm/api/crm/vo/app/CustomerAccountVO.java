package com.haohan.cloud.scm.api.crm.vo.app;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @author dy
 * @date 2020/1/18
 */
@Data
@NoArgsConstructor
public class CustomerAccountVO {

    @ApiModelProperty("总计欠款")
    private BigDecimal totalDept;

    @ApiModelProperty("销售单据")
    private AccountSalesVO sales;

    @ApiModelProperty("收款单据")
    private AccountSettlementVO settlement;

    public CustomerAccountVO(BigDecimal totalDept) {
        this.totalDept = null == totalDept ? BigDecimal.ZERO : totalDept;
    }
}
