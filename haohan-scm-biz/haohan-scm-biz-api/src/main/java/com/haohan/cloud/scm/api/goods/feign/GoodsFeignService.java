/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.goods.feign;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.goods.dto.GoodsExtDTO;
import com.haohan.cloud.scm.api.goods.entity.Goods;
import com.haohan.cloud.scm.api.goods.req.GoodsFeignReq;
import com.haohan.cloud.scm.api.goods.req.GoodsReq;
import com.haohan.cloud.scm.api.goods.vo.GoodsVO;
import com.haohan.cloud.scm.api.supply.entity.SupplierGoods;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 商品内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "GoodsFeignService", value = ScmServiceName.SCM_GOODS)
public interface GoodsFeignService {


    /**
     * 通过id查询商品   (不带平台定价)
     *
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/Goods/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改商品
     *
     * @param goods 商品
     * @return R
     */
    @PostMapping("/api/feign/Goods/updateSalecStatus")
    R<Boolean> updateSalecStatus(@RequestBody Goods goods, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param goodsReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/Goods/countByGoodsReq")
    R<Integer> countByGoodsReq(@RequestBody GoodsReq goodsReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 查询商品详情列表 带收藏状态  (使用平台定价)
     *
     * @param req  可选buyerId/pricingMerchantId, pricingDate
     * @param from
     * @return
     */
    @PostMapping("/api/feign/Goods/queryGoodsInfoList")
    R<Page<GoodsVO>> queryGoodsInfoList(@RequestBody GoodsFeignReq req, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 根据规格id查询商品信息  (使用平台定价)
     *
     * @param req  必须goodsModelId, 可选buyerId/pricingMerchantId, pricingDate
     * @param from
     * @return
     */
    @PostMapping("/api/feign/Goods/fetchGoodsInfoByModelId")
    R<GoodsVO> fetchGoodsInfoByModelId(@RequestBody GoodsFeignReq req, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 分页查询商品列表
     *
     * @param req
     * @param from
     * @return
     */
    @PostMapping("/api/feign/Goods/findExtPage")
    R<Page<GoodsExtDTO>> findExtPage(@RequestBody GoodsFeignReq req, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 商品信息 带规格及类型列表  (使用平台定价)
     *
     * @param req  必须goodsId, 可选buyerId/pricingMerchantId, pricingDate
     * @param from
     * @return
     */
    @PostMapping("/api/feign/Goods/fetchGoodsInfoById")
    R<GoodsVO> fetchGoodsInfoById(@RequestBody GoodsFeignReq req, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据供应商品创建平台商品
     *
     * @param req goodsId、shopId:创建商品的平台店铺， 为空时默认平台店铺
     * @param from
     * @return
     */
    @PostMapping("/api/feign/Goods/createGoodsBySupplyGoods")
    R<List<SupplierGoods>> createGoodsBySupplyGoods(@RequestBody GoodsFeignReq req, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 商品收藏列表(详情)  (使用平台定价)
     *
     * @param req  可选buyerId/pricingMerchantId, pricingDate
     * @param from
     * @return
     */
    @PostMapping("/api/feign/Goods/findCollectionsPage")
    R<Page<GoodsVO>> findCollectionsPage(@RequestBody GoodsFeignReq req, @RequestHeader(SecurityConstants.FROM) String from);

}
