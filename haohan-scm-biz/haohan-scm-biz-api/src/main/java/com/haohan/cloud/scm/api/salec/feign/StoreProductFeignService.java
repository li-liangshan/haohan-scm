/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.salec.feign;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.salec.entity.StoreProduct;
import com.haohan.cloud.scm.api.salec.req.StoreProductFeignReq;
import com.haohan.cloud.scm.api.salec.req.StoreProductReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 商品表内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "StoreProductFeignService", value = ScmServiceName.SCM_BIZ_SALEC)
public interface StoreProductFeignService {


    /**
     * 通过id查询商品表
     *
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/StoreProduct/{id}", method = RequestMethod.GET)
    R<StoreProduct> getById(@PathVariable("id") Integer id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 商品表 列表信息
     *
     * @param storeProductReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/StoreProduct/fetchStoreProductPage")
    R<Page<StoreProduct>> getStoreProductPage(@RequestBody StoreProductReq storeProductReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 商品表 列表信息
     *
     * @param storeProductReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/StoreProduct/fetchStoreProductList")
    R<List<StoreProduct>> getStoreProductList(@RequestBody StoreProductReq storeProductReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增商品表
     *
     * @param storeProduct 商品表
     * @return R
     */
    @PostMapping("/api/feign/StoreProduct/add")
    R<Boolean> save(@RequestBody StoreProduct storeProduct, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改商品表
     *
     * @param storeProduct 商品表
     * @return R
     */
    @PostMapping("/api/feign/StoreProduct/update")
    R<Boolean> updateById(@RequestBody StoreProduct storeProduct, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除商品表
     *
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/StoreProduct/delete/{id}")
    R<Boolean> removeById(@PathVariable("id") Integer id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/StoreProduct/batchDelete")
    R<Boolean> removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/StoreProduct/listByIds")
    R<List<StoreProduct>> listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param storeProductReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/StoreProduct/countByStoreProductReq")
    R<Integer> countByStoreProductReq(@RequestBody StoreProductReq storeProductReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param storeProductReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/StoreProduct/getOneByStoreProductReq")
    R<StoreProduct> getOneByStoreProductReq(@RequestBody StoreProductReq storeProductReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param storeProductList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/StoreProduct/saveOrUpdateBatch")
    R<Boolean> saveOrUpdateBatch(@RequestBody List<StoreProduct> storeProductList, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 商品上下架修改
     *
     * @param req
     * @param from
     * @return
     */
    @PostMapping("/api/feign/StoreProduct/marketable")
    R<Boolean> marketableChange(@RequestBody StoreProductFeignReq req, @RequestHeader(SecurityConstants.FROM) String from);


}
