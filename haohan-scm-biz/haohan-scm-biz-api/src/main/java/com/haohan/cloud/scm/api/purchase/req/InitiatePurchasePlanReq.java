package com.haohan.cloud.scm.api.purchase.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author cx
 * @date 2019/6/4
 */
@Data
@ApiModel(description = "发起采购计划需求")
public class InitiatePurchasePlanReq {

    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台商家ID" ,required = true)
    private String pmId;

    @ApiModelProperty(value = "商品规格ID",required = true)
    private String goodsModelId;

    @NotBlank(message = "methodType不能为空")
    @ApiModelProperty(value = "采购方式类型:1.竞价采购2.单品采购3.协议供应",required = true)
    private String methodType;

    @ApiModelProperty(value = "商品需求数量")
    private BigDecimal needBuyNum;

    @ApiModelProperty(value = "供应商ID")
    private String supplierId;

    @ApiModelProperty(value = "采购价")
    private BigDecimal buyPrice;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "竞价截止时间" , example = "2000-01-01 12:00:00")
    private LocalDateTime biddingEndTime;

    @ApiModelProperty(value = "用户ID")
    private String uId;


}
