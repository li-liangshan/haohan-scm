/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.opc.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.opc.entity.PlatformEmployee;
import com.haohan.cloud.scm.api.opc.req.PlatformEmployeeReq;
import com.haohan.cloud.scm.opc.service.PlatformEmployeeService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 平台员工管理
 *
 * @author haohan
 * @date 2019-05-30 10:23:01
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/PlatformEmployee")
@Api(value = "platformemployee", tags = "platformemployee内部接口服务")
public class PlatformEmployeeFeignApiCtrl {

    private final PlatformEmployeeService platformEmployeeService;


    /**
     * 通过id查询平台员工管理
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(platformEmployeeService.getById(id));
    }


    /**
     * 分页查询 平台员工管理 列表信息
     * @param platformEmployeeReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchPlatformEmployeePage")
    public R getPlatformEmployeePage(@RequestBody PlatformEmployeeReq platformEmployeeReq) {
        Page page = new Page(platformEmployeeReq.getPageNo(), platformEmployeeReq.getPageSize());
        PlatformEmployee platformEmployee =new PlatformEmployee();
        BeanUtil.copyProperties(platformEmployeeReq, platformEmployee);

        return new R<>(platformEmployeeService.page(page, Wrappers.query(platformEmployee)));
    }


    /**
     * 全量查询 平台员工管理 列表信息
     * @param platformEmployeeReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchPlatformEmployeeList")
    public R getPlatformEmployeeList(@RequestBody PlatformEmployeeReq platformEmployeeReq) {
        PlatformEmployee platformEmployee =new PlatformEmployee();
        BeanUtil.copyProperties(platformEmployeeReq, platformEmployee);

        return new R<>(platformEmployeeService.list(Wrappers.query(platformEmployee)));
    }


    /**
     * 新增平台员工管理
     * @param platformEmployee 平台员工管理
     * @return R
     */
    @Inner
    @SysLog("新增平台员工管理")
    @PostMapping("/add")
    public R save(@RequestBody PlatformEmployee platformEmployee) {
        return new R<>(platformEmployeeService.save(platformEmployee));
    }

    /**
     * 修改平台员工管理
     * @param platformEmployee 平台员工管理
     * @return R
     */
    @Inner
    @SysLog("修改平台员工管理")
    @PostMapping("/update")
    public R updateById(@RequestBody PlatformEmployee platformEmployee) {
        return new R<>(platformEmployeeService.updateById(platformEmployee));
    }

    /**
     * 通过id删除平台员工管理
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除平台员工管理")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(platformEmployeeService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除平台员工管理")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(platformEmployeeService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询平台员工管理")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(platformEmployeeService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param platformEmployeeReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询平台员工管理总记录}")
    @PostMapping("/countByPlatformEmployeeReq")
    public R countByPlatformEmployeeReq(@RequestBody PlatformEmployeeReq platformEmployeeReq) {

        return new R<>(platformEmployeeService.count(Wrappers.query(platformEmployeeReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param platformEmployeeReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据platformEmployeeReq查询一条货位信息表")
    @PostMapping("/getOneByPlatformEmployeeReq")
    public R getOneByPlatformEmployeeReq(@RequestBody PlatformEmployeeReq platformEmployeeReq) {

        return new R<>(platformEmployeeService.getOne(Wrappers.query(platformEmployeeReq), false));
    }


    /**
     * 批量修改OR插入
     * @param platformEmployeeList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<PlatformEmployee> platformEmployeeList) {

        return new R<>(platformEmployeeService.saveOrUpdateBatch(platformEmployeeList));
    }

}
