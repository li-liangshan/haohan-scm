package com.haohan.cloud.scm.bill.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.bill.dto.BillInfoDTO;
import com.haohan.cloud.scm.api.bill.dto.BillSqlDTO;
import com.haohan.cloud.scm.api.bill.entity.ReceivableBill;
import com.haohan.cloud.scm.api.constant.NumberPrefixConstant;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import com.haohan.cloud.scm.api.crm.vo.app.BillPageVO;
import com.haohan.cloud.scm.bill.mapper.ReceivableBillMapper;
import com.haohan.cloud.scm.bill.service.ReceivableBillService;
import com.haohan.cloud.scm.common.tools.util.ScmIncrementUtil;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author dy
 * @date 2019/11/27
 */
@Service
@AllArgsConstructor
public class ReceivableBillServiceImpl extends ServiceImpl<ReceivableBillMapper, ReceivableBill> implements ReceivableBillService {
    private final ScmIncrementUtil scmIncrementUtil;

    @Override
    public boolean save(ReceivableBill entity) {
        entity.setTenantId(null);
        if (StrUtil.isEmpty(entity.getBillSn())) {
            String sn = scmIncrementUtil.inrcSnByClass(ReceivableBill.class, NumberPrefixConstant.RECEIVABLE_BILL_SN_PRE);
            entity.setBillSn(sn);
        }
        return retBool(baseMapper.insert(entity));
    }

    @Override
    public boolean updateById(ReceivableBill entity) {
        // 不可修改项
        entity.setBillSn(null);
        entity.setTenantId(null);
        entity.setCreateBy(null);
        entity.setCreateDate(null);
        return retBool(baseMapper.updateById(entity));
    }


    @Override
    public ReceivableBill fetchBySn(String billSn) {
        return baseMapper.selectList(Wrappers.<ReceivableBill>query().lambda()
                .eq(ReceivableBill::getBillSn, billSn)).stream().findFirst().orElse(null);
    }

    /**
     * 查询订单的预付账单
     *
     * @param orderSn
     * @return
     */
    @Override
    public ReceivableBill fetchAdvanceByOrder(String orderSn) {
        return fetchByOrderSn(orderSn, YesNoEnum.yes);
    }

    private ReceivableBill fetchByOrderSn(String orderSn, YesNoEnum advanceFlag) {
        return baseMapper.selectList(Wrappers.<ReceivableBill>query().lambda()
                .eq(ReceivableBill::getAdvanceFlag, advanceFlag)
                .eq(ReceivableBill::getOrderSn, orderSn)).stream().findFirst().orElse(null);
    }

    /**
     * 查询订单的账单 (普通，非预付账单)
     *
     * @param orderSn
     * @return
     */
    @Override
    public ReceivableBill fetchNormalByOrder(String orderSn) {
        return fetchByOrderSn(orderSn, YesNoEnum.no);
    }

    /**
     * 按下单客户统计 账单金额
     *
     * @param params reviewStatus、billType、settlementStatus、customerIdSet
     *               excludeStatus
     *               current、size
     * @return customerId、customerName、billAmount
     */
    @Override
    public BillPageVO<BillInfoDTO> countBillByCustomer(BillSqlDTO params) {
        // 总数
        BillInfoDTO total = baseMapper.countBill(params);
        List<BillInfoDTO> list = baseMapper.countBillByCustomer(params);

        BillPageVO<BillInfoDTO> result = new BillPageVO<>(total.getTotal(), total.getBillAmount());
        result.setPage(params.getCurrent(), params.getSize());
        result.setRecords(list);
        return result;
    }

    /**
     * 账单统计 总数量、总金额
     *
     * @param params customerId、reviewStatus、billType、settlementStatus、customerIdSet
     *               excludeStatus、advanceFlag 、startDate、endDate(可单独使用)
     * @return total、billAmount
     */
    @Override
    public BillInfoDTO countBill(BillSqlDTO params) {
        return baseMapper.countBill(params);
    }

}
