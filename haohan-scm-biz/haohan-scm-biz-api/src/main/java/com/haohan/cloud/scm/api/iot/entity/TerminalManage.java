/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.haohan.cloud.scm.api.iot.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

/**
 * 终端设备管理
 *
 * @author haohan
 * @date 2019-05-13 19:14:39
 */
@Data
@TableName("scm_terminal_manage")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "终端设备管理")
public class TerminalManage extends Model<TerminalManage> {
private static final long serialVersionUID = 1L;

    /**
   * 主键
   */
    @TableId(type = IdType.INPUT)
    private String id;
    /**
   * 设备编号
   */
    private Integer terminalNo;
    /**
   * 设备类型
   */
    private String terminalType;
    /**
   * 设备名称
   */
    private String name;
    /**
   * 设备别名
   */
    private String alias;
    /**
   * SN码
   */
    private String snCode;
    /**
   * 制造厂商
   */
    private String producer;
    /**
   * IMEI
   */
    private String imeiCode;
    /**
   * 购货时间
   */
    private LocalDateTime purchaseTime;
    /**
   * 出库时间
   */
    private LocalDateTime sellTime;
    /**
   * 商家id
   */
    private String merchantId;
    /**
   * 店铺id
   */
    private String shopId;
    /**
   * 店铺名称
   */
    private String shopName;
    /**
   * 设备状态
   */
    private String status;
    /**
   * 设备备注
   */
    private String remark;
    /**
   * 创建者
   */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    /**
   * 创建时间
   */
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
   * 更新者
   */
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;
    /**
   * 更新时间
   */
    @ApiModelProperty(value = "更新时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateDate;
    /**
   * 备注信息
   */
    private String remarks;
    /**
   * 删除标记
   */
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;
    /**
   * 租户id
   */
    private Integer tenantId;

}
