/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.opc.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.haohan.cloud.scm.api.constant.enums.crm.SalesGoodsTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Digits;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 发货记录明细
 *
 * @author haohan
 * @date 2020-01-06 14:28:22
 */
@Data
@TableName("scm_ops_ship_record_detail")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "发货记录明细")
public class ShipRecordDetail extends Model<ShipRecordDetail> {
    private static final long serialVersionUID = 1L;

    @Length(max = 64, message = "主键长度最大64字符")
    @ApiModelProperty(value = "主键")
    @TableId(type = IdType.INPUT)
    private String id;

    @Length(max = 64, message = "发货记录编号长度最大64字符")
    @ApiModelProperty(value = "发货记录编号")
    private String shipRecordSn;

    @Length(max = 64, message = "商品id长度最大64字符")
    @ApiModelProperty(value = "商品id")
    private String goodsId;

    @Length(max = 64, message = "商品规格id长度最大64字符")
    @ApiModelProperty(value = "商品规格id")
    private String goodsModelId;

    @Length(max = 64, message = "商品名称长度最大64字符")
    @ApiModelProperty(value = "商品名称")
    private String goodsName;

    @Length(max = 64, message = "规格名称长度最大64字符")
    @ApiModelProperty(value = "规格名称")
    private String modelName;

    @Length(max = 64, message = "单位长度最大64字符")
    @ApiModelProperty(value = "单位")
    private String unit;

    @Length(max = 255, message = "商品图片长度最大255字符")
    @ApiModelProperty(value = "商品图片")
    private String goodsImg;

    @Digits(integer = 10, fraction = 2, message = "商品数量的整数位最大10位, 小数位最大2位")
    @ApiModelProperty(value = "商品数量")
    private BigDecimal goodsNum;

    @Digits(integer = 10, fraction = 2, message = "成交价格的整数位最大10位, 小数位最大2位")
    @ApiModelProperty(value = "成交价格")
    private BigDecimal dealPrice;

    @Digits(integer = 10, fraction = 2, message = "商品金额的整数位最大10位, 小数位最大2位")
    @ApiModelProperty(value = "商品金额")
    private BigDecimal amount;

    @ApiModelProperty(value = "商品销售类型:1.普通2.促销品3.赠品", notes = "暂无")
    private SalesGoodsTypeEnum salesType;

    @Length(max = 64, message = "创建者长度最大64字符")
    @ApiModelProperty(value = "创建者")
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;

    @Length(max = 64, message = "更新者长度最大64字符")
    @ApiModelProperty(value = "更新者")
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;

    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.UPDATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateDate;

    @Length(max = 255, message = "备注信息长度最大255字符")
    @ApiModelProperty(value = "备注信息")
    private String remarks;

    @Length(max = 1, message = "删除标记长度最大1字符")
    @ApiModelProperty(value = "删除标记")
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;

    @ApiModelProperty(value = "租户id")
    private Integer tenantId;

}
