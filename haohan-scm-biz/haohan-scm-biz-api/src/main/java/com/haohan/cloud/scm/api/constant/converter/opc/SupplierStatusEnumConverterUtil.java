package com.haohan.cloud.scm.api.constant.converter.opc;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.supply.SupplierStatusEnum;

/**
 * @author xwx
 * @date 2019/6/4
 */
public class SupplierStatusEnumConverterUtil implements Converter<SupplierStatusEnum> {
    @Override
    public SupplierStatusEnum convert(Object o, SupplierStatusEnum supplierStatusEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return SupplierStatusEnum.getByType(o.toString());
    }
}
