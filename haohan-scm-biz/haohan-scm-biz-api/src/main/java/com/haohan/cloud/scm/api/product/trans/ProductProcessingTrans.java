package com.haohan.cloud.scm.api.product.trans;

import com.haohan.cloud.scm.api.constant.ScmCommonConstant;
import com.haohan.cloud.scm.api.constant.enums.product.*;
import com.haohan.cloud.scm.api.goods.dto.GoodsModelDTO;
import com.haohan.cloud.scm.api.manage.entity.UPassport;
import com.haohan.cloud.scm.api.product.entity.ProductInfo;
import com.haohan.cloud.scm.api.product.entity.ProductLossRecord;
import com.haohan.cloud.scm.api.product.entity.ProductProcessing;
import com.haohan.cloud.scm.api.product.entity.Recipe;

import java.time.LocalDateTime;

/**
 * @author cx
 * @date 2019/6/17
 */
public class ProductProcessingTrans {

    public static void addProductInfoTrans(GoodsModelDTO goodsModelDTO, Recipe recipe, ProductInfo productInfo){
        productInfo.setGoodsId(goodsModelDTO.getGoodsId());
        productInfo.setGoodsModelId(recipe.getGoodsModelId());
        productInfo.setProductName(goodsModelDTO.getGoodsName() + "|" + goodsModelDTO.getModelName());
        productInfo.setUnit(goodsModelDTO.getModelUnit());
        productInfo.setProductStatus(ProductStatusEnum.normal);
        productInfo.setProductQuality(ProductQialityEnum.normal);
        productInfo.setProductPlaceStatus(ProductPlaceStatusEnum.product);
        productInfo.setProductType(ProductTypeEnum.handle);
        productInfo.setPurchasePrice(goodsModelDTO.getCostPrice());
        productInfo.setProcessingType(ProcessingTypeEnum.formula);
        // 来源货品编号设置  此时为初始货品
        productInfo.setSourceProductSn(ScmCommonConstant.ORIGINAL_PRODUCT_SN);

    }

    public static void addProcessingTrans(Recipe recipe, UPassport uPassport, ProductProcessing processing){
        processing.setProcessingTime(LocalDateTime.now());
        processing.setProcessingType(ProcessingTypeEnum.formula);
        processing.setRecipeSn(recipe.getRecipeSn());
        processing.setRecipeName(recipe.getRecipeName());
        processing.setOperatorName(uPassport.getLoginName());
        processing.setOperatorId(uPassport.getId());
    }

    public static void addLossRecord(ProductInfo info, ProductLossRecord record){
        record.setProductSn(info.getProductSn());
        record.setAfterProductSn(info.getProductSn());
        record.setLossProductSn(info.getProductSn());
        record.setLossType(LossTypeEnum.handle);
        record.setUnit(info.getUnit());
        record.setOriginalNumber(info.getProductNumber());
        record.setRecordTime(LocalDateTime.now());
    }

}

