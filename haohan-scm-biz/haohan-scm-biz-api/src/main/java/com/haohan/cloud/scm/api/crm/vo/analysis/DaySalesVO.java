package com.haohan.cloud.scm.api.crm.vo.analysis;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author dy
 * @date 2019/11/8
 */
@Data
@Api("日销售情况")
public class DaySalesVO {

    @ApiModelProperty(value = "订单金额")
    private BigDecimal orderAmount;

    @ApiModelProperty(value = "回款金额")
    private BigDecimal backAmount;

    @ApiModelProperty(value = "订单数")
    private Integer orderNum;

    @ApiModelProperty(value = "员工销售情况列表")
    private List<EmployeeSalesVO> list;

}
