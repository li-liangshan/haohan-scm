package com.haohan.cloud.scm.api.constant.enums.market;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xwx
 * @date 2019/5/30
 */
@Getter
@AllArgsConstructor
public enum SexEnum implements IBaseEnum {
    /**
     * 性别:1男2女
     */
    man("1", "男"),
    woman("2", "女");

    private static final Map<String, SexEnum> MAP = new HashMap<>(8);
    private static final Map<String, SexEnum> DESC_MAP = new HashMap<>(8);

    static {
        for (SexEnum e : SexEnum.values()) {
            MAP.put(e.getType(), e);
            DESC_MAP.put(e.getDesc(), e);
        }
    }

    @JsonCreator
    public static SexEnum getByType(String type) {
        return MAP.get(type);
    }

    public static SexEnum getByDesc(String desc) {
        return DESC_MAP.get(desc);
    }

    /**
     * 类型
     */
    @EnumValue
    @JsonValue
    private String type;
    /**
     * 描述
     */

    private String desc;
}
