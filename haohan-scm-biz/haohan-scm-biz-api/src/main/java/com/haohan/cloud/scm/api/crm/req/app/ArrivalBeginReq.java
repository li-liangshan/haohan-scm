package com.haohan.cloud.scm.api.crm.req.app;

import com.haohan.cloud.scm.api.constant.ScmCommonConstant;
import com.haohan.cloud.scm.api.crm.entity.CustomerVisit;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * @author dy
 * @date 2020/1/3
 */
@Data
public class ArrivalBeginReq {

    @NotBlank(message = "客户编号不能为空")
    @Length(max = 32, message = "客户编号长度最大32字符")
    @ApiModelProperty(value = "客户编号")
    private String customerSn;

    @NotBlank(message = "拜访员工id不能为空")
    @Length(max = 32, message = "拜访员工id长度最大32字符")
    @ApiModelProperty(value = "拜访员工id")
    private String employeeId;

    @NotBlank(message = "抵达地址定位不能为空")
    @Pattern(regexp = ScmCommonConstant.POSITION_PATTEN, message = "抵达地址定位, 经纬度格式有误, 经度正负180度之间, 纬度正负90度之间, 弧度数表示")
    @ApiModelProperty(value = "抵达地址定位")
    private String arrivalPosition;

    @NotBlank(message = "抵达地址不能为空")
    @Length(max = 64, message = "抵达地址长度最大64字符")
    @ApiModelProperty(value = "抵达地址")
    private String arrivalAddress;

    public CustomerVisit transTo() {
        CustomerVisit visit = new CustomerVisit();
        visit.setCustomerSn(this.customerSn);
        visit.setEmployeeId(this.employeeId);
        visit.setArrivalPosition(this.arrivalPosition);
        visit.setArrivalAddress(this.arrivalAddress);
        return visit;
    }
}
