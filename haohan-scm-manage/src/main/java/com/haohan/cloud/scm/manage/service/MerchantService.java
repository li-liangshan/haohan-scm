/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.manage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.manage.entity.Merchant;

/**
 * 商家信息
 *
 * @author haohan
 * @date 2019-05-13 17:37:44
 */
public interface MerchantService extends IService<Merchant> {

    Merchant fetchPlatformMerchant();

    void flushPlatformMerchant();

}
