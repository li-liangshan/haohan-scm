package com.haohan.cloud.scm.api.constant.converter.product;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.product.ProductionEmployeeTypeEnum;

/**
 * @author xwx
 * @date 2019/6/4
 */
public class ProductionEmployeeTypeEnumConverterUtil implements Converter<ProductionEmployeeTypeEnum> {
    @Override
    public ProductionEmployeeTypeEnum convert(Object o, ProductionEmployeeTypeEnum productionEmployeeTypeEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return ProductionEmployeeTypeEnum.getByType(o.toString());
    }
}
