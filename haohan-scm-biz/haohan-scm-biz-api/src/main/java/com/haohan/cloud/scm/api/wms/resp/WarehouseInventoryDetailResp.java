package com.haohan.cloud.scm.api.wms.resp;

import com.haohan.cloud.scm.api.wms.entity.WarehouseInventoryDetail;
import lombok.Data;

import java.util.List;

/**
 * @author cx
 * @date 2019/8/10
 */

@Data
public class WarehouseInventoryDetailResp {

    private List<WarehouseInventoryDetail> list;
}
