package com.haohan.cloud.scm.api.constant.converter.message;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.message.MessageTypeEnum;

/**
 * @author xwx
 * @date 2019/6/5
 */
public class MessageTypeEnumConverterUtil implements Converter<MessageTypeEnum> {
    @Override
    public MessageTypeEnum convert(Object o, MessageTypeEnum messageTypeEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return MessageTypeEnum.getByType(o.toString());
    }
}
