package com.haohan.cloud.scm.api.supply.req.order;

import com.haohan.cloud.scm.api.constant.enums.common.DeliveryTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.saleb.BuyOrderStatusEnum;
import com.haohan.cloud.scm.api.supply.entity.SupplyOrder;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * @author dy
 * @date 2020/4/24
 */
@Data
public class SupplyOrderQueryReq implements Serializable {
    private static final long serialVersionUID = 1L;

    @Length(max = 32, message = "供应订单编号的长度最大为32字符")
    @ApiModelProperty(value = "供应订单编号")
    private String supplySn;

    @Length(max = 32, message = "供应商id的长度最大为32字符")
    @ApiModelProperty(value = "供应商id")
    private String supplierId;

    @ApiModelProperty(value = "状态")
    private BuyOrderStatusEnum status;

    @ApiModelProperty(value = "配送方式")
    private DeliveryTypeEnum deliveryType;

    @ApiModelProperty(value = "供应日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate supplyDate;

    // 非eq

    @Length(max = 32, message = "供应商名称的长度最大为32字符")
    @ApiModelProperty(value = "供应商名称")
    private String supplierName;

    @Length(max = 32, message = "联系人的长度最大为32字符")
    @ApiModelProperty(value = "联系人")
    private String contact;

    @Length(max = 32, message = "联系电话的长度最大为32字符")
    @ApiModelProperty(value = "联系电话")
    private String telephone;

    @Length(max = 32, message = "供应地址的长度最大为32字符")
    @ApiModelProperty(value = "供应地址")
    private String address;

    public SupplyOrder transTo() {
        SupplyOrder order = new SupplyOrder();
        order.setSupplySn(this.supplySn);
        order.setSupplierId(this.supplierId);
        order.setStatus(this.status);
        order.setDeliveryType(this.deliveryType);
        order.setSupplyDate(this.supplyDate);
        return order;
    }
}
