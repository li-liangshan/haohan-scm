package com.haohan.cloud.scm.api.supply.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author xwx
 * @date 2019/6/18
 */
@Data
@ApiModel(description = "供应商品库存详情")
public class QueryGoodsRepertoryDetailReq {

    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台id",required = true)
    private String pmId;

    @NotBlank(message = "id不能为空")
    @ApiModelProperty(value = "协议供应商品主键",required = true)
    private String id;

    @ApiModelProperty(value = "通行证id",required = true)
    private String uid;

}
