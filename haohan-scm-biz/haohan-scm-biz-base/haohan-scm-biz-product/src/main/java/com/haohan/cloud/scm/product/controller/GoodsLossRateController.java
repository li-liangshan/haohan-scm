/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.product.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.product.entity.GoodsLossRate;
import com.haohan.cloud.scm.api.product.req.GoodsLossRateReq;
import com.haohan.cloud.scm.product.service.GoodsLossRateService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 单品损耗率
 *
 * @author haohan
 * @date 2019-05-29 14:44:31
 */
@RestController
@AllArgsConstructor
@RequestMapping("/goodslossrate" )
@Api(value = "goodslossrate", tags = "goodslossrate管理")
public class GoodsLossRateController {

    private final GoodsLossRateService goodsLossRateService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param goodsLossRate 单品损耗率
     * @return
     */
    @GetMapping("/page" )
    public R getGoodsLossRatePage(Page page, GoodsLossRate goodsLossRate) {
        return new R<>(goodsLossRateService.page(page, Wrappers.query(goodsLossRate)));
    }


    /**
     * 通过id查询单品损耗率
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(goodsLossRateService.getById(id));
    }

    /**
     * 新增单品损耗率
     * @param goodsLossRate 单品损耗率
     * @return R
     */
    @SysLog("新增单品损耗率" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_goodslossrate_add')" )
    public R save(@RequestBody GoodsLossRate goodsLossRate) {
        return new R<>(goodsLossRateService.save(goodsLossRate));
    }

    /**
     * 修改单品损耗率
     * @param goodsLossRate 单品损耗率
     * @return R
     */
    @SysLog("修改单品损耗率" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_goodslossrate_edit')" )
    public R updateById(@RequestBody GoodsLossRate goodsLossRate) {
        return new R<>(goodsLossRateService.updateById(goodsLossRate));
    }

    /**
     * 通过id删除单品损耗率
     * @param id id
     * @return R
     */
    @SysLog("删除单品损耗率" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_goodslossrate_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(goodsLossRateService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除单品损耗率")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_goodslossrate_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(goodsLossRateService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询单品损耗率")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(goodsLossRateService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param goodsLossRateReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询单品损耗率总记录}")
    @PostMapping("/countByGoodsLossRateReq")
    public R countByGoodsLossRateReq(@RequestBody GoodsLossRateReq goodsLossRateReq) {

        return new R<>(goodsLossRateService.count(Wrappers.query(goodsLossRateReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param goodsLossRateReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据goodsLossRateReq查询一条货位信息表")
    @PostMapping("/getOneByGoodsLossRateReq")
    public R getOneByGoodsLossRateReq(@RequestBody GoodsLossRateReq goodsLossRateReq) {

        return new R<>(goodsLossRateService.getOne(Wrappers.query(goodsLossRateReq), false));
    }


    /**
     * 批量修改OR插入
     * @param goodsLossRateList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_goodslossrate_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<GoodsLossRate> goodsLossRateList) {

        return new R<>(goodsLossRateService.saveOrUpdateBatch(goodsLossRateList));
    }


}
