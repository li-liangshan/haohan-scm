package com.haohan.cloud.scm.api.saleb.req.buyer;

import com.haohan.cloud.scm.api.saleb.entity.Buyer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author dy
 * @date 2020/6/18
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class BuyerFeignReq extends BuyerEditReq {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "通行证id")
    private String uid;

    @ApiModelProperty(value = "商家名称")
    private String merchantName;

    @Override
    public Buyer transTo() {
        Buyer buyer = super.transTo();
        buyer.setPassportId(this.uid);
        buyer.setMerchantName(this.merchantName);
        return buyer;
    }

}
