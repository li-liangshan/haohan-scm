package com.haohan.cloud.scm.opc.core;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.constant.enums.bill.OrderTypeEnum;
import com.haohan.cloud.scm.api.opc.entity.ShipRecord;
import com.haohan.cloud.scm.api.opc.entity.Shipper;
import com.haohan.cloud.scm.api.opc.req.ship.EditShipRecordReq;
import com.haohan.cloud.scm.api.opc.req.ship.QueryShipRecordReq;
import com.haohan.cloud.scm.api.opc.req.ship.QueryShipperReq;
import com.haohan.cloud.scm.api.opc.vo.ShipRecordVO;

import java.util.Set;

/**
 * @author dy
 * @date 2020/1/6
 */
public interface ShipManageCoreService {

    /**
     * 分页查询
     *
     * @param page
     * @param req
     * @return
     */
    IPage<ShipRecord> fetchPage(Page page, QueryShipRecordReq req);

    /**
     * 详情
     *
     * @param shipSn
     * @return
     */
    ShipRecordVO fetchInfo(String shipSn);


    ShipRecordVO fetchInfoByOrderSn(String orderSn);

    /**
     * 分页查询发货人
     *
     * @param page
     * @param req
     * @return
     */
    IPage<Shipper> fetchShipperPage(Page page, QueryShipperReq req);

    /**
     * 根据订单创建发货记录(已存在发货记录则修改)
     *
     * @param orderSn
     * @param orderType
     * @return
     */
    ShipRecord createShipRecordByOrder(String orderSn, OrderTypeEnum orderType);

    /**
     * 根据订单创建发货记录(批量)
     *
     * @param orderSnSet
     * @param orderType
     * @return
     */
    boolean createShipRecordBatchByOrder(Set<String> orderSnSet, OrderTypeEnum orderType);

    /**
     * 完成发货操作
     *
     * @param req
     * @return
     */
    ShipRecord completeShip(EditShipRecordReq req);

    /**
     * 删除发货记录 (不为已发货)
     *
     * @param orderSn
     * @param orderType
     * @return
     */
    boolean deleteShipRecordByOrder(String orderSn, OrderTypeEnum orderType);

    boolean updateMerchantName(String pmId, String pmName);

    /**
     * 根据订单信息更新发货记录  (不为已发货)
     *
     * @param orderSn
     * @param orderType
     * @return
     */
    boolean updateShipRecordByOrder(String orderSn, OrderTypeEnum orderType);
}
