/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.crm.feign;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.crm.entity.SalesContract;
import com.haohan.cloud.scm.api.crm.req.SalesContractReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 销售合同内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "SalesContractFeignService", value = ScmServiceName.SCM_CRM)
public interface SalesContractFeignService {


    /**
     * 通过id查询销售合同
     *
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/SalesContract/{id}", method = RequestMethod.GET)
    R<SalesContract> getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 销售合同 列表信息
     *
     * @param salesContractReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/SalesContract/fetchSalesContractPage")
    R<Page<SalesContract>> getSalesContractPage(@RequestBody SalesContractReq salesContractReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 销售合同 列表信息
     *
     * @param salesContractReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/SalesContract/fetchSalesContractList")
    R<List<SalesContract>> getSalesContractList(@RequestBody SalesContractReq salesContractReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增销售合同
     *
     * @param salesContract 销售合同
     * @return R
     */
    @PostMapping("/api/feign/SalesContract/add")
    R<Boolean> save(@RequestBody SalesContract salesContract, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改销售合同
     *
     * @param salesContract 销售合同
     * @return R
     */
    @PostMapping("/api/feign/SalesContract/update")
    R<Boolean> updateById(@RequestBody SalesContract salesContract, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除销售合同
     *
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/SalesContract/delete/{id}")
    R<Boolean> removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/SalesContract/batchDelete")
    R<Boolean> removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/SalesContract/listByIds")
    R<List<SalesContract>> listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param salesContractReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/SalesContract/countBySalesContractReq")
    R<Integer> countBySalesContractReq(@RequestBody SalesContractReq salesContractReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param salesContractReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/SalesContract/getOneBySalesContractReq")
    R<SalesContract> getOneBySalesContractReq(@RequestBody SalesContractReq salesContractReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param salesContractList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/SalesContract/saveOrUpdateBatch")
    R<Boolean> saveOrUpdateBatch(@RequestBody List<SalesContract> salesContractList, @RequestHeader(SecurityConstants.FROM) String from);


}
