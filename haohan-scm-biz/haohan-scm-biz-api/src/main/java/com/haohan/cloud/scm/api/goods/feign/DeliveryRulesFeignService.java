/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.goods.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.goods.entity.DeliveryRules;
import com.haohan.cloud.scm.api.goods.req.DeliveryRulesReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 配送规则内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "DeliveryRulesFeignService", value = ScmServiceName.SCM_GOODS)
public interface DeliveryRulesFeignService {


    /**
     * 通过id查询配送规则
     *
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/DeliveryRules/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 配送规则 列表信息
     *
     * @param deliveryRulesReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/DeliveryRules/fetchDeliveryRulesPage")
    R getDeliveryRulesPage(@RequestBody DeliveryRulesReq deliveryRulesReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 配送规则 列表信息
     *
     * @param deliveryRulesReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/DeliveryRules/fetchDeliveryRulesList")
    R getDeliveryRulesList(@RequestBody DeliveryRulesReq deliveryRulesReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增配送规则
     *
     * @param deliveryRules 配送规则
     * @return R
     */
    @PostMapping("/api/feign/DeliveryRules/add")
    R save(@RequestBody DeliveryRules deliveryRules, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改配送规则
     *
     * @param deliveryRules 配送规则
     * @return R
     */
    @PostMapping("/api/feign/DeliveryRules/update")
    R updateById(@RequestBody DeliveryRules deliveryRules, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除配送规则
     *
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/DeliveryRules/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/DeliveryRules/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/DeliveryRules/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param deliveryRulesReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/DeliveryRules/countByDeliveryRulesReq")
    R countByDeliveryRulesReq(@RequestBody DeliveryRulesReq deliveryRulesReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param deliveryRulesReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/DeliveryRules/getOneByDeliveryRulesReq")
    R getOneByDeliveryRulesReq(@RequestBody DeliveryRulesReq deliveryRulesReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param deliveryRulesList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/DeliveryRules/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<DeliveryRules> deliveryRulesList, @RequestHeader(SecurityConstants.FROM) String from);


}
