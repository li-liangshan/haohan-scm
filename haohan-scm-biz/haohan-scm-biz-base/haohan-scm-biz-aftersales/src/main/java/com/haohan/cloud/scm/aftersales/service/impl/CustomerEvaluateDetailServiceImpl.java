/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.aftersales.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.aftersales.mapper.CustomerEvaluateDetailMapper;
import com.haohan.cloud.scm.aftersales.service.CustomerEvaluateDetailService;
import com.haohan.cloud.scm.api.aftersales.entity.CustomerEvaluateDetail;
import org.springframework.stereotype.Service;

/**
 * 客户服务评分明细
 *
 * @author haohan
 * @date 2019-05-13 20:27:10
 */
@Service
public class CustomerEvaluateDetailServiceImpl extends ServiceImpl<CustomerEvaluateDetailMapper, CustomerEvaluateDetail> implements CustomerEvaluateDetailService {

}
