package com.haohan.cloud.scm.api.crm.vo.analysis;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author dy
 * @date 2019/11/7
 */
@Data
@Api("app展示汇总统计")
public class SummaryAndTodayAppVO {

    @ApiModelProperty(value = "总拜访数")
    private Integer visitNum;

    @ApiModelProperty(value = "客户总数")
    private Integer customerNum;

    @ApiModelProperty(value = "客户订单总数")
    private Integer orderNum;

    @ApiModelProperty(value = "总销售金额")
    private BigDecimal salesAmount;

    @ApiModelProperty(value = "今日拜访数")
    private Integer todayVisitNum;

    @ApiModelProperty(value = "今日客户新增数")
    private Integer todayCustomerNum;

    @ApiModelProperty(value = "今日订单数")
    private Integer todayOrderNum;

    @ApiModelProperty(value = "今日销售金额")
    private BigDecimal todaySalesAmount;

}
