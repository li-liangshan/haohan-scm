/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.opc.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.opc.entity.CostOrder;
import com.haohan.cloud.scm.api.opc.req.CostOrderReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 成本核算单内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "CostOrderFeignService", value = ScmServiceName.SCM_BIZ_OPC)
public interface CostOrderFeignService {


    /**
     * 通过id查询成本核算单
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/CostOrder/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 成本核算单 列表信息
     * @param costOrderReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/CostOrder/fetchCostOrderPage")
    R getCostOrderPage(@RequestBody CostOrderReq costOrderReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 成本核算单 列表信息
     * @param costOrderReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/CostOrder/fetchCostOrderList")
    R getCostOrderList(@RequestBody CostOrderReq costOrderReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增成本核算单
     * @param costOrder 成本核算单
     * @return R
     */
    @PostMapping("/api/feign/CostOrder/add")
    R save(@RequestBody CostOrder costOrder, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改成本核算单
     * @param costOrder 成本核算单
     * @return R
     */
    @PostMapping("/api/feign/CostOrder/update")
    R updateById(@RequestBody CostOrder costOrder, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除成本核算单
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/CostOrder/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/CostOrder/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/CostOrder/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param costOrderReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/CostOrder/countByCostOrderReq")
    R countByCostOrderReq(@RequestBody CostOrderReq costOrderReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param costOrderReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/CostOrder/getOneByCostOrderReq")
    R getOneByCostOrderReq(@RequestBody CostOrderReq costOrderReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param costOrderList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/CostOrder/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<CostOrder> costOrderList, @RequestHeader(SecurityConstants.FROM) String from);


}
