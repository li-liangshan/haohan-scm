/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.manage.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.manage.entity.UserOpenPlatform;
import com.haohan.cloud.scm.api.manage.req.UserOpenPlatformReq;
import com.haohan.cloud.scm.manage.service.UserOpenPlatformService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 第三方开放平台用户管理
 *
 * @author haohan
 * @date 2019-05-29 14:29:38
 */
@RestController
@AllArgsConstructor
@RequestMapping("/useropenplatform" )
@Api(value = "useropenplatform", tags = "useropenplatform管理")
public class UserOpenPlatformController {

    private final UserOpenPlatformService userOpenPlatformService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param userOpenPlatform 第三方开放平台用户管理
     * @return
     */
    @GetMapping("/page" )
    public R getUserOpenPlatformPage(Page page, UserOpenPlatform userOpenPlatform) {
        return new R<>(userOpenPlatformService.page(page, Wrappers.query(userOpenPlatform)));
    }


    /**
     * 通过id查询第三方开放平台用户管理
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(userOpenPlatformService.getById(id));
    }

    /**
     * 新增第三方开放平台用户管理
     * @param userOpenPlatform 第三方开放平台用户管理
     * @return R
     */
    @SysLog("新增第三方开放平台用户管理" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_useropenplatform_add')" )
    public R save(@RequestBody UserOpenPlatform userOpenPlatform) {
        return new R<>(userOpenPlatformService.save(userOpenPlatform));
    }

    /**
     * 修改第三方开放平台用户管理
     * @param userOpenPlatform 第三方开放平台用户管理
     * @return R
     */
    @SysLog("修改第三方开放平台用户管理" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_useropenplatform_edit')" )
    public R updateById(@RequestBody UserOpenPlatform userOpenPlatform) {
        return new R<>(userOpenPlatformService.updateById(userOpenPlatform));
    }

    /**
     * 通过id删除第三方开放平台用户管理
     * @param id id
     * @return R
     */
    @SysLog("删除第三方开放平台用户管理" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_useropenplatform_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(userOpenPlatformService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除第三方开放平台用户管理")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_useropenplatform_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(userOpenPlatformService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询第三方开放平台用户管理")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(userOpenPlatformService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param userOpenPlatformReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询第三方开放平台用户管理总记录}")
    @PostMapping("/countByUserOpenPlatformReq")
    public R countByUserOpenPlatformReq(@RequestBody UserOpenPlatformReq userOpenPlatformReq) {

        return new R<>(userOpenPlatformService.count(Wrappers.query(userOpenPlatformReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param userOpenPlatformReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据userOpenPlatformReq查询一条货位信息表")
    @PostMapping("/getOneByUserOpenPlatformReq")
    public R getOneByUserOpenPlatformReq(@RequestBody UserOpenPlatformReq userOpenPlatformReq) {

        return new R<>(userOpenPlatformService.getOne(Wrappers.query(userOpenPlatformReq), false));
    }


    /**
     * 批量修改OR插入
     * @param userOpenPlatformList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_useropenplatform_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<UserOpenPlatform> userOpenPlatformList) {

        return new R<>(userOpenPlatformService.saveOrUpdateBatch(userOpenPlatformList));
    }


}
