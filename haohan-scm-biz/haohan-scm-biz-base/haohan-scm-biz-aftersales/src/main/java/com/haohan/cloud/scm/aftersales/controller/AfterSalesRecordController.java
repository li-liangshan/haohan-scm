/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.aftersales.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.aftersales.service.AfterSalesRecordService;
import com.haohan.cloud.scm.api.aftersales.entity.AfterSalesRecord;
import com.haohan.cloud.scm.api.aftersales.req.AfterSalesRecordReq;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 售后记录
 *
 * @author haohan
 * @date 2019-05-30 10:24:29
 */
@RestController
@AllArgsConstructor
@RequestMapping("/aftersalesrecord" )
@Api(value = "aftersalesrecord", tags = "aftersalesrecord管理")
public class AfterSalesRecordController {

    private final AfterSalesRecordService afterSalesRecordService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param afterSalesRecord 售后记录
     * @return
     */
    @GetMapping("/page" )
    public R getAfterSalesRecordPage(Page page, AfterSalesRecord afterSalesRecord) {
        return new R<>(afterSalesRecordService.page(page, Wrappers.query(afterSalesRecord)));
    }


    /**
     * 通过id查询售后记录
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(afterSalesRecordService.getById(id));
    }

    /**
     * 新增售后记录
     * @param afterSalesRecord 售后记录
     * @return R
     */
    @SysLog("新增售后记录" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_aftersalesrecord_add')" )
    public R save(@RequestBody AfterSalesRecord afterSalesRecord) {
        return new R<>(afterSalesRecordService.save(afterSalesRecord));
    }

    /**
     * 修改售后记录
     * @param afterSalesRecord 售后记录
     * @return R
     */
    @SysLog("修改售后记录" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_aftersalesrecord_edit')" )
    public R updateById(@RequestBody AfterSalesRecord afterSalesRecord) {
        return new R<>(afterSalesRecordService.updateById(afterSalesRecord));
    }

    /**
     * 通过id删除售后记录
     * @param id id
     * @return R
     */
    @SysLog("删除售后记录" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_aftersalesrecord_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(afterSalesRecordService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除售后记录")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_aftersalesrecord_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(afterSalesRecordService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询售后记录")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(afterSalesRecordService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param afterSalesRecordReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询售后记录总记录}")
    @PostMapping("/countByAfterSalesRecordReq")
    public R countByAfterSalesRecordReq(@RequestBody AfterSalesRecordReq afterSalesRecordReq) {

        return new R<>(afterSalesRecordService.count(Wrappers.query(afterSalesRecordReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param afterSalesRecordReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据afterSalesRecordReq查询一条货位信息表")
    @PostMapping("/getOneByAfterSalesRecordReq")
    public R getOneByAfterSalesRecordReq(@RequestBody AfterSalesRecordReq afterSalesRecordReq) {

        return new R<>(afterSalesRecordService.getOne(Wrappers.query(afterSalesRecordReq), false));
    }


    /**
     * 批量修改OR插入
     * @param afterSalesRecordList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_aftersalesrecord_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<AfterSalesRecord> afterSalesRecordList) {

        return new R<>(afterSalesRecordService.saveOrUpdateBatch(afterSalesRecordList));
    }


}
