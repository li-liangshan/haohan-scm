package com.haohan.cloud.scm.api.crm.vo.analysis;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * @author dy
 * @date 2020/1/10
 * 分析结果 分页扩展 总金额、总数量
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class AnalysisPageVO<T> extends Page<T> {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "总金额")
    private BigDecimal totalAmount;

    @ApiModelProperty(value = "总数量")
    private Integer totalNum;

    public AnalysisPageVO() {
        super();
        this.totalAmount = BigDecimal.ZERO;
        this.totalNum = 0;
    }

    public AnalysisPageVO(long total, BigDecimal totalAmount, Integer totalNum) {
        super();
        this.setTotal(total);
        this.totalAmount = totalAmount;
        this.totalNum = totalNum;
    }

    public void setPage(long current, long size) {
        this.setCurrent(current);
        this.setSize(size);
    }

    public void copyProperty(AnalysisPageVO entity) {
        this.setCurrent(entity.getCurrent());
        this.setSize(entity.getSize());
        this.setTotal(entity.getTotal());
        this.totalAmount = entity.getTotalAmount();
        this.totalNum = entity.getTotalNum();
    }


}
