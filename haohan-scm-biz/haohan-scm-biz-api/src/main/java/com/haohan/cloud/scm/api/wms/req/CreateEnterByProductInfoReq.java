package com.haohan.cloud.scm.api.wms.req;

import com.haohan.cloud.scm.api.product.entity.ProductInfo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @author xwx
 * @date 2019/6/14
 */
@Data
@ApiModel(description = "创建入库单 根据货品信息")
public class CreateEnterByProductInfoReq {

    @NotEmpty(message = "list不能为空")
    @ApiModelProperty(value = "货品信息集合" , required = true)
    private List<ProductInfo> list;

    @NotBlank(message = "warehouseSn不能为空")
    @ApiModelProperty(value = "仓库编号" , required = true)
    private String warehouseSn;

    @ApiModelProperty(value = "入库申请人id")
    private String applicantId;

    @ApiModelProperty(value = "入库申请人名称")
    private String applicantName;

    @ApiModelProperty(value = "验收人id")
    private String auditorId;

    @ApiModelProperty(value = "验收人名称")
    private String auditorName;

}
