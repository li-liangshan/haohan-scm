package com.haohan.cloud.scm.api.common.req.pay;

import lombok.Data;

/**
 * @program: haohan-fresh-scm
 * @description: 支付结果
 * @author: Simon
 * @create: 2019-07-26
 **/
@Data
public class PayResultReq extends BasePayParams {


}
