package com.haohan.cloud.scm.api.wms.req;

import com.haohan.cloud.scm.api.constant.enums.product.PalletPutTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.product.PalletTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author xwx
 * @date 2019/6/13
 */
@Data
@ApiModel(description = "货品放入托盘操作")
public class ProductEnterReq {
    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台id" , required = true)
    private String pmId;

    @NotBlank(message = "productSn不能为空")
    @ApiModelProperty(value = "货品编号" , required = true)
    private String productSn;

    @ApiModelProperty(value = "托盘编号")
    private String palletSn;

    @ApiModelProperty(value = "托盘类型:1木质2.塑料3.金属")
    private PalletTypeEnum palletType;

    @ApiModelProperty(value = "托盘放置类型:1.货位2.暂存点3.其他")
    private PalletPutTypeEnum palletPutType;

    @ApiModelProperty(value = "仓库编号")
    private String warehouseSn;

}
