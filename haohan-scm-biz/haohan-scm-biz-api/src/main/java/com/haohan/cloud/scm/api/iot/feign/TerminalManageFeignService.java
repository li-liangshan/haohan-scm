/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.iot.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.iot.entity.TerminalManage;
import com.haohan.cloud.scm.api.iot.req.TerminalManageReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 终端设备管理内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "TerminalManageFeignService", value = ScmServiceName.SCM_IOT)
public interface TerminalManageFeignService {


    /**
     * 通过id查询终端设备管理
     *
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/TerminalManage/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 终端设备管理 列表信息
     *
     * @param terminalManageReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/TerminalManage/fetchTerminalManagePage")
    R getTerminalManagePage(@RequestBody TerminalManageReq terminalManageReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 终端设备管理 列表信息
     *
     * @param terminalManageReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/TerminalManage/fetchTerminalManageList")
    R getTerminalManageList(@RequestBody TerminalManageReq terminalManageReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增终端设备管理
     *
     * @param terminalManage 终端设备管理
     * @return R
     */
    @PostMapping("/api/feign/TerminalManage/add")
    R save(@RequestBody TerminalManage terminalManage, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改终端设备管理
     *
     * @param terminalManage 终端设备管理
     * @return R
     */
    @PostMapping("/api/feign/TerminalManage/update")
    R updateById(@RequestBody TerminalManage terminalManage, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除终端设备管理
     *
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/TerminalManage/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/TerminalManage/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/TerminalManage/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param terminalManageReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/TerminalManage/countByTerminalManageReq")
    R countByTerminalManageReq(@RequestBody TerminalManageReq terminalManageReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param terminalManageReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/TerminalManage/getOneByTerminalManageReq")
    R getOneByTerminalManageReq(@RequestBody TerminalManageReq terminalManageReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param terminalManageList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/TerminalManage/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<TerminalManage> terminalManageList, @RequestHeader(SecurityConstants.FROM) String from);


}
