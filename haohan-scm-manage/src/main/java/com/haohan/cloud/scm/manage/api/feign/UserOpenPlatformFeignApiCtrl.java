/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.manage.api.feign;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.haohan.cloud.scm.api.manage.entity.UserOpenPlatform;
import com.haohan.cloud.scm.api.manage.req.UserOpenPlatformReq;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.haohan.cloud.scm.manage.service.UserOpenPlatformService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.data.tenant.TenantContextHolder;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 第三方开放平台用户管理
 *
 * @author haohan
 * @date 2019-05-29 14:29:38
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/UserOpenPlatform")
@Api(value = "useropenplatform", tags = "useropenplatform内部接口服务")
public class UserOpenPlatformFeignApiCtrl {

    private final UserOpenPlatformService userOpenPlatformService;


//    /**
//     * 通过id查询第三方开放平台用户管理
//     * @param id id
//     * @return R
//     */
//    @Inner
//    @GetMapping("/{id}")
//    public R getById(@PathVariable("id") String id) {
//        return new R<>(userOpenPlatformService.getById(id));
//    }
//
//
//    /**
//     * 分页查询 第三方开放平台用户管理 列表信息
//     * @param userOpenPlatformReq 请求对象
//     * @return
//     */
//    @Inner
//    @PostMapping("/fetchUserOpenPlatformPage")
//    public R getUserOpenPlatformPage(@RequestBody UserOpenPlatformReq userOpenPlatformReq) {
//        Page page = new Page(userOpenPlatformReq.getPageNo(), userOpenPlatformReq.getPageSize());
//        UserOpenPlatform userOpenPlatform =new UserOpenPlatform();
//        BeanUtil.copyProperties(userOpenPlatformReq, userOpenPlatform);
//
//        return new R<>(userOpenPlatformService.page(page, Wrappers.query(userOpenPlatform)));
//    }
//
//
//    /**
//     * 全量查询 第三方开放平台用户管理 列表信息
//     * @param userOpenPlatformReq 请求对象
//     * @return
//     */
//    @Inner
//    @PostMapping("/fetchUserOpenPlatformList")
//    public R getUserOpenPlatformList(@RequestBody UserOpenPlatformReq userOpenPlatformReq) {
//        UserOpenPlatform userOpenPlatform =new UserOpenPlatform();
//        BeanUtil.copyProperties(userOpenPlatformReq, userOpenPlatform);
//
//        return new R<>(userOpenPlatformService.list(Wrappers.query(userOpenPlatform)));
//    }
//
//
//    /**
//     * 新增第三方开放平台用户管理
//     * @param userOpenPlatform 第三方开放平台用户管理
//     * @return R
//     */
//    @Inner
//    @SysLog("新增第三方开放平台用户管理")
//    @PostMapping("/add")
//    public R save(@RequestBody UserOpenPlatform userOpenPlatform) {
//        return new R<>(userOpenPlatformService.save(userOpenPlatform));
//    }
//
//    /**
//     * 修改第三方开放平台用户管理
//     * @param userOpenPlatform 第三方开放平台用户管理
//     * @return R
//     */
//    @Inner
//    @SysLog("修改第三方开放平台用户管理")
//    @PostMapping("/update")
//    public R updateById(@RequestBody UserOpenPlatform userOpenPlatform) {
//        return new R<>(userOpenPlatformService.updateById(userOpenPlatform));
//    }
//
//    /**
//     * 通过id删除第三方开放平台用户管理
//     * @param id id
//     * @return R
//     */
//    @Inner
//    @SysLog("删除第三方开放平台用户管理")
//    @PostMapping("/delete/{id}")
//    public R removeById(@PathVariable String id) {
//        return new R<>(userOpenPlatformService.removeById(id));
//    }
//
//    /**
//      * 删除（根据ID 批量删除)
//      * @param idList 主键ID列表
//      * @return R
//      */
//    @Inner
//    @SysLog("批量删除第三方开放平台用户管理")
//    @PostMapping("/batchDelete")
//    public R removeByIds(@RequestBody List<String> idList) {
//        return new R<>(userOpenPlatformService.removeByIds(idList));
//    }
//
//
//    /**
//    * 批量查询（根据IDS）
//    * @param idList 主键ID列表
//    * @return R
//    */
//    @Inner
//    @SysLog("根据IDS批量查询第三方开放平台用户管理")
//    @PostMapping("/listByIds")
//    public R listByIds(@RequestBody List<String> idList) {
//        return new R<>(userOpenPlatformService.listByIds(idList));
//    }
//
//
//    /**
//     * 根据 Wrapper 条件，查询总记录数
//     * @param userOpenPlatformReq 实体对象,可以为空
//     * @return R
//     */
//    @Inner
//    @SysLog("查询第三方开放平台用户管理总记录}")
//    @PostMapping("/countByUserOpenPlatformReq")
//    public R countByUserOpenPlatformReq(@RequestBody UserOpenPlatformReq userOpenPlatformReq) {
//
//        return new R<>(userOpenPlatformService.count(Wrappers.query(userOpenPlatformReq)));
//    }


    /**
     * 查询开放平台用户 (可根据openId、uid、appId查询)
     * 不限制tenantId
     *
     * @param userOpenPlatformReq 实体对象
     * @return R
     */
    @Inner
    @PostMapping("/getOneByUserOpenPlatformReq")
    public R<UserOpenPlatform> getOneByUserOpenPlatformReq(@RequestBody UserOpenPlatformReq userOpenPlatformReq) {
        // 处理tenantId, 目前多租户共用表, 修改时间倒序
        Integer tenantId = TenantContextHolder.getTenantId();
        TenantContextHolder.setTenantId(null);
        UserOpenPlatform userOpenPlatform = userOpenPlatformService.list(Wrappers.<UserOpenPlatform>query(userOpenPlatformReq).lambda()
                .orderByDesc(UserOpenPlatform::getUpdateDate)
        ).stream().findFirst().orElse(null);
        TenantContextHolder.setTenantId(tenantId);
        return RUtil.success(userOpenPlatform);
    }


//    /**
//     * 批量修改OR插入
//     * @param userOpenPlatformList 实体对象集合 大小不超过1000条数据
//     * @return R
//     */
//    @Inner
//    @SysLog("批量修改OR插入货位信息表")
//    @PostMapping("/saveOrUpdateBatch")
//    public R saveOrUpdateBatch(@RequestBody List<UserOpenPlatform> userOpenPlatformList) {
//
//        return new R<>(userOpenPlatformService.saveOrUpdateBatch(userOpenPlatformList));
//    }

}
