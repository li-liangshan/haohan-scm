package com.haohan.cloud.scm.api.wms.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import java.time.LocalDate;

/**
 * @author dy
 * @date 2019/7/8
 */
@Data
@ApiModel("查询待出库列表")
public class QueryWaitExitReq {

    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台id", required = true)
    private String pmId;
    /**
     * 送货时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "送货时间", example = "2019-07-08")
    private LocalDate deliveryDate;
    /**
     * 送货批次:0第一批1第二批
     */
    @ApiModelProperty(value = "送货批次", example = "0")
    private String deliverySeq;

}
