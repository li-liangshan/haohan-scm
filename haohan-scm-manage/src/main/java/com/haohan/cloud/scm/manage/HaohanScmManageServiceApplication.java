/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 *
 */

package com.haohan.cloud.scm.manage;

import com.pig4cloud.pigx.common.security.annotation.EnablePigxFeignClients;
import com.pig4cloud.pigx.common.security.annotation.EnablePigxResourceServer;
import com.pig4cloud.pigx.common.swagger.annotation.EnablePigxSwagger2;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author lengleng
 * @date 2018/07/29
 * 代码生成模块
 */

@EnablePigxSwagger2
@SpringCloudApplication
@EnablePigxFeignClients(basePackages = {"com.pig4cloud.pigx.*","com.haohan.cloud.scm.api.*"})
@EnablePigxResourceServer
@ComponentScan(basePackages = "com.haohan.cloud.scm")
@MapperScan(basePackages = "com.haohan.cloud.scm.*.mapper")
public class HaohanScmManageServiceApplication {

  public static void main(String[] args) {
		SpringApplication.run(HaohanScmManageServiceApplication.class, args);
	}
}
