package com.haohan.cloud.scm.api.crm.req.bill;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author dy
 * @date 2020/1/19
 */
@Data
public class QueryReceivableAccountReq {

    @NotBlank(message = "员工ID不能为空")
    @Length(max = 32, message = "员工ID最大长度32字符")
    @ApiModelProperty(value = "app登陆员工")
    private String employeeId;

    @Length(max = 32, message = "下单客户编号最大长度32字符")
    @ApiModelProperty(value = "下单客户编号", notes = "客户sn")
    private String customerSn;

    @Length(max = 32, message = "销售区域编码最大长度32字符")
    @ApiModelProperty(value = "销售区域编码")
    private String areaSn;

    @Length(max = 32, message = "市场编码最大长度32字符")
    @ApiModelProperty(value = "市场编码")
    private String marketSn;

    @Length(max = 32, message = "客户经理id最大长度32字符")
    @ApiModelProperty(value = "客户经理id")
    private String directorId;

}
