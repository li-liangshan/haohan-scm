/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.wms.req;

import com.haohan.cloud.scm.api.wms.entity.ShelfManagement;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 货品上下架记录
 *
 * @author haohan
 * @date 2019-05-28 19:12:54
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "货品上下架记录")
public class ShelfManagementReq extends ShelfManagement {

    private long pageSize;
    private long pageNo;




}
