package com.haohan.cloud.scm.api.crm.vo.app;

import com.haohan.cloud.scm.api.constant.enums.market.MarketEmployeeTypeEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author dy
 * @date 2019/11/6
 */
@Data
@Api("app登陆")
public class CrmAppLoginVO {

    private String accessToken;
    private String active;
    private String deptId;
    private Integer expiresIn;
    private String refreshToken;
    private String scope;
    private Integer tenantId;
    private String tokenType;
    private String employeeId;
    private String employeeName;
    private String shopId;

    @ApiModelProperty(value = "市场部员工类型:1市场总监2区域经理3业务经理4.社区合伙人")
    private MarketEmployeeTypeEnum employeeType;

}
