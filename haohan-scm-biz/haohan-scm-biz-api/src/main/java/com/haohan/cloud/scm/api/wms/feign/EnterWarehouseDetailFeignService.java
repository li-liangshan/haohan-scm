/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.wms.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.wms.entity.EnterWarehouseDetail;
import com.haohan.cloud.scm.api.wms.req.EnterWarehouseDetailReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 入库单明细内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "EnterWarehouseDetailFeignService", value = ScmServiceName.SCM_BIZ_WMS)
public interface EnterWarehouseDetailFeignService {


    /**
     * 通过id查询入库单明细
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/EnterWarehouseDetail/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 入库单明细 列表信息
     * @param enterWarehouseDetailReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/EnterWarehouseDetail/fetchEnterWarehouseDetailPage")
    R getEnterWarehouseDetailPage(@RequestBody EnterWarehouseDetailReq enterWarehouseDetailReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 入库单明细 列表信息
     * @param enterWarehouseDetailReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/EnterWarehouseDetail/fetchEnterWarehouseDetailList")
    R getEnterWarehouseDetailList(@RequestBody EnterWarehouseDetailReq enterWarehouseDetailReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增入库单明细
     * @param enterWarehouseDetail 入库单明细
     * @return R
     */
    @PostMapping("/api/feign/EnterWarehouseDetail/add")
    R save(@RequestBody EnterWarehouseDetail enterWarehouseDetail, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改入库单明细
     * @param enterWarehouseDetail 入库单明细
     * @return R
     */
    @PostMapping("/api/feign/EnterWarehouseDetail/update")
    R updateById(@RequestBody EnterWarehouseDetail enterWarehouseDetail, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除入库单明细
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/EnterWarehouseDetail/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/EnterWarehouseDetail/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/EnterWarehouseDetail/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param enterWarehouseDetailReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/EnterWarehouseDetail/countByEnterWarehouseDetailReq")
    R countByEnterWarehouseDetailReq(@RequestBody EnterWarehouseDetailReq enterWarehouseDetailReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @return R
     */
    @PostMapping("/api/feign/EnterWarehouseDetail/getOneByEnterWarehouseDetailReq")
    R getOneByEnterWarehouseDetailReq();


    /**
     * 批量修改OR插入
     *
     * @param enterWarehouseDetailList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/EnterWarehouseDetail/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<EnterWarehouseDetail> enterWarehouseDetailList, @RequestHeader(SecurityConstants.FROM) String from);


}
