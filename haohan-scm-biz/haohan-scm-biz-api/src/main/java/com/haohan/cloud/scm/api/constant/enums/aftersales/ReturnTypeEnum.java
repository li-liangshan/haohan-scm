package com.haohan.cloud.scm.api.constant.enums.aftersales;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dy
 * @date 2019/8/6
 */
@Getter
@AllArgsConstructor
public enum ReturnTypeEnum implements IBaseEnum {
    /**
     * 退货类型0:B客户1:采购部
     */
    customer("0", "B客户"),
    purchase("1", "采购部");

    private static final Map<String, ReturnTypeEnum> MAP = new HashMap<>(8);

    static {
        for (ReturnTypeEnum e : ReturnTypeEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static ReturnTypeEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

    @EnumValue
    @JsonValue
    private String type;
    private String desc;
}