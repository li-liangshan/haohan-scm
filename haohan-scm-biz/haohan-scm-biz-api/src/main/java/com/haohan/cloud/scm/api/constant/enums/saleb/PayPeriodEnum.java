package com.haohan.cloud.scm.api.constant.enums.saleb;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dy
 * @date 2019/6/15
 * 账期:1.月结2.周结3.日结
 */
@Getter
@AllArgsConstructor
public enum PayPeriodEnum implements IBaseEnum {

    /**
     * 账期:1.月结2.周结3.日结
     */
    month("1", "月结"),
    week("2", "周结"),
    day("3", "日结");

    private static final Map<String, PayPeriodEnum> MAP = new HashMap<>(8);

    static {
        for (PayPeriodEnum e : PayPeriodEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static PayPeriodEnum getByType(String type) {
            return MAP.get(type);
    }

    @EnumValue
    @JsonValue
    private String type;
    private String desc;
}
