package com.haohan.cloud.scm.api.common.req.supplier;

import com.haohan.cloud.scm.api.common.req.admin.PdsBaseApiReq;

import javax.validation.constraints.NotBlank;

/**
 * 供应商商品 请求
 *
 * @author dy
 * @date 2019/01/03
 */
public class SupplyPriceApiReq extends PdsBaseApiReq {

    @NotBlank(message = "goodsModelId不能为空")
    private String goodsModelId;

    public String getGoodsModelId() {
        return goodsModelId;
    }

    public void setGoodsModelId(String goodsModelId) {
        this.goodsModelId = goodsModelId;
    }
}
