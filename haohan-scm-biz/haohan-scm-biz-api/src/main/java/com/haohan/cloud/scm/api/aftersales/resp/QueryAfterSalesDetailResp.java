package com.haohan.cloud.scm.api.aftersales.resp;

import com.haohan.cloud.scm.api.constant.enums.aftersales.AfterOrderTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.aftersales.AfterSalesStatusEnum;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author xwx
 * @date 2019/7/23
 */
@Data
public class QueryAfterSalesDetailResp {
    /**
     * 主键
     */
    private String id;
    /**
     * 平台商家id
     */
    private String pmId;
    /**
     * 售后单编号
     */
    private String afterSalesSn;
    /**
     * 售后金额
     */
    private BigDecimal afterSalesAmount;
    /**
     * 联系人uid
     */
    private String linkManUid;
    /**
     * 联系人名称
     */
    private String linkManName;
    /**
     * 联系人电话
     */
    private String linkManTelephone;
    /**
     * 售后订单类型:1.供应商报价单2.B客户采购单3.C客户零售单
     */
    private AfterOrderTypeEnum afterOrderType;
    /**
     * 供应商报价单编号
     */
    private String offerOrderId;
    /**
     * B客户采购单编号
     */
    private String buyId;
    /**
     * C客户零售单编号
     */
    private String goodsOrderId;
    /**
     * 处理结果
     */
    private String resultDesc;
    /**
     * 售后状态:1已上报2处理中3待确认4已完成
     */
    private AfterSalesStatusEnum afterSalesStatus;
    /**
     * 申请时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime applyTime;
    /**
     * 处理时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime actionTime;
    /**
     * 完成时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime finishTime;

    //商品数据
    /**
     * 供应商名称
     */
    private String supplierName;
    /**
     * 商品名称
     */
    private String goodsName;
    /**
     * 采购数量
     */
    private BigDecimal buyNum;
    /**
     * 成交单价
     */
    private BigDecimal dealPrice;
    /**
     * 商品图片
     */
    private String goodsImg;
    /**
     * 单位
     */
    private String unit;
}
