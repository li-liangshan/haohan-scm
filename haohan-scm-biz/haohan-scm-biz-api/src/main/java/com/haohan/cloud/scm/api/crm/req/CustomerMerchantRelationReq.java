/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.crm.req;

import com.haohan.cloud.scm.api.crm.entity.CustomerMerchantRelation;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 客户商家关系
 *
 * @author haohan
 * @date 2019-11-05 15:42:07
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "客户商家关系")
public class CustomerMerchantRelationReq extends CustomerMerchantRelation {

    private long pageSize;
    private long pageNo;


}
