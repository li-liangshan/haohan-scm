package com.haohan.cloud.scm.api.common;

import com.haohan.cloud.framework.entity.BaseResp;
import com.haohan.cloud.scm.api.common.req.pay.PayReqParams;
import com.haohan.cloud.scm.common.tools.http.MethodType;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @program: haohan-fresh-scm
 * @description: 支付API常量
 * @author: Simon
 * @create: 2019-07-25
 **/
@Getter
@AllArgsConstructor
public enum  PayApiConstant implements IApiEnum{


  account_query("支付账户查询","/f/xiaodian/api/pay/queryAccountByAppId", PayReqParams.class, BaseResp.class,MethodType.POST),

  pay_send("发起支付请求","/f/xiaodian/api/pay/send", PayReqParams.class, BaseResp.class,MethodType.POST),

  pay_cancel("支付取消","/f/xiaodian/api/pay/cancel", PayReqParams.class, BaseResp.class,MethodType.POST),

  pay_refund("退款申请","/f/xiaodian/api/pay/refund", PayReqParams.class, BaseResp.class,MethodType.POST),

  pay_query("查询支付结果","/f/xiaodian/api/pay/query", PayReqParams.class, BaseResp.class,MethodType.POST),

  pay_queryResult("查询订单支付状态","/f/xiaodian/api/pay/queryPayResult", PayReqParams.class, BaseResp.class,MethodType.POST),

  pay_queryOrder("查询订单记录","/f/xiaodian/api/pay/fetchOrderRecord", PayReqParams.class, BaseResp.class,MethodType.POST),

  pay_queryPayRecordInfo("查询完成订单支付记录","/f/xiaodian/api/pay/queryPayRecord",PayReqParams.class,BaseResp.class,MethodType.POST);

  private String desc;
  private String url;
  private Class reqClass;
  private Class respClass;
  private MethodType methodType;
}

