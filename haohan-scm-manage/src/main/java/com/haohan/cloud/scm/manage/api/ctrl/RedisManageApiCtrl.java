package com.haohan.cloud.scm.manage.api.ctrl;

import cn.hutool.core.util.StrUtil;
import com.haohan.cloud.scm.api.constant.ScmCommonConstant;
import com.haohan.cloud.scm.common.tools.redis.RedisUpdateCoreService;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.haohan.cloud.scm.common.tools.util.ScmIncrementUtil;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.data.tenant.TenantContextHolder;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author dy
 * @date 2019/6/18
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/manage/redis")
@Api(value = "ApiManageRedis", tags = "RedisManage缓存主键管理")
public class RedisManageApiCtrl {

    private final RedisUpdateCoreService redisUpdateCoreService;
    private final ScmIncrementUtil scmIncrementUtil;

    @SysLog("更新haohan_scm的逻辑主键sn缓存")
    @PostMapping("/updateSn")
    public R<String> redisUpdateSn() {
        return RUtil.success(redisUpdateCoreService.updateAllSn());
    }

    @SysLog("更新haohan_scm的主键id缓存")
    @PostMapping("/updateId")
    public R<String> redisUpdateId() {
        return RUtil.success(redisUpdateCoreService.updateAllId());
    }

    /**
     * 设置租户小程序是否开启支付
     *
     * @param flag true 开启支付
     * @return
     */
    @SysLog("设置租户小程序支付开关")
    @PutMapping("/payFlag")
    public R<Boolean> payFlagModify(Boolean flag, String appId) {
        // 多租户单独控制小程序开关
        Integer tenantId = TenantContextHolder.getTenantId();
        if (tenantId == null) {
            tenantId = 1;
        }
        if (null == flag) {
            flag = false;
        }
        // 按appId单独控制
        String infix = StrUtil.isEmpty(appId) ? StrUtil.COLON  : StrUtil.COLON + appId + StrUtil.COLON;
        // 缓存中值为true时 开启支付
        return RUtil.success(scmIncrementUtil.setValue(tenantId.toString() + infix + ScmCommonConstant.PAY_FLAG, flag.toString(), 0));
    }

}
