/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.goods.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.goods.entity.StockKeepingUnit;
import com.haohan.cloud.scm.api.goods.req.StockKeepingUnitReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 库存商品库内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "StockKeepingUnitFeignService", value = ScmServiceName.SCM_GOODS)
public interface StockKeepingUnitFeignService {


    /**
     * 通过id查询库存商品库
     *
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/StockKeepingUnit/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 库存商品库 列表信息
     *
     * @param stockKeepingUnitReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/StockKeepingUnit/fetchStockKeepingUnitPage")
    R getStockKeepingUnitPage(@RequestBody StockKeepingUnitReq stockKeepingUnitReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 库存商品库 列表信息
     *
     * @param stockKeepingUnitReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/StockKeepingUnit/fetchStockKeepingUnitList")
    R getStockKeepingUnitList(@RequestBody StockKeepingUnitReq stockKeepingUnitReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增库存商品库
     *
     * @param stockKeepingUnit 库存商品库
     * @return R
     */
    @PostMapping("/api/feign/StockKeepingUnit/add")
    R save(@RequestBody StockKeepingUnit stockKeepingUnit, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改库存商品库
     *
     * @param stockKeepingUnit 库存商品库
     * @return R
     */
    @PostMapping("/api/feign/StockKeepingUnit/update")
    R updateById(@RequestBody StockKeepingUnit stockKeepingUnit, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除库存商品库
     *
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/StockKeepingUnit/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/StockKeepingUnit/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/StockKeepingUnit/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param stockKeepingUnitReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/StockKeepingUnit/countByStockKeepingUnitReq")
    R countByStockKeepingUnitReq(@RequestBody StockKeepingUnitReq stockKeepingUnitReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param stockKeepingUnitReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/StockKeepingUnit/getOneByStockKeepingUnitReq")
    R getOneByStockKeepingUnitReq(@RequestBody StockKeepingUnitReq stockKeepingUnitReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param stockKeepingUnitList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/StockKeepingUnit/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<StockKeepingUnit> stockKeepingUnitList, @RequestHeader(SecurityConstants.FROM) String from);


}
