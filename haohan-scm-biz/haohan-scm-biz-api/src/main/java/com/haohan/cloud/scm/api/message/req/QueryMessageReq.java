package com.haohan.cloud.scm.api.message.req;

import com.haohan.cloud.scm.api.constant.enums.message.DepartmentTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.message.MessageTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.message.MsgActionTypeEnum;
import com.haohan.cloud.scm.api.message.entity.MessageRecord;
import com.haohan.cloud.scm.common.tools.constant.SingleGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.groups.Default;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Set;

/**
 * @author dy
 * @date 2020/1/14
 * 详情查询:SingleGroup
 */
@Data
public class QueryMessageReq implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "消息类型:1微信2站内信3短信")
    private MessageTypeEnum messageType;

    @ApiModelProperty(value = "业务部门类型:1供应2采购3生产4物流5市场6平台7门店8客户")
    private DepartmentTypeEnum departmentType;

    @ApiModelProperty(value = "业务类型:0.系统通知 51.日报 52.销售订单 53.销量上报 54.库存上报 55.竞品上报")
    private MsgActionTypeEnum msgActionType;

    // 非eq

    @NotBlank(message = "消息编号不能为空", groups = {SingleGroup.class})
    @Length(max = 32, message = "消息编号最大长度32字符", groups = {SingleGroup.class, Default.class})
    @ApiModelProperty(value = "消息编号")
    private String messageSn;

    @Length(max = 32, message = "消息标题最大长度32字符")
    @ApiModelProperty(value = "消息标题")
    private String title;

    @ApiModelProperty(value = "查询发出日期-开始日期", notes = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate startDate;

    @ApiModelProperty(value = "查询发出日期-结束日期", notes = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate endDate;

    @ApiModelProperty(value = "发送人uid 集合")
    private Set<String> senderUidSet;

    @Length(max = 32, message = "发送人名称最大长度32字符")
    @ApiModelProperty(value = "发送人名称")
    private String senderName;

    public void setQueryDate(LocalDate startDate, LocalDate endDate) {
        if (null != startDate && null != endDate) {
            this.startDate = startDate;
            this.endDate = endDate;
        }

    }

    public MessageRecord transTo() {
        MessageRecord msg = new MessageRecord();
        msg.setMessageType(this.messageType);
        msg.setDepartmentType(this.departmentType);
        msg.setMsgActionType(this.msgActionType);
        return msg;
    }
}
