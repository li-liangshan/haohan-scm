package com.haohan.cloud.scm.api.crm.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.haohan.cloud.scm.api.constant.enums.market.CustomerTypeEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author dy
 * @date 2019/9/25
 */
@Data
@Api("客户类型数量统计")
public class CustomerTypeVO {

    @ApiModelProperty("客户类型名称")
    @JsonProperty("name")
    private String typeName;

    @ApiModelProperty("客户类型值")
    @JsonProperty("type")
    private String typeValue;

    @ApiModelProperty("客户数量")
    @JsonProperty("value")
    private Integer num;

    public CustomerTypeVO(CustomerTypeEnum customerType, Integer num) {
        if (null != customerType) {
            this.typeName = customerType.getDesc();
            this.typeValue = customerType.getType();
        } else {
            this.typeName = "未定义";
            this.typeValue = "0";
        }
        this.num = num;
    }

}
