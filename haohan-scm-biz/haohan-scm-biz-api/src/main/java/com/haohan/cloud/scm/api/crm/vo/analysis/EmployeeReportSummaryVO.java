package com.haohan.cloud.scm.api.crm.vo.analysis;

import com.haohan.cloud.scm.api.constant.enums.market.MarketEmployeeTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.market.SexEnum;
import com.haohan.cloud.scm.api.crm.entity.MarketEmployee;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.List;

/**
 * @author dy
 * @date 2019/11/12
 */
@Data
@Api("员工日报数汇总")
@NoArgsConstructor
public class EmployeeReportSummaryVO {

    @ApiModelProperty(value = "员工id")
    private String employeeId;

    @ApiModelProperty(value = "员工名称")
    private String employeeName;

    @ApiModelProperty(value = "部门名称")
    private String department;

    @ApiModelProperty(value = "市场部员工类型:1市场总监2区域经理3业务经理4.社区合伙人")
    private MarketEmployeeTypeEnum employeeType;

    @ApiModelProperty(value = "性别:1男2女")
    private SexEnum sex;

    @ApiModelProperty(value = "日报总数")
    private Integer totalNum;

    @ApiModelProperty(value = "查询开始时间")
    protected LocalDate startDate;

    @ApiModelProperty(value = "查询结束时间")
    protected LocalDate endDate;

    @ApiModelProperty(value = "日报情况列表(按日)")
    List<WorkReportDayVO> reportList;

    public EmployeeReportSummaryVO(MarketEmployee employee) {
        if(null != employee){
            this.setEmployeeId(employee.getId());
            this.setEmployeeName(employee.getName());
            this.setDepartment(employee.getDeptId());
            this.setEmployeeType(employee.getEmployeeType());
            this.setSex(employee.getSex());
        }
    }
}
