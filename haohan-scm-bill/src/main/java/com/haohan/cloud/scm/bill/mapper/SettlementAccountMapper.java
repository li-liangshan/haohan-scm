package com.haohan.cloud.scm.bill.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haohan.cloud.scm.api.bill.entity.SettlementAccount;

/**
 * @author dy
 * @date 2019/11/27
 */
public interface SettlementAccountMapper extends BaseMapper<SettlementAccount> {

}
