package com.haohan.cloud.scm.api.constant;


import java.time.format.DateTimeFormatter;

/**
 * @author dy
 * @date 2019/6/6
 */
public interface ScmCommonConstant {

    /**
     * 请求头中 tenantId
     */
    String TENANT_ID_HEADER = "tenant-id";
    /**
     * 租户id
     */
    String TENANT_KEY = "tenantId";
    String PM_KEY = "pmId";
    /**
     * 平台商家id:租户id 映射的缓存key(原系统使用)
     */
    String TENANT_MAP_KEY = "tenant-pm";

    /**
     * 货品有效期 默认天数
     */
    long VALIDITY_DAY = 7;

    /**
     * 原始货品编号,标识货品是最初采购的或加工生产的
     */
    String ORIGINAL_PRODUCT_SN = "0";

    /**
     * 初始化虚拟库存
     */
    Integer INIT_VIRTUAL_STORAGE = 100;

    /**
     * 规格库存缓存数量 键值
     */
    String MODEL_STORAGE_KEY = "model_storage_key:";

    /**
     * 小程序 支付开关
     */
    String PAY_FLAG = "PAY_FLAG";

    /**
     * 商品导入时的默认分类名称 一级
     */
    String DEFAULT_PARENT_NAME = "未确认商品";
    /**
     * 商品导入时的默认分类名称 二级
     */
    String DEFAULT_CATEGORY_NAME = "导入商品";

    /**
     * 定位 经度正则
     * 正负180度之间, 弧度数表示
     */
    String LONGITUDE_PATTEN = "^[\\-\\+]?(0?\\d{1,2}|1[0-7]?\\d|180)\\.\\d{1,20}$";
    /**
     * 定位 纬度正则
     * 正负90度之间, 弧度数表示
     */
    String LATITUDE_PATTEN = "^[\\-\\+]?([0-8]?\\d|90)\\.\\d{1,20}$";
    /**
     * 定位 正则 （可为空串）
     * 经度正负180度之间, 纬度正负90度之间, 弧度数表示
     */
    String POSITION_PATTEN = "(^[\\-\\+]?(0?\\d{1,2}|1[0-7]?\\d|180)\\.\\d{1,20}[,][\\-\\+]?([0-8]?\\d|90)\\.\\d{1,20}$)?";

    /**
     * 月份正则  yyyy-MM (可为空串)
     */
    String MONTH_PATTEN = "(^\\d{4}-[0,1]\\d)?";

    /**
     * 月份 日期格式化
     */
    DateTimeFormatter MONTH_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM");

    /**
     * 时间 日期格式化
     */
    DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    /**
     * 定位距离最大值
     */
    int MAX_DISTANCE = 30;
    /**
     * 定位距离最小值
     */
    int MIN_DISTANCE = 3;

    /**
     * 图片上传 文件名
     */
    String[] PHOTO_SUFFIX = {".JPEG", ".GIF", ".JPG", ".PNG", ".BMP"};
    /**
     * 文件大小限制
     */
    long LIMIT_FILE_SIZE = 2 * 1024 * 1024;

    /**
     * 密码初始值
     */
    String PASSWORD = "123456";

    /**
     * crm默认user名称
     */
    String CRM_DEFAULT_USERNAME = "crm";

    /**
     * 消息模块 系统消息发送人 uid
     */
    String SCM_MSG_SYSTEM_SENDER_UID = "0";

    /**
     * 消息模块 系统消息发送人 名称
     */
    String SCM_MSG_SYSTEM_SENDER_NAME = "系统消息";

}
