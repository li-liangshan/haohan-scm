package com.haohan.cloud.scm.api.supply.vo;

import com.haohan.cloud.scm.api.constant.enums.common.DeliveryTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.saleb.BuyOrderStatusEnum;
import com.haohan.cloud.scm.api.supply.entity.SupplyOrder;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author dy
 * @date 2020/4/25
 */
@Data
@NoArgsConstructor
public class SupplyOrderVO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "供应订单编号")
    private String supplySn;

    @ApiModelProperty(value = "供应商id")
    private String supplierId;

    @ApiModelProperty(value = "供应商名称")
    private String supplierName;

    @ApiModelProperty(value = "下单时间")
    private LocalDateTime orderTime;

    @ApiModelProperty(value = "供应日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate supplyDate;

    @ApiModelProperty(value = "需求描述")
    private String needNote;

    @ApiModelProperty(value = "供应总金额")
    private BigDecimal totalAmount;

    @ApiModelProperty(value = "商品合计金额")
    private BigDecimal sumAmount;

    @ApiModelProperty(value = "其他金额")
    private BigDecimal otherAmount;

    @ApiModelProperty(value = "联系人")
    private String contact;

    @ApiModelProperty(value = "联系电话")
    private String telephone;

    @ApiModelProperty(value = "供应地址")
    private String address;

    @ApiModelProperty(value = "状态")
    private BuyOrderStatusEnum status;

    @ApiModelProperty(value = "成交时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime dealTime;

    @ApiModelProperty(value = "配送方式")
    private DeliveryTypeEnum deliveryType;

    @ApiModelProperty(value = "货品种数")
    private Integer goodsNum;

    @ApiModelProperty(value = "供应订单明细")
    private List<SupplyOrderDetailVO> detailList;

    public SupplyOrderVO(SupplyOrder order) {
        this.supplySn = order.getSupplySn();
        this.supplierId = order.getSupplierId();
        this.supplierName = order.getSupplierName();
        this.orderTime = order.getOrderTime();
        this.supplyDate = order.getSupplyDate();
        this.needNote = order.getNeedNote();
        this.totalAmount = order.getTotalAmount();
        this.sumAmount = order.getSumAmount();
        this.otherAmount = order.getOtherAmount();
        this.contact = order.getContact();
        this.telephone = order.getTelephone();
        this.address = order.getAddress();
        this.status = order.getStatus();
        this.dealTime = order.getDealTime();
        this.deliveryType = order.getDeliveryType();
        this.goodsNum = order.getGoodsNum();
    }

}
