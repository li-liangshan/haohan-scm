package com.haohan.cloud.scm.api.wechat.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @author dy
 * @date 2020/5/28
 */
@Data
public class WxTenantReq implements Serializable {
    private static final long serialVersionUID = 1L;


    @NotBlank(message = "uid不能为空")
    @Length(max = 64, message = "通行证id长度最大64字符")
    @ApiModelProperty(value = "通行证id")
    private String uid;

    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台商家id")
    private String pmId;

}
