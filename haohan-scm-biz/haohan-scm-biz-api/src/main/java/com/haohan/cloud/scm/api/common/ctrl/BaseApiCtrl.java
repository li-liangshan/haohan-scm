package com.haohan.cloud.scm.api.common.ctrl;

import cn.hutool.core.util.StrUtil;
import com.haohan.cloud.scm.api.common.BaseApiService;
import com.haohan.cloud.scm.api.common.entity.req.UploadPhotoReq;
import com.haohan.cloud.scm.api.common.entity.resp.UploadPhotoResp;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.pig4cloud.pigx.common.core.util.R;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author dy
 * @date 2019/6/25
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/common/haohanshop")
@Api(value = "haohanshop接口", tags = "haohanshop接口")
public class BaseApiCtrl {

    private final BaseApiService apiService;
    private final String STATIC_PATH = "/data/www/static/";
    private final String FILE_SUFFIX = ".";
    private final long LIMIT_FILE_SIZE = 2 * 1024 * 1024;

    @PostMapping("/uploadPhoto")
    @ResponseBody
    public R<UploadPhotoResp> callTest(@Validated UploadPhotoReq req) throws Exception {

        R<UploadPhotoResp> r = RUtil.error();
        MultipartFile mf = req.getFile();
        // 文件限制
        String fileName = mf.getOriginalFilename();
        String suffix = "";
        if (StrUtil.isNotBlank(fileName) && fileName.contains(FILE_SUFFIX)) {
            suffix = fileName.substring(fileName.lastIndexOf(FILE_SUFFIX)).toUpperCase();
        }
        if (!StrUtil.equalsAny(suffix, ".JPEG", ".GIF", ".JPG", ".PNG", ".BMP")) {
            r.setMsg("图片格式有误,支持JPEG,GIF,JPG,PNG,BMP");
            return r;
        }
        // 图片限制大小 2MB
        if (mf.getSize() > LIMIT_FILE_SIZE) {
            r.setMsg("图片最大支持2MB");
            return r;
        }
        // 返回结果
        UploadPhotoResp dataResp;

        // 目前测试返回固定值
        dataResp = new UploadPhotoResp();
        dataResp.setPhotoUrl("http://haohanshop-file.oss-cn-beijing.aliyuncs.com/merchantFiles/9/03/20190625/1561469093977Rcn8.png");
        dataResp.setPhotoGalleryId("11");
        return new R<>(dataResp);

//        // 本地存储文件
//        File file = createFile(mf, req.getPmId());
//
//        HashMap<String, Object> paramMap = new HashMap<>(8);
//        paramMap.put("file", file);
//        paramMap.put("pmId", req.getPmId());
//        // 请求路径
////        String url = "http://localhost:8080/haohanshop";
//        String url = apiService.getApiUrl();
//        url += CommonApiConstant.common_goods_uploadPhoto.getUrl();
//
//        String result = HttpUtil.post(url, paramMap);
//        BaseResp baseResp = JSONUtil.toBean(result, BaseResp.class);
//        if (baseResp.isSuccess()) {
//            dataResp = JSONUtil.toBean(baseResp.getExt().toString(), UploadPhotoResp.class);
//            r = new R<>(dataResp);
//        } else {
//            r.setMsg(baseResp.getMsg());
//        }
//        // 上传完成删除本地文件
//        Path p = Paths.get(file.getPath());
//        Files.deleteIfExists(p);
//        return r;

    }

    /**
     * 图片文件 在本地创建
     *
     * @param mf
     * @param pmId
     * @return
     * @throws IOException
     */
    private File createFile(MultipartFile mf, String pmId) throws IOException {
        String filePath = STATIC_PATH + pmId;
        Path path = Paths.get(filePath);
        // 创建文件夹
        if (Files.notExists(path)) {
            Files.createDirectories(path);
        }
        String fileName = mf.getOriginalFilename();
        Path p = Paths.get(filePath + File.separator + fileName);
        // 删除已存在
        Files.deleteIfExists(p);
        File file = p.toFile();
        mf.transferTo(file.getAbsoluteFile());
        return file;
    }


}
