package com.haohan.cloud.scm.api.salec.trans;

import com.haohan.cloud.scm.api.constant.enums.common.DeliveryTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.saleb.BuyOrderStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.saleb.DetailSummaryFlagEnum;
import com.haohan.cloud.scm.api.saleb.entity.BuyOrder;
import com.haohan.cloud.scm.api.saleb.entity.BuyOrderDetail;
import com.haohan.cloud.scm.api.saleb.entity.Buyer;
import com.haohan.cloud.scm.api.salec.dto.COrderDetailDTO;
import com.haohan.cloud.scm.api.salec.entity.StoreOrder;
import lombok.experimental.UtilityClass;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * @author cx
 * @date 2019/7/13
 */
@UtilityClass
public class SalecBuyOrderTrans {

    public static BuyOrder CreateBuyOrderTrans(StoreOrder order, Buyer buyer){
        BuyOrder buyOrder = new BuyOrder();
        buyOrder.setPmId(buyer.getPmId());
        buyOrder.setStatus(BuyOrderStatusEnum.submit);
        buyOrder.setAddress(order.getUserAddress());
        buyOrder.setBuyerName(buyer.getBuyerName());
        buyOrder.setBuyerId(buyer.getId());
        buyOrder.setBuyerUid(buyer.getPassportId());
        buyOrder.setBuyTime(timeTrans(order.getPayTime()));
        buyOrder.setContact(order.getRealName());
        buyOrder.setTelephone(order.getUserPhone());
        buyOrder.setDeliveryType(DeliveryTypeEnum.getByType(order.getDeliveryType()));
        buyOrder.setShipFee(order.getTotalPostage());
        buyOrder.setGenPrice(order.getTotalPrice());
        buyOrder.setTotalPrice(order.getPayPrice());
        buyOrder.setRemarks("商城转换订单, 用户:" + order.getRealName() + "|");
        return buyOrder;
    }

    /**
     * 10位时间戳转换为LocalDateTime
     * @param time
     * @return
     */
    public static LocalDateTime timeTrans(int time){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String format = sdf.format(new Date(Long.valueOf(time + "000")));
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return LocalDateTime.parse(format,formatter);
    }

    /**
     * 生成采购单明细
     * @param dto
     * @return
     */
    public static BuyOrderDetail buyOrderDetailTrans(COrderDetailDTO dto){
        BuyOrderDetail detail = new BuyOrderDetail();
        detail.setSummaryFlag(DetailSummaryFlagEnum.wait);
        detail.setUnit(dto.getProductInfo().getUnit_name());
        detail.setMarketPrice(dto.getProductInfo().getOt_price());
        detail.setStatus(BuyOrderStatusEnum.submit);
        detail.setGoodsImg(dto.getProductInfo().getAttrInfo().getImage());
        detail.setGoodsName(dto.getProductInfo().getStore_name());
        detail.setGoodsModel(dto.getProductInfo().getAttrInfo().getSuk());
        detail.setGoodsModelId(dto.getProduct_attr_unique());
        detail.setBuyPrice(dto.getTruePrice());
        detail.setGoodsNum(dto.getCart_num());
        detail.setOrderGoodsNum(dto.getCart_num());
        return detail;
    }

}
