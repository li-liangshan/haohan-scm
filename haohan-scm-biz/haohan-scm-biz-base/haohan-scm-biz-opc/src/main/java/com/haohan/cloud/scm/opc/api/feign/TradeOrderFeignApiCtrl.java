/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.opc.api.feign;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.opc.entity.TradeOrder;
import com.haohan.cloud.scm.api.opc.req.TradeOrderReq;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.haohan.cloud.scm.opc.service.TradeOrderService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 交易订单
 *
 * @author haohan
 * @date 2019-05-30 10:21:55
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/TradeOrder")
@Api(value = "tradeorder", tags = "tradeorder内部接口服务")
public class TradeOrderFeignApiCtrl {

    private final TradeOrderService tradeOrderService;


    /**
     * 通过id查询交易订单
     *
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(tradeOrderService.getById(id));
    }


    /**
     * 分页查询 交易订单 列表信息
     *
     * @param tradeOrderReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchTradeOrderPage")
    public R getTradeOrderPage(@RequestBody TradeOrderReq tradeOrderReq) {
        Page page = new Page(tradeOrderReq.getPageNo(), tradeOrderReq.getPageSize());
        TradeOrder tradeOrder = new TradeOrder();
        BeanUtil.copyProperties(tradeOrderReq, tradeOrder);

        return new R<>(tradeOrderService.page(page, Wrappers.query(tradeOrder)));
    }


    /**
     * 全量查询 交易订单 列表信息
     *
     * @param tradeOrderReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchTradeOrderList")
    public R getTradeOrderList(@RequestBody TradeOrderReq tradeOrderReq) {
        TradeOrder tradeOrder = new TradeOrder();
        BeanUtil.copyProperties(tradeOrderReq, tradeOrder);
        List<TradeOrder> list = tradeOrderService.list(Wrappers.query(tradeOrder));
        if (CollUtil.isEmpty(list)) {
            return RUtil.error().setMsg("list为空");
        }

        return new R<>(list);
    }


    /**
     * 新增交易订单
     *
     * @param tradeOrder 交易订单
     * @return R
     */
    @Inner
    @SysLog("新增交易订单")
    @PostMapping("/add")
    public R save(@RequestBody TradeOrder tradeOrder) {
        return new R<>(tradeOrderService.save(tradeOrder));
    }

    /**
     * 修改交易订单
     *
     * @param tradeOrder 交易订单
     * @return R
     */
    @Inner
    @SysLog("修改交易订单")
    @PostMapping("/update")
    public R updateById(@RequestBody TradeOrder tradeOrder) {
        return new R<>(tradeOrderService.updateById(tradeOrder));
    }

    /**
     * 通过id删除交易订单
     *
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除交易订单")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(tradeOrderService.removeById(id));
    }

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @return R
     */
    @Inner
    @SysLog("批量删除交易订单")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(tradeOrderService.removeByIds(idList));
    }


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @Inner
    @SysLog("根据IDS批量查询交易订单")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(tradeOrderService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param tradeOrderReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询交易订单总记录}")
    @PostMapping("/countByTradeOrderReq")
    public R countByTradeOrderReq(@RequestBody TradeOrderReq tradeOrderReq) {

        return new R<>(tradeOrderService.count(Wrappers.query(tradeOrderReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     *
     * @param tradeOrderReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据tradeOrderReq查询一条货位信息表")
    @PostMapping("/getOneByTradeOrderReq")
    public R getOneByTradeOrderReq(@RequestBody TradeOrderReq tradeOrderReq) {

        return new R<>(tradeOrderService.getOne(Wrappers.query(tradeOrderReq), false));
    }


    /**
     * 批量修改OR插入
     *
     * @param tradeOrderList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<TradeOrder> tradeOrderList) {

        return new R<>(tradeOrderService.saveOrUpdateBatch(tradeOrderList));
    }

}
