package com.haohan.cloud.scm.product.core.impl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.haohan.cloud.scm.api.constant.enums.product.SortingStatusEnum;
import com.haohan.cloud.scm.api.product.entity.SortingOrder;
import com.haohan.cloud.scm.api.product.entity.SortingOrderDetail;
import com.haohan.cloud.scm.api.product.req.SortingBatchReq;
import com.haohan.cloud.scm.common.tools.exception.EmptyDataException;
import com.haohan.cloud.scm.product.core.IProductSortingOrderService;
import com.haohan.cloud.scm.product.core.ScmSortingOrderService;
import com.haohan.cloud.scm.product.service.SortingOrderDetailService;
import com.haohan.cloud.scm.product.service.SortingOrderService;
import com.haohan.cloud.scm.product.utils.ScmProductUtils;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author xwx
 * @date 2019/7/18
 */
@Service
@AllArgsConstructor
public class ScmSortingOrderServiceImpl implements ScmSortingOrderService {

    private final SortingOrderService sortingOrderService;
    private final SortingOrderDetailService sortingOrderDetailService;
    private final ScmProductUtils scmProductUtils;
    private final IProductSortingOrderService iProductSortingOrderService;

    /**
     * 分拣单操作 - 一键分拣
     * @param req
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean sortingBatch(SortingBatchReq req) {
        //查询可分拣的分拣单列表  状态 不为3已分拣4已完成
        QueryWrapper<SortingOrder> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(SortingOrder::getPmId, req.getPmId())
                .eq(SortingOrder::getDeliveryDate, req.getDeliveryDate())
                .eq(SortingOrder::getDeliverySeq, req.getDeliverySeq())
                .ne(SortingOrder::getSortingStatus, SortingStatusEnum.selected)
                .ne(SortingOrder::getSortingStatus, SortingStatusEnum.finish);
        List<SortingOrder> list = sortingOrderService.list(queryWrapper);
        if (CollUtil.isEmpty(list)){
            throw new EmptyDataException("分拣单列表为空");
        }
        int success = 0;
        for (SortingOrder order : list) {
            //查询可分拣的分拣单明细列表
            SortingOrderDetail orderDetail = new SortingOrderDetail();
            orderDetail.setPmId(order.getPmId());
            orderDetail.setSortingOrderSn(order.getSortingOrderSn());
            orderDetail.setSortingStatus(SortingStatusEnum.sorting);
            List<SortingOrderDetail> detailList = sortingOrderDetailService.list(Wrappers.query(orderDetail));
            if (CollUtil.isEmpty(detailList)){
                continue;
            }
            for (SortingOrderDetail detail : detailList) {
                // 分拣单明细  修改分拣数量 修改状态:已分拣
                SortingOrderDetail update = new SortingOrderDetail();
                update.setId(detail.getId());
                if(null == detail.getSortingNumber()){
                    update.setSortingNumber(detail.getPurchaseNumber());
                }
                update.setSortingStatus(SortingStatusEnum.selected);
                sortingOrderDetailService.updateById(update);
            }
            //修改分拣单
            SortingOrder updateOrder = new SortingOrder();
            updateOrder.setId(order.getId());
            updateOrder.setDeadlineTime(LocalDateTime.now());
            updateOrder.setSortingStatus(SortingStatusEnum.selected);
            sortingOrderService.updateById(updateOrder);
            success++;
        }
        if(success==0){
            return false;
        }
        return true;
    }

}
