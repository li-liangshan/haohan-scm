package com.haohan.cloud.scm.api.constant.enums.crm;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dy
 * @date 2019/12/10
 */
@Getter
@AllArgsConstructor
public enum PaymentTypeEnum implements IBaseEnum {

    /**
     * 付款方式 1.现金，2.微信转账，3.线上支付
     */
    cash("1", "现金"),
    transfer("2", "微信转账"),
    online("3", "线上支付");

    private static final Map<String, PaymentTypeEnum> MAP = new HashMap<>(8);

    static {
        for (PaymentTypeEnum e : PaymentTypeEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static PaymentTypeEnum getByType(String type) {
        return MAP.get(type);
    }

    @EnumValue
    @JsonValue
    private String type;
    private String desc;
}
