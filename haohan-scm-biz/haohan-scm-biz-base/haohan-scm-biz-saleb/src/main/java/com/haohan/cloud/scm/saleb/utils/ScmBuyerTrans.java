package com.haohan.cloud.scm.saleb.utils;

import com.haohan.cloud.scm.api.constant.enums.manage.MerchantPdsTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.manage.MerchantStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import com.haohan.cloud.scm.api.manage.entity.Merchant;
import com.haohan.cloud.scm.api.saleb.entity.Buyer;
import lombok.Data;
import lombok.experimental.UtilityClass;

/**
 * @author dy
 * @date 2020/3/3
 */
@Data
@UtilityClass
public class ScmBuyerTrans {

    /**
     * 商城采购商初始化商家
     * @param buyer
     * @param merchantName
     * @return
     */
    public Merchant initMerchant(Buyer buyer, String merchantName) {
        Merchant merchant = new Merchant();
        merchant.setMerchantName(merchantName);
        merchant.setAddress(buyer.getAddress());
        merchant.setContact(buyer.getContact());
        merchant.setTelephone(buyer.getTelephone());
        merchant.setBizDesc("商城用户采购商默认创建商家");
        merchant.setStatus(MerchantStatusEnum.stayAudit);
        //  merchantType  prodType productLine暂不设置
        merchant.setIsAutomaticOrder(YesNoEnum.no);
        merchant.setPdsType(MerchantPdsTypeEnum.mall);
        return merchant;
    }
}
