/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.goods.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haohan.cloud.scm.api.common.dto.SelfPageMapper;
import com.haohan.cloud.scm.api.goods.dto.GoodsExtDTO;
import com.haohan.cloud.scm.api.goods.dto.GoodsSqlDTO;
import com.haohan.cloud.scm.api.goods.entity.Goods;

import java.util.List;

/**
 * 商品
 *
 * @author haohan
 * @date 2019-05-13 18:46:46
 */
public interface GoodsMapper extends BaseMapper<Goods> , SelfPageMapper {
    /**
     * 自定义分页使用
     *
     * @param params
     * @return
     */
    Integer queryCount(GoodsSqlDTO params);

    /**
     * 自定义分页使用
     *
     * @param params
     * @return
     */
    List<GoodsExtDTO> queryPage(GoodsSqlDTO params);

}
