package com.haohan.cloud.scm.api.salec;

/**
 * @author dy
 * @date 2020/6/8
 * salec 使用常量
 */
public interface ScmSalecConstant {

    /**
     * 同步商品分类防重复 (拼接shopId使用)
     */
    String SYNC_CATEGORY_SHOP = "salec_details:sync_category_shop:";

    /**
     * 同步商品防重复 (拼接shopId使用)
     */
    String SYNC_GOODS_SHOP = "salec_details:sync_goods_shop:";

    /**
     * 同步商品防重复 (拼接goodsId使用)
     */
    String SYNC_GOODS_ID = "salec_details:sync_goods:";

    /**
     * 同步商品防重复 时间：2分钟
     */
    int SYNC_CACHE_SECONDS = 120;


}
