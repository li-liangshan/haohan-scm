/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.purchase.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseAccount;
import com.haohan.cloud.scm.api.purchase.req.PurchaseAccountReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 采购员工账户内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "PurchaseAccountFeignService", value = ScmServiceName.SCM_BIZ_PURCHASE)
public interface PurchaseAccountFeignService {


    /**
     * 通过id查询采购员工账户
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/PurchaseAccount/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 采购员工账户 列表信息
     * @param purchaseAccountReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/PurchaseAccount/fetchPurchaseAccountPage")
    R getPurchaseAccountPage(@RequestBody PurchaseAccountReq purchaseAccountReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 采购员工账户 列表信息
     * @param purchaseAccountReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/PurchaseAccount/fetchPurchaseAccountList")
    R getPurchaseAccountList(@RequestBody PurchaseAccountReq purchaseAccountReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增采购员工账户
     * @param purchaseAccount 采购员工账户
     * @return R
     */
    @PostMapping("/api/feign/PurchaseAccount/add")
    R save(@RequestBody PurchaseAccount purchaseAccount, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改采购员工账户
     * @param purchaseAccount 采购员工账户
     * @return R
     */
    @PostMapping("/api/feign/PurchaseAccount/update")
    R updateById(@RequestBody PurchaseAccount purchaseAccount, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除采购员工账户
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/PurchaseAccount/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/PurchaseAccount/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/PurchaseAccount/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param purchaseAccountReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/PurchaseAccount/countByPurchaseAccountReq")
    R countByPurchaseAccountReq(@RequestBody PurchaseAccountReq purchaseAccountReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param purchaseAccountReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/PurchaseAccount/getOneByPurchaseAccountReq")
    R getOneByPurchaseAccountReq(@RequestBody PurchaseAccountReq purchaseAccountReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param purchaseAccountList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/PurchaseAccount/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<PurchaseAccount> purchaseAccountList, @RequestHeader(SecurityConstants.FROM) String from);


}
