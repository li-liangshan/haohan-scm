package com.haohan.cloud.scm.common.tools.redis;

/**
 * @author dy
 * @date 2019/8/24
 */
public interface RedisUpdateCoreService {

    String updateAllSn();

    String updateAllId();
}
