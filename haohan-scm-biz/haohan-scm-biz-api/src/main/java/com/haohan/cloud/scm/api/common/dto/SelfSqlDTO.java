package com.haohan.cloud.scm.api.common.dto;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pig4cloud.pigx.common.core.constant.CommonConstants;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * @author dy
 * @date 2019/11/6
 * tenantId 由多租户配置和多租户插件维护
 */
@Data
@Api("自定义sql的入参基础对象")
public class SelfSqlDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "删除标记")
    protected String delFlag;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "查询开始时间")
    protected LocalDate startDate;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "查询结束时间")
    protected LocalDate endDate;

    // 分页参数

    @ApiModelProperty(value = "当前页")
    protected Long current;

    @ApiModelProperty(value = "每页显示条数")
    protected Long size;

    @ApiModelProperty(value = "当前分页偏移量")
    public Long getOffset() {
        return this.current > 0 ? (this.current - 1) * this.size : 0;
    }

    protected SelfSqlDTO() {
        // 逻辑删除 条件
        this.delFlag = CommonConstants.STATUS_NORMAL;
        // 分页默认
        this.current = 1L;
        this.size = 10L;
    }

    /**
     * 设置 查询时间
     */
    public void queryDate(LocalDate startDate, LocalDate endDate) {
        if (null != startDate && null != endDate) {
            this.startDate = startDate;
            this.endDate = endDate;
        }
    }

    /**
     * 设置 查询分页
     */
    public void queryPage(Page page) {
        this.current = page.getCurrent();
        this.size = page.getSize();
    }

}
