/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.product.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.product.entity.GoodsLossRate;
import com.haohan.cloud.scm.product.mapper.GoodsLossRateMapper;
import com.haohan.cloud.scm.product.service.GoodsLossRateService;
import org.springframework.stereotype.Service;

/**
 * 单品损耗率
 *
 * @author haohan
 * @date 2019-05-13 18:20:23
 */
@Service
public class GoodsLossRateServiceImpl extends ServiceImpl<GoodsLossRateMapper, GoodsLossRate> implements GoodsLossRateService {

}
