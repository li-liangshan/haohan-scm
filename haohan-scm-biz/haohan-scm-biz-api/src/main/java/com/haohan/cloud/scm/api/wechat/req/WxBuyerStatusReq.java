package com.haohan.cloud.scm.api.wechat.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author dy
 * @date 2020/5/26
 */
@Data
public class WxBuyerStatusReq {

    @NotBlank(message = "通行证id不能为空")
    @Length(max = 64, message = "通行证id长度最大64字符")
    @ApiModelProperty(value = "通行证id")
    private String uid;

}
