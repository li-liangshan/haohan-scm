package com.haohan.cloud.scm.api.wechat.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author dy
 * @date 2019/7/23
 */
@Data
@ApiModel("小程序用户关联")
public class WechatAddReq {

    @NotBlank(message = "appId不能为空")
    @ApiModelProperty(value = "应用Id", required = true)
    private String appId;

    @NotBlank(message = "code不能为空")
    @ApiModelProperty(value = "微信小程序临时登录凭证", required = true)
    private String code;

    @NotBlank(message = "rawData不能为空")
    @ApiModelProperty(value = "小程序用户个人信息json串", required = true)
    private String rawData;

    @ApiModelProperty(value = "小程序用户个人信息encryptedData")
    private String encryptedData;

    @ApiModelProperty(value = "小程序用户个人信息signature")
    private String signature;

    @ApiModelProperty(value = "小程序用户个人信息iv")
    private String iv;

}
