package com.haohan.cloud.scm.api.crm.req.market;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author dy
 * @date 2020/3/13
 */
@Data
public class MoveMarketReq {

    @NotBlank(message = "原市场编码不能为空")
    @ApiModelProperty(value = "原市场编码")
    @Length(max = 32, message = "原市场编码长度最大为32字符")
    private String sourceMarketSn;

    @NotBlank(message = "目标市场编码不能为空")
    @ApiModelProperty(value = "目标市场编码")
    @Length(max = 32, message = "目标市场编码长度最大为32字符")
    private String targetMarketSn;


}
