/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.product.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.product.entity.ProductionEmployee;
import com.haohan.cloud.scm.product.mapper.ProductionEmployeeMapper;
import com.haohan.cloud.scm.product.service.ProductionEmployeeService;
import org.springframework.stereotype.Service;

/**
 * 生产部员工表
 *
 * @author haohan
 * @date 2019-05-13 18:16:14
 */
@Service
public class ProductionEmployeeServiceImpl extends ServiceImpl<ProductionEmployeeMapper, ProductionEmployee> implements ProductionEmployeeService {

}
