package com.haohan.cloud.scm.api.crm.vo.analysis;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author dy
 * @date 2019/11/7
 */
@Data
@Api("客户拜访效果统计")
public class CustomerVisitEffectVO {

    @ApiModelProperty(value = "客户数")
    private Integer customerNum;

    @ApiModelProperty(value = "市场数")
    private Integer marketNum;

    @ApiModelProperty(value = "总拜访数")
    private Integer visitNum;

    @ApiModelProperty(value = "总拜访客户数")
    private Integer visitCustomerNum;

    @ApiModelProperty(value = "拜访覆盖率,百分比")
    private BigDecimal coverageRate;

    @ApiModelProperty(value = "成交客户数")
    private Integer dealCustomerNum;

    @ApiModelProperty(value = "成交率,百分比")
    private BigDecimal dealRate;

}
