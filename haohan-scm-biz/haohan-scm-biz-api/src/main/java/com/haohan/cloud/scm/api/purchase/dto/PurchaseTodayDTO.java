package com.haohan.cloud.scm.api.purchase.dto;

import lombok.Data;

/**
 * @author cx
 * @date 2019/7/11
 */
@Data
public class PurchaseTodayDTO {

    private String pmId;
    private String status;
    private String start;
    private String end;
}
