/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.tms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haohan.cloud.scm.api.tms.entity.RouteManage;


/**
 * 路线管理
 *
 * @author haohan
 * @date 2019-06-05 09:25:40
 */
public interface RouteManageMapper extends BaseMapper<RouteManage> {

}
