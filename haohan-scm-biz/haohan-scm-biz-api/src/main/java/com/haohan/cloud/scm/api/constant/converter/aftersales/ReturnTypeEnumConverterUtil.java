package com.haohan.cloud.scm.api.constant.converter.aftersales;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.aftersales.ReturnTypeEnum;

/**
 * @author dy
 * @date 2019/8/6
 */
public class ReturnTypeEnumConverterUtil implements Converter<ReturnTypeEnum> {
    @Override
    public ReturnTypeEnum convert(Object o, ReturnTypeEnum returnTypeEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return ReturnTypeEnum.getByType(o.toString());
    }
}