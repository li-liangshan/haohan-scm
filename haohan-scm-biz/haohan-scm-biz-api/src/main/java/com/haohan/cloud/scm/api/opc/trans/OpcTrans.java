package com.haohan.cloud.scm.api.opc.trans;

import com.haohan.cloud.scm.api.constant.enums.common.DeliveryTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.*;
import com.haohan.cloud.scm.api.constant.enums.supply.SupplierStatusEnum;
import com.haohan.cloud.scm.api.goods.dto.GoodsModelDTO;
import com.haohan.cloud.scm.api.opc.entity.SummaryOrder;
import com.haohan.cloud.scm.api.opc.entity.TradeOrder;
import com.haohan.cloud.scm.api.opc.resp.TemporarySummaryOrderResp;
import com.haohan.cloud.scm.api.product.entity.ProductInfo;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseOrderDetail;
import com.haohan.cloud.scm.api.saleb.dto.BuyOrderDetailDTO;
import lombok.Data;

import java.time.LocalDateTime;
/**
 * @author dy
 * @date 2019/5/28
 */
@Data
public class OpcTrans {

    /**
     * 初始化summaryOrder 的状态值
     * @param summaryOrder
     * @return
     */
    public static SummaryOrder initSummaryOrder(SummaryOrder summaryOrder){
        summaryOrder.setStatus(PdsSummaryStatusEnum.wait);
        summaryOrder.setSummaryStatus(SummaryStatusEnum.wait);
        summaryOrder.setIsGenTrade(YesNoEnum.no);
        if(null == summaryOrder.getAllocateType()){
            summaryOrder.setAllocateType(AllocateTypeEnum.purchase);
        }
        summaryOrder.setSummaryTime(LocalDateTime.now());
        // 原系统使用goods_id 属性  此处兼容
        summaryOrder.setGoodsId(summaryOrder.getGoodsModelId());
        return summaryOrder;
    }

    /**
     * 复制商品规格相关属性
     * @param summaryOrder
     * @param model
     * @return
     */
    public static SummaryOrder copyGoodsModelInfo(SummaryOrder summaryOrder, GoodsModelDTO model){
        summaryOrder.setGoodsCategoryId(model.getGoodsCategoryId());
        summaryOrder.setGoodsImg(model.getModelUrl());
        summaryOrder.setGoodsName(model.getGoodsName());
        summaryOrder.setMarketPrice(model.getModelPrice());
        summaryOrder.setUnit(model.getModelUnit());
        // 原系统使用goods_id 属性  此处兼容
        summaryOrder.setGoodsId(model.getId());
        summaryOrder.setGoodsModelId(model.getId());
        summaryOrder.setGoodsModel(model.getModelName());
        return summaryOrder;
    }

    public static TemporarySummaryOrderResp temporarySummaryOrder(GoodsModelDTO model, BuyOrderDetailDTO detail){
      TemporarySummaryOrderResp order = new TemporarySummaryOrderResp();
      order.setGoodsImg(model.getModelUrl());
      order.setNeedBuyNum(detail.getGoodsNum());
      order.setBuySeq(BuySeqEnum.getByType(detail.getBuySeq()));
      order.setDeliveryTime(detail.getDeliveryTime());
      order.setUnit(model.getModelUnit());
      order.setGoodsModel(model.getModelName());
      order.setGoodsId(model.getGoodsId());
      order.setGoodsModelId(detail.getGoodsModelId());
      order.setGoodsStorage(model.getModelStorage());
      order.setMarketPrice(model.getModelPrice());
      order.setGoodsName(detail.getGoodsName());
      return order;
    }

    public static PurchaseOrderDetail setPropertiesTrans(GoodsModelDTO model, SummaryOrder order) {
      PurchaseOrderDetail detail = new PurchaseOrderDetail();
      detail.setPmId(order.getPmId());
      detail.setSummaryDetailSn(order.getSummaryOrderId());
      detail.setGoodsImg(model.getModelUrl());
      detail.setModelName(model.getModelName());
      detail.setGoodsName(model.getGoodsName());
      detail.setGoodsCategoryId(model.getGoodsCategoryId());
      detail.setUnit(model.getModelUnit());
      detail.setMarketPrice(model.getModelPrice());
      detail.setGoodsId(model.getGoodsId());
      detail.setGoodsCategoryName(model.getCategoryName());
      detail.setGoodsModelId(model.getId());
      return detail;
    }

    /**
     * 根据采购单明细设置交易单
     * @param tradeOrder
     * @param detail
     * @return
     */
    public static TradeOrder copyFromBuyOrderDetailDTO(TradeOrder tradeOrder, BuyOrderDetailDTO detail){
        tradeOrder.setPmId(detail.getPmId());
        tradeOrder.setSummaryBuyId(detail.getSummaryBuyId());
        tradeOrder.setBuyId(detail.getBuyId());
        tradeOrder.setBuyerId(detail.getBuyerId());
        tradeOrder.setBuySeq(BuySeqEnum.getByType(detail.getBuySeq()));
        tradeOrder.setBuyNum(detail.getGoodsNum());
        tradeOrder.setBuyTime(detail.getBuyTime());
        tradeOrder.setBuyNode(detail.getRemarks());
        tradeOrder.setMarketPrice(detail.getMarketPrice());
        tradeOrder.setBuyPrice(detail.getBuyPrice());
        tradeOrder.setContact(detail.getContact());
        tradeOrder.setContactPhone(detail.getTelephone());
        tradeOrder.setDeliveryTime(detail.getDeliveryTime());
        tradeOrder.setDeliveryAddress(detail.getAddress());
        tradeOrder.setDeliveryType(DeliveryTypeEnum.getByType(detail.getDeliveryType()));
        return tradeOrder;
    }

    /**
     * 根据货品信息设置交易单
     * @param tradeOrder
     * @param productInfo
     * @return
     */
    public static TradeOrder copyFromProductInfo(TradeOrder tradeOrder, ProductInfo productInfo){
        tradeOrder.setSupplierId(productInfo.getSupplierId());
        tradeOrder.setSupplyPrice(productInfo.getPurchasePrice());
        tradeOrder.setSortOutNum(productInfo.getProductNumber());
        // 原系统使用字段
//        tradeOrder.setOfferType(PdsOfferTypeEnum.platformOffer);
//        tradeOrder.setOfferId();
//        tradeOrder.setBuyOperator();
        return tradeOrder;
    }

    /**
     * 根据商品信息 设置交易单
     * @param tradeOrder
     * @param goodsModelDTO
     * @return
     */
    public static TradeOrder copyFromGoodsModelDTO(TradeOrder tradeOrder, GoodsModelDTO goodsModelDTO) {
        // 商品信息
        tradeOrder.setGoodsName(goodsModelDTO.getGoodsName());
        // 原系统使用goods_id 属性  此处兼容
        tradeOrder.setGoodsId(goodsModelDTO.getId());
        tradeOrder.setGoodsImg(goodsModelDTO.getModelUrl());
        tradeOrder.setGoodsModel(goodsModelDTO.getModelName());
        tradeOrder.setGoodsCategory(goodsModelDTO.getCategoryName());
        tradeOrder.setUnit(goodsModelDTO.getModelUnit());
        return tradeOrder;
    }

    /**
     * 分拣创建交易单时, 状态设置
     * @param tradeOrder
     * @return
     */
    public static TradeOrder initTradeBySorting(TradeOrder tradeOrder) {
        tradeOrder.setOpStatus(OpStatusEnum.sorted);
        tradeOrder.setDeliveryStatus(DeliveryStatusEnum.wait_delivery);
        tradeOrder.setTransStatus(TransStatusEnum.done);
        tradeOrder.setSupplierStatus(SupplierStatusEnum.shipped);
        tradeOrder.setBuyerStatus(BuyerStatusEnum.wait_ship);
        tradeOrder.setDealTime(LocalDateTime.now());
        return tradeOrder;
    }
}
