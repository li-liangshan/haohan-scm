/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.goods.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.goods.entity.StandardProductUnit;
import com.haohan.cloud.scm.api.goods.req.StandardProductUnitReq;
import com.haohan.cloud.scm.goods.service.StandardProductUnitService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 标准商品库
 *
 * @author haohan
 * @date 2019-05-29 14:26:47
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/StandardProductUnit")
@Api(value = "standardproductunit", tags = "standardproductunit内部接口服务")
public class StandardProductUnitFeignApiCtrl {

    private final StandardProductUnitService standardProductUnitService;


    /**
     * 通过id查询标准商品库
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(standardProductUnitService.getById(id));
    }


    /**
     * 分页查询 标准商品库 列表信息
     * @param standardProductUnitReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchStandardProductUnitPage")
    public R getStandardProductUnitPage(@RequestBody StandardProductUnitReq standardProductUnitReq) {
        Page page = new Page(standardProductUnitReq.getPageNo(), standardProductUnitReq.getPageSize());
        StandardProductUnit standardProductUnit =new StandardProductUnit();
        BeanUtil.copyProperties(standardProductUnitReq, standardProductUnit);

        return new R<>(standardProductUnitService.page(page, Wrappers.query(standardProductUnit)));
    }


    /**
     * 全量查询 标准商品库 列表信息
     * @param standardProductUnitReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchStandardProductUnitList")
    public R getStandardProductUnitList(@RequestBody StandardProductUnitReq standardProductUnitReq) {
        StandardProductUnit standardProductUnit =new StandardProductUnit();
        BeanUtil.copyProperties(standardProductUnitReq, standardProductUnit);

        return new R<>(standardProductUnitService.list(Wrappers.query(standardProductUnit)));
    }


    /**
     * 新增标准商品库
     * @param standardProductUnit 标准商品库
     * @return R
     */
    @Inner
    @SysLog("新增标准商品库")
    @PostMapping("/add")
    public R save(@RequestBody StandardProductUnit standardProductUnit) {
        return new R<>(standardProductUnitService.save(standardProductUnit));
    }

    /**
     * 修改标准商品库
     * @param standardProductUnit 标准商品库
     * @return R
     */
    @Inner
    @SysLog("修改标准商品库")
    @PostMapping("/update")
    public R updateById(@RequestBody StandardProductUnit standardProductUnit) {
        return new R<>(standardProductUnitService.updateById(standardProductUnit));
    }

    /**
     * 通过id删除标准商品库
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除标准商品库")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(standardProductUnitService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除标准商品库")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(standardProductUnitService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询标准商品库")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(standardProductUnitService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param standardProductUnitReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询标准商品库总记录}")
    @PostMapping("/countByStandardProductUnitReq")
    public R countByStandardProductUnitReq(@RequestBody StandardProductUnitReq standardProductUnitReq) {

        return new R<>(standardProductUnitService.count(Wrappers.query(standardProductUnitReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param standardProductUnitReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据standardProductUnitReq查询一条货位信息表")
    @PostMapping("/getOneByStandardProductUnitReq")
    public R getOneByStandardProductUnitReq(@RequestBody StandardProductUnitReq standardProductUnitReq) {

        return new R<>(standardProductUnitService.getOne(Wrappers.query(standardProductUnitReq), false));
    }


    /**
     * 批量修改OR插入
     * @param standardProductUnitList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<StandardProductUnit> standardProductUnitList) {

        return new R<>(standardProductUnitService.saveOrUpdateBatch(standardProductUnitList));
    }

}
