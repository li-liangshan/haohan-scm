/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.supply.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.bill.dto.OrderInfoDTO;
import com.haohan.cloud.scm.api.supply.entity.SupplyOrder;
import com.haohan.cloud.scm.api.supply.req.SupplyOrderReq;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.haohan.cloud.scm.supply.core.ScmSupplyOrderCoreService;
import com.haohan.cloud.scm.supply.service.SupplyOrderService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 供应订单
 *
 * @author haohan
 * @date 2019-09-21 14:58:37
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/SupplyOrder")
@Api(value = "supplyorder", tags = "supplyorder-供应订单内部接口服务}")
public class SupplyOrderFeignApiCtrl {

    private final SupplyOrderService supplyOrderService;
    private final ScmSupplyOrderCoreService supplyOrderCoreService;

    /**
     * 通过id查询供应订单
     *
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    @ApiOperation(value = "通过id查询供应订单")
    public R<SupplyOrder> getById(@PathVariable("id") String id) {
        return new R<>(supplyOrderService.getById(id));
    }


    /**
     * 分页查询 供应订单 列表信息
     *
     * @param supplyOrderReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchSupplyOrderPage")
    @ApiOperation(value = "分页查询 供应订单 列表信息")
    public R<Page<SupplyOrder>> getSupplyOrderPage(@RequestBody SupplyOrderReq supplyOrderReq) {
        Page<SupplyOrder> page = new Page<>(supplyOrderReq.getPageNo(), supplyOrderReq.getPageSize());
        return R.ok((Page<SupplyOrder>) supplyOrderService.page(page, Wrappers.query(supplyOrderReq)));
    }


    /**
     * 全量查询 供应订单 列表信息
     *
     * @param supplyOrderReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchSupplyOrderList")
    @ApiOperation(value = "全量查询 供应订单 列表信息")
    public R<List<SupplyOrder>> getSupplyOrderList(@RequestBody SupplyOrderReq supplyOrderReq) {
        SupplyOrder supplyOrder = new SupplyOrder();
        BeanUtil.copyProperties(supplyOrderReq, supplyOrder);

        return new R<>(supplyOrderService.list(Wrappers.query(supplyOrder)));
    }


    /**
     * 新增供应订单
     *
     * @param supplyOrder 供应订单
     * @return R
     */
    @Inner
    @SysLog("新增供应订单")
    @PostMapping("/add")
    @ApiOperation(value = "新增供应订单")
    public R<Boolean> save(@RequestBody SupplyOrder supplyOrder) {
        return new R<>(supplyOrderService.save(supplyOrder));
    }

    /**
     * 修改供应订单
     *
     * @param supplyOrder 供应订单
     * @return R
     */
    @Inner
    @SysLog("修改供应订单")
    @PostMapping("/update")
    @ApiOperation(value = "修改供应订单")
    public R<Boolean> updateById(@RequestBody SupplyOrder supplyOrder) {
        return new R<>(supplyOrderService.updateById(supplyOrder));
    }

    /**
     * 通过id删除供应订单
     *
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除供应订单")
    @PostMapping("/delete/{id}")
    @ApiOperation(value = "通过id删除供应订单")
    public R<Boolean> removeById(@PathVariable String id) {
        return new R<>(supplyOrderService.removeById(id));
    }

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @return R
     */
    @Inner
    @SysLog("批量删除供应订单")
    @PostMapping("/batchDelete")
    @ApiOperation(value = "通过IDS批量删除供应订单")
    public R<Boolean> removeByIds(@RequestBody List<String> idList) {
        return new R<>(supplyOrderService.removeByIds(idList));
    }


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @Inner
    @SysLog("根据IDS批量查询供应订单")
    @PostMapping("/listByIds")
    @ApiOperation(value = "根据IDS批量查询供应订单")
    public R<List<SupplyOrder>> listByIds(@RequestBody List<String> idList) {
        return R.ok((List<SupplyOrder>) supplyOrderService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param supplyOrderReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询供应订单总记录")
    @PostMapping("/countBySupplyOrderReq")
    @ApiOperation(value = "查询供应订单总记录")
    public R<Integer> countBySupplyOrderReq(@RequestBody SupplyOrderReq supplyOrderReq) {

        return new R<>(supplyOrderService.count(Wrappers.query(supplyOrderReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     *
     * @param supplyOrderReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据supplyOrderReq查询一条供应订单信息")
    @PostMapping("/getOneBySupplyOrderReq")
    @ApiOperation(value = "根据supplyOrderReq查询一条供应订单信息")
    public R<SupplyOrder> getOneBySupplyOrderReq(@RequestBody SupplyOrderReq supplyOrderReq) {

        return new R<>(supplyOrderService.getOne(Wrappers.query(supplyOrderReq), false));
    }


    /**
     * 批量修改OR插入供应订单
     *
     * @param supplyOrderList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入供应订单数据")
    @PostMapping("/saveOrUpdateBatch")
    @ApiOperation(value = "批量修改OR插入供应订单数据")
    public R<Boolean> saveOrUpdateBatch(@RequestBody List<SupplyOrder> supplyOrderList) {

        return new R<>(supplyOrderService.saveOrUpdateBatch(supplyOrderList));
    }

    @Inner
    @GetMapping("/orderInfo")
    @ApiOperation(value = "通过sn查询供应单详情")
    public R<OrderInfoDTO> fetchOrderIfo(@RequestParam("orderSn") String orderSn) {
        // 明细中商品为供应商商品
        return RUtil.success(supplyOrderCoreService.fetchOrderInfoBySupply(orderSn));
    }


}
