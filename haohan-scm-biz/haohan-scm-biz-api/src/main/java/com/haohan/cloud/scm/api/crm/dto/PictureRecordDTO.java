package com.haohan.cloud.scm.api.crm.dto;

import com.haohan.cloud.scm.api.crm.entity.PictureRecord;
import com.haohan.cloud.scm.api.manage.entity.PhotoGallery;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author dy
 * @date 2019/9/29
 */
@Data
@NoArgsConstructor
public class PictureRecordDTO {

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    private String id;
    /**
     * 客户编号
     */
    @ApiModelProperty(value = "客户编号")
    private String customerSn;
    /**
     * 客户名称
     */
    @ApiModelProperty(value = "客户名称")
    private String customerName;
    /**
     * 拍照员工id
     */
    @ApiModelProperty(value = "拍照员工id")
    private String employeeId;
    /**
     * 拍照员工姓名
     */
    @ApiModelProperty(value = "拍照员工姓名")
    private String employeeName;
    /**
     * 图片组编号
     */
    @ApiModelProperty(value = "图片组编号")
    private String groupNum;
    /**
     * 地址
     */
    @ApiModelProperty(value = "地址")
    private String address;
    /**
     * 地址定位 (经度，纬度)
     */
    @ApiModelProperty(value = "地址定位 (经度，纬度)")
    private String position;

    @ApiModelProperty(value = "备注信息")
    private String remarks;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;

    /**
     * 图片列表
     */
    @ApiModelProperty(value = "图片列表")
    private List<PhotoGallery> photoList;

    public PictureRecordDTO(PictureRecord picture) {
        this.id = picture.getId();
        this.customerSn = picture.getCustomerSn();
        this.customerName = picture.getCustomerName();
        this.employeeId = picture.getEmployeeId();
        this.employeeName = picture.getEmployeeName();
        this.groupNum = picture.getGroupNum();
        this.address = picture.getAddress();
        this.position = picture.getPosition();
        this.remarks = picture.getRemarks();
        this.createDate = picture.getCreateDate();
    }
}
