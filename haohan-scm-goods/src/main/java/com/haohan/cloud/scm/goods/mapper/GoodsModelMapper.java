/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.goods.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.goods.dto.GoodsModelDTO;
import com.haohan.cloud.scm.api.goods.dto.GoodsSqlDTO;
import com.haohan.cloud.scm.api.goods.dto.GoodsStorageRankDTO;
import com.haohan.cloud.scm.api.goods.entity.GoodsModel;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 商品规格
 *
 * @author haohan
 * @date 2019-05-13 18:46:34
 */
public interface GoodsModelMapper extends BaseMapper<GoodsModel> {

    /**
     * 关联 goods / goodsCategory
     * 返回带 商品名称/ 商品分类名称
     *
     * @param query modelId
     * @return
     */
    GoodsModelDTO getInfo(GoodsSqlDTO query);

    /**
     * 分页 查询商品规格列表
     * 关联 goods / goodsCategory
     * 返回带 商品名称/ 商品分类名称
     * 分页插件处理tenantId
     *
     * @return
     */
    IPage<GoodsModelDTO> fetchList(Page<GoodsModelDTO> page,
                                   @Param("modelName") String modelName,
                                   @Param("modelUnit") String modelUnit,
                                   @Param("modelCode") String modelCode,
                                   @Param("shopId") String shopId,
                                   @Param("goodsSn") String goodsSn,
                                   @Param("goodsId") String goodsId,
                                   @Param("isMarketable") String isMarketable,
                                   @Param("goodsName") String goodsName,
                                   @Param("goodsCategoryId") String goodsCategoryId,
                                   @Param("categoryName") String categoryName);

    /**
     * 查询商品库存排行
     *
     * @return
     */
    List<GoodsStorageRankDTO> getStorageRank();

    /**
     * 查询商品库存预警数量
     *
     * @return
     */
    Integer queryStorageWarn();

    /**
     * 查询分类及子分类 的商品规格id列表
     * @param categoryId
     * @return
     */
    List<String> fetchModelListByCategory(String categoryId);

    /**
     * 查询规格列表
     * @param query
     * @return
     */
    List<GoodsModelDTO> findGoodsModelList(GoodsSqlDTO query);
}
