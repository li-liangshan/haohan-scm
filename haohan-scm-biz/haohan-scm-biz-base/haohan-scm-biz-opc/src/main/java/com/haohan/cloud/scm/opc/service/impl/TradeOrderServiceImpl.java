/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.opc.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.constant.NumberPrefixConstant;
import com.haohan.cloud.scm.api.opc.entity.TradeOrder;
import com.haohan.cloud.scm.common.tools.util.ScmIncrementUtil;
import com.haohan.cloud.scm.opc.mapper.TradeOrderMapper;
import com.haohan.cloud.scm.opc.service.TradeOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

/**
 * 交易订单
 *
 * @author haohan
 * @date 2019-05-13 20:39:42
 */
@Service
public class TradeOrderServiceImpl extends ServiceImpl<TradeOrderMapper, TradeOrder> implements TradeOrderService {
    @Autowired
    @Lazy
    private ScmIncrementUtil scmIncrementUtil;

    @Override
    public boolean save(TradeOrder entity) {
        if (StrUtil.isEmpty(entity.getTradeId())) {
            String sn = scmIncrementUtil.inrcSnByClass(TradeOrder.class, NumberPrefixConstant.TRADE_ORDER_SN_PRE);
            entity.setTradeId(sn);
        }
        return super.save(entity);
    }

}
