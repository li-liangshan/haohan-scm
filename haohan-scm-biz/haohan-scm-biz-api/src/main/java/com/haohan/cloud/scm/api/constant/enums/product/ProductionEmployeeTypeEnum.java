package com.haohan.cloud.scm.api.constant.enums.product;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

@Getter
@AllArgsConstructor
public enum ProductionEmployeeTypeEnum {
    product("1", "生产负责人"),
    storage("2", "仓储管理员"),
    workers("3", "工人");

    private static final Map<String, ProductionEmployeeTypeEnum> MAP = new HashMap<>(8);

    static {
        for (ProductionEmployeeTypeEnum e : ProductionEmployeeTypeEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static ProductionEmployeeTypeEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

    @EnumValue
    @JsonValue
    private String type;

    private String desc;
}
