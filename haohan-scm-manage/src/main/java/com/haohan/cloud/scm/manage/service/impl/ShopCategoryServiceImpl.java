/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.manage.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.manage.entity.ShopCategory;
import com.haohan.cloud.scm.manage.mapper.ShopCategoryMapper;
import com.haohan.cloud.scm.manage.service.ShopCategoryService;
import org.springframework.stereotype.Service;

/**
 * 店铺分类
 *
 * @author haohan
 * @date 2019-05-13 17:39:16
 */
@Service
public class ShopCategoryServiceImpl extends ServiceImpl<ShopCategoryMapper, ShopCategory> implements ShopCategoryService {

}
