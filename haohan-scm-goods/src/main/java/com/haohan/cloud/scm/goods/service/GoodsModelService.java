/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.goods.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.goods.dto.GoodsModelDTO;
import com.haohan.cloud.scm.api.goods.dto.GoodsSqlDTO;
import com.haohan.cloud.scm.api.goods.dto.GoodsStorageRankDTO;
import com.haohan.cloud.scm.api.goods.entity.GoodsModel;

import java.util.List;

/**
 * 商品规格
 *
 * @author haohan
 * @date 2019-05-13 18:46:34
 */
public interface GoodsModelService extends IService<GoodsModel> {


    /**
     * 关联 goods / goodsCategory
     * 返回带 商品名称/ 商品分类名称
     *
     * @param id
     * @return
     */
    GoodsModelDTO getInfoById(String id);

    GoodsModelDTO getInfoBySn(String modelSn);

    /**
     * 关联 goods / goodsCategory
     * 返回带 商品名称/ 商品分类名称
     *
     * @param query modelId
     * @return
     */
    GoodsModelDTO getInfo(GoodsSqlDTO query);

    /**
     * 分页 查询商品规格列表
     *
     * @param page
     * @param goodsModel
     * @return
     */
    IPage<GoodsModelDTO> fetchPage(Page<GoodsModelDTO> page, GoodsModelDTO goodsModel);


    /**
     * 根据 modelId集合查询
     *
     * @param query
     * @return
     */
    List<GoodsModelDTO> findGoodsModelList(GoodsSqlDTO query);

    /**
     * 查询商品库存排行
     *
     * @return
     */
    List<GoodsStorageRankDTO> getStorageRank();

    /**
     * 查询商品库存预警数量
     *
     * @return
     */
    Integer queryStorageWarn();

    /**
     * 查询 规格列表
     *
     * @param goodsId
     * @return
     */
    List<GoodsModel> fetchModelList(String goodsId);

    List<String> fetchModelListByCategory(String categoryId);
}
