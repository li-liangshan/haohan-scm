/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.opc.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.opc.entity.DealCostDetail;
import com.haohan.cloud.scm.opc.mapper.DealCostDetailMapper;
import com.haohan.cloud.scm.opc.service.DealCostDetailService;
import org.springframework.stereotype.Service;

/**
 * 交易成本明细
 *
 * @author haohan
 * @date 2019-05-13 20:36:20
 */
@Service
public class DealCostDetailServiceImpl extends ServiceImpl<DealCostDetailMapper, DealCostDetail> implements DealCostDetailService {

}
