package com.haohan.cloud.scm.api.constant.converter.purchase;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.purchase.TaskActionTypeEnum;


/**
 * @author cx
>>>>>>> Stashed changes
 * @date 2019/6/4
 */
public class TaskActionTypeEnumConverterUtil implements Converter<TaskActionTypeEnum> {
    @Override
    public TaskActionTypeEnum convert(Object o, TaskActionTypeEnum taskActionTypeEnum) throws IllegalArgumentException {

        if (null == o) {
            return null;
        }
        return TaskActionTypeEnum.getByType(o.toString());
    }
}
