package com.haohan.cloud.scm.api.supply.req;

import com.haohan.cloud.scm.api.constant.enums.purchase.ReceiveTypeEnum;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @author dy
 * @date 2019/6/6
 */
@Data
public class AddOfferOrderReq {
    // 必需参数
    /**
     * 供应商
     */
    @NotBlank(message = "supplierId不能为空")
    private String supplierId;
    /**
     * 供应商报价
     */
    @NotNull(message = "supplyPrice不能为空")
    private BigDecimal supplyPrice;
    /**
     * 报价类型:1平台报价2市场报价3指定报价4货源报价
     */
    @NotBlank(message = "offerType不能为空")
    private String offerType;
    /**
     * 商品规格id
     */
    @NotBlank(message = "goodsModelId不能为空")
    private String goodsModelId;

    // 可选
    /**
     * 商品Id
     */
    private String goodsId;
    /**
     * 报价单号
     */
    private String offerOrderId;
    /**
     * 询价单号
     */
    private String askOrderId;
    /**
     * 采购数量
     */
    private BigDecimal buyNum;
    /**
     * 成交单价
     */
    private BigDecimal dealPrice;
    /**
     * 数量  默认 9999
     */
    private BigDecimal supplyNum = BigDecimal.valueOf(9999);
    /**
     * 实物图片
     */
    private String supplyImg;
    /**
     * 供应说明
     */
    private String supplyDesc;
    /**
     * 报价单状态1待报价2已报价3中标4未中标
     */
    private String status;
    /**
     * 发货状态:0不需发货1.待备货2.备货中3.已发货4.已接收5.售后中
     */
    private String shipStatus;
    /**
     * 询价时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime askPriceTime;

    /**
     * 起售数量
     */
    private BigDecimal minSaleNum = BigDecimal.ZERO;
    /**
     * 备货日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate prepareDate;
    /**
     * 采购批次
     */
    private String buySeq;
    /**
     * 采购明细编号
     */
    private String purchaseDetailSn;
    /**
     * 揽货方式1.自提2.送货上门  默认送货上门
     */
    private String receiveType = ReceiveTypeEnum.delivery.getType();

}
