/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.wms.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.wms.entity.Cell;
import com.haohan.cloud.scm.api.wms.req.CellReq;
import com.haohan.cloud.scm.wms.service.CellService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 货位信息表
 *
 * @author haohan
 * @date 2019-05-29 13:21:43
 */
@RestController
@AllArgsConstructor
@RequestMapping("/cell" )
@Api(value = "cell", tags = "cell管理")
public class CellController {

    private final CellService cellService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param cell 货位信息表
     * @return
     */
    @GetMapping("/page" )
    public R getCellPage(Page page, Cell cell) {
        return new R<>(cellService.page(page, Wrappers.query(cell)));
    }


    /**
     * 通过id查询货位信息表
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(cellService.getById(id));
    }

    /**
     * 新增货位信息表
     * @param cell 货位信息表
     * @return R
     */
    @SysLog("新增货位信息表" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_cell_add')" )
    public R save(@RequestBody Cell cell) {
        return new R<>(cellService.save(cell));
    }

    /**
     * 修改货位信息表
     * @param cell 货位信息表
     * @return R
     */
    @SysLog("修改货位信息表" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_cell_edit')" )
    public R updateById(@RequestBody Cell cell) {
        return new R<>(cellService.updateById(cell));
    }

    /**
     * 通过id删除货位信息表
     * @param id id
     * @return R
     */
    @SysLog("删除货位信息表" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_cell_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(cellService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除货位信息表")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_cell_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(cellService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询货位信息表")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(cellService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param cellReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询货位信息表总记录}")
    @PostMapping("/countByCellReq")
    public R countByCellReq(@RequestBody CellReq cellReq) {

        return new R<>(cellService.count(Wrappers.query(cellReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param cellReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据cellReq查询一条货位信息表")
    @PostMapping("/getOneByCellReq")
    public R getOneByCellReq(@RequestBody CellReq cellReq) {

        return new R<>(cellService.getOne(Wrappers.query(cellReq), false));
    }


    /**
     * 批量修改OR插入
     * @param cellList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_cell_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<Cell> cellList) {

        return new R<>(cellService.saveOrUpdateBatch(cellList));
    }


}
