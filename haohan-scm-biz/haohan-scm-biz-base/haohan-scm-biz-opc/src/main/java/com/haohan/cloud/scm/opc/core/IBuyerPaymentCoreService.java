package com.haohan.cloud.scm.opc.core;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.opc.entity.BuyerPayment;
import com.haohan.cloud.scm.api.opc.req.payment.CountReceivableBillReq;
import com.haohan.cloud.scm.api.opc.req.payment.PaymentDetailReq;
import com.haohan.cloud.scm.api.saleb.entity.BuyOrder;

import java.util.List;

/**
 * @author dy
 * @date 2019/7/3
 */
public interface IBuyerPaymentCoreService {

    /**
     * 分页查询 结果带联查数据
     *
     * @param page
     * @param buyerPayment
     * @return
     */
    IPage fetchPage(Page page, BuyerPayment buyerPayment);

    /**
     * 创建应收账单
     *  根据 应付来源订单编号:receivableSn/账单类型:billType 去查对应订单获取金额
     * @param buyerPayment 必需:pmId/receivableSn/billType
     * @return 失败时返回null
     */
    BuyerPayment createReceivable(BuyerPayment buyerPayment);


    /**
     * 查询应收账单 是否通过审核 带商家名称
     * @param buyerPayment 必需 pmId / BuyerPaymentId
     * @return 不为确认状态时 返回null
     */
    BuyerPayment checkPayment(BuyerPayment buyerPayment);

    /**
     * 批量创建应收账单  都是 订单应收
     *  根据 配送日期/批次
     * @param buyOrder pmId/ 配送日期/批次
     * @return
     */
    String createReceivableBatch(BuyOrder buyOrder);


    /**
     * 查询应收账单是否可审核 对应订单需为确认状态
     * @param pmId
     * @param buyerPaymentId
     * @return 账单更新金额
     */
    BuyerPayment checkReceivable(String pmId, String buyerPaymentId);

    /**
     *  审核应收账单
     * @param buyerPayment 必需 pmId / reviewStatus / buyerPayment/buyerPaymentId 可选: remarks
     * @return
     */
    boolean reviewPayment(BuyerPayment buyerPayment);

    /**
     * 查询账单详情
     * @param req
     * @return
     */
    List<Object> queryPaymentDetail(PaymentDetailReq req);

    /**
     * 查询到期的应收账单数
     * @param req
     * @return
     */
    Integer countReceivableBill(CountReceivableBillReq req);
}
