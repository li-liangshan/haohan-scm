/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.crm.feign;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.crm.entity.CustomerAddress;
import com.haohan.cloud.scm.api.crm.req.CustomerAddressReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 客户地址内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "CustomerAddressFeignService", value = ScmServiceName.SCM_CRM)
public interface CustomerAddressFeignService {


    /**
     * 通过id查询客户地址
     *
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/CustomerAddress/{id}", method = RequestMethod.GET)
    R<CustomerAddress> getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 客户地址 列表信息
     *
     * @param customerAddressReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/CustomerAddress/fetchCustomerAddressPage")
    R<Page<CustomerAddress>> getCustomerAddressPage(@RequestBody CustomerAddressReq customerAddressReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 客户地址 列表信息
     *
     * @param customerAddressReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/CustomerAddress/fetchCustomerAddressList")
    R<List<CustomerAddress>> getCustomerAddressList(@RequestBody CustomerAddressReq customerAddressReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增客户地址
     *
     * @param customerAddress 客户地址
     * @return R
     */
    @PostMapping("/api/feign/CustomerAddress/add")
    R<Boolean> save(@RequestBody CustomerAddress customerAddress, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改客户地址
     *
     * @param customerAddress 客户地址
     * @return R
     */
    @PostMapping("/api/feign/CustomerAddress/update")
    R<Boolean> updateById(@RequestBody CustomerAddress customerAddress, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除客户地址
     *
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/CustomerAddress/delete/{id}")
    R<Boolean> removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/CustomerAddress/batchDelete")
    R<Boolean> removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/CustomerAddress/listByIds")
    R<List<CustomerAddress>> listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param customerAddressReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/CustomerAddress/countByCustomerAddressReq")
    R<Integer> countByCustomerAddressReq(@RequestBody CustomerAddressReq customerAddressReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param customerAddressReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/CustomerAddress/getOneByCustomerAddressReq")
    R<CustomerAddress> getOneByCustomerAddressReq(@RequestBody CustomerAddressReq customerAddressReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param customerAddressList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/CustomerAddress/saveOrUpdateBatch")
    R<Boolean> saveOrUpdateBatch(@RequestBody List<CustomerAddress> customerAddressList, @RequestHeader(SecurityConstants.FROM) String from);


}
