package com.haohan.cloud.scm.api.wechat.req;

import com.haohan.cloud.scm.api.constant.enums.common.DeliveryTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.BuySeqEnum;
import com.haohan.cloud.scm.api.saleb.req.BuyDetailModifyReq;
import com.haohan.cloud.scm.api.saleb.req.order.BuyOrderEditReq;
import com.haohan.cloud.scm.common.tools.constant.SecondGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.Valid;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

/**
 * @author dy
 * @date 2020/5/30
 * 新增 FirstGroup
 * 修改 SecondGroup
 */
@Data
public class WxBuyOrderEditReq implements Serializable {
    private static final long serialVersionUID = 1L;

    @NotBlank(message = "通行证id不能为空")
    @Length(max = 64, message = "通行证id长度最大64字符")
    private String uid;

    @NotBlank(message = "采购订单编号不能为空", groups = SecondGroup.class)
    @Length(max = 64, message = "采购订单编号长度最大64字符")
    @ApiModelProperty(value = "采购订单编号")
    private String buyOrderSn;

    @NotNull(message = "送货日期为空")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty("送货日期")
    private LocalDate deliveryDate;

    private BuySeqEnum buySeq;

    @Length(max = 255, message = "采购需求长度最大255字符")
    private String needNote;

    @Digits(integer = 10, fraction = 2, message = "运费的整数位最大10位, 小数位最大2位")
    @ApiModelProperty(value = "运费")
    private BigDecimal shipFee;

    @ApiModelProperty(value = "配送方式")
    private DeliveryTypeEnum deliveryType;

    @Valid
    @NotEmpty(message = "商品明细不能为空")
    @ApiModelProperty("商品明细列表")
    private List<BuyDetailModifyReq> detailList;

    public BuyOrderEditReq transTo(String buyerId) {
        BuyOrderEditReq modifyReq = new BuyOrderEditReq();
        modifyReq.setBuyOrderSn(this.buyOrderSn);
        modifyReq.setBuyerId(buyerId);
        modifyReq.setDeliveryDate(this.deliveryDate);
        modifyReq.setBuySeq(this.buySeq);
        modifyReq.setNeedNote(this.needNote);
        modifyReq.setShipFee(this.shipFee);
        modifyReq.setDeliveryType(this.deliveryType);
        modifyReq.setDetailList(this.detailList);
        return modifyReq;
    }
}
