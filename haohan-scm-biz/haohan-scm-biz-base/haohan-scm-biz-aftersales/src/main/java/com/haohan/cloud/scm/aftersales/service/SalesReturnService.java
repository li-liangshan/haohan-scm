/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.aftersales.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.aftersales.entity.SalesReturn;

/**
 * 售后退货记录
 *
 * @author haohan
 * @date 2019-05-13 20:29:00
 */
public interface SalesReturnService extends IService<SalesReturn> {

}
