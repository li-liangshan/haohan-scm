package com.haohan.cloud.scm.api.constant.converter.opc;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.opc.ShipTypeEnum;

/**
 * @author dy
 * @date 2020/1/6
 */
public class ShipTypeEnumConverterUtil implements Converter<ShipTypeEnum> {

    @Override
    public ShipTypeEnum convert(Object o, ShipTypeEnum e) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return ShipTypeEnum.getByType(o.toString());
    }

}
