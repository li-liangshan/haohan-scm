package com.haohan.cloud.scm.api.crm.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.haohan.cloud.scm.api.constant.enums.market.AreaTypeEnum;
import lombok.Data;

import java.util.List;

/**
 * @author dy
 * @date 2019/9/25
 */
@Data
public class CustomerMapVO {

    /**
     * 地区名称
     */
    private String areaName;

    /**
     * 地区等级
     */
    private AreaTypeEnum level;
    /**
     * 客户数量
     */
    @JsonProperty("total")
    private Integer num;

    /**
     * 客户信息列表
     */
    @JsonProperty("details")
    private List<CustomerMapDetailVO> customerList;
    /**
     * 下级区域 客户数量信息
     */
    @JsonProperty("rows")
    private List<CustomerAreaVO> areaList;

    /**
     * 不同类型客户信息
     */
    @JsonProperty("types")
    private List<CustomerTypeVO> typeList;

    public CustomerMapVO(List<CustomerMapDetailVO> customerList, List<CustomerAreaVO> areaList, List<CustomerTypeVO> typeList) {
        this.customerList = customerList;
        this.areaList = areaList;
        this.typeList = typeList;
    }


}
