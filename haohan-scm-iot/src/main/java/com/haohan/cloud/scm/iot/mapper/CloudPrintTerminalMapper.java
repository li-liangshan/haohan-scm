/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.iot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haohan.cloud.scm.api.iot.entity.CloudPrintTerminal;

/**
 * 云打印终端管理
 *
 * @author haohan
 * @date 2019-05-13 19:12:48
 */
public interface CloudPrintTerminalMapper extends BaseMapper<CloudPrintTerminal> {

}
