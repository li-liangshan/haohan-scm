/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.wms.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.wms.entity.StoragePlace;
import com.haohan.cloud.scm.api.wms.req.StoragePlaceReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 暂存点信息表内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "StoragePlaceFeignService", value = ScmServiceName.SCM_BIZ_WMS)
public interface StoragePlaceFeignService {


    /**
     * 通过id查询暂存点信息表
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/StoragePlace/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 暂存点信息表 列表信息
     * @param storagePlaceReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/StoragePlace/fetchStoragePlacePage")
    R getStoragePlacePage(@RequestBody StoragePlaceReq storagePlaceReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 暂存点信息表 列表信息
     * @param storagePlaceReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/StoragePlace/fetchStoragePlaceList")
    R getStoragePlaceList(@RequestBody StoragePlaceReq storagePlaceReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增暂存点信息表
     * @param storagePlace 暂存点信息表
     * @return R
     */
    @PostMapping("/api/feign/StoragePlace/add")
    R save(@RequestBody StoragePlace storagePlace, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改暂存点信息表
     * @param storagePlace 暂存点信息表
     * @return R
     */
    @PostMapping("/api/feign/StoragePlace/update")
    R updateById(@RequestBody StoragePlace storagePlace, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除暂存点信息表
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/StoragePlace/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/StoragePlace/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/StoragePlace/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param storagePlaceReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/StoragePlace/countByStoragePlaceReq")
    R countByStoragePlaceReq(@RequestBody StoragePlaceReq storagePlaceReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param storagePlaceReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/StoragePlace/getOneByStoragePlaceReq")
    R getOneByStoragePlaceReq(@RequestBody StoragePlaceReq storagePlaceReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param storagePlaceList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/StoragePlace/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<StoragePlace> storagePlaceList, @RequestHeader(SecurityConstants.FROM) String from);


}
