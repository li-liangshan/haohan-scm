package com.haohan.cloud.scm.api.common;

import com.haohan.cloud.framework.entity.BaseResp;
import com.haohan.cloud.scm.api.common.req.supplier.SupplierGoodsRelationReq;
import com.haohan.cloud.scm.api.common.req.supplier.SupplyPriceApiReq;
import com.haohan.cloud.scm.common.tools.http.MethodType;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;

/**
 * Created by zgw on 2019/5/22.
 * @author zgw
 */
@Getter
@AllArgsConstructor
public enum SupplierApiConstant implements IApiEnum{

    admin_supplier_paymentRecord("供应商交易单按日生成货款记录", "/f/pds/api/supplier/info/paymentRecord", HashMap.class, BaseResp.class, MethodType.POST),
    admin_supplier_confirm("供应商商品分类列表查询带关联状态", "/f/pds/api/supplier/info/goods/categoryStatusList", HashMap.class, BaseResp.class, MethodType.POST),

    admin_supplier_fetchGoodsList("供应商商品列表查询", "/f/pds/api/supplier/info/goods/fetchList", HashMap.class, BaseResp.class, MethodType.POST),

    admin_supplier_modifyGoods("供应商商品关联新增/取消", "/f/pds/api/supplier/info/goods/modify", HashMap.class, BaseResp.class, MethodType.POST),

    admin_supplier_querySupplyPriceList("供应商商品供应价格查询", "/f/pds/api/supplier/info/goods/querySupplyPriceList", SupplyPriceApiReq.class, BaseResp.class, MethodType.POST),

    admin_supplier_relationGoods("批量生成供应商和平台商家商品的映射关系", "/f/pds/api/supplier/info/goods/relationGoods", SupplierGoodsRelationReq.class, BaseResp.class, MethodType.POST);


    private String desc;
    private String url;
    private Class reqClass;
    private Class respClass;
    private MethodType methodType;
}
