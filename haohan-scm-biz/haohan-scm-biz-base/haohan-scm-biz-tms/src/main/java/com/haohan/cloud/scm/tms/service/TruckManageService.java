/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.tms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.tms.entity.TruckManage;

/**
 * 车辆管理
 *
 * @author haohan
 * @date 2019-06-05 09:26:20
 */
public interface TruckManageService extends IService<TruckManage> {

}
