package com.haohan.cloud.scm.opc.api.ctrl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.opc.entity.ShipRecord;
import com.haohan.cloud.scm.api.opc.entity.Shipper;
import com.haohan.cloud.scm.api.opc.req.ship.EditShipRecordReq;
import com.haohan.cloud.scm.api.opc.req.ship.QueryShipRecordReq;
import com.haohan.cloud.scm.api.opc.req.ship.QueryShipperReq;
import com.haohan.cloud.scm.api.opc.vo.ShipRecordVO;
import com.haohan.cloud.scm.common.tools.constant.SingleGroup;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.haohan.cloud.scm.opc.core.ShipManageCoreService;
import com.pig4cloud.pigx.common.core.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.groups.Default;

/**
 * @author dy
 * @date 2020/1/6
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/opc/shipManage")
@Api(value = "shipManage", tags = "发货管理")
public class ShipManageApiCtrl {

    private final ShipManageCoreService shipManageCoreService;

    /**
     * 通过sn查询发货记录详情
     *
     * @param req
     * @return R
     */
    @ApiOperation(value = "通过sn查询", notes = "通过sn查询")
    @GetMapping("/record/fetchInfo")
    public R<ShipRecordVO> fetchInfo(@Validated(SingleGroup.class) QueryShipRecordReq req) {
        return RUtil.success(shipManageCoreService.fetchInfo(req.getShipRecordSn()));
    }

    /**
     * 通过订单sn查询发货记录详情
     *
     * @param orderSn
     * @return R
     */
    @ApiOperation(value = "通过订单sn查询")
    @GetMapping("/record/fetchInfoByOrderSn")
    public R<ShipRecordVO> fetchInfoByOrderSn(String orderSn) {
        if(StrUtil.isEmpty(orderSn)){
            return R.failed("订单编号不能为空");
        }
        return RUtil.success(shipManageCoreService.fetchInfoByOrderSn(orderSn));
    }

    /**
     * 分页查询
     *
     * @param page 分页对象
     * @param req  发货记录
     * @return
     */
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @GetMapping("/record/page")
    public R<IPage<ShipRecord>> queryShipRecordPage(Page page, @Validated QueryShipRecordReq req) {
        return RUtil.success(shipManageCoreService.fetchPage(page, req));
    }

    @ApiOperation(value = "完成发货操作", notes = "选择发货人，物流公司")
    @PostMapping("/record/complete")
    public R<ShipRecord> completeShip(@Validated({SingleGroup.class, Default.class}) EditShipRecordReq req) {
        return RUtil.success(shipManageCoreService.completeShip(req));
    }


    /**
     * 分页查询发货人
     *
     * @param page
     * @param req
     * @return
     */
    @ApiOperation(value = "分页查询发货人")
    @GetMapping("/shipper/page")
    public R<IPage<Shipper>> queryShipperPage(Page page, @Validated QueryShipperReq req) {
        return RUtil.success(shipManageCoreService.fetchShipperPage(page, req));
    }

}
