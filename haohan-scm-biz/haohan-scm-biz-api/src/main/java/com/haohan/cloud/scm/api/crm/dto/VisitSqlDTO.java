package com.haohan.cloud.scm.api.crm.dto;

import com.haohan.cloud.scm.api.common.dto.SelfSqlDTO;
import com.haohan.cloud.scm.api.constant.enums.crm.VisitStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.crm.VisitStepEnum;
import com.haohan.cloud.scm.api.constant.enums.market.CustomerStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.market.CustomerTypeEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author dy
 * @date 2020/1/3
 */
@Data

@EqualsAndHashCode(callSuper = true)
public class VisitSqlDTO extends SelfSqlDTO {

    @ApiModelProperty(value = "主键")
    private String id;

    @ApiModelProperty(value = "客户编号")
    private String customerSn;

    @ApiModelProperty(value = "拜访联系人id")
    private String linkmanId;

    @ApiModelProperty(value = "拜访员工id")
    private String employeeId;

    @ApiModelProperty(value = "拜访状态: 1.待拜访 2.拜访中 3.已拜访 4.未完成  VisitStatusEnum")
    private String visitStatus;

    @ApiModelProperty(value = "客户状态:0.无1潜在2.有意向3成交4失败 CustomerStatusEnum")
    private String customerStatus;

    @ApiModelProperty(value = "进展阶段:1初次拜访2了解交流3.深入跟进4达成合作5商务往来 VisitStepEnum")
    private String  visitStep;

    // 非eq

    @ApiModelProperty(value = "in 客户编号列表, 逗号分割")
    private String customerSns;

    @ApiModelProperty(value = "客户名称")
    private String customerName;

    @ApiModelProperty(value = "拜访联系人名称")
    private String linkmanName;

    @ApiModelProperty(value = "拜访员工名称")
    private String employeeName;

    // 客户联查

    @ApiModelProperty(value = "in销售区域列表, 逗号分割")
    private String areaSns;

    @ApiModelProperty(value = "客户类型", notes = "客户类型:1经销商2门店 CustomerTypeEnum")
    private String customerType;

}
