/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.crm.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.haohan.cloud.scm.api.constant.enums.common.ReviewStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.crm.PayStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.crm.SalesTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * 销售订单
 *
 * @author haohan
 * @date 2019-09-04 18:32:05
 */
@Data
@TableName("crm_sales_order")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "销售订单")
public class SalesOrder extends Model<SalesOrder> {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 销售订单编号
     */
    @ApiModelProperty(value = "销售订单编号")
    private String salesOrderSn;
    /**
     * 客户编号
     */
    @ApiModelProperty(value = "客户编号")
    private String customerSn;
    /**
     * 客户名称
     */
    @ApiModelProperty(value = "客户名称")
    private String customerName;
    /**
     * 供货商编号
     */
    @ApiModelProperty(value = "供货商编号")
    private String supplierSn;
    /**
     * 供货商名称
     */
    @ApiModelProperty(value = "供货商名称")
    private String supplierName;
    /**
     * 下单时间
     */
    @ApiModelProperty(value = "下单时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime orderTime;
    /**
     * 交货日期
     */
    @ApiModelProperty(value = "交货日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate deliveryDate;
    /**
     * 合计金额
     */
    @ApiModelProperty(value = "合计金额")
    private BigDecimal sumAmount;

    @ApiModelProperty(value = "其他金额")
    private BigDecimal otherAmount;
    /**
     * 总计金额  = 合计 + 其他 -优惠
     */
    @ApiModelProperty(value = "总计金额")
    private BigDecimal totalAmount;
    /**
     * 销售单类型:1代客下单,2自主下单
     */
    @ApiModelProperty(value = "销售单类型:1代客下单,2自主下单")
    private SalesTypeEnum salesType;
    /**
     * 业务员
     */
    @ApiModelProperty(value = "业务员")
    private String employeeId;
    /**
     * 业务员名称
     */
    @ApiModelProperty(value = "业务员名称")
    private String employeeName;
    /**
     * 收货人id
     */
    @ApiModelProperty(value = "收货人id")
    private String linkmanId;
    /**
     * 收货人名称
     */
    @ApiModelProperty(value = "收货人名称")
    private String linkmanName;
    /**
     * 收货地址
     */
    @ApiModelProperty(value = "收货地址")
    private String address;
    /**
     * 收货人手机号
     */
    @ApiModelProperty(value = "收货人手机号")
    private String telephone;
    /**
     * 审核状态: 1.待审核2.审核不通过3.审核通过
     */
    @ApiModelProperty(value = "审核状态: 1.待审核2.审核不通过3.审核通过")
    private ReviewStatusEnum reviewStatus;
    /**
     * 货品种数
     */
    @ApiModelProperty(value = "货品种数")
    private Integer goodsNum;
    /**
     * 订单支付状态:0未付,1已付,2部分支付
     */
    @ApiModelProperty(value = "订单支付状态:0未付,1已付,2部分支付")
    private PayStatusEnum payStatus;
    /**
     * 外部订单号
     */
    @ApiModelProperty(value = "外部订单号")
    private String externalSn;
    /**
     * 创建者
     */
    @ApiModelProperty(value = "创建者")
    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
     * 更新者
     */
    @ApiModelProperty(value = "更新者")
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.UPDATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateDate;
    /**
     * 备注信息
     */
    @ApiModelProperty(value = "备注信息")
    private String remarks;
    /**
     * 删除标记
     */
    @ApiModelProperty(value = "删除标记")
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;
    /**
     * 租户id
     */
    @ApiModelProperty(value = "租户id")
    private Integer tenantId;

}
