package com.haohan.cloud.scm.api.constant.converter.manage;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.manage.PdsUpassportStatusEnum;

/**
 * @author dy
 * @date 2019/9/12
 */
public class PdsUpassportStatusEnumConverterUtil implements Converter<PdsUpassportStatusEnum> {
    @Override
    public PdsUpassportStatusEnum convert(Object o, PdsUpassportStatusEnum e) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return PdsUpassportStatusEnum.getByType(o.toString());
    }
}
