package com.haohan.cloud.scm.api.constant.converter.market;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.market.CustomerTypeEnum;

/**
 * @author xwx
 * @date 2019/6/5
 */
public class CustomerTypeEnumConverterUtil implements Converter<CustomerTypeEnum> {
    @Override
    public CustomerTypeEnum convert(Object o, CustomerTypeEnum customerTypeEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return CustomerTypeEnum.getByType(o.toString());
    }
}
