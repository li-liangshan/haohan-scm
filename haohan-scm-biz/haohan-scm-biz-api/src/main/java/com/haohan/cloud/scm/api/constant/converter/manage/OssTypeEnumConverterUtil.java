package com.haohan.cloud.scm.api.constant.converter.manage;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.manage.OssTypeEnum;

/**
 * @author dy
 * @date 2019/9/12
 */
public class OssTypeEnumConverterUtil implements Converter<OssTypeEnum> {
    @Override
    public OssTypeEnum convert(Object o, OssTypeEnum ossTypeEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return OssTypeEnum.getByType(o.toString());
    }
}
