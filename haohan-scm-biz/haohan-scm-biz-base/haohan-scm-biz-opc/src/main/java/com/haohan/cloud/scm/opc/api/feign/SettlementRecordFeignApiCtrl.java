/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.opc.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.opc.entity.SettlementRecord;
import com.haohan.cloud.scm.api.opc.req.ReadySettlementReq;
import com.haohan.cloud.scm.api.opc.req.SettlementRecordReq;
import com.haohan.cloud.scm.api.pay.entity.OrderPayRecord;
import com.haohan.cloud.scm.opc.core.IScmOpcSettlementService;
import com.haohan.cloud.scm.opc.service.SettlementRecordService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 结算记录
 *
 * @author haohan
 * @date 2019-05-30 10:19:56
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/SettlementRecord")
@Api(value = "settlementrecord", tags = "settlementrecord内部接口服务")
public class SettlementRecordFeignApiCtrl {

    private final SettlementRecordService settlementRecordService;
    private final IScmOpcSettlementService scmOpcSettlementService;


    /**
     * 通过id查询结算记录
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(settlementRecordService.getById(id));
    }


    /**
     * 分页查询 结算记录 列表信息
     * @param settlementRecordReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchSettlementRecordPage")
    public R getSettlementRecordPage(@RequestBody SettlementRecordReq settlementRecordReq) {
        Page page = new Page(settlementRecordReq.getPageNo(), settlementRecordReq.getPageSize());
        SettlementRecord settlementRecord =new SettlementRecord();
        BeanUtil.copyProperties(settlementRecordReq, settlementRecord);

        return new R<>(settlementRecordService.page(page, Wrappers.query(settlementRecord)));
    }


    /**
     * 全量查询 结算记录 列表信息
     * @param settlementRecordReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchSettlementRecordList")
    public R getSettlementRecordList(@RequestBody SettlementRecordReq settlementRecordReq) {
        SettlementRecord settlementRecord =new SettlementRecord();
        BeanUtil.copyProperties(settlementRecordReq, settlementRecord);

        return new R<>(settlementRecordService.list(Wrappers.query(settlementRecord)));
    }


    /**
     * 新增结算记录
     * @param settlementRecord 结算记录
     * @return R
     */
    @Inner
    @SysLog("新增结算记录")
    @PostMapping("/add")
    public R save(@RequestBody SettlementRecord settlementRecord) {
        return new R<>(settlementRecordService.save(settlementRecord));
    }

    /**
     * 修改结算记录
     * @param settlementRecord 结算记录
     * @return R
     */
    @Inner
    @SysLog("修改结算记录")
    @PostMapping("/update")
    public R updateById(@RequestBody SettlementRecord settlementRecord) {
        return new R<>(settlementRecordService.updateById(settlementRecord));
    }

    /**
     * 通过id删除结算记录
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除结算记录")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(settlementRecordService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除结算记录")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(settlementRecordService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询结算记录")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(settlementRecordService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param settlementRecordReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询结算记录总记录}")
    @PostMapping("/countBySettlementRecordReq")
    public R countBySettlementRecordReq(@RequestBody SettlementRecordReq settlementRecordReq) {

        return new R<>(settlementRecordService.count(Wrappers.query(settlementRecordReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param settlementRecordReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据settlementRecordReq查询一条货位信息表")
    @PostMapping("/getOneBySettlementRecordReq")
    public R getOneBySettlementRecordReq(@RequestBody SettlementRecordReq settlementRecordReq) {

        return new R<>(settlementRecordService.getOne(Wrappers.query(settlementRecordReq), false));
    }


    /**
     * 批量修改OR插入
     * @param settlementRecordList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<SettlementRecord> settlementRecordList) {

        return new R<>(settlementRecordService.saveOrUpdateBatch(settlementRecordList));
    }

    /**
     * 准备结算
     * 先判断对应的账单 其订单来源是否为确定状态(金额不会改变)
     * 确定时会创建对应结算记录(初始化,未结算)
     * 失败时不创建结算记录
     *
     * @param req 必需:pmId/settlementType/ paymentSn
     * @return 失败时返回null
     */
    @Inner
    @SysLog("准备结算")
    @PostMapping("/readySettlement")
    @ApiOperation(value = "准备结算")
    public R<SettlementRecord> readySettlement(@RequestBody ReadySettlementReq req){
        return scmOpcSettlementService.readySettlement(req);
    }

    @Inner
    @SysLog("在线支付后完成结算")
    @PostMapping("/payToSettlement")
    @ApiOperation(value = "在线支付后完成结算")
    public R<Boolean> payToSettlement(@RequestBody OrderPayRecord payRecord){
        return scmOpcSettlementService.payToSettlement(payRecord);
    }



}
