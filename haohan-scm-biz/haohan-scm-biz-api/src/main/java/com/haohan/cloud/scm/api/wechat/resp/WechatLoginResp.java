package com.haohan.cloud.scm.api.wechat.resp;

import com.haohan.cloud.scm.api.constant.enums.purchase.EmployeeTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.supply.PdsSupplierTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.wechat.WechatUseTypeEnum;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseEmployee;
import com.haohan.cloud.scm.api.supply.entity.Supplier;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author dy
 * @date 2019/7/23
 */
@Data
@ApiModel("小程序登录返回")
public class WechatLoginResp {

    @ApiModelProperty(value = "平台商家id", required = true)
    private String pmId;

    @ApiModelProperty(value = "人员id", required = true)
    private String id;

    @ApiModelProperty(value = "名称", required = true)
    private String name;

    @ApiModelProperty(value = "联系电话", required = true)
    private String telephone;

    @ApiModelProperty(value = "人员类型", required = true)
    private String type;

    @ApiModelProperty(value = "使用类型:1.采购,2.供应", required = true)
    private String wechatUseType;


    public void copyFormPurchase(PurchaseEmployee employee) {
        this.pmId = employee.getPmId();
        this.id = employee.getId();
        this.name = employee.getName();
        this.telephone = employee.getTelephone();
        EmployeeTypeEnum employeeType = employee.getPurchaseEmployeeType();
        if (null == employeeType) {
            employeeType = EmployeeTypeEnum.buyer;
        }
        this.type = employeeType.getType();
        this.wechatUseType = WechatUseTypeEnum.purchase.getType();
    }

    public void copyFormSupplier(Supplier supplier) {
        this.pmId = supplier.getPmId();
        this.id = supplier.getId();
        this.name = supplier.getSupplierName();
        this.telephone = supplier.getTelephone();
        PdsSupplierTypeEnum supplierType = supplier.getSupplierType();
        if (null == supplierType) {
            supplierType = PdsSupplierTypeEnum.ordinary;
        }
        this.type = supplierType.getType();
        this.wechatUseType = WechatUseTypeEnum.supply.getType();
    }
}
