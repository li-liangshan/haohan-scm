/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.haohan.cloud.scm.api.opc.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.haohan.cloud.scm.api.constant.enums.common.DeliveryTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.*;
import com.haohan.cloud.scm.api.constant.enums.supply.PdsOfferTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.supply.SupplierStatusEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * 交易订单
 *
 * @author haohan
 * @date 2019-05-13 20:39:42
 */
@Data
@TableName("scm_trade_order")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "交易订单")
public class TradeOrder extends Model<TradeOrder> {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 平台商家id
     */
    private String pmId;
    /**
     * 交易单号
     */
    private String tradeId;
    /**
     * 汇总单号
     */
    private String summaryBuyId;
    /**
     * 采购编号
     */
    private String buyId;
    /**
     * 报价类型
     */
    private PdsOfferTypeEnum offerType;
    /**
     * 报价单号
     */
    private String offerId;
    /**
     * 采购商
     */
    private String buyerId;
    /**
     * 供应商
     */
    private String supplierId;
    /**
     * 采购批次
     */
    private BuySeqEnum buySeq;
    /**
     * 商品ID
     */
    private String goodsId;
    /**
     * 商品名称
     */
    private String goodsName;
    /**
     * 商品图片
     */
    private String goodsImg;
    /**
     * 商品规格
     */
    private String goodsModel;
    /**
     * 商品分类
     */
    private String goodsCategory;
    /**
     * 单位
     */
    private String unit;
    /**
     * 采购数量
     */
    private BigDecimal buyNum;
    /**
     * 采购时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime buyTime;
    /**
     * 采购备注
     */
    private String buyNode;
    /**
     * 市场价
     */
    private BigDecimal marketPrice;
    /**
     * 供应单价
     */
    private BigDecimal supplyPrice;
    /**
     * 采购单价
     */
    private BigDecimal buyPrice;
    /**
     * 成交时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime dealTime;
    /**
     * 联系人
     */
    private String contact;
    /**
     * 联系电话
     */
    private String contactPhone;
    /**
     * 送货时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd ")
    private LocalDate deliveryTime;
    /**
     * 送货地址
     */
    private String deliveryAddress;
    /**
     * 供应商状态
     */
    private SupplierStatusEnum supplierStatus;
    /**
     * 采购商状态
     */
    private BuyerStatusEnum buyerStatus;
    /**
     * 采购员
     */
    private String buyOperator;
    /**
     * 运营状态
     */
    private OpStatusEnum opStatus;
    /**
     * 配送状态
     */
    private DeliveryStatusEnum deliveryStatus;
    /**
     * 交易状态
     */
    private TransStatusEnum transStatus;
    /**
     * 分拣数量
     */
    private BigDecimal sortOutNum;
    /**
     * 配送方式
     */
    private DeliveryTypeEnum deliveryType;
    /**
     * 货品编号
     */
    @ApiModelProperty(value = "货品编号")
    private String productSn;
    /**
     * 创建者
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
     * 更新者
     */
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateDate;
    /**
     * 备注信息
     */
    private String remarks;
    /**
     * 删除标记
     */
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;
    /**
     * 租户id
     */
    private Integer tenantId;

}
