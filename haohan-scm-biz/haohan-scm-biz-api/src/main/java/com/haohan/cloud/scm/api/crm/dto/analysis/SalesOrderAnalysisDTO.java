package com.haohan.cloud.scm.api.crm.dto.analysis;

import cn.hutool.core.util.StrUtil;
import com.haohan.cloud.scm.api.common.dto.SelfSqlDTO;
import com.haohan.cloud.scm.api.constant.enums.crm.SalesGoodsTypeEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.math.BigDecimal;

/**
 * @author dy
 * @date 2019/11/7
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Api("销售订单分析")
public class SalesOrderAnalysisDTO extends SelfSqlDTO {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "客户订单数")
    private Integer orderNum;

    @ApiModelProperty(value = "销售金额")
    private BigDecimal salesAmount;

    @ApiModelProperty(value = "审核状态: 1.待审核2.审核不通过3.审核通过", notes = "ReviewStatusEnum")
    private String reviewStatus;

    @ApiModelProperty(value = "排除的审核状态", notes = "ReviewStatusEnum")
    private String excludeStatus;

    @ApiModelProperty(value = "客户编号")
    private String customerSn;

    @ApiModelProperty(value = "客户名称")
    private String customerName;

    @ApiModelProperty(value = "业务员")
    private String employeeId;

    @ApiModelProperty(value = "业务员名称")
    private String employeeName;

    @ApiModelProperty(value = "商品名称")
    private String goodsName;

    @ApiModelProperty(value = "商品规格名称")
    private String modelName;

    @ApiModelProperty(value = "单位")
    private String unit;

    @ApiModelProperty(value = "商品规格编号")
    private String goodsModelSn;

    @ApiModelProperty(value = "业务员id列表")
    private String employeeIds;

    @ApiModelProperty(value = "客户sn列表")
    private String customerSns;

    @ApiModelProperty(value = "是否按客户分组")
    private Boolean customerGroup;

    @ApiModelProperty(value = "商品数")
    private BigDecimal goodsNum;

    @ApiModelProperty(value = "区域编码")
    private String areaSn;

    @ApiModelProperty(value = "市场编码")
    private String marketSn;

    @ApiModelProperty(value = "商品销售类型:1.普通2.特价3.赠品")
    private SalesGoodsTypeEnum salesGoodsType;

    @ApiModelProperty(value = "订单上报人id")
    private String reportManId;

    // 字典处理(自定义sql使用)

    public String getSalesGoodsType() {
        return null == this.salesGoodsType ? null : this.salesGoodsType.getType();
    }

    /**
     * 处理上报人查询
     * reportManId 传入即 只查询该上报人
     */
    public void reportManHandle() {
        if (StrUtil.isNotEmpty(this.reportManId)) {
            this.employeeId = this.reportManId;
        }
    }
}
