/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.supply.api.feign;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.supply.entity.SupplierGoods;
import com.haohan.cloud.scm.api.supply.req.*;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.haohan.cloud.scm.supply.core.IScmSupplierGoodsService;
import com.haohan.cloud.scm.supply.core.IScmSupplyOrderService;
import com.haohan.cloud.scm.supply.core.ScmSupplyGoodsCoreService;
import com.haohan.cloud.scm.supply.service.SupplierGoodsService;
import com.haohan.cloud.scm.supply.utils.ScmSupplyUtils;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 供应商货物表
 *
 * @author haohan
 * @date 2019-05-29 13:13:14
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/SupplierGoods")
@Api(value = "suppliergoods", tags = "suppliergoods内部接口服务")
public class SupplierGoodsFeignApiCtrl {

    private final ScmSupplyGoodsCoreService supplyGoodsCoreService;
    private final SupplierGoodsService supplierGoodsService;
    private final IScmSupplyOrderService scmSupplyOrderService;
    private final ScmSupplyUtils scmSupplyUtils;
    private final IScmSupplierGoodsService scmSupplierGoodsService;
    /**
     * 通过id查询供应商货物表
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(supplierGoodsService.getById(id));
    }


    /**
     * 分页查询 供应商货物表 列表信息
     * @param supplierGoodsReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchSupplierGoodsPage")
    public R getSupplierGoodsPage(@RequestBody SupplierGoodsReq supplierGoodsReq) {
        Page page = new Page(supplierGoodsReq.getPageNo(), supplierGoodsReq.getPageSize());
        SupplierGoods supplierGoods =new SupplierGoods();
        BeanUtil.copyProperties(supplierGoodsReq, supplierGoods);

        return new R<>(supplierGoodsService.page(page, Wrappers.query(supplierGoods)));
    }


    /**
     * 全量查询 供应商货物表 列表信息
     * @param supplierGoodsReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchSupplierGoodsList")
    public R getSupplierGoodsList(@RequestBody SupplierGoodsReq supplierGoodsReq) {
        SupplierGoods supplierGoods =new SupplierGoods();
        BeanUtil.copyProperties(supplierGoodsReq, supplierGoods);

        return new R<>(supplierGoodsService.list(Wrappers.query(supplierGoods)));
    }


    /**
     * 新增供应商货物表
     * @param supplierGoods 供应商货物表
     * @return R
     */
    @Inner
    @SysLog("新增供应商货物表")
    @PostMapping("/add")
    public R save(@RequestBody SupplierGoods supplierGoods) {
        return new R<>(supplierGoodsService.save(supplierGoods));
    }

    /**
     * 修改供应商货物表
     * @param supplierGoods 供应商货物表
     * @return R
     */
    @Inner
    @SysLog("修改供应商货物表")
    @PostMapping("/update")
    public R updateById(@RequestBody SupplierGoods supplierGoods) {
        return new R<>(supplierGoodsService.updateById(supplierGoods));
    }

    /**
     * 通过id删除供应商货物表
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除供应商货物表")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(supplierGoodsService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除供应商货物表")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(supplierGoodsService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询供应商货物表")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(supplierGoodsService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param supplierGoodsReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询供应商货物表总记录}")
    @PostMapping("/countBySupplierGoodsReq")
    public R countBySupplierGoodsReq(@RequestBody SupplierGoodsReq supplierGoodsReq) {

        return new R<>(supplierGoodsService.count(Wrappers.query(supplierGoodsReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param supplierGoodsReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据supplierGoodsReq查询一条货位信息表")
    @PostMapping("/getOneBySupplierGoodsReq")
    public R getOneBySupplierGoodsReq(@RequestBody SupplierGoodsReq supplierGoodsReq) {

        return new R<>(supplierGoodsService.getOne(Wrappers.query(supplierGoodsReq), false));
    }


    /**
     * 批量修改OR插入
     * @param supplierGoodsList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<SupplierGoods> supplierGoodsList) {

        return new R<>(supplierGoodsService.saveOrUpdateBatch(supplierGoodsList));
    }

    @Inner
    @PostMapping("/querySupplyGoodsRepertoryList")
    @ApiOperation(value = "查询供应商品库存信息列表")
    public R querySupplyGoodsRepertoryList(@RequestBody @Validated QueryGoodsRepertoryListReq req){
        Page page = scmSupplyOrderService.querySupplyGoodsRepertoryList(req);
        return new R<>(page);
    }

    @Inner
    @PostMapping("/queryList")
    @ApiOperation(value = "查询供应商 售卖商品信息列表")
    public R querySupplierGoodsList(@RequestBody @Validated SupplierGoodsListReq req) {
        if (StrUtil.isEmpty(req.getUid())) {
            //判断供应商是否被该采购员工管理
            scmSupplyUtils.fetchByBuyerUid(req.getUid(), req.getSupplierId());
        }
        Page page = scmSupplierGoodsService.querySupplierGoodsList(req);
        return new R<>(page);
    }
    /**
     * 查询供应商 售卖商品信息详情
     * @param req  必须：pmId/uId/supplierId/id
     * @return
     */
    @Inner
    @PostMapping("/queryDetail")
    @ApiOperation(value = "查询供应商 售卖商品信息详情")
    public R querySupplierGoodsDetail(@RequestBody SupplierGoodsDetailReq req){

        return new R<>(scmSupplierGoodsService.querySupplierGoodsDetail(req));
    }
    /**
     * 查询供应商品库存详情
     * @param req   必须：id/pmId
     * @return
     */
    @Inner
    @PostMapping("/querySupplyGoodsRepertoryDetail")
    @ApiOperation(value = "查询供应商品库存详情")
    public R querySupplyGoodsRepertoryDetail(@RequestBody QueryGoodsRepertoryDetailReq req){

        return new R<>(scmSupplyOrderService.querySupplyGoodsRepertory(req));
    }

    /**
     * 删除商品的供应关联关系
     *
     * @param req goodsId
     * @return
     */
    @Inner
    @PostMapping("/deleteRelationByGoods")
    @ApiOperation(value = "删除商品的供应关联关系")
    public R<Boolean> deleteRelationByGoods(@RequestBody SupplierGoodsReq req) {
        return RUtil.success(supplyGoodsCoreService.deleteRelationByGoods(req.getGoodsId()));
    }
}
