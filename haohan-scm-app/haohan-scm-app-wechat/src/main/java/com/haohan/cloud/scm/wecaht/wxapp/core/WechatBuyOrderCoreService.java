package com.haohan.cloud.scm.wecaht.wxapp.core;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.haohan.cloud.scm.api.goods.vo.GoodsVO;
import com.haohan.cloud.scm.api.saleb.vo.BuyOrderVO;
import com.haohan.cloud.scm.api.wechat.req.WxBuyOrderEditReq;
import com.haohan.cloud.scm.api.wechat.req.WxBuyOrderReq;
import com.haohan.cloud.scm.api.wechat.req.WxGoodsQueryReq;

/**
 * @author dy
 * @date 2020/5/30
 */
public interface WechatBuyOrderCoreService {

    /**
     * 分页查询采购单
     *
     * @param req
     * @return
     */
    IPage<BuyOrderVO> findPage(WxBuyOrderReq req);

    /**
     * 查询采购单详情
     *
     * @param req
     * @return
     */
    BuyOrderVO fetchInfo(WxBuyOrderReq req);

    /**
     * 新增采购单
     *
     * @param req
     * @return
     */
    BuyOrderVO addBuyOrder(WxBuyOrderEditReq req);

    boolean modifyBuyOrder(WxBuyOrderEditReq req);

    boolean cancelBuyOrder(WxBuyOrderReq req);
    /**
     * 采购商 经常购买商品详情列表 带收藏状态
     * @param req
     * @return
     */
    IPage<GoodsVO> findOftenPage(WxGoodsQueryReq req);

}
