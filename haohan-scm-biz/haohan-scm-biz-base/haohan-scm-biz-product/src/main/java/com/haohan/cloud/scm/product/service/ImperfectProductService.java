/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.product.entity.ImperfectProduct;

/**
 * 残次货品处理记录
 *
 * @author haohan
 * @date 2019-05-13 18:20:47
 */
public interface ImperfectProductService extends IService<ImperfectProduct> {

}
