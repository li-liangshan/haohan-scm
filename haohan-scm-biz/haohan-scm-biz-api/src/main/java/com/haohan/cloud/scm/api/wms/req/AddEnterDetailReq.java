package com.haohan.cloud.scm.api.wms.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author xwx
 * @date 2019/6/14
 */
@Data
@ApiModel(description = "创建入库单明细 根据采购单明细")
public class AddEnterDetailReq {
    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台id", required = true)
    private String pmId;

    @NotBlank(message = "purchaseDetailSn不能为空")
    @ApiModelProperty(value = "采购单明细编号", required = true)
    private String purchaseDetailSn;

    @ApiModelProperty(value = "入库申请人id")
    private String applicantId;

    @ApiModelProperty(value = "入库申请人名称")
    private String applicantName;
}
