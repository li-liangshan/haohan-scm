package com.haohan.cloud.scm.api.constant.enums.common;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xwx
 * @date 2019/5/30
 */
@Getter
@AllArgsConstructor
public enum PdsPayTypeEnum implements IBaseEnum {
    /**
     * 支付方式：01.微信扫码02.微信刷卡03.微信公众号04.微信小程序05.支付宝扫码06.支付宝条码07.支付宝服务窗12.现金支付
     */
    sweepWeChatYards("01","微信扫码"),
    weChatSwipe("02","微信刷卡"),
    weChatOfficialAccount("03","微信公众号"),
    appletWeChat("04","微信小程序"),
    alipayScanCode("05","支付宝扫码"),
    alipayBarcode("06","支付宝条码"),
    alipayServiceWindow("07","支付宝服务窗"),
    readyUp("12","现金支付");

    private static final Map<String, PdsPayTypeEnum> MAP = new HashMap<>(8);

    static {
        for (PdsPayTypeEnum e : PdsPayTypeEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static PdsPayTypeEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

    /**
     * 类型
     */
    @EnumValue
    @JsonValue
    private String type;
    /**
     * 描述
     */

    private String desc;
}
