package com.haohan.cloud.scm.wecaht.wxapp.api;

import com.haohan.cloud.scm.api.wechat.req.WechatAddReq;
import com.haohan.cloud.scm.api.wechat.req.WxTenantReq;
import com.haohan.cloud.scm.api.wechat.vo.WxPassportVO;
import com.haohan.cloud.scm.api.wechat.vo.WxTenantVO;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.haohan.cloud.scm.wecaht.wxapp.core.WechatCommonCoreService;
import com.pig4cloud.pigx.common.core.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author dy
 * @date 2020/5/27
 * 小程序通用接口(通行证获取、开发平台用户处理)
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/wechat/common")
@Api(value = "CommonApiCtrl", tags = "微信小程序通用接口服务")
public class WechatCommonApiCtrl {

    private final WechatCommonCoreService wechantCommonCoreService;

    /**
     * 小程序用户租户id获取
     * 后续修改从小程序登陆接口获取
     * @param req
     * @return
     */
    @GetMapping("/tenantId")
    @ApiOperation(value = "小程序用户租户id获取")
    R<WxTenantVO> fetchTenantId(@Validated WxTenantReq req) {
        return RUtil.success(wechantCommonCoreService.fetchTenantId(req));
    }

    /**
     * 小程序用户通行证获取
     *   微信code验证返回uid
     *
     * @param req
     * @return
     */
    @PostMapping("/passport")
    @ApiOperation(value = "小程序用户通行证获取")
    R<WxPassportVO> addPassport(@Validated WechatAddReq req) {
        return RUtil.success(wechantCommonCoreService.addPassport(req));
    }


}
