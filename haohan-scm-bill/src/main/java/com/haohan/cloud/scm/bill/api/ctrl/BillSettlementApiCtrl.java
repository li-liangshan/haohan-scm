package com.haohan.cloud.scm.bill.api.ctrl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.bill.entity.SettlementAccount;
import com.haohan.cloud.scm.api.bill.req.SettlementFinishReq;
import com.haohan.cloud.scm.api.bill.req.SettlementReadyReq;
import com.haohan.cloud.scm.api.bill.req.SettlementReq;
import com.haohan.cloud.scm.api.bill.req.SummarySettlementReq;
import com.haohan.cloud.scm.api.bill.vo.SettlementInfoVO;
import com.haohan.cloud.scm.bill.core.SettlementCoreService;
import com.haohan.cloud.scm.common.tools.constant.SecondGroup;
import com.haohan.cloud.scm.common.tools.constant.SingleGroup;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @author dy
 * @date 2019/12/28
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/bill/settlement")
@Api(value = "BillSettlement", tags = "账单结算管理")
public class BillSettlementApiCtrl {

    private final SettlementCoreService settlementCoreService;

    @GetMapping("/queryInfo")
    @ApiOperation(value = "结算单详情", notes = "根据settlementSn查询")
    public R<SettlementInfoVO> fetchInfo(@Validated({SingleGroup.class}) SettlementReq req) {
        return RUtil.success(settlementCoreService.fetchInfo(req.getSettlementSn()));
    }

    @GetMapping("/page")
    @ApiOperation(value = "结算单分页查询")
    public R<IPage> findPage(Page<SettlementAccount> page, @Validated SettlementReq req) {
        return RUtil.success(settlementCoreService.findPage(page, req));
    }

    @SysLog("结算单准备结算")
    @PostMapping("/ready")
    @ApiOperation(value = "结算单准备结算")
    public R<SettlementInfoVO> readySettlement(@Validated SettlementReadyReq req) {
        return RUtil.success(settlementCoreService.readySettlement(req.getSettlementSn(), req.getSettlementAmount()));
    }

    @SysLog("多账单合并结算单")
    @PostMapping("/summary/create")
    @ApiOperation(value = "多账单合并结算单")
    public R<SettlementAccount> createSummarySettlement(@RequestBody @Validated SummarySettlementReq req) {
        return RUtil.success(settlementCoreService.createSummarySettlement(req));
    }

    @SysLog("结算单完成收款结算")
    @PostMapping("/finish")
    @ApiOperation(value = "结算单完成收款结算")
    public R<Boolean> finishSummarySettlement(@RequestBody @Validated SettlementFinishReq req) {
        settlementCoreService.finishSettlement(req.transTo());
        return RUtil.success(true);
    }

    @SysLog("结算撤销")
    @PostMapping("/revoke")
    @ApiOperation(value = "结算撤销", notes = "结算单对应账单需重新审核，结算单删除")
    public R<Boolean> revokeSettlement(@Validated({SecondGroup.class}) SettlementReadyReq req) {
        settlementCoreService.revokeSettlement(req.getSettlementSn());
        return RUtil.success(true);
    }

}
