/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.manage.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.manage.entity.Brand;
import com.haohan.cloud.scm.manage.mapper.BrandMapper;
import com.haohan.cloud.scm.manage.service.BrandService;
import org.springframework.stereotype.Service;

/**
 * 品牌管理
 *
 * @author haohan
 * @date 2019-05-13 17:12:26
 */
@Service
public class BrandServiceImpl extends ServiceImpl<BrandMapper, Brand> implements BrandService {

}
