/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.iot.req;

import com.haohan.cloud.scm.api.iot.entity.CloudPrintTerminal;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 云打印终端管理
 *
 * @author haohan
 * @date 2019-05-28 20:31:39
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "云打印终端管理")
public class CloudPrintTerminalReq extends CloudPrintTerminal {

    private long pageSize;
    private long pageNo;




}
