/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.manage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.manage.entity.BusinessNote;

/**
 * 商务留言
 *
 * @author haohan
 * @date 2019-05-13 17:15:46
 */
public interface BusinessNoteService extends IService<BusinessNote> {

}
