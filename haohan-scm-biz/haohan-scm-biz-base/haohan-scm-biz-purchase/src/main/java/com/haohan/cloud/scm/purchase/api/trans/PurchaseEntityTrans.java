package com.haohan.cloud.scm.purchase.api.trans;

import cn.hutool.core.bean.BeanUtil;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseAccount;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseEmployee;
import com.haohan.cloud.scm.api.purchase.resp.PurchaseEmployeeDetailResp;

/**
 * @author dy
 * @date 2019/5/17
 */
public class PurchaseEntityTrans {

    public static <T> boolean transFromEmployee(T resp, PurchaseEmployee purchaseemployee) {
        if (null == purchaseemployee) {
            return false;
        }
        BeanUtil.copyProperties(purchaseemployee, resp);
        return true;
    }

    public static boolean transFromAccount(PurchaseEmployeeDetailResp resp, PurchaseAccount account) {
        if (null == account) {
            return false;
        }
        resp.setAccountBalance(account.getAccountBalance());
        resp.setAccountStatus(account.getStatus());
        return true;
    }


}
