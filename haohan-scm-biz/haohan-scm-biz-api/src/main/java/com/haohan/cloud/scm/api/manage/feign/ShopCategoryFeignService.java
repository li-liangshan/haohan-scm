/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.manage.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.manage.entity.ShopCategory;
import com.haohan.cloud.scm.api.manage.req.ShopCategoryReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 店铺分类内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "ShopCategoryFeignService", value = ScmServiceName.SCM_MANAGE)
public interface ShopCategoryFeignService {


    /**
     * 通过id查询店铺分类
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/ShopCategory/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 店铺分类 列表信息
     * @param shopCategoryReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/ShopCategory/fetchShopCategoryPage")
    R getShopCategoryPage(@RequestBody ShopCategoryReq shopCategoryReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 店铺分类 列表信息
     * @param shopCategoryReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/ShopCategory/fetchShopCategoryList")
    R getShopCategoryList(@RequestBody ShopCategoryReq shopCategoryReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增店铺分类
     * @param shopCategory 店铺分类
     * @return R
     */
    @PostMapping("/api/feign/ShopCategory/add")
    R save(@RequestBody ShopCategory shopCategory, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改店铺分类
     * @param shopCategory 店铺分类
     * @return R
     */
    @PostMapping("/api/feign/ShopCategory/update")
    R updateById(@RequestBody ShopCategory shopCategory, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除店铺分类
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/ShopCategory/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/ShopCategory/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/ShopCategory/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param shopCategoryReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/ShopCategory/countByShopCategoryReq")
    R countByShopCategoryReq(@RequestBody ShopCategoryReq shopCategoryReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param shopCategoryReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/ShopCategory/getOneByShopCategoryReq")
    R getOneByShopCategoryReq(@RequestBody ShopCategoryReq shopCategoryReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param shopCategoryList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/ShopCategory/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<ShopCategory> shopCategoryList, @RequestHeader(SecurityConstants.FROM) String from);


}
