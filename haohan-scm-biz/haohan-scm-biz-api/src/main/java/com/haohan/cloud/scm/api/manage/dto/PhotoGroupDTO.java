package com.haohan.cloud.scm.api.manage.dto;

import com.haohan.cloud.scm.api.manage.entity.PhotoGallery;
import com.haohan.cloud.scm.api.manage.trans.PhotoTrans;
import lombok.Data;

import java.util.List;

/**
 * @author dy
 * @date 2019/9/11
 */
@Data
public class PhotoGroupDTO {

    // 图片组信息
    /**
     * 商家ID
     */
    private String merchantId;
    /**
     * 图片组编号
     */
    private String groupNum;
    /**
     * 图片组名称
     */
    private String groupName;
    /**
     * 类别标签
     */
    private String categoryTag;

    /**
     * 图片资源 列表 (不为null)
     */
    private List<PhotoGallery> photoList;

    public void transPhotoList(List<String> list) {
        photoList = PhotoTrans.transPhotoList(list);
    }

}
