/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.opc.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.opc.entity.DealCostDetail;
import com.haohan.cloud.scm.api.opc.req.DealCostDetailReq;
import com.haohan.cloud.scm.opc.service.DealCostDetailService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 交易成本明细
 *
 * @author haohan
 * @date 2019-05-30 10:17:46
 */
@RestController
@AllArgsConstructor
@RequestMapping("/dealcostdetail" )
@Api(value = "dealcostdetail", tags = "dealcostdetail管理")
public class DealCostDetailController {

    private final DealCostDetailService dealCostDetailService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param dealCostDetail 交易成本明细
     * @return
     */
    @GetMapping("/page" )
    public R getDealCostDetailPage(Page page, DealCostDetail dealCostDetail) {
        return new R<>(dealCostDetailService.page(page, Wrappers.query(dealCostDetail)));
    }


    /**
     * 通过id查询交易成本明细
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(dealCostDetailService.getById(id));
    }

    /**
     * 新增交易成本明细
     * @param dealCostDetail 交易成本明细
     * @return R
     */
    @SysLog("新增交易成本明细" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_dealcostdetail_add')" )
    public R save(@RequestBody DealCostDetail dealCostDetail) {
        return new R<>(dealCostDetailService.save(dealCostDetail));
    }

    /**
     * 修改交易成本明细
     * @param dealCostDetail 交易成本明细
     * @return R
     */
    @SysLog("修改交易成本明细" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_dealcostdetail_edit')" )
    public R updateById(@RequestBody DealCostDetail dealCostDetail) {
        return new R<>(dealCostDetailService.updateById(dealCostDetail));
    }

    /**
     * 通过id删除交易成本明细
     * @param id id
     * @return R
     */
    @SysLog("删除交易成本明细" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_dealcostdetail_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(dealCostDetailService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除交易成本明细")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_dealcostdetail_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(dealCostDetailService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询交易成本明细")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(dealCostDetailService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param dealCostDetailReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询交易成本明细总记录}")
    @PostMapping("/countByDealCostDetailReq")
    public R countByDealCostDetailReq(@RequestBody DealCostDetailReq dealCostDetailReq) {

        return new R<>(dealCostDetailService.count(Wrappers.query(dealCostDetailReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param dealCostDetailReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据dealCostDetailReq查询一条货位信息表")
    @PostMapping("/getOneByDealCostDetailReq")
    public R getOneByDealCostDetailReq(@RequestBody DealCostDetailReq dealCostDetailReq) {

        return new R<>(dealCostDetailService.getOne(Wrappers.query(dealCostDetailReq), false));
    }


    /**
     * 批量修改OR插入
     * @param dealCostDetailList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_dealcostdetail_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<DealCostDetail> dealCostDetailList) {

        return new R<>(dealCostDetailService.saveOrUpdateBatch(dealCostDetailList));
    }


}
