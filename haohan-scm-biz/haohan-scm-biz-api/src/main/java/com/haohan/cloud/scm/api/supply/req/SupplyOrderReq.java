/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.supply.req;

import com.haohan.cloud.scm.api.supply.entity.SupplyOrder;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 供应订单
 *
 * @author haohan
 * @date 2019-09-21 14:58:37
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "供应订单")
public class SupplyOrderReq extends SupplyOrder {

    private long pageSize;
    private long pageNo;


}
