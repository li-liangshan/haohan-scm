/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.crm.dto;

import com.haohan.cloud.scm.api.constant.enums.common.UseStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.market.CompanyNatureEnum;
import com.haohan.cloud.scm.api.constant.enums.market.CustomerTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.supply.GradeTypeEnum;
import com.haohan.cloud.scm.api.manage.entity.PhotoGallery;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 客户基础信息
 *
 * @author haohan
 * @date 2019-09-04 18:29:58
 */
@Data
@ApiModel(description = "客户基础信息")
public class CustomerDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    private String id;
    /**
     * 客户编号
     */
    @ApiModelProperty(value = "客户编号")
    private String customerSn;
    /**
     * 客户名称
     */
    @ApiModelProperty(value = "客户名称")
    private String customerName;
    /**
     * 销售区域编号
     */
    @ApiModelProperty(value = "销售区域")
    private String areaSn;
    /**
     * 销售区域名称
     */
    @ApiModelProperty(value = "销售区域")
    private String areaName;

    @ApiModelProperty(value = "市场编码")
    private String marketSn;

    @ApiModelProperty(value = "市场名称")
    private String marketName;
    /**
     * 客户联系人名称
     */
    @ApiModelProperty(value = "客户联系人")
    private String contact;
    /**
     * 联系人手机
     */
    @ApiModelProperty(value = "联系手机")
    private String telephone;
    /**
     * 座机电话
     */
    @ApiModelProperty(value = "座机电话")
    private String phoneNumber;
    /**
     * 省
     */
    @ApiModelProperty(value = "省")
    private String province;
    /**
     * 市
     */
    @ApiModelProperty(value = "市")
    private String city;
    /**
     * 区
     */
    @ApiModelProperty(value = "区")
    private String district;
    /**
     * 街道、乡镇
     */
    @ApiModelProperty(value = "街道、乡镇")
    private String street;
    /**
     * 详细地址
     */
    @ApiModelProperty(value = "详细地址")
    private String address;
    /**
     * 客户地址定位 (经度，纬度)
     */
    @ApiModelProperty(value = "客户地址定位 (经度，纬度)")
    private String position;
    /**
     * 客户标签
     */
    @ApiModelProperty(value = "客户标签")
    private String tags;
    /**
     * 邮编
     */
    @ApiModelProperty(value = "邮编")
    private String postcode;
    /**
     * 传真
     */
    @ApiModelProperty(value = "传真")
    private String fax;
    /**
     * 网址
     */
    @ApiModelProperty(value = "网址")
    private String website;
    /**
     * 营业时间描述
     */
    @ApiModelProperty(value = "营业时间")
    private String serviceTime;
    /**
     * 门头照图片组编号
     */
    @ApiModelProperty(value = "门头照图片组编号")
    private String photoGroupNum;
    /**
     * 经营面积
     */
    @ApiModelProperty(value = "经营面积")
    private String operateArea;
    /**
     * 年经营流水
     */
    @ApiModelProperty(value = "年经营流水")
    private String shopSale;
    /**
     * 业务介绍
     */
    @ApiModelProperty(value = "业务介绍")
    private String bizDesc;
    /**
     * 营业执照
     */
    @ApiModelProperty(value = "营业执照")
    private String bizLicense;
    /**
     * 营业执照名称
     */
    @ApiModelProperty(value = "营业执照名称")
    private String licenseName;

    @ApiModelProperty(value = "营业执照图片组编号")
    private String licenseGroupNum;
    /**
     * 工商注册号
     */
    @ApiModelProperty(value = "工商注册号")
    private String registrationNum;
    /**
     * 注册日期
     */
    @ApiModelProperty(value = "注册日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate registrationDate;
    /**
     * 客户注册全称
     */
    @ApiModelProperty(value = "客户注册全称")
    private String regName;
    /**
     * 经营法人名称
     */
    @ApiModelProperty(value = "经营法人名称")
    private String legalName;
    /**
     * 经营地址
     */
    @ApiModelProperty(value = "经营地址")
    private String regAddress;
    /**
     * 收录时间
     */
    @ApiModelProperty(value = "收录时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime initTime;
    /**
     * 客户状态   0.未启用 1.启用 2.待审核
     */
    @ApiModelProperty(value = "客户状态")
    private UseStatusEnum status;
    /**
     * 客户类型:1经销商2门店
     */
    @ApiModelProperty(value = "客户类型:1经销商2门店")
    private CustomerTypeEnum customerType;
    /**
     * 客户负责人id
     */
    @ApiModelProperty(value = "客户负责人id")
    private String directorId;

    @ApiModelProperty(value = "客户负责人名称")
    private String directorName;
    /**
     * 公司性质:1.个体2.民营3.国有4.其他
     */
    @ApiModelProperty(value = "公司性质:1.个体2.民营3.国有4.其他")
    private CompanyNatureEnum companyNature;
    /**
     * 客户级别:1星-5星
     */
    @ApiModelProperty(value = "客户级别:1星-5星")
    private GradeTypeEnum customerLevel;
    /**
     * 备注信息
     */
    @ApiModelProperty(value = "备注信息")
    private String remarks;

    /**
     * 门头照图片列表
     */
    @ApiModelProperty(value = "门头照图片列表")
    private List<PhotoGallery> photoList;

    @ApiModelProperty(value = "营业执照图片列表")
    private List<PhotoGallery> licensePhotoList;

    @ApiModelProperty("距离")
    private BigDecimal distance;

}
