package com.haohan.cloud.scm.manage.api.ctrl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.manage.entity.Merchant;
import com.haohan.cloud.scm.api.manage.req.merchant.EditMerchantReq;
import com.haohan.cloud.scm.api.manage.req.merchant.QueryMerchantReq;
import com.haohan.cloud.scm.api.manage.vo.MerchantVO;
import com.haohan.cloud.scm.common.tools.constant.FirstGroup;
import com.haohan.cloud.scm.common.tools.constant.SecondGroup;
import com.haohan.cloud.scm.common.tools.constant.SingleGroup;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.haohan.cloud.scm.manage.core.MerchantCoreService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @author dy
 * @date 2020/3/25
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/manage/merchant")
@Api(value = "ApiManageMerchant", tags = "MerchantManage 商家管理")
public class MerchantManageApiCtrl {

    private final MerchantCoreService merchantCoreService;


    @GetMapping("/page")
    public R<IPage<MerchantVO>> findPage(Page<Merchant> page, @Validated QueryMerchantReq req) {
        return RUtil.success(merchantCoreService.findPage(page, req));
    }

    @GetMapping("/info")
    public R<MerchantVO> fetchInfo(@Validated({SingleGroup.class}) QueryMerchantReq req) {
        return RUtil.success(merchantCoreService.fetchInfo(req.getMerchantId()));
    }

    @GetMapping("/platform")
    @ApiOperation("查询平台商家信息")
    public R<MerchantVO> fetchPlatform() {
        return RUtil.success(merchantCoreService.fetchPlatform());
    }

    @SysLog("初始化平台商家信息")
    @PreAuthorize("@pms.hasPermission('scm_merchant_add')")
    @PostMapping("/platform")
    public R<MerchantVO> initPlatform(@Validated(FirstGroup.class) EditMerchantReq req) {
        Merchant merchant = req.transTo();
        merchant.setId(null);
        return RUtil.success(merchantCoreService.initPlatform(merchant));
    }

    @SysLog("新增商家信息")
    @PreAuthorize("@pms.hasPermission('scm_merchant_add')")
    @PostMapping
    public R<Boolean> add(@Validated(FirstGroup.class) EditMerchantReq req) {
        Merchant merchant = req.transTo();
        merchant.setId(null);
        return RUtil.success(merchantCoreService.addMerchant(merchant));
    }

    @SysLog("修改商家信息")
    @PreAuthorize("@pms.hasPermission('scm_merchant_edit')")
    @PutMapping
    public R<Boolean> modify(@Validated(SecondGroup.class) EditMerchantReq req) {
        return RUtil.success(merchantCoreService.modifyMerchant(req.transTo()));
    }

    @SysLog("删除商家信息")
    @PreAuthorize("@pms.hasPermission('scm_merchant_del')")
    @DeleteMapping("/{id}")
    public R<Boolean> delete(@PathVariable String id) {
        return RUtil.success(merchantCoreService.deleteMerchant(id));
    }

}
