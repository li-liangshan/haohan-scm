package com.haohan.cloud.scm.api.constant.converter.product;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.product.ShelfOperateEnum;

/**
 * @author xwx
 * @date 2019/6/5
 */
public class ShelfOperateEnumConverterUtil implements Converter<ShelfOperateEnum> {
    @Override
    public ShelfOperateEnum convert(Object o, ShelfOperateEnum shelfOperateEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return ShelfOperateEnum.getByType(o.toString());
    }
}
