/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.crm.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.haohan.cloud.scm.api.constant.enums.crm.DataReportEnum;
import com.haohan.cloud.scm.api.constant.enums.crm.DataReportStatusEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * 数据汇报
 *
 * @author haohan
 * @date 2019-09-04 18:31:52
 */
@Data
@TableName("crm_data_report")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "数据汇报")
public class DataReport extends Model<DataReport> {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 上报编号
     */
    @ApiModelProperty(value = "上报编号")
    private String reportSn;
    /**
     * 上报类型 0.库存 1.销售记录 2.竞品
     */
    @ApiModelProperty(value = "上报类型 0.库存 1.销售记录 2.竞品")
    private DataReportEnum reportType;
    /**
     * 客户编码
     */
    @ApiModelProperty(value = "客户编码")
    private String customerSn;
    /**
     * 客户名称
     */
    @ApiModelProperty(value = "客户名称")
    private String customerName;
    /**
     * 图片组编号
     */
    @ApiModelProperty(value = "图片组编号")
    private String photoGroupNum;
    /**
     * 上报位置定位
     */
    @ApiModelProperty(value = "上报位置定位")
    private String reportLocation;

    @ApiModelProperty(value = "上报位置地址")
    private String reportAddress;
    /**
     * 上报人id(员工)
     */
    @ApiModelProperty(value = "上报人id(员工)")
    private String reportManId;
    /**
     * 上报人名称
     */
    @ApiModelProperty(value = "上报人名称")
    private String reportMan;
    /**
     * 上报日期\销售日期
     */
    @ApiModelProperty(value = "上报日期/销售日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate reportDate;
    /**
     * 上报人手机号
     */
    @ApiModelProperty(value = "上报人手机号")
    private String reportTelephone;
    /**
     * 上报状态 0.待确认 1.已确认 2.不通过
     */
    @ApiModelProperty(value = "上报状态 0.待确认 1.已确认 2.不通过")
    private DataReportStatusEnum reportStatus;
    /**
     * 操作人id
     */
    @ApiModelProperty(value = "操作人id")
    private String operatorId;
    /**
     * 操作人
     */
    @ApiModelProperty(value = "操作人")
    private String operatorName;
    /**
     * 商品总数
     */
    @ApiModelProperty(value = "商品总数")
    private Integer goodsTotalNum;

    @ApiModelProperty(value = "其他金额")
    private BigDecimal otherAmount;

    @ApiModelProperty(value = "商品合计金额")
    private BigDecimal sumAmount;
    /**
     * 商品总金额  = 合计 + 其他 -优惠
     */
    @ApiModelProperty(value = "总金额")
    private BigDecimal totalAmount;
    /**
     * 创建者
     */
    @ApiModelProperty(value = "创建者")
    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
     * 更新者
     */
    @ApiModelProperty(value = "更新者")
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.UPDATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateDate;
    /**
     * 备注信息
     */
    @ApiModelProperty(value = "备注信息")
    private String remarks;
    /**
     * 删除标记
     */
    @ApiModelProperty(value = "删除标记")
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;
    /**
     * 租户id
     */
    @ApiModelProperty(value = "租户id")
    private Integer tenantId;

}
