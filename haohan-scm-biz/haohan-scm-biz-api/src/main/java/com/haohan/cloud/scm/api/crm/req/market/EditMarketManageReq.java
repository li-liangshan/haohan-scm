package com.haohan.cloud.scm.api.crm.req.market;

import com.haohan.cloud.scm.api.constant.ScmCommonConstant;
import com.haohan.cloud.scm.api.constant.enums.market.MarketTypeEnum;
import com.haohan.cloud.scm.api.crm.entity.MarketManage;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.util.List;

/**
 * @author dy
 * @date 2019/9/26
 */
@Data
public class EditMarketManageReq {
    @ApiModelProperty(value = "主键")
    @Length(min = 0, max = 32, message = "主键长度在0至32之间")
    private String id;

    @NotBlank(message = "销售区域不能为空")
    @ApiModelProperty(value = "区域编码")
    @Length(min = 0, max = 32, message = "区域编码长度在0至32之间")
    private String areaSn;
//    @ApiModelProperty(value = "市场编码")
//    private String marketSn;

    @NotBlank(message = "市场名称不能为空")
    @ApiModelProperty(value = "市场名称")
    @Length(min = 0, max = 32, message = "市场名称长度在0至32之间")
    private String marketName;

    @ApiModelProperty(value = "市场类型")
    private MarketTypeEnum marketType;

    @ApiModelProperty(value = "标签")
    @Length(min = 0, max = 64, message = "标签长度在0至64之间")
    private String tags;

    @ApiModelProperty(value = "描述")
    @Length(min = 0, max = 255, message = "描述长度在0至255之间")
    private String description;

    @ApiModelProperty(value = "电话")
    @Length(min = 0, max = 20, message = "电话长度在0至20之间")
    private String phone;

    @ApiModelProperty(value = "图片组编号")
    @Length(min = 0, max = 32, message = "图片组编号长度在0至32之间")
    private String photoGroupNum;

    @ApiModelProperty(value = "省")
    @Length(min = 0, max = 32, message = "省长度在0至32之间")
    private String province;

    @ApiModelProperty(value = "市")
    @Length(min = 0, max = 32, message = "市长度在0至32之间")
    private String city;

    @ApiModelProperty(value = "区")
    @Length(min = 0, max = 32, message = "区长度在0至32之间")
    private String district;

    @ApiModelProperty(value = "街道、乡镇")
    @Length(min = 0, max = 32, message = "街道、乡镇长度在0至32之间")
    private String street;

    @ApiModelProperty(value = "地址")
    @Length(min = 0, max = 255, message = "地址长度在0至255之间")
    private String address;

    @Pattern(regexp = ScmCommonConstant.POSITION_PATTEN, message = "市场地址定位, 经纬度格式有误, 经度正负180度之间, 纬度正负90度之间, 弧度数表示")
    @Length(min = 0, max = 64, message = "地址定位长度在0至64之间")
    @ApiModelProperty(value = "地址定位 (经度，纬度)")
    private String position;

    @ApiModelProperty(value = "备注信息")
    @Length(min = 0, max = 255, message = "备注信息长度在0至255之间")
    private String remarks;

    @ApiModelProperty(value = "图片列表")
    private List<String> photoList;


    public MarketManage transTo() {
        MarketManage marketManage = new MarketManage();
        marketManage.setId(this.id);
        marketManage.setAreaSn(this.areaSn);
        marketManage.setMarketName(this.marketName);
        marketManage.setMarketType(this.marketType);
        marketManage.setTags(this.tags);
        marketManage.setDescription(this.description);
        marketManage.setPhone(this.phone);
        marketManage.setPhotoGroupNum(this.photoGroupNum);
        marketManage.setProvince(this.province);
        marketManage.setCity(this.city);
        marketManage.setDistrict(this.district);
        marketManage.setStreet(this.street);
        marketManage.setAddress(this.address);
        marketManage.setPosition(this.position);
        marketManage.setRemarks(this.remarks);
        return marketManage;
    }
}
