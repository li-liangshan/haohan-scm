/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.product.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.product.entity.Recipe;
import com.haohan.cloud.scm.api.product.req.RecipeReq;
import com.haohan.cloud.scm.product.service.RecipeService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 商品加工配方表
 *
 * @author haohan
 * @date 2019-05-29 14:46:05
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/Recipe")
@Api(value = "recipe", tags = "recipe内部接口服务")
public class RecipeFeignApiCtrl {

    private final RecipeService recipeService;


    /**
     * 通过id查询商品加工配方表
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(recipeService.getById(id));
    }


    /**
     * 分页查询 商品加工配方表 列表信息
     * @param recipeReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchRecipePage")
    public R getRecipePage(@RequestBody RecipeReq recipeReq) {
        Page page = new Page(recipeReq.getPageNo(), recipeReq.getPageSize());
        Recipe recipe =new Recipe();
        BeanUtil.copyProperties(recipeReq, recipe);

        return new R<>(recipeService.page(page, Wrappers.query(recipe)));
    }


    /**
     * 全量查询 商品加工配方表 列表信息
     * @param recipeReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchRecipeList")
    public R getRecipeList(@RequestBody RecipeReq recipeReq) {
        Recipe recipe =new Recipe();
        BeanUtil.copyProperties(recipeReq, recipe);

        return new R<>(recipeService.list(Wrappers.query(recipe)));
    }


    /**
     * 新增商品加工配方表
     * @param recipe 商品加工配方表
     * @return R
     */
    @Inner
    @SysLog("新增商品加工配方表")
    @PostMapping("/add")
    public R save(@RequestBody Recipe recipe) {
        return new R<>(recipeService.save(recipe));
    }

    /**
     * 修改商品加工配方表
     * @param recipe 商品加工配方表
     * @return R
     */
    @Inner
    @SysLog("修改商品加工配方表")
    @PostMapping("/update")
    public R updateById(@RequestBody Recipe recipe) {
        return new R<>(recipeService.updateById(recipe));
    }

    /**
     * 通过id删除商品加工配方表
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除商品加工配方表")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(recipeService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除商品加工配方表")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(recipeService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询商品加工配方表")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(recipeService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param recipeReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询商品加工配方表总记录}")
    @PostMapping("/countByRecipeReq")
    public R countByRecipeReq(@RequestBody RecipeReq recipeReq) {

        return new R<>(recipeService.count(Wrappers.query(recipeReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param recipeReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据recipeReq查询一条货位信息表")
    @PostMapping("/getOneByRecipeReq")
    public R getOneByRecipeReq(@RequestBody RecipeReq recipeReq) {

        return new R<>(recipeService.getOne(Wrappers.query(recipeReq), false));
    }


    /**
     * 批量修改OR插入
     * @param recipeList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<Recipe> recipeList) {

        return new R<>(recipeService.saveOrUpdateBatch(recipeList));
    }

}
