/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.goods.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.goods.entity.ProductAttrValue;
import com.haohan.cloud.scm.api.goods.req.ProductAttrValueReq;
import com.haohan.cloud.scm.goods.service.ProductAttrValueService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 商品库属性值
 *
 * @author haohan
 * @date 2019-05-29 14:26:23
 */
@RestController
@AllArgsConstructor
@RequestMapping("/productattrvalue" )
@Api(value = "productattrvalue", tags = "productattrvalue管理")
public class ProductAttrValueController {

    private final ProductAttrValueService productAttrValueService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param productAttrValue 商品库属性值
     * @return
     */
    @GetMapping("/page" )
    public R getProductAttrValuePage(Page page, ProductAttrValue productAttrValue) {
        return new R<>(productAttrValueService.page(page, Wrappers.query(productAttrValue)));
    }


    /**
     * 通过id查询商品库属性值
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(productAttrValueService.getById(id));
    }

    /**
     * 新增商品库属性值
     * @param productAttrValue 商品库属性值
     * @return R
     */
    @SysLog("新增商品库属性值" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_productattrvalue_add')" )
    public R save(@RequestBody ProductAttrValue productAttrValue) {
        return new R<>(productAttrValueService.save(productAttrValue));
    }

    /**
     * 修改商品库属性值
     * @param productAttrValue 商品库属性值
     * @return R
     */
    @SysLog("修改商品库属性值" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_productattrvalue_edit')" )
    public R updateById(@RequestBody ProductAttrValue productAttrValue) {
        return new R<>(productAttrValueService.updateById(productAttrValue));
    }

    /**
     * 通过id删除商品库属性值
     * @param id id
     * @return R
     */
    @SysLog("删除商品库属性值" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_productattrvalue_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(productAttrValueService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除商品库属性值")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_productattrvalue_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(productAttrValueService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询商品库属性值")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(productAttrValueService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param productAttrValueReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询商品库属性值总记录}")
    @PostMapping("/countByProductAttrValueReq")
    public R countByProductAttrValueReq(@RequestBody ProductAttrValueReq productAttrValueReq) {

        return new R<>(productAttrValueService.count(Wrappers.query(productAttrValueReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param productAttrValueReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据productAttrValueReq查询一条货位信息表")
    @PostMapping("/getOneByProductAttrValueReq")
    public R getOneByProductAttrValueReq(@RequestBody ProductAttrValueReq productAttrValueReq) {

        return new R<>(productAttrValueService.getOne(Wrappers.query(productAttrValueReq), false));
    }


    /**
     * 批量修改OR插入
     * @param productAttrValueList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_productattrvalue_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<ProductAttrValue> productAttrValueList) {

        return new R<>(productAttrValueService.saveOrUpdateBatch(productAttrValueList));
    }


}
