package com.haohan.cloud.scm.api.crm.req.imp;

import com.haohan.cloud.scm.api.crm.dto.imp.CustomerLinkmanImport;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @author dy
 * @date 2019/10/12
 */
@Data
public class CustomerLinkmanImportReq {

    @NotEmpty(message = "客户联系人列表不能为空")
    private List<CustomerLinkmanImport> linkmanList;

}
