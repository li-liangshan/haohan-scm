/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.manage.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.manage.entity.UnionBankNo;
import com.haohan.cloud.scm.manage.mapper.UnionBankNoMapper;
import com.haohan.cloud.scm.manage.service.UnionBankNoService;
import org.springframework.stereotype.Service;

/**
 * 联行号
 *
 * @author haohan
 * @date 2019-05-13 17:16:56
 */
@Service
public class UnionBankNoServiceImpl extends ServiceImpl<UnionBankNoMapper, UnionBankNo> implements UnionBankNoService {

}
