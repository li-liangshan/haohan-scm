package com.haohan.cloud.scm.common.tools.http;


import org.apache.http.HeaderElement;
import org.apache.http.HeaderElementIterator;
import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.conn.ConnectionKeepAliveStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.IdleConnectionEvictor;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicHeaderElementIterator;
import org.apache.http.protocol.HTTP;
import org.apache.http.protocol.HttpContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

/**
 * Created by zgw on 2019/5/22.
 */
@Configuration
public class HttpClientConfig {

  @Value("${http_max_total}")
  private int maxTotal = 800;

  @Value("${http_default_max_perRoute}")
  private int defaultMaxPerRoute = 80;

  @Value("${http_validate_after_inactivity}")
  private int validateAfterInactivity = 1000;

  @Value("${http_connection_request_timeout}")
  private int connectionRequestTimeout = 5000;

  @Value("${http_connection_timeout}")
  private int connectTimeout = 10000;

  @Value("${http_socket_timeout}")
  private int socketTimeout = 20000;

  @Value("${waitTime}")
  private int waitTime = 30000;

  @Value("${idleConTime}")
  private int idleConTime = 3;

  @Value("${retryCount}")
  private int retryCount = 3;

  @Bean
  public PoolingHttpClientConnectionManager createPoolingHttpClientConnectionManager() {
    PoolingHttpClientConnectionManager poolmanager = new PoolingHttpClientConnectionManager();
    poolmanager.setMaxTotal(maxTotal);
    poolmanager.setDefaultMaxPerRoute(defaultMaxPerRoute);
    poolmanager.setValidateAfterInactivity(validateAfterInactivity);
    return poolmanager;
  }

  @Bean
  public CloseableHttpClient createHttpClient(PoolingHttpClientConnectionManager poolManager) {
    HttpClientBuilder httpClientBuilder = HttpClientBuilder.create().setConnectionManager(poolManager);
    httpClientBuilder.setKeepAliveStrategy(new ConnectionKeepAliveStrategy() {

      @Override
      public long getKeepAliveDuration(HttpResponse response, HttpContext context) {
        HeaderElementIterator iterator = new BasicHeaderElementIterator(response.headerIterator(HTTP.CONN_KEEP_ALIVE));
        while (iterator.hasNext()) {
          HeaderElement headerElement = iterator.nextElement();
          String param = headerElement.getName();
          String value = headerElement.getValue();
          if (null != value && param.equalsIgnoreCase("timeout")) {
            return Long.parseLong(value) * 1000;
          }
        }
        return 30 * 1000;
      }
    });
    httpClientBuilder.setRetryHandler(new DefaultHttpRequestRetryHandler(retryCount, false));
    return httpClientBuilder.build();
  }
  @Bean
  public RequestConfig createRequestConfig() {
    return RequestConfig.custom()
      .setConnectionRequestTimeout(connectionRequestTimeout)     // 从连接池中取连接的超时时间
      .setConnectTimeout(connectTimeout)                        // 连接超时时间
      .setSocketTimeout(socketTimeout)                        // 请求超时时间
      .build();
  }

  @Bean
  public IdleConnectionEvictor createIdleConnectionEvictor(PoolingHttpClientConnectionManager poolManager) {
    IdleConnectionEvictor idleConnectionEvictor = new IdleConnectionEvictor(poolManager, waitTime, TimeUnit.SECONDS);
    return idleConnectionEvictor;
  }

}
