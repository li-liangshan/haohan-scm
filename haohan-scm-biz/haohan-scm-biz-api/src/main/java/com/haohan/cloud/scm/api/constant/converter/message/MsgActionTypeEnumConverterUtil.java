package com.haohan.cloud.scm.api.constant.converter.message;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.message.MsgActionTypeEnum;

/**
 * @author dy
 * @date 2020/1/13
 */
public class MsgActionTypeEnumConverterUtil implements Converter<MsgActionTypeEnum> {
    @Override
    public MsgActionTypeEnum convert(Object o, MsgActionTypeEnum messageTypeEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return MsgActionTypeEnum.getByType(o.toString());
    }
}
