package com.haohan.cloud.scm.goods.core;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.goods.dto.GoodsModelDTO;
import com.haohan.cloud.scm.api.goods.entity.GoodsModel;
import com.haohan.cloud.scm.api.goods.req.manage.FetchModelListReq;
import com.haohan.cloud.scm.api.goods.req.manage.GoodsPricingReq;
import com.haohan.cloud.scm.api.goods.vo.GoodsModelVO;
import com.haohan.cloud.scm.api.goods.vo.GoodsVO;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author dy
 * @date 2019/9/13
 */
public interface GoodsModelCoreService {

    /**
     * 名称查询, 若无对应商品则新增 商品(下架状态, 分类为  未确认商品-> 导入商品)
     *
     * @param goodsModelDTO goodsName / modelName /unit /price / shopId
     * @return
     */
    GoodsModelDTO fetchModelByNameOrAdd(GoodsModelDTO goodsModelDTO);

    /**
     * 查询商品规格信息  根据商品名称 规格名称 规格单位
     * 优先 规格名称匹配 无时 匹配 规格单位
     * 都无 或多条匹配时 返回null
     *
     * @param goodsModelDTO 必须shopId goodsName
     * @return
     */
    GoodsModelDTO fetchGoodsModelByName(GoodsModelDTO goodsModelDTO);

    /**
     * 根据规格id查询商品信息
     *
     * @param goodsModelId
     * @param req          pricingDate、  buyerId/pricingMerchantId或buyerId
     * @return
     */
    GoodsVO fetchGoodsInfoByModelId(String goodsModelId, GoodsPricingReq req);

    /**
     * 根据规格id查询商品信息
     * 无typeName
     *
     * @param goodsModelId
     * @param req          pricingDate、  buyerId/pricingMerchantId或buyerId
     * @return
     */
    GoodsModelVO fetchModelVO(String goodsModelId, GoodsPricingReq req);
    /**
     * 查询商品规格列表
     *
     * @param page
     * @param req
     * @return
     */
    IPage<GoodsModelDTO> fetchModelPage(Page<GoodsModelDTO> page, FetchModelListReq req);

    /**
     * 查询商品规格列表 (带类型名称)
     *
     * @param page
     * @param req
     * @return
     */
    IPage<GoodsModelVO> fetchModelInfoPage(Page page, FetchModelListReq req);

    /**
     * 规格库存扣减
     *
     * @param modelList
     * @return
     */
    boolean modelStorageSubtractBatch(List<GoodsModel> modelList);

    boolean changeStorage(String modelId, BigDecimal storage);
}
