/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.product.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.product.entity.ProductionTask;
import com.haohan.cloud.scm.api.product.req.ProductionTaskReq;
import com.haohan.cloud.scm.product.service.ProductionTaskService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 生产任务表
 *
 * @author haohan
 * @date 2019-05-29 14:45:27
 */
@RestController
@AllArgsConstructor
@RequestMapping("/productiontask" )
@Api(value = "productiontask", tags = "productiontask管理")
public class ProductionTaskController {

    private final ProductionTaskService productionTaskService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param productionTask 生产任务表
     * @return
     */
    @GetMapping("/page" )
    public R getProductionTaskPage(Page page, ProductionTask productionTask) {
        return new R<>(productionTaskService.page(page, Wrappers.query(productionTask)));
    }


    /**
     * 通过id查询生产任务表
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(productionTaskService.getById(id));
    }

    /**
     * 新增生产任务表
     * @param productionTask 生产任务表
     * @return R
     */
    @SysLog("新增生产任务表" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_productiontask_add')" )
    public R save(@RequestBody ProductionTask productionTask) {
        return new R<>(productionTaskService.save(productionTask));
    }

    /**
     * 修改生产任务表
     * @param productionTask 生产任务表
     * @return R
     */
    @SysLog("修改生产任务表" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_productiontask_edit')" )
    public R updateById(@RequestBody ProductionTask productionTask) {
        return new R<>(productionTaskService.updateById(productionTask));
    }

    /**
     * 通过id删除生产任务表
     * @param id id
     * @return R
     */
    @SysLog("删除生产任务表" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_productiontask_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(productionTaskService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除生产任务表")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_productiontask_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(productionTaskService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询生产任务表")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(productionTaskService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param productionTaskReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询生产任务表总记录}")
    @PostMapping("/countByProductionTaskReq")
    public R countByProductionTaskReq(@RequestBody ProductionTaskReq productionTaskReq) {

        return new R<>(productionTaskService.count(Wrappers.query(productionTaskReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param productionTaskReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据productionTaskReq查询一条货位信息表")
    @PostMapping("/getOneByProductionTaskReq")
    public R getOneByProductionTaskReq(@RequestBody ProductionTaskReq productionTaskReq) {

        return new R<>(productionTaskService.getOne(Wrappers.query(productionTaskReq), false));
    }


    /**
     * 批量修改OR插入
     * @param productionTaskList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_productiontask_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<ProductionTask> productionTaskList) {

        return new R<>(productionTaskService.saveOrUpdateBatch(productionTaskList));
    }


}
