package com.haohan.cloud.scm.wecaht.wxapp.api.purchase;

import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: haohan-fresh-scm
 * @description: 商品采购API
 * @author: Simon
 * @create: 2019-07-18
 **/
@RestController
@AllArgsConstructor
@RequestMapping("/api/wechat/wxapp/messageManage")
@Api(value = "MessageManageApiCtrl", tags = "消息管理接口服务")
public class MessageManageApiCtrl {



}
