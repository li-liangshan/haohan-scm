package com.haohan.cloud.scm.api.goods.dto.imp;

import com.haohan.cloud.scm.api.constant.enums.goods.GoodsFromTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.goods.GoodsStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.goods.GoodsTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import com.haohan.cloud.scm.api.goods.dto.GoodsDTO;
import com.haohan.cloud.scm.api.goods.dto.GoodsModelDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author dy
 * @date 2019/10/16
 */
@Data
public class GoodsImport {

    @NotBlank(message = "商品名称不能为空")
    @Length(min = 0, max = 32, message = "商品名称的字符长度必须在1至32之间")
    @ApiModelProperty(value = "商品名称")
    private String goodsName;

    @NotBlank(message = "分类名称不能为空")
    @Length(min = 0, max = 32, message = "分类名称的字符长度必须在1至32之间")
    @ApiModelProperty(value = "分类名称")
    private String categoryName;

    @Length(min = 0, max = 65535, message = "商品描述的字符长度必须在0至65535之间")
    @ApiModelProperty(value = "商品描述")
    private String detailDesc;

    @Length(min = 0, max = 255, message = "图片地址的字符长度必须在0至255之间")
    @ApiModelProperty(value = "图片地址")
    private String thumbUrl;

    @Length(min = 0, max = 32, message = "行业名称的字符长度必须在0至32之间")
    @ApiModelProperty(value = "行业名称")
    private String industry;

    @Length(min = 0, max = 32, message = "品牌名称的字符长度必须在0至32之间")
    @ApiModelProperty(value = "品牌名称")
    private String brand;

    @Length(min = 0, max = 32, message = "厂家制造商的字符长度必须在0至32之间")
    @ApiModelProperty(value = "厂家制造商")
    private String manufacturer;

    @NotBlank(message = "规格属性信息不能为空")
    @Length(min = 0, max = 500, message = "规格属性信息的字符长度必须在1至500之间")
    @ApiModelProperty(value = "规格属性信息")
    private String modelInfo;

    @NotNull(message = "售价不能为空")
    @Digits(integer = 8, fraction = 2, message = "售价的整数位最大8位, 小数位最大2位")
    @Range(min = 0, max = 1000000, message = "售价的大小必须在1至1000000之间")
    @ApiModelProperty(value = "售价")
    private BigDecimal marketPrice;

    @NotBlank(message = "单位不能为空")
    @Length(min = 0, max = 10, message = "单位的字符长度必须在1至10之间")
    @ApiModelProperty(value = "单位")
    private String unit;

    @Digits(integer = 8, fraction = 2, message = "库存的整数位最大8位, 小数位最大2位")
    @ApiModelProperty(value = "库存")
    private BigDecimal storage;

    @Length(min = 0, max = 32, message = "扫码条码的字符长度必须在0至32之间")
    @ApiModelProperty(value = "扫码条码")
    private String scanCode;

    @Length(min = 0, max = 255, message = "规格图片地址的字符长度必须在0至255之间")
    @ApiModelProperty(value = "规格图片地址")
    private String modelUrl;

    @Digits(integer = 7, fraction = 3, message = "重量的整数位最大7位, 小数位最大3位")
    @Range(min = 0, max = 1000000, message = "重量的大小必须在1至1000000之间")
    @ApiModelProperty(value = "重量")
    private BigDecimal weight;

    @Digits(integer = 4, fraction = 6, message = "体积的整数位最大4位, 小数位最大6位")
    @Range(min = 0, max = 10000, message = "体积的大小必须在1至10000之间")
    @ApiModelProperty(value = "体积")
    private BigDecimal volume;

    @Length(min = 0, max = 64, message = "备注信息的字符长度必须在0至64之间")
    @ApiModelProperty(value = "备注信息")
    private String remarks;

    public GoodsModelDTO initGoodsModel() {
        GoodsModelDTO model = new GoodsModelDTO();
        model.setModelInfo(this.modelInfo);
        model.setModelPrice(this.marketPrice);
        model.setModelUnit(this.unit);
        model.setModelStorage(this.storage);
        model.setModelCode(this.scanCode);
        model.setModelUrl(this.modelUrl);
        model.setWeight(this.weight);
        model.setVolume(this.volume);
        model.setRemarks(this.remarks);
        return model;

    }

    public GoodsDTO initGoods() {
        GoodsDTO goods = new GoodsDTO();
        goods.setGoodsName(this.goodsName);
        goods.setCategoryName(this.categoryName);
        goods.setDetailDesc(this.detailDesc);
        goods.setThumbUrl(this.thumbUrl);
        goods.setIndustry(this.industry);
        goods.setBrand(this.brand);
        goods.setManufacturer(this.manufacturer);

        goods.setSort("100");
        goods.setScanCode(this.scanCode);
        goods.setStorage(this.storage);
        goods.setMarketPrice(this.marketPrice);
        goods.setUnit(this.unit);

        // 状态属性
        goods.setIsMarketable(YesNoEnum.no);
        goods.setSaleRule(YesNoEnum.no);
        goods.setServiceSelection(YesNoEnum.no);
        goods.setDeliveryRule(YesNoEnum.no);
        goods.setGoodsGift(YesNoEnum.no);
        goods.setGoodsModel(YesNoEnum.no);
        goods.setGoodsStatus(GoodsStatusEnum.stock);
        goods.setGoodsFrom(GoodsFromTypeEnum.platform);
        goods.setGoodsType(GoodsTypeEnum.physical);
        goods.setSalecFlag(YesNoEnum.no);

        goods.setAttrMap(new HashMap<>(8));
        goods.setModelList(new ArrayList<>());
        return goods;
    }
}
