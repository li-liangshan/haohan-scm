package com.haohan.cloud.scm.api.product.trans;

import com.haohan.cloud.scm.api.constant.enums.product.ShipOrderStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.product.SortingStatusEnum;
import com.haohan.cloud.scm.api.product.entity.ShipOrder;
import com.haohan.cloud.scm.api.product.entity.SortingOrder;
import com.haohan.cloud.scm.api.saleb.entity.BuyOrder;


/**
 * @author cx
 * @date 2019/7/6
 */
public class SortingOrderTrans {
    public static SortingOrder createSortingOrder(BuyOrder order){
        SortingOrder sortingOrder = new SortingOrder();
        sortingOrder.setPmId(order.getPmId());
        sortingOrder.setBuyId(order.getBuyId());
        sortingOrder.setBuyerId(order.getBuyerId());
        sortingOrder.setBuyerName(order.getBuyerName());
        sortingOrder.setSortingStatus(SortingStatusEnum.wait);
        sortingOrder.setDeliveryDate(order.getDeliveryTime());
        sortingOrder.setDeliverySeq(order.getBuySeq());
        return sortingOrder;
    }

    public static ShipOrder createShipOrder(SortingOrder order) {
        ShipOrder shipOrder = new ShipOrder();
        shipOrder.setPmId(order.getPmId());
        shipOrder.setBuyerId(order.getBuyerId());
        shipOrder.setBuyId(order.getBuyId());
        shipOrder.setStatus(ShipOrderStatusEnum.wait);
        return shipOrder;
    }
}
