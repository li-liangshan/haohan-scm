/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.haohan.cloud.scm.api.wms.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.haohan.cloud.scm.api.constant.enums.opc.BuySeqEnum;
import com.haohan.cloud.scm.api.constant.enums.product.ExitStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.product.ExitTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * 出库单明细
 *
 * @author haohan
 * @date 2019-05-13 21:29:03
 */
@Data
@TableName("scm_pws_exit_warehouse_detail")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "出库单明细")
public class ExitWarehouseDetail extends Model<ExitWarehouseDetail> {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 平台商家id
     */
    private String pmId;
    /**
     * 出库单编号
     */
    private String exitWarehouseSn;
    /**
     * 出库单明细编号
     */
    private String exitWarehouseDetailSn;
    /**
     * 出库申请时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime applyTime;
    /**
     * 仓库编号
     */
    private String warehouseSn;
    /**
     * 出库货品编号
     */
    private String productSn;
    /**
     * 出库货品数量
     */
    private BigDecimal productNumber;
    /**
     * 货品单位
     */
    private String unit;
    /**
     * 出库状态:1待出货2已接收3未接收4部分接收
     */
    private ExitStatusEnum exitStatus;
    /**
     * 出库单类型:1.配送2.加工
     */
    private ExitTypeEnum exitType;
    /**
     * 生产任务编号
     */
    private String productionTaskSn;
    /**
     * 汇总单编号
     */
    private String summaryOrderId;
    /**
     * 送货时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate deliveryDate;
    /**
     * 送货批次:0第一批1第二批
     */
    private BuySeqEnum deliverySeq;
    /**
     * 暂存点编号(目标放置点)
     */
    private String storagePlaceSn;
    /**
     * 暂存点名称
     */
    private String storagePlaceName;
    /**
     * 验收人
     */
    private String auditorId;
    /**
     * 验收人名称
     */
    private String auditorName;
    /**
     * 验收时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime auditTime;

    /**
     * 客户订单明细
     */
    private String buyOrderDetailSn;

    /**
     * 出库商品规格id
     */
    private String goodsModelId;

    /**
     * 创建者
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
     * 更新者
     */
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateDate;
    /**
     * 备注信息
     */
    private String remarks;
    /**
     * 删除标记
     */
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;
    /**
     * 租户id
     */
    private Integer tenantId;

}
