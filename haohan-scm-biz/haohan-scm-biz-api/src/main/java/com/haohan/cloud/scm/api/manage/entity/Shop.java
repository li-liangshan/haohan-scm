/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.haohan.cloud.scm.api.manage.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.haohan.cloud.scm.api.constant.enums.manage.MerchantStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.manage.ShopLevelEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

/**
 * 店铺
 *
 * @author haohan
 * @date 2019-05-13 17:39:04
 */
@Data
@TableName("scm_shop")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "店铺")
public class Shop extends Model<Shop> {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 店铺编号  未使用(原系统使用,和即速应用关联)
     */
    private String code;
    /**
     * 店铺名称
     */
    private String name;
    /**
     * 店铺区域  暂未使用
     */
    private String areaId;
    /**
     * 店铺地址
     */
    private String address;
    /**
     * 店铺负责人
     */
    private String manager;
    /**
     * 店铺电话
     */
    private String telephone;
    /**
     * 经度
     */
    private String mapLongitude;
    /**
     * 纬度
     */
    private String mapLatitude;
    /**
     * 商家ID
     */
    private String merchantId;
    /**
     * 营业时间 描述
     */
    private String onlineTime;
    /**
     * 店铺服务  (店铺经营范围,仅展示)
     */
    private String shopService;
    /**
     * 店铺模板ID 暂未使用(原系统使用)
     */
    private String templateId;
    /**
     * 图片组编号  轮播图
     */
    private String photoGroupNum;
    /**
     * 店铺介绍
     */
    private String shopDesc;
    /**
     * 启用状态  原系统   0待审核2启用-1停用
     */
    private MerchantStatusEnum status;
    /**
     * 店铺收款码  保存图片组编号
     */
    private String payCode;
    /**
     * 店铺二维码  保存图片组编号
     */
    private String qrcode;
    /**
     * 店铺Logo  保存图片组编号
     */
    private String shopLogo;
    /**
     * 店铺位置  暂未使用(原系统描述店铺省市区及定位json)
     */
    private String shopLocation;
    /**
     * 店铺类型  暂未使用(原系统  0 餐饮, 1零售  终端使用)
     */
    private String shopType;
    /**
     * 配送距离 暂未使用(原系统 配送员功能相关)
     */
    private String deliverDistence;
    /**
     * 店铺等级   (原系统  0 总店, 1 分店 2采购配送店) 目前使用采购配送店
     */
    private ShopLevelEnum shopLevel;
    /**
     * 是否更新即速商品  暂未使用(原系统同步标志)
     */
    private String isUpdateJisu;
    /**
     * 店铺服务模式  暂未使用(原系统 字典shop_service_type: 1 云小店 2 云连锁总店  3 云连锁分店)
     */
    private String serviceType;
    /**
     * 行业名称  仅展示
     */
    private String industry;
    /**
     * 店铺分类id   暂未使用(原系统用于聚合平台)
     */
    private String shopCategory;
    /**
     * 认证类型    暂未使用(原系统 字典shop_auth_type   0未认证 1已认证)
     */
    private String authType;
    /**
     * 聚合平台类型  暂未使用(原系统 字典aggregation_shop_type  0非聚合  1农贸城)
     */
    private String aggregationType;
    /**
     * 交易类型  暂未使用(原系统 字典shop_trade_type  1当面交易  2在线下单  3送货上门)
     */
    private String tradeType;
    /**
     * 创建者
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
     * 更新者
     */
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateDate;
    /**
     * 备注信息
     */
    private String remarks;
    /**
     * 删除标记
     */
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;
    /**
     * 租户id
     */
    private Integer tenantId;

}
