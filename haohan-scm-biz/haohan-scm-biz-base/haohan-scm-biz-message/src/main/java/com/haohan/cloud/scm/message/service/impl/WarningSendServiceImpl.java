/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.message.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.message.entity.WarningSend;
import com.haohan.cloud.scm.message.mapper.WarningSendMapper;
import com.haohan.cloud.scm.message.service.WarningSendService;
import org.springframework.stereotype.Service;

/**
 * 预警发送记录表
 *
 * @author haohan
 * @date 2019-05-13 18:23:45
 */
@Service
public class WarningSendServiceImpl extends ServiceImpl<WarningSendMapper, WarningSend> implements WarningSendService {

}
