package com.haohan.cloud.scm.api.crm.req.customer;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @author dy
 * @date 2020/4/29
 */
@Data
public class CustomerMerchantBatchReq {

    @NotEmpty(message = "客户编号列表不能为空")
    @ApiModelProperty("客户编号列表")
    private List<String> customerSnList;
}
