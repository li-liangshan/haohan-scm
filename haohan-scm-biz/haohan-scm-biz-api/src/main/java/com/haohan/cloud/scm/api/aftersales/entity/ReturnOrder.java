/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.aftersales.entity;
import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.haohan.cloud.scm.api.constant.enums.aftersales.ReturnStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.aftersales.ReturnTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDateTime;



/**
 * 退货单
 *
 * @author haohan
 * @date 2019-07-30 16:09:37
 */
@Data
@TableName("scm_ass_return_order")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "退货单")
public class ReturnOrder extends Model<ReturnOrder> {
    private static final long serialVersionUID=1L;

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 商家ID
     */
    @ApiModelProperty(value = "商家ID")
    private String pmId;
    /**
     * 退货单编号
     */
    @ApiModelProperty(value = "退货单编号")
    private String returnSn;
    /**
     * 采购单编号
     */
    @ApiModelProperty(value = "采购单编号")
    private String orderSn;
    /**
     * 退货类型0:B客户1:采购部
     */
    @ApiModelProperty(value = "退货类型0:B客户1:采购部")
    private ReturnTypeEnum returnType;

    /**
     * 退货人
     */
    @ApiModelProperty(value = "退货人")
    private String returnCustomer;
    /**
     * 退货用户ID  (对应buyerId / supplierId)
     */
    @ApiModelProperty(value = "退货用户ID")
    private String returnCustomerId;
    /**
     * 账单编号
     */
    @ApiModelProperty(value = "账单编号")
    private String paymentSn;
    /**
     * 商品数量
     */
    @ApiModelProperty(value = "商品数量")
    private BigDecimal goodsNum;
    /**
     * 总计金额
     */
    @ApiModelProperty(value = "总计金额")
    private BigDecimal totalAmount;
    /**
     * 合计金额
     */
    @ApiModelProperty(value = "合计金额")
    private BigDecimal sumAmount;
    /**
     * 其他费用
     */
    @ApiModelProperty(value = "其他费用")
    private BigDecimal otherAmount;
    /**
     * 操作员ID
     */
    @ApiModelProperty(value = "操作员ID")
    private String operatorId;

    /**
     * 退货申请时间
     */
    @ApiModelProperty(value = "申请时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime applyTime;

    /**
     * 完成时间
     */
    @ApiModelProperty(value = "完成时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime finishTime;
    /**
     * 退货状态1:待审核2:审核不通过3:审核通过4:退货中5:已退货6:已完成
     */
    @ApiModelProperty(value = "退货状态1:待审核2:审核不通过3:审核通过4:退货中5:已退货6:已完成")
    private ReturnStatusEnum returnStatus;
    /**
     * 退货原因
     */
    @ApiModelProperty(value = "退货原因")
    private String returnReason;
    /**
     * 退货图片组编号
     */
    @ApiModelProperty(value = "退货图片组编号")
    private String photoGroupNum;
    /**
     * 创建者
     */
    @ApiModelProperty(value = "创建者")
    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
     * 更新者
     */
    @ApiModelProperty(value = "更新者")
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;
    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.UPDATE)
    @ApiModelProperty(value = "更新时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateDate;
    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String remarks;
    /**
     * 删除标记
     */
    @TableLogic
    @ApiModelProperty(value = "删除标记")
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;
    /**
     * 租户id
     */
    @ApiModelProperty(value = "租户id")
    private Integer tenantId;

}
