/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.salec.feign;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.salec.entity.StoreCategory;
import com.haohan.cloud.scm.api.salec.req.StoreCategoryReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 商品分类表内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "StoreCategoryFeignService", value = ScmServiceName.SCM_BIZ_SALEC)
public interface StoreCategoryFeignService {


  /**
   * 通过id查询商品分类表
   *
   * @param id id
   * @return R
   */
  @RequestMapping(value = "/api/feign/StoreCategory/{id}", method = RequestMethod.GET)
  R<StoreCategory> getById(@PathVariable("id") Integer id, @RequestHeader(SecurityConstants.FROM) String from);


  /**
   * 分页查询 商品分类表 列表信息
   *
   * @param storeCategoryReq 请求对象
   * @return
   */
  @PostMapping("/api/feign/StoreCategory/fetchStoreCategoryPage")
  R<Page<StoreCategory>> getStoreCategoryPage(@RequestBody StoreCategoryReq storeCategoryReq, @RequestHeader(SecurityConstants.FROM) String from);


  /**
   * 全量查询 商品分类表 列表信息
   *
   * @param storeCategoryReq 请求对象
   * @return
   */
  @PostMapping("/api/feign/StoreCategory/fetchStoreCategoryList")
  R<List<StoreCategory>> getStoreCategoryList(@RequestBody StoreCategoryReq storeCategoryReq, @RequestHeader(SecurityConstants.FROM) String from);

  /**
   * 新增商品分类表
   *
   * @param storeCategory 商品分类表
   * @return R
   */
  @PostMapping("/api/feign/StoreCategory/add")
  R<Boolean> save(@RequestBody StoreCategory storeCategory, @RequestHeader(SecurityConstants.FROM) String from);


  /**
   * 修改商品分类表
   *
   * @param storeCategory 商品分类表
   * @return R
   */
  @PostMapping("/api/feign/StoreCategory/update")
  R<Boolean> updateById(@RequestBody StoreCategory storeCategory, @RequestHeader(SecurityConstants.FROM) String from);


  /**
   * 通过id删除商品分类表
   *
   * @param id id
   * @return R
   */
  @PostMapping("/api/feign/StoreCategory/delete/{id}")
  R<Boolean> removeById(@PathVariable("id") Integer id, @RequestHeader(SecurityConstants.FROM) String from);

  /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
  @PostMapping("/api/feign/StoreCategory/batchDelete")
  R<Boolean> removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


  /**
   * 批量查询（根据IDS）
   *
   * @param idList 主键ID列表
   * @return R
   */
  @PostMapping("/api/feign/StoreCategory/listByIds")
  R<List<StoreCategory>> listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


  /**
   * 根据 Wrapper 条件，查询总记录数
   *
   * @param storeCategoryReq 实体对象,可以为空
   * @return R
   */
  @PostMapping("/api/feign/StoreCategory/countByStoreCategoryReq")
  R<Integer> countByStoreCategoryReq(@RequestBody StoreCategoryReq storeCategoryReq, @RequestHeader(SecurityConstants.FROM) String from);


  /**
   * 根据对象条件，查询一条记录
   *
   * @param storeCategoryReq 实体对象,可以为空
   * @return R
   */
  @PostMapping("/api/feign/StoreCategory/getOneByStoreCategoryReq")
  R<StoreCategory> getOneByStoreCategoryReq(@RequestBody StoreCategoryReq storeCategoryReq, @RequestHeader(SecurityConstants.FROM) String from);


  /**
   * 批量修改OR插入
   *
   * @param storeCategoryList 实体对象集合 大小不超过1000条数据
   * @return R
   */
  @PostMapping("/api/feign/StoreCategory/saveOrUpdateBatch")
  R<Boolean> saveOrUpdateBatch(@RequestBody List<StoreCategory> storeCategoryList, @RequestHeader(SecurityConstants.FROM) String from);


}
