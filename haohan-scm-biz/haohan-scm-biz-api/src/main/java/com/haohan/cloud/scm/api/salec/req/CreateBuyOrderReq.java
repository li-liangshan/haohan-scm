package com.haohan.cloud.scm.api.salec.req;

import com.haohan.cloud.scm.api.constant.enums.common.DeliveryTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.BuySeqEnum;
import com.haohan.cloud.scm.api.salec.entity.StoreOrder;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;

/**
 * @author cx
 * @date 2019/7/13
 */
@Data
public class CreateBuyOrderReq {

    /**
     * c端订单列表
     */
    @NotNull(message = "订单不能为空")
    private List<StoreOrder> orderList;

    /**
     * 送货批次
     */
    @NotNull(message = "送货批次不能为空")
    private BuySeqEnum buySeq;

    /**
     * 送货时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @NotNull(message = "送货时间不能为空")
    private LocalDate deliveryTime;

    @NotNull(message = "配送类型")
    private DeliveryTypeEnum deliveryType;
}
