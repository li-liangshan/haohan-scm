package com.haohan.cloud.scm.common.tools.exception;

/**
 * 数据为空时抛出
 *
 * @author dy
 * @date 2019/5/17
 */
public class EmptyDataException extends RuntimeException {

    private static final long serialVersionUID = -5429638369649398442L;

    public EmptyDataException() {
    }

    public EmptyDataException(String msg) {
        super(msg);
    }

    public EmptyDataException(Throwable cause) {
        super(cause);
    }

    public EmptyDataException(String message, Throwable cause) {
        super(message, cause);
    }

    public EmptyDataException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
