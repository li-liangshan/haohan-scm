/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.purchase.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseTask;

/**
 * 采购任务
 *
 * @author haohan
 * @date 2019-05-13 18:52:46
 */
public interface PurchaseTaskMapper extends BaseMapper<PurchaseTask> {

}
