package com.haohan.cloud.scm.api.crm.vo.app;

import cn.hutool.core.bean.BeanUtil;
import com.haohan.cloud.scm.api.bill.dto.BillInfoDTO;
import com.haohan.cloud.scm.api.constant.enums.common.ReviewStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import com.haohan.cloud.scm.api.constant.enums.saleb.BillTypeEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * @author dy
 * @date 2020/1/18
 */
@Data
public class CrmBillVO {

    /**
     * 账单编号 (应收、应付)
     */
    private String billSn;
    /**
     * 账单审核状态: 1.待审核2.审核不通过3.审核通过
     */
    private ReviewStatusEnum reviewStatus;
    /**
     * 账单类型: 1订单应收 2退款应收 3采购应付 4退款应付
     */
    private BillTypeEnum billType;
    /**
     * 结算单编号
     */
    private String settlementSn;
    /**
     * 结算状态 是否支付 0否1是
     */
    private YesNoEnum settlementStatus;


    // 订单相关

    /**
     * 平台商家ID
     */
    private String pmId;
    /**
     * 平台商家名称
     */
    private String pmName;
    /**
     * 来源订单编号
     */
    private String orderSn;

    @ApiModelProperty(value = "下单客户sn")
    private String customerSn;

    @ApiModelProperty(value = "下单客户名称")
    private String customerName;

    @ApiModelProperty(value = "客户商家id")
    private String merchantId;

    @ApiModelProperty(value = "客户商家名称")
    private String merchantName;

    @ApiModelProperty(value = "订单成交日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate dealDate;

    private String employeeName;

    private Integer goodsNum;
    // 金额相关

    @ApiModelProperty(value = "预付标志", notes = "是否预付账单")
    private YesNoEnum advanceFlag;

    @ApiModelProperty(value = "预付金额")
    private BigDecimal advanceAmount;

    @ApiModelProperty(value = "账单金额", notes = "用于结算的金额")
    private BigDecimal billAmount;

    @ApiModelProperty(value = "订单金额", notes = "对应订单总金额")
    private BigDecimal orderAmount;

    @ApiModelProperty(value = "备注信息")
    protected String remarks;



    public CrmBillVO(BillInfoDTO bill) {
        BeanUtil.copyProperties(bill, this);
    }
}
