/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.purchase.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.opc.entity.SummaryOrder;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseOrderDetail;
import com.haohan.cloud.scm.api.purchase.req.ChoiceSupplierReq;
import com.haohan.cloud.scm.api.purchase.req.CompleteTaskReq;
import com.haohan.cloud.scm.api.purchase.req.PurchaseOrderDetailReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 采购部采购单明细内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "PurchaseOrderDetailFeignService", value = ScmServiceName.SCM_BIZ_PURCHASE)
public interface PurchaseOrderDetailFeignService {


    /**
     * 通过id查询采购部采购单明细
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/PurchaseOrderDetail/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 采购部采购单明细 列表信息
     * @param purchaseOrderDetailReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/PurchaseOrderDetail/fetchPurchaseOrderDetailPage")
    R getPurchaseOrderDetailPage(@RequestBody PurchaseOrderDetailReq purchaseOrderDetailReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 采购部采购单明细 列表信息
     * @param purchaseOrderDetailReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/PurchaseOrderDetail/fetchPurchaseOrderDetailList")
    R getPurchaseOrderDetailList(@RequestBody PurchaseOrderDetailReq purchaseOrderDetailReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增采购部采购单明细
     * @param purchaseOrderDetail 采购部采购单明细
     * @return R
     */
    @PostMapping("/api/feign/PurchaseOrderDetail/add")
    R save(@RequestBody PurchaseOrderDetail purchaseOrderDetail, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改采购部采购单明细
     * @param purchaseOrderDetail 采购部采购单明细
     * @return R
     */
    @PostMapping("/api/feign/PurchaseOrderDetail/update")
    R updateById(@RequestBody PurchaseOrderDetail purchaseOrderDetail, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除采购部采购单明细
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/PurchaseOrderDetail/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/PurchaseOrderDetail/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/PurchaseOrderDetail/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param purchaseOrderDetailReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/PurchaseOrderDetail/countByPurchaseOrderDetailReq")
    R countByPurchaseOrderDetailReq(@RequestBody PurchaseOrderDetailReq purchaseOrderDetailReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param purchaseOrderDetailReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/PurchaseOrderDetail/getOneByPurchaseOrderDetailReq")
    R getOneByPurchaseOrderDetailReq(@RequestBody PurchaseOrderDetailReq purchaseOrderDetailReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param purchaseOrderDetailList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/PurchaseOrderDetail/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<PurchaseOrderDetail> purchaseOrderDetailList, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 通过id查询采购部采购单明细
     * @param req 采购单明细编号
     * @return R
     */
    @PostMapping( "/api/feign/PurchaseOrderDetail/getOne")
    R getOne(@RequestBody ChoiceSupplierReq req, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 根据汇总单生成采购明细
     * @param summaryOrder
     * @param from
     * @return
     */
    @PostMapping("/api/feign/PurchaseOrderDetail/addPurchaseOrderDetail")
    R addPurchaseOrderDetail(@RequestBody SummaryOrder summaryOrder,@RequestHeader(SecurityConstants.FROM)String from);

    /**
     * 确认揽活
     * @param req
     * @param from
     * @return
     */
    @PostMapping("/api/feign/PurchaseOrderDetail/deliveryTask")
    R deliveryTask(@RequestBody CompleteTaskReq req,@RequestHeader(SecurityConstants.FROM)String from);
}
