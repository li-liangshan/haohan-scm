package com.haohan.cloud.scm.api.common.req.admin;


import javax.validation.constraints.NotBlank;

/**
 * @author shenyu
 * @create 2018/12/14
 */
public class PdsBaseApiReq {
    @NotBlank(message = "missing param pmId")
    private String pmId;
    private Integer pageNo;
    private Integer pageSize;

    public String getPmId() {
        return pmId;
    }

    public void setPmId(String pmId) {
        this.pmId = pmId;
    }

    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
}
