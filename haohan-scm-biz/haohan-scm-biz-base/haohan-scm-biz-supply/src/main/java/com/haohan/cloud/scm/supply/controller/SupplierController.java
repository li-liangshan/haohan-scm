/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.supply.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.supply.entity.Supplier;
import com.haohan.cloud.scm.api.supply.req.supplier.SupplierEditReq;
import com.haohan.cloud.scm.api.supply.req.supplier.SupplierQueryReq;
import com.haohan.cloud.scm.api.supply.vo.SupplierVO;
import com.haohan.cloud.scm.common.tools.constant.FirstGroup;
import com.haohan.cloud.scm.common.tools.constant.SecondGroup;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.haohan.cloud.scm.supply.core.ScmSupplierCoreService;
import com.haohan.cloud.scm.supply.service.SupplierService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


/**
 * 供应商
 *
 * @author haohan
 * @date 2019-05-29 13:13:47
 */
@RestController
@AllArgsConstructor
@RequestMapping("/supplier")
@Api(value = "supplier", tags = "supplier管理")
public class SupplierController {

    private final SupplierService supplierService;
    private final ScmSupplierCoreService supplierCoreService;

    /**
     * 分页查询
     *
     * @param page 分页对象
     * @param req  供应商
     * @return
     */
    @GetMapping("/page")
    public R<IPage<SupplierVO>> getSupplierPage(Page<Supplier> page, SupplierQueryReq req) {
        return RUtil.success(supplierCoreService.findPage(page, req));
    }


    /**
     * 通过id查询供应商
     *
     * @param id id
     * @return R
     */
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return RUtil.success(supplierService.getById(id));
    }

    /**
     * 新增供应商
     *
     * @param req 供应商
     * @return R
     */
    @SysLog("新增供应商")
    @PostMapping
    @PreAuthorize("@pms.hasPermission('supply_supplier_add')")
    public R<Boolean> save(@RequestBody @Validated(FirstGroup.class) SupplierEditReq req) {
        return RUtil.success(supplierCoreService.addSupplier(req));
    }

    /**
     * 修改供应商
     *
     * @param req 供应商
     * @return R
     */
    @SysLog("修改供应商")
    @PutMapping
    @PreAuthorize("@pms.hasPermission('supply_supplier_edit')")
    public R<Boolean> updateById(@RequestBody @Validated(SecondGroup.class) SupplierEditReq req) {
        return RUtil.success(supplierCoreService.modifySupplier(req));
    }

    /**
     * 通过id删除供应商
     *
     * @param id id
     * @return R
     */
    @SysLog("删除供应商")
    @DeleteMapping("/{id}")
    @PreAuthorize("@pms.hasPermission('supply_supplier_del')")
    public R<Boolean> removeById(@PathVariable String id) {
        return RUtil.success(supplierCoreService.deleteSupplier(id));
    }


}
