#!/usr/bin/env bash

app_home=$(dirname $(pwd))

git checkout master

git pull

mvn clean install


java -jar -Xmx256m -Xms256m haohan-scm-biz/haohan-scm-biz-core/target/haohan-scm-biz-core.jar > /data/docker/logs/haohan-cloud.log &

sleep 10

java -jar -Xmx256m -Xms256m haohan-scm-manage/target/haohan-scm-manage.jar > /data/docker/logs/haohan-cloud.log  &

sleep 10

java -jar -Xmx256m -Xms256m haohan-scm-bi/target/haohan-scm-bi.jar > /data/docker/logs/haohan-cloud.log  &



echo "done"

exit;


