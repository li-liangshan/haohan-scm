package com.haohan.cloud.scm.api.constant.converter.opc;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;

/**
 * @author dy
 * @date 2019/6/1
 */
public class YesNoEnumConverterUtil implements Converter<YesNoEnum> {

    @Override
    public YesNoEnum convert(Object o, YesNoEnum e) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return YesNoEnum.getByType(o.toString());
    }

}
