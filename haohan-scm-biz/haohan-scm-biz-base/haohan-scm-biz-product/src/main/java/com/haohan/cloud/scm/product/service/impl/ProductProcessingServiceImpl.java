/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.product.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.constant.NumberPrefixConstant;
import com.haohan.cloud.scm.api.product.entity.ProductProcessing;
import com.haohan.cloud.scm.common.tools.util.ScmIncrementUtil;
import com.haohan.cloud.scm.product.mapper.ProductProcessingMapper;
import com.haohan.cloud.scm.product.service.ProductProcessingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

/**
 * 货品加工记录表
 *
 * @author haohan
 * @date 2019-05-13 18:16:20
 */
@Service
public class ProductProcessingServiceImpl extends ServiceImpl<ProductProcessingMapper, ProductProcessing> implements ProductProcessingService {
    @Autowired
    @Lazy
    private ScmIncrementUtil scmIncrementUtil;

    @Override
    public boolean save(ProductProcessing entity) {
        if (StrUtil.isEmpty(entity.getProcessingSn())) {
            String sn = scmIncrementUtil.inrcSnByClass(ProductProcessing.class, NumberPrefixConstant.PRODUCT_PROCESSING_SN_PRE);
            entity.setProcessingSn(sn);
        }
        return super.save(entity);
    }

}
