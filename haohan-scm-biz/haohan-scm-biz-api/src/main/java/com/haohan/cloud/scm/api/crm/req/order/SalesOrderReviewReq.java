package com.haohan.cloud.scm.api.crm.req.order;

import com.haohan.cloud.scm.api.constant.enums.common.ReviewStatusEnum;
import com.haohan.cloud.scm.common.tools.constant.SecondGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @author dy
 * @date 2019/9/28
 * web审核： FirstGroup
 * app审核： SecondGroup
 */
@Data
@ApiModel("销售订单审核")
public class SalesOrderReviewReq {

    @NotBlank(message = "订单编号不能为空")
    @Length(min = 0, max = 32, message = "订单编号长度在0至32之间")
    private String salesOrderSn;

    @NotNull(message = "审核状态不能为空")
    private ReviewStatusEnum reviewStatus;

    @NotBlank(message = "员工id不能为空", groups = {SecondGroup.class})
    @Length(min = 0, max = 32, message = "员工id长度在0至32之间")
    private String employeeId;

    @ApiModelProperty(value = "需预付金额")
    private BigDecimal advanceAmount;

    @ApiModelProperty(value = "备注信息")
    @Length(max = 125, message = "备注信息的最大长度125字符")
    private String remarks;

}
