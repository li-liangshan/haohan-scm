package com.haohan.cloud.scm.api.constant.enums.manage;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dy
 * @date 2019/6/10
 */
@Getter
@AllArgsConstructor
public enum PdsUpassportStatusEnum implements IBaseEnum {
    /**
     * 通行证状态: 0 ：冻结  1：正常  2 待审核 3 小白用户
     */
    disable("0", "冻结"),
    enable("1", "正常"),
    wait("2", "待审核"),
    fresh("3", "小白用户");

    private static final Map<String, PdsUpassportStatusEnum> MAP = new HashMap<>(8);

    static {
        for (PdsUpassportStatusEnum e : PdsUpassportStatusEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static PdsUpassportStatusEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

    @EnumValue
    @JsonValue
    private String type;
    private String desc;

}
