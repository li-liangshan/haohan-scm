package com.haohan.cloud.scm.api.bill.dto;

import com.haohan.cloud.scm.api.bill.entity.BaseBill;
import com.haohan.cloud.scm.api.constant.enums.common.ReviewStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import com.haohan.cloud.scm.api.constant.enums.saleb.BillTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * @author dy
 * @date 2019/11/26
 */
@Data
@ApiModel("账单信息")
@NoArgsConstructor
public class BillInfoDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private String id;
    /**
     * 账单编号 (应收、应付)
     */
    private String billSn;
    /**
     * 账单审核状态: 1.待审核2.审核不通过3.审核通过
     */
    private ReviewStatusEnum reviewStatus;
    /**
     * 账单类型: 1订单应收 2退款应收 3采购应付 4退款应付
     */
    private BillTypeEnum billType;
    /**
     * 结算单编号
     */
    private String settlementSn;
    /**
     * 结算状态 是否支付 0否1是
     */
    private YesNoEnum settlementStatus;

    // 订单相关
    /**
     * 平台商家ID
     */
    private String pmId;
    /**
     * 平台商家名称
     */
    private String pmName;
    /**
     * 来源订单编号
     */
    private String orderSn;

    @ApiModelProperty(value = "下单客户id", notes = "采购商id、供应商id、客户sn")
    private String customerId;

    @ApiModelProperty(value = "下单客户名称", notes = "采购商、供应商")
    private String customerName;

    @ApiModelProperty(value = "客户商家id")
    private String merchantId;

    @ApiModelProperty(value = "客户商家名称")
    private String merchantName;

    @ApiModelProperty(value = "订单成交日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate dealDate;

    // 金额相关

    @ApiModelProperty(value = "预付标志", notes = "是否预付账单")
    private YesNoEnum advanceFlag;

    @ApiModelProperty(value = "预付金额")
    private BigDecimal advanceAmount;

    @ApiModelProperty(value = "账单金额", notes = "用于结算的金额")
    private BigDecimal billAmount;

    @ApiModelProperty(value = "订单金额", notes = "对应订单总金额")
    private BigDecimal orderAmount;

    @ApiModelProperty(value = "备注信息")
    protected String remarks;

    // 扩展

    @ApiModelProperty(value = "总数", notes = "分页总记录数")
    private Long total;

    public <T extends BaseBill> BillInfoDTO(T bill) {
        if (null != bill) {
            this.id = bill.getId();
            this.billSn = bill.getBillSn();
            this.reviewStatus = bill.getReviewStatus();
            this.billType = bill.getBillType();
            this.settlementSn = bill.getSettlementSn();
            this.settlementStatus = bill.getSettlementStatus();
            this.pmId = bill.getPmId();
            this.pmName = bill.getPmName();
            this.orderSn = bill.getOrderSn();
            this.customerId = bill.getCustomerId();
            this.customerName = bill.getCustomerName();
            this.merchantId = bill.getMerchantId();
            this.merchantName = bill.getMerchantName();
            this.dealDate = bill.getDealDate();
            this.advanceFlag = bill.getAdvanceFlag();
            this.advanceAmount = bill.getAdvanceAmount();
            this.billAmount = bill.getBillAmount();
            this.orderAmount = bill.getOrderAmount();
            this.remarks = bill.getRemarks();
        }
    }

    public <T extends BaseBill> void transToBill(T bill) {
        bill.setId(this.id);
        bill.setBillSn(this.billSn);
        bill.setReviewStatus(this.reviewStatus);
        bill.setBillType(this.billType);
        bill.setSettlementSn(this.settlementSn);
        bill.setSettlementStatus(this.settlementStatus);
        bill.setPmId(this.pmId);
        bill.setPmName(this.pmName);
        bill.setOrderSn(this.orderSn);
        bill.setCustomerId(this.customerId);
        bill.setCustomerName(this.customerName);
        bill.setMerchantId(this.merchantId);
        bill.setMerchantName(this.merchantName);
        bill.setDealDate(this.dealDate);
        bill.setAdvanceFlag(this.advanceFlag);
        bill.setAdvanceAmount(this.advanceAmount);
        bill.setBillAmount(this.billAmount);
        bill.setOrderAmount(this.orderAmount);
        bill.setRemarks(this.remarks);
    }


}
