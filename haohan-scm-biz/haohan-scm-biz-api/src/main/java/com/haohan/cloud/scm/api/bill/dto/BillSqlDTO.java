package com.haohan.cloud.scm.api.bill.dto;

import com.haohan.cloud.scm.api.common.dto.SelfSqlDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author dy
 * @date 2020/1/19
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class BillSqlDTO extends SelfSqlDTO {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "账单编号(应收、应付)")
    private String billSn;

    @ApiModelProperty(value = "账单类型", notes = "BillTypeEnum")
    private String billType;

    @ApiModelProperty(value = "账单审核状态: 1.待审核2.审核不通过3.审核通过", notes = "ReviewStatusEnum")
    private String reviewStatus;

    @ApiModelProperty(value = "排除的账单审核状态: 1.待审核2.审核不通过3.审核通过", notes = "ReviewStatusEnum")
    private String excludeStatus;

    @ApiModelProperty(value = "结算状态", notes = "YesNoEnum")
    private String settlementStatus;

    @ApiModelProperty(value = "预付标志", notes = "是否预付账单, YesNoEnum")
    protected String advanceFlag;

    @ApiModelProperty(value = "下单客户ID", notes = "采购商id、供应商id、客户sn")
    private String customerId;

    @ApiModelProperty(value = "下单客户ID 列表, 逗号分隔", notes = "采购商id、供应商id、客户sn")
    private String customerIds;

}
