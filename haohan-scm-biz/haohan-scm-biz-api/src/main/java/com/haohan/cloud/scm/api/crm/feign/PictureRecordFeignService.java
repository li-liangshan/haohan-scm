/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.crm.feign;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.crm.entity.PictureRecord;
import com.haohan.cloud.scm.api.crm.req.PictureRecordReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 客户现场图片内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "PictureRecordFeignService", value = ScmServiceName.SCM_BIZ_WMS)
public interface PictureRecordFeignService {


    /**
     * 通过id查询客户现场图片
     *
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/PictureRecord/{id}", method = RequestMethod.GET)
    R<PictureRecord> getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 客户现场图片 列表信息
     *
     * @param pictureRecordReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/PictureRecord/fetchPictureRecordPage")
    R<Page<PictureRecord>> getPictureRecordPage(@RequestBody PictureRecordReq pictureRecordReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 客户现场图片 列表信息
     *
     * @param pictureRecordReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/PictureRecord/fetchPictureRecordList")
    R<List<PictureRecord>> getPictureRecordList(@RequestBody PictureRecordReq pictureRecordReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增客户现场图片
     *
     * @param pictureRecord 客户现场图片
     * @return R
     */
    @PostMapping("/api/feign/PictureRecord/add")
    R<Boolean> save(@RequestBody PictureRecord pictureRecord, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改客户现场图片
     *
     * @param pictureRecord 客户现场图片
     * @return R
     */
    @PostMapping("/api/feign/PictureRecord/update")
    R<Boolean> updateById(@RequestBody PictureRecord pictureRecord, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除客户现场图片
     *
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/PictureRecord/delete/{id}")
    R<Boolean> removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/PictureRecord/batchDelete")
    R<Boolean> removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/PictureRecord/listByIds")
    R<List<PictureRecord>> listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param pictureRecordReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/PictureRecord/countByPictureRecordReq")
    R<Integer> countByPictureRecordReq(@RequestBody PictureRecordReq pictureRecordReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param pictureRecordReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/PictureRecord/getOneByPictureRecordReq")
    R<PictureRecord> getOneByPictureRecordReq(@RequestBody PictureRecordReq pictureRecordReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param pictureRecordList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/PictureRecord/saveOrUpdateBatch")
    R<Boolean> saveOrUpdateBatch(@RequestBody List<PictureRecord> pictureRecordList, @RequestHeader(SecurityConstants.FROM) String from);


}
