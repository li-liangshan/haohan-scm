package com.haohan.cloud.scm.api.purchase.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author dy
 * @date 2019/5/16
 */
@Data
public class PurchaseQueryLendingListReq {


    @NotBlank(message = "pmId不能为空")
    private String pmId;

    @NotBlank(message = "uId不能为空")
    @ApiModelProperty(value = "用户id",required = true)
    private String uId;

    private long size=10;
    private long current=1;

}
