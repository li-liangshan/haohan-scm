package com.haohan.cloud.framework.exception;

import com.haohan.cloud.framework.entity.BaseResp;
import com.haohan.cloud.framework.utils.JacksonUtils;

import java.io.Serializable;

public abstract class ActionException extends RuntimeException {

	/**
	 *
	 */
	private static final long serialVersionUID = -3848547174252753762L;

	protected boolean isAjax = false;

	protected BaseResp exceptionResp = new BaseResp();

	public ActionException() {
		exceptionResp.error();
	}

	public ActionException(int code, String msg) {
		super(msg);
		exceptionResp.setCode(code);
		exceptionResp.setMsg(msg);
	}

	public ActionException(String message) {
		super(message);
		exceptionResp.error().setMsg(message);;
	}

	public ActionException(Throwable cause) {
		super(cause);
		exceptionResp.error().setMsg(cause.getMessage());
	}

	public ActionException(String message, Throwable cause) {
		super(message, cause);
		exceptionResp.error().setMsg(message);
	}

	public ActionException setExt(Serializable obj) {
		this.exceptionResp.setExt(obj);
		return this;
	}

	public String toJson() {
		return JacksonUtils.toJson(exceptionResp);
	}

	public boolean isAjax() {
		return isAjax;
	}

	public void setAjax(boolean isAjax) {
		this.isAjax = isAjax;
	}

	public ActionException setAjax() {
		this.isAjax = true;
		return this;
	}

	public BaseResp getExceptionResp() {
		return exceptionResp;
	}

	public void setExceptionResp(BaseResp exceptionResp) {
		this.exceptionResp = exceptionResp;
	}

	public void throwException(){
		throw this;
	}
}
