package com.haohan.cloud.scm.common.tools.constant;

import com.haohan.cloud.scm.common.tools.config.AliyunOssConfig;

/**
 * @author dy
 * @date 2019/10/30
 */
public interface AliyunConstant {

    String ENDPOINT = AliyunOssConfig.ENDPOINT;

    String ACCESS_KEY_ID = AliyunOssConfig.ACCESS_KEY_ID;

    String ACCESS_KEY_SECRET = AliyunOssConfig.ACCESS_KEY_SECRET;

    String BUCKET_NAME = AliyunOssConfig.BUCKET_NAME;

    String FILE_BUCKET_HTTP = AliyunOssConfig.FILE_BUCKET_HTTP;

    String MERCHANT_FILE_FOLDER = AliyunOssConfig.MERCHANT_FILE_FOLDER;

    String FILE_SEPARATOR = "/";



}
