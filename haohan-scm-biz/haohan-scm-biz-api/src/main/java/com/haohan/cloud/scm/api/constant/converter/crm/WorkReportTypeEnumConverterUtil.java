package com.haohan.cloud.scm.api.constant.converter.crm;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.crm.WorkReportTypeEnum;

/**
 * @author dy
 * @date 2019/9/27
 */
public class WorkReportTypeEnumConverterUtil implements Converter<WorkReportTypeEnum> {
    @Override
    public WorkReportTypeEnum convert(Object o, WorkReportTypeEnum workReportTypeEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return WorkReportTypeEnum.getByType(o.toString());
    }

}