/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.salec.feign;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.salec.entity.StoreProductAttrResult;
import com.haohan.cloud.scm.api.salec.req.StoreProductAttrResultReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 商品属性详情表内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "StoreProductAttrResultFeignService", value = ScmServiceName.SCM_BIZ_SALEC)
public interface StoreProductAttrResultFeignService {


  /**
   * 通过id查询商品属性详情表
   *
   * @param productId id
   * @return R
   */
  @RequestMapping(value = "/api/feign/StoreProductAttrResult/{productId}", method = RequestMethod.GET)
  R<StoreProductAttrResult> getById(@PathVariable("productId") Integer productId, @RequestHeader(SecurityConstants.FROM) String from);


  /**
   * 分页查询 商品属性详情表 列表信息
   *
   * @param storeProductAttrResultReq 请求对象
   * @return
   */
  @PostMapping("/api/feign/StoreProductAttrResult/fetchStoreProductAttrResultPage")
  R<Page<StoreProductAttrResult>> getStoreProductAttrResultPage(@RequestBody StoreProductAttrResultReq storeProductAttrResultReq, @RequestHeader(SecurityConstants.FROM) String from);


  /**
   * 全量查询 商品属性详情表 列表信息
   *
   * @param storeProductAttrResultReq 请求对象
   * @return
   */
  @PostMapping("/api/feign/StoreProductAttrResult/fetchStoreProductAttrResultList")
  R<List<StoreProductAttrResult>> getStoreProductAttrResultList(@RequestBody StoreProductAttrResultReq storeProductAttrResultReq, @RequestHeader(SecurityConstants.FROM) String from);

  /**
   * 新增商品属性详情表
   *
   * @param storeProductAttrResult 商品属性详情表
   * @return R
   */
  @PostMapping("/api/feign/StoreProductAttrResult/add")
  R<Boolean> save(@RequestBody StoreProductAttrResult storeProductAttrResult, @RequestHeader(SecurityConstants.FROM) String from);


  /**
   * 修改商品属性详情表
   *
   * @param storeProductAttrResult 商品属性详情表
   * @return R
   */
  @PostMapping("/api/feign/StoreProductAttrResult/update")
  R<Boolean> updateById(@RequestBody StoreProductAttrResult storeProductAttrResult, @RequestHeader(SecurityConstants.FROM) String from);


  /**
   * 通过id删除商品属性详情表
   *
   * @param productId id
   * @return R
   */
  @PostMapping("/api/feign/StoreProductAttrResult/delete/{productId}")
  R<Boolean> removeById(@PathVariable("productId") Integer productId, @RequestHeader(SecurityConstants.FROM) String from);

  /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
  @PostMapping("/api/feign/StoreProductAttrResult/batchDelete")
  R<Boolean> removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


  /**
   * 批量查询（根据IDS）
   *
   * @param idList 主键ID列表
   * @return R
   */
  @PostMapping("/api/feign/StoreProductAttrResult/listByIds")
  R<List<StoreProductAttrResult>> listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


  /**
   * 根据 Wrapper 条件，查询总记录数
   *
   * @param storeProductAttrResultReq 实体对象,可以为空
   * @return R
   */
  @PostMapping("/api/feign/StoreProductAttrResult/countByStoreProductAttrResultReq")
  R<Integer> countByStoreProductAttrResultReq(@RequestBody StoreProductAttrResultReq storeProductAttrResultReq, @RequestHeader(SecurityConstants.FROM) String from);


  /**
   * 根据对象条件，查询一条记录
   *
   * @param storeProductAttrResultReq 实体对象,可以为空
   * @return R
   */
  @PostMapping("/api/feign/StoreProductAttrResult/getOneByStoreProductAttrResultReq")
  R<StoreProductAttrResult> getOneByStoreProductAttrResultReq(@RequestBody StoreProductAttrResultReq storeProductAttrResultReq, @RequestHeader(SecurityConstants.FROM) String from);


  /**
   * 批量修改OR插入
   *
   * @param storeProductAttrResultList 实体对象集合 大小不超过1000条数据
   * @return R
   */
  @PostMapping("/api/feign/StoreProductAttrResult/saveOrUpdateBatch")
  R<Boolean> saveOrUpdateBatch(@RequestBody List<StoreProductAttrResult> storeProductAttrResultList, @RequestHeader(SecurityConstants.FROM) String from);


}
