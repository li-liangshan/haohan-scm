package com.haohan.cloud.scm.api.constant.converter.opc;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.opc.ShipRecordStatusEnum;

/**
 * @author dy
 * @date 2020/1/6
 */
public class ShipRecordStatusEnumConverterUtil implements Converter<ShipRecordStatusEnum> {

    @Override
    public ShipRecordStatusEnum convert(Object o, ShipRecordStatusEnum e) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return ShipRecordStatusEnum.getByType(o.toString());
    }

}
