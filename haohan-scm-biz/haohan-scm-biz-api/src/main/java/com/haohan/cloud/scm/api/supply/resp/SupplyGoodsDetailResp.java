package com.haohan.cloud.scm.api.supply.resp;

import com.haohan.cloud.scm.api.constant.enums.supply.GradeTypeEnum;
import com.haohan.cloud.scm.api.supply.entity.OfferOrder;
import lombok.Data;

/**
 *
 * @author cx
 * @date 2019/8/16
 */

@Data
public class SupplyGoodsDetailResp extends OfferOrder {

    private String cateName;

    private String address;

    private String phone;

    private GradeTypeEnum level;
}
