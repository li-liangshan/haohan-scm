package com.haohan.cloud.scm.bill.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.bill.dto.BillInfoDTO;
import com.haohan.cloud.scm.api.bill.dto.BillSqlDTO;
import com.haohan.cloud.scm.api.bill.entity.PayableBill;
import com.haohan.cloud.scm.api.crm.vo.app.BillPageVO;

/**
 * @author dy
 * @date 2019/11/27
 */
public interface PayableBillService extends IService<PayableBill> {

    /**
     * 根据 编号查询
     *
     * @param billSn
     * @return
     */
    PayableBill fetchBySn(String billSn);

    /**
     * 查询订单的预付账单
     *
     * @param orderSn
     * @return
     */
    PayableBill fetchAdvanceByOrder(String orderSn);

    /**
     * 查询订单的账单 (普通，非预付账单)
     *
     * @param orderSn
     * @return
     */
    PayableBill fetchNormalByOrder(String orderSn);

    /**
     * 按下单客户统计 账单金额
     *
     * @param params reviewStatus、billType、settlementStatus、customerIdSet
     * @return customerId、customerName、billAmount
     */
    BillPageVO<BillInfoDTO> countBillByCustomer(BillSqlDTO params);

    /**
     * 账单统计 总数量、总金额
     *
     * @param params reviewStatus、billType、settlementStatus、customerIdSet
     *               excludeStatus、advanceFlag
     * @return total、billAmount
     */
    BillInfoDTO countBill(BillSqlDTO params);
}
