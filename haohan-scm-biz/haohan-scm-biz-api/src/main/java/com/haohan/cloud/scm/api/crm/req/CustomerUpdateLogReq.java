/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.crm.req;

import com.haohan.cloud.scm.api.crm.entity.CustomerUpdateLog;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;


/**
 * 客户修改日志
 *
 * @author haohan
 * @date 2019-08-30 11:45:33
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "客户修改日志")
public class CustomerUpdateLogReq extends CustomerUpdateLog {

    private long pageSize;
    private long pageNo;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate startDate;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate endDate;

}
