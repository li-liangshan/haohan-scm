package com.haohan.cloud.scm.api.goods.req.manage;

import com.haohan.cloud.scm.api.constant.enums.goods.GoodsFromTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.goods.GoodsStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.goods.GoodsTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import com.haohan.cloud.scm.api.goods.entity.Goods;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.Valid;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author dy
 * @date 2020/5/6
 */
@Data
public class EditGoodsReq implements Serializable {
    private static final long serialVersionUID = 1L;

    @Length(max = 32, message = "商品id的长度最大32字符")
    @ApiModelProperty("主键")
    private String goodsId;

    @NotBlank(message = "店铺id不能为空")
    @Length(max = 32, message = "店铺id的长度最大32字符")
    @ApiModelProperty("店铺id")
    private String shopId;

    @NotBlank(message = "分类id不能为空")
    @Length(max = 32, message = "分类id的长度最大32字符")
    @ApiModelProperty("分类id")
    private String goodsCategoryId;

    @NotBlank(message = "商品名称不能为空")
    @Length(max = 32, message = "商品名称的长度最大32字符")
    @ApiModelProperty(value = "商品名称")
    private String goodsName;

    @Length(max = 65535, message = "商品描述的长度最大65535字符")
    @ApiModelProperty(value = "商品描述")
    private String detailDesc;

    @NotBlank(message = "店铺id不能为空")
    @Length(max = 255, message = "图片地址的长度最大255字符")
    @ApiModelProperty(value = "图片地址")
    private String thumbUrl;

    @Length(max = 255, message = "搜索关键词的长度最大255字符")
    @ApiModelProperty(value = "概要描述(搜索关键词)，用于商品名称别名搜索")
    private String simpleDesc;

    @Digits(integer = 8, fraction = 2, message = "库存数量的整数位最大8位, 小数位最大2位")
    @ApiModelProperty(value = "库存数量")
    private BigDecimal storage;

    @Range(min = 0, max = 99999, message = "排序值在0至99999之间")
    @ApiModelProperty(value = "排序值")
    private Integer sort;

    @Length(max = 64, message = "扫码购编码的长度最大64字符")
    @ApiModelProperty(value = "扫码购编码")
    private String scanCode;

    // 状态属性

//    @ApiModelProperty(value = "是否上架")
//    private YesNoEnum isMarketable;

    @ApiModelProperty(value = "售卖规则标记")
    private YesNoEnum saleRule;

    @ApiModelProperty(value = "服务选项标记")
    private YesNoEnum serviceSelection;

    @ApiModelProperty(value = "配送规则标记")
    private YesNoEnum deliveryRule;
    /**
     * 赠品标记
     */
    private YesNoEnum goodsGift;
    /**
     * 商品状态(出售中/仓库中/已售罄)
     */
    @NotNull(message = "商品出售状态不能为空")
    private GoodsStatusEnum goodsStatus;
    /**
     * 商品来源 0.小店平台 1.即速应用
     */
    private GoodsFromTypeEnum goodsFrom;
    /**
     * 商品类型
     */
    private GoodsTypeEnum goodsType;
    /**
     * 是否c端销售
     */
    private YesNoEnum salecFlag;

    // 默认价格属性
    /**
     * 零售定价,单位元 (使用,市场价)
     */
    @NotNull(message = "市场价不能为空")
    @Digits(integer = 8, fraction = 2, message = "市场价的整数位最大8位, 小数位最大2位")
    private BigDecimal marketPrice;
    /**
     * vip定价,单位元  (会员价)
     */
    @Digits(integer = 8, fraction = 2, message = "会员价的整数位最大8位, 小数位最大2位")
    private BigDecimal vipPrice;
    /**
     * 虚拟价格
     */
    @Digits(integer = 8, fraction = 2, message = "虚拟价格的整数位最大8位, 小数位最大2位")
    private BigDecimal virtualPrice;
    /**
     * 计量单位
     */
    @NotBlank(message = "计量单位不能为空")
    @Length(max = 6, message = "计量单位的长度最大6字符")
    private String unit;

    // 规格

//    @NotNull(message = "商品规格map不能为空")
//    @ApiModelProperty(value = "商品规格属性名列表的映射")
//    Map<String, List<String>> attrMap;

    @Valid
    @NotEmpty(message = "商品规格列表不能为空")
    @ApiModelProperty(value = "商品规格列表")
    private List<EditModelReq> modelList;

    // 图片组图片 (轮播图)

    @ApiModelProperty(value = "图片组图片 (轮播图) id列表")
    private List<String> photoList;

    // 扩展使用属性(无需传入)

    @ApiModelProperty(value = "商家id")
    @Length(max = 64, message = "商家id的长度最大64字符")
    private String merchantId;


    public Goods transTo() {
        Goods goods = new Goods();
        goods.setId(this.goodsId);
        goods.setShopId(this.shopId);
        goods.setGoodsCategoryId(this.goodsCategoryId);
        goods.setGoodsName(this.goodsName);
        goods.setDetailDesc(this.detailDesc);
        goods.setThumbUrl(this.thumbUrl);
        goods.setSimpleDesc(this.simpleDesc);
        goods.setStorage(this.storage);
        goods.setSort(null == this.sort ? null : this.sort.toString());
        goods.setScanCode(this.scanCode);
        goods.setSaleRule(this.saleRule);
        goods.setServiceSelection(this.serviceSelection);
        goods.setDeliveryRule(this.deliveryRule);
        goods.setGoodsGift(this.goodsGift);
        // 上下架状态  仓库中为下架
        goods.setGoodsStatus(this.goodsStatus);
        goods.setIsMarketable(this.goodsStatus == GoodsStatusEnum.stock ? 0 : 1);
        goods.setGoodsFrom(this.goodsFrom);
        goods.setGoodsType(this.goodsType);
        // c端销售状态固定不可直接修改
//        goods.setSalecFlag(this.salecFlag);
        goods.setMerchantId(this.merchantId);
        return goods;
    }
}
