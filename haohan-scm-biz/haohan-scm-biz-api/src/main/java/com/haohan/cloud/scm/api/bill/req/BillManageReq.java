package com.haohan.cloud.scm.api.bill.req;

import com.haohan.cloud.scm.common.tools.constant.SingleGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @author dy
 * @date 2019/12/7
 * SingleGroup 账单修改金额
 */
@Data
@ApiModel("账单管理")
public class BillManageReq {

    @NotBlank(message = "账单编号不能为空")
    @Length(max = 32, message = "账单编号最大长度32字符")
    @ApiModelProperty(value = "账单编号(应收、应付)")
    private String billSn;

    @NotNull(message = "账单金额不能为空", groups = {SingleGroup.class})
    @Digits(integer = 8, fraction = 2, message = "账单金额的整数位最大8位, 小数位最大2位")
    @ApiModelProperty(value = "账单金额", notes = "用于结算的金额")
    private BigDecimal billAmount;

    @Digits(integer = 8, fraction = 2, message = "预付金额的整数位最大8位, 小数位最大2位")
    @ApiModelProperty(value = "预付金额")
    private BigDecimal advanceAmount;

    @ApiModelProperty(value = "审核通过标识")
    private Boolean successFlag;

    @Length(max = 64, message = "备注信息最大长度64字符")
    @ApiModelProperty(value = "备注信息")
    private String remarks;

}
