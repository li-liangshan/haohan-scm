package com.haohan.cloud.scm.api.goods.req;

import com.haohan.cloud.scm.api.goods.entity.GoodsModel;
import com.haohan.cloud.scm.api.goods.req.manage.GoodsPricingReq;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;

/**
 * @author dy
 * @date 2020/4/23
 */
@Data
public class GoodsModelFeignReq implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("商家id")
    private String merchantId;

    @ApiModelProperty("店铺id")
    private String shopId;

    @ApiModelProperty("商品id")
    private String goodsId;

    @ApiModelProperty("商品规格id")
    private String modelId;

    @ApiModelProperty("商品分类id")
    private String categoryId;

    @ApiModelProperty("商品规格id集合")
    private Set<String> modelIdSet;

    @ApiModelProperty("商品规格id集合")
    private List<GoodsModel> modelList;

    // 扩展

    @Length(max = 32, message = "采购商id的长度最大32字符")
    @ApiModelProperty("采购商id,用于联查平台商品定价")
    private String buyerId;

    @Length(max = 32, message = "平台商品定价商家id的长度最大32字符")
    @ApiModelProperty("平台商品定价商家id")
    private String pricingMerchantId;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty("平台商品定价查询日期")
    private LocalDate pricingDate;

    public GoodsPricingReq transToPricingReq() {
        GoodsPricingReq req = new GoodsPricingReq();
        req.setBuyerId(this.buyerId);
        req.setPricingMerchantId(this.pricingMerchantId);
        req.setPricingDate(this.pricingDate);
        return req;
    }
}
