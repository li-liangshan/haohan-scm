package com.haohan.cloud.scm.product.api.ctrl;

import com.haohan.cloud.scm.api.product.entity.ProductLossRecord;
import com.haohan.cloud.scm.api.product.req.ProductInfoInventoryReq;
import com.haohan.cloud.scm.product.core.IProductLossRecordService;
import com.pig4cloud.pigx.common.core.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author cx
 * @date 2019/6/14
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/product/productLossRecord")
@Api(value = "ApiProductLossRecord", tags = "ProductLossRecord 货品报损记录操作api")
public class ProductLossRecordApiCtrl {


    private final IProductLossRecordService iProductLossRecordService;

    @PostMapping("/addLossRecord")
    @ApiOperation(value = "新增报损记录")
    public R addLossRecord(@RequestBody ProductInfoInventoryReq req){
        return R.ok(iProductLossRecordService.addLossRecord(req));
    }

    @PostMapping("/editLossRecord")
    @ApiOperation(value = "编辑报损记录")
    public R editLossRecord(@RequestBody ProductLossRecord record){
        return R.ok(iProductLossRecordService.editLossRecord(record));
    }
}
