package com.haohan.cloud.scm.api.crm.req.report;

import com.haohan.cloud.scm.api.constant.ScmCommonConstant;
import com.haohan.cloud.scm.api.constant.enums.crm.DataReportEnum;
import com.haohan.cloud.scm.api.constant.enums.crm.DataReportStatusEnum;
import com.haohan.cloud.scm.api.crm.entity.DataReport;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

/**
 * @author dy
 * @date 2019/9/28
 */
@Data
public class DataReportModifyReq {

    @NotBlank(message = "上报编号不能为空")
    @Length(min = 0, max = 32, message = "上报编号长度在0至32之间")
    @ApiModelProperty(value = "上报编号")
    private String reportSn;

    @ApiModelProperty(value = "上报类型 0.库存 1.销售记录 2.竞品")
    private DataReportEnum reportType;

    @Pattern(regexp = ScmCommonConstant.POSITION_PATTEN, message = "位置定位, 经纬度格式有误, 经度正负180度之间, 纬度正负90度之间, 弧度数表示")
    @ApiModelProperty(value = "上报位置定位")
    private String reportLocation;

    @Length(min = 0, max = 64, message = "上报位置地址长度在0至64之间")
    @ApiModelProperty(value = "上报位置地址")
    private String reportAddress;

    @Length(min = 0, max = 32, message = "上报人id长度在0至32之间")
    @ApiModelProperty(value = "上报人id(员工)")
    private String reportManId;

    @NotNull(message = "上报日期不能为空")
    @ApiModelProperty(value = "上报日期/销售日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate reportDate;

//    @Length(min = 0, max = 32, message = "操作人id长度在0至32之间")
//    @ApiModelProperty(value = "操作人id")
//    private String operatorId;

    @ApiModelProperty(value = "上报状态0待确认1已确认")
    private DataReportStatusEnum reportStatus;

    @Digits(integer = 8, fraction = 2, message = "其他金额的整数位最大8位, 小数位最大2位")
    @Range(min = 0, max = 1000000, message = "批发价在0至1000000之间")
    @ApiModelProperty(value = "商品总金额")
    private BigDecimal totalAmount;

    @Digits(integer = 8, fraction = 2, message = "其他金额的整数位最大8位, 小数位最大2位")
    @Range(min = 0, max = 1000000, message = "其他金额在0至1000000之间")
    @ApiModelProperty(value = "其他金额")
    private BigDecimal otherAmount;

    @Length(min = 0, max = 255, message = "备注信息长度在0至255之间")
    @ApiModelProperty(value = "备注信息")
    private String remarks;

    @Length(min = 0, max = 32, message = "图片组编号长度在0至32之间")
    @ApiModelProperty(value = "图片组编号")
    private String photoGroupNum;

    @ApiModelProperty(value = "图片列表")
    private List<String> photoList;

    @Valid
    @NotEmpty(message = "上报明细不能为空")
    @ApiModelProperty(value = "上报明细")
    private List<DataDetailAddReq> detailList;


    public DataReport transTo() {
        DataReport report = new DataReport();
        report.setReportType(this.reportType);
        report.setReportLocation(this.reportLocation);
        report.setReportAddress(this.reportAddress);
        report.setReportManId(this.reportManId);
        report.setReportDate(this.reportDate);
        // 修改后的上报都为待确认
        report.setReportStatus(DataReportStatusEnum.wait);
        report.setTotalAmount(this.totalAmount);
        report.setOtherAmount(this.otherAmount);
        report.setRemarks(this.remarks);
        return report;
    }
}
