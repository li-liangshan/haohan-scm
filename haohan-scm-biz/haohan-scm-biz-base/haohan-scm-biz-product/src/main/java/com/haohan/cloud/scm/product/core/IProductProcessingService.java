package com.haohan.cloud.scm.product.core;

import com.haohan.cloud.scm.api.product.dto.ProductPakingDTO;
import com.haohan.cloud.scm.api.product.dto.ProductPakingResultDTO;
import com.haohan.cloud.scm.api.product.entity.ProductProcessing;
import com.haohan.cloud.scm.api.product.req.CreateProductProcessingReq;


/**
 * @author cx
 * @date 2019/6/14
 */
public interface IProductProcessingService {

    /**
     * 创建货品加工记录(需生成货品损耗记录)
     * @param req
     * @return
     */
    ProductProcessing createProductProcessing(CreateProductProcessingReq req);

    /**
     * 货品组合 (按配方 合并为一个 原始货品)
     * @param dto
     * @return
     */
    ProductPakingResultDTO productPacking(ProductPakingDTO dto) ;
}
