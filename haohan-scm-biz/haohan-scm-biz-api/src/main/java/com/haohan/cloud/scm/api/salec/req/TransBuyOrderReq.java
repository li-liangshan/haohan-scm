package com.haohan.cloud.scm.api.salec.req;

import com.haohan.cloud.scm.api.constant.enums.common.DeliveryTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.BuySeqEnum;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

/**
 * @author dy
 * @date 2020/6/11
 */
@Data
public class TransBuyOrderReq implements Serializable {
    private static final long serialVersionUID = 1L;

    @NotEmpty(message = "订单编号列表不能为空")
    private List<String> orderIdList;

    @NotNull(message = "送货批次不能为空")
    private BuySeqEnum buySeq;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @NotNull(message = "送货日期不能为空")
    private LocalDate deliveryDate;

    @NotNull(message = "配送类型不能为空")
    private DeliveryTypeEnum deliveryType;

}
