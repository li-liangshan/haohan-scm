/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.manage.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.manage.entity.BusinessNote;
import com.haohan.cloud.scm.api.manage.req.BusinessNoteReq;
import com.haohan.cloud.scm.manage.service.BusinessNoteService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 商务留言
 *
 * @author haohan
 * @date 2019-05-29 14:25:50
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/BusinessNote")
@Api(value = "businessnote", tags = "businessnote内部接口服务")
public class BusinessNoteFeignApiCtrl {

    private final BusinessNoteService businessNoteService;


    /**
     * 通过id查询商务留言
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(businessNoteService.getById(id));
    }


    /**
     * 分页查询 商务留言 列表信息
     * @param businessNoteReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchBusinessNotePage")
    public R getBusinessNotePage(@RequestBody BusinessNoteReq businessNoteReq) {
        Page page = new Page(businessNoteReq.getPageNo(), businessNoteReq.getPageSize());
        BusinessNote businessNote =new BusinessNote();
        BeanUtil.copyProperties(businessNoteReq, businessNote);

        return new R<>(businessNoteService.page(page, Wrappers.query(businessNote)));
    }


    /**
     * 全量查询 商务留言 列表信息
     * @param businessNoteReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchBusinessNoteList")
    public R getBusinessNoteList(@RequestBody BusinessNoteReq businessNoteReq) {
        BusinessNote businessNote =new BusinessNote();
        BeanUtil.copyProperties(businessNoteReq, businessNote);

        return new R<>(businessNoteService.list(Wrappers.query(businessNote)));
    }


    /**
     * 新增商务留言
     * @param businessNote 商务留言
     * @return R
     */
    @Inner
    @SysLog("新增商务留言")
    @PostMapping("/add")
    public R save(@RequestBody BusinessNote businessNote) {
        return new R<>(businessNoteService.save(businessNote));
    }

    /**
     * 修改商务留言
     * @param businessNote 商务留言
     * @return R
     */
    @Inner
    @SysLog("修改商务留言")
    @PostMapping("/update")
    public R updateById(@RequestBody BusinessNote businessNote) {
        return new R<>(businessNoteService.updateById(businessNote));
    }

    /**
     * 通过id删除商务留言
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除商务留言")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(businessNoteService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除商务留言")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(businessNoteService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询商务留言")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(businessNoteService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param businessNoteReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询商务留言总记录}")
    @PostMapping("/countByBusinessNoteReq")
    public R countByBusinessNoteReq(@RequestBody BusinessNoteReq businessNoteReq) {

        return new R<>(businessNoteService.count(Wrappers.query(businessNoteReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param businessNoteReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据businessNoteReq查询一条货位信息表")
    @PostMapping("/getOneByBusinessNoteReq")
    public R getOneByBusinessNoteReq(@RequestBody BusinessNoteReq businessNoteReq) {

        return new R<>(businessNoteService.getOne(Wrappers.query(businessNoteReq), false));
    }


    /**
     * 批量修改OR插入
     * @param businessNoteList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<BusinessNote> businessNoteList) {

        return new R<>(businessNoteService.saveOrUpdateBatch(businessNoteList));
    }

}
