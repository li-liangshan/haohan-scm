package com.haohan.cloud.scm.api.constant.enums.supply;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xwx
 * @date 2019/6/4
 */
@Getter
@AllArgsConstructor
public enum SupplierStatusEnum {
    /**
     * 供应商状态
     */
    prepare("0", "待备货"),
    take("1", "待揽货"),
    shipped("2", "已发货");


    private static final Map<String, SupplierStatusEnum> MAP = new HashMap<>(8);

    static {
        for (SupplierStatusEnum e : SupplierStatusEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static SupplierStatusEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

    @EnumValue
    @JsonValue
    private String type;
    private String desc;
}
