/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.purchase.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.purchase.entity.SupplierRelation;
import com.haohan.cloud.scm.purchase.mapper.SupplierRelationMapper;
import com.haohan.cloud.scm.purchase.service.SupplierRelationService;
import org.springframework.stereotype.Service;

/**
 * 采购员工管理供应商
 *
 * @author haohan
 * @date 2019-05-13 18:47:46
 */
@Service
public class SupplierRelationServiceImpl extends ServiceImpl<SupplierRelationMapper, SupplierRelation> implements SupplierRelationService {

}
