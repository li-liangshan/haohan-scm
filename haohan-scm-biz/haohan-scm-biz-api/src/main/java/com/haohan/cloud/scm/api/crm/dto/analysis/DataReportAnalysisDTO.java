package com.haohan.cloud.scm.api.crm.dto.analysis;

import cn.hutool.core.util.StrUtil;
import com.haohan.cloud.scm.api.common.dto.SelfSqlDTO;
import com.haohan.cloud.scm.api.constant.enums.crm.SalesGoodsTypeEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * @author dy
 * @date 2019/11/11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Api("数据上报统计")
public class DataReportAnalysisDTO extends SelfSqlDTO {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "数据上报订单数")
    private Integer num;

    @ApiModelProperty(value = "数据上报订单金额")
    private BigDecimal amount;

    @ApiModelProperty(value = "上报类型 0.库存 1.销售记录 2.竞品")
    private String reportType;

    @ApiModelProperty(value = "上报状态0待确认1确认")
    private String reportStatus;

    @ApiModelProperty(value = "排除的上报状态")
    private String excludeStatus;

    @ApiModelProperty(value = "客户编号")
    private String customerSn;

    @ApiModelProperty(value = "客户名称")
    private String customerName;

    @ApiModelProperty(value = "业务员")
    private String employeeId;

    @ApiModelProperty(value = "业务员名称")
    private String employeeName;

    @ApiModelProperty(value = "上报人id(员工)")
    private String reportManId;

    @ApiModelProperty(value = "上报日期/销售日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate reportDate;

    @ApiModelProperty(value = "商品名称")
    private String goodsName;

    @ApiModelProperty(value = "商品规格名称")
    private String modelName;

    @ApiModelProperty(value = "商品规格编号")
    private String goodsModelSn;

    @ApiModelProperty(value = "单位")
    private String goodsUnit;

    @ApiModelProperty(value = "业务员id列表，逗号连接")
    private String employeeIds;

    @ApiModelProperty(value = "是否按客户分组")
    private Boolean customerGroup;

    @ApiModelProperty(value = "商品数")
    private BigDecimal goodsNum;

    @ApiModelProperty(value = "区域编码")
    private String areaSn;

    @ApiModelProperty(value = "市场编码")
    private String marketSn;

    @ApiModelProperty(value = "客户编号列表")
    private String customerSns;

    @ApiModelProperty(value = "商品销售类型:1.普通2.特价3.赠品")
    private SalesGoodsTypeEnum salesGoodsType;

    // 字典处理(自定义sql使用)

    public String getSalesGoodsType() {
        return null == this.salesGoodsType ? null : this.salesGoodsType.getType();
    }

    /**
     * 处理上报人查询
     * reportManId 传入即 只查询该上报人
     */
    public void transEmployeeToReportMan() {
        if (StrUtil.isEmpty(this.reportManId) && StrUtil.isNotEmpty(this.employeeId)) {
            this.reportManId = this.employeeId;
        }
        this.employeeId = null;
    }
}
