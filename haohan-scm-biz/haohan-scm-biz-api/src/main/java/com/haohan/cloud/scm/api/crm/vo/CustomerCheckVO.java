package com.haohan.cloud.scm.api.crm.vo;

import com.haohan.cloud.scm.api.crm.entity.Customer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author dy
 * @date 2020/4/7
 */
@Data
public class CustomerCheckVO {

    @ApiModelProperty(value = "是否已存在")
    private Boolean existFlag;

    @ApiModelProperty(value = "客户编号")
    private String customerSn;

    @ApiModelProperty(value = "客户名称")
    private String customerName;

    @ApiModelProperty(value = "客户详细地址")
    private String address;

    @ApiModelProperty(value = "客户联系人名称")
    private String contact;

    @ApiModelProperty(value = "联系人手机")
    private String telephone;

    @ApiModelProperty(value = "客户负责人名称")
    private String directorName;

    public CustomerCheckVO() {
        this.existFlag = false;
    }

    public void copyFrom(Customer customer) {
        this.customerSn = customer.getCustomerSn();
        this.customerName=customer.getCustomerName();
        this.address=customer.getAddress();
        this.contact=customer.getContact();
        this.telephone=customer.getTelephone();
    }
}
