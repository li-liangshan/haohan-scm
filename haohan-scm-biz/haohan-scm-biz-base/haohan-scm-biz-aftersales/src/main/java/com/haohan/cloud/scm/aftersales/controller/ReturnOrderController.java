/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.aftersales.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.aftersales.service.ReturnOrderService;
import com.haohan.cloud.scm.api.aftersales.entity.ReturnOrder;
import com.haohan.cloud.scm.api.aftersales.req.ReturnOrderReq;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 退货单
 *
 * @author haohan
 * @date 2019-07-30 16:09:37
 */
@RestController
@AllArgsConstructor
@RequestMapping("/returnorder" )
@Api(value = "returnorder", tags = "returnorder管理")
public class ReturnOrderController {

    private final ReturnOrderService returnOrderService;


    /**
     * 分页查询
     * @param page 分页对象
     * @param returnOrder 退货单
     * @return
     */
    @GetMapping("/page" )
    @ApiOperation(value = "分页查询退货单")
    public R getReturnOrderPage(Page page, ReturnOrder returnOrder) {
        return new R<>(returnOrderService.page(page, Wrappers.query(returnOrder)));
    }


    /**
     * 通过id查询退货单
     * @param id
     * @return R
     */
    @GetMapping("/{id}")
    @ApiOperation(value = "通过id查询退货单")
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(returnOrderService.getById(id));
    }

    /**
     * 新增退货单
     * @param returnOrder 退货单
     * @return R
     */
    @SysLog("新增退货单" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_returnorder_add')" )
    @ApiOperation(value = "新增退货单")
    public R save(@RequestBody ReturnOrder returnOrder) {
        return new R<>(returnOrderService.save(returnOrder));
    }

    /**
     * 修改退货单
     * @param returnOrder 退货单
     * @return R
     */
    @SysLog("修改退货单" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_returnorder_edit')")
    @ApiOperation(value = "修改退货单")
    public R updateById(@RequestBody ReturnOrder returnOrder) {
        return new R<>(returnOrderService.updateById(returnOrder));
    }

    /**
     * 通过id删除退货单
     * @param id id
     * @return R
     */
    @SysLog("删除退货单" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_returnorder_del')")
    @ApiOperation(value = "通过id删除退货单")
    public R removeById(@PathVariable String id) {
        return new R<>(returnOrderService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除退货单")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_returnorder_del')")
    @ApiOperation(value = "根据IDS批量删除退货单")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(returnOrderService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询退货单")
    @PostMapping("/listByIds")
    @PreAuthorize("@pms.hasPermission('scm_returnorder_del')")
    @ApiOperation(value = "根据IDS批量查询退货单")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(returnOrderService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param returnOrderReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询退货单总记录")
    @PostMapping("/countByReturnOrderReq")
    @ApiOperation(value = "查询退货单总记录")
    public R countByReturnOrderReq(@RequestBody ReturnOrderReq returnOrderReq) {
        return new R<>(returnOrderService.count(Wrappers.query(returnOrderReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param returnOrderReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据returnOrderReq查询一条退货单记录")
    @PostMapping("/getOneByReturnOrderReq")
    @ApiOperation(value = "根据returnOrderReq查询一条退货单记录")
    public R getOneByReturnOrderReq(@RequestBody ReturnOrderReq returnOrderReq) {

        return new R<>(returnOrderService.getOne(Wrappers.query(returnOrderReq), false));
    }


    /**
     * 批量修改OR插入退货单
     * @param returnOrderList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入退货单")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_returnorder_edit')")
    @ApiOperation(value = "批量修改OR插入退货单信息")
    public R saveOrUpdateBatch(@RequestBody List<ReturnOrder> returnOrderList) {

        return new R<>(returnOrderService.saveOrUpdateBatch(returnOrderList));
    }


}
