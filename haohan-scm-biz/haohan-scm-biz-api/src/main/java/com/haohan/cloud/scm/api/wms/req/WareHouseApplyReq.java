package com.haohan.cloud.scm.api.wms.req;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel(description = "入库单申请")
public class WareHouseApplyReq {


  /**
   * 采购单编号
   */
  private String purchaseSn;

  /**
   * 入库货品编号
   */
  private String productSn;

  /**
   * 采购单明细编号
   */
  private String purchaseDetailSn;

  /**
   * 入库申请人
   */
  private String applicantId;
  /**
   * 申请人名称
   */
  private String applicantName;


}
