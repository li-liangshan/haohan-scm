package com.haohan.cloud.scm.common.tools.redis;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * @program: haohan-fresh-scm
 * @description: 缓存Redis配置
 * @author: Simon
 * @create: 2019-05-28
 **/

@Configuration
public class JedisConfig {

  @Value("${jedis.maxIdle}")
  private int maxIdle;

  @Value("${jedis.maxTotal}")
  private int maxTotal;

  @Value("${jedis.testOnBorrow}")
  private boolean testOnBorrow;

  @Value("${jedis.host}")
  private String host;

  @Value("${jedis.port}")
  private int port;

  @Value("${jedis.timeout}")
  private int timeout;

  @Value("${jedis.password}")
  private String password;


  @Bean
  public JedisPool createJedisPool(){

    GenericObjectPoolConfig poolConfig = new JedisPoolConfig();
    poolConfig.setMaxIdle(maxIdle);
    poolConfig.setMaxTotal(maxTotal);
    poolConfig.setTestOnBorrow(testOnBorrow);
    if(StringUtils.isEmpty(password)){
      password = null;
    }
    JedisPool jedisPool = new JedisPool(poolConfig,host,port,timeout,password);

    return jedisPool;

  }



}
