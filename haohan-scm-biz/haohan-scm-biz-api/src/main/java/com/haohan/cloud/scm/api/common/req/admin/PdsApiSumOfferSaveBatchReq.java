package com.haohan.cloud.scm.api.common.req.admin;


import com.haohan.cloud.scm.api.common.params.PdsOfferOrderSaveParams;

import java.util.List;

/**
 * @author shenyu
 * @create 2018/12/26
 */
public class PdsApiSumOfferSaveBatchReq {
    private String pmId;
    private List<PdsOfferOrderSaveParams> offerOrderList;

    public String getPmId() {
        return pmId;
    }

    public void setPmId(String pmId) {
        this.pmId = pmId;
    }

    public List<PdsOfferOrderSaveParams> getOfferOrderList() {
        return offerOrderList;
    }

    public void setOfferOrderList(List<PdsOfferOrderSaveParams> offerOrderList) {
        this.offerOrderList = offerOrderList;
    }
}
