/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.wms.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.wms.entity.Pallet;
import com.haohan.cloud.scm.api.wms.req.PalletReq;
import com.haohan.cloud.scm.wms.service.PalletService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 托盘信息表
 *
 * @author haohan
 * @date 2019-05-29 13:22:38
 */
@RestController
@AllArgsConstructor
@RequestMapping("/pallet" )
@Api(value = "pallet", tags = "pallet管理")
public class PalletController {

    private final PalletService palletService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param pallet 托盘信息表
     * @return
     */
    @GetMapping("/page" )
    public R getPalletPage(Page page, Pallet pallet) {
        return new R<>(palletService.page(page, Wrappers.query(pallet)));
    }


    /**
     * 通过id查询托盘信息表
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(palletService.getById(id));
    }

    /**
     * 新增托盘信息表
     * @param pallet 托盘信息表
     * @return R
     */
    @SysLog("新增托盘信息表" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_pallet_add')" )
    public R save(@RequestBody Pallet pallet) {
        return new R<>(palletService.save(pallet));
    }

    /**
     * 修改托盘信息表
     * @param pallet 托盘信息表
     * @return R
     */
    @SysLog("修改托盘信息表" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_pallet_edit')" )
    public R updateById(@RequestBody Pallet pallet) {
        return new R<>(palletService.updateById(pallet));
    }

    /**
     * 通过id删除托盘信息表
     * @param id id
     * @return R
     */
    @SysLog("删除托盘信息表" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_pallet_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(palletService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除托盘信息表")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_pallet_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(palletService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询托盘信息表")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(palletService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param palletReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询托盘信息表总记录}")
    @PostMapping("/countByPalletReq")
    public R countByPalletReq(@RequestBody PalletReq palletReq) {

        return new R<>(palletService.count(Wrappers.query(palletReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param palletReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据palletReq查询一条货位信息表")
    @PostMapping("/getOneByPalletReq")
    public R getOneByPalletReq(@RequestBody PalletReq palletReq) {

        return new R<>(palletService.getOne(Wrappers.query(palletReq), false));
    }


    /**
     * 批量修改OR插入
     * @param palletList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_pallet_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<Pallet> palletList) {

        return new R<>(palletService.saveOrUpdateBatch(palletList));
    }


}
