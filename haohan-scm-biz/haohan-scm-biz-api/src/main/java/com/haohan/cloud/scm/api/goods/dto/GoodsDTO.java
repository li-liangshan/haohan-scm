package com.haohan.cloud.scm.api.goods.dto;

import cn.hutool.core.util.StrUtil;
import com.haohan.cloud.scm.api.constant.enums.goods.GoodsFromTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.goods.GoodsStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.goods.GoodsTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.goods.ModelAttrNameEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import com.haohan.cloud.scm.api.goods.entity.Goods;
import com.haohan.cloud.scm.api.goods.entity.GoodsPriceRule;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @author dy
 * @date 2019/10/16
 */
@Data
@NoArgsConstructor
public class GoodsDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "商品id")
    private String id;

    @ApiModelProperty(value = "商品名称")
    private String goodsName;

    @ApiModelProperty(value = "商品描述")
    private String detailDesc;

    @ApiModelProperty(value = "图片地址")
    private String thumbUrl;

    /**
     * 商品唯一编号
     */
    private String goodsSn;
    /**
     * 概要描述  用于商品名称别名搜索
     */
    private String simpleDesc;
    /**
     * 图片组编号
     */
    private String photoGroupNum;
    /**
     * 库存数量
     */
    private BigDecimal storage;
    /**
     * 排序
     */
    private String sort;
    /**
     * 扫码购编码
     */
    private String scanCode;
    /**
     * 公共商品库通用编号
     */
    private String generalSn;

    // 状态属性
    /**
     * 是否上架 YesNoEnum  0.否 1.是
     */
    private YesNoEnum isMarketable;
    /**
     * 售卖规则标记
     */
    private YesNoEnum saleRule;
    /**
     * 服务选项标记
     */
    private YesNoEnum serviceSelection;
    /**
     * 配送规则标记
     */
    private YesNoEnum deliveryRule;
    /**
     * 赠品标记
     */
    private YesNoEnum goodsGift;
    /**
     * 商品规格标记
     */
    private YesNoEnum goodsModel;
    /**
     * 商品状态(出售中/仓库中/已售罄)
     */
    private GoodsStatusEnum goodsStatus;
    /**
     * 商品来源 0.小店平台 1.即速应用
     */
    private GoodsFromTypeEnum goodsFrom;
    /**
     * 商品类型
     */
    private GoodsTypeEnum goodsType;
    /**
     * 是否c端销售
     */
    private YesNoEnum salecFlag;

    // 默认价格属性
    /**
     * 零售定价,单位元 (使用,市场价)
     */
    private BigDecimal marketPrice;
    /**
     * vip定价,单位元  (会员价)
     */
    private BigDecimal vipPrice;
    /**
     * 虚拟价格
     */
    private BigDecimal virtualPrice;
    /**
     * 计量单位
     */
    private String unit;

    // 其他属性
    /**
     * 第三方编号/即速商品id
     */
    private String thirdGoodsSn;

    @ApiModelProperty(value = "行业名称")
    private String industry;

    @ApiModelProperty(value = "品牌名称")
    private String brand;

    @ApiModelProperty(value = "厂家制造商")
    private String manufacturer;

    // 商家店铺

    @ApiModelProperty(value = "店铺ID")
    private String shopId;

    @ApiModelProperty(value = "商家ID")
    private String merchantId;

    // 分类

    @ApiModelProperty(value = "分类id")
    private String categoryId;

    @ApiModelProperty(value = "分类名称")
    private String categoryName;

    // 规格

    @ApiModelProperty(value = "商品规格属性值列表的映射")
    Map<ModelAttrNameEnum, List<String>> attrMap;

    @ApiModelProperty(value = "商品规格列表")
    List<GoodsModelDTO> modelList;

    public GoodsDTO(Goods goods){
        this.id = goods.getId();
        this.shopId = goods.getShopId();
        this.merchantId = goods.getMerchantId();
        this.categoryId = goods.getGoodsCategoryId();
        this.goodsSn = goods.getGoodsSn();
        this.goodsName = goods.getGoodsName();
        this.detailDesc = goods.getDetailDesc();
        this.simpleDesc = goods.getSimpleDesc();
        this.brand = goods.getBrandId();
        this.saleRule = goods.getSaleRule();
        this.photoGroupNum = goods.getPhotoGroupNum();
        this.thumbUrl = goods.getThumbUrl();
        this.isMarketable = goods.getIsMarketable() == 1 ? YesNoEnum.yes: YesNoEnum.no;
        this.storage = goods.getStorage();
        this.serviceSelection = goods.getServiceSelection();
        this.deliveryRule = goods.getDeliveryRule();
        this.goodsGift = goods.getGoodsGift();
        this.goodsModel = goods.getGoodsModel();
        this.goodsStatus = goods.getGoodsStatus();
        this.goodsFrom = goods.getGoodsFrom();
        this.sort = goods.getSort();
        this.goodsType = goods.getGoodsType();
        this.scanCode = goods.getScanCode();
        this.thirdGoodsSn = goods.getThirdGoodsSn();
        this.generalSn = goods.getGeneralSn();
        this.salecFlag = goods.getSalecFlag();
        // todo 其他属性
    }

    public void copyPriceRule(GoodsPriceRule rule){
        this.marketPrice = rule.getMarketPrice();
        this.vipPrice =rule.getVipPrice();
        this.virtualPrice= rule.getVirtualPrice();
        this.unit = rule.getUnit();
    }

    public GoodsPriceRule transToPriceRule() {
        GoodsPriceRule rule = new GoodsPriceRule();
        rule.setShopId(this.shopId);
        rule.setMerchantId(this.merchantId);
        rule.setGoodsId(this.id);
        rule.setMarketPrice(this.marketPrice);
        rule.setVipPrice(this.vipPrice);
        rule.setVirtualPrice(this.virtualPrice);
        rule.setUnit(this.unit);
        return rule;
    }

    public Goods transToGoods() {
        Goods goods = new Goods();
         goods.setId(this.id);
         goods.setShopId(this.shopId);
         goods.setMerchantId(this.merchantId);
         goods.setGoodsCategoryId(this.categoryId);
         goods.setGoodsSn(this.goodsSn);
         goods.setGoodsName(this.goodsName);
         goods.setDetailDesc(this.detailDesc);
         goods.setSimpleDesc(this.simpleDesc);
         goods.setBrandId(this.brand);
         goods.setSaleRule(this.saleRule);
         goods.setPhotoGroupNum(this.photoGroupNum);
         goods.setThumbUrl(this.thumbUrl);
         goods.setIsMarketable(Integer.valueOf(this.isMarketable.getType()));
         goods.setStorage(this.storage);
         goods.setServiceSelection(this.serviceSelection);
         goods.setDeliveryRule(this.deliveryRule);
         goods.setGoodsGift(this.goodsGift);
         goods.setGoodsModel(this.goodsModel);
         goods.setGoodsStatus(this.goodsStatus);
         goods.setGoodsFrom(this.goodsFrom);
         goods.setSort(this.sort);
         goods.setGoodsType(this.goodsType);
         goods.setScanCode(this.scanCode);
         goods.setThirdGoodsSn(this.thirdGoodsSn);
         goods.setGeneralSn(this.generalSn);
         goods.setSalecFlag(this.salecFlag);
        // todo 其他属性
        String remarks = StrUtil.emptyToDefault(this.industry,"")
                .concat(",")
                .concat(StrUtil.emptyToDefault(this.brand,""))
                .concat(",")
                .concat(StrUtil.emptyToDefault(this.manufacturer,""));
        goods.setRemarks(remarks);
        return goods;
    }
}
