/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.crm.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.haohan.cloud.scm.api.constant.ScmCommonConstant;
import com.haohan.cloud.scm.api.constant.enums.crm.VisitStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.crm.VisitStepEnum;
import com.haohan.cloud.scm.api.constant.enums.market.CustomerStatusEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Pattern;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * 客户拜访记录
 *
 * @author haohan
 * @date 2019-08-30 12:01:54
 */
@Data
@TableName("crm_customer_visit")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "客户拜访记录")
public class CustomerVisit extends Model<CustomerVisit> {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 客户编号
     */
    @ApiModelProperty(value = "客户编号")
    private String customerSn;
    /**
     * 客户名称
     */
    @ApiModelProperty(value = "客户名称")
    private String customerName;
    /**
     * 拜访地址
     */
    @Length(max = 64, message = "拜访地址长度最大64字符")
    @ApiModelProperty(value = "拜访地址")
    private String visitAddress;
    /**
     * 拜访联系人id
     */
    @ApiModelProperty(value = "拜访联系人id")
    private String linkmanId;
    /**
     * 拜访联系人名称
     */
    @ApiModelProperty(value = "拜访联系人名称")
    private String linkmanName;
    /**
     * 拜访日期
     */
    @ApiModelProperty(value = "拜访日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate visitDate;
    /**
     * 拜访内容
     */
    @ApiModelProperty(value = "拜访内容")
    private String visitContent;
    /**
     * 进展阶段:1初次拜访2了解交流3.深入跟进4达成合作5商务往来
     */
    @ApiModelProperty(value = "进展阶段:1初次拜访2了解交流3.深入跟进4达成合作5商务往来")
    private VisitStepEnum visitStep;
    /**
     * 拜访图片组编号
     */
    @ApiModelProperty(value = "拜访图片组编号")
    private String photoNum;
    /**
     * 拜访员工id
     */
    @ApiModelProperty(value = "拜访员工id")
    private String employeeId;
    /**
     * 拜访员工名称
     */
    @ApiModelProperty(value = "拜访员工名称")
    private String employeeName;
    /**
     * 下次回访时间
     */
    @ApiModelProperty(value = "下次回访时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime nextVisitTime;
    /**
     * 客户状态:0.无1潜在2.有意向3成交4失败
     */
    @ApiModelProperty(value = "客户状态:0.无1潜在2.有意向3成交4失败")
    private CustomerStatusEnum customerStatus;
    /**
     * 修改为  拜访状态: 1.待拜访 2.拜访中 3.已拜访 4.未完成
     */
    @ApiModelProperty(value = "拜访状态: 1.待拜访 2.拜访中 3.已拜访 4.未完成")
    private VisitStatusEnum visitStatus;
    /**
     * 拜访总结
     */
    @Length(max = 255, message = "客户拜访记录id长度最大255字符")
    @ApiModelProperty(value = "拜访总结")
    private String summaryContent;
    /**
     * 抵达时间
     */
    @ApiModelProperty(value = "抵达时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime arrivalTime;
    /**
     * 离开时间
     */
    @ApiModelProperty(value = "离开时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime departureTime;
    /**
     * 抵达地址定位
     */
    @Pattern(regexp = ScmCommonConstant.POSITION_PATTEN, message = "抵达地址定位, 经纬度格式有误, 经度正负180度之间, 纬度正负90度之间, 弧度数表示")
    @ApiModelProperty(value = "抵达地址定位")
    private String arrivalPosition;

    @ApiModelProperty(value = "抵达地址")
    private String arrivalAddress;
    /**
     * 离开地址定位
     */
    @Pattern(regexp = ScmCommonConstant.POSITION_PATTEN, message = "离开地址定位, 经纬度格式有误, 经度正负180度之间, 纬度正负90度之间, 弧度数表示")
    @ApiModelProperty(value = "离开地址定位")
    private String departurePosition;

    @ApiModelProperty(value = "离开地址")
    private String departureAddress;
    /**
     * 创建者
     */
    @ApiModelProperty(value = "创建者")
    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
     * 更新者
     */
    @ApiModelProperty(value = "更新者")
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.UPDATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateDate;
    /**
     * 备注信息
     */
    @ApiModelProperty(value = "备注信息")
    private String remarks;
    /**
     * 删除标记
     */
    @ApiModelProperty(value = "删除标记")
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;
    /**
     * 租户id
     */
    @ApiModelProperty(value = "租户id")
    private Integer tenantId;

}
