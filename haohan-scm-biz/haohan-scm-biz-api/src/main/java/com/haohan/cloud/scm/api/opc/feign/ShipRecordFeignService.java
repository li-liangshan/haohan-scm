/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.opc.feign;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.opc.entity.ShipRecord;
import com.haohan.cloud.scm.api.opc.req.ShipRecordReq;
import com.haohan.cloud.scm.api.opc.req.ship.ShipRecordFeignReq;
import com.haohan.cloud.scm.api.opc.vo.ShipRecordVO;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 发货记录内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "ShipRecordFeignService", value = ScmServiceName.SCM_BIZ_OPC)
public interface ShipRecordFeignService {


    /**
     * 通过id查询发货记录
     *
     * @param id   id
     * @param from
     * @return R
     */
    @RequestMapping(value = "/api/feign/ShipRecord/{id}", method = RequestMethod.GET)
    R<ShipRecord> getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 发货记录 列表信息
     *
     * @param req  请求对象
     * @param from
     * @return
     */
    @PostMapping("/api/feign/ShipRecord/fetchShipRecordPage")
    R<Page<ShipRecord>> getShipRecordPage(@RequestBody ShipRecordFeignReq req, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 通过sn查询发货记录详情
     *
     * @param shipRecordSn
     * @param from
     * @return R
     */
    @RequestMapping(value = "/api/feign/ShipRecord/fetchInfo/{shipRecordSn}", method = RequestMethod.GET)
    R<ShipRecordVO> fetchInfo(@PathVariable("shipRecordSn") String shipRecordSn, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 全量查询 发货记录 列表信息
     *
     * @param shipRecordReq 请求对象
     * @param from
     * @return
     */
    @PostMapping("/api/feign/ShipRecord/fetchShipRecordList")
    R<List<ShipRecord>> getShipRecordList(@RequestBody ShipRecordReq shipRecordReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增发货记录
     *
     * @param shipRecord 发货记录
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/ShipRecord/add")
    R<Boolean> save(@RequestBody ShipRecord shipRecord, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改发货记录
     *
     * @param shipRecord 发货记录
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/ShipRecord/update")
    R<Boolean> updateById(@RequestBody ShipRecord shipRecord, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除发货记录
     *
     * @param id   id
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/ShipRecord/delete/{id}")
    R<Boolean> removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/ShipRecord/batchDelete")
    R<Boolean> removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/ShipRecord/listByIds")
    R<List<ShipRecord>> listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param shipRecordReq 实体对象,可以为空
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/ShipRecord/countByShipRecordReq")
    R<Integer> countByShipRecordReq(@RequestBody ShipRecordReq shipRecordReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param shipRecordReq 实体对象,可以为空
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/ShipRecord/getOneByShipRecordReq")
    R<ShipRecord> getOneByShipRecordReq(@RequestBody ShipRecordReq shipRecordReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param shipRecordList 实体对象集合 大小不超过1000条数据
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/ShipRecord/saveOrUpdateBatch")
    R<Boolean> saveOrUpdateBatch(@RequestBody List<ShipRecord> shipRecordList, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 根据订单创建发货记录(已存在发货记录则修改)
     * @param req
     * @param from
     * @return
     */
    @PostMapping("/api/feign/ShipRecord/createByOrder")
    R<ShipRecord> createShipRecordByOrder(@RequestBody ShipRecordFeignReq req, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 根据订单创建发货记录(批量)
     * @param req
     * @param from
     * @return
     */
    @PostMapping("/api/feign/ShipRecord/createBatchByOrder")
    R<Boolean> createShipRecordBatchByOrder(@RequestBody ShipRecordFeignReq req, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 删除发货记录 (不为已发货)
     * @param req
     * @param from
     * @return
     */
    @PostMapping("/api/feign/ShipRecord/deleteShipRecordByOrder")
    R<Boolean> deleteShipRecordByOrder(@RequestBody ShipRecordFeignReq req, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 通过订单sn查询发货记录
     * @param req
     * @param from
     * @return
     */
    @PostMapping("/api/feign/ShipRecord/fetchOneByOrder")
    R<ShipRecord> fetchOneByOrder(@RequestBody ShipRecordFeignReq req, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 发货记录商家名称修改
     * @param req
     * @param from
     * @return
     */
    @PostMapping("/api/feign/ShipRecord/updateMerchantName")
    R<Boolean> updateMerchantName(@RequestBody ShipRecordReq req, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 根据订单信息更新发货记录  (不为已发货)
     * @param req
     * @param from
     * @return
     */
    @PostMapping("/api/feign/ShipRecord/updateShipRecordByOrder")
    R<Boolean> updateShipRecordByOrder(@RequestBody ShipRecordFeignReq req, @RequestHeader(SecurityConstants.FROM) String from);

}
