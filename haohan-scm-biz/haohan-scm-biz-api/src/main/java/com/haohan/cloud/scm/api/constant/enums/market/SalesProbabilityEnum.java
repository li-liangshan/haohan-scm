package com.haohan.cloud.scm.api.constant.enums.market;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xwx
 * @date 2019/5/30
 */
@Getter
@AllArgsConstructor
public enum SalesProbabilityEnum {
    /**
     * 可能性:1高2中3低
     */
    high("1","高"),
    middle("2","中"),
    low("3","低");

    private static final Map<String, SalesProbabilityEnum> MAP = new HashMap<>(8);

    static {
        for (SalesProbabilityEnum e : SalesProbabilityEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static SalesProbabilityEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

    /**
     * 类型
     */
    @EnumValue
    @JsonValue
    private String type;
    /**
     * 描述
     */

    private String desc;
}
