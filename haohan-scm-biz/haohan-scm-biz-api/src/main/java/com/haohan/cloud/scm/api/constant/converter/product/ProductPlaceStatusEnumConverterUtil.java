package com.haohan.cloud.scm.api.constant.converter.product;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.product.ProductPlaceStatusEnum;

/**
 * @author xwx
 * @date 2019/6/4
 */
public class ProductPlaceStatusEnumConverterUtil implements Converter<ProductPlaceStatusEnum> {
    @Override
    public ProductPlaceStatusEnum convert(Object o, ProductPlaceStatusEnum productPlaceStatusEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return ProductPlaceStatusEnum.getByType(o.toString());
    }
}
