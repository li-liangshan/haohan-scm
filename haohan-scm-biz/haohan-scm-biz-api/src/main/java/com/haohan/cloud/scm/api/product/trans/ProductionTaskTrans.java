package com.haohan.cloud.scm.api.product.trans;

import com.haohan.cloud.scm.api.constant.enums.common.TaskStatusEnum;
import com.haohan.cloud.scm.api.goods.dto.GoodsModelDTO;
import com.haohan.cloud.scm.api.opc.entity.SummaryOrder;
import com.haohan.cloud.scm.api.product.entity.ProductionTask;

/**
 * @author cx
 * @date 2019/6/13
 */
public class ProductionTaskTrans {
    public static void summaryOrderToProductionTaskTrans(ProductionTask task,SummaryOrder order){
            task.setPmId(order.getPmId());
            task.setTaskStatus(TaskStatusEnum.wait);
            task.setSummaryOrderId(order.getId());
            task.setGoodsModelId(order.getGoodsModelId());
            task.setGoodsName(order.getGoodsName());
            task.setUnit(order.getUnit());
            task.setDeliveryDate(order.getDeliveryTime());
            task.setNeedNumber(order.getNeedBuyNum());
    }

    public static void goodsModelToProductionTaskTrans(ProductionTask task, GoodsModelDTO model){
        task.setGoodsModelName(model.getModelName());
        task.setGoodsName(model.getGoodsName());
        task.setUnit(model.getModelUnit());
        task.setTaskStatus(TaskStatusEnum.wait);
    }

}
