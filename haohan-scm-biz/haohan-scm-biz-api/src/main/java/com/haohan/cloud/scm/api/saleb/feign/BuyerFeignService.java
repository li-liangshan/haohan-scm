/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.saleb.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.saleb.entity.Buyer;
import com.haohan.cloud.scm.api.saleb.req.BuyerReq;
import com.haohan.cloud.scm.api.saleb.req.CountBuyerNumReq;
import com.haohan.cloud.scm.api.saleb.req.QueryBuyerStatusReq;
import com.haohan.cloud.scm.api.saleb.req.buyer.BuyerFeignReq;
import com.haohan.cloud.scm.api.saleb.vo.BuyerPayVO;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 采购商内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "BuyerFeignService", value = ScmServiceName.SCM_BIZ_SALEB)
public interface BuyerFeignService {


    /**
     * 通过id查询采购商
     *
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/Buyer/{id}", method = RequestMethod.GET)
    R<Buyer> getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 采购商 列表信息
     *
     * @param buyerReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/Buyer/fetchBuyerPage")
    R getBuyerPage(@RequestBody BuyerReq buyerReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 采购商 列表信息
     *
     * @param buyerReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/Buyer/fetchBuyerList")
    R getBuyerList(@RequestBody BuyerReq buyerReq, @RequestHeader(SecurityConstants.FROM) String from);

//    /**
//     * 新增采购商
//     *
//     * @param buyer 采购商
//     * @return R
//     */
//    @PostMapping("/api/feign/Buyer/add")
//    R save(@RequestBody Buyer buyer, @RequestHeader(SecurityConstants.FROM) String from);
//
//
//    /**
//     * 修改采购商
//     *
//     * @param buyer 采购商
//     * @return R
//     */
//    @PostMapping("/api/feign/Buyer/update")
//    R updateById(@RequestBody Buyer buyer, @RequestHeader(SecurityConstants.FROM) String from);


//    /**
//     * 通过id删除采购商
//     *
//     * @param id id
//     * @return R
//     */
//    @PostMapping("/api/feign/Buyer/delete/{id}")
//    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);
//
//    /**
//     * 删除（根据ID 批量删除)
//     *
//     * @param idList 主键ID列表
//     * @return R
//     */
//    @PostMapping("/api/feign/Buyer/batchDelete")
//    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/Buyer/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param buyerReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/Buyer/countByBuyerReq")
    R<Integer> countByBuyerReq(@RequestBody BuyerReq buyerReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param buyerReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/Buyer/getOneByBuyerReq")
    R<Buyer> getOneByBuyerReq(@RequestBody BuyerReq buyerReq, @RequestHeader(SecurityConstants.FROM) String from);


//    /**
//     * 批量修改OR插入
//     *
//     * @param buyerList 实体对象集合 大小不超过1000条数据
//     * @return R
//     */
//    @PostMapping("/api/feign/Buyer/saveOrUpdateBatch")
//    R saveOrUpdateBatch(@RequestBody List<Buyer> buyerList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 查询B客户某时间范围段人数
     *
     * @param countBuyerNumReq
     * @return
     */
    @PostMapping("/api/feign/Buyer/countRangeBuyerNum")
    R countRangeBuyerNum(@RequestBody CountBuyerNumReq countBuyerNumReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 查询采购商是否可下单
     *
     * @return
     */
    @PostMapping("/api/feign/Buyer/queryStatus")
    R<Boolean> buyerStatus(@Validated QueryBuyerStatusReq req, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 查询采购商是否需下单支付
     *
     * @param req
     * @param from
     * @return
     */
    @PostMapping("/api/feign/Buyer/buyerPayInfo")
    R<BuyerPayVO> buyerPayInfo(@Validated QueryBuyerStatusReq req, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 根据uid查询
     * 查询不到时新增采购商及商家
     * (salec用户转采购商, 默认无saleb小程序、管理后台登陆功能)
     *
     * @param query
     * @param from
     * @return
     */
    @PostMapping("/api/feign/Buyer/fetchBuyerByUidWithAdd")
    R<Buyer> fetchBuyerByUidWithAdd(@RequestBody BuyerFeignReq query, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 客户联系人创建采购商(用于登录saleb小程序)
     * (默认无管理后台登陆功能)
     *
     * @param buyer
     * @param from
     * @return
     */
    @PostMapping("/api/feign/Buyer/createByCustomer")
    R<Buyer> createBuyerByCustomer(@RequestBody Buyer buyer, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 采购商商家名称修改
     *
     * @param req 采购商  必须：merchantId/merchantName
     * @return R
     */
    @PostMapping("/api/feign/Buyer/updateMerchantName")
    R<Boolean> updateMerchantName(@RequestBody BuyerReq req, @RequestHeader(SecurityConstants.FROM) String from);

}
