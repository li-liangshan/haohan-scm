/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.salec.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;

/**
 * 商品属性详情表
 *
 * @author haohan
 * @date 2020-05-21 18:43:47
 */
@Data
@TableName("eb_store_product_attr_result")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "商品属性详情表")
public class StoreProductAttrResult extends Model<StoreProductAttrResult> {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "商品ID")
    private Integer productId;

    @Length(max = 65535, message = "商品属性参数长度最大65535字符")
    @ApiModelProperty(value = "商品属性参数")
    private String result;

    @ApiModelProperty(value = "上次修改时间")
    private Integer changeTime;

    @ApiModelProperty(value = "活动类型 0=商品，1=秒杀，2=砍价，3=拼团")
    private Integer type;

    @ApiModelProperty(value = "租户id")
    private Integer tenantId;

}
