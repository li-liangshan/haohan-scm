package com.haohan.cloud.scm.api.crm.trans;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.haohan.cloud.scm.api.constant.enums.crm.DataReportEnum;
import com.haohan.cloud.scm.api.constant.enums.message.DepartmentTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.message.InMailTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.message.MsgActionTypeEnum;
import com.haohan.cloud.scm.api.crm.entity.MarketEmployee;
import com.haohan.cloud.scm.api.message.dto.SendInMailMessageDTO;
import lombok.Data;
import lombok.experimental.UtilityClass;

/**
 * @author dy
 * @date 2020/1/14
 */
@Data
@UtilityClass
public class CrmMsgTrans {

    public String dataReportTypeTrans(DataReportEnum reportType) {
        String msg;
        switch (reportType) {
            case sales:
                msg = "销量上报";
                break;
            case stock:
                msg = "库存上报";
                break;
            case competition:
                msg = "竞品上报";
                break;
            default:
                msg = "上报";
        }
        return msg;
    }

    /**
     * 数据上报对应的业务类型
     *
     * @param reportType
     * @return
     */
    public MsgActionTypeEnum departmentTypeTrans(DataReportEnum reportType) {
        MsgActionTypeEnum type;
        switch (reportType) {
            case sales:
                type = MsgActionTypeEnum.salesReport;
                break;
            case stock:
                type = MsgActionTypeEnum.stockReport;
                break;
            case competition:
                type = MsgActionTypeEnum.competitionReport;
                break;
            default:
                type = MsgActionTypeEnum.system;
        }
        return type;
    }

    public SendInMailMessageDTO initInMailMessage(MarketEmployee employee, String title, String querySn, MsgActionTypeEnum type) {
        SendInMailMessageDTO msg = new SendInMailMessageDTO();
        msg.setTitle(title);
        msg.setContent(title);
        msg.setInMailType(InMailTypeEnum.notice);
        msg.setSenderUid(employee.getPassportId());
        msg.setSenderName(employee.getName());
        msg.setDepartmentType(DepartmentTypeEnum.bazaar);
        msg.setMsgActionType(type);
        JSONObject jsonObj = JSONUtil.createObj();
        jsonObj.put("querySn", querySn);
        msg.setReqParams(jsonObj.toString());
        return msg;
    }

}
