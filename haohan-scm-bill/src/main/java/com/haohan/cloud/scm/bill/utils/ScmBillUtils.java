package com.haohan.cloud.scm.bill.utils;

import cn.hutool.core.util.StrUtil;
import com.haohan.cloud.scm.api.bill.dto.OrderInfoDTO;
import com.haohan.cloud.scm.api.bill.trans.ScmBillTrans;
import com.haohan.cloud.scm.api.constant.ScmCacheNameConstant;
import com.haohan.cloud.scm.api.constant.enums.crm.PayStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.saleb.BillTypeEnum;
import com.haohan.cloud.scm.api.manage.dto.PhotoGroupDTO;
import com.haohan.cloud.scm.api.manage.entity.PhotoGallery;
import com.haohan.cloud.scm.api.manage.entity.PhotoGroupManage;
import com.haohan.cloud.scm.api.manage.feign.PhotoGroupManageFeignService;
import com.haohan.cloud.scm.common.tools.exception.ErrorDataException;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author dy
 * @date 2019/11/27
 * feign调用
 */
@Component
@AllArgsConstructor
public class ScmBillUtils {

    private final PhotoGroupManageFeignService photoGroupManageFeignService;

    /**
     * 保存或修改图片组图片
     * 修改时：需groupNum / photoList
     * 新增时 需 图片组名称/ 类别标签 / merchantId
     *
     * @param photoGroupDTO
     * @return
     */
    @CacheEvict(value = ScmCacheNameConstant.PHOTO_GROUP_LIST, key = "#photoGroupDTO.groupNum", condition = "#photoGroupDTO.groupNum != null")
    public String savePhotoGroup(PhotoGroupDTO photoGroupDTO) {
        R<PhotoGroupManage> r = photoGroupManageFeignService.savePhotoGroup(photoGroupDTO, SecurityConstants.FROM_IN);
        if (RUtil.isFailed(r)) {
            throw new ErrorDataException("保存或修改图片组图片失败");
        } else if (null == r.getData()) {
            return null;
        }
        return r.getData().getGroupNum();
    }

    /**
     * 查询图片组图片
     *
     * @param groupNum
     * @return
     */
    private PhotoGroupDTO fetchPhotoGroup(String groupNum) {
        R<PhotoGroupDTO> r = photoGroupManageFeignService.fetchByGroupNum(groupNum, SecurityConstants.FROM_IN);
        if (!RUtil.isSuccess(r)) {
            throw new ErrorDataException("查询图片组图片失败");
        } else if (null == r.getData()) {
            return null;
        }
        return r.getData();
    }

    /**
     * 查询图片组图片列表
     *
     * @param groupNum
     * @return 找不到时 返回 空列表
     */
    @Cacheable(value = ScmCacheNameConstant.PHOTO_GROUP_LIST, key = "#groupNum", condition = "#groupNum != null")
    public List<PhotoGallery> fetchPhotoList(String groupNum) {
        List<PhotoGallery> photoList = new ArrayList<>();
        if (StrUtil.isNotBlank(groupNum)) {
            PhotoGroupDTO photoGroupDTO = fetchPhotoGroup(groupNum);
            if (null != photoGroupDTO) {
                photoList = photoGroupDTO.getPhotoList();
            }
        }
        return photoList;
    }

    /**
     * 查询订单详情
     *
     * @param orderSn
     * @param billType
     * @return
     */
    public OrderInfoDTO fetchOrderInfo(String orderSn, BillTypeEnum billType) {
        return ScmBillOrderFactory.getOrderService(ScmBillTrans.transBillOrderType(billType))
                .fetchOrderInfo(orderSn);
    }

    /**
     * 订单结算状态修改
     *
     * @param orderSn
     * @param billType
     * @return
     */
    public boolean updateOrderSettlement(String orderSn, BillTypeEnum billType, PayStatusEnum status) {
        return ScmBillOrderFactory.getOrderService(ScmBillTrans.transBillOrderType(billType))
                .updateOrderSettlement(orderSn, status);
    }
}
