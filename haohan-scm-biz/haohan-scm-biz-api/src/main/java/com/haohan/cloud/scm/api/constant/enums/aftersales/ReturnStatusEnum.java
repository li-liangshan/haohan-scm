package com.haohan.cloud.scm.api.constant.enums.aftersales;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dy
 * @date 2019/8/6
 */
@Getter
@AllArgsConstructor
public enum ReturnStatusEnum implements IBaseEnum {
    /**
     * 退货状态1:待审核2:审核不通过3:审核通过4:退货中5:已退货6:已完成
     */
    wait("1", "待审核"),
    failed("2", "审核不通过"),
    adopt("3", "审核通过"),
    back("4", "退货中"),
    goods_back("5", "已退货"),
    success("6", "已完成");

    private static final Map<String, ReturnStatusEnum> MAP = new HashMap<>(8);

    static {
        for (ReturnStatusEnum e : ReturnStatusEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static ReturnStatusEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

    @EnumValue
    @JsonValue
    private String type;
    private String desc;
}