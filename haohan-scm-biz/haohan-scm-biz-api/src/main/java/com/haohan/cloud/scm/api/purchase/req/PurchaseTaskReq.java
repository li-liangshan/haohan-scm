/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.purchase.req;

import com.haohan.cloud.scm.api.purchase.entity.PurchaseTask;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 采购任务
 *
 * @author haohan
 * @date 2019-05-29 13:35:16
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "采购任务")
public class PurchaseTaskReq extends PurchaseTask {

    private long pageSize;
    private long pageNo;




}
