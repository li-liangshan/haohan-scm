/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.opc.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.opc.entity.SettlementRecord;
import com.haohan.cloud.scm.api.opc.req.SettlementRecordReq;
import com.haohan.cloud.scm.opc.service.SettlementRecordService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 结算记录
 *
 * @author haohan
 * @date 2019-05-30 10:19:56
 */
@RestController
@AllArgsConstructor
@RequestMapping("/settlementrecord" )
@Api(value = "settlementrecord", tags = "settlementrecord管理")
public class SettlementRecordController {

    private final SettlementRecordService settlementRecordService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param settlementRecord 结算记录
     * @return
     */
    @GetMapping("/page" )
    public R getSettlementRecordPage(Page page, SettlementRecord settlementRecord) {
        QueryWrapper<SettlementRecord> query = Wrappers.query(settlementRecord);
        query.orderByDesc("settlement_id");
        return new R<>(settlementRecordService.page(page, query));
    }


    /**
     * 通过id查询结算记录
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(settlementRecordService.getById(id));
    }

    /**
     * 新增结算记录
     * @param settlementRecord 结算记录
     * @return R
     */
    @SysLog("新增结算记录" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_settlementrecord_add')" )
    public R save(@RequestBody SettlementRecord settlementRecord) {
        return R.failed("不支持");
    }

    /**
     * 修改结算记录
     * @param settlementRecord 结算记录
     * @return R
     */
    @SysLog("修改结算记录" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_settlementrecord_edit')" )
    public R updateById(@RequestBody SettlementRecord settlementRecord) {
        return R.failed("不支持");
    }

    /**
     * 通过id删除结算记录
     * @param id id
     * @return R
     */
    @SysLog("删除结算记录" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_settlementrecord_del')" )
    public R removeById(@PathVariable String id) {
        return R.failed("不支持");
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除结算记录")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_settlementrecord_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return R.failed("不支持");
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询结算记录")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(settlementRecordService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param settlementRecordReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询结算记录总记录}")
    @PostMapping("/countBySettlementRecordReq")
    public R countBySettlementRecordReq(@RequestBody SettlementRecordReq settlementRecordReq) {

        return new R<>(settlementRecordService.count(Wrappers.query(settlementRecordReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param settlementRecordReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据settlementRecordReq查询一条货位信息表")
    @PostMapping("/getOneBySettlementRecordReq")
    public R getOneBySettlementRecordReq(@RequestBody SettlementRecordReq settlementRecordReq) {

        return new R<>(settlementRecordService.getOne(Wrappers.query(settlementRecordReq), false));
    }


    /**
     * 批量修改OR插入
     * @param settlementRecordList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_settlementrecord_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<SettlementRecord> settlementRecordList) {

        return R.failed("不支持");
    }


}
