package com.haohan.cloud.scm.api.constant.converter.manage;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.manage.PhotoTypeEnum;

/**
 * @author dy
 * @date 2019/9/12
 */
public class PhotoTypeEnumConverterUtil implements Converter<PhotoTypeEnum> {
    @Override
    public PhotoTypeEnum convert(Object o, PhotoTypeEnum photoTypeEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return PhotoTypeEnum.getByType(o.toString());
    }
}
