package com.haohan.cloud.scm.api.constant.enums.product;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dy
 */
@Getter
@AllArgsConstructor
public enum ProductPlaceStatusEnum implements IBaseEnum {

    /**
     * 货品位置状态:0.仓库1.采购部2.生产部3.物流部4.门店5.客户
     */
    stock("0","仓库"),
    purchase("1", "采购部"),
    product("2", "生产部"),
    delivery("3", "物流部"),
    store("4", "门店"),
    customer("5", "客户");

    private static final Map<String, ProductPlaceStatusEnum> MAP = new HashMap<>(8);

    static {
        for (ProductPlaceStatusEnum e : ProductPlaceStatusEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static ProductPlaceStatusEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

    @EnumValue
    @JsonValue
    private String type;

    private String desc;
}
