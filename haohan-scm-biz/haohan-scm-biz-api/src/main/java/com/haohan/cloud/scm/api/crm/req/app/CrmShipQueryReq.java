package com.haohan.cloud.scm.api.crm.req.app;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.constant.enums.opc.ShipRecordStatusEnum;
import com.haohan.cloud.scm.api.opc.req.ship.ShipRecordFeignReq;
import com.haohan.cloud.scm.common.tools.constant.SingleGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.groups.Default;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * @author dy
 * @date 2020/1/6
 */
@Data
public class CrmShipQueryReq implements Serializable {
    private static final long serialVersionUID = 1L;

    @NotBlank(message = "员工ID不能为空", groups = {SingleGroup.class, Default.class})
    @Length(max = 32, message = "员工ID最大长度32字符", groups = {SingleGroup.class, Default.class})
    @ApiModelProperty(value = "app登陆员工")
    private String employeeId;

    @NotBlank(message = "发货单编号不能为空", groups = {SingleGroup.class})
    @Length(max = 64, message = "发货单编号长度最大64字符", groups = {SingleGroup.class, Default.class})
    @ApiModelProperty(value = "发货单编号")
    private String shipSn;

    @Length(max = 64, message = "订单编号长度最大64字符")
    @ApiModelProperty(value = "订单编号")
    private String orderSn;

    @Length(max = 64, message = "客户编号长度最大64字符")
    @ApiModelProperty(value = "客户编号")
    private String customerSn;

    @ApiModelProperty(value = "发货单状态: 1.待发货 2.已发货 3.已关闭")
    private ShipRecordStatusEnum shipStatus;

    // 非eq

    @Length(max = 64, message = "发货人名称长度最大64字符")
    @ApiModelProperty(value = "发货人名称")
    private String shipperName;

    @ApiModelProperty(value = "查询发货日期-开始日期", notes = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate startDate;

    @ApiModelProperty(value = "查询发货日期-结束日期", notes = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate endDate;


    public ShipRecordFeignReq transTo(Page page) {
        ShipRecordFeignReq req = new ShipRecordFeignReq();
        req.setCurrent(page.getCurrent());
        req.setSize(page.getSize());
        req.setShipRecordSn(this.shipSn);
        req.setOrderSn(this.orderSn);
        req.setShipStatus(this.shipStatus);

        req.setShipperName(this.shipperName);
        req.setStartDate(this.startDate);
        req.setEndDate(this.endDate);
        return req;
    }
}
