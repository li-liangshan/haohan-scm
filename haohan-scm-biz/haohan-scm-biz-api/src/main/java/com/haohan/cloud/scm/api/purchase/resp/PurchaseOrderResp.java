package com.haohan.cloud.scm.api.purchase.resp;

import com.haohan.cloud.scm.api.purchase.entity.PurchaseOrder;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseOrderDetail;
import lombok.Data;

import java.util.List;

/**
 * @author xwx
 * @date 2019/7/3
 */
@Data
public class PurchaseOrderResp extends PurchaseOrder {

    private List<PurchaseOrderDetail> detailList;

}
