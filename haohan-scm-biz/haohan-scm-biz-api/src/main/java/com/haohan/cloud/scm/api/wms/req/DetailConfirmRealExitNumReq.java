package com.haohan.cloud.scm.api.wms.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;

/**
 * @author dy
 * @date 2019/7/13
 */
@Data
@ApiModel("确认出库单明细,实出数量")
public class DetailConfirmRealExitNumReq {

    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台id", required = true)
    private String pmId;

    @NotBlank(message = "出库明细id不能为空")
    @ApiModelProperty(value = "出库明细id", required = true)
    private String id;

    @NotBlank(message = "实际出库数量productNumber不能为空")
    @ApiModelProperty(value = "实际出库数量", required = true)
    private BigDecimal productNumber;

    @ApiModelProperty(value = "出库单编号")
    private String exitWarehouseSn;

}
