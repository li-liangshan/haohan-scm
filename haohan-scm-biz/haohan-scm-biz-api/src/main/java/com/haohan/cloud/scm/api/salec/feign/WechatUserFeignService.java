/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.salec.feign;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.salec.entity.WechatUser;
import com.haohan.cloud.scm.api.salec.req.WechatUserReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 微信用户表内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "WechatUserFeignService", value = ScmServiceName.SCM_BIZ_SALEC)
public interface WechatUserFeignService {


    /**
     * 通过id查询微信用户表
     *
     * @param uid id
     * @return R
     */
    @RequestMapping(value = "/api/feign/WechatUser/{uid}", method = RequestMethod.GET)
    R<WechatUser> getById(@PathVariable("uid") Integer uid, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 微信用户表 列表信息
     *
     * @param wechatUserReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/WechatUser/fetchWechatUserPage")
    R<Page<WechatUser>> getWechatUserPage(@RequestBody WechatUserReq wechatUserReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 微信用户表 列表信息
     *
     * @param wechatUserReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/WechatUser/fetchWechatUserList")
    R<List<WechatUser>> getWechatUserList(@RequestBody WechatUserReq wechatUserReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增微信用户表
     *
     * @param wechatUser 微信用户表
     * @return R
     */
    @PostMapping("/api/feign/WechatUser/add")
    R<Boolean> save(@RequestBody WechatUser wechatUser, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改微信用户表
     *
     * @param wechatUser 微信用户表
     * @return R
     */
    @PostMapping("/api/feign/WechatUser/update")
    R<Boolean> updateById(@RequestBody WechatUser wechatUser, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除微信用户表
     *
     * @param uid id
     * @return R
     */
    @PostMapping("/api/feign/WechatUser/delete/{uid}")
    R<Boolean> removeById(@PathVariable("uid") Integer uid, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/WechatUser/batchDelete")
    R<Boolean> removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/WechatUser/listByIds")
    R<List<WechatUser>> listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param wechatUserReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/WechatUser/countByWechatUserReq")
    R<Integer> countByWechatUserReq(@RequestBody WechatUserReq wechatUserReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param wechatUserReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/WechatUser/getOneByWechatUserReq")
    R<WechatUser> getOneByWechatUserReq(@RequestBody WechatUserReq wechatUserReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param wechatUserList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/WechatUser/saveOrUpdateBatch")
    R<Boolean> saveOrUpdateBatch(@RequestBody List<WechatUser> wechatUserList, @RequestHeader(SecurityConstants.FROM) String from);


}
