/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.supply.feign;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.supply.entity.SupplyOrderDetail;
import com.haohan.cloud.scm.api.supply.req.SupplyOrderDetailReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 供应订单明细内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "SupplyOrderDetailFeignService", value = ScmServiceName.SCM_BIZ_SUPPLY)
public interface SupplyOrderDetailFeignService {


    /**
     * 通过id查询供应订单明细
     *
     * @param id   id
     * @param from
     * @return R
     */
    @RequestMapping(value = "/api/feign/SupplyOrderDetail/{id}", method = RequestMethod.GET)
    R<SupplyOrderDetail> getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 供应订单明细 列表信息
     *
     * @param supplyOrderDetailReq 请求对象
     * @param from
     * @return
     */
    @PostMapping("/api/feign/SupplyOrderDetail/fetchSupplyOrderDetailPage")
    R<Page<SupplyOrderDetail>> getSupplyOrderDetailPage(@RequestBody SupplyOrderDetailReq supplyOrderDetailReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 供应订单明细 列表信息
     *
     * @param supplyOrderDetailReq 请求对象
     * @param from
     * @return
     */
    @PostMapping("/api/feign/SupplyOrderDetail/fetchSupplyOrderDetailList")
    R<List<SupplyOrderDetail>> getSupplyOrderDetailList(@RequestBody SupplyOrderDetailReq supplyOrderDetailReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增供应订单明细
     *
     * @param supplyOrderDetail 供应订单明细
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/SupplyOrderDetail/add")
    R<Boolean> save(@RequestBody SupplyOrderDetail supplyOrderDetail, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改供应订单明细
     *
     * @param supplyOrderDetail 供应订单明细
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/SupplyOrderDetail/update")
    R<Boolean> updateById(@RequestBody SupplyOrderDetail supplyOrderDetail, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除供应订单明细
     *
     * @param id   id
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/SupplyOrderDetail/delete/{id}")
    R<Boolean> removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/SupplyOrderDetail/batchDelete")
    R<Boolean> removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/SupplyOrderDetail/listByIds")
    R<List<SupplyOrderDetail>> listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param supplyOrderDetailReq 实体对象,可以为空
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/SupplyOrderDetail/countBySupplyOrderDetailReq")
    R<Integer> countBySupplyOrderDetailReq(@RequestBody SupplyOrderDetailReq supplyOrderDetailReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param supplyOrderDetailReq 实体对象,可以为空
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/SupplyOrderDetail/getOneBySupplyOrderDetailReq")
    R<SupplyOrderDetail> getOneBySupplyOrderDetailReq(@RequestBody SupplyOrderDetailReq supplyOrderDetailReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param supplyOrderDetailList 实体对象集合 大小不超过1000条数据
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/SupplyOrderDetail/saveOrUpdateBatch")
    R<Boolean> saveOrUpdateBatch(@RequestBody List<SupplyOrderDetail> supplyOrderDetailList, @RequestHeader(SecurityConstants.FROM) String from);


}
