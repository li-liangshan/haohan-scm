package com.haohan.cloud.scm.api.purchase.trans;

import cn.hutool.core.bean.BeanUtil;
import com.haohan.cloud.scm.api.constant.enums.purchase.*;
import com.haohan.cloud.scm.api.goods.dto.GoodsModelDTO;
import com.haohan.cloud.scm.api.opc.entity.SummaryOrder;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseOrderDetail;
import com.haohan.cloud.scm.api.purchase.req.ParamsReq;

/**
 * @author cx
 * @date 2019/6/19
 */
public class PurchaseOrderDetailTrans {
    public static PurchaseOrderDetail summaryOrderToDetailTrans(SummaryOrder order){
        PurchaseOrderDetail detail = new PurchaseOrderDetail();
        detail.setPmId(order.getPmId());
        detail.setSummaryDetailSn(order.getSummaryOrderId());
        detail.setGoodsId(order.getGoodsId());
        detail.setGoodsName(order.getGoodsName());
        detail.setGoodsImg(order.getGoodsImg());
        detail.setGoodsModelId(order.getGoodsModelId());
        detail.setUnit(order.getUnit());
        detail.setGoodsCategoryId(order.getGoodsCategoryId());
        detail.setNeedBuyNum(order.getNeedBuyNum());
        detail.setMarketPrice(order.getMarketPrice());
        detail.setModelName(order.getGoodsModel());
        // 状态设置
        detail.setPurchaseStatus(PurchaseStatusEnum.pending);
        detail.setPurchaseOrderType(PurchaseOrderTypeEnum.materialsNeeded);
        detail.setActionType(ActionTypeEnum.normal);
        detail.setMethodType(MethodTypeEnum.single);
        detail.setLendingType(LendingTypeEnum.noLending);
        detail.setBiddingStatus(BiddingStatusEnum.noBidding);
        return detail;
    }

    /**
     * 直接新增采购单和采购单明细
     * @param goodsModelDTO
     * @return
     */
    public static PurchaseOrderDetail trans(GoodsModelDTO goodsModelDTO, ParamsReq req){
        PurchaseOrderDetail orderDetail = new PurchaseOrderDetail();
        BeanUtil.copyProperties(req,orderDetail);
        orderDetail.setGoodsId(goodsModelDTO.getGoodsId());
        orderDetail.setGoodsModelId(goodsModelDTO.getId());
        orderDetail.setGoodsCategoryId(goodsModelDTO.getGoodsCategoryId());
        orderDetail.setGoodsImg(goodsModelDTO.getModelUrl());
        orderDetail.setGoodsCategoryName(goodsModelDTO.getCategoryName());
        orderDetail.setGoodsName(goodsModelDTO.getGoodsName());
        orderDetail.setModelName(goodsModelDTO.getModelName());
        orderDetail.setUnit(goodsModelDTO.getModelUnit());
        orderDetail.setPurchaseStatus(PurchaseStatusEnum.pending);
        orderDetail.setMarketPrice(goodsModelDTO.getModelPrice());
        return orderDetail;
    }
}
