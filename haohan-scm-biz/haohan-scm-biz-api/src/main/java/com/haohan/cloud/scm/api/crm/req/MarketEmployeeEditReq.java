package com.haohan.cloud.scm.api.crm.req;

import cn.hutool.core.bean.BeanUtil;
import com.haohan.cloud.scm.api.constant.enums.common.UseStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.market.MarketEmployeeTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.market.SexEnum;
import com.haohan.cloud.scm.api.crm.entity.MarketEmployee;
import com.haohan.cloud.scm.common.tools.constant.SecondGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;

/**
 * @author dy
 * @date 2019/12/12
 */
@Data
public class MarketEmployeeEditReq {

    @NotBlank(message = "id不能为空", groups = {SecondGroup.class})
    @Length(max = 32, message = "主键id的长度最大为32字符")
    @ApiModelProperty(value = "主键")
    private String id;

    @NotBlank(message = "员工名称不能为空")
    @Length(max = 10, message = "员工名称的长度最大为10字符")
    @ApiModelProperty(value = "名称")
    private String name;

    @NotBlank(message = "联系电话不能为空")
    @Length(max = 20, message = "联系电话的长度最大为20字符")
    @ApiModelProperty(value = "联系电话")
    private String telephone;

    @ApiModelProperty(value = "市场部员工类型:1市场总监2区域经理3业务经理4.社区合伙人")
    private MarketEmployeeTypeEnum employeeType;

    @ApiModelProperty(value = "启用状态 0.未启用 1.启用 2.待审核")
    private UseStatusEnum useStatus;

    @ApiModelProperty(value = "默认分润比例")
    private BigDecimal defaultRate;

    @ApiModelProperty(value = "性别:1男2女")
    private SexEnum sex;

    @ApiModelProperty(value = "职位")
    private String post;

    @ApiModelProperty(value = "部门id")
    private String deptId;

    @ApiModelProperty(value = "角色")
    private String roleIds;

    @ApiModelProperty(value = "区域")
    private String areaSns;

    @Length(max = 255, message = "头像图片地址的长度最大为255字符")
    @ApiModelProperty(value = "头像图片地址")
    private String avatar;

    public MarketEmployee transTo() {
        MarketEmployee employee = new MarketEmployee();
        BeanUtil.copyProperties(this, employee);
        return employee;
    }

    public MarketEmployee copyOwnEdit() {
        // 只修改 名称、电话、性别 、职位
        MarketEmployee employee = new MarketEmployee();
        employee.setId(this.id);
        employee.setName(this.name);
        employee.setTelephone(this.getTelephone());
        employee.setSex(this.sex);
        employee.setPost(this.post);
        employee.setAvatar(this.avatar);
        return employee;
    }


}
