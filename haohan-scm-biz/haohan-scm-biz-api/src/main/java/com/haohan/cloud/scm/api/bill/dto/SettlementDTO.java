package com.haohan.cloud.scm.api.bill.dto;

import com.haohan.cloud.scm.api.constant.enums.bill.SettlementStyleEnum;
import com.haohan.cloud.scm.api.constant.enums.common.SettlementTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import com.haohan.cloud.scm.api.constant.enums.purchase.PayTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author dy
 * @date 2019/11/26
 */
@Data
@ApiModel("结算单信息")
public class SettlementDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private String id;

    @Length(max = 32, message = "结算单编号的最大长度为32字符")
    @ApiModelProperty(value = "结算单编号")
    private String settlementSn;
    /**
     * 平台商家id
     */
    private String pmId;
    /**
     * 平台商家名称
     */
    private String pmName;
    /**
     * 结算公司id: merchantId
     */
    private String companyId;
    /**
     * 结算公司名称
     */
    private String companyName;
    /**
     * 结算订单编号
     */
    private String orderSn;
    /**
     * 结算账单编号
     */
    private String billSn;

    // 结算相关
    /**
     * 结算类型: 应收/应付
     */
    private SettlementTypeEnum settlementType;
    /**
     * 结算金额
     */
    private BigDecimal settlementAmount;
    /**
     * 结算时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime settlementTime;
    /**
     * 付款方式:1对公转账2现金支付3在线支付4承兑汇票
     */
    private PayTypeEnum payType;
    /**
     * 是否结算:0否1是
     */
    private YesNoEnum settlementStatus;
    /**
     * 结算单类型: 普通, 汇总, 明细
     */
    private SettlementStyleEnum settlementStyle;
    /**
     * 汇总结算单编号
     */
    private String summarySettlementSn;

    // 记录信息

    @Length(max = 32, message = "结款人名称的最大长度为32字符")
    @ApiModelProperty(value = "结款人名称")
    private String companyOperator;
    /**
     * 结算凭证图片组编号
     */
    private String groupNum;

    @Length(max = 255, message = "结算说明的最大长度为255字符")
    @ApiModelProperty(value = "结算说明")
    private String settlementDesc;

    @Length(max = 32, message = "经办人名称的最大长度为32字符")
    @ApiModelProperty(value = "经办人名称")
    private String operatorName;

    // 扩展信息

    @Length(max = 32, message = "随机码的最大长度为32字符")
    @ApiModelProperty(value = "开始结算状态随机码（验证是否此次结算）")
    private String randomCode;

    @ApiModelProperty(value = "图片列表")
    private List<String> photoList;

}
