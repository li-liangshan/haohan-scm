/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.opc.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.constant.NumberPrefixConstant;
import com.haohan.cloud.scm.api.opc.entity.SummaryOrder;
import com.haohan.cloud.scm.common.tools.util.ScmIncrementUtil;
import com.haohan.cloud.scm.opc.mapper.SummaryOrderMapper;
import com.haohan.cloud.scm.opc.service.SummaryOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

/**
 * 采购单汇总
 *
 * @author haohan
 * @date 2019-05-13 20:32:23
 */
@Service
public class SummaryOrderServiceImpl extends ServiceImpl<SummaryOrderMapper, SummaryOrder> implements SummaryOrderService {

    @Autowired
    @Lazy
    private ScmIncrementUtil scmIncrementUtil;

    @Override
    public boolean save(SummaryOrder entity) {
        if (StrUtil.isEmpty(entity.getSummaryOrderId())) {
            String sn = scmIncrementUtil.inrcSnByClass(SummaryOrder.class, NumberPrefixConstant.SUMMARY_ORDER_SN_PRE);
            entity.setSummaryOrderId(sn);
        }
        return super.save(entity);
    }

}
