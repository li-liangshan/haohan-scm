package com.haohan.cloud.scm.api.crm.dto.analysis;

import com.haohan.cloud.scm.api.common.dto.SelfSqlDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

/**
 * @author dy
 * @date 2019/11/6
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Api("客户计数统计分析")
public class CustomerCountAnalysisDTO extends SelfSqlDTO {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "客户负责人id")
    private String directorId;

    @ApiModelProperty(value = "客户sn列表")
    private String customerSns;

    @ApiModelProperty(value = "业务员id列表")
    private String employeeIds;

    public CustomerCountAnalysisDTO(String directorId, LocalDate startDate, LocalDate endDate) {
        this.directorId = directorId;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public CustomerCountAnalysisDTO(String directorId) {
        this.directorId = directorId;
    }
}
