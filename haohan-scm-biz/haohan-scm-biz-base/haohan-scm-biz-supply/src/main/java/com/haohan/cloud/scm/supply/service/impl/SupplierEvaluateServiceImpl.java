/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.supply.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.supply.entity.SupplierEvaluate;
import com.haohan.cloud.scm.supply.mapper.SupplierEvaluateMapper;
import com.haohan.cloud.scm.supply.service.SupplierEvaluateService;
import org.springframework.stereotype.Service;

/**
 * 供应商评价记录
 *
 * @author haohan
 * @date 2019-05-13 17:44:49
 */
@Service
public class SupplierEvaluateServiceImpl extends ServiceImpl<SupplierEvaluateMapper, SupplierEvaluate> implements SupplierEvaluateService {

}
