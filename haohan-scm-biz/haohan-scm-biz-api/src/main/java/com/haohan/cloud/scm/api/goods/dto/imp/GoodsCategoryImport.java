package com.haohan.cloud.scm.api.goods.dto.imp;

import cn.hutool.core.util.StrUtil;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import com.haohan.cloud.scm.api.goods.dto.GoodsCategoryDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.math.BigDecimal;

/**
 * @author dy
 * @date 2019/10/16
 */
@Data
public class GoodsCategoryImport {

    @NotBlank(message = "分类名称不能为空")
    @Length(min = 0, max = 10, message = "分类名称的字符长度必须在1至10之间")
    @ApiModelProperty(value = "分类名称")
    private String name;

    @NotBlank(message = "上级分类名称不能为空")
    @Length(min = 0, max = 10, message = "上级分类名称的字符长度必须在1至10之间")
    @ApiModelProperty(value = "上级分类名称")
    private String parentName;

    @Length(min = 0, max = 100, message = "分类描述的字符长度必须在0至100之间")
    @ApiModelProperty(value = "分类描述")
    private String description;

    @Pattern(regexp = "[0-9]{0,10}", message = "排序为正整数，位数不大于10")
    @ApiModelProperty(value = "排序")
    private String sort;

    @Length(min = 0, max = 255, message = "图片地址的字符长度必须在0至255之间")
    @ApiModelProperty(value = "图片地址")
    private String logo;

    public GoodsCategoryDTO transTo() {
        GoodsCategoryDTO category = new GoodsCategoryDTO();
        category.setName(this.name);
        category.setParentName(this.parentName);
        category.setDescription(this.description);
        category.setSort(StrUtil.isEmpty(this.sort) ? BigDecimal.TEN : new BigDecimal(this.sort));
        category.setLogo(this.logo);

        category.setCategoryType(YesNoEnum.no);
        category.setStatus(YesNoEnum.no);
        return category;
    }
}
