/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.salec.feign;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.salec.entity.StoreProductAttrValue;
import com.haohan.cloud.scm.api.salec.req.StoreProductAttrValueReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 商品属性值表内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "StoreProductAttrValueFeignService", value = ScmServiceName.SCM_BIZ_SALEC)
public interface StoreProductAttrValueFeignService {


  /**
   * 通过id查询商品属性值表
   *
   * @param suk id
   * @return R
   */
  @RequestMapping(value = "/api/feign/StoreProductAttrValue/{suk}", method = RequestMethod.GET)
  R<StoreProductAttrValue> getById(@PathVariable("suk") String suk, @RequestHeader(SecurityConstants.FROM) String from);


  /**
   * 分页查询 商品属性值表 列表信息
   *
   * @param storeProductAttrValueReq 请求对象
   * @return
   */
  @PostMapping("/api/feign/StoreProductAttrValue/fetchStoreProductAttrValuePage")
  R<Page<StoreProductAttrValue>> getStoreProductAttrValuePage(@RequestBody StoreProductAttrValueReq storeProductAttrValueReq, @RequestHeader(SecurityConstants.FROM) String from);


  /**
   * 全量查询 商品属性值表 列表信息
   *
   * @param storeProductAttrValueReq 请求对象
   * @return
   */
  @PostMapping("/api/feign/StoreProductAttrValue/fetchStoreProductAttrValueList")
  R<List<StoreProductAttrValue>> getStoreProductAttrValueList(@RequestBody StoreProductAttrValueReq storeProductAttrValueReq, @RequestHeader(SecurityConstants.FROM) String from);

  /**
   * 新增商品属性值表
   *
   * @param storeProductAttrValue 商品属性值表
   * @return R
   */
  @PostMapping("/api/feign/StoreProductAttrValue/add")
  R<Boolean> save(@RequestBody StoreProductAttrValue storeProductAttrValue, @RequestHeader(SecurityConstants.FROM) String from);


  /**
   * 修改商品属性值表
   *
   * @param storeProductAttrValue 商品属性值表
   * @return R
   */
  @PostMapping("/api/feign/StoreProductAttrValue/update")
  R<Boolean> updateById(@RequestBody StoreProductAttrValue storeProductAttrValue, @RequestHeader(SecurityConstants.FROM) String from);


  /**
   * 通过id删除商品属性值表
   *
   * @param suk id
   * @return R
   */
  @PostMapping("/api/feign/StoreProductAttrValue/delete/{suk}")
  R<Boolean> removeById(@PathVariable("suk") String suk, @RequestHeader(SecurityConstants.FROM) String from);

  /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
  @PostMapping("/api/feign/StoreProductAttrValue/batchDelete")
  R<Boolean> removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


  /**
   * 批量查询（根据IDS）
   *
   * @param idList 主键ID列表
   * @return R
   */
  @PostMapping("/api/feign/StoreProductAttrValue/listByIds")
  R<List<StoreProductAttrValue>> listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


  /**
   * 根据 Wrapper 条件，查询总记录数
   *
   * @param storeProductAttrValueReq 实体对象,可以为空
   * @return R
   */
  @PostMapping("/api/feign/StoreProductAttrValue/countByStoreProductAttrValueReq")
  R<Integer> countByStoreProductAttrValueReq(@RequestBody StoreProductAttrValueReq storeProductAttrValueReq, @RequestHeader(SecurityConstants.FROM) String from);


  /**
   * 根据对象条件，查询一条记录
   *
   * @param storeProductAttrValueReq 实体对象,可以为空
   * @return R
   */
  @PostMapping("/api/feign/StoreProductAttrValue/getOneByStoreProductAttrValueReq")
  R<StoreProductAttrValue> getOneByStoreProductAttrValueReq(@RequestBody StoreProductAttrValueReq storeProductAttrValueReq, @RequestHeader(SecurityConstants.FROM) String from);


  /**
   * 批量修改OR插入
   *
   * @param storeProductAttrValueList 实体对象集合 大小不超过1000条数据
   * @return R
   */
  @PostMapping("/api/feign/StoreProductAttrValue/saveOrUpdateBatch")
  R<Boolean> saveOrUpdateBatch(@RequestBody List<StoreProductAttrValue> storeProductAttrValueList, @RequestHeader(SecurityConstants.FROM) String from);


}
