/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.wms.req;

import com.haohan.cloud.scm.api.wms.entity.Shelf;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 货架信息表
 *
 * @author haohan
 * @date 2019-05-28 19:12:48
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "货架信息表")
public class ShelfReq extends Shelf {

    private long pageSize;
    private long pageNo;




}
