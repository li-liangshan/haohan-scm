package com.haohan.cloud.scm.api.constant.converter.goods;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.goods.GoodsTypeEnum;

/**
 * @author dy
 * @date 2019/10/8
 */
public class GoodsTypeEnumConverterUtil implements Converter<GoodsTypeEnum> {
    @Override
    public GoodsTypeEnum convert(Object o, GoodsTypeEnum e) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return GoodsTypeEnum.getByType(o.toString());
    }
}
