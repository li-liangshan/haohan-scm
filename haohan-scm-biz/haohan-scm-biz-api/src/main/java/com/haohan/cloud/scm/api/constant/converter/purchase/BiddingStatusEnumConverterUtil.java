package com.haohan.cloud.scm.api.constant.converter.purchase;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.purchase.BiddingStatusEnum;

/**
 * @author xwx
 * @date 2019/6/3
 */
public class BiddingStatusEnumConverterUtil implements Converter<BiddingStatusEnum> {
    @Override
    public BiddingStatusEnum convert(Object o, BiddingStatusEnum biddingStatusEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return BiddingStatusEnum.getByType(o.toString());
    }
}
