/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.supply.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.supply.entity.SupplierGrade;
import com.haohan.cloud.scm.supply.mapper.SupplierGradeMapper;
import com.haohan.cloud.scm.supply.service.SupplierGradeService;
import org.springframework.stereotype.Service;

/**
 * 供应商评级记录
 *
 * @author haohan
 * @date 2019-05-13 17:45:14
 */
@Service
public class SupplierGradeServiceImpl extends ServiceImpl<SupplierGradeMapper, SupplierGrade> implements SupplierGradeService {

}
