package com.haohan.cloud.scm.api.purchase.trans;

import com.haohan.cloud.scm.api.constant.enums.purchase.MethodTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.supply.PdsOfferTypeEnum;

/**
 * @author xwx
 * @date 2019/6/5
 */
public class PurchaseEnumTrans {

    /**
     * MethodTypeEnum 转换为 PdsOfferTypeEnum
     */
    public static PdsOfferTypeEnum change(MethodTypeEnum methodType) {
        if (methodType == MethodTypeEnum.bidding) {
            return PdsOfferTypeEnum.bazaarOffer;
        }
        if (methodType == MethodTypeEnum.agreement) {
            return PdsOfferTypeEnum.appointOffer;
        }
        // methodType==MethodTypeEnum.single
        return PdsOfferTypeEnum.platformOffer;
    }
}
