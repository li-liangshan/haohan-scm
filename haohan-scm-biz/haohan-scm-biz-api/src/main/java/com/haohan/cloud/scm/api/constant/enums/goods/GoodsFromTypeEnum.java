package com.haohan.cloud.scm.api.constant.enums.goods;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dy
 * @date 2019/10/8
 */
@Getter
@AllArgsConstructor
public enum GoodsFromTypeEnum implements IBaseEnum {

    /**
     * 商品来源 0.小店平台 1.即速应用
     */
    platform("0", "小店平台"),
    jisuapp("1", "即速应用");

    private static final Map<String, GoodsFromTypeEnum> MAP = new HashMap<>(8);

    static {
        for (GoodsFromTypeEnum e : GoodsFromTypeEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static GoodsFromTypeEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

    @EnumValue
    @JsonValue
    private String type;
    private String desc;
}
