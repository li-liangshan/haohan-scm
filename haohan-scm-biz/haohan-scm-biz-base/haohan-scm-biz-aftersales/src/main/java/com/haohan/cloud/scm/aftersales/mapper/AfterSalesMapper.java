/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.aftersales.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haohan.cloud.scm.api.aftersales.entity.AfterSales;

/**
 * 售后单
 *
 * @author haohan
 * @date 2019-05-13 20:29:17
 */
public interface AfterSalesMapper extends BaseMapper<AfterSales> {

}
