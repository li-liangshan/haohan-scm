package com.haohan.cloud.scm.api.crm.vo.app;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author dy
 * @date 2020/1/18
 */
@Data
public class AccountSettlementVO {

    @ApiModelProperty(value = "已收金额")
    private BigDecimal receivableAmount;

    public AccountSettlementVO(){
        this.receivableAmount = BigDecimal.ONE;
    }

}
