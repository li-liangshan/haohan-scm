/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.salec.req;

import com.haohan.cloud.scm.api.salec.entity.StoreProductCate;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 产品分类辅助表
 *
 * @author haohan
 * @date 2019-06-19 17:42:13
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "产品分类辅助表")
public class StoreProductCateReq extends StoreProductCate {

  private long pageSize;
  private long pageNo;


}
