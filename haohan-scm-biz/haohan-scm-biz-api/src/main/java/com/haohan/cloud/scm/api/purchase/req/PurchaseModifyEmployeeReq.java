package com.haohan.cloud.scm.api.purchase.req;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author dy
 * @date 2019/5/16
 */
@Data
public class PurchaseModifyEmployeeReq {
    @NotBlank(message = "pmId不能为空")
    private String pmId;
    @NotBlank(message = "id不能为空")
    private String id;

    /**
     * 名称
     */
    @Length(min = 0, max = 20, message = "name长度在0-20个字符")
    private String name;
    /**
     * 联系电话
     */
    @Length(min = 0, max = 11, message = "telephone长度在0-11个字符")
    private String telephone;
    /**
     * 采购员工类型:1采购总监2采购经理3采购员
     */
    private String purchaseEmployeeType;

    /**
     * 启用状态:0.未启用1.启用
     */
    private String useStatus;

    @Length(min = 0, max = 20, message = "uid长度在0-20个字符")
    private String passportId;

    @Length(min = 0, max = 20, message = "userId长度在0-20个字符")
    private String userId;


}
