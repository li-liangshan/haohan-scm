/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.goods.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.saleb.req.GoodsCollectionsReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 收藏商品内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "GoodsCollectionsFeignService", value = ScmServiceName.SCM_GOODS)
public interface GoodsCollectionsFeignService {


    /**
     * 通过id查询收藏商品
     *
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/GoodsCollections/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 收藏商品 列表信息
     *
     * @param goodsCollectionsReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/GoodsCollections/fetchGoodsCollectionsPage")
    R getGoodsCollectionsPage(@RequestBody GoodsCollectionsReq goodsCollectionsReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 收藏商品 列表信息
     *
     * @param goodsCollectionsReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/GoodsCollections/fetchGoodsCollectionsList")
    R getGoodsCollectionsList(@RequestBody GoodsCollectionsReq goodsCollectionsReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/GoodsCollections/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param goodsCollectionsReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/GoodsCollections/countByGoodsCollectionsReq")
    R countByGoodsCollectionsReq(@RequestBody GoodsCollectionsReq goodsCollectionsReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param goodsCollectionsReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/GoodsCollections/getOneByGoodsCollectionsReq")
    R getOneByGoodsCollectionsReq(@RequestBody GoodsCollectionsReq goodsCollectionsReq, @RequestHeader(SecurityConstants.FROM) String from);


    @PostMapping("/api/feign/GoodsCollections/addCollections")
    R<Boolean> addCollections(@RequestBody GoodsCollectionsReq query, @RequestHeader(SecurityConstants.FROM) String from);

    @PostMapping("/api/feign/GoodsCollections/removeCollections")
    R<Boolean> removeCollections(@RequestBody GoodsCollectionsReq query, @RequestHeader(SecurityConstants.FROM) String from);

}
