package com.haohan.cloud.scm.saleb.api;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.haohan.cloud.scm.api.opc.entity.SummaryOrder;
import com.haohan.cloud.scm.api.saleb.dto.BuyOrderDetailDTO;
import com.haohan.cloud.scm.api.saleb.entity.BuyOrder;
import com.haohan.cloud.scm.api.saleb.req.SalebBuyOrderDetailQueryReq;
import com.haohan.cloud.scm.api.saleb.req.SalebBuyOrderQueryReq;
import com.haohan.cloud.scm.api.saleb.req.SalebSummaryGoodsNumReq;
import com.haohan.cloud.scm.api.saleb.trans.SalebTrans;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.haohan.cloud.scm.saleb.core.ISalebBuyOrderService;
import com.haohan.cloud.scm.saleb.service.BuyOrderDetailService;
import com.haohan.cloud.scm.saleb.service.BuyOrderService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author dy
 * @date 2019/5/27
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/saleb")
@Api(value = "ApiSalebFeign", tags = "SalebFeign B端客户内部调用")
public class SalebFeignApiCtrl {

    private final BuyOrderService buyOrderService;
    private final BuyOrderDetailService buyOrderDetailService;
    private final ISalebBuyOrderService salebBuyOrderService;

    /**
     * 查询 客户采购单
     * 通过 id buyId
     *
     * @param req
     * @return
     */
    @Inner
    @PostMapping("buyOrder/queryOne")
    public R queryOne(@RequestBody SalebBuyOrderQueryReq req) {
        BuyOrder buyOrder = new BuyOrder();
        buyOrder.setId(req.getId());
        buyOrder.setBuyId(req.getBuyId());
        BuyOrder b = buyOrderService.getOne(Wrappers.query(buyOrder));
        return new R<>(b);
    }


    /**
     * 查询 采购单明细 关联采购单
     * 通过 id sn 等
     *
     * @param req
     * @return
     */
    @Inner
    @PostMapping("buyOrderDetail/queryOne")
    public R queryOne(@RequestBody SalebBuyOrderDetailQueryReq req) {
        BuyOrderDetailDTO detail = new BuyOrderDetailDTO();
        BeanUtil.copyProperties(req, detail);
        BuyOrderDetailDTO b = buyOrderDetailService.getOneWithOrder(detail);
        if (null == b) {
            return RUtil.error().setMsg("找不到对应信息");
        }
        return new R<>(b);
    }

    /**
     * 查询 采购单明细列表 关联采购单
     * 采购单 参数: buySeq/deliveryTime
     * 明细 参数: summaryFlag/status/buyId/buyDetailSn/pmId/id
     *
     * @param req
     * @return
     */
    @Inner
    @PostMapping("buyOrderDetail/queryList")
    public R queryList(@RequestBody SalebBuyOrderDetailQueryReq req) {
        BuyOrderDetailDTO detail = new BuyOrderDetailDTO();
        BeanUtil.copyProperties(req, detail);
        List<BuyOrderDetailDTO> list = buyOrderDetailService.getListWithOrder(detail);
        if (CollUtil.isEmpty(list)) {
            return RUtil.error().setMsg("列表为空");
        }
        return new R<>(list);
    }

    /**
     * 查询 汇总的商品下单数量 单个商品
     * 通过 商品规格id  送货日期 批次
     *
     * @param req 所有参数都必须
     * @return
     */
    @Inner
    @PostMapping("buyOrderDetail/summaryGoodsNum")
    public R summaryGoodsNum(@RequestBody SalebSummaryGoodsNumReq req) {
        BuyOrderDetailDTO detail = new BuyOrderDetailDTO();
        // 参数处理
        SalebTrans.transToBuyOrderDetailDTO(detail, req);
        BuyOrderDetailDTO resp = buyOrderDetailService.summaryGoodsNum(detail);
        if (null == resp) {
            return RUtil.error().setMsg("找不到需汇总商品数据");
        }
        // 存入 汇总数据
        SummaryOrder summaryOrder = new SummaryOrder();
        SalebTrans.transToSummaryOrder(resp, summaryOrder);
        return new R<>(summaryOrder);
    }

    /**
     * 查询汇总单的采购总数量
     *
     * @param req
     * @return
     */
    @Inner
    @PostMapping("buyOrderDetail/summaryGoodsNumList")
    public R<List<BuyOrderDetailDTO>> summaryGoodsNumList(@RequestBody SalebSummaryGoodsNumReq req) {
        BuyOrderDetailDTO buyOrderDetailDTO = new BuyOrderDetailDTO();
        // 参数处理
        SalebTrans.transToBuyOrderDetailDTO(buyOrderDetailDTO, req);
        //查询汇总单列表
        List<BuyOrderDetailDTO> list = buyOrderDetailService.summaryGoodsNumList(buyOrderDetailDTO);
        return new R<>(list);
    }

    /**
     * 确认 采购单的所有明细已成交 修改采购单状态为已成交
     *
     * @param buyId
     * @return
     */
    @Inner
    @PostMapping("buyOrder/confirmBuyOrder")
    public R<Boolean> confirmBuyOrder(@RequestBody String buyId) {
        R<Boolean> r = new R<>();
        return r.setData(salebBuyOrderService.confirmBuyOrder(buyId));
    }

}
