package com.haohan.cloud.scm.api.goods.req;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.goods.req.manage.GoodsListReq;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * @author dy
 * @date 2020/4/17
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class GoodsFeignReq extends GoodsListReq {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "通行证id")
    private String uid;

    @ApiModelProperty(value = "商品规格id")
    private String goodsModelId;

    @ApiModelProperty(value = "分页参数 当前页")
    private long current = 1;

    @ApiModelProperty(value = "分页参数 每页显示条数")
    private long size = 10;

    public void setPage(Page page) {
        if (null != page) {
            this.current = page.getCurrent();
            this.size = page.getSize();
        }
    }
}
