/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.message.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.message.entity.MessageRecord;
import com.haohan.cloud.scm.message.mapper.MessageRecordMapper;
import com.haohan.cloud.scm.message.service.MessageRecordService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 消息发送记录
 *
 * @author haohan
 * @date 2019-05-13 18:34:27
 */
@Service
public class MessageRecordServiceImpl extends ServiceImpl<MessageRecordMapper, MessageRecord> implements MessageRecordService {


    @Override
    public MessageRecord fetchBySn(String messageSn) {
        List<MessageRecord> list = baseMapper.selectList(Wrappers.<MessageRecord>query().lambda()
                .eq(MessageRecord::getMessageSn, messageSn)
        );
        return CollUtil.isEmpty(list) ? null : list.get(0);
    }
}
