package com.haohan.cloud.scm.goods.core;

/**
 * @author dy
 * @date 2020/5/27
 */
public interface GoodsCollectionsCoreService {

    /**
     * 新增收藏
     *
     * @param uid
     * @param goodsId
     * @return
     */
    boolean addCollections(String uid, String goodsId);

    /**
     * 取消收藏
     *
     * @param uid
     * @param goodsId
     * @return
     */
    boolean removeCollections(String uid, String goodsId);

}
