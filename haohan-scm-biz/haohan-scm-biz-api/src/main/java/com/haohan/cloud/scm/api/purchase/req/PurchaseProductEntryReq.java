package com.haohan.cloud.scm.api.purchase.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @author xwx
 * @date 2019/7/8
 */
@Data
@ApiModel(description = "采购明细货品 实际入库")
public class PurchaseProductEntryReq {
    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台商家id", required = true)
    private String pmId;

    @NotBlank(message = "purchaseDetailSn不能为空")
    @ApiModelProperty(value = "采购单明细编号" , required = true)
    private String purchaseDetailSn;

    @NotNull(message = "realBuyNum不能为空")
    @ApiModelProperty(value = "实际入库数量" , required = true)
    private BigDecimal realBuyNum;

    @NotBlank(message = "taskId不能为空")
    @ApiModelProperty(value = "采购任务id")
    private String taskId;
}
