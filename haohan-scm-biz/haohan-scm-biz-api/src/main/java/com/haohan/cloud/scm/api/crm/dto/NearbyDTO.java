package com.haohan.cloud.scm.api.crm.dto;

import com.haohan.cloud.scm.api.common.dto.SelfSqlDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author dy
 * @date 2019/10/24
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel("附近客户、市场")
public class NearbyDTO extends SelfSqlDTO {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("距离")
    private Integer distance;

    @ApiModelProperty(value = "经度")
    private String longitude;

    @ApiModelProperty(value = "纬度")
    private String latitude;

    @ApiModelProperty(value = "客户筛选列表,逗号连接")
    private String customerSns;

    @ApiModelProperty(value = "区域筛选列表,逗号连接")
    private String areaSns;

}
