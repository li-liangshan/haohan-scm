/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.wms.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.wms.entity.ShelfManagement;
import com.haohan.cloud.scm.api.wms.req.ShelfManagementReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 货品上下架记录内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "ShelfManagementFeignService", value = ScmServiceName.SCM_BIZ_WMS)
public interface ShelfManagementFeignService {


    /**
     * 通过id查询货品上下架记录
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/ShelfManagement/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 货品上下架记录 列表信息
     * @param shelfManagementReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/ShelfManagement/fetchShelfManagementPage")
    R getShelfManagementPage(@RequestBody ShelfManagementReq shelfManagementReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 货品上下架记录 列表信息
     * @param shelfManagementReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/ShelfManagement/fetchShelfManagementList")
    R getShelfManagementList(@RequestBody ShelfManagementReq shelfManagementReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增货品上下架记录
     * @param shelfManagement 货品上下架记录
     * @return R
     */
    @PostMapping("/api/feign/ShelfManagement/add")
    R save(@RequestBody ShelfManagement shelfManagement, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改货品上下架记录
     * @param shelfManagement 货品上下架记录
     * @return R
     */
    @PostMapping("/api/feign/ShelfManagement/update")
    R updateById(@RequestBody ShelfManagement shelfManagement, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除货品上下架记录
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/ShelfManagement/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/ShelfManagement/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/ShelfManagement/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param shelfManagementReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/ShelfManagement/countByShelfManagementReq")
    R countByShelfManagementReq(@RequestBody ShelfManagementReq shelfManagementReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param shelfManagementReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/ShelfManagement/getOneByShelfManagementReq")
    R getOneByShelfManagementReq(@RequestBody ShelfManagementReq shelfManagementReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param shelfManagementList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/ShelfManagement/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<ShelfManagement> shelfManagementList, @RequestHeader(SecurityConstants.FROM) String from);


}
