/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.purchase.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseReport;
import com.haohan.cloud.scm.api.purchase.req.PurchaseAnswerPurchaseReportReq;
import com.haohan.cloud.scm.api.purchase.req.PurchaseModifyPurchaseReportReq;
import com.haohan.cloud.scm.api.purchase.req.PurchaseReportReq;
import com.haohan.cloud.scm.purchase.core.IScmPurchaseMarketReportService;
import com.haohan.cloud.scm.purchase.service.PurchaseReportService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 采购汇报
 *
 * @author haohan
 * @date 2019-05-29 13:35:21
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/PurchaseReport")
@Api(value = "purchasereport", tags = "purchasereport内部接口服务")
public class PurchaseReportFeignApiCtrl {

    private final PurchaseReportService purchaseReportService;
    private final IScmPurchaseMarketReportService marketReportService;


    /**
     * 通过id查询采购汇报
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(purchaseReportService.getById(id));
    }


    /**
     * 分页查询 采购汇报 列表信息
     * @param purchaseReportReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchPurchaseReportPage")
    public R getPurchaseReportPage(@RequestBody PurchaseReportReq purchaseReportReq) {
        Page page = new Page(purchaseReportReq.getPageNo(), purchaseReportReq.getPageSize());
        PurchaseReport purchaseReport =new PurchaseReport();
        BeanUtil.copyProperties(purchaseReportReq, purchaseReport);

        return new R<>(purchaseReportService.page(page, Wrappers.query(purchaseReport)));
    }


    /**
     * 全量查询 采购汇报 列表信息
     * @param purchaseReportReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchPurchaseReportList")
    public R getPurchaseReportList(@RequestBody PurchaseReportReq purchaseReportReq) {
        PurchaseReport purchaseReport =new PurchaseReport();
        BeanUtil.copyProperties(purchaseReportReq, purchaseReport);

        return new R<>(purchaseReportService.list(Wrappers.query(purchaseReport)));
    }


    /**
     * 新增采购汇报
     * @param purchaseReport 采购汇报
     * @return R
     */
    @Inner
    @SysLog("新增采购汇报")
    @PostMapping("/add")
    public R save(@RequestBody PurchaseReport purchaseReport) {
        return new R<>(purchaseReportService.save(purchaseReport));
    }

    /**
     * 修改采购汇报
     * @param purchaseReport 采购汇报
     * @return R
     */
    @Inner
    @SysLog("修改采购汇报")
    @PostMapping("/update")
    public R updateById(@RequestBody PurchaseReport purchaseReport) {
        return new R<>(purchaseReportService.updateById(purchaseReport));
    }

    /**
     * 通过id删除采购汇报
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除采购汇报")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(purchaseReportService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除采购汇报")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(purchaseReportService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询采购汇报")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(purchaseReportService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param purchaseReportReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询采购汇报总记录}")
    @PostMapping("/countByPurchaseReportReq")
    public R countByPurchaseReportReq(@RequestBody PurchaseReportReq purchaseReportReq) {

        return new R<>(purchaseReportService.count(Wrappers.query(purchaseReportReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param purchaseReportReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据purchaseReportReq查询一条货位信息表")
    @PostMapping("/getOneByPurchaseReportReq")
    public R getOneByPurchaseReportReq(@RequestBody PurchaseReportReq purchaseReportReq) {

        return new R<>(purchaseReportService.getOne(Wrappers.query(purchaseReportReq), false));
    }


    /**
     * 批量修改OR插入
     * @param purchaseReportList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<PurchaseReport> purchaseReportList) {

        return new R<>(purchaseReportService.saveOrUpdateBatch(purchaseReportList));
    }
    /**
     * 修改 采购汇报
     *
     * @param req
     * @return
     */
    @Inner
    @SysLog("修改 采购汇报")
    @PostMapping("/modifyPurchaseReport")
    public R modifyPurchaseReport(@RequestBody PurchaseModifyPurchaseReportReq req) {
        return new R<>(marketReportService.modifyPurchaseReport(req));
    }
    /**
     * 回复采购汇报
     *
     * @param req
     * @return
     */
    @Inner
    @SysLog("回复采购汇报")
    @PostMapping("/answerPurchaseReport")
    public R answerPurchaseReport(@RequestBody PurchaseAnswerPurchaseReportReq req) {
        return new R<>(marketReportService.answerPurchaseReport(req));
    }

}
