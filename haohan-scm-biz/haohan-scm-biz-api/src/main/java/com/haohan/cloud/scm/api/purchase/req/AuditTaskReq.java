package com.haohan.cloud.scm.api.purchase.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;

/**
 * @author cx
 * @date 2019/6/10
 */
@Data
@ApiModel(description = "总监批量审核采购任务需求")
public class AuditTaskReq {

  @NotBlank(message = "id不能为空")
  @ApiModelProperty(value = "采购任务id", required = true)
  private String id;

  @NotBlank(message = "purchaseStatus不能为空")
  @ApiModelProperty(value = "采购状态1.待处理2.待审核3.采购中4.采购完成5.部分完成6.已关闭", required = true)
  private String purchaseStatus;

  @NotBlank(message = "methodType不能为空")
  @ApiModelProperty(value = "采购方式类型1.竞价采购2.单品采购3.协议供应", required = true)
  private String methodType;

  @ApiModelProperty(value = "需求采购数量")
  private BigDecimal needBuyNum;

  @ApiModelProperty(value = "用户id")
  private String uId;

}
