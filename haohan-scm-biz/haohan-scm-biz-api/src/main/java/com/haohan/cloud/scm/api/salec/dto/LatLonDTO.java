package com.haohan.cloud.scm.api.salec.dto;

import lombok.Data;

/**
 * @author cx
 * @date 2019/7/16
 */

@Data
public class LatLonDTO {
    private String Latitude;

    private String Longitude;
}
