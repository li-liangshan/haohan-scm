/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.wms.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.wms.entity.Shelf;
import com.haohan.cloud.scm.api.wms.req.ShelfReq;
import com.haohan.cloud.scm.wms.service.ShelfService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 货架信息表
 *
 * @author haohan
 * @date 2019-05-29 13:22:52
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/Shelf")
@Api(value = "shelf", tags = "shelf内部接口服务")
public class ShelfFeignApiCtrl {

    private final ShelfService shelfService;


    /**
     * 通过id查询货架信息表
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(shelfService.getById(id));
    }


    /**
     * 分页查询 货架信息表 列表信息
     * @param shelfReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchShelfPage")
    public R getShelfPage(@RequestBody ShelfReq shelfReq) {
        Page page = new Page(shelfReq.getPageNo(), shelfReq.getPageSize());
        Shelf shelf =new Shelf();
        BeanUtil.copyProperties(shelfReq, shelf);

        return new R<>(shelfService.page(page, Wrappers.query(shelf)));
    }


    /**
     * 全量查询 货架信息表 列表信息
     * @param shelfReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchShelfList")
    public R getShelfList(@RequestBody ShelfReq shelfReq) {
        Shelf shelf =new Shelf();
        BeanUtil.copyProperties(shelfReq, shelf);

        return new R<>(shelfService.list(Wrappers.query(shelf)));
    }


    /**
     * 新增货架信息表
     * @param shelf 货架信息表
     * @return R
     */
    @Inner
    @SysLog("新增货架信息表")
    @PostMapping("/add")
    public R save(@RequestBody Shelf shelf) {
        return new R<>(shelfService.save(shelf));
    }

    /**
     * 修改货架信息表
     * @param shelf 货架信息表
     * @return R
     */
    @Inner
    @SysLog("修改货架信息表")
    @PostMapping("/update")
    public R updateById(@RequestBody Shelf shelf) {
        return new R<>(shelfService.updateById(shelf));
    }

    /**
     * 通过id删除货架信息表
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除货架信息表")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(shelfService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除货架信息表")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(shelfService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询货架信息表")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(shelfService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param shelfReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询货架信息表总记录}")
    @PostMapping("/countByShelfReq")
    public R countByShelfReq(@RequestBody ShelfReq shelfReq) {

        return new R<>(shelfService.count(Wrappers.query(shelfReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param shelfReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据shelfReq查询一条货位信息表")
    @PostMapping("/getOneByShelfReq")
    public R getOneByShelfReq(@RequestBody ShelfReq shelfReq) {

        return new R<>(shelfService.getOne(Wrappers.query(shelfReq), false));
    }


    /**
     * 批量修改OR插入
     * @param shelfList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<Shelf> shelfList) {

        return new R<>(shelfService.saveOrUpdateBatch(shelfList));
    }

}
