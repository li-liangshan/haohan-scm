package com.haohan.cloud.scm.api.purchase.trans;

import com.haohan.cloud.scm.api.constant.enums.common.TaskStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.purchase.TaskActionTypeEnum;
import com.haohan.cloud.scm.api.opc.entity.SummaryOrder;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseOrder;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseOrderDetail;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseTask;

public class PurchaseInfoTrans {

    public static PurchaseOrderDetail trans(SummaryOrder order){
        PurchaseOrderDetail detail = new PurchaseOrderDetail();
        return detail;
    }

    /**
     * 初始化采购任务 根据采购单明细
     * @return
     */

    public static PurchaseTask addPurchaseTask(PurchaseOrder order){
        PurchaseTask task = new PurchaseTask();
        task.setPmId(order.getPmId());
        task.setPurchaseSn(order.getPurchaseSn());
        task.setTaskActionType(TaskActionTypeEnum.audit);
        task.setTaskStatus(TaskStatusEnum.wait);
        task.setDeadlineTime(order.getBuyFinalTime());
        return task;
    }
}
