/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.product.req;

import com.haohan.cloud.scm.api.product.entity.SortingOrderDetail;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 分拣单明细
 *
 * @author haohan
 * @date 2019-05-28 20:54:24
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "分拣单明细")
public class SortingOrderDetailReq extends SortingOrderDetail {

    private long pageSize;
    private long pageNo;




}
