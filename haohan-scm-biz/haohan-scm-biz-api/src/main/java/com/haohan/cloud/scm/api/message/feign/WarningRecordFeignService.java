/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.message.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.message.entity.WarningRecord;
import com.haohan.cloud.scm.api.message.req.WarningRecordReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 预警记录表内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "WarningRecordFeignService", value = ScmServiceName.SCM_BIZ_MSG)
public interface WarningRecordFeignService {


    /**
     * 通过id查询预警记录表
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/WarningRecord/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 预警记录表 列表信息
     * @param warningRecordReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/WarningRecord/fetchWarningRecordPage")
    R getWarningRecordPage(@RequestBody WarningRecordReq warningRecordReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 预警记录表 列表信息
     * @param warningRecordReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/WarningRecord/fetchWarningRecordList")
    R getWarningRecordList(@RequestBody WarningRecordReq warningRecordReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增预警记录表
     * @param warningRecord 预警记录表
     * @return R
     */
    @PostMapping("/api/feign/WarningRecord/add")
    R save(@RequestBody WarningRecord warningRecord, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改预警记录表
     * @param warningRecord 预警记录表
     * @return R
     */
    @PostMapping("/api/feign/WarningRecord/update")
    R updateById(@RequestBody WarningRecord warningRecord, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除预警记录表
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/WarningRecord/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/WarningRecord/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/WarningRecord/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param warningRecordReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/WarningRecord/countByWarningRecordReq")
    R countByWarningRecordReq(@RequestBody WarningRecordReq warningRecordReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param warningRecordReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/WarningRecord/getOneByWarningRecordReq")
    R getOneByWarningRecordReq(@RequestBody WarningRecordReq warningRecordReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param warningRecordList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/WarningRecord/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<WarningRecord> warningRecordList, @RequestHeader(SecurityConstants.FROM) String from);


}
