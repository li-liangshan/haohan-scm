/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.tms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.tms.entity.LogisticsEmployee;


/**
 * 物流部员工管理
 *
 * @author haohan
 * @date 2019-06-05 09:26:11
 */
public interface LogisticsEmployeeService extends IService<LogisticsEmployee> {

}
