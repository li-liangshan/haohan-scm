/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.tms.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.tms.entity.LogisticsEmployee;
import com.haohan.cloud.scm.tms.mapper.LogisticsEmployeeMapper;
import com.haohan.cloud.scm.tms.service.LogisticsEmployeeService;
import org.springframework.stereotype.Service;

/**
 * 物流部员工管理
 *
 * @author haohan
 * @date 2019-06-05 09:26:11
 */
@Service
public class LogisticsEmployeeServiceImpl extends ServiceImpl<LogisticsEmployeeMapper, LogisticsEmployee> implements LogisticsEmployeeService {

}
