package com.haohan.cloud.scm.api.crm.dto.imp;

import com.haohan.cloud.scm.api.constant.enums.common.ReviewStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.crm.PayStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.crm.SalesGoodsTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.crm.SalesTypeEnum;
import com.haohan.cloud.scm.api.crm.dto.SalesOrderDTO;
import com.haohan.cloud.scm.api.crm.entity.MarketEmployee;
import com.haohan.cloud.scm.api.crm.entity.SalesOrderDetail;
import com.haohan.cloud.scm.api.crm.trans.CrmOrderTrans;
import com.haohan.cloud.scm.api.goods.dto.GoodsModelDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 * @author dy
 * @date 2019/10/12
 */
@Data
public class SalesOrderImport {

    @Length(min = 0, max = 32, message = "外部订单号的字符长度必须在0至32之间")
    @ApiModelProperty(value = "外部订单号")
    private String externalSn;

    @Length(min = 0, max = 32, message = "客户编码的字符长度必须在0至32之间")
    @ApiModelProperty(value = "客户编码")
    private String customerSn;

    @Length(min = 0, max = 32, message = "客户名称的字符长度必须在1至32之间")
    @ApiModelProperty(value = "客户名称")
    private String customerName;

    @Length(min = 0, max = 32, message = "供货商编码的字符长度必须在0至32之间")
    @ApiModelProperty(value = "供货商编码")
    private String supplierSn;

    @Length(min = 0, max = 32, message = "供货商名称的字符长度必须在0至32之间")
    @ApiModelProperty(value = "供货商名称")
    private String supplierName;

    @NotNull(message = "下单日期不能为空")
    @ApiModelProperty(value = "下单日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate orderTime;

    @NotBlank(message = "业务员不能为空")
    @Length(min = 0, max = 32, message = "业务员的字符长度必须在1至32之间")
    @ApiModelProperty(value = "业务员名称")
    private String employeeName;

    @NotNull(message = "交货日期不能为空")
    @ApiModelProperty(value = "交货日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate deliveryDate;

    @NotBlank(message = "收货人名称不能为空")
    @Length(min = 0, max = 32, message = "收货人名称的字符长度必须在1至32之间")
    @ApiModelProperty(value = "收货人名称")
    private String linkmanName;

    @NotBlank(message = "收货人手机号不能为空")
    @Length(min = 0, max = 20, message = "收货人手机号的字符长度必须在1至20之间")
    @ApiModelProperty(value = "收货人手机号")
    private String telephone;

    @NotBlank(message = "收货地址不能为空")
    @Length(min = 0, max = 64, message = "收货地址的字符长度必须在1至64之间")
    @ApiModelProperty(value = "收货地址")
    private String address;

    @Length(min = 0, max = 64, message = "订单备注的字符长度必须在0至64之间")
    @ApiModelProperty(value = "订单备注")
    private String remarks;

    @Length(min = 0, max = 32, message = "商品规格编码的字符长度必须在1至32之间")
    @ApiModelProperty(value = "商品规格编码")
    private String goodsModelSn;

    @Length(min = 0, max = 32, message = "商品名称的字符长度必须在0至32之间")
    @ApiModelProperty(value = "商品名称")
    private String goodsName;

    @Length(min = 0, max = 32, message = "商品规格名称的字符长度必须在0至32之间")
    @ApiModelProperty(value = "商品规格名称")
    private String modelName;

    @NotBlank(message = "销售单位不能为空")
    @Length(min = 0, max = 10, message = "销售单位的字符长度必须在0至10之间")
    @ApiModelProperty(value = "销售单位")
    private String unit;

    @NotNull(message = "销售单价不能为空")
    @Digits(integer = 8, fraction = 2, message = "销售单价的整数位最大8位, 小数位最大2位")
    @Range(min = 0, max = 1000000, message = "销售单价的大小必须在0至1000000之间")
    @ApiModelProperty(value = "销售单价")
    private BigDecimal dealPrice;

    @NotNull(message = "销售数量不能为空")
    @Digits(integer = 8, fraction = 2, message = "销售数量的整数位最大8位, 小数位最大2位")
    @Range(min = 0, max = 1000000, message = "销售数量的大小必须在0至1000000之间")
    @ApiModelProperty(value = "销售数量")
    private BigDecimal goodsNum;

    @ApiModelProperty(value = "金额")
    private BigDecimal amount;

    @Length(min = 0, max = 64, message = "商品行备注的字符长度必须在0至64之间")
    @ApiModelProperty(value = "商品行备注")
    private String detailRemarks;

    public SalesOrderDTO initSalesOrder(MarketEmployee employee) {
        SalesOrderDTO order = new SalesOrderDTO();
        order.setCustomerSn(this.customerSn);
        order.setCustomerName(this.customerName);
        order.setOrderTime(this.orderTime.atStartOfDay());
        order.setDeliveryDate(this.deliveryDate);
        // 金额 商品种类数 无
        order.setSalesType(SalesTypeEnum.self);
        order.setEmployeeId(employee.getId());
        order.setEmployeeName(employee.getName());
        // 联系人id 无
        order.setLinkmanName(this.linkmanName);
        order.setAddress(this.address);
        order.setTelephone(this.telephone);
        order.setReviewStatus(ReviewStatusEnum.wait);
        order.setPayStatus(PayStatusEnum.wait);
        order.setExternalSn(this.externalSn);
        order.setRemarks(this.remarks);
        order.setDetailList(new ArrayList<>());
        return order;
    }

    public SalesOrderDetail initOrderDetail(GoodsModelDTO model) {
        SalesOrderDetail detail = new SalesOrderDetail();
        detail.setCustomerSn(this.customerSn);
        detail.setSalesGoodsType(CrmOrderTrans.fetchTypeByPrice(this.dealPrice, model));
        detail.setGoodsModelId(model.getId());
        detail.setGoodsModelSn(model.getGoodsModelSn());
        detail.setGoodsImg(model.getModelUrl());
        detail.setGoodsName(model.getGoodsName());
        detail.setModelName(model.getModelName());
        detail.setGoodsNum(this.goodsNum);
        detail.setMarketPrice(model.getModelPrice());
        detail.setDealPrice(this.dealPrice);
        detail.setUnit(model.getModelUnit());
        detail.setAmount(this.amount != null && this.amount.compareTo(BigDecimal.ZERO) > 0 ? this.amount : this.goodsNum.multiply(this.dealPrice));
        detail.setRemarks(this.detailRemarks);
        return detail;
    }
}
