/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.message.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.message.entity.MessageRecord;
import com.haohan.cloud.scm.api.message.req.MessageRecordReq;
import com.haohan.cloud.scm.api.message.req.QueryMessageReq;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.haohan.cloud.scm.message.core.MessageCoreService;
import com.haohan.cloud.scm.message.service.MessageRecordService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 消息发送记录
 *
 * @author haohan
 * @date 2019-05-29 13:48:28
 */
@RestController
@AllArgsConstructor
@RequestMapping("/messagerecord" )
@Api(value = "messagerecord", tags = "messagerecord管理")
public class MessageRecordController {

    private final MessageRecordService messageRecordService;
    private final MessageCoreService messageCoreService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param req 消息发送记录
     * @return
     */
    @GetMapping("/page" )
    public R getMessageRecordPage(Page<MessageRecord> page, QueryMessageReq req) {
        return RUtil.success(messageCoreService.findPage(page, req));
    }


    /**
     * 通过id查询消息发送记录
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return RUtil.success(messageRecordService.getById(id));
    }

    /**
     * 新增消息发送记录
     * @param messageRecord 消息发送记录
     * @return R
     */
    @SysLog("新增消息发送记录" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_messagerecord_add')" )
    public R save(@RequestBody MessageRecord messageRecord) {
        return RUtil.success(messageRecordService.save(messageRecord));
    }

    /**
     * 修改消息发送记录
     * @param messageRecord 消息发送记录
     * @return R
     */
    @SysLog("修改消息发送记录" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_messagerecord_edit')" )
    public R updateById(@RequestBody MessageRecord messageRecord) {
        return RUtil.success(messageRecordService.updateById(messageRecord));
    }

    /**
     * 通过id删除消息发送记录
     * @param id id
     * @return R
     */
    @SysLog("删除消息发送记录" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_messagerecord_del')" )
    public R removeById(@PathVariable String id) {
        return RUtil.success(messageRecordService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除消息发送记录")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_messagerecord_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return RUtil.success(messageRecordService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询消息发送记录")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return RUtil.success(messageRecordService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param messageRecordReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询消息发送记录总记录}")
    @PostMapping("/countByMessageRecordReq")
    public R countByMessageRecordReq(@RequestBody MessageRecordReq messageRecordReq) {

        return RUtil.success(messageRecordService.count(Wrappers.query(messageRecordReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param messageRecordReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据messageRecordReq查询一条货位信息表")
    @PostMapping("/getOneByMessageRecordReq")
    public R getOneByMessageRecordReq(@RequestBody MessageRecordReq messageRecordReq) {

        return RUtil.success(messageRecordService.getOne(Wrappers.query(messageRecordReq), false));
    }


    /**
     * 批量修改OR插入
     * @param messageRecordList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_messagerecord_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<MessageRecord> messageRecordList) {

        return RUtil.success(messageRecordService.saveOrUpdateBatch(messageRecordList));
    }


}
