package com.haohan.cloud.scm.api.crm.req.analysis;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author dy
 * @date 2019/11/7
 */
@Data
public class BusinessAppReq {

    @NotBlank(message = "员工id不能为空")
    @Length(max = 20, message = "员工id字符长度在0至20之间")
    @ApiModelProperty(value = "员工id")
    private String employeeId;

}
