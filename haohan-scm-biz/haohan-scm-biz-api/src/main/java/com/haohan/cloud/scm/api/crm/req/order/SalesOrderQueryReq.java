package com.haohan.cloud.scm.api.crm.req.order;

import com.haohan.cloud.scm.api.constant.enums.common.ReviewStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.crm.PayStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.crm.SalesTypeEnum;
import com.haohan.cloud.scm.api.crm.entity.SalesOrder;
import com.haohan.cloud.scm.common.tools.constant.SingleGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import java.time.LocalDate;

/**
 * @author dy
 * @date 2019/12/3
 */
@Data
@ApiModel("销售订单查询")
public class SalesOrderQueryReq {

    @Length(max = 64, message = "客户名称字符长度在0至64之间")
    @ApiModelProperty(value = "客户名称")
    private String customerName;

    @Length(max = 10, message = "业务员名称字符长度在0至10之间")
    @ApiModelProperty(value = "业务员名称")
    private String employeeName;

    @Length(max = 10, message = "收货人名称字符长度在0至10之间")
    @ApiModelProperty(value = "收货人名称")
    private String linkmanName;

    @Length(max = 64, message = "供货商名称字符长度在0至64之间")
    @ApiModelProperty(value = "供货商名称")
    private String supplierName;

    @Length(max = 32, message = "销售订单编号字符长度在0至32之间")
    @ApiModelProperty(value = "销售订单编号")
    private String salesOrderSn;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "交货日期-开时时间")
    private LocalDate startDate;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "交货日期-结束时间")
    private LocalDate endDate;

    // eq参数

    @Length(max = 32, message = "客户编号字符长度在0至32之间")
    @ApiModelProperty(value = "客户编号")
    private String customerSn;

    @NotBlank(message = "业务员id不能为空", groups = {SingleGroup.class})
    @Length(max = 32, message = "业务员id字符长度在0至32之间")
    @ApiModelProperty(value = "业务员id")
    private String employeeId;

    @Length(max = 32, message = "收货人id字符长度在0至32之间")
    @ApiModelProperty(value = "收货人id")
    private String linkmanId;

    @Length(max = 32, message = "供货商编号字符长度在0至32之间")
    @ApiModelProperty(value = "供货商编号")
    private String supplierSn;

    @ApiModelProperty(value = "销售单类型:1代客下单,2自主下单")
    private SalesTypeEnum salesType;

    @ApiModelProperty(value = "审核状态: 1.待审核2.审核不通过3.审核通过")
    private ReviewStatusEnum reviewStatus;

    @ApiModelProperty(value = "订单支付状态:0未付,1已付,2部分支付")
    private PayStatusEnum payStatus;

    public SalesOrder transTo() {
        SalesOrder order = new SalesOrder();
        // eq查询参数
        order.setSalesOrderSn(this.salesOrderSn);
        order.setCustomerSn(this.customerSn);
        order.setEmployeeId(this.employeeId);
        order.setLinkmanId(this.linkmanId);
        order.setSupplierSn(this.supplierSn);
        order.setSalesType(this.salesType);
        order.setReviewStatus(this.reviewStatus);
        order.setPayStatus(this.payStatus);
        return order;
    }
}
