package com.haohan.cloud.scm.api.purchase.trans;

import com.haohan.cloud.scm.api.constant.enums.purchase.BiddingStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.purchase.PurchaseStatusEnum;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseOrderDetail;
import com.haohan.cloud.scm.api.supply.entity.OfferOrder;

/**
 * @author cx
 * @date 2019/6/11
 */
public class PurchaseOrderSupplierTrans {

  public static PurchaseOrderDetail purchaseOrderSupplierTrans(OfferOrder order){
    PurchaseOrderDetail detail = new PurchaseOrderDetail();
    detail.setBiddingStatus(BiddingStatusEnum.alreadyBidding);
    detail.setSupplierId(order.getSupplierId());
    detail.setSupplierName(order.getSupplierName());
    detail.setBuyPrice(order.getSupplyPrice());
    detail.setReceiveType(order.getReceiveType());
    detail.setPurchaseStatus(PurchaseStatusEnum.stockUp);
    return detail;
  }
}
