package com.haohan.cloud.scm.api.salec.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author cx
 * @date 2019/6/21
 */
@Data
@ApiModel(description = "spu商品详情信息")
public class ValueResultDTO {

    @ApiModelProperty(value = "spu商品规格信息")
    List<AttrDTO> attr;

    @ApiModelProperty(value = "商品信息详情")
    List<ValueDTO> value;
}
