package com.haohan.cloud.scm.api.common.req.admin;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @author shenyu
 * @create 2018/12/14
 */
public class PdsDateSeqBaseApiReq extends PdsBaseApiReq {
    @NotNull(message = "lack of parameter deliveryTime")
    private Date deliveryTime;
    @NotBlank(message = "lack of parameter buySeq")
    private String buySeq;

    public Date getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(Date deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public String getBuySeq() {
        return buySeq;
    }

    public void setBuySeq(String buySeq) {
        this.buySeq = buySeq;
    }
}
