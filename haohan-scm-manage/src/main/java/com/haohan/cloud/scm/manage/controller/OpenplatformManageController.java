/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.manage.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.manage.entity.OpenplatformManage;
import com.haohan.cloud.scm.api.manage.req.OpenplatformManageReq;
import com.haohan.cloud.scm.manage.service.OpenplatformManageService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 开放平台应用资料管理
 *
 * @author haohan
 * @date 2019-05-29 14:27:27
 */
@RestController
@AllArgsConstructor
@RequestMapping("/openplatformmanage" )
@Api(value = "openplatformmanage", tags = "openplatformmanage管理")
public class OpenplatformManageController {

    private final OpenplatformManageService openplatformManageService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param openplatformManage 开放平台应用资料管理
     * @return
     */
    @GetMapping("/page" )
    public R getOpenplatformManagePage(Page page, OpenplatformManage openplatformManage) {
        return new R<>(openplatformManageService.page(page, Wrappers.query(openplatformManage)));
    }


    /**
     * 通过id查询开放平台应用资料管理
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(openplatformManageService.getById(id));
    }

    /**
     * 新增开放平台应用资料管理
     * @param openplatformManage 开放平台应用资料管理
     * @return R
     */
    @SysLog("新增开放平台应用资料管理" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_openplatformmanage_add')" )
    public R save(@RequestBody OpenplatformManage openplatformManage) {
        return new R<>(openplatformManageService.save(openplatformManage));
    }

    /**
     * 修改开放平台应用资料管理
     * @param openplatformManage 开放平台应用资料管理
     * @return R
     */
    @SysLog("修改开放平台应用资料管理" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_openplatformmanage_edit')" )
    public R updateById(@RequestBody OpenplatformManage openplatformManage) {
        return new R<>(openplatformManageService.updateById(openplatformManage));
    }

    /**
     * 通过id删除开放平台应用资料管理
     * @param id id
     * @return R
     */
    @SysLog("删除开放平台应用资料管理" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_openplatformmanage_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(openplatformManageService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除开放平台应用资料管理")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_openplatformmanage_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(openplatformManageService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询开放平台应用资料管理")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(openplatformManageService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param openplatformManageReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询开放平台应用资料管理总记录}")
    @PostMapping("/countByOpenplatformManageReq")
    public R countByOpenplatformManageReq(@RequestBody OpenplatformManageReq openplatformManageReq) {

        return new R<>(openplatformManageService.count(Wrappers.query(openplatformManageReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param openplatformManageReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据openplatformManageReq查询一条货位信息表")
    @PostMapping("/getOneByOpenplatformManageReq")
    public R getOneByOpenplatformManageReq(@RequestBody OpenplatformManageReq openplatformManageReq) {

        return new R<>(openplatformManageService.getOne(Wrappers.query(openplatformManageReq), false));
    }


    /**
     * 批量修改OR插入
     * @param openplatformManageList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_openplatformmanage_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<OpenplatformManage> openplatformManageList) {

        return new R<>(openplatformManageService.saveOrUpdateBatch(openplatformManageList));
    }


}
