/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.purchase.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseEmployee;
import com.haohan.cloud.scm.purchase.mapper.PurchaseEmployeeMapper;
import com.haohan.cloud.scm.purchase.service.PurchaseEmployeeService;
import org.springframework.stereotype.Service;

/**
 * 采购员工管理
 *
 * @author haohan
 * @date 2019-05-13 18:48:18
 */
@Service
public class PurchaseEmployeeServiceImpl extends ServiceImpl<PurchaseEmployeeMapper, PurchaseEmployee> implements PurchaseEmployeeService {
}
