/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.saleb.feign;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.bill.dto.OrderInfoDTO;
import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.saleb.entity.BuyOrder;
import com.haohan.cloud.scm.api.saleb.req.BuyOrderFeignReq;
import com.haohan.cloud.scm.api.saleb.req.BuyOrderReq;
import com.haohan.cloud.scm.api.saleb.req.CountBuyOrderReq;
import com.haohan.cloud.scm.api.saleb.req.UpdateBuyOrderStatusReq;
import com.haohan.cloud.scm.api.saleb.req.order.BuyOrderEditReq;
import com.haohan.cloud.scm.api.saleb.vo.BuyOrderVO;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 采购单内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "BuyOrderFeignService", value = ScmServiceName.SCM_BIZ_SALEB)
public interface BuyOrderFeignService {


//  /**
//   * 通过id查询采购单
//   *
//   * @param id id
//   * @return R
//   */
//  @RequestMapping(value = "/api/feign/buyOrder/{id}", method = RequestMethod.GET)
//  R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


//  /**
//   * 分页查询 采购单 列表信息
//   *
//   * @param buyOrderReq 请求对象
//   * @return
//   */
//  @PostMapping("/api/feign/buyOrder/fetchBuyOrderPage")
//  R getBuyOrderPage(@RequestBody BuyOrderReq buyOrderReq, @RequestHeader(SecurityConstants.FROM) String from);


  /**
   * 全量查询 采购单 列表信息
   *
   * @param buyOrderReq 请求对象
   * @return
   */
  @PostMapping("/api/feign/buyOrder/fetchBuyOrderList")
  R getBuyOrderList(@RequestBody BuyOrderReq buyOrderReq, @RequestHeader(SecurityConstants.FROM) String from);

  /**
   * 新增采购单  (salec迁移后废弃)
   *
   * @param buyOrder 采购单
   * @return R
   */
  @PostMapping("/api/feign/buyOrder/add")
  R save(@RequestBody BuyOrder buyOrder, @RequestHeader(SecurityConstants.FROM) String from);


//  /**
//   * 修改采购单
//   *
//   * @param buyOrder 采购单
//   * @return R
//   */
//  @PostMapping("/api/feign/buyOrder/update")
//  R updateById(@RequestBody BuyOrder buyOrder, @RequestHeader(SecurityConstants.FROM) String from);


//  /**
//   * 通过id删除采购单
//   *
//   * @param id id
//   * @return R
//   */
//  @PostMapping("/api/feign/buyOrder/delete/{id}")
//  R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

//  /**
//   * 删除（根据ID 批量删除)
//   *
//   * @param idList 主键ID列表
//   * @return R
//   */
//  @PostMapping("/api/feign/buyOrder/batchDelete")
//  R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


//  /**
//   * 批量查询（根据IDS）
//   *
//   * @param idList 主键ID列表
//   * @return R
//   */
//  @PostMapping("/api/feign/buyOrder/listByIds")
//  R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


//  /**
//   * 根据 Wrapper 条件，查询总记录数
//   *
//   * @param buyOrderReq 实体对象,可以为空
//   * @return R
//   */
//  @PostMapping("/api/feign/buyOrder/countByBuyOrderReq")
//  R countByBuyOrderReq(@RequestBody BuyOrderReq buyOrderReq, @RequestHeader(SecurityConstants.FROM) String from);


  /**
   * 根据对象条件，查询一条记录
   *
   * @param buyOrderReq 实体对象,可以为空
   * @return R
   */
  @PostMapping("/api/feign/buyOrder/getOneByBuyOrderReq")
  R getOneByBuyOrderReq(@RequestBody BuyOrderReq buyOrderReq, @RequestHeader(SecurityConstants.FROM) String from);


//  /**
//   * 批量修改OR插入
//   *
//   * @param buyOrderList 实体对象集合 大小不超过1000条数据
//   * @return R
//   */
//  @PostMapping("/api/feign/buyOrder/saveOrUpdateBatch")
//  R saveOrUpdateBatch(@RequestBody List<BuyOrder> buyOrderList, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 修改B订单及明细的状态   (迁移后废弃)
     *
     * @return R
     */
    @PostMapping("/api/feign/buyOrder/updateBuyOrderStatus")
    R<Boolean> updateBuyOrderStatus(@RequestBody UpdateBuyOrderStatusReq req, @RequestHeader(SecurityConstants.FROM) String from);



  /**
   * 查找指定时间段b订单
   *
   * @param buyOrderReq
   * @param from
   * @return
   */
  @PostMapping("/api/feign/buyOrder/queryBuyOrderByTime")
  R<List> queryBuyOrderByTime(@RequestBody BuyOrderReq buyOrderReq, @RequestHeader(SecurityConstants.FROM) String from);



  /**
   * 查询区域对应的订单总数和订单总金额
   *
   * @param buyOrderReq
   * @return
   */
  @PostMapping("/api/feign/buyOrder/fetchAreaOrderOverView")
  R getAreaOrderOverView(@RequestBody BuyOrderReq buyOrderReq, @RequestHeader(SecurityConstants.FROM) String from);

  /**
   * 查询区域对应的订单总数和订单总金额
   *
   * @param countBuyOrderReq
   * @return
   */
  @PostMapping("/api/feign/buyOrder/queryAreaOrderOverView")
  R<List> queryAreaOrderOverView(@RequestBody CountBuyOrderReq countBuyOrderReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 获取订单详细
     *
     * @param orderSn
     * @param from
     * @return
     */
    @GetMapping("/api/feign/buyOrder/orderInfo")
    R<OrderInfoDTO> fetchOrderIfo(@RequestParam("orderSn") String orderSn, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 分页查询  buyOrder联查buyer 返回商家
     * @param query
     * @param from
     * @return
     */
    @PostMapping("/api/feign/buyOrder/findExtPage")
    R<Page<OrderInfoDTO>> findExtPage(@RequestBody BuyOrderFeignReq query, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询  buyOrder 带明细列表
     * @param query
     * @param from
     * @return
     */
    @PostMapping("/api/feign/buyOrder/findPage")
    R<Page<BuyOrderVO>> findPage(@RequestBody BuyOrderFeignReq query, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 采购单详情
     * @param buyOrderSn
     * @param from
     * @return
     */
    @GetMapping("/api/feign/buyOrder/fetchInfo")
    R<BuyOrderVO> fetchInfo(@RequestParam("buyOrderSn") String buyOrderSn, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 采购单下单
     * @param query
     * @param from
     * @return 有来源订单号的 订单已转换过采购单时 返回null
     */
    @PostMapping("/api/feign/buyOrder/addOrder")
    R<BuyOrderVO> addBuyOrder(@RequestBody BuyOrderEditReq query, @RequestHeader(SecurityConstants.FROM) String from);
    /**
     * 采购单修改
     * @param query
     * @param from
     * @return
     */
    @PostMapping("/api/feign/buyOrder/modifyOrder")
    R<Boolean> modifyBuyOrder(@RequestBody BuyOrderEditReq query, @RequestHeader(SecurityConstants.FROM) String from);

    @PostMapping("/api/feign/buyOrder/cancelOrder")
    R<Boolean> cancelBuyOrder(@RequestParam("buyOrderSn") String buyOrderSn, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 订单结算状态修改
     *
     * @param req
     * @param from
     * @return
     */
    @PostMapping("/api/feign/buyOrder/updateOrderSettlement")
    R<Boolean> updateOrderSettlement(@RequestBody BuyOrderFeignReq req, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 采购订单状态改变(不包括已取消)
     * 采购单状态：1.已下单2.待确认3.成交4.取消  5.待发货 6.待收货
     *
     * @param req  buyOrderSn、status(修改后状态)
     * @param from
     * @return
     */
    @PostMapping("/api/feign/buyOrder/updateOrderStatus")
    R<Boolean> updateOrderStatus(@RequestBody BuyOrderFeignReq req, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 查询采购商常购买的商品规格id列表 (销量最高的30个)
     * @param req
     * @param from
     * @return
     */
    @PostMapping("/api/feign/buyOrder/queryOftenModelIds")
    R<List<String>> queryOftenModelIds(@RequestBody BuyOrderFeignReq req, @RequestHeader(SecurityConstants.FROM) String from);
}
