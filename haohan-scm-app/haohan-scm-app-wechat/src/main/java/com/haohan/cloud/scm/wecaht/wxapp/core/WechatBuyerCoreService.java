package com.haohan.cloud.scm.wecaht.wxapp.core;

import com.haohan.cloud.scm.api.saleb.vo.BuyerPayVO;
import com.haohan.cloud.scm.api.wechat.req.WxBuyerStatusReq;

/**
 * @author dy
 * @date 2020/5/26
 */
public interface WechatBuyerCoreService {

    boolean buyerStatus(WxBuyerStatusReq req);

    BuyerPayVO buyerPayInfo(WxBuyerStatusReq req);

}
