package com.haohan.cloud.scm.api.order;

import com.haohan.cloud.scm.api.bill.dto.OrderInfoDTO;
import com.haohan.cloud.scm.api.constant.enums.crm.PayStatusEnum;

/**
 * @author dy
 * @date 2020/3/6
 * 各种订单的通用方法
 */
public interface OrderCommonService {


    /**
     * 根据订单编号查询订单详情
     *
     * @param orderSn
     * @return
     */
    OrderInfoDTO fetchOrderInfo(String orderSn);


    /**
     * 订单结算状态修改
     *
     * @param orderSn
     * @return
     */
    boolean updateOrderSettlement(String orderSn, PayStatusEnum status);

    /**
     * 订单完成发货
     *
     * @param orderSn
     */
    void orderCompleteShip(String orderSn);
}
