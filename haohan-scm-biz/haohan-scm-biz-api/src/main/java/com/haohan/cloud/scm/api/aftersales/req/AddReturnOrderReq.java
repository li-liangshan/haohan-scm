package com.haohan.cloud.scm.api.aftersales.req;

import com.haohan.cloud.scm.api.purchase.entity.PurchaseOrder;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseOrderDetail;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author cx
 * @date 2019/8/6
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class AddReturnOrderReq extends PurchaseOrder {

    private List<PurchaseOrderDetail> detailList;

    private String returnType;

    private BigDecimal otherAmount;

    /**
     * 退货图片id列表
     */
    @ApiModelProperty(value = "退货图片id列表")
    private List<String> photoList;
}
