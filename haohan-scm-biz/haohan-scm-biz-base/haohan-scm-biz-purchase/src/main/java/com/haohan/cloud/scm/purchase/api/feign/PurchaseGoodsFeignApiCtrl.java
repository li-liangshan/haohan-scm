/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.purchase.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseGoods;
import com.haohan.cloud.scm.api.purchase.req.PurchaseGoodsReq;
import com.haohan.cloud.scm.purchase.service.PurchaseGoodsService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 采购员工管理商品
 *
 * @author haohan
 * @date 2019-05-29 13:34:42
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/PurchaseGoods")
@Api(value = "purchasegoods", tags = "purchasegoods内部接口服务")
public class PurchaseGoodsFeignApiCtrl {

    private final PurchaseGoodsService purchaseGoodsService;


    /**
     * 通过id查询采购员工管理商品
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(purchaseGoodsService.getById(id));
    }


    /**
     * 分页查询 采购员工管理商品 列表信息
     * @param purchaseGoodsReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchPurchaseGoodsPage")
    public R getPurchaseGoodsPage(@RequestBody PurchaseGoodsReq purchaseGoodsReq) {
        Page page = new Page(purchaseGoodsReq.getPageNo(), purchaseGoodsReq.getPageSize());
        PurchaseGoods purchaseGoods =new PurchaseGoods();
        BeanUtil.copyProperties(purchaseGoodsReq, purchaseGoods);

        return new R<>(purchaseGoodsService.page(page, Wrappers.query(purchaseGoods)));
    }


    /**
     * 全量查询 采购员工管理商品 列表信息
     * @param purchaseGoodsReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchPurchaseGoodsList")
    public R getPurchaseGoodsList(@RequestBody PurchaseGoodsReq purchaseGoodsReq) {
        PurchaseGoods purchaseGoods =new PurchaseGoods();
        BeanUtil.copyProperties(purchaseGoodsReq, purchaseGoods);

        return new R<>(purchaseGoodsService.list(Wrappers.query(purchaseGoods)));
    }


    /**
     * 新增采购员工管理商品
     * @param purchaseGoods 采购员工管理商品
     * @return R
     */
    @Inner
    @SysLog("新增采购员工管理商品")
    @PostMapping("/add")
    public R save(@RequestBody PurchaseGoods purchaseGoods) {
        return new R<>(purchaseGoodsService.save(purchaseGoods));
    }

    /**
     * 修改采购员工管理商品
     * @param purchaseGoods 采购员工管理商品
     * @return R
     */
    @Inner
    @SysLog("修改采购员工管理商品")
    @PostMapping("/update")
    public R updateById(@RequestBody PurchaseGoods purchaseGoods) {
        return new R<>(purchaseGoodsService.updateById(purchaseGoods));
    }

    /**
     * 通过id删除采购员工管理商品
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除采购员工管理商品")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(purchaseGoodsService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除采购员工管理商品")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(purchaseGoodsService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询采购员工管理商品")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(purchaseGoodsService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param purchaseGoodsReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询采购员工管理商品总记录}")
    @PostMapping("/countByPurchaseGoodsReq")
    public R countByPurchaseGoodsReq(@RequestBody PurchaseGoodsReq purchaseGoodsReq) {

        return new R<>(purchaseGoodsService.count(Wrappers.query(purchaseGoodsReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param purchaseGoodsReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据purchaseGoodsReq查询一条货位信息表")
    @PostMapping("/getOneByPurchaseGoodsReq")
    public R getOneByPurchaseGoodsReq(@RequestBody PurchaseGoodsReq purchaseGoodsReq) {

        return new R<>(purchaseGoodsService.getOne(Wrappers.query(purchaseGoodsReq), false));
    }


    /**
     * 批量修改OR插入
     * @param purchaseGoodsList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<PurchaseGoods> purchaseGoodsList) {

        return new R<>(purchaseGoodsService.saveOrUpdateBatch(purchaseGoodsList));
    }

}
