/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.manage.req;

import com.haohan.cloud.scm.api.manage.entity.Industry;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 行业类型
 *
 * @author haohan
 * @date 2019-05-28 20:34:34
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "行业类型")
public class IndustryReq extends Industry {

    private long pageSize;
    private long pageNo;




}
