package com.haohan.cloud.scm.api.constant.converter.product;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.product.AllotStatusEnum;

/**
 * @author xwx
 * @date 2019/6/5
 */
public class AllotStatusEnumConverterUtil implements Converter<AllotStatusEnum> {
    @Override
    public AllotStatusEnum convert(Object o, AllotStatusEnum allotStatusEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return AllotStatusEnum.getByType(o.toString());
    }
}
