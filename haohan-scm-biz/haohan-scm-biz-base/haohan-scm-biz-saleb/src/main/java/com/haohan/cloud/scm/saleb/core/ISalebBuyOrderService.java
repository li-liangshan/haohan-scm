package com.haohan.cloud.scm.saleb.core;


import com.haohan.cloud.scm.api.saleb.entity.BuyOrder;
import com.haohan.cloud.scm.api.saleb.entity.BuyOrderDetail;
import com.haohan.cloud.scm.api.saleb.req.BuyOrderReq;
import com.haohan.cloud.scm.api.saleb.req.CancelBuyOrderReq;
import com.haohan.cloud.scm.api.saleb.req.CreateBuyOrderDetailReq;
import com.haohan.cloud.scm.api.saleb.req.UpdateBuyOrderStatusReq;
import com.haohan.cloud.scm.api.salec.resp.OrderMonitorResp;

import java.util.List;

/**
 * @author dy
 * @date 2019/6/14
 * 采购单相关
 */
public interface ISalebBuyOrderService {

    /**
     * 确认 采购单的所有明细已成交
     *
     * @param buyId
     * @return
     */
    Boolean confirmBuyOrder(String buyId);


    /**
     * 采购明细 判断库存备货 锁定库存
     * @param buyOrderDetail
     * @return
     */
    Boolean detailConfirmStock(BuyOrderDetail buyOrderDetail);

    /**
     * 批量修改订单明星
     * @param list
     * @return
     */
    Boolean updBuyOrderDetailList(List<BuyOrderDetail> list);

    /**
     * 取消订单
     * @param req
     * @return
     */
    Boolean cancelBuyOrder(CancelBuyOrderReq req);

    /**
     * 采购明细 判断库存备货 锁定库存(批量)
     * @param order
     * @return
     */
    Boolean detailConfirmStockList(BuyOrder order);

    /**
     * 修改采购单状态 为wait
     * @param pmId
     * @param minute 距下单时间的分钟数
     * @return
     */
    Integer buyOrderChangeStatusWait(String pmId, int minute);

    /**
     * 根据商品创建订单明细
     * @param req
     * @return
     */
    List<BuyOrderDetail> createBuyOrderDetail(CreateBuyOrderDetailReq req);

    /**
     * 查询b订单
     * @param req
     * @return
     */
    List<OrderMonitorResp> queryBuyOrderByTime(BuyOrderReq req);

    /**
     * 修改B订单及明细的状态
     * @param req
     * @return  执行失败抛出异常
     */
    void updateBuyOrderStatus(UpdateBuyOrderStatusReq req);

    /**
     * 订单确认收货 交易完成  pmId / buyId
     *   执行失败抛出异常
     * @param buyOrder
     */
    void confirmReceive(BuyOrder buyOrder);
}

