/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.saleb.req;

import com.haohan.cloud.scm.api.goods.entity.GoodsCollections;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 收藏商品
 *
 * @author haohan
 * @date 2019-05-29 13:30:26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "收藏商品")
public class GoodsCollectionsReq extends GoodsCollections {

    private long pageSize;
    private long pageNo;




}
