/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.goods.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.goods.entity.GoodsModelTotal;
import com.haohan.cloud.scm.goods.mapper.GoodsModelTotalMapper;
import com.haohan.cloud.scm.goods.service.GoodsModelTotalService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 商品规格名称
 *
 * @author haohan
 * @date 2019-05-13 18:46:29
 */
@Service
public class GoodsModelTotalServiceImpl extends ServiceImpl<GoodsModelTotalMapper, GoodsModelTotal> implements GoodsModelTotalService {


    @Override
    public List<GoodsModelTotal> fetchTotalList(String goodsId) {
        return baseMapper.selectList(Wrappers.<GoodsModelTotal>query().lambda()
                .eq(GoodsModelTotal::getGoodsId, goodsId));
    }
}
