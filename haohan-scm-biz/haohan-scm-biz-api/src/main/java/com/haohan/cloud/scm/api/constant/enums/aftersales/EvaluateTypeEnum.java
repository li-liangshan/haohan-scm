package com.haohan.cloud.scm.api.constant.enums.aftersales;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xwx
 * @date 2019/5/30
 */
@Getter
@AllArgsConstructor
public enum EvaluateTypeEnum {
    /**
     * 评分类型 1.产品讲解2.售后服务3.产品喜好
     */
    productExplanation("1","产品讲解"),
    afterSales("2","售后服务"),
    productPreferences("3","产品喜好");

    private static final Map<String, EvaluateTypeEnum> MAP = new HashMap<>(8);

    static {
        for (EvaluateTypeEnum e : EvaluateTypeEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static EvaluateTypeEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

    /**
     * 类型
     */
    @EnumValue
    @JsonValue
    private String type;
    /**
     * 描述
     */
    private String desc;
}
