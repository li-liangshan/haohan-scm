/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.saleb.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.bill.dto.OrderInfoDTO;
import com.haohan.cloud.scm.api.saleb.entity.BuyOrder;
import com.haohan.cloud.scm.api.saleb.req.BuyOrderFeignReq;
import com.haohan.cloud.scm.api.saleb.req.BuyOrderReq;
import com.haohan.cloud.scm.api.saleb.req.CountBuyOrderReq;
import com.haohan.cloud.scm.api.saleb.req.UpdateBuyOrderStatusReq;
import com.haohan.cloud.scm.api.saleb.req.order.BuyOrderEditReq;
import com.haohan.cloud.scm.api.saleb.resp.QueryOrderSumAndPriceSumResp;
import com.haohan.cloud.scm.api.saleb.vo.BuyOrderVO;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.haohan.cloud.scm.saleb.core.BuyOrderCoreService;
import com.haohan.cloud.scm.saleb.core.ISalebBuyOrderService;
import com.haohan.cloud.scm.saleb.service.BuyOrderService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 采购单
 *
 * @author haohan
 * @date 2019-05-29 13:29:53
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/buyOrder")
@Api(value = "buyorder", tags = "buyorder内部接口服务")
public class BuyOrderFeignApiCtrl {

    private final BuyOrderService buyOrderService;
    private final BuyOrderCoreService buyOrderCoreService;
    private final ISalebBuyOrderService salebBuyOrderService;

//  /**
//   * 通过id查询采购单
//   *
//   * @param id id
//   * @return R
//   */
//  @Inner
//  @GetMapping("/{id}")
//  public R getById(@PathVariable("id") String id) {
//    return new R<>(buyOrderService.getById(id));
//  }


//  /**
//   * 分页查询 采购单 列表信息
//   *
//   * @param buyOrderReq 请求对象
//   * @return
//   */
//  @Inner
//  @PostMapping("/fetchBuyOrderPage")
//  public R getBuyOrderPage(@RequestBody BuyOrderReq buyOrderReq) {
//    Page page = new Page(buyOrderReq.getPageNo(), buyOrderReq.getPageSize());
//    BuyOrder buyOrder = new BuyOrder();
//    BeanUtil.copyProperties(buyOrderReq, buyOrder);
//
//    return new R<>(buyOrderService.page(page, Wrappers.query(buyOrder)));
//  }

    @Inner
    @PostMapping("/findExtPage")
    @ApiOperation(value = "分页查询  buyOrder联查buyer 返回商家")
    public R<IPage<OrderInfoDTO>> findExtPage(@RequestBody BuyOrderFeignReq query) {
        return RUtil.success(buyOrderCoreService.findExtPage(new Page<>(query.getCurrent(), query.getSize()), query));
    }

    @Inner
    @PostMapping("/findPage")
    @ApiOperation(value = "分页查询  buyOrder带明细列表")
    public R<IPage<BuyOrderVO>> findPage(@RequestBody BuyOrderFeignReq query) {
        return RUtil.success(buyOrderCoreService.findPage(new Page<>(query.getCurrent(), query.getSize()), query, true));
    }

    @Inner
    @GetMapping("/fetchInfo")
    @ApiOperation(value = "通过sn查询采购单详情")
    public R<BuyOrderVO> fetchInfo(@RequestParam("buyOrderSn") String buyOrderSn) {
        return RUtil.success(buyOrderCoreService.fetchInfo(buyOrderSn));
    }

    @Inner
    @PostMapping("/addOrder")
    @ApiOperation(value = "采购单下单")
    public R<BuyOrderVO> addBuyOrder(@RequestBody BuyOrderEditReq query) {
        // 有来源订单号的验证是否已创建过采购单
        BuyOrder order = buyOrderService.fetchBySourceOrderId(query.getSourceOrderId());
        return null == order ? RUtil.success(buyOrderCoreService.addBuyOrder(query)) : R.failed("该订单已转换过采购单");
    }

    @Inner
    @PostMapping("/modifyOrder")
    @ApiOperation(value = "采购单修改")
    public R<Boolean> modifyBuyOrder(@RequestBody BuyOrderEditReq query) {
        return RUtil.success(buyOrderCoreService.modifyBuyOrder(query));
    }

    @Inner
    @PostMapping("/cancelOrder")
    @ApiOperation(value = "采购单取消")
    public R<Boolean> cancelBuyOrder(@RequestParam("buyOrderSn") String buyOrderSn) {
        return RUtil.success(buyOrderCoreService.cancelBuyOrder(buyOrderSn));
    }

  /**
   * 全量查询 采购单 列表信息
   *
   * @param buyOrderReq 请求对象
   * @return
   */
  @Inner
  @PostMapping("/fetchBuyOrderList")
  public R getBuyOrderList(@RequestBody BuyOrderReq buyOrderReq) {
    BuyOrder buyOrder = new BuyOrder();
    BeanUtil.copyProperties(buyOrderReq, buyOrder);

    return new R<>(buyOrderService.list(Wrappers.query(buyOrder)));
  }


  /**
   * 新增采购单  (salec迁移后废弃)
   *
   * @param buyOrder 采购单
   * @return R
   */
  @Inner
  @SysLog("新增采购单")
  @PostMapping("/add")
  public R save(@RequestBody BuyOrder buyOrder) {
    return new R<>(buyOrderService.save(buyOrder));
  }

//  /**
//   * 修改采购单
//   *
//   * @param buyOrder 采购单
//   * @return R
//   */
//  @Inner
//  @SysLog("修改采购单")
//  @PostMapping("/update")
//  public R updateById(@RequestBody BuyOrder buyOrder) {
//    return new R<>(buyOrderService.updateById(buyOrder));
//  }

//  /**
//   * 通过id删除采购单
//   *
//   * @param id id
//   * @return R
//   */
//  @Inner
//  @SysLog("删除采购单")
//  @PostMapping("/delete/{id}")
//  public R removeById(@PathVariable String id) {
//    return new R<>(buyOrderService.removeById(id));
//  }

//  /**
//   * 删除（根据ID 批量删除)
//   *
//   * @param idList 主键ID列表
//   * @return R
//   */
//  @Inner
//  @SysLog("批量删除采购单")
//  @PostMapping("/batchDelete")
//  public R removeByIds(@RequestBody List<String> idList) {
//    return new R<>(buyOrderService.removeByIds(idList));
//  }


//  /**
//   * 批量查询（根据IDS）
//   *
//   * @param idList 主键ID列表
//   * @return R
//   */
//  @Inner
//  @SysLog("根据IDS批量查询采购单")
//  @PostMapping("/listByIds")
//  public R listByIds(@RequestBody List<String> idList) {
//    return new R<>(buyOrderService.listByIds(idList));
//  }


//  /**
//   * 根据 Wrapper 条件，查询总记录数
//   *
//   * @param buyOrderReq 实体对象,可以为空
//   * @return R
//   */
//  @Inner
//  @SysLog("查询采购单总记录}")
//  @PostMapping("/countByBuyOrderReq")
//  public R countByBuyOrderReq(@RequestBody BuyOrderReq buyOrderReq) {
//
//    return new R<>(buyOrderService.count(Wrappers.query(buyOrderReq)));
//  }


  /**
   * 根据对象条件，查询一条记录
   *
   * @param buyOrderReq 实体对象,可以为空
   * @return R
   */
  @Inner
  @PostMapping("/getOneByBuyOrderReq")
  public R getOneByBuyOrderReq(@RequestBody BuyOrderReq buyOrderReq) {

    return new R<>(buyOrderService.getOne(Wrappers.query(buyOrderReq), false));
  }


//  /**
//   * 批量修改OR插入
//   *
//   * @param buyOrderList 实体对象集合 大小不超过1000条数据
//   * @return R
//   */
//  @Inner
//  @SysLog("批量修改OR插入货位信息表")
//  @PostMapping("/saveOrUpdateBatch")
//  public R saveOrUpdateBatch(@RequestBody List<BuyOrder> buyOrderList) {
//    return new R<>(buyOrderService.saveOrUpdateBatch(buyOrderList));
//  }

    /**
     * 修改B订单及明细的状态  (迁移后废弃)
     *
     * @param
     * @return R
     */
    @Inner
    @SysLog("修改B订单及明细的状态")
    @PostMapping("/updateBuyOrderStatus")
    public R<Boolean> updateBuyOrderStatus(@RequestBody UpdateBuyOrderStatusReq req) {
        try{
            salebBuyOrderService.updateBuyOrderStatus(req);
            return R.ok(true);
        }catch (Exception e){
            return R.ok(false, e.getMessage());
        }
    }

    @Inner
    @GetMapping("/orderInfo")
    @ApiOperation(value = "通过sn查询采购单详情")
    public R<OrderInfoDTO> fetchOrderIfo(@RequestParam("orderSn") String orderSn) {
        return RUtil.success(buyOrderCoreService.fetchOrderInfoWithExt(orderSn));
    }

    /**
   * 查询b订单列表
   *
   * @param buyOrderReq
   * @return
   */
  @Inner
  @PostMapping("/queryBuyOrderByTime")
  public R<List> queryBuyOrderByTime(@RequestBody BuyOrderReq buyOrderReq) {
    return new R<>(salebBuyOrderService.queryBuyOrderByTime(buyOrderReq));
  }

  @Inner
  @PostMapping("/fetchAreaOrderOverView")
  public R getAreaOrderOverView(@RequestBody BuyOrderReq buyOrderReq) {
    CountBuyOrderReq orderReq = new CountBuyOrderReq();
    orderReq.setPmId(buyOrderReq.getPmId());
    orderReq.setStartTime(buyOrderReq.getBeginCreateTime());
    orderReq.setEndTime(buyOrderReq.getEndCreateTime());
    return new R<>(buyOrderService.countBuyOrder(orderReq));
  }

  /**
   * 查询区域对应的订单总数和订单总金额
   *
   * @param countBuyOrderReq
   * @return
   */
  @Inner
  @PostMapping("/queryAreaOrderOverView")
  public R<List> queryAreaOrderOverView(@RequestBody CountBuyOrderReq countBuyOrderReq) {
    List<QueryOrderSumAndPriceSumResp> list = buyOrderService.countBuyOrder(countBuyOrderReq);
    return new R<>(list);
  }

    @Inner
    @SysLog("订单结算状态修改")
    @PostMapping("/updateOrderSettlement")
    public R<Boolean> updateOrderSettlement(@RequestBody BuyOrderFeignReq req) {
        return RUtil.success(buyOrderCoreService.updateOrderSettlement(req.getBuyOrderSn(), req.getPayStatus()));
    }

    /**
     * 采购订单状态改变(不包括已取消)
     * 采购单状态：1.已下单2.待确认3.成交4.取消  5.待发货 6.待收货
     *
     * @param req  buyOrderSn、status(修改后状态)
     * @return
     */
    @Inner
    @SysLog("采购订单状态改变")
    @PostMapping("/updateOrderStatus")
    public R<Boolean> updateOrderStatus(@RequestBody BuyOrderFeignReq req) {
        return RUtil.success(buyOrderCoreService.updateOrderStatus(req.getBuyOrderSn(), req.getStatus()));
    }

    /**
     * 查询采购商常购买的商品规格id列表(销量最高的30个)
     * @param req
     * @return
     */
    @Inner
    @PostMapping("/queryOftenModelIds")
    public R<List<String>> queryOftenModelIds(@RequestBody BuyOrderFeignReq req) {
        return RUtil.success(buyOrderCoreService.queryOftenModelIds(req.getBuyerId()));
    }
}
