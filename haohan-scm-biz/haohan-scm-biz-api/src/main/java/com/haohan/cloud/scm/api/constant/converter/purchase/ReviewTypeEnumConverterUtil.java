package com.haohan.cloud.scm.api.constant.converter.purchase;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.purchase.ReviewTypeEnum;

/**
 * @author cx
 * @date 2019/6/4
 */
public class ReviewTypeEnumConverterUtil implements Converter<ReviewTypeEnum> {
    @Override
    public ReviewTypeEnum convert(Object o, ReviewTypeEnum reviewTypeEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return ReviewTypeEnum.getByType(o.toString());
    }
}
