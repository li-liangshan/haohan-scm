/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.goods.req;

import com.haohan.cloud.scm.api.goods.entity.SaleRules;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 售卖规则
 *
 * @author haohan
 * @date 2019-05-28 19:58:06
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "售卖规则")
public class SaleRulesReq extends SaleRules {

    private long pageSize;
    private long pageNo;




}
