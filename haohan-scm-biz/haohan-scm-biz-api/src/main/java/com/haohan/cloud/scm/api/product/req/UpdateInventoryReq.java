package com.haohan.cloud.scm.api.product.req;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author cx
 * @date 2019/6/5
 */
@Data
public class UpdateInventoryReq {

  @NotBlank(message = "pmId不能为空")
  private String pmId;

  @NotBlank(message = "goodsModelId不能为空")
  private String goodsModelId;
}
