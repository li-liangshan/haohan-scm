/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.product.entity.Recipe;

/**
 * 商品加工配方表
 *
 * @author haohan
 * @date 2019-05-13 18:15:51
 */
public interface RecipeService extends IService<Recipe> {

}
