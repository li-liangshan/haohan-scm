/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.manage.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.manage.entity.AuthApp;
import com.haohan.cloud.scm.api.manage.req.AuthAppReq;
import com.haohan.cloud.scm.manage.service.AuthAppService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 授权应用管理
 *
 * @author haohan
 * @date 2019-05-29 14:25:29
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/AuthApp")
@Api(value = "authapp", tags = "authapp内部接口服务")
public class AuthAppFeignApiCtrl {

    private final AuthAppService authAppService;


    /**
     * 通过id查询授权应用管理
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(authAppService.getById(id));
    }


    /**
     * 分页查询 授权应用管理 列表信息
     * @param authAppReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchAuthAppPage")
    public R getAuthAppPage(@RequestBody AuthAppReq authAppReq) {
        Page page = new Page(authAppReq.getPageNo(), authAppReq.getPageSize());
        AuthApp authApp =new AuthApp();
        BeanUtil.copyProperties(authAppReq, authApp);

        return new R<>(authAppService.page(page, Wrappers.query(authApp)));
    }


    /**
     * 全量查询 授权应用管理 列表信息
     * @param authAppReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchAuthAppList")
    public R getAuthAppList(@RequestBody AuthAppReq authAppReq) {
        AuthApp authApp =new AuthApp();
        BeanUtil.copyProperties(authAppReq, authApp);

        return new R<>(authAppService.list(Wrappers.query(authApp)));
    }


    /**
     * 新增授权应用管理
     * @param authApp 授权应用管理
     * @return R
     */
    @Inner
    @SysLog("新增授权应用管理")
    @PostMapping("/add")
    public R save(@RequestBody AuthApp authApp) {
        return new R<>(authAppService.save(authApp));
    }

    /**
     * 修改授权应用管理
     * @param authApp 授权应用管理
     * @return R
     */
    @Inner
    @SysLog("修改授权应用管理")
    @PostMapping("/update")
    public R updateById(@RequestBody AuthApp authApp) {
        return new R<>(authAppService.updateById(authApp));
    }

    /**
     * 通过id删除授权应用管理
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除授权应用管理")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(authAppService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除授权应用管理")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(authAppService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询授权应用管理")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(authAppService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param authAppReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询授权应用管理总记录}")
    @PostMapping("/countByAuthAppReq")
    public R countByAuthAppReq(@RequestBody AuthAppReq authAppReq) {

        return new R<>(authAppService.count(Wrappers.query(authAppReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param authAppReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据authAppReq查询一条货位信息表")
    @PostMapping("/getOneByAuthAppReq")
    public R getOneByAuthAppReq(@RequestBody AuthAppReq authAppReq) {

        return new R<>(authAppService.getOne(Wrappers.query(authAppReq), false));
    }


    /**
     * 批量修改OR插入
     * @param authAppList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<AuthApp> authAppList) {

        return new R<>(authAppService.saveOrUpdateBatch(authAppList));
    }

}
