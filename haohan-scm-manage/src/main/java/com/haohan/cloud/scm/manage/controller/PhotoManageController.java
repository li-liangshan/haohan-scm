/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.manage.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.manage.entity.PhotoManage;
import com.haohan.cloud.scm.api.manage.req.PhotoManageReq;
import com.haohan.cloud.scm.manage.service.PhotoManageService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 图片管理
 *
 * @author haohan
 * @date 2019-05-29 14:27:37
 */
@RestController
@AllArgsConstructor
@RequestMapping("/photomanage" )
@Api(value = "photomanage", tags = "photomanage管理")
public class PhotoManageController {

    private final PhotoManageService photoManageService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param photoManage 图片管理
     * @return
     */
    @GetMapping("/page" )
    public R getPhotoManagePage(Page page, PhotoManage photoManage) {
        return new R<>(photoManageService.page(page, Wrappers.query(photoManage)));
    }


    /**
     * 通过id查询图片管理
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(photoManageService.getById(id));
    }

    /**
     * 新增图片管理
     * @param photoManage 图片管理
     * @return R
     */
    @SysLog("新增图片管理" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_photomanage_add')" )
    public R save(@RequestBody PhotoManage photoManage) {
        return new R<>(photoManageService.save(photoManage));
    }

    /**
     * 修改图片管理
     * @param photoManage 图片管理
     * @return R
     */
    @SysLog("修改图片管理" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_photomanage_edit')" )
    public R updateById(@RequestBody PhotoManage photoManage) {
        return new R<>(photoManageService.updateById(photoManage));
    }

    /**
     * 通过id删除图片管理
     * @param id id
     * @return R
     */
    @SysLog("删除图片管理" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_photomanage_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(photoManageService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除图片管理")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_photomanage_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(photoManageService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询图片管理")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(photoManageService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param photoManageReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询图片管理总记录}")
    @PostMapping("/countByPhotoManageReq")
    public R countByPhotoManageReq(@RequestBody PhotoManageReq photoManageReq) {

        return new R<>(photoManageService.count(Wrappers.query(photoManageReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param photoManageReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据photoManageReq查询一条货位信息表")
    @PostMapping("/getOneByPhotoManageReq")
    public R getOneByPhotoManageReq(@RequestBody PhotoManageReq photoManageReq) {

        return new R<>(photoManageService.getOne(Wrappers.query(photoManageReq), false));
    }


    /**
     * 批量修改OR插入
     * @param photoManageList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_photomanage_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<PhotoManage> photoManageList) {

        return new R<>(photoManageService.saveOrUpdateBatch(photoManageList));
    }


}
