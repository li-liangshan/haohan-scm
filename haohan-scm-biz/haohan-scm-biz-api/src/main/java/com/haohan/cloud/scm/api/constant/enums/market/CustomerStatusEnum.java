package com.haohan.cloud.scm.api.constant.enums.market;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xwx
 * @date 2019/5/30
 */
@Getter
@AllArgsConstructor
public enum CustomerStatusEnum implements IBaseEnum {
    /**
     * 客户状态:0.无1潜在2.有意向3成交4失败
     */
    without("0","无"),
    potential("1","潜在"),
    intention("2","有意向"),
    makeBargain("3","成交"),
    failure("4","失败");

    private static final Map<String, CustomerStatusEnum> MAP = new HashMap<>(8);

    static {
        for (CustomerStatusEnum e : CustomerStatusEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static CustomerStatusEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

    /**
     * 类型
     */
    @EnumValue
    @JsonValue
    private String type;
    /**
     * 描述
     */

    private String desc;
}
