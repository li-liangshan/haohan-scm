/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.manage.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.manage.entity.MerchantAppManage;
import com.haohan.cloud.scm.api.manage.req.MerchantAppManageReq;
import com.haohan.cloud.scm.manage.service.MerchantAppManageService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 商家应用管理
 *
 * @author haohan
 * @date 2019-05-29 14:26:16
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/MerchantAppManage")
@Api(value = "merchantappmanage", tags = "merchantappmanage内部接口服务")
public class MerchantAppManageFeignApiCtrl {

    private final MerchantAppManageService merchantAppManageService;


    /**
     * 通过id查询商家应用管理
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(merchantAppManageService.getById(id));
    }


    /**
     * 分页查询 商家应用管理 列表信息
     * @param merchantAppManageReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchMerchantAppManagePage")
    public R getMerchantAppManagePage(@RequestBody MerchantAppManageReq merchantAppManageReq) {
        Page page = new Page(merchantAppManageReq.getPageNo(), merchantAppManageReq.getPageSize());
        MerchantAppManage merchantAppManage =new MerchantAppManage();
        BeanUtil.copyProperties(merchantAppManageReq, merchantAppManage);

        return new R<>(merchantAppManageService.page(page, Wrappers.query(merchantAppManage)));
    }


    /**
     * 全量查询 商家应用管理 列表信息
     * @param merchantAppManageReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchMerchantAppManageList")
    public R getMerchantAppManageList(@RequestBody MerchantAppManageReq merchantAppManageReq) {
        MerchantAppManage merchantAppManage =new MerchantAppManage();
        BeanUtil.copyProperties(merchantAppManageReq, merchantAppManage);

        return new R<>(merchantAppManageService.list(Wrappers.query(merchantAppManage)));
    }


    /**
     * 新增商家应用管理
     * @param merchantAppManage 商家应用管理
     * @return R
     */
    @Inner
    @SysLog("新增商家应用管理")
    @PostMapping("/add")
    public R save(@RequestBody MerchantAppManage merchantAppManage) {
        return new R<>(merchantAppManageService.save(merchantAppManage));
    }

    /**
     * 修改商家应用管理
     * @param merchantAppManage 商家应用管理
     * @return R
     */
    @Inner
    @SysLog("修改商家应用管理")
    @PostMapping("/update")
    public R updateById(@RequestBody MerchantAppManage merchantAppManage) {
        return new R<>(merchantAppManageService.updateById(merchantAppManage));
    }

    /**
     * 通过id删除商家应用管理
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除商家应用管理")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(merchantAppManageService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除商家应用管理")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(merchantAppManageService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询商家应用管理")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(merchantAppManageService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param merchantAppManageReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询商家应用管理总记录}")
    @PostMapping("/countByMerchantAppManageReq")
    public R countByMerchantAppManageReq(@RequestBody MerchantAppManageReq merchantAppManageReq) {

        return new R<>(merchantAppManageService.count(Wrappers.query(merchantAppManageReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param merchantAppManageReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据merchantAppManageReq查询一条货位信息表")
    @PostMapping("/getOneByMerchantAppManageReq")
    public R getOneByMerchantAppManageReq(@RequestBody MerchantAppManageReq merchantAppManageReq) {

        return new R<>(merchantAppManageService.getOne(Wrappers.query(merchantAppManageReq), false));
    }


    /**
     * 批量修改OR插入
     * @param merchantAppManageList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<MerchantAppManage> merchantAppManageList) {

        return new R<>(merchantAppManageService.saveOrUpdateBatch(merchantAppManageList));
    }

}
