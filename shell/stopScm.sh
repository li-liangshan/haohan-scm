#!/bin/bash

ps -ef | grep haohan-scm-manage.jar | awk '{print $2}' | xargs kill -9
ps -ef | grep haohan-scm-biz-core.jar | awk '{print $2}' | xargs kill -9
