/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.crm.feign;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.crm.entity.SalesArea;
import com.haohan.cloud.scm.api.crm.req.AreaReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 区域管理内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "AreaFeignService", value = ScmServiceName.SCM_CRM)
public interface AreaFeignService {


    /**
     * 通过id查询区域管理
     *
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/Area/{id}", method = RequestMethod.GET)
    R<SalesArea> getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 区域管理 列表信息
     *
     * @param areaReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/Area/fetchAreaPage")
    R<Page<SalesArea>> getAreaPage(@RequestBody AreaReq areaReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 区域管理 列表信息
     *
     * @param areaReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/Area/fetchAreaList")
    R<List<SalesArea>> getAreaList(@RequestBody AreaReq areaReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增区域管理
     *
     * @param area 区域管理
     * @return R
     */
    @PostMapping("/api/feign/Area/add")
    R<Boolean> save(@RequestBody SalesArea area, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改区域管理
     *
     * @param area 区域管理
     * @return R
     */
    @PostMapping("/api/feign/Area/update")
    R<Boolean> updateById(@RequestBody SalesArea area, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除区域管理
     *
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/Area/delete/{id}")
    R<Boolean> removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/Area/batchDelete")
    R<Boolean> removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/Area/listByIds")
    R<List<SalesArea>> listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param areaReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/Area/countByAreaReq")
    R<Integer> countByAreaReq(@RequestBody AreaReq areaReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param areaReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/Area/getOneByAreaReq")
    R<SalesArea> getOneByAreaReq(@RequestBody AreaReq areaReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param areaList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/Area/saveOrUpdateBatch")
    R<Boolean> saveOrUpdateBatch(@RequestBody List<SalesArea> areaList, @RequestHeader(SecurityConstants.FROM) String from);


}
