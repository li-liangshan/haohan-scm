package com.haohan.cloud.scm.api.purchase.req;

import com.haohan.cloud.scm.api.constant.enums.purchase.MethodTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.purchase.PayTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.purchase.PurchaseStatusEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author xwx
 * @date 2019/7/17
 */
@Data
@ApiModel(description = "采购单明细")
public class DetailReq {
    @NotBlank(message = "purchaseDetailSn不能为空")
    @ApiModelProperty(value = "采购单明细编号",required = true)
    private String purchaseDetailSn;

    @NotBlank(message = "transactorId不能为空")
    @ApiModelProperty(value = "任务执行人id",required = true)
    private String transactorId;

    @NotBlank(message = "transactorName不能为空")
    @ApiModelProperty(value = "任务执行人名称",required = true)
    private String transactorName;

    @NotBlank(message = "needBuyNum不能为空")
    @ApiModelProperty(value = "需求采购数量",required = true)
    private BigDecimal needBuyNum;

    @ApiModelProperty(value = "采购价")
    private BigDecimal buyPrice;

    @ApiModelProperty(value = "付款方式1.协议2.现款")
    private PayTypeEnum payType;

    @ApiModelProperty(value = "采购方式类型:1.竞价采购2.单品采购3.协议供应")
    private MethodTypeEnum methodType;

    @ApiModelProperty(value = "竞价截止时间")
    private LocalDateTime biddingEndTime;

    @ApiModelProperty(value = "采购截止时间")
    private LocalDateTime buyFinalTime;

    @ApiModelProperty(value = "采购状态:1.待处理2.待审核3.采购中4.备货中5.已揽货6.采购完成7.部分完成8.已关闭")
    private PurchaseStatusEnum purchaseStatus;

    @ApiModelProperty(value = "任务内容说明")
    private String content;

}
