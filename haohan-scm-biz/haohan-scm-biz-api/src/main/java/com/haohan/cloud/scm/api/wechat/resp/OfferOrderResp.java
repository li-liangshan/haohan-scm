package com.haohan.cloud.scm.api.wechat.resp;

import com.haohan.cloud.scm.api.supply.entity.OfferOrder;
import lombok.Data;

/**
 * @author cx
 * @date 2019/8/19
 */

@Data
public class OfferOrderResp extends OfferOrder {

    private String goodsImg;

    private String cateName;

    private String modelName;
}
