package com.haohan.cloud.scm.api.iot.Resp;

import com.haohan.cloud.scm.api.iot.entity.FeiePrinter;
import lombok.Data;

/**
 *
 * @author cx
 * @date 2019/8/24
 */

@Data
public class FeiePrinterResp extends FeiePrinter {

    /**
     * 店铺名称
     */
    private String shopName;

    /**
     * 商家名称
     */
    private String merchantName;
}
