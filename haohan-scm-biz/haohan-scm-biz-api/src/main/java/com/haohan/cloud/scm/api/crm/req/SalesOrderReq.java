/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.crm.req;

import com.haohan.cloud.scm.api.crm.entity.SalesOrder;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 销售订单
 *
 * @author haohan
 * @date 2019-08-30 11:45:42
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "销售订单")
public class SalesOrderReq extends SalesOrder {

    private long pageSize;
    private long pageNo;


}
