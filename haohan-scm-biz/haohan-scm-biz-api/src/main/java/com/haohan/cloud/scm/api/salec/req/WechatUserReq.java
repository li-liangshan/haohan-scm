/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.salec.req;

import com.haohan.cloud.scm.api.salec.entity.WechatUser;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 微信用户表
 *
 * @author haohan
 * @date 2019-07-10 22:42:10
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "微信用户表")
public class WechatUserReq extends WechatUser {

    private long pageSize;
    private long pageNo;


}
