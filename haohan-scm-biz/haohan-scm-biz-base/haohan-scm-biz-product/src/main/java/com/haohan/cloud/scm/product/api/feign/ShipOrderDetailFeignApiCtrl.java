/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.product.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.product.entity.ShipOrderDetail;
import com.haohan.cloud.scm.api.product.req.ShipOrderDetailReq;
import com.haohan.cloud.scm.product.service.ShipOrderDetailService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 送货明细
 *
 * @author haohan
 * @date 2019-05-29 14:46:17
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/ShipOrderDetail")
@Api(value = "shiporderdetail", tags = "shiporderdetail内部接口服务")
public class ShipOrderDetailFeignApiCtrl {

    private final ShipOrderDetailService shipOrderDetailService;


    /**
     * 通过id查询送货明细
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(shipOrderDetailService.getById(id));
    }


    /**
     * 分页查询 送货明细 列表信息
     * @param shipOrderDetailReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchShipOrderDetailPage")
    public R getShipOrderDetailPage(@RequestBody ShipOrderDetailReq shipOrderDetailReq) {
        Page page = new Page(shipOrderDetailReq.getPageNo(), shipOrderDetailReq.getPageSize());
        ShipOrderDetail shipOrderDetail =new ShipOrderDetail();
        BeanUtil.copyProperties(shipOrderDetailReq, shipOrderDetail);

        return new R<>(shipOrderDetailService.page(page, Wrappers.query(shipOrderDetail)));
    }


    /**
     * 全量查询 送货明细 列表信息
     * @param shipOrderDetailReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchShipOrderDetailList")
    public R getShipOrderDetailList(@RequestBody ShipOrderDetailReq shipOrderDetailReq) {
        ShipOrderDetail shipOrderDetail =new ShipOrderDetail();
        BeanUtil.copyProperties(shipOrderDetailReq, shipOrderDetail);

        return new R<>(shipOrderDetailService.list(Wrappers.query(shipOrderDetail)));
    }


    /**
     * 新增送货明细
     * @param shipOrderDetail 送货明细
     * @return R
     */
    @Inner
    @SysLog("新增送货明细")
    @PostMapping("/add")
    public R save(@RequestBody ShipOrderDetail shipOrderDetail) {
        return new R<>(shipOrderDetailService.save(shipOrderDetail));
    }

    /**
     * 修改送货明细
     * @param shipOrderDetail 送货明细
     * @return R
     */
    @Inner
    @SysLog("修改送货明细")
    @PostMapping("/update")
    public R updateById(@RequestBody ShipOrderDetail shipOrderDetail) {
        return new R<>(shipOrderDetailService.updateById(shipOrderDetail));
    }

    /**
     * 通过id删除送货明细
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除送货明细")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(shipOrderDetailService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除送货明细")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(shipOrderDetailService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询送货明细")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(shipOrderDetailService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param shipOrderDetailReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询送货明细总记录}")
    @PostMapping("/countByShipOrderDetailReq")
    public R countByShipOrderDetailReq(@RequestBody ShipOrderDetailReq shipOrderDetailReq) {

        return new R<>(shipOrderDetailService.count(Wrappers.query(shipOrderDetailReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param shipOrderDetailReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据shipOrderDetailReq查询一条货位信息表")
    @PostMapping("/getOneByShipOrderDetailReq")
    public R getOneByShipOrderDetailReq(@RequestBody ShipOrderDetailReq shipOrderDetailReq) {

        return new R<>(shipOrderDetailService.getOne(Wrappers.query(shipOrderDetailReq), false));
    }


    /**
     * 批量修改OR插入
     * @param shipOrderDetailList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<ShipOrderDetail> shipOrderDetailList) {

        return new R<>(shipOrderDetailService.saveOrUpdateBatch(shipOrderDetailList));
    }

}
