/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.opc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haohan.cloud.scm.api.opc.entity.CostOrder;

/**
 * 成本核算单
 *
 * @author haohan
 * @date 2019-05-13 20:36:32
 */
public interface CostOrderMapper extends BaseMapper<CostOrder> {

}
