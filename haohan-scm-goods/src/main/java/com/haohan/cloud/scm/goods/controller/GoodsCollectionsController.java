/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.goods.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.goods.entity.GoodsCollections;
import com.haohan.cloud.scm.api.saleb.req.GoodsCollectionsReq;
import com.haohan.cloud.scm.goods.service.GoodsCollectionsService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 收藏商品
 *
 * @author haohan
 * @date 2019-05-29 13:30:26
 */
@RestController
@AllArgsConstructor
@RequestMapping("/goodscollections")
@Api(value = "goodscollections", tags = "goodscollections管理")
public class GoodsCollectionsController {

    private final GoodsCollectionsService goodsCollectionsService;

    /**
     * 分页查询
     *
     * @param page             分页对象
     * @param goodsCollections 收藏商品
     * @return
     */
    @GetMapping("/page")
    public R getGoodsCollectionsPage(Page page, GoodsCollections goodsCollections) {
        return new R<>(goodsCollectionsService.page(page, Wrappers.query(goodsCollections)));
    }


    /**
     * 通过id查询收藏商品
     *
     * @param id id
     * @return R
     */
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(goodsCollectionsService.getById(id));
    }

    /**
     * 新增收藏商品
     *
     * @param goodsCollections 收藏商品
     * @return R
     */
    @SysLog("新增收藏商品")
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_goodscollections_add')")
    public R save(@RequestBody GoodsCollections goodsCollections) {
        return new R<>(goodsCollectionsService.save(goodsCollections));
    }

    /**
     * 修改收藏商品
     *
     * @param goodsCollections 收藏商品
     * @return R
     */
    @SysLog("修改收藏商品")
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_goodscollections_edit')")
    public R updateById(@RequestBody GoodsCollections goodsCollections) {
        return new R<>(goodsCollectionsService.updateById(goodsCollections));
    }

    /**
     * 通过id删除收藏商品
     *
     * @param id id
     * @return R
     */
    @SysLog("删除收藏商品")
    @DeleteMapping("/{id}")
    @PreAuthorize("@pms.hasPermission('scm_goodscollections_del')")
    public R removeById(@PathVariable String id) {
        return new R<>(goodsCollectionsService.removeById(id));
    }


    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @return R
     */
    @SysLog("批量删除收藏商品")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_goodscollections_del')")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(goodsCollectionsService.removeByIds(idList));
    }


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @SysLog("根据IDS批量查询收藏商品")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(goodsCollectionsService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param goodsCollectionsReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询收藏商品总记录}")
    @PostMapping("/countByGoodsCollectionsReq")
    public R countByGoodsCollectionsReq(@RequestBody GoodsCollectionsReq goodsCollectionsReq) {

        return new R<>(goodsCollectionsService.count(Wrappers.query(goodsCollectionsReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     *
     * @param goodsCollectionsReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据goodsCollectionsReq查询一条货位信息表")
    @PostMapping("/getOneByGoodsCollectionsReq")
    public R getOneByGoodsCollectionsReq(@RequestBody GoodsCollectionsReq goodsCollectionsReq) {

        return new R<>(goodsCollectionsService.getOne(Wrappers.query(goodsCollectionsReq), false));
    }


    /**
     * 批量修改OR插入
     *
     * @param goodsCollectionsList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_goodscollections_edit')")
    public R saveOrUpdateBatch(@RequestBody List<GoodsCollections> goodsCollectionsList) {

        return new R<>(goodsCollectionsService.saveOrUpdateBatch(goodsCollectionsList));
    }


}
