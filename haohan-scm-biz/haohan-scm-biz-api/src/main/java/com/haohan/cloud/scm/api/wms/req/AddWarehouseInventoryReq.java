package com.haohan.cloud.scm.api.wms.req;

import com.haohan.cloud.scm.api.product.req.ProductInfoInventoryReq;
import com.haohan.cloud.scm.api.wms.entity.WarehouseInventory;
import lombok.Data;

import java.util.List;

/**
 * @author cx
 * @date 2019/8/10
 */

@Data
public class AddWarehouseInventoryReq extends WarehouseInventory {

    private List<ProductInfoInventoryReq> infos;
}
