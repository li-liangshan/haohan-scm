package com.haohan.cloud.scm.api.wechat.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author dy
 * @date 2020/5/28
 */
@Data
public class WxTenantVO implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer tenantId;

    public WxTenantVO(Integer tenantId) {
        this.tenantId = tenantId;
    }
}
