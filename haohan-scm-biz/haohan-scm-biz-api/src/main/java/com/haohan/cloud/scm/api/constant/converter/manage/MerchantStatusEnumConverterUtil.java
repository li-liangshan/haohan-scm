package com.haohan.cloud.scm.api.constant.converter.manage;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.manage.MerchantStatusEnum;

/**
 * @author dy
 * @date 2019/9/24
 */
public class MerchantStatusEnumConverterUtil implements Converter<MerchantStatusEnum> {
    @Override
    public MerchantStatusEnum convert(Object o, MerchantStatusEnum merchantStatusEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return MerchantStatusEnum.getByType(o.toString());
    }
}
