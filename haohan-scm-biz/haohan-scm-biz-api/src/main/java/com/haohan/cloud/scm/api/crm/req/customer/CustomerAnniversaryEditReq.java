package com.haohan.cloud.scm.api.crm.req.customer;

import cn.hutool.core.bean.BeanUtil;
import com.haohan.cloud.scm.api.constant.enums.market.AnniversaryTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.market.CalendarTypeEnum;
import com.haohan.cloud.scm.api.crm.entity.CustomerAnniversary;
import com.haohan.cloud.scm.common.tools.constant.SecondGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * @author dy
 * @date 2019/11/30
 */
@Data
@ApiModel(value = "客户纪念日编辑")
public class CustomerAnniversaryEditReq {

    @NotBlank(message = "id不能为空", groups = {SecondGroup.class})
    @Length(max = 32, message = "主键的长度最大32字符")
    @ApiModelProperty(value = "主键")
    private String id;

    @NotBlank(message = "主题内容不能为空")
    @Length(max = 255, message = "主题内容的长度最大255字符")
    @ApiModelProperty(value = "主题内容")
    private String content;

    @NotBlank(message = "客户编号不能为空")
    @Length(max = 32, message = "客户编号的长度最大32字符")
    @ApiModelProperty(value = "客户编号")
    private String customerSn;

    @NotBlank(message = "客户联系人id不能为空")
    @Length(max = 32, message = "客户联系人id的长度最大32字符")
    @ApiModelProperty(value = "客户联系人id")
    private String linkmanId;

    @NotNull(message = "纪念日类型不能为空")
    @ApiModelProperty(value = "纪念日类型:1.公司2.联系人")
    private AnniversaryTypeEnum anniversaryType;

    @NotNull(message = "纪念日时间不能为空")
    @ApiModelProperty(value = "纪念日时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime anniversaryTime;

    @NotNull(message = "日历类型不能为空")
    @ApiModelProperty(value = "日历类型:1.公历2.农历")
    private CalendarTypeEnum calendarType;

    @NotBlank(message = "市场负责人id不能为空")
    @Length(max = 32, message = "市场负责人id的长度最大32字符")
    @ApiModelProperty(value = "市场负责人id")
    private String directorId;

    @Length(max = 255, message = "备注信息的长度最大255字符")
    @ApiModelProperty(value = "备注信息")
    private String remarks;

    public CustomerAnniversary transTo() {
        CustomerAnniversary anniversary = new CustomerAnniversary();
        BeanUtil.copyProperties(this, anniversary);
        return anniversary;
    }
}
