/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.purchase.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseOrderDetail;
import com.haohan.cloud.scm.api.purchase.req.PurchaseOrderDetailReq;
import com.haohan.cloud.scm.purchase.service.PurchaseOrderDetailService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 采购部采购单明细
 *
 * @author haohan
 * @date 2019-05-29 13:35:25
 */
@RestController
@AllArgsConstructor
@RequestMapping("/purchaseorderdetail" )
@Api(value = "purchaseorderdetail", tags = "purchaseorderdetail管理")
public class PurchaseOrderDetailController {

    private final PurchaseOrderDetailService purchaseOrderDetailService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param purchaseOrderDetail 采购部采购单明细
     * @return
     */
    @GetMapping("/page" )
    public R getPurchaseOrderDetailPage(Page page, PurchaseOrderDetail purchaseOrderDetail) {
        return new R<>(purchaseOrderDetailService.page(page, Wrappers.query(purchaseOrderDetail)));
    }


    /**
     * 通过id查询采购部采购单明细
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(purchaseOrderDetailService.getById(id));
    }

    /**
     * 新增采购部采购单明细
     * @param purchaseOrderDetail 采购部采购单明细
     * @return R
     */
    @SysLog("新增采购部采购单明细" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('purchase_purchaseorderdetail_add')" )
    public R save(@RequestBody PurchaseOrderDetail purchaseOrderDetail) {
        return new R<>(purchaseOrderDetailService.save(purchaseOrderDetail));
    }

    /**
     * 修改采购部采购单明细
     * @param purchaseOrderDetail 采购部采购单明细
     * @return R
     */
    @SysLog("修改采购部采购单明细" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('purchase_purchaseorderdetail_edit')" )
    public R updateById(@RequestBody PurchaseOrderDetail purchaseOrderDetail) {
        return new R<>(purchaseOrderDetailService.updateById(purchaseOrderDetail));
    }

    /**
     * 通过id删除采购部采购单明细
     * @param id id
     * @return R
     */
    @SysLog("删除采购部采购单明细" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('purchase_purchaseorderdetail_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(purchaseOrderDetailService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除采购部采购单明细")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('purchase_purchaseorderdetail_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(purchaseOrderDetailService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询采购部采购单明细")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(purchaseOrderDetailService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param purchaseOrderDetailReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询采购部采购单明细总记录}")
    @PostMapping("/countByPurchaseOrderDetailReq")
    public R countByPurchaseOrderDetailReq(@RequestBody PurchaseOrderDetailReq purchaseOrderDetailReq) {

        return new R<>(purchaseOrderDetailService.count(Wrappers.query(purchaseOrderDetailReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param purchaseOrderDetailReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据purchaseOrderDetailReq查询一条货位信息表")
    @PostMapping("/getOneByPurchaseOrderDetailReq")
    public R getOneByPurchaseOrderDetailReq(@RequestBody PurchaseOrderDetailReq purchaseOrderDetailReq) {

        return new R<>(purchaseOrderDetailService.getOne(Wrappers.query(purchaseOrderDetailReq), false));
    }


    /**
     * 批量修改OR插入
     * @param purchaseOrderDetailList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('purchase_purchaseorderdetail_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<PurchaseOrderDetail> purchaseOrderDetailList) {

        return new R<>(purchaseOrderDetailService.saveOrUpdateBatch(purchaseOrderDetailList));
    }


}
