package com.haohan.cloud.scm.api.crm.vo;

import cn.hutool.core.bean.BeanUtil;
import com.haohan.cloud.scm.api.constant.enums.crm.SalesGoodsTypeEnum;
import com.haohan.cloud.scm.api.crm.entity.DataReportDetail;
import com.haohan.cloud.scm.api.goods.vo.GoodsVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author dy
 * @date 2019/12/7
 */
@Data
@ApiModel("数据上报明细信息, 带商品信息")
@NoArgsConstructor
public class DataReportDetailVO {

    /**
     * 上报编号
     */
    @ApiModelProperty(value = "上报编号")
    private String reportSn;
    /**
     * 商品规格id
     */
    @ApiModelProperty(value = "商品规格id")
    private String goodsModelId;
    /**
     * 商品名称
     */
    @ApiModelProperty(value = "商品名称")
    private String goodsName;
    /**
     * 规格属性
     */
    @ApiModelProperty(value = "规格属性")
    private String modelAttr;
    /**
     * 单位
     */
    @ApiModelProperty(value = "单位")
    private String goodsUnit;
    /**
     * 商品图片
     */
    @ApiModelProperty(value = "商品图片")
    private String goodsImg;
    /**
     * 商品规格编号
     */
    @ApiModelProperty(value = "商品规格编号")
    private String modelSn;
    /**
     * 条形码
     */
    @ApiModelProperty(value = "条形码")
    private String modelCode;
    /**
     * 批发价
     */
    @ApiModelProperty(value = "批发价")
    private BigDecimal tradePrice;
    /**
     * 保质期
     */
    @ApiModelProperty(value = "保质期")
    private String expiration;
    /**
     * 生产日期
     */
    @ApiModelProperty(value = "生产日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime productTime;
    /**
     * 到期日期
     */
    @ApiModelProperty(value = "到期日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime maturityTime;
    /**
     * 数量
     */
    @ApiModelProperty(value = "数量")
    private BigDecimal goodsNum;

    @ApiModelProperty(value = "金额")
    private BigDecimal amount;
    /**
     * 商品销售类型:1.普通2.促销品3.赠品
     */
    @ApiModelProperty(value = "商品销售类型:1.普通2.促销品3.赠品")
    private SalesGoodsTypeEnum salesGoodsType;
    /**
     * 备注信息
     */
    @ApiModelProperty(value = "备注信息")
    private String remarks;

    @ApiModelProperty(value = "对应商品详情")
    private GoodsVO goodsInfo;

    @ApiModelProperty(value = "赠品明细数量")
    private BigDecimal giftNum;

    public DataReportDetailVO(DataReportDetail detail) {
        BeanUtil.copyProperties(detail, this);
        this.amount = (null != this.tradePrice && null != this.goodsNum) ? this.goodsNum.multiply(this.tradePrice) : BigDecimal.ZERO;
        this.giftNum = BigDecimal.ZERO;
    }
}
