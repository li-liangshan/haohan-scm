package com.haohan.cloud.scm.api.common.req.pay;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 商品订单明细Entity
 *
 * @author haohan
 * @version 2018-09-01
 */
@Data
public class GoodsOrderDetail implements Serializable {
  private static final long serialVersionUID = 1L;
  /**
   * 订单编号
   */

  private String orderId;

  private String merchantId;    // 商家id
  private String goodsId;    // 商品编号
  private String modelId;    //sku id
  private String goodsName;    // 商品名称
  private BigDecimal goodsPrice;    // 实际售价
  private BigDecimal marketPrice;    // 市场售价
  private BigDecimal goodsNum;    // 商品份数
  private String goodsUnit;    // 商品单位
  private String modelName;      //商品规格
  private String goodsAttrIds;    // 商品属性集合
  private String extAttr;    // 扩展属性
  private Integer isReal;    // 是否实物
  private Integer cartGoodsType;    //购物类型
  private String activityId;    // 活动编号
  private String giftName;    // 赠品名称
  private String giftId;    // 赠品id
  private String giftSchedule;    // 赠送周期
  private Integer giftNum;    // 赠送数量
  private String deliverySchedule;    // 配送周期
  private Date deliveryStartDate;    //配送起始时间
  private String arriveType;    // 配送时效
  private Integer deliveryNum;    // 每次配送数量
  private String deliveryPlanType;    // 配送计划类型
  private String deliveryType;    // 配送方式
  private Integer deliveryTotalNum;  //配送总数量
  private String serviceName;    // 服务名称
  private String serviceDetail;    // 服务内容
  private BigDecimal servicePrice;    // 服务价格
  private String serviceSchedule;    // 服务周期
  private Integer serviceNum;    // 服务次数


  //查询销量额外需要的参数
  private String shopId;
  private String payStatus;
  private Date startTime;
  private Date endTime;

  public GoodsOrderDetail() {
    super();
  }


}
