package com.haohan.cloud.scm.api.crm.req.app;

import com.haohan.cloud.scm.api.constant.enums.common.UseStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.market.CompanyNatureEnum;
import com.haohan.cloud.scm.api.constant.enums.market.CustomerTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.supply.GradeTypeEnum;
import com.haohan.cloud.scm.api.crm.entity.Customer;
import com.haohan.cloud.scm.common.tools.constant.SingleGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import java.time.LocalDate;

/**
 * @author dy
 * @date 2019/10/23
 */
@Data
public class FetchCustomerListReq {

    @ApiModelProperty(value = "客户类型:1经销商2门店")
    private CustomerTypeEnum customerType;

    @Length(max = 32, message = "市场编码字符长度在0至32之间")
    @ApiModelProperty(value = "市场编码")
    private String marketSn;

    @Length(max = 32, message = "客户经理id字符长度在0至32之间")
    @ApiModelProperty(value = "客户负责人id")
    private String directorId;

    @Length(max = 32, message = "客户编号字符长度在0至32之间")
    @ApiModelProperty(value = "客户编号")
    private String customerSn;

    @ApiModelProperty(value = "客户联系手机")
    private String telephone;

    @ApiModelProperty(value = "客户级别", notes = "客户级别:1星-5星")
    private GradeTypeEnum customerLevel;

    @ApiModelProperty(value = "客户状态", notes = "客户状态:0.未启用 1.启用 2.待审核")
    private UseStatusEnum status;

    @ApiModelProperty(value = "公司性质", notes = "公司性质:1.个体2.民营3.国有4.其他")
    private CompanyNatureEnum companyNature;
    // 单独处理请求参数

    @ApiModelProperty(value = "是否标注定位")
    private Boolean positionFlag;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间-开时时间")
    private LocalDate startDate;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间-结束时间")
    private LocalDate endDate;

    @Length(max = 20, message = "客户名称字符长度在0至20之间")
    @ApiModelProperty(value = "客户名称")
    private String customerName;

    @Length(max = 32, message = "销售区域字符长度在0至32之间")
    @ApiModelProperty(value = "销售区域")
    private String areaSn;

    @Length(max = 10, message = "客户联系人名称字符长度在0至10之间")
    @ApiModelProperty(value = "客户联系人名称")
    private String linkManName;

    @Length(max = 20, message = "客户标签字符长度在0至20之间")
    @ApiModelProperty(value = "客户标签")
    private String tags;

    // app登陆员工

    @NotBlank(message = "员工id不能为空", groups = {SingleGroup.class})
    @Length(max = 32, message = "员工id字符长度在0至32之间", groups = {SingleGroup.class})
    @ApiModelProperty(value = "员工id")
    private String employeeId;

    public Customer transTo() {
        Customer customer = new Customer();
        // eq参数
        customer.setCustomerType(this.customerType);
        customer.setMarketSn(this.marketSn);
        customer.setDirectorId(this.directorId);
        customer.setCustomerLevel(this.customerLevel);
        customer.setStatus(this.status);
        customer.setCompanyNature(this.companyNature);
        return customer;
    }
}
