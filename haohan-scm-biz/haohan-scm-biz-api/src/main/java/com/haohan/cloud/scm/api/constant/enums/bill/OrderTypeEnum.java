package com.haohan.cloud.scm.api.constant.enums.bill;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dy
 * @date 2019/11/26
 * 订单类型
 */
@Getter
@AllArgsConstructor
public enum OrderTypeEnum implements IBaseEnum {

    /**
     * 订单类型： 1.采购订单、2.供应订单、3.退货订单、4.销售订单
     */
    buy("1", "采购订单"),
    supply("2", "供应订单"),
    back("3", "退货订单"),
    sales("4", "销售订单");

    private static final Map<String, OrderTypeEnum> MAP = new HashMap<>(8);

    static {
        for (OrderTypeEnum e : OrderTypeEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static OrderTypeEnum getByType(String type) {
        return MAP.get(type);
    }

    @EnumValue
    @JsonValue
    private String type;
    private String desc;
}
