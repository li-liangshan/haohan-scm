package com.haohan.cloud.scm.api.opc.req;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

/**
 * @author dy
 * @date 2019/5/27
 */
@Data
public class OpcTradeOrderQueryReq {

    /**
     * 主键
     */
    private String id;
    /**
     * 平台商家id
     */
    private String pmId;
    /**
     * 交易单号
     */
    private String tradeId;
    /**
     * 汇总单号
     */
    private String summaryBuyId;
    /**
     * 采购编号
     */
    private String buyId;
    /**
     * 报价类型
     */
    private String offerType;
    /**
     * 报价单号
     */
    private String offerId;
    /**
     * 采购商
     */
    private String buyerId;
    /**
     * 供应商
     */
    private String supplierId;
    /**
     * 采购批次
     */
    private String buySeq;
    /**
     * 商品ID
     */
    private String goodsId;
    /**
     * 采购时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime buyTime;
    /**
     * 成交时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime dealTime;
    /**
     * 送货时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime deliveryTime;
    /**
     * 供应商状态
     */
    private String supplierStatus;
    /**
     * 采购商状态
     */
    private String buyerStatus;
    /**
     * 采购员
     */
    private String buyOperator;
    /**
     * 运营状态
     */
    private String opStatus;
    /**
     * 配送状态
     */
    private String deliveryStatus;
    /**
     * 交易状态
     */
    private String transStatus;
    /**
     * 配送方式
     */
    private String deliveryType;
    /**
     * 租户id
     */
    private Integer tenantId;

}
