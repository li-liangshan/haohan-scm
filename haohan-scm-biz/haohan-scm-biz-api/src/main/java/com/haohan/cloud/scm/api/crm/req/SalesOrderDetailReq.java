/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.crm.req;

import com.haohan.cloud.scm.api.crm.entity.SalesOrderDetail;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Set;


/**
 * 销售订单明细
 *
 * @author haohan
 * @date 2019-08-30 11:45:38
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "销售订单明细")
public class SalesOrderDetailReq extends SalesOrderDetail {

    private long pageSize;
    private long pageNo;

    @ApiModelProperty("商品规格id集合")
    private Set<String> modeIdSet;


}
