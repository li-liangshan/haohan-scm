/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.haohan.cloud.scm.api.saleb.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.haohan.cloud.scm.api.constant.enums.common.UseStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import com.haohan.cloud.scm.api.constant.enums.saleb.BuyerTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.saleb.PayPeriodEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

/**
 * 采购商
 *
 * @author haohan
 * @date 2019-05-13 20:35:22
 */
@Data
@TableName("scm_buyer")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "采购商")
public class Buyer extends Model<Buyer> {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 平台商家ID  (原系统使用)
     */
    private String pmId;
    /**
     * 通行证ID
     */
    private String passportId;
    /**
     * 商家ID
     */
    private String merchantId;
    /**
     * 商家名称
     */
    private String merchantName;
    /**
     * 全称
     */
    private String buyerName;
    /**
     * 采购商名称
     */
    private String shortName;
    /**
     * 联系人
     */
    private String contact;
    /**
     * 联系电话
     */
    private String telephone;
    /**
     * 微信
     */
    private String wechatId;
    /**
     * 操作员
     */
    private String operator;
    /**
     * 账期:1.月结2.周结3.日结
     */
    private PayPeriodEnum payPeriod;
    /**
     * 账期日：  设置账期对应的结算日，使用数字，最大2位
     */
    private String payDay;
    /**
     * 采购商地址
     */
    private String address;
    /**
     * 是否限制下单（下单限制状态，需验证 账期内账单是否全部结算） 0否1是
     */
    private YesNoEnum needConfirmation;
    /**
     * 是否需下单支付(下单后马上支付) 0否1是
     */
    private YesNoEnum needPay;
    /**
     * 采购商类型
     */
    private BuyerTypeEnum buyerType;
    /**
     * 是否开启消息推送
     */
    private YesNoEnum needPush;
    /**
     * 是否启用
     */
    private UseStatusEnum status;
    /**
     * 排序值
     */
    private String sort;

    /**
     * 用户id
     */
    private String userId;

    /**
     * 创建者
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
     * 更新者
     */
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateDate;
    /**
     * 备注信息
     */
    private String remarks;
    /**
     * 删除标记
     */
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;
    /**
     * 租户id
     */
    private Integer tenantId;
    /**
     * 区域地址
     */
    private String area;
    /**
     * 经度
     */
    private String longitude;
    /**
     * 纬度
     */
    private String latitude;

}
