/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.saleb.req;

import com.haohan.cloud.scm.api.saleb.entity.BuyOrder;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;


/**
 * 采购单
 *
 * @author haohan
 * @date 2019-05-29 13:29:53
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "采购单")
public class BuyOrderReq extends BuyOrder {

    private long pageSize;
    private long pageNo;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime beginCreateTime;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime endCreateTime;

}
