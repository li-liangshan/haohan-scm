/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.haohan.cloud.scm.api.purchase.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.haohan.cloud.scm.api.constant.enums.purchase.LendingStatusEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 放款申请记录
 *
 * @author haohan
 * @date 2019-05-13 18:48:27
 */
@Data
@TableName("scm_pms_lending_record")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "放款申请记录")
public class LendingRecord extends Model<LendingRecord> {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 平台商家id
     */
    private String pmId;
    /**
     * 放款申请记录编号
     */
    private String lendingSn;
    /**
     * 采购明细编号
     */
    private String purchaseDetailSn;
    /**
     * 申请人
     */
    private String applicantId;
    /**
     * 申请人名称
     */
    private String applicantName;
    /**
     * 申请内容
     */
    private String applicantContent;
    /**
     * 申请时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime applyTime;
    /**
     * 审核人
     */
    private String auditorId;
    /**
     * 审核人名称
     */
    private String auditorName;
    /**
     * 审核人意见
     */
    private String auditorOpinion;
    /**
     * 审核时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime auditTime;
    /**
     * 复审人
     */
    private String reviewerId;
    /**
     * 复审人名称
     */
    private String reviewerName;
    /**
     * 复审人意见
     */
    private String reviewerOpinion;
    /**
     * 复审时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime reviewTime;
    /**
     * 申请金额
     */
    private BigDecimal applyAmount;
    /**
     * 收款人
     */
    private String payee;
    /**
     * 收款人名称
     */
    private String payeeName;
    /**
     * 放款金额
     */
    private BigDecimal lendingAmount;
    /**
     * 放款人
     */
    private String lenderId;
    /**
     * 放款人名称
     */
    private String lenderName;
    /**
     * 放款时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime lendingTime;
    /**
     * 放款说明
     */
    private String lengingContent;
    /**
     * 请款状态:1.已申请2.通过初审3.待复审4.审核未通过5.待放款6.已放款
     */
    private LendingStatusEnum lendingStatus;
    /**
     * 创建者
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
     * 更新者
     */
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateDate;
    /**
     * 备注信息
     */
    private String remarks;
    /**
     * 删除标记
     */
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;
    /**
     * 租户id
     */
    private Integer tenantId;

}
