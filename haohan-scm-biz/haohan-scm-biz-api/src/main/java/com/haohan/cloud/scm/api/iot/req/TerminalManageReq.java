/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.iot.req;

import com.haohan.cloud.scm.api.iot.entity.TerminalManage;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 终端设备管理
 *
 * @author haohan
 * @date 2019-05-28 20:32:05
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "终端设备管理")
public class TerminalManageReq extends TerminalManage {

    private long pageSize;
    private long pageNo;




}
