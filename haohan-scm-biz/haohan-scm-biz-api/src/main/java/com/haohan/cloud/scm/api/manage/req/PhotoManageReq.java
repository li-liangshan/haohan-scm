/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.manage.req;

import com.haohan.cloud.scm.api.manage.entity.PhotoManage;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 图片管理
 *
 * @author haohan
 * @date 2019-05-28 20:37:22
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "图片管理")
public class PhotoManageReq extends PhotoManage {

    private long pageSize = 10;
    private long pageNo = 1;




}
