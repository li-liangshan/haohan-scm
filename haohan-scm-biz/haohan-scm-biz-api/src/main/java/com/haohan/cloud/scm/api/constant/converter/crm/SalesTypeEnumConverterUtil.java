package com.haohan.cloud.scm.api.constant.converter.crm;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.crm.SalesTypeEnum;

/**
 * @author dy
 * @date 2019/8/31
 */
public class SalesTypeEnumConverterUtil implements Converter<SalesTypeEnum> {
    @Override
    public SalesTypeEnum convert(Object o, SalesTypeEnum useStatusEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return SalesTypeEnum.getByType(o.toString());
    }

}
