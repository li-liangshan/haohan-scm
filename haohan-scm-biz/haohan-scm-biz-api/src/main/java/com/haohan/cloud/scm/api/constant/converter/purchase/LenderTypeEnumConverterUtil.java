package com.haohan.cloud.scm.api.constant.converter.purchase;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.purchase.LenderTypeEnum;

/**
 * @author xwx
 * @date 2019/6/3
 */
public class LenderTypeEnumConverterUtil implements Converter<LenderTypeEnum> {
    @Override
    public LenderTypeEnum convert(Object o, LenderTypeEnum lenderTypeEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return LenderTypeEnum.getByType(o.toString());
    }
}
