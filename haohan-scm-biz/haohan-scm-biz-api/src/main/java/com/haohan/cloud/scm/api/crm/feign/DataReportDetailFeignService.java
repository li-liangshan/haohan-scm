/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.crm.feign;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.crm.entity.DataReportDetail;
import com.haohan.cloud.scm.api.crm.req.DataReportDetailReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 数据汇报明细内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "DataReportDetailFeignService", value = ScmServiceName.SCM_CRM)
public interface DataReportDetailFeignService {


    /**
     * 通过id查询数据汇报明细
     *
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/DataReportDetail/{id}", method = RequestMethod.GET)
    R<DataReportDetail> getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 数据汇报明细 列表信息
     *
     * @param dataReportDetailReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/DataReportDetail/fetchDataReportDetailPage")
    R<Page<DataReportDetail>> getDataReportDetailPage(@RequestBody DataReportDetailReq dataReportDetailReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 数据汇报明细 列表信息
     *
     * @param dataReportDetailReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/DataReportDetail/fetchDataReportDetailList")
    R<List<DataReportDetail>> getDataReportDetailList(@RequestBody DataReportDetailReq dataReportDetailReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增数据汇报明细
     *
     * @param dataReportDetail 数据汇报明细
     * @return R
     */
    @PostMapping("/api/feign/DataReportDetail/add")
    R<Boolean> save(@RequestBody DataReportDetail dataReportDetail, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改数据汇报明细
     *
     * @param dataReportDetail 数据汇报明细
     * @return R
     */
    @PostMapping("/api/feign/DataReportDetail/update")
    R<Boolean> updateById(@RequestBody DataReportDetail dataReportDetail, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除数据汇报明细
     *
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/DataReportDetail/delete/{id}")
    R<Boolean> removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/DataReportDetail/batchDelete")
    R<Boolean> removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/DataReportDetail/listByIds")
    R<List<DataReportDetail>> listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param dataReportDetailReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/DataReportDetail/countByDataReportDetailReq")
    R<Integer> countByDataReportDetailReq(@RequestBody DataReportDetailReq dataReportDetailReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param dataReportDetailReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/DataReportDetail/getOneByDataReportDetailReq")
    R<DataReportDetail> getOneByDataReportDetailReq(@RequestBody DataReportDetailReq dataReportDetailReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param dataReportDetailList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/DataReportDetail/saveOrUpdateBatch")
    R<Boolean> saveOrUpdateBatch(@RequestBody List<DataReportDetail> dataReportDetailList, @RequestHeader(SecurityConstants.FROM) String from);


}
