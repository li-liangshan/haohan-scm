/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.opc.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.constant.NumberPrefixConstant;
import com.haohan.cloud.scm.api.opc.entity.CostControl;
import com.haohan.cloud.scm.common.tools.util.ScmIncrementUtil;
import com.haohan.cloud.scm.opc.mapper.CostControlMapper;
import com.haohan.cloud.scm.opc.service.CostControlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

/**
 * 成本管控
 *
 * @author haohan
 * @date 2019-05-13 20:36:45
 */
@Service
public class CostControlServiceImpl extends ServiceImpl<CostControlMapper, CostControl> implements CostControlService {

    @Autowired
    @Lazy
    private ScmIncrementUtil scmIncrementUtil;

    @Override
    public boolean save(CostControl entity) {
        if (StrUtil.isEmpty(entity.getCostId())) {
            String sn = scmIncrementUtil.inrcSnByClass(CostControl.class, NumberPrefixConstant.COST_CONTROL_SN_PRE);
            entity.setCostId(sn);
        }
        return super.save(entity);
    }

}
