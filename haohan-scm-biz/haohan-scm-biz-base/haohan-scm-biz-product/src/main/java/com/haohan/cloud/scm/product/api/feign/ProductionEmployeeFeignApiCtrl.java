/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.product.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.product.entity.ProductionEmployee;
import com.haohan.cloud.scm.api.product.req.ProductionEmployeeReq;
import com.haohan.cloud.scm.product.service.ProductionEmployeeService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 生产部员工表
 *
 * @author haohan
 * @date 2019-05-29 14:45:20
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/ProductionEmployee")
@Api(value = "productionemployee", tags = "productionemployee内部接口服务")
public class ProductionEmployeeFeignApiCtrl {

    private final ProductionEmployeeService productionEmployeeService;


    /**
     * 通过id查询生产部员工表
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(productionEmployeeService.getById(id));
    }


    /**
     * 分页查询 生产部员工表 列表信息
     * @param productionEmployeeReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchProductionEmployeePage")
    public R getProductionEmployeePage(@RequestBody ProductionEmployeeReq productionEmployeeReq) {
        Page page = new Page(productionEmployeeReq.getPageNo(), productionEmployeeReq.getPageSize());
        ProductionEmployee productionEmployee =new ProductionEmployee();
        BeanUtil.copyProperties(productionEmployeeReq, productionEmployee);

        return new R<>(productionEmployeeService.page(page, Wrappers.query(productionEmployee)));
    }


    /**
     * 全量查询 生产部员工表 列表信息
     * @param productionEmployeeReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchProductionEmployeeList")
    public R getProductionEmployeeList(@RequestBody ProductionEmployeeReq productionEmployeeReq) {
        ProductionEmployee productionEmployee =new ProductionEmployee();
        BeanUtil.copyProperties(productionEmployeeReq, productionEmployee);

        return new R<>(productionEmployeeService.list(Wrappers.query(productionEmployee)));
    }


    /**
     * 新增生产部员工表
     * @param productionEmployee 生产部员工表
     * @return R
     */
    @Inner
    @SysLog("新增生产部员工表")
    @PostMapping("/add")
    public R save(@RequestBody ProductionEmployee productionEmployee) {
        return new R<>(productionEmployeeService.save(productionEmployee));
    }

    /**
     * 修改生产部员工表
     * @param productionEmployee 生产部员工表
     * @return R
     */
    @Inner
    @SysLog("修改生产部员工表")
    @PostMapping("/update")
    public R updateById(@RequestBody ProductionEmployee productionEmployee) {
        return new R<>(productionEmployeeService.updateById(productionEmployee));
    }

    /**
     * 通过id删除生产部员工表
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除生产部员工表")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(productionEmployeeService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除生产部员工表")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(productionEmployeeService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询生产部员工表")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(productionEmployeeService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param productionEmployeeReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询生产部员工表总记录}")
    @PostMapping("/countByProductionEmployeeReq")
    public R countByProductionEmployeeReq(@RequestBody ProductionEmployeeReq productionEmployeeReq) {

        return new R<>(productionEmployeeService.count(Wrappers.query(productionEmployeeReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param productionEmployeeReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据productionEmployeeReq查询一条货位信息表")
    @PostMapping("/getOneByProductionEmployeeReq")
    public R getOneByProductionEmployeeReq(@RequestBody ProductionEmployeeReq productionEmployeeReq) {

        return new R<>(productionEmployeeService.getOne(Wrappers.query(productionEmployeeReq), false));
    }


    /**
     * 批量修改OR插入
     * @param productionEmployeeList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<ProductionEmployee> productionEmployeeList) {

        return new R<>(productionEmployeeService.saveOrUpdateBatch(productionEmployeeList));
    }

}
