package com.haohan.cloud.scm.api.constant.converter.product;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.product.ProductQialityEnum;

/**
 * @author xwx
 * @date 2019/6/4
 */
public class ProductQialityEnumConverterUtil implements Converter<ProductQialityEnum> {
    @Override
    public ProductQialityEnum convert(Object o, ProductQialityEnum productQialityEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return ProductQialityEnum.getByType(o.toString());
    }
}
