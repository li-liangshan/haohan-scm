package com.haohan.cloud.scm.purchase.api.ctrl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.purchase.entity.DealRecord;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseEmployee;
import com.haohan.cloud.scm.api.purchase.req.*;
import com.haohan.cloud.scm.api.purchase.resp.PurchaseEmployeeDetailResp;
import com.haohan.cloud.scm.common.tools.annotation.ScmJsonFilter;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.haohan.cloud.scm.purchase.core.IScmPurchaseEmployeeInfoService;
import com.haohan.cloud.scm.purchase.service.PurchaseEmployeeService;
import com.pig4cloud.pigx.common.core.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @author dy
 * @date 2019/5/16
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/purchase/employee")
@Api(value = "ApiPurchaseEmployeeInfo", tags = "purchaseEmployeeInfo采购员工信息")
public class PurchaseEmployeeInfoApiCtrl {

    private final IScmPurchaseEmployeeInfoService employeeInfoService;
    private final PurchaseEmployeeService purchaseEmployeeService;


    /**
     * 查询采购员工详情
     *
     * @param req
     * @return
     */
    @GetMapping("/queryEmployeeDetail")
    public R<PurchaseEmployeeDetailResp> queryEmployeeDetail(@Validated PurchaseEmployeeDetailReq req) {
      return new R(employeeInfoService.queryEmployeeDetail(req));
    }
    /**
     * 修改采购员工信息
     *
     * @param req
     * @return
     */
    @PostMapping("/modifyEmployee")
    public R<Boolean> modifyEmployeeDetail(@Validated PurchaseModifyEmployeeReq req) {
        return new R(employeeInfoService.modifyEmployeeDetail(req));
    }
    /**
     * 查询采购员工列表 管理商品
     *
     * @param req
     * @return
     */
    @GetMapping("/queryEmployeeGoodsList")
    public R<Page> queryEmployeeGoodsList(@Validated PurchaseEmployeeGoodsReq req) {
        return new R(employeeInfoService.queryEmployeeGoodsList(req));
    }
    /**
     * 查询采购员工列表 管理供应商
     *
     * @param req
     * @return
     */
    @GetMapping("/queryEmployeeSupplierList")
    public R<Page> queryEmployeeSupplierList(@Validated PurchaseEmployeeSupplierReq req) {
        return new R(employeeInfoService.queryEmployeeSupplierList(req));
    }
    /**
     * 查询采购员工 资金扣减记录
     *
     * @param req
     * @return
     */
    @GetMapping("/queryEmployeeBillList")
    @ScmJsonFilter(type = DealRecord.class)
    public R<Page> queryEmployeeBillList(@Validated PurchaseEmployeeBillReq req) {
        return new R(employeeInfoService.queryEmployeeBillList(req));
    }

    /**
     * 采购员工新增供应商
     * @param req
     * @return
     */
    @PostMapping("/addSupplier")
    @ApiOperation(value = "采购员工新增供应商")
    public R addSupplier(@Validated BuyerAddSupplierReq req){

        return new R<>(employeeInfoService.addSupplier(req));
    }

    /**
     * 新增采购员工
     *
     * @param req
     * @return
     */
    @PostMapping("/addEmployee")
    @ApiOperation(value = "新增采购部员工")
    public R addEmployee(@Validated PurchaseAddEmployeeReq req) {
        PurchaseEmployee employee = new PurchaseEmployee();
        BeanUtil.copyProperties(req, employee);
        return employeeInfoService.addPurchaseEmployee(employee);
    }

    /**
     * 采购员工绑定系统用户 根据手机号
     *
     * @param telephone
     * @return
     */
    @PostMapping("/relationUser")
    @ApiOperation(value = "采购员工绑定系统用户")
    @ApiImplicitParam(paramType = "query", dataType = "String", name = "telephone", value = "手机号", required = true)
    public R relationEmployeeUser(@RequestParam(value = "telephone") String telephone) {
        if(StrUtil.isEmpty(telephone)){
            return RUtil.error().setMsg("telephone不能为空");
        }
        return employeeInfoService.relationEmployeeUser(telephone);
    }

    /**
     * 查询采购员工列表
     *
     * @param req
     * @return
     */
    @PostMapping("/queryEmployeeList")
    @ApiOperation(value = "查询采购员工列表")
    public R queryEmployeeList(@RequestBody @Validated QueryEmployeeListReq req) {
        PurchaseEmployee purchaseEmployee = new PurchaseEmployee();
        purchaseEmployee.setPurchaseEmployeeType(req.getPurchaseEmployeeType());
        purchaseEmployee.setPmId(req.getPmId());
        return new R<>(purchaseEmployeeService.list(Wrappers.query(purchaseEmployee)));
    }

}
