package com.haohan.cloud.scm.aftersales.api.ctrl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.aftersales.core.ScmAfterSalesService;
import com.haohan.cloud.scm.aftersales.service.AfterSalesService;
import com.haohan.cloud.scm.aftersales.utils.ScmAfterSalesUtils;
import com.haohan.cloud.scm.api.aftersales.entity.AfterSales;
import com.haohan.cloud.scm.api.aftersales.req.QueryAfterSalesDetailReq;
import com.haohan.cloud.scm.api.aftersales.req.QueryAfterSalesListReq;
import com.pig4cloud.pigx.common.core.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @author xwx
 * @date 2019/7/22
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/afterSales")
@Api(value = "ApiAfterSales", tags = "afterSales售后单管理")
public class AfterSalesApiCtrl {

    private final AfterSalesService afterSalesService;
    private final ScmAfterSalesUtils scmAfterSalesUtils;
    private final ScmAfterSalesService scmAfterSalesService;

    /**
     * 查询售后单列表
     * @param req
     * @return
     */
    @PostMapping("/queryAfterSalesList")
    @ApiOperation(value = "查询售后单列表--小程序" , notes = "按售后状态分类查询")
    public R<Page> queryAfterSalesList(@RequestBody @Validated QueryAfterSalesListReq req) {
        scmAfterSalesUtils.queryUPassportById(req.getUid());
        Page page = new Page(req.getCurrent(), req.getSize());
        AfterSales afterSales = new AfterSales();
        afterSales.setPmId(req.getPmId());
        afterSales.setAfterSalesStatus(req.getAfterSalesStatus());
        return new R(afterSalesService.page(page, Wrappers.query(afterSales)));
    }

    /**
     * 查询售后单详情
     * @param req
     * @return
     */
    @GetMapping("/queryAfterSalesDetail")
    @ApiOperation(value = "查询售后单详情")
    public R queryAfterSalesDetail(@Validated QueryAfterSalesDetailReq req) {
        return new R<>(scmAfterSalesService.queryAfterSalesDetail(req));
    }
}
