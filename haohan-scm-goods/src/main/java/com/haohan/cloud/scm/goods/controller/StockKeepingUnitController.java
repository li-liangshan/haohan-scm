/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.goods.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.goods.entity.StockKeepingUnit;
import com.haohan.cloud.scm.api.goods.req.StockKeepingUnitReq;
import com.haohan.cloud.scm.goods.service.StockKeepingUnitService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 库存商品库
 *
 * @author haohan
 * @date 2019-05-29 14:27:56
 */
@RestController
@AllArgsConstructor
@RequestMapping("/stockkeepingunit" )
@Api(value = "stockkeepingunit", tags = "stockkeepingunit管理")
public class StockKeepingUnitController {

    private final StockKeepingUnitService stockKeepingUnitService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param stockKeepingUnit 库存商品库
     * @return
     */
    @GetMapping("/page" )
    public R getStockKeepingUnitPage(Page page, StockKeepingUnit stockKeepingUnit) {
        return new R<>(stockKeepingUnitService.page(page, Wrappers.query(stockKeepingUnit)));
    }


    /**
     * 通过id查询库存商品库
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(stockKeepingUnitService.getById(id));
    }

    /**
     * 新增库存商品库
     * @param stockKeepingUnit 库存商品库
     * @return R
     */
    @SysLog("新增库存商品库" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_stockkeepingunit_add')" )
    public R save(@RequestBody StockKeepingUnit stockKeepingUnit) {
        return new R<>(stockKeepingUnitService.save(stockKeepingUnit));
    }

    /**
     * 修改库存商品库
     * @param stockKeepingUnit 库存商品库
     * @return R
     */
    @SysLog("修改库存商品库" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_stockkeepingunit_edit')" )
    public R updateById(@RequestBody StockKeepingUnit stockKeepingUnit) {
        return new R<>(stockKeepingUnitService.updateById(stockKeepingUnit));
    }

    /**
     * 通过id删除库存商品库
     * @param id id
     * @return R
     */
    @SysLog("删除库存商品库" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_stockkeepingunit_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(stockKeepingUnitService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除库存商品库")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_stockkeepingunit_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(stockKeepingUnitService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询库存商品库")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(stockKeepingUnitService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param stockKeepingUnitReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询库存商品库总记录}")
    @PostMapping("/countByStockKeepingUnitReq")
    public R countByStockKeepingUnitReq(@RequestBody StockKeepingUnitReq stockKeepingUnitReq) {

        return new R<>(stockKeepingUnitService.count(Wrappers.query(stockKeepingUnitReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param stockKeepingUnitReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据stockKeepingUnitReq查询一条货位信息表")
    @PostMapping("/getOneByStockKeepingUnitReq")
    public R getOneByStockKeepingUnitReq(@RequestBody StockKeepingUnitReq stockKeepingUnitReq) {

        return new R<>(stockKeepingUnitService.getOne(Wrappers.query(stockKeepingUnitReq), false));
    }


    /**
     * 批量修改OR插入
     * @param stockKeepingUnitList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_stockkeepingunit_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<StockKeepingUnit> stockKeepingUnitList) {

        return new R<>(stockKeepingUnitService.saveOrUpdateBatch(stockKeepingUnitList));
    }


}
