package com.haohan.cloud.scm.common.message.service;


import com.haohan.cloud.scm.common.message.entity.WechatMessageNotify;

/**
 * @author cx
 * @date 2019/6/19
 */
public interface WechatMessageService {

    String wechatNotify(WechatMessageNotify notify);
}
