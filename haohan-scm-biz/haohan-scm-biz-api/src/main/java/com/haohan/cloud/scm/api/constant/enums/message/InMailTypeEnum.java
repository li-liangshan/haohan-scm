package com.haohan.cloud.scm.api.constant.enums.message;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dy
 * @date 2020/1/14
 */
@Getter
@AllArgsConstructor
public enum InMailTypeEnum implements IBaseEnum {

    /**
     * 站内信类型:1.公告 2.及时通信
     */
    notice("1", "公告"),
    instant("2", "及时通信");

    private static final Map<String, InMailTypeEnum> MAP = new HashMap<>(8);

    static {
        for (InMailTypeEnum e : InMailTypeEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static InMailTypeEnum getByType(String type) {
        return MAP.get(type);
    }

    @EnumValue
    @JsonValue
    private String type;
    private String desc;
}
