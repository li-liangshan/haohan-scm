package com.haohan.cloud.scm.api.supply.req.goods;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author dy
 * @date 2020/4/18
 */
@Data
public class SupplyGoodsPlatformSalesReq {

    @NotBlank(message = "供应商店铺id不能为空 ")
    @ApiModelProperty(value = "供应商店铺id")
    private String shopId;

    @NotBlank(message = "供应商品id不能为空")
    @Length(max = 32, message = "供应商品id的最大长度为32字符")
    @ApiModelProperty(value = "供应商品id")
    private String supplyGoodsId;

    @Length(max = 32, message = "平台店铺id的最大长度为32字符")
    @ApiModelProperty(value = "平台店铺id")
    private String platformShopId;

}
