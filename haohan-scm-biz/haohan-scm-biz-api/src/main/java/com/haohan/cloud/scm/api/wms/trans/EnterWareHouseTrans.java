package com.haohan.cloud.scm.api.wms.trans;

import cn.hutool.core.bean.BeanUtil;
import com.haohan.cloud.scm.api.constant.enums.product.EnterStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.product.EnterTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.product.StorageTypeEnum;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseOrderDetail;
import com.haohan.cloud.scm.api.wms.entity.EnterWarehouse;
import com.haohan.cloud.scm.api.wms.entity.EnterWarehouseDetail;
import com.haohan.cloud.scm.api.wms.req.EnterWarehouseByOrderDetailReq;

import java.time.LocalDateTime;

/**
 * @program: haohan-fresh-scm
 * @description: 入库对象转换
 * @author: Simon
 * @create: 2019-05-27
 **/
public class EnterWareHouseTrans {


    public static EnterWarehouseDetail trans(PurchaseOrderDetail orderDetail) {

        EnterWarehouseDetail warehouseDetail = new EnterWarehouseDetail();

        BeanUtil.copyProperties(orderDetail, warehouseDetail);
//    warehouse.setEnterWarehouseSn();//TODO 前端业务统一设置
//    warehouseDetail.setEnterWarehouseDetailSn();// TODO 全局设置生成规则

        warehouseDetail.setApplyTime(LocalDateTime.now());


        return warehouseDetail;

    }


    public static EnterWarehouse enterWarehouseTrans(EnterWarehouseByOrderDetailReq req) {
        EnterWarehouse enterWarehouse = new EnterWarehouse();
        enterWarehouse.setPmId(req.getPmId());
        enterWarehouse.setPurchaseSn(req.getPurchaseSn());
        enterWarehouse.setApplyTime(LocalDateTime.now());
        enterWarehouse.setEnterType(EnterTypeEnum.purchaseEnter);
        enterWarehouse.setEnterStatus(EnterStatusEnum.handle);
        enterWarehouse.setStorageType(StorageTypeEnum.enterwarehouse);
        return enterWarehouse;
    }

}
