/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.opc.req;

import com.haohan.cloud.scm.api.opc.entity.CostControl;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 成本管控
 *
 * @author haohan
 * @date 2019-05-30 10:16:37
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "成本管控")
public class CostControlReq extends CostControl {

    private long pageSize;
    private long pageNo;




}
