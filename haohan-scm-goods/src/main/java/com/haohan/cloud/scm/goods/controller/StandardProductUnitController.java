/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.goods.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.goods.entity.StandardProductUnit;
import com.haohan.cloud.scm.api.goods.req.StandardProductUnitReq;
import com.haohan.cloud.scm.goods.service.StandardProductUnitService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 标准商品库
 *
 * @author haohan
 * @date 2019-05-29 14:26:47
 */
@RestController
@AllArgsConstructor
@RequestMapping("/standardproductunit" )
@Api(value = "standardproductunit", tags = "standardproductunit管理")
public class StandardProductUnitController {

    private final StandardProductUnitService standardProductUnitService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param standardProductUnit 标准商品库
     * @return
     */
    @GetMapping("/page" )
    public R getStandardProductUnitPage(Page page, StandardProductUnit standardProductUnit) {
        return new R<>(standardProductUnitService.page(page, Wrappers.query(standardProductUnit)));
    }


    /**
     * 通过id查询标准商品库
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(standardProductUnitService.getById(id));
    }

    /**
     * 新增标准商品库
     * @param standardProductUnit 标准商品库
     * @return R
     */
    @SysLog("新增标准商品库" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_standardproductunit_add')" )
    public R save(@RequestBody StandardProductUnit standardProductUnit) {
        return new R<>(standardProductUnitService.save(standardProductUnit));
    }

    /**
     * 修改标准商品库
     * @param standardProductUnit 标准商品库
     * @return R
     */
    @SysLog("修改标准商品库" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_standardproductunit_edit')" )
    public R updateById(@RequestBody StandardProductUnit standardProductUnit) {
        return new R<>(standardProductUnitService.updateById(standardProductUnit));
    }

    /**
     * 通过id删除标准商品库
     * @param id id
     * @return R
     */
    @SysLog("删除标准商品库" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_standardproductunit_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(standardProductUnitService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除标准商品库")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_standardproductunit_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(standardProductUnitService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询标准商品库")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(standardProductUnitService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param standardProductUnitReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询标准商品库总记录}")
    @PostMapping("/countByStandardProductUnitReq")
    public R countByStandardProductUnitReq(@RequestBody StandardProductUnitReq standardProductUnitReq) {

        return new R<>(standardProductUnitService.count(Wrappers.query(standardProductUnitReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param standardProductUnitReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据standardProductUnitReq查询一条货位信息表")
    @PostMapping("/getOneByStandardProductUnitReq")
    public R getOneByStandardProductUnitReq(@RequestBody StandardProductUnitReq standardProductUnitReq) {

        return new R<>(standardProductUnitService.getOne(Wrappers.query(standardProductUnitReq), false));
    }


    /**
     * 批量修改OR插入
     * @param standardProductUnitList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_standardproductunit_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<StandardProductUnit> standardProductUnitList) {

        return new R<>(standardProductUnitService.saveOrUpdateBatch(standardProductUnitList));
    }


}
