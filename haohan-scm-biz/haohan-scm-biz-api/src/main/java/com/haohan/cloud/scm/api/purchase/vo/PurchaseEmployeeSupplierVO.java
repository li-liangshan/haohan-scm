package com.haohan.cloud.scm.api.purchase.vo;

import lombok.Data;

/**
 * @author dy
 * @date 2019/5/18
 */
@Data
public class PurchaseEmployeeSupplierVO {
    /**
     * 主键
     */
    private String id;
    /**
     * 员工id
     */
    private String employeeId;
    /**
     * 供应商id
     */
    private String supplierId;
    /**
     * 启用状态:0.未启用1.启用
     */
    private String useStatus;

    /**
     * 商家名称
     */
    private String merchantName;
    /**
     * 全称
     */
    private String supplierName;
    /**
     * 供应商名称
     */
    private String shortName;
    /**
     * 联系人
     */
    private String contact;
    /**
     * 联系电话
     */
    private String telephone;
    /**
     * 账期
     */
    private String payPeriod;
    /**
     * 供应商地址
     */
    private String address;
    /**
     * 标签
     */
    private String tags;
    /**
     * 是否启用
     */
    private String status;
    /**
     * 供应商类型
     */
    private String supplierType;
    /**
     * 是否开启消息推送
     */
    private String needPush;


}
