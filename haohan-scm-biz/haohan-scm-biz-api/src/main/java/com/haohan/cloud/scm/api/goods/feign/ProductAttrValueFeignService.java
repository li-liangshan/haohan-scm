/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.goods.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.goods.entity.ProductAttrValue;
import com.haohan.cloud.scm.api.goods.req.ProductAttrValueReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 商品库属性值内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "ProductAttrValueFeignService", value = ScmServiceName.SCM_GOODS)
public interface ProductAttrValueFeignService {


    /**
     * 通过id查询商品库属性值
     *
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/ProductAttrValue/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 商品库属性值 列表信息
     *
     * @param productAttrValueReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/ProductAttrValue/fetchProductAttrValuePage")
    R getProductAttrValuePage(@RequestBody ProductAttrValueReq productAttrValueReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 商品库属性值 列表信息
     *
     * @param productAttrValueReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/ProductAttrValue/fetchProductAttrValueList")
    R getProductAttrValueList(@RequestBody ProductAttrValueReq productAttrValueReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增商品库属性值
     *
     * @param productAttrValue 商品库属性值
     * @return R
     */
    @PostMapping("/api/feign/ProductAttrValue/add")
    R save(@RequestBody ProductAttrValue productAttrValue, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改商品库属性值
     *
     * @param productAttrValue 商品库属性值
     * @return R
     */
    @PostMapping("/api/feign/ProductAttrValue/update")
    R updateById(@RequestBody ProductAttrValue productAttrValue, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除商品库属性值
     *
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/ProductAttrValue/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/ProductAttrValue/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/ProductAttrValue/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param productAttrValueReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/ProductAttrValue/countByProductAttrValueReq")
    R countByProductAttrValueReq(@RequestBody ProductAttrValueReq productAttrValueReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param productAttrValueReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/ProductAttrValue/getOneByProductAttrValueReq")
    R getOneByProductAttrValueReq(@RequestBody ProductAttrValueReq productAttrValueReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param productAttrValueList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/ProductAttrValue/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<ProductAttrValue> productAttrValueList, @RequestHeader(SecurityConstants.FROM) String from);


}
