package com.haohan.cloud.scm.api.crm.dto;

import com.haohan.cloud.scm.api.common.dto.SelfSqlDTO;
import com.haohan.cloud.scm.api.crm.entity.MarketEmployee;
import com.haohan.cloud.scm.api.crm.entity.SalesArea;
import com.pig4cloud.pigx.admin.api.entity.SysDept;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author dy
 * @date 2019/12/23
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SalesAreaSqlDTO extends SelfSqlDTO {

    @ApiModelProperty(value = "区域编号列表,逗号连接")
    private String areaSns;

    @ApiModelProperty(value = "类名")
    private String className;

    // 查询 员工负责区域使用

    private String employeeId;
    private String employeeClassName;
    private String deptId;
    private String deptClassName;

    public SalesAreaSqlDTO() {
        super();
        this.className = SalesArea.class.getSimpleName();
    }

    public SalesAreaSqlDTO(String employeeId, String deptId) {
        super();
        this.deptClassName = SysDept.class.getSimpleName();
        this.employeeClassName = MarketEmployee.class.getSimpleName();
        this.employeeId = employeeId;
        this.deptId = deptId;

    }
}
