package com.haohan.cloud.scm.api.supply.req;

import com.haohan.cloud.scm.api.constant.enums.supply.ShipStatusEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author xwx
 * @date 2019/7/27
 */
@Data
@ApiModel(description = "修改报价单")
public class UpdateOfferOrderReq {
    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台id",required = true)
    private String pmId;

    @NotBlank(message = "uid不能为空")
    @ApiModelProperty(value = "通行证id",required = true)
    private String uid;

    @NotBlank(message = "id不能为空")
    @ApiModelProperty(value = "报价单id",required = true)
    private String id;

    @NotBlank(message = "shipStatus不能为空")
    @ApiModelProperty(value = "发货状态:0不需发货1.待备货2.备货中3.已发货4.已接收5.售后中",required = true)
    private ShipStatusEnum shipStatus;

}
