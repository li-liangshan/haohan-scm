package com.haohan.cloud.scm.api.supply.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author xwx
 * @date 2019/5/17
 */
@Data
@ApiModel(description = "查询供应商 售卖商品信息列表")
public class SupplierGoodsListReq {
    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台id",required = true)
    private String pmId;

    @NotBlank(message = "supplierId不能为空")
    @ApiModelProperty(value = "供应商id",required = true)
    private String supplierId;

    @ApiModelProperty(value = "采购员uid" )
    private String uid;

    @ApiModelProperty(value = "每页显示条数" )
    private long pageSize=10;

    @ApiModelProperty(value = "页码" )
    private long pageNo=1;
}
