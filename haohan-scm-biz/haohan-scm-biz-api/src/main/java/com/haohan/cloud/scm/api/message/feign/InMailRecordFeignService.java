/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.message.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.message.dto.SendInMailMessageDTO;
import com.haohan.cloud.scm.api.message.entity.InMailRecord;
import com.haohan.cloud.scm.api.message.req.InMailRecordReq;
import com.haohan.cloud.scm.api.message.vo.MsgInfoVO;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 站内信记录表内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "InMailRecordFeignService", value = ScmServiceName.SCM_BIZ_MSG)
public interface InMailRecordFeignService {


    /**
     * 通过id查询站内信记录表
     *
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/InMailRecord/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 站内信记录表 列表信息
     *
     * @param inMailRecordReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/InMailRecord/fetchInMailRecordPage")
    R getInMailRecordPage(@RequestBody InMailRecordReq inMailRecordReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 站内信记录表 列表信息
     *
     * @param inMailRecordReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/InMailRecord/fetchInMailRecordList")
    R getInMailRecordList(@RequestBody InMailRecordReq inMailRecordReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增站内信记录表
     *
     * @param inMailRecord 站内信记录表
     * @return R
     */
    @PostMapping("/api/feign/InMailRecord/add")
    R save(@RequestBody InMailRecord inMailRecord, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改站内信记录表
     *
     * @param inMailRecord 站内信记录表
     * @return R
     */
    @PostMapping("/api/feign/InMailRecord/update")
    R updateById(@RequestBody InMailRecord inMailRecord, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除站内信记录表
     *
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/InMailRecord/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/InMailRecord/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/InMailRecord/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param inMailRecordReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/InMailRecord/countByInMailRecordReq")
    R countByInMailRecordReq(@RequestBody InMailRecordReq inMailRecordReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param inMailRecordReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/InMailRecord/getOneByInMailRecordReq")
    R getOneByInMailRecordReq(@RequestBody InMailRecordReq inMailRecordReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param inMailRecordList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/InMailRecord/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<InMailRecord> inMailRecordList, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 查询消息详情
     *
     * @param messageSn
     * @param from
     * @return
     */
    @GetMapping(value = "/api/feign/InMailRecord/fetchInfo/{messageSn}")
    R<MsgInfoVO> fetchInfo(@PathVariable("messageSn") String messageSn, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 发送站内信
     *
     * @param message
     * @param from
     * @return
     */
    @PostMapping("/api/feign/InMailRecord/sendMessage")
    R<Boolean> sendInMailMessage(@RequestBody SendInMailMessageDTO message, @RequestHeader(SecurityConstants.FROM) String from);

    @PostMapping("/api/feign/InMailRecord/modifyByDeleteSource")
    R<Boolean> modifyByDeleteSource(@RequestBody SendInMailMessageDTO message, @RequestHeader(SecurityConstants.FROM) String from);

}
