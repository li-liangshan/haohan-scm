/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.aftersales.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.aftersales.mapper.SalesReturnMapper;
import com.haohan.cloud.scm.aftersales.service.SalesReturnService;
import com.haohan.cloud.scm.api.aftersales.entity.SalesReturn;
import org.springframework.stereotype.Service;

/**
 * 售后退货记录
 *
 * @author haohan
 * @date 2019-05-13 20:29:00
 */
@Service
public class SalesReturnServiceImpl extends ServiceImpl<SalesReturnMapper, SalesReturn> implements SalesReturnService {

}
