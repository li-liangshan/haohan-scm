/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.goods.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.goods.entity.ProductCategory;
import com.haohan.cloud.scm.api.goods.req.ProductCategoryReq;
import com.haohan.cloud.scm.goods.service.ProductCategoryService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 商品库分类
 *
 * @author haohan
 * @date 2019-05-29 14:26:32
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/ProductCategory")
@Api(value = "productcategory", tags = "productcategory内部接口服务")
public class ProductCategoryFeignApiCtrl {

    private final ProductCategoryService productCategoryService;


    /**
     * 通过id查询商品库分类
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(productCategoryService.getById(id));
    }


    /**
     * 分页查询 商品库分类 列表信息
     * @param productCategoryReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchProductCategoryPage")
    public R getProductCategoryPage(@RequestBody ProductCategoryReq productCategoryReq) {
        Page page = new Page(productCategoryReq.getPageNo(), productCategoryReq.getPageSize());
        ProductCategory productCategory =new ProductCategory();
        BeanUtil.copyProperties(productCategoryReq, productCategory);

        return new R<>(productCategoryService.page(page, Wrappers.query(productCategory)));
    }


    /**
     * 全量查询 商品库分类 列表信息
     * @param productCategoryReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchProductCategoryList")
    public R getProductCategoryList(@RequestBody ProductCategoryReq productCategoryReq) {
        ProductCategory productCategory =new ProductCategory();
        BeanUtil.copyProperties(productCategoryReq, productCategory);

        return new R<>(productCategoryService.list(Wrappers.query(productCategory)));
    }


    /**
     * 新增商品库分类
     * @param productCategory 商品库分类
     * @return R
     */
    @Inner
    @SysLog("新增商品库分类")
    @PostMapping("/add")
    public R save(@RequestBody ProductCategory productCategory) {
        return new R<>(productCategoryService.save(productCategory));
    }

    /**
     * 修改商品库分类
     * @param productCategory 商品库分类
     * @return R
     */
    @Inner
    @SysLog("修改商品库分类")
    @PostMapping("/update")
    public R updateById(@RequestBody ProductCategory productCategory) {
        return new R<>(productCategoryService.updateById(productCategory));
    }

    /**
     * 通过id删除商品库分类
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除商品库分类")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(productCategoryService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除商品库分类")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(productCategoryService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询商品库分类")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(productCategoryService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param productCategoryReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询商品库分类总记录}")
    @PostMapping("/countByProductCategoryReq")
    public R countByProductCategoryReq(@RequestBody ProductCategoryReq productCategoryReq) {

        return new R<>(productCategoryService.count(Wrappers.query(productCategoryReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param productCategoryReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据productCategoryReq查询一条货位信息表")
    @PostMapping("/getOneByProductCategoryReq")
    public R getOneByProductCategoryReq(@RequestBody ProductCategoryReq productCategoryReq) {

        return new R<>(productCategoryService.getOne(Wrappers.query(productCategoryReq), false));
    }


    /**
     * 批量修改OR插入
     * @param productCategoryList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<ProductCategory> productCategoryList) {

        return new R<>(productCategoryService.saveOrUpdateBatch(productCategoryList));
    }

}
