/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.goods.req;

import com.haohan.cloud.scm.api.goods.entity.GoodsPromotion;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 商品促销库
 *
 * @author haohan
 * @date 2019-08-30 11:45:09
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "商品促销库")
public class GoodsPromotionReq extends GoodsPromotion {

    private long pageSize;
    private long pageNo;


}
