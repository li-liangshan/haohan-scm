/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.saleb.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.constant.enums.saleb.BuyOrderStatusEnum;
import com.haohan.cloud.scm.api.saleb.dto.BuyOrderDetailDTO;
import com.haohan.cloud.scm.api.saleb.dto.BuyOrderSqlDTO;
import com.haohan.cloud.scm.api.saleb.dto.GoodsProfitDTO;
import com.haohan.cloud.scm.api.saleb.entity.BuyOrderDetail;
import com.haohan.cloud.scm.api.saleb.req.QuerySummaryOrderDetailReq;

import java.math.BigDecimal;
import java.util.List;

/**
 * 采购单明细
 *
 * @author haohan
 * @date 2019-05-13 20:34:19
 */
public interface BuyOrderDetailService extends IService<BuyOrderDetail> {

    /**
     * 汇总商品下单数量
     *
     * @param buyOrderDetailDTO 必需参数:
     *                          pmId/buySeq/goodsModelId/status/
     *                          deliveryTime/summaryFlag
     * @return
     */
    BuyOrderDetailDTO summaryGoodsNum(BuyOrderDetailDTO buyOrderDetailDTO);

    /**
     * 汇总商品下单列表
     *
     * @param buyOrderDetailDTO
     */
    List<BuyOrderDetailDTO> summaryGoodsNumList(BuyOrderDetailDTO buyOrderDetailDTO);


    /**
     * 查询 采购单明细 关联采购单
     * 采购单 参数: buySeq/deliveryTime
     * 明细 参数: summaryFlag/status/buyId/buyDetailSn/pmId/id
     *
     * @param buyOrderDetailDTO
     * @return
     */
    BuyOrderDetailDTO getOneWithOrder(BuyOrderDetailDTO buyOrderDetailDTO);


    /**
     * 查询 采购单明细列表 关联采购单
     * 采购单 参数: buySeq/deliveryTime
     * 明细 参数: summaryFlag/status/buyId/buyDetailSn/pmId/id
     *
     * @param buyOrderDetailDTO
     * @return
     */
    List<BuyOrderDetailDTO> getListWithOrder(BuyOrderDetailDTO buyOrderDetailDTO);

    /**
     * 查询商品毛利
     *
     * @param pmId
     * @return
     */
    List<GoodsProfitDTO> queryGoodsProfit(String pmId);

    /**
     * 查询汇总单对应汇总详情
     *
     * @param req
     * @return
     */
    List<BuyOrderDetail> queryWaitSummaryOrderDetail(QuerySummaryOrderDetailReq req);

    /**
     * 查询商品数量
     *
     * @param buyId
     * @return
     */
    BigDecimal queryPriceSum(String buyId);

    /**
     * 根据编号查询采购明细
     *
     * @param buyDetailSn
     * @return
     */
    BuyOrderDetail fetchBySn(String buyDetailSn);

    List<BuyOrderDetail> findListBySn(String buyOrderSn);

    List<BuyOrderDetail> queryOftenModelIds(BuyOrderSqlDTO query);

    /**
     * 修改订单明细的订单状态(不包括已取消)
     *
     * @param buyOrderSn
     * @param status     (修改后状态)
     * @return
     */
    boolean updateStatusByOrderSn(String buyOrderSn, BuyOrderStatusEnum status);
}
