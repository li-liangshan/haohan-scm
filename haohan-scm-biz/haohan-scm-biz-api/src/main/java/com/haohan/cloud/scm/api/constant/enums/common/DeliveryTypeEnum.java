package com.haohan.cloud.scm.api.constant.enums.common;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dy
 * @date 2019/5/31
 */
@Getter
@AllArgsConstructor
public enum DeliveryTypeEnum implements IBaseEnum {
    /**
     * 配送方式:0快递1自提9送货上门
     */
    express("0", "快递"),
    self_delivery("1", "自提"),
    home_delivery("9", "送货上门");

    private static final Map<String, DeliveryTypeEnum> MAP = new HashMap<>(8);

    static {
        for (DeliveryTypeEnum e : DeliveryTypeEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static DeliveryTypeEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

    /**
     * 类型
     */
    @EnumValue
    @JsonValue
    private String type;
    /**
     * 描述
     */
    private String desc;
}
