/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.saleb.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.saleb.entity.BuyOrder;
import com.haohan.cloud.scm.api.saleb.req.BuyOrderReq;
import com.haohan.cloud.scm.api.saleb.req.QueryBuyOrderReq;
import com.haohan.cloud.scm.api.saleb.vo.BuyOrderVO;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.haohan.cloud.scm.saleb.core.BuyOrderCoreService;
import com.haohan.cloud.scm.saleb.service.BuyOrderService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 采购单
 *
 * @author haohan
 * @date 2019-05-29 13:29:53
 */
@RestController
@AllArgsConstructor
@RequestMapping("/buyorder" )
@Api(value = "buyorder", tags = "buyorder管理")
public class BuyOrderController {

    private final BuyOrderService buyOrderService;
    private final BuyOrderCoreService buyOrderCoreService;

    /**
     * 分页查询
     *
     * @param page 分页对象
     * @param req  采购单
     * @return
     */
    @GetMapping("/page")
    public R<IPage<BuyOrderVO>> getBuyOrderPage(Page<BuyOrder> page, QueryBuyOrderReq req) {
        return RUtil.success(buyOrderCoreService.findPage(page, req));
    }


    /**
     * 通过id查询采购单
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(buyOrderService.getById(id));
    }

    /**
     * 新增采购单
     * @param buyOrder 采购单
     * @return R
     */
    @SysLog("新增采购单" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_buyorder_add')" )
    public R save(@RequestBody BuyOrder buyOrder) {
        return new R<>(buyOrderService.save(buyOrder));
    }

    /**
     * 修改采购单
     * @param buyOrder 采购单
     * @return R
     */
    @SysLog("修改采购单" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_buyorder_edit')" )
    public R updateById(@RequestBody BuyOrder buyOrder) {
        return new R<>(buyOrderService.updateById(buyOrder));
    }

    /**
     * 通过id删除采购单
     * @param id id
     * @return R
     */
    @SysLog("删除采购单" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_buyorder_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(buyOrderService.removeById(id));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(buyOrderService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param buyOrderReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/countByBuyOrderReq")
    public R countByBuyOrderReq(@RequestBody BuyOrderReq buyOrderReq) {

        return new R<>(buyOrderService.count(Wrappers.query(buyOrderReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param buyOrderReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/getOneByBuyOrderReq")
    public R getOneByBuyOrderReq(@RequestBody BuyOrderReq buyOrderReq) {

        return new R<>(buyOrderService.getOne(Wrappers.query(buyOrderReq), false));
    }


}
