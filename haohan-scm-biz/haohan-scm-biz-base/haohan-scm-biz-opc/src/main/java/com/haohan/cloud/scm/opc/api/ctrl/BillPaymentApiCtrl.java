package com.haohan.cloud.scm.opc.api.ctrl;

import com.haohan.cloud.scm.api.opc.dto.BillPaymentInfoDTO;
import com.haohan.cloud.scm.api.opc.req.payment.QueryBillInfoReq;
import com.haohan.cloud.scm.opc.core.BillPaymentCoreService;
import com.pig4cloud.pigx.common.core.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author dy
 * @date 2019/9/21
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/opc/billPayment")
@Api(value = "ApiBillPayment", tags = "采购商货款统计 应收账单")
public class BillPaymentApiCtrl {

    private final BillPaymentCoreService billPaymentCoreService;

    @GetMapping("/fetchBillInfo")
    @ApiOperation(value = "查询账单详情")
    public R<BillPaymentInfoDTO> queryBillInfo(@Validated QueryBillInfoReq req){
        if(null == req.getSettlementType()){
            return R.failed(null, "缺少账单类型");
        }
        return R.ok(billPaymentCoreService.fetchBillInfo(req.transTo()));
    }

}
