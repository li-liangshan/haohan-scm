/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.opc.feign;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.opc.entity.SettlementRelation;
import com.haohan.cloud.scm.api.opc.req.settlement.SettlementRelationReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 结算单账单关系表内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "SettlementRelationFeignService", value = ScmServiceName.SCM_BIZ_OPC)
public interface SettlementRelationFeignService {


    /**
     * 通过id查询结算单账单关系表
     *
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/SettlementRelation/{id}", method = RequestMethod.GET)
    R<SettlementRelation> getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 结算单账单关系表 列表信息
     *
     * @param settlementRelationReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/SettlementRelation/fetchSettlementRelationPage")
    R<Page<SettlementRelation>> getSettlementRelationPage(@RequestBody SettlementRelationReq settlementRelationReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 结算单账单关系表 列表信息
     *
     * @param settlementRelationReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/SettlementRelation/fetchSettlementRelationList")
    R<List<SettlementRelation>> getSettlementRelationList(@RequestBody SettlementRelationReq settlementRelationReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增结算单账单关系表
     *
     * @param settlementRelation 结算单账单关系表
     * @return R
     */
    @PostMapping("/api/feign/SettlementRelation/add")
    R<Boolean> save(@RequestBody SettlementRelation settlementRelation, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改结算单账单关系表
     *
     * @param settlementRelation 结算单账单关系表
     * @return R
     */
    @PostMapping("/api/feign/SettlementRelation/update")
    R<Boolean> updateById(@RequestBody SettlementRelation settlementRelation, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除结算单账单关系表
     *
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/SettlementRelation/delete/{id}")
    R<Boolean> removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/SettlementRelation/batchDelete")
    R<Boolean> removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/SettlementRelation/listByIds")
    R<List<SettlementRelation>> listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param settlementRelationReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/SettlementRelation/countBySettlementRelationReq")
    R<Integer> countBySettlementRelationReq(@RequestBody SettlementRelationReq settlementRelationReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param settlementRelationReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/SettlementRelation/getOneBySettlementRelationReq")
    R<SettlementRelation> getOneBySettlementRelationReq(@RequestBody SettlementRelationReq settlementRelationReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param settlementRelationList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/SettlementRelation/saveOrUpdateBatch")
    R<Boolean> saveOrUpdateBatch(@RequestBody List<SettlementRelation> settlementRelationList, @RequestHeader(SecurityConstants.FROM) String from);


}
