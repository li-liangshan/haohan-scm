package com.haohan.cloud.scm.api.manage.req;

import com.haohan.cloud.scm.api.constant.enums.manage.MerchantStatusEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Set;

/**
 * @author dy
 * @date 2020/4/23
 */
@Data
public class MerchantFeignReq implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "商家id列表")
    private Set<String> merchantIdSet;

    @ApiModelProperty(value = "商家启用状态", notes = "原系统使用字典 2启用  0 待审核 -1 停用")
    private MerchantStatusEnum status;

}
