/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.goods.api.feign;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.haohan.cloud.scm.api.goods.dto.GoodsCategoryTree;
import com.haohan.cloud.scm.api.goods.entity.GoodsCategory;
import com.haohan.cloud.scm.api.goods.req.GoodsCategoryReq;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.haohan.cloud.scm.goods.core.GoodsCategoryCoreService;
import com.haohan.cloud.scm.goods.service.GoodsCategoryService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 商品分类
 *
 * @author haohan
 * @date 2019-05-29 14:25:22
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/GoodsCategory")
@Api(value = "goodscategory", tags = "goodscategory内部接口服务")
public class GoodsCategoryFeignApiCtrl {

    private final GoodsCategoryService goodsCategoryService;
    private final GoodsCategoryCoreService categoryCoreService;


    /**
     * 通过id查询商品分类
     *
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(goodsCategoryService.getById(id));
    }


//    /**
//     * 分页查询 商品分类 列表信息
//     * @param goodsCategoryReq 请求对象
//     * @return
//     */
//    @Inner
//    @PostMapping("/fetchGoodsCategoryPage")
//    public R getGoodsCategoryPage(@RequestBody GoodsCategoryReq goodsCategoryReq) {
//        Page page = new Page(goodsCategoryReq.getPageNo(), goodsCategoryReq.getPageSize());
//        GoodsCategory goodsCategory =new GoodsCategory();
//        BeanUtil.copyProperties(goodsCategoryReq, goodsCategory);
//
//        return new R<>(goodsCategoryService.page(page, Wrappers.query(goodsCategory)));
//    }


    /**
     * 全量查询 商品分类 列表信息
     *
     * @param goodsCategoryReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchGoodsCategoryList")
    public R<List<GoodsCategory>> getGoodsCategoryList(@RequestBody GoodsCategoryReq goodsCategoryReq) {
        return RUtil.success(goodsCategoryService.list(Wrappers.<GoodsCategory>query(goodsCategoryReq)));
    }


//    /**
//     * 新增商品分类
//     * @param goodsCategory 商品分类
//     * @return R
//     */
//    @Inner
//    @SysLog("新增商品分类")
//    @PostMapping("/add")
//    public R save(@RequestBody GoodsCategory goodsCategory) {
//        return new R<>(goodsCategoryService.save(goodsCategory));
//    }
//
//    /**
//     * 修改商品分类
//     * @param goodsCategory 商品分类
//     * @return R
//     */
//    @Inner
//    @SysLog("修改商品分类")
//    @PostMapping("/update")
//    public R updateById(@RequestBody GoodsCategory goodsCategory) {
//        return new R<>(goodsCategoryService.updateById(goodsCategory));
//    }
//
//    /**
//     * 通过id删除商品分类
//     * @param id id
//     * @return R
//     */
//    @Inner
//    @SysLog("删除商品分类")
//    @PostMapping("/delete/{id}")
//    public R removeById(@PathVariable String id) {
//        return new R<>(goodsCategoryService.removeById(id));
//    }
//
//    /**
//      * 删除（根据ID 批量删除)
//      * @param idList 主键ID列表
//      * @return R
//      */
//    @Inner
//    @SysLog("批量删除商品分类")
//    @PostMapping("/batchDelete")
//    public R removeByIds(@RequestBody List<String> idList) {
//        return new R<>(goodsCategoryService.removeByIds(idList));
//    }
//
//
//    /**
//    * 批量查询（根据IDS）
//    * @param idList 主键ID列表
//    * @return R
//    */
//    @Inner
//    @SysLog("根据IDS批量查询商品分类")
//    @PostMapping("/listByIds")
//    public R listByIds(@RequestBody List<String> idList) {
//        return new R<>(goodsCategoryService.listByIds(idList));
//    }
//
//
//    /**
//     * 根据 Wrapper 条件，查询总记录数
//     * @param goodsCategoryReq 实体对象,可以为空
//     * @return R
//     */
//    @Inner
//    @SysLog("查询商品分类总记录}")
//    @PostMapping("/countByGoodsCategoryReq")
//    public R countByGoodsCategoryReq(@RequestBody GoodsCategoryReq goodsCategoryReq) {
//
//        return new R<>(goodsCategoryService.count(Wrappers.query(goodsCategoryReq)));
//    }
//
//
//    /**
//     * 根据对象条件，查询一条记录
//     * @param goodsCategoryReq 实体对象,可以为空
//     * @return R
//     */
//    @Inner
//    @SysLog("根据goodsCategoryReq查询一条货位信息表")
//    @PostMapping("/getOneByGoodsCategoryReq")
//    public R getOneByGoodsCategoryReq(@RequestBody GoodsCategoryReq goodsCategoryReq) {
//
//        return new R<>(goodsCategoryService.getOne(Wrappers.query(goodsCategoryReq), false));
//    }
//
//
//    /**
//     * 批量修改OR插入
//     * @param goodsCategoryList 实体对象集合 大小不超过1000条数据
//     * @return R
//     */
//    @Inner
//    @SysLog("批量修改OR插入货位信息表")
//    @PostMapping("/saveOrUpdateBatch")
//    public R saveOrUpdateBatch(@RequestBody List<GoodsCategory> goodsCategoryList) {
//
//        return new R<>(goodsCategoryService.saveOrUpdateBatch(goodsCategoryList));
//    }

    /**
     * 分类树查询
     *
     * @param req
     * @return
     */
    @Inner
    @PostMapping("/findCategoryTree")
    public R<List<GoodsCategoryTree>> findCategoryTree(@RequestBody GoodsCategoryReq req) {
        return RUtil.success(goodsCategoryService.selectTree(req.getShopId(), req.isAllShow()));

    }

    /**
     * 查询分类及所有父级
     *
     * @param categoryId
     * @param shopId
     * @return
     */
    @Inner
    @GetMapping("/findAllParentWithSelfById")
    public R<List<GoodsCategory>> findAllParentWithSelfById(@RequestParam("categoryId") String categoryId, @RequestParam("shopId") String shopId) {
        return RUtil.success(categoryCoreService.findAllParentWithSelfById(categoryId, shopId));

    }

}
