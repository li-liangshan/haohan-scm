package com.haohan.cloud.scm.api.purchase.resp;


import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author dy
 * @date 2019/5/17
 */
@Data
public class PurchaseQueryMarketPriceResp {
    private String id;
    /**
     * 平台商家id
     */
    private String pmId;
    /**
     * 商品规格ID
     */
    private String goodsModelId;
    /**
     * 商品图片
     */
    private String goodsImg;
    /**
     * 商品名称
     */
    private String goodsName;
    /**
     * 规格名称
     */
    private String modelName;
    /**
     * 单位
     */
    private String unit;
    /**
     * 市场价
     */
    private BigDecimal marketPrice;
    /**
     * 记录时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime recordTime;
    /**
     * 发起人
     */
    private String initiatorId;
    /**
     * 供应商id
     */
    private String supplierId;
    /**
     * 供应商名称
     */
    private String supplierName;
    /**
     * 备注信息
     */
    private String remarks;

}
