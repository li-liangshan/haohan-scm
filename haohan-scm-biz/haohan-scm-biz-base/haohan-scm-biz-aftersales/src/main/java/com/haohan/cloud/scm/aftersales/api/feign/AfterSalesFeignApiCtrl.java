/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.aftersales.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.aftersales.core.ScmAfterSalesService;
import com.haohan.cloud.scm.aftersales.service.AfterSalesService;
import com.haohan.cloud.scm.api.aftersales.entity.AfterSales;
import com.haohan.cloud.scm.api.aftersales.req.AfterSalesReq;
import com.haohan.cloud.scm.api.aftersales.req.QueryAfterSalesDetailReq;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 售后单
 *
 * @author haohan
 * @date 2019-05-30 10:24:23
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/AfterSales")
@Api(value = "aftersales", tags = "aftersales内部接口服务")
public class AfterSalesFeignApiCtrl {

    private final AfterSalesService afterSalesService;
    private final ScmAfterSalesService scmAfterSalesService;

    /**
     * 通过id查询售后单
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(afterSalesService.getById(id));
    }


    /**
     * 分页查询 售后单 列表信息
     * @param afterSalesReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchAfterSalesPage")
    public R getAfterSalesPage(@RequestBody AfterSalesReq afterSalesReq) {
        Page page = new Page(afterSalesReq.getPageNo(), afterSalesReq.getPageSize());
        AfterSales afterSales =new AfterSales();
        BeanUtil.copyProperties(afterSalesReq, afterSales);

        return new R<>(afterSalesService.page(page, Wrappers.query(afterSales)));
    }


    /**
     * 全量查询 售后单 列表信息
     * @param afterSalesReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchAfterSalesList")
    public R getAfterSalesList(@RequestBody AfterSalesReq afterSalesReq) {
        AfterSales afterSales =new AfterSales();
        BeanUtil.copyProperties(afterSalesReq, afterSales);

        return new R<>(afterSalesService.list(Wrappers.query(afterSales)));
    }


    /**
     * 新增售后单
     * @param afterSales 售后单
     * @return R
     */
    @Inner
    @SysLog("新增售后单")
    @PostMapping("/add")
    public R save(@RequestBody AfterSales afterSales) {
        return new R<>(afterSalesService.save(afterSales));
    }

    /**
     * 修改售后单
     * @param afterSales 售后单
     * @return R
     */
    @Inner
    @SysLog("修改售后单")
    @PostMapping("/update")
    public R updateById(@RequestBody AfterSales afterSales) {
        return new R<>(afterSalesService.updateById(afterSales));
    }

    /**
     * 通过id删除售后单
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除售后单")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(afterSalesService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除售后单")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(afterSalesService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询售后单")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(afterSalesService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param afterSalesReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询售后单总记录}")
    @PostMapping("/countByAfterSalesReq")
    public R countByAfterSalesReq(@RequestBody AfterSalesReq afterSalesReq) {

        return new R<>(afterSalesService.count(Wrappers.query(afterSalesReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param afterSalesReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据afterSalesReq查询一条货位信息表")
    @PostMapping("/getOneByAfterSalesReq")
    public R getOneByAfterSalesReq(@RequestBody AfterSalesReq afterSalesReq) {

        return new R<>(afterSalesService.getOne(Wrappers.query(afterSalesReq), false));
    }


    /**
     * 批量修改OR插入
     * @param afterSalesList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<AfterSales> afterSalesList) {

        return new R<>(afterSalesService.saveOrUpdateBatch(afterSalesList));
    }

    /**
     * 查询售后单详情
     * @param req
     * @return
     */
    @Inner
    @SysLog("查询售后单详情")
    @PostMapping("/queryAfterSalesDetail")
    public R queryAfterSalesDetail(@RequestBody QueryAfterSalesDetailReq req) {

        return new R<>(scmAfterSalesService.queryAfterSalesDetail(req));
    }


}
