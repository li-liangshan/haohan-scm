package com.haohan.cloud.scm.api.supply.req;

import com.haohan.cloud.scm.api.constant.enums.supply.GradeTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author xwx
 * @date 2019/7/25
 */
@Data
@ApiModel(description = "查询供应商货源（加筛选条件）")
public class FilterOfferOrderReq {
    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台id",required = true)
    private String pmId;

    @ApiModelProperty(value = "采购员uid",required = true)
    private String uid;

    @ApiModelProperty(value = "供应商评级:1星-5星")
    private GradeTypeEnum supplierLevel;

    @ApiModelProperty(value = "商品分类id")
    private String goodsCategoryId;

    @ApiModelProperty(value = "分页大小")
    private long size = 10;
    @ApiModelProperty(value = "分页当前页码")
    private long current = 1;

    @ApiModelProperty(value = "供应商id")
    private String supplierId;

}
