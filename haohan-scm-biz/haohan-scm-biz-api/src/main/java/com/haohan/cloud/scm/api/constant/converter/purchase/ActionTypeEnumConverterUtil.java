package com.haohan.cloud.scm.api.constant.converter.purchase;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.purchase.ActionTypeEnum;

/**
 * @author xwx
 * @date 2019/6/3
 */
public class ActionTypeEnumConverterUtil implements Converter<ActionTypeEnum> {
    @Override
    public ActionTypeEnum convert(Object o, ActionTypeEnum actionTypeEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return ActionTypeEnum.getByType(o.toString());
    }
}
