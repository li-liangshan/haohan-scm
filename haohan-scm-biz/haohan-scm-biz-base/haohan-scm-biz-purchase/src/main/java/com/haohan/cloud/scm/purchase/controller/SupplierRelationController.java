/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.purchase.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.purchase.entity.SupplierRelation;
import com.haohan.cloud.scm.api.purchase.req.SupplierRelationReq;
import com.haohan.cloud.scm.purchase.service.SupplierRelationService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 采购员工管理供应商
 *
 * @author haohan
 * @date 2019-05-29 13:34:47
 */
@RestController
@AllArgsConstructor
@RequestMapping("/supplierrelation" )
@Api(value = "supplierrelation", tags = "supplierrelation管理")
public class SupplierRelationController {

    private final SupplierRelationService supplierRelationService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param supplierRelation 采购员工管理供应商
     * @return
     */
    @GetMapping("/page" )
    public R getSupplierRelationPage(Page page, SupplierRelation supplierRelation) {
        return new R<>(supplierRelationService.page(page, Wrappers.query(supplierRelation)));
    }


    /**
     * 通过id查询采购员工管理供应商
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(supplierRelationService.getById(id));
    }

    /**
     * 新增采购员工管理供应商
     * @param supplierRelation 采购员工管理供应商
     * @return R
     */
    @SysLog("新增采购员工管理供应商" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('purchase_supplierrelation_add')" )
    public R save(@RequestBody SupplierRelation supplierRelation) {
        return new R<>(supplierRelationService.save(supplierRelation));
    }

    /**
     * 修改采购员工管理供应商
     * @param supplierRelation 采购员工管理供应商
     * @return R
     */
    @SysLog("修改采购员工管理供应商" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('purchase_supplierrelation_edit')" )
    public R updateById(@RequestBody SupplierRelation supplierRelation) {
        return new R<>(supplierRelationService.updateById(supplierRelation));
    }

    /**
     * 通过id删除采购员工管理供应商
     * @param id id
     * @return R
     */
    @SysLog("删除采购员工管理供应商" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('purchase_supplierrelation_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(supplierRelationService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除采购员工管理供应商")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('purchase_supplierrelation_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(supplierRelationService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询采购员工管理供应商")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(supplierRelationService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param supplierRelationReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询采购员工管理供应商总记录}")
    @PostMapping("/countBySupplierRelationReq")
    public R countBySupplierRelationReq(@RequestBody SupplierRelationReq supplierRelationReq) {

        return new R<>(supplierRelationService.count(Wrappers.query(supplierRelationReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param supplierRelationReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据supplierRelationReq查询一条货位信息表")
    @PostMapping("/getOneBySupplierRelationReq")
    public R getOneBySupplierRelationReq(@RequestBody SupplierRelationReq supplierRelationReq) {

        return new R<>(supplierRelationService.getOne(Wrappers.query(supplierRelationReq), false));
    }


    /**
     * 批量修改OR插入
     * @param supplierRelationList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('purchase_supplierrelation_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<SupplierRelation> supplierRelationList) {

        return new R<>(supplierRelationService.saveOrUpdateBatch(supplierRelationList));
    }


}
