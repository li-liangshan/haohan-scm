/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.manage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haohan.cloud.scm.api.common.dto.SelfPageMapper;
import com.haohan.cloud.scm.api.manage.dto.ShopExtDTO;
import com.haohan.cloud.scm.api.manage.dto.ShopSqlDTO;
import com.haohan.cloud.scm.api.manage.entity.Shop;

import java.util.List;

/**
 * 店铺
 *
 * @author haohan
 * @date 2019-05-13 17:39:04
 */
public interface ShopMapper extends BaseMapper<Shop>, SelfPageMapper {

    /**
     * 自定义分页使用
     *
     * @param params
     * @return
     */
    Integer queryCount(ShopSqlDTO params);

    /**
     * 自定义分页使用
     *
     * @param params
     * @return
     */
    List<ShopExtDTO> queryPage(ShopSqlDTO params);

}
