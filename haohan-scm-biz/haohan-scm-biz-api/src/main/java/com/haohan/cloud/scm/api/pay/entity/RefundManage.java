/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.pay.entity;


import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Digits;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 退款管理
 *
 * @author haohan
 * @date 2020-06-10 19:17:43
 */
@Data
@TableName("scm_refund_manage")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "退款管理")
public class RefundManage extends Model<RefundManage> {

  private static final long serialVersionUID = 1L;


  @Length(max = 64, message = "主键长度最大64字符")
  @ApiModelProperty(value = "主键")
  @TableId(type = IdType.INPUT)
  private String id;

  @Length(max = 32, message = "商家ID长度最大32字符")
  @ApiModelProperty(value = "商家ID")
  private String merchantId;

  @Length(max = 64, message = "商家名称长度最大64字符")
  @ApiModelProperty(value = "商家名称")
  private String merchantName;

  @Length(max = 64, message = "流水号长度最大64字符")
  @ApiModelProperty(value = "流水号")
  private String requestId;

  @Length(max = 64, message = "主商户编号长度最大64字符")
  @ApiModelProperty(value = "主商户编号")
  private String partnerId;

  @Length(max = 64, message = "商户原订单号长度最大64字符")
  @ApiModelProperty(value = "商户原订单号")
  private String orderId;

  @Length(max = 64, message = "原平台交易订单号长度最大64字符")
  @ApiModelProperty(value = "原平台交易订单号")
  private String orgTransId;

  @Length(max = 64, message = "原商户交易流水号长度最大64字符")
  @ApiModelProperty(value = "原商户交易流水号")
  private String orgReqId;

  @Digits(integer = 10, fraction = 2, message = "订单金额的整数位最大10位, 小数位最大2位")
  @ApiModelProperty(value = "订单金额")
  private BigDecimal orderAmount;

  @Digits(integer = 10, fraction = 2, message = "支付金额的整数位最大10位, 小数位最大2位")
  @ApiModelProperty(value = "支付金额")
  private BigDecimal payAmount;

  @Digits(integer = 10, fraction = 2, message = "退款金额的整数位最大10位, 小数位最大2位")
  @ApiModelProperty(value = "退款金额")
  private BigDecimal refundAmount;

  @Length(max = 32, message = "业务类型长度最大32字符")
  @ApiModelProperty(value = "业务类型")
  private String busType;

  @ApiModelProperty(value = "申请退款时间")
  private LocalDateTime refundApplyTime;

  @Length(max = 32, message = "退款结果SUCCESS成功/FAIL失败长度最大32字符")
  @ApiModelProperty(value = "退款结果SUCCESS成功/FAIL失败")
  private String status;

  @Length(max = 32, message = "返回码长度最大32字符")
  @ApiModelProperty(value = "返回码")
  private String respCode;

  @Length(max = 128, message = "返回信息描述长度最大128字符")
  @ApiModelProperty(value = "返回信息描述")
  private String respDesc;

  @Length(max = 32, message = "退款平台流水号长度最大32字符")
  @ApiModelProperty(value = "退款平台流水号")
  private String tradeNo;

  @ApiModelProperty(value = "退款返回时间")
  private LocalDateTime respTime;

  @Length(max = 255, message = "申请退款原因长度最大255字符")
  @ApiModelProperty(value = "申请退款原因")
  private String refundCause;

  @Length(max = 64, message = "创建者长度最大64字符")
  @ApiModelProperty(value = "创建者")
  @TableField(fill = FieldFill.INSERT)
  private String createBy;

  @ApiModelProperty(value = "创建时间")
  @TableField(fill = FieldFill.INSERT)
  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private LocalDateTime createDate;

  @Length(max = 64, message = "更新者长度最大64字符")
  @ApiModelProperty(value = "更新者")
  @TableField(fill = FieldFill.UPDATE)
  private String updateBy;

  @ApiModelProperty(value = "更新时间")
  @TableField(fill = FieldFill.UPDATE)
  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private LocalDateTime updateDate;

  @Length(max = 500, message = "备注信息长度最大500字符")
  @ApiModelProperty(value = "备注信息")
  private String remarks;

  @Length(max = 1, message = "删除标记长度最大1字符")
  @ApiModelProperty(value = "删除标记")
  @TableLogic
  @TableField(fill = FieldFill.INSERT)
  private String delFlag;

  @ApiModelProperty(value = "租户id")
  private Integer tenantId;

}
