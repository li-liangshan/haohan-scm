package com.haohan.cloud.scm.api.bill.feign;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.bill.dto.BillInfoDTO;
import com.haohan.cloud.scm.api.bill.entity.ReceivableBill;
import com.haohan.cloud.scm.api.bill.req.BillInfoFeignReq;
import com.haohan.cloud.scm.api.bill.vo.BillInfoVO;
import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.crm.vo.app.BillPageVO;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author dy
 * @date 2019/12/4
 * 账单 内部接口服务
 */
@FeignClient(contextId = "ReceivableBillFeignService", value = ScmServiceName.SCM_BILL)
public interface ReceivableBillFeignService {


    /**
     * 分页查询 账单 列表信息
     *
     * @param req  请求对象
     * @param from
     * @return
     */
    @PostMapping("/api/feign/bill/receivable/findPage")
    R<Page<BillInfoDTO>> findPage(@RequestBody BillInfoFeignReq req, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 账单 列表信息  暂不使用
     *
     * @param billInfo 请求对象
     * @param from
     * @return
     */
    @PostMapping("/api/feign/bill/receivable/findList")
    R<List<BillInfoDTO>> findList(@RequestBody BillInfoDTO billInfo, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 根据编号查询账单
     *
     * @param billSn
     * @param from
     * @return
     */
    @GetMapping("/api/feign/bill/receivable/fetchOneBySn/{billSn}")
    R<ReceivableBill> fetchOneBySn(@PathVariable("billSn") String billSn, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据订单编号查询 普通账单
     *
     * @param orderSn
     * @param from
     * @return 可为null
     */
    @GetMapping("/api/feign/bill/receivable/fetchNormalByOrder/{orderSn}")
    R<ReceivableBill> fetchNormalByOrder(@PathVariable("orderSn") String orderSn, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 查询账单详情
     *
     * @param req  billSn 或 orderSn
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/bill/receivable/fetchOne")
    R<BillInfoVO> fetchOneReceivable(@RequestBody BillInfoFeignReq req, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 创建账单
     *
     * @param billInfo 必需: orderSn/billType/advanceAmount
     *                 advanceAmount 预付金额，设置不同账单：
     *                 金额0 普通账单，
     *                 金额等于订单金额， 账单不需审核
     *                 金额在0至订单金额间，多创建个预付账单（预付账单不需审核）
     * @param from
     * @return
     */
    @PostMapping("/api/feign/bill/receivable/createBill")
    R<BillInfoDTO> createBill(@RequestBody BillInfoDTO billInfo, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 根据订单信息更新账单(普通账单)
     * 无账单时无操作
     * 账单不能已结算
     * @param req  orderSn、billType
     * @param from
     * @return
     */
    @PostMapping("/api/feign/bill/receivable/updateBillByOrder")
    R<Boolean> updateBillByOrder(@RequestBody BillInfoFeignReq req, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 账单审核通过（创建结算单）
     * 审核不通过 改变状态
     *
     * @param req  必须 billSn、successFlag 可选 remarks
     * @param from
     * @return
     */
    @PostMapping("/api/feign/bill/receivable/review")
    R<Boolean> receivableReview(@RequestBody BillInfoFeignReq req, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 按下单客户统计 账单金额
     *
     * @param req  customerId、reviewStatus、billType、settlementStatus、customerIdSet
     *             excludeStatus、advanceFlag 、startDate、endDate(可单独使用)
     *             current、size
     * @param from
     * @return customerId、customerName、billAmount
     */
    @PostMapping("/api/feign/bill/receivable/countCustomer")
    R<BillPageVO<BillInfoDTO>> countBillByCustomer(@RequestBody BillInfoFeignReq req, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 账单统计 总数量、总金额
     *
     * @param req  customerId、reviewStatus、billType、settlementStatus、customerIdSet
     *             excludeStatus、advanceFlag 、startDate、endDate(可单独使用)
     * @param from
     * @return total、billAmount
     */
    @PostMapping("/api/feign/bill/receivable/countBill")
    R<BillInfoDTO> countBill(@RequestBody BillInfoFeignReq req, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 批量创建普通账单
     *
     * @param req  orderSnSet、billType
     * @param from
     * @return
     */
    @PostMapping("/api/feign/bill/receivable/createNormalBillBatch")
    R<Boolean> createNormalBillBatch(@RequestBody BillInfoFeignReq req, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 修改账单中客户名称
     *
     * @param req
     * @param from
     * @return
     */
    @PostMapping("/api/feign/bill/receivable/updateCustomerName")
    R<Boolean> updateCustomerName(@RequestBody BillInfoFeignReq req, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 删除账单 不为已审核通过
     * @param req orderSn、billType
     * @param from
     * @return
     */
    @PostMapping("/api/feign/bill/receivable/deleteBillByOrder")
    R<Boolean> deleteBillByOrder(@RequestBody BillInfoFeignReq req, @RequestHeader(SecurityConstants.FROM) String from);
}
