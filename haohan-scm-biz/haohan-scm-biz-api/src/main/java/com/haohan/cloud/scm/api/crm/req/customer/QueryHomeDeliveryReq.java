package com.haohan.cloud.scm.api.crm.req.customer;

import com.haohan.cloud.scm.api.constant.enums.crm.DeliveryGoodsStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.crm.DeliveryGoodsTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.crm.MemberTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.crm.PaymentTypeEnum;
import com.haohan.cloud.scm.api.crm.entity.HomeDeliveryRecord;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

/**
 * @author dy
 * @date 2019/12/10
 */
@Data
public class QueryHomeDeliveryReq {

    /**
     * 客户编码
     */
    @Length(max = 64, message = "客户编码的最大长度为64字符")
    @ApiModelProperty(value = "客户编码")
    private String customerSn;

    /**
     * 联系人id
     */
    @Length(max = 64, message = "联系人id的最大长度为64字符")
    @ApiModelProperty(value = "联系人id")
    private String linkmanId;

    /**
     * 配件类型 1普通件 2急件
     */
    @ApiModelProperty(value = "配件类型 1普通件 2急件")
    private DeliveryGoodsTypeEnum goodsType;


    /**
     * 配送状态 1未送，2已送，3异常
     */
    @ApiModelProperty(value = "配送状态 1未送，2已送，3异常")
    private DeliveryGoodsStatusEnum deliveryStatus;
    /**
     * 会员类型 1普通，2年卡
     */
    @ApiModelProperty(value = "会员类型（1普通，2年卡）")
    private MemberTypeEnum memberType;
    /**
     * 付款方式 1.现金，2.微信转账，3.线上支付
     */
    @ApiModelProperty(value = "付款方式 1.现金，2.微信转账，3.线上支付")
    private PaymentTypeEnum paymentType;

    /**
     * 订单号
     */
    @Length(max = 64, message = "订单号的最大长度为64字符")
    @ApiModelProperty(value = "订单号")
    private String orderSn;
    /**
     * 订单类型
     */
    @Length(max = 5, message = "订单类型的最大长度为5字符")
    @ApiModelProperty(value = "订单类型")
    private String orderTye;
    /**
     * 第三方订单号
     */
    @Length(max = 64, message = "第三方订单号的最大长度为64字符")
    @ApiModelProperty(value = "第三方订单号")
    private String thirdOrderSn;

    // 非eq参数
    /**
     * 客户名称
     */
    @Length(max = 64, message = "客户名称的最大长度为64字符")
    @ApiModelProperty(value = "客户名称")
    private String customerName;
    /**
     * 联系人名称
     */
    @Length(max = 64, message = "联系人名称的最大长度为64字符")
    @ApiModelProperty(value = "联系人名称")
    private String linkmanName;

    /**
     * 配送物品 （自定义标签）
     */
    @Length(max = 64, message = "配送物品的最大长度为64字符")
    @ApiModelProperty(value = "配送物品 （自定义标签）")
    private String goodsTags;

    /**
     * 其他服务 （自定义标签）
     */
    @Length(max = 64, message = "其他服务的最大长度为64字符")
    @ApiModelProperty(value = "其他服务 （自定义标签）")
    private String otherServices;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "配送日期-开始日期")
    private LocalDate startDate;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "配送日期-结束日期")
    private LocalDate endDate;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "预约配送日期-开始日期")
    private LocalDate appointmentStartDate;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "预约配送日期-结束日期")
    private LocalDate appointmentEndDate;

    public HomeDeliveryRecord transTo() {
        HomeDeliveryRecord record = new HomeDeliveryRecord();
        // eq参数
        record.setCustomerSn(this.customerSn);
        record.setLinkmanId(this.linkmanId);
        record.setGoodsType(this.goodsType);
        record.setDeliveryStatus(this.deliveryStatus);
        record.setMemberType(this.memberType);
        record.setPaymentType(this.paymentType);
        record.setOrderSn(this.orderSn);
        record.setOrderTye(this.orderTye);
        record.setThirdOrderSn(this.thirdOrderSn);
        return record;
    }
}
