package com.haohan.cloud.scm.api.purchase.req;

import com.haohan.cloud.scm.api.opc.req.FormulaReq;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author cx
 * @date 2019/6/3
 */
@Data
public class PurchaseNeedReq {
  /**
   * 平台商家id
   */
  @NotBlank(message = "pmId不能为空")
  private String pmId;

  /**
   * 采购明细编号
   */
  @NotBlank(message = "originalPurchaseDetailSn不能为空")
  private String originalPurchaseDetailSn;

  /**
   * 需求采购商品信息
   */
  private List<FormulaReq> purchaseInfo;

  /**
   * 采购方式类型
   */
  private String methodType;

  /**
   * 供应商
   */
  private String supplierId;
  /**
   * 采购价
   */
  private BigDecimal buyPrice;
  /**
   * 竞价截止时间
   */
  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private LocalDateTime biddingEndTime;

}
