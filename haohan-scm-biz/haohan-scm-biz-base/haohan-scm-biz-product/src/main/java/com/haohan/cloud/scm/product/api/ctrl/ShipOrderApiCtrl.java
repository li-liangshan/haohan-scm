package com.haohan.cloud.scm.product.api.ctrl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.haohan.cloud.scm.api.product.entity.ShipOrder;
import com.haohan.cloud.scm.api.product.req.AddShipReq;
import com.haohan.cloud.scm.api.product.req.ArriveBatchReq;
import com.haohan.cloud.scm.api.product.req.DeliveryBeginReq;
import com.haohan.cloud.scm.api.product.req.ProductDeliveryReq;
import com.haohan.cloud.scm.common.tools.exception.ErrorDataException;
import com.haohan.cloud.scm.product.core.IShipOrderService;
import com.haohan.cloud.scm.product.service.ShipOrderService;
import com.pig4cloud.pigx.common.core.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author xwx
 * @date 2019/5/27
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/product/shipOrder")
@Api(value = "ApiShipOrder", tags = "shipOrder生成送货单")
public class ShipOrderApiCtrl {

    private final IShipOrderService shipOrderCoreService;
    private final ShipOrderService shipOrderService;


    /**
     * 生成送货单
     * @param req
     * @return
     */
    @PostMapping("/addShipOrder")
    public R<ShipOrder> addShipOrder(@Validated AddShipReq req){
        if (StrUtil.isEmpty(req.getBuyId())){
            throw new ErrorDataException("buyId有错");
        }
        ShipOrder shipOrder = shipOrderCoreService.addShipOrder(req);
        return new R<>(shipOrder);
    }

  /**
   * 库存满足时  运营 发起送货
   * @param req
   * @return
   */
    @PostMapping("/initiateDelivery")
    public R<Boolean> initiateDelivery(@Validated ProductDeliveryReq req) {
      return new R(shipOrderCoreService.initiateDelivery(req));
    }

    /**
     * 开始配送 改变配送单状态 修改B订单状态
     *
     * @param req
     * @return
     */
    @PostMapping("/deliveryBegin")
    public R deliveryBegin(@Validated DeliveryBeginReq req) {
        QueryWrapper<ShipOrder> query = new QueryWrapper<>();
        query.lambda()
                .eq(ShipOrder::getPmId, req.getPmId())
                .eq(ShipOrder::getShipId, req.getShipId());
        try {
            ShipOrder shipOrder = shipOrderService.getOne(query);
            if (null == shipOrder) {
                throw new ErrorDataException("配送单有误");
            }
            shipOrderCoreService.deliveryBegin(shipOrder);
            return R.ok(true);
        } catch (Exception e) {
            return R.failed(e.getMessage());
        }
    }

    /**
     * 单个配送单 送达  修改配送单状态
     * @param req 必须 pmId/shipId
     */
    @PostMapping("/arrive")
    public R arrive(@Validated DeliveryBeginReq req) {
        try {
            ShipOrder shipOrder = new ShipOrder();
            shipOrder.setPmId(req.getPmId());
            shipOrder.setShipId(req.getShipId());
            shipOrderCoreService.arrive(shipOrder);
            return R.ok(true);
        } catch (Exception e) {
            return R.failed(e.getMessage());
        }
    }

    /**
     * 配送单批量送达
     *
     * @param req
     * @return
     */
    @PostMapping("/arriveBatch")
    @ApiOperation(value = "配送单批量送达")
    public R arriveBatch(@Validated ArriveBatchReq req) {
        String msg = shipOrderCoreService.arriveBatch(req);
        if(null == msg){
            return R.failed("当前时间批次无可送达订单");
        }
        return R.ok(true, msg);
    }

}
