package com.haohan.cloud.scm.aftersales.api.ctrl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.aftersales.core.ScmReturnOrderService;
import com.haohan.cloud.scm.aftersales.service.ReturnOrderService;
import com.haohan.cloud.scm.api.aftersales.entity.ReturnOrder;
import com.haohan.cloud.scm.api.aftersales.req.AddBuyReturnOrderReq;
import com.haohan.cloud.scm.api.aftersales.req.AddReturnOrderByOfferReq;
import com.haohan.cloud.scm.api.aftersales.req.AddReturnOrderReq;
import com.haohan.cloud.scm.api.aftersales.req.EditReturnOrderReq;
import com.haohan.cloud.scm.api.wms.feign.EnterWarehouseFeignService;
import com.haohan.cloud.scm.api.wms.req.EnterWarehouseReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

/**
 * @author xwx
 * @date 2019/7/22
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/returnOrder")
@Api(value = "ApiReturnOrder", tags = "returnOrder退货单管理")
public class ReturnOrderApiCtrl {

    private final ReturnOrderService returnOrderService;

    private final ScmReturnOrderService scmReturnOrderService;

    private final EnterWarehouseFeignService enterWarehouseFeignService;

    @GetMapping("/page" )
    @ApiOperation(value = "分页查询退货单带应退金额")
    public R getReturnOrderPage(Page page, ReturnOrder returnOrder) {
        IPage ipage = returnOrderService.page(page, Wrappers.query(returnOrder));
        ipage.setRecords(scmReturnOrderService.queryReturnOrderAmount(ipage.getRecords()));
        return new R<>(ipage);
    }

    @PostMapping("/addReturnOrder")
    @ApiOperation(value = "添加退货单")
    public R addReturnOrder(@RequestBody AddReturnOrderReq req){
        return new R(scmReturnOrderService.addReturnOrder(req));
    }

    @PostMapping("/addBuyReturnOrder")
    @ApiOperation(value = "添加b客户退货单")
    public R addBuyReturnOrder(@RequestBody AddBuyReturnOrderReq req){
        return new R(scmReturnOrderService.buyOrderReturn(req));
    }

    @GetMapping("/queryReturnDetail")
    @ApiOperation(value = "查看退货详情")
    public R queryReturnDetail(ReturnOrder order){
        return new R(scmReturnOrderService.queryReturnOrderDetail(order));
    }

    @PostMapping("/editReturnOrder")
    @ApiOperation(value = "编辑退货订单详情")
    public R editReturnOrder(@RequestBody EditReturnOrderReq req){ return new R(scmReturnOrderService.editReturnOrder(req));}

    @GetMapping("/queryEnterDetail")
    @ApiOperation(value = "查询订单退货入库单详情")
    public R queryEnterDetail(EnterWarehouseReq req){
        return enterWarehouseFeignService.queryEnterOrderDetail(req, SecurityConstants.FROM_IN);
    }

    @PostMapping("/auditPassOrder")
    @ApiOperation(value = "审核退货单")
    public R auditPassOrder(@RequestBody EditReturnOrderReq req){
        return R.ok(scmReturnOrderService.auditPassOrder(req));
    }

    @PostMapping("/auditPassReturnOrder")
    @ApiOperation(value = "退货入库")
    public R auditPassReturnOrder(@RequestBody EditReturnOrderReq order){
        return new R<>(scmReturnOrderService.auditPassReturnOrder(order));
    }

    @PostMapping("/addReturnOrderByOfferOrder")
    @ApiOperation(value = "根据报价单创建退货单")
    public R addReturnOrderByOfferOrder(@RequestBody AddReturnOrderByOfferReq req){
        return new R<>(scmReturnOrderService.addReturnOrderByOfferOrder(req));
    }

    @PostMapping("/passReturnOrderAddStock")
    @ApiOperation(value = "退货单生成库存")
    public R passReturnOrderAddStock(@RequestBody EditReturnOrderReq order){
        return new R<>(scmReturnOrderService.passReturnOrderAddStock(order));
    }

    @PostMapping("/receiptGoods")
    @ApiOperation(value = "确认收货")
    public R receiptGoods(@RequestBody EditReturnOrderReq order){
        return new R<>(scmReturnOrderService.receiptGoods(order));
    }
}
