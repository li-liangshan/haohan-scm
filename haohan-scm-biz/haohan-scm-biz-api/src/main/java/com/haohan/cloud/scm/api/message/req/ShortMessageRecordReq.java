/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.message.req;

import com.haohan.cloud.scm.api.message.entity.ShortMessageRecord;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 短信消息记录表
 *
 * @author haohan
 * @date 2019-05-28 20:03:53
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "短信消息记录表")
public class ShortMessageRecordReq extends ShortMessageRecord {

    private long pageSize;
    private long pageNo;




}
