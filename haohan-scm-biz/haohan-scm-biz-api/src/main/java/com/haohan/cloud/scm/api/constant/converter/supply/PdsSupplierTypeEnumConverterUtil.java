package com.haohan.cloud.scm.api.constant.converter.supply;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.supply.PdsSupplierTypeEnum;

/**
 * @author xwx
 * @date 2019/6/4
 */
public class PdsSupplierTypeEnumConverterUtil implements Converter<PdsSupplierTypeEnum> {
    @Override
    public PdsSupplierTypeEnum convert(Object o, PdsSupplierTypeEnum pdsSupplierTypeEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return PdsSupplierTypeEnum.getByType(o.toString());
    }

}
