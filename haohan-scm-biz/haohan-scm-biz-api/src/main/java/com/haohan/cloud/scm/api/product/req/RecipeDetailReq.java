/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.product.req;

import com.haohan.cloud.scm.api.product.entity.RecipeDetail;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 商品加工配方明细表
 *
 * @author haohan
 * @date 2019-05-28 20:53:19
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "商品加工配方明细表")
public class RecipeDetailReq extends RecipeDetail {

    private long pageSize;
    private long pageNo;




}
