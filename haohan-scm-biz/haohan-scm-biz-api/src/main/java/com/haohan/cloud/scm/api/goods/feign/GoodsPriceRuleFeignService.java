/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.goods.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.goods.entity.GoodsPriceRule;
import com.haohan.cloud.scm.api.goods.req.GoodsPriceRuleReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 定价规则/市场价/销售价/VIP价格内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "GoodsPriceRuleFeignService", value = ScmServiceName.SCM_GOODS)
public interface GoodsPriceRuleFeignService {


    /**
     * 通过id查询定价规则/市场价/销售价/VIP价格
     *
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/GoodsPriceRule/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 定价规则/市场价/销售价/VIP价格 列表信息
     *
     * @param goodsPriceRuleReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/GoodsPriceRule/fetchGoodsPriceRulePage")
    R getGoodsPriceRulePage(@RequestBody GoodsPriceRuleReq goodsPriceRuleReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 定价规则/市场价/销售价/VIP价格 列表信息
     *
     * @param goodsPriceRuleReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/GoodsPriceRule/fetchGoodsPriceRuleList")
    R getGoodsPriceRuleList(@RequestBody GoodsPriceRuleReq goodsPriceRuleReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增定价规则/市场价/销售价/VIP价格
     *
     * @param goodsPriceRule 定价规则/市场价/销售价/VIP价格
     * @return R
     */
    @PostMapping("/api/feign/GoodsPriceRule/add")
    R save(@RequestBody GoodsPriceRule goodsPriceRule, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改定价规则/市场价/销售价/VIP价格
     *
     * @param goodsPriceRule 定价规则/市场价/销售价/VIP价格
     * @return R
     */
    @PostMapping("/api/feign/GoodsPriceRule/update")
    R updateById(@RequestBody GoodsPriceRule goodsPriceRule, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除定价规则/市场价/销售价/VIP价格
     *
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/GoodsPriceRule/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/GoodsPriceRule/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/GoodsPriceRule/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param goodsPriceRuleReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/GoodsPriceRule/countByGoodsPriceRuleReq")
    R countByGoodsPriceRuleReq(@RequestBody GoodsPriceRuleReq goodsPriceRuleReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param goodsPriceRuleReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/GoodsPriceRule/getOneByGoodsPriceRuleReq")
    R getOneByGoodsPriceRuleReq(@RequestBody GoodsPriceRuleReq goodsPriceRuleReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param goodsPriceRuleList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/GoodsPriceRule/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<GoodsPriceRule> goodsPriceRuleList, @RequestHeader(SecurityConstants.FROM) String from);


}
