package com.haohan.cloud.scm.api.constant.converter.opc;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.opc.SummaryStatusEnum;

/**
 * @author dy
 * @date 2019/6/1
 */
public class SummaryStatusEnumConverterUtil implements Converter<SummaryStatusEnum> {

    @Override
    public SummaryStatusEnum convert(Object o, SummaryStatusEnum e) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return SummaryStatusEnum.getByType(o.toString());
    }

}
