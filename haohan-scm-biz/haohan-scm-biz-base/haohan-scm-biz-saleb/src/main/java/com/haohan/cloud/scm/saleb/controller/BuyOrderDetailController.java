/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.saleb.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.saleb.entity.BuyOrderDetail;
import com.haohan.cloud.scm.api.saleb.req.BuyOrderDetailReq;
import com.haohan.cloud.scm.saleb.service.BuyOrderDetailService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 采购单明细
 *
 * @author haohan
 * @date 2019-05-29 13:29:47
 */
@RestController
@AllArgsConstructor
@RequestMapping("/buyorderdetail" )
@Api(value = "buyorderdetail", tags = "buyorderdetail管理")
public class BuyOrderDetailController {

    private final BuyOrderDetailService buyOrderDetailService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param buyOrderDetail 采购单明细
     * @return
     */
    @GetMapping("/page" )
    public R getBuyOrderDetailPage(Page page, BuyOrderDetail buyOrderDetail) {
        return new R<>(buyOrderDetailService.page(page, Wrappers.query(buyOrderDetail)));
    }


    /**
     * 通过id查询采购单明细
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(buyOrderDetailService.getById(id));
    }

    /**
     * 新增采购单明细
     * @param buyOrderDetail 采购单明细
     * @return R
     */
    @SysLog("新增采购单明细" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_buyorderdetail_add')" )
    public R save(@RequestBody BuyOrderDetail buyOrderDetail) {
        return new R<>(buyOrderDetailService.save(buyOrderDetail));
    }

    /**
     * 修改采购单明细
     * @param buyOrderDetail 采购单明细
     * @return R
     */
    @SysLog("修改采购单明细" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_buyorderdetail_edit')" )
    public R updateById(@RequestBody BuyOrderDetail buyOrderDetail) {
        return new R<>(buyOrderDetailService.updateById(buyOrderDetail));
    }

    /**
     * 通过id删除采购单明细
     * @param id id
     * @return R
     */
    @SysLog("删除采购单明细" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_buyorderdetail_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(buyOrderDetailService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除采购单明细")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_buyorderdetail_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(buyOrderDetailService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(buyOrderDetailService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param buyOrderDetailReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/countByBuyOrderDetailReq")
    public R countByBuyOrderDetailReq(@RequestBody BuyOrderDetailReq buyOrderDetailReq) {

        return new R<>(buyOrderDetailService.count(Wrappers.query(buyOrderDetailReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param buyOrderDetailReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据buyOrderDetailReq查询一条货位信息表")
    @PostMapping("/getOneByBuyOrderDetailReq")
    public R getOneByBuyOrderDetailReq(@RequestBody BuyOrderDetailReq buyOrderDetailReq) {

        return new R<>(buyOrderDetailService.getOne(Wrappers.query(buyOrderDetailReq), false));
    }


    /**
     * 批量修改OR插入
     * @param buyOrderDetailList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_buyorderdetail_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<BuyOrderDetail> buyOrderDetailList) {

        return new R<>(buyOrderDetailService.saveOrUpdateBatch(buyOrderDetailList));
    }
}
