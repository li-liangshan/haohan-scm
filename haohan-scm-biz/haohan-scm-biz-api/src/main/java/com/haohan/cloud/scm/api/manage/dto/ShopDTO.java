package com.haohan.cloud.scm.api.manage.dto;

import com.haohan.cloud.scm.api.constant.enums.manage.ShopLevelEnum;
import com.haohan.cloud.scm.api.manage.entity.PhotoGallery;
import lombok.Data;

import java.util.List;

/**
 * @author dy
 * @date 2019/9/12
 */
@Data
public class ShopDTO {

    /**
     * 主键
     */
    private String id;
    /**
     * 店铺编号
     */
    private String code;
    /**
     * 店铺名称
     */
    private String name;
    /**
     * 店铺区域
     */
    private String areaId;
    /**
     * 店铺地址
     */
    private String address;
    /**
     * 店铺负责人
     */
    private String manager;
    /**
     * 店铺电话
     */
    private String telephone;
    /**
     * 经度
     */
    private String mapLongitude;
    /**
     * 纬度
     */
    private String mapLatitude;
    /**
     * 商家ID
     */
    private String merchantId;
    /**
     * 营业时间 描述
     */
    private String onlineTime;
    /**
     * 店铺服务
     */
    private String shopService;
    /**
     * 店铺模板ID
     */
    private String templateId;
    /**
     * 店铺介绍
     */
    private String shopDesc;
    /**
     * 启用状态  原系统   0待审核2启用-1停用
     */
    private String status;
    /**
     * 店铺位置
     */
    private String shopLocation;
    /**
     * 店铺类型
     */
    private String shopType;
    /**
     * 配送距离
     */
    private String deliverDistence;
    /**
     * 店铺等级
     */
    private ShopLevelEnum shopLevel;
    /**
     * 是否更新即速商品
     */
    private String isUpdateJisu;
    /**
     * 店铺服务模式
     */
    private String serviceType;
    /**
     * 行业名称
     */
    private String industry;
    /**
     * 店铺分类id
     */
    private String shopCategory;
    /**
     * 认证类型
     */
    private String authType;
    /**
     * 聚合平台类型
     */
    private String aggregationType;
    /**
     * 交易类型
     */
    private String tradeType;


    /**
     * 图片组编号  轮播图
     */
    private String photoGroupNum;
    /**
     *  轮播图 列表
     */
    private List<PhotoGallery> photoList;
    /**
     * 店铺收款码  保存图片组编号
     */
    private String payCode;
    /**
     * 店铺收款码 列表
     */
    private List<PhotoGallery> payCodeList;
    /**
     * 店铺二维码  保存图片组编号
     */
    private String qrcode;
    /**
     * 店铺二维码 列表
     */
    private List<PhotoGallery> qrcodeList;
    /**
     * 店铺Logo  保存图片组编号
     */
    private String shopLogo;
    /**
     * 店铺Logo 列表
     */
    private List<PhotoGallery> shopLogoList;

}
