package com.haohan.cloud.scm.api.salec.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author dy
 * @date 2020/6/9
 */
@Data
public class ResultVO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "商品类型属性信息")
    private List<ResultAttrVO> attr;

    private List<ResultAttrValueVO> value;


}
