/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.manage.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.manage.entity.AuthApp;
import com.haohan.cloud.scm.manage.mapper.AuthAppMapper;
import com.haohan.cloud.scm.manage.service.AuthAppService;
import org.springframework.stereotype.Service;

/**
 * 授权应用管理
 *
 * @author haohan
 * @date 2019-05-13 17:43:32
 */
@Service
public class AuthAppServiceImpl extends ServiceImpl<AuthAppMapper, AuthApp> implements AuthAppService {

}
