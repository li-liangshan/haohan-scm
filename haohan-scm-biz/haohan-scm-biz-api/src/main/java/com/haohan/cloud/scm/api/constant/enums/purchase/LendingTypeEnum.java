package com.haohan.cloud.scm.api.constant.enums.purchase;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xwx
 * @date 2019/5/30
 */
@Getter
@AllArgsConstructor
public enum LendingTypeEnum implements IBaseEnum {

    /**
     * 请款类型1.不请款2.需请款
     */
    noLending("1","不请款"),
    needLending("2","需请款");

    private static final Map<String, LendingTypeEnum> MAP = new HashMap<>(8);

    static {
        for (LendingTypeEnum e : LendingTypeEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static LendingTypeEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

    /**
     * 类型
     */
    @EnumValue
    @JsonValue
    private String type;
    /**
     * 描述
     */
    private String desc;
}
