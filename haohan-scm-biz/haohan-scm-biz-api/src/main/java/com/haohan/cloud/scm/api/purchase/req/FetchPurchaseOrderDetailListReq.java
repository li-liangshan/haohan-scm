package com.haohan.cloud.scm.api.purchase.req;

import com.haohan.cloud.scm.api.constant.enums.purchase.MethodTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.purchase.PurchaseOrderTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.purchase.PurchaseStatusEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

/**
 * @author xwx
 * @date 2019/7/24
 */
@Data
@ApiModel(description = "查询所有采购单明细列表（可根据条件筛选）")
public class FetchPurchaseOrderDetailListReq {
    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台商家id", required = true)
    private String pmId;

    @ApiModelProperty(value = "采购员工通行证id")
    private String uid;

    @ApiModelProperty(value = "采购状态:1.待处理2.待审核3.采购中4.备货中5.已揽货6.采购完成7.部分完成8.已关闭")
    private PurchaseStatusEnum purchaseStatus;

    @ApiModelProperty(value = "采购订单分类:1.采购计划2.按需采购")
    private PurchaseOrderTypeEnum purchaseOrderType;

    @ApiModelProperty(value = "采购截止时间")
    private LocalDateTime buyFinalTime;

    @ApiModelProperty(value = "采购方式类型:1.竞价采购2.单品采购3.协议供应")
    private MethodTypeEnum methodType;

    @ApiModelProperty(value = "分页大小")
    private long size = 10;
    @ApiModelProperty(value = "分页当前页码")
    private long current = 1;

}
