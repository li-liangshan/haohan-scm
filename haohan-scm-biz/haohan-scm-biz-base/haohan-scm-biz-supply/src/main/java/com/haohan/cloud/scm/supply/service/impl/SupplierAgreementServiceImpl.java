/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.supply.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.supply.entity.SupplierAgreement;
import com.haohan.cloud.scm.supply.mapper.SupplierAgreementMapper;
import com.haohan.cloud.scm.supply.service.SupplierAgreementService;
import org.springframework.stereotype.Service;

/**
 * 协议供应商品记录
 *
 * @author haohan
 * @date 2019-05-13 17:44:58
 */
@Service
public class SupplierAgreementServiceImpl extends ServiceImpl<SupplierAgreementMapper, SupplierAgreement> implements SupplierAgreementService {

}
