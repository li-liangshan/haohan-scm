package com.haohan.cloud.scm.api.purchase.req;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author cx
 * @date 2019/6/6
 */
@Data
public class QueryPurchaseTaskReq {

    @NotBlank(message = "pmId不能为空")
    private String pmId;

    @NotBlank(message = "id不能为空")
    private String id;
    /**
     * 通行证id
     */
    private String uid;
}
