/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.haohan.cloud.scm.api.product.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.haohan.cloud.scm.api.constant.enums.common.TaskStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.product.DeliverySeqEnum;
import com.haohan.cloud.scm.api.constant.enums.product.ProductionTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * 生产任务表
 *
 * @author haohan
 * @date 2019-05-13 18:16:02
 */
@Data
@TableName("scm_pws_production_task")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "生产任务表")
public class ProductionTask extends Model<ProductionTask> {
  private static final long serialVersionUID = 1L;

  /**
   * 主键
   */
  @TableId(type = IdType.INPUT)
  private String id;
  /**
   * 平台商家id
   */
  private String pmId;
  /**
   * 生产任务编号
   */
  private String productionTaskSn;
  /**
   * 发起人
   */
  private String initiatorId;
  /**
   * 发起人名称
   */
  private String initiatorName;
  /**
   * 执行人
   */
  private String transactorId;
  /**
   * 执行人名称
   */
  private String transactorName;
  /**
   * 生产任务类别:1.加工2.配送3.库存盘点
   */
  private ProductionTypeEnum productionType;
  /**
   * 任务状态1.待处理2.执行中3.已完成4.未完成
   */
  private TaskStatusEnum taskStatus;
  /**
   * 汇总单编号
   */
  private String summaryOrderId;
  /**
   * 需求商品规格id
   */
  private String goodsModelId;
  /**
   * 商品名称
   */
  private String goodsName;
  /**
   * 商品规格名称
   */
  private String goodsModelName;
  /**
   * 需求货品编号
   */
  private String productSn;
  /**
   * 货品名称
   */
  private String productName;
  /**
   * 需求数量
   */
  private BigDecimal needNumber;
  /**
   * 货品单位
   */
  private String unit;
  /**
   * 任务内容说明
   */
  private String taskContent;
  /**
   * 任务执行备注
   */
  private String actionDesc;
  /**
   * 送货日期
   */
  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private LocalDate deliveryDate;
  /**
   * 送货批次:0第一批1第二批
   */
  private DeliverySeqEnum deliverySeq;
  /**
   * 任务截止时间
   */
  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private LocalDateTime deadlineTime;
  /**
   * 任务操作时间
   */
  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private LocalDateTime actionTime;
  /**
   * 创建者
   */
  @TableField(fill = FieldFill.INSERT)
  private String createBy;
  /**
   * 创建时间
   */
  @TableField(fill = FieldFill.INSERT)
  @ApiModelProperty(value = "创建时间", example = "2019-05-21 13:23:12")
  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private LocalDateTime createDate;
  /**
   * 更新者
   */
  @TableField(fill = FieldFill.UPDATE)
  private String updateBy;
  /**
   * 更新时间
   */
  @ApiModelProperty(value = "更新时间", example = "2019-05-21 13:23:12")
  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  @TableField(fill = FieldFill.UPDATE)
  private LocalDateTime updateDate;
  /**
   * 备注信息
   */
  private String remarks;
  /**
   * 删除标记
   */
  @TableLogic
  @TableField(fill = FieldFill.INSERT)
  private String delFlag;
  /**
   * 租户id
   */
  private Integer tenantId;

}
