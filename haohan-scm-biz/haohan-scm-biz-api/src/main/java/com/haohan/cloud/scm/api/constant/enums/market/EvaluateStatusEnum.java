package com.haohan.cloud.scm.api.constant.enums.market;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xwx
 * @date 2019/5/30
 */
@Getter
@AllArgsConstructor
public enum EvaluateStatusEnum {
    /**
     * 考核状态:1待考核2已考核3.考核完成4.未考核
     */
    stayExamine("1","待考核"),
    alreadyExamine("2","已考核"),
    accomplishExamine("3","考核完成"),
    notExamine("4","未考核");

    private static final Map<String, EvaluateStatusEnum> MAP = new HashMap<>(8);

    static {
        for (EvaluateStatusEnum e : EvaluateStatusEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static EvaluateStatusEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

    /**
     * 类型
     */
    @EnumValue
    @JsonValue
    private String type;
    /**
     * 描述
     */

    private String desc;
}
