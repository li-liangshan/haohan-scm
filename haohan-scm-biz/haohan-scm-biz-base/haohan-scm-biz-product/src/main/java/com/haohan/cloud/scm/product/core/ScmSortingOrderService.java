package com.haohan.cloud.scm.product.core;

import com.haohan.cloud.scm.api.product.req.SortingBatchReq;

/**
 * @author xwx
 * @date 2019/7/18
 */
public interface ScmSortingOrderService {

    /**
     * 分拣单操作 - 一键分拣
     * @param req
     * @return
     */
    Boolean sortingBatch(SortingBatchReq req);

}
