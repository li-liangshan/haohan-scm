/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.purchase.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.purchase.entity.DealRecord;
import com.haohan.cloud.scm.api.purchase.req.DealRecordReq;
import com.haohan.cloud.scm.purchase.service.DealRecordService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 账户扣减记录
 *
 * @author haohan
 * @date 2019-05-29 13:35:11
 */
@RestController
@AllArgsConstructor
@RequestMapping("/dealrecord" )
@Api(value = "dealrecord", tags = "dealrecord管理")
public class DealRecordController {

    private final DealRecordService dealRecordService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param dealRecord 账户扣减记录
     * @return
     */
    @GetMapping("/page")
    public R getDealRecordPage(Page page, DealRecord dealRecord) {
        return new R<>(dealRecordService.page(page, Wrappers.query(dealRecord)));
    }


    /**
     * 通过id查询账户扣减记录
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(dealRecordService.getById(id));
    }

    /**
     * 新增账户扣减记录
     * @param dealRecord 账户扣减记录
     * @return R
     */
    @SysLog("新增账户扣减记录" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('purchase_dealrecord_add')" )
    public R save(@RequestBody DealRecord dealRecord) {
        return new R<>(dealRecordService.save(dealRecord));
    }

    /**
     * 修改账户扣减记录
     * @param dealRecord 账户扣减记录
     * @return R
     */
    @SysLog("修改账户扣减记录" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('purchase_dealrecord_edit')" )
    public R updateById(@RequestBody DealRecord dealRecord) {
        return new R<>(dealRecordService.updateById(dealRecord));
    }

    /**
     * 通过id删除账户扣减记录
     * @param id id
     * @return R
     */
    @SysLog("删除账户扣减记录" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('purchase_dealrecord_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(dealRecordService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除账户扣减记录")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('purchase_dealrecord_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(dealRecordService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询账户扣减记录")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(dealRecordService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param dealRecordReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询账户扣减记录总记录}")
    @PostMapping("/countByDealRecordReq")
    public R countByDealRecordReq(@RequestBody DealRecordReq dealRecordReq) {

        return new R<>(dealRecordService.count(Wrappers.query(dealRecordReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param dealRecordReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据dealRecordReq查询一条货位信息表")
    @PostMapping("/getOneByDealRecordReq")
    public R getOneByDealRecordReq(@RequestBody DealRecordReq dealRecordReq) {

        return new R<>(dealRecordService.getOne(Wrappers.query(dealRecordReq), false));
    }


    /**
     * 批量修改OR插入
     * @param dealRecordList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('purchase_dealrecord_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<DealRecord> dealRecordList) {

        return new R<>(dealRecordService.saveOrUpdateBatch(dealRecordList));
    }


}
