package com.haohan.cloud.scm.api.constant.converter.bill;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.bill.OrderStatusEnum;

/**
 * @author dy
 * @date 2019/11/26
 */
public class OrderStatusEnumConverterUtil implements Converter<OrderStatusEnum> {
    @Override
    public OrderStatusEnum convert(Object o, OrderStatusEnum e) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return OrderStatusEnum.getByType(o.toString());
    }

}