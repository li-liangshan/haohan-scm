package com.haohan.cloud.scm.api.product.req;

import com.haohan.cloud.scm.api.product.entity.SortingOrderDetail;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @author dy
 * @date 2019/8/10
 */
@Data
public class SortingDetailConfirmReq {
    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "商家id", required = true)
    private String pmId;

    @NotBlank(message = "sortingDetailSn不能为空")
    @ApiModelProperty(value = "分拣明细编号", required = true)
    private String sortingDetailSn;

    @NotNull(message = "sortingNumber不能为空")
    @ApiModelProperty(value = "分拣数量", required = true)
    private BigDecimal sortingNumber;

    public SortingOrderDetail transTo() {
        SortingOrderDetail detail = new SortingOrderDetail();
        detail.setPmId(this.pmId);
        detail.setSortingDetailSn(this.sortingDetailSn);
        detail.setSortingNumber(this.sortingNumber);
        return detail;
    }
}
