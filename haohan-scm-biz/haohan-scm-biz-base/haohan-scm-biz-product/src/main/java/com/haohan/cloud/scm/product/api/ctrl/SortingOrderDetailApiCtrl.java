package com.haohan.cloud.scm.product.api.ctrl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.product.dto.SortingOrderDetailDTO;
import com.haohan.cloud.scm.api.product.entity.SortingOrderDetail;
import com.haohan.cloud.scm.api.product.req.QuerySortingOrderDetailReq;
import com.haohan.cloud.scm.api.product.req.SortingDetailConfirmReq;
import com.haohan.cloud.scm.product.core.IProductSortingOrderService;
import com.haohan.cloud.scm.product.service.SortingOrderDetailService;
import com.pig4cloud.pigx.common.core.util.R;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author cx
 * @date 2019/7/8
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/product/sortingOrderDetail")
@Api(value = "ApiSortingOrderDetail", tags = "SortingOrderDetail分拣明细")
public class SortingOrderDetailApiCtrl {
    private final IProductSortingOrderService iProductSortingOrderService;
    private final SortingOrderDetailService sortingOrderDetailService;

    /**
     * 查询分捡单明细列表
     * @param req
     * @return
     */
    @GetMapping("/queryList")
    public R<IPage<SortingOrderDetailDTO>> queryDetailList(Page page, @Validated QuerySortingOrderDetailReq req){
        SortingOrderDetailDTO detail = req.transToDTO();
        return R.ok(sortingOrderDetailService.queryDetailList(page, detail));
    }

    /**
     * 分拣单明细 开始分拣
     * @param req
     * @return
     */
    @PostMapping("/sortingDetailConfirm")
    public R<Boolean> sortingDetailConfirm(@Validated SortingDetailConfirmReq req){
        SortingOrderDetail detail = req.transTo();
        return new R<>(iProductSortingOrderService.sortingDetailConfirm(detail));
    }
}
