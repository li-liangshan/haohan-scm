package com.haohan.cloud.scm.api.salec.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author cx
 * @date 2019/6/21
 */
@Data
@ApiModel(description = "商品属性信息")
public class AttrDTO {

    @ApiModelProperty(value = "属性名")
    private String value;

    private String detailValue ="";

    private Boolean attrHidden = true;

    @ApiModelProperty(value = "此商品存在的属性值")
    private List<String> detail;
}
