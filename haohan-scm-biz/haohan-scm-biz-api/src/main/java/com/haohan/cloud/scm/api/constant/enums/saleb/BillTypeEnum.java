package com.haohan.cloud.scm.api.constant.enums.saleb;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;


/**
 * @author dy
 * @date 2019/6/15
 */
@Getter
@AllArgsConstructor
public enum BillTypeEnum implements IBaseEnum {

    /**
     * 账单类型:
     * 应收:11订单应收 12退货应收 13销售应收
     * 应付:21采购应付 22退货应付
     */
    order("11", "订单应收"),
    offerBack("12", "退货应收"),
    sales("13", "销售应收"),

    /**
     * 采购应付: 对应供应商订单
     */
    purchase("21", "采购应付"),
    orderBack("22", "退货应付");

    private static final Map<String, BillTypeEnum> MAP = new HashMap<>(8);

    static {
        for (BillTypeEnum e : BillTypeEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static BillTypeEnum getByType(String type) {
        return MAP.get(type);
    }

    @EnumValue
    @JsonValue
    private String type;
    private String desc;
}
