/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.message.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haohan.cloud.scm.api.message.entity.WechatMessageTemplate;

/**
 * 消息模板
 *
 * @author haohan
 * @date 2019-05-13 18:34:07
 */
public interface WechatMessageTemplateMapper extends BaseMapper<WechatMessageTemplate> {

}
