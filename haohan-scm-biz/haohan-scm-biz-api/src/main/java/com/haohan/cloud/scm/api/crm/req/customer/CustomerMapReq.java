package com.haohan.cloud.scm.api.crm.req.customer;

import com.haohan.cloud.scm.api.constant.enums.market.AreaTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.market.CustomerTypeEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

/**
 * @author dy
 * @date 2019/9/25
 */
@Data
public class CustomerMapReq {

    @ApiModelProperty(value = "地区名称", required = true)
    @NotBlank(message = "地区名称不能为空")
    private String areaName;

    @ApiModelProperty(value = "地区等级", required = true)
    @NotNull(message = "地区等级不能为空")
    private AreaTypeEnum level;

    @ApiModelProperty(value = "销售区域编号")
    @Length(min = 0, max = 32, message = "销售区域编号长度在0至32之间")
    private String areaSn;

    @ApiModelProperty(value = "客户负责人id")
    @Length(min = 0, max = 32, message = "客户经理id长度在0至32之间")
    private String directorId;

    @ApiModelProperty(value = "客户类型:1经销商2门店")
    private CustomerTypeEnum customerType;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "新增时间")
    private LocalDate queryDate;

    @ApiModelProperty(value = "是否需要定位点(经纬度)", notes = "默认不需要")
    private Boolean positionFlag = false;

}
