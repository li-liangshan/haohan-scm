package com.haohan.cloud.scm.api.constant.converter.crm;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.crm.MemberTypeEnum;

/**
 * @author dy
 * @date 2019/12/10
 */
public class MemberTypeEnumConverterUtil implements Converter<MemberTypeEnum> {
    @Override
    public MemberTypeEnum convert(Object o, MemberTypeEnum e) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return MemberTypeEnum.getByType(o.toString());
    }

}