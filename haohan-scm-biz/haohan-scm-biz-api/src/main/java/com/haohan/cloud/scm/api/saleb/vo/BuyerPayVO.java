package com.haohan.cloud.scm.api.saleb.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author dy
 * @date 2020/5/26
 */
@Data
@NoArgsConstructor
public class BuyerPayVO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("是否下单支付")
    private Boolean payFlag;

    @ApiModelProperty("预付金额")
    private BigDecimal amount;

    public BuyerPayVO(boolean payFlag, BigDecimal amount){
        this.payFlag = payFlag;
        this.amount = amount;
    }

}
