/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.wms.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.wms.entity.ExitWarehouseDetail;
import com.haohan.cloud.scm.api.wms.req.ExitWarehouseDetailReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 出库单明细内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "ExitWarehouseDetailFeignService", value = ScmServiceName.SCM_BIZ_WMS)
public interface ExitWarehouseDetailFeignService {


    /**
     * 通过id查询出库单明细
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/ExitWarehouseDetail/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 出库单明细 列表信息
     * @param exitWarehouseDetailReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/ExitWarehouseDetail/fetchExitWarehouseDetailPage")
    R getExitWarehouseDetailPage(@RequestBody ExitWarehouseDetailReq exitWarehouseDetailReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 出库单明细 列表信息
     * @param exitWarehouseDetailReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/ExitWarehouseDetail/fetchExitWarehouseDetailList")
    R getExitWarehouseDetailList(@RequestBody ExitWarehouseDetailReq exitWarehouseDetailReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增出库单明细
     * @param exitWarehouseDetail 出库单明细
     * @return R
     */
    @PostMapping("/api/feign/ExitWarehouseDetail/add")
    R save(@RequestBody ExitWarehouseDetail exitWarehouseDetail, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改出库单明细
     * @param exitWarehouseDetail 出库单明细
     * @return R
     */
    @PostMapping("/api/feign/ExitWarehouseDetail/update")
    R updateById(@RequestBody ExitWarehouseDetail exitWarehouseDetail, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除出库单明细
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/ExitWarehouseDetail/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/ExitWarehouseDetail/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/ExitWarehouseDetail/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param exitWarehouseDetailReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/ExitWarehouseDetail/countByExitWarehouseDetailReq")
    R countByExitWarehouseDetailReq(@RequestBody ExitWarehouseDetailReq exitWarehouseDetailReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param exitWarehouseDetailReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/ExitWarehouseDetail/getOneByExitWarehouseDetailReq")
    R getOneByExitWarehouseDetailReq(@RequestBody ExitWarehouseDetailReq exitWarehouseDetailReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param exitWarehouseDetailList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/ExitWarehouseDetail/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<ExitWarehouseDetail> exitWarehouseDetailList, @RequestHeader(SecurityConstants.FROM) String from);

     /**
     * 创建出库单明细(锁库存)
     *
     * @param detail 实体对象 需创建的出库单信息
     * @return R
     */
    @PostMapping("/api/feign/ExitWarehouseDetail/createExitWarehouseDetail")
    R<ExitWarehouseDetail> createExitDetailStock(@RequestBody ExitWarehouseDetail detail, @RequestHeader(SecurityConstants.FROM) String from);


}
