package com.haohan.cloud.scm.api.manage.req.merchant;

import com.haohan.cloud.scm.api.constant.enums.manage.MerchantPdsTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.manage.MerchantStatusEnum;
import com.haohan.cloud.scm.api.manage.entity.Merchant;
import com.haohan.cloud.scm.common.tools.constant.FirstGroup;
import com.haohan.cloud.scm.common.tools.constant.SecondGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author dy
 * @date 2020/3/25
 * 新增  FirstGroup
 * 修改 SecondGroup
 */
@Data
public class EditMerchantReq {

    @NotBlank(message = "商家id不能为空", groups = {SecondGroup.class})
    @Length(max = 32, message = "商家id最大长度32字符")
    @ApiModelProperty(value = "商家id")
    private String merchantId;

    @NotBlank(message = "商家名称不能为空", groups = {FirstGroup.class})
    @Length(max = 32, message = "商家名称最大长度32字符")
    @ApiModelProperty(value = "商家名称")
    private String merchantName;

    @NotBlank(message = "商家地址不能为空", groups = {FirstGroup.class})
    @Length(max = 64, message = "商家地址最大长度64字符")
    @ApiModelProperty(value = "商家地址")
    private String address;

    @NotBlank(message = "商家联系人名称不能为空", groups = {FirstGroup.class})
    @Length(max = 10, message = "商家联系人名称最大长度10字符")
    @ApiModelProperty(value = "商家联系人名称")
    private String contact;

    @NotBlank(message = "商家电话不能为空", groups = {FirstGroup.class})
    @Length(max = 20, message = "商家电话最大长度20字符")
    @ApiModelProperty(value = "商家电话")
    private String telephone;

    @Length(max = 32, message = "所属行业最大长度32字符")
    @ApiModelProperty(value = "所属行业")
    private String industry;

    @Length(max = 32, message = "规模最大长度32字符")
    @ApiModelProperty(value = "规模")
    private String scale;

    @Length(max = 64, message = "业务介绍最大长度64字符")
    @ApiModelProperty(value = "业务介绍")
    private String bizDesc;

    @Length(max = 250, message = "店铺备注最大长度250字符")
    @ApiModelProperty(value = "备注")
    private String remarks;

    @NotNull(message = "商家启用状态不能为空", groups = {FirstGroup.class})
    @ApiModelProperty(value = "商家启用状态", notes = "原系统使用字典 2启用  0 待审核 -1 停用")
    private MerchantStatusEnum status;

    @ApiModelProperty(value = "采购配送商家类型", notes = "商家的采购配送类型:0 普通商家  1 平台商家")
    private MerchantPdsTypeEnum pdsType;

    public Merchant transTo() {
        Merchant merchant = new Merchant();
        merchant.setId(this.merchantId);
        merchant.setMerchantName(this.merchantName);
        merchant.setAddress(this.address);
        merchant.setContact(this.contact);
        merchant.setTelephone(this.telephone);
        merchant.setIndustry(this.industry);
        merchant.setScale(this.scale);
        merchant.setBizDesc(this.bizDesc);
        merchant.setStatus(this.status);
        merchant.setPdsType(this.pdsType);
        merchant.setRemarks(this.remarks);
        return merchant;
    }
}
