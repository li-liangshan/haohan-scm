package com.haohan.cloud.scm.api.crm.trans;

import lombok.experimental.UtilityClass;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dy
 * @date 2019/10/23
 */
@UtilityClass
public class CrmImportHeader {

    public static final Map<String, String> EMPLOYEE;
    public static final Map<String, String> MARKET;
    public static final Map<String, String> CUSTOMER;
    public static final Map<String, String> CUSTOMER_LINKMAN;
    public static final Map<String, String> SALES_REPORT;
    public static final Map<String, String> SALES_ORDER;

    static {
        // 员工
        EMPLOYEE = new HashMap<>(12);
        EMPLOYEE.put("员工姓名", "name");
        EMPLOYEE.put("手机", "telephone");
        EMPLOYEE.put("员工类型", "employeeType");
        EMPLOYEE.put("性别", "sex");
        EMPLOYEE.put("部门", "department");
        EMPLOYEE.put("职位", "post");
        EMPLOYEE.put("备注", "remarks");

        // 市场
        MARKET = new HashMap<>(20);
        MARKET.put("市场名称", "marketName");
        MARKET.put("销售区域", "areaName");
        MARKET.put("市场类型", "marketType");
        MARKET.put("标签", "tags");
        MARKET.put("描述", "description");
        MARKET.put("电话", "phone");
        MARKET.put("省", "province");
        MARKET.put("市", "city");
        MARKET.put("区", "district");
        MARKET.put("街道/乡镇", "street");
        MARKET.put("详细地址", "address");
        MARKET.put("地址定位", "position");
        MARKET.put("备注", "remarks");

        // 客户
        CUSTOMER = new HashMap<>(44);
        CUSTOMER.put("客户编号", "customerSn");
        CUSTOMER.put("客户名称", "customerName");
        CUSTOMER.put("销售区域", "areaName");
        CUSTOMER.put("客户类型", "customerType");
        CUSTOMER.put("联系人姓名", "contact");
        CUSTOMER.put("联系人手机", "telephone");
        CUSTOMER.put("座机电话", "phoneNumber");
        CUSTOMER.put("客户经理", "directorName");
        CUSTOMER.put("客户标签", "tags");
        CUSTOMER.put("省", "province");
        CUSTOMER.put("市", "city");
        CUSTOMER.put("区", "district");
        CUSTOMER.put("街道/乡镇", "street");
        CUSTOMER.put("详细地址", "address");
        CUSTOMER.put("市场名称", "marketName");
        CUSTOMER.put("营业执照", "bizLicense");
        CUSTOMER.put("营业执照名称", "licenseName");
        CUSTOMER.put("工商注册号", "registrationNum");
        CUSTOMER.put("注册日期", "registrationDate");
        CUSTOMER.put("客户注册全称", "regName");
        CUSTOMER.put("经营法人名称", "legalName");
        CUSTOMER.put("经营地址", "regAddress");
        CUSTOMER.put("营业面积", "operateArea");
        CUSTOMER.put("年经营流水", "shopSale");
        CUSTOMER.put("业务介绍", "bizDesc");
        CUSTOMER.put("营业时间", "serviceTime");
        CUSTOMER.put("公司性质", "companyNature");
        CUSTOMER.put("客户级别", "customerLevel");
        CUSTOMER.put("邮编", "postcode");
        CUSTOMER.put("传真", "fax");
        CUSTOMER.put("网址", "website");
        CUSTOMER.put("备注", "remarks");

        // 客户联系人
        CUSTOMER_LINKMAN = new HashMap<>(16);
        CUSTOMER_LINKMAN.put("客户名称", "customerName");
        CUSTOMER_LINKMAN.put("主要联系人", "primaryFlag");
        CUSTOMER_LINKMAN.put("姓名", "name");
        CUSTOMER_LINKMAN.put("手机", "telephone");
        CUSTOMER_LINKMAN.put("性别", "sex");
        CUSTOMER_LINKMAN.put("部门", "department");
        CUSTOMER_LINKMAN.put("职位", "position");
        CUSTOMER_LINKMAN.put("邮箱", "email");
        CUSTOMER_LINKMAN.put("联系地址", "address");
        CUSTOMER_LINKMAN.put("描述", "linkmanDesc");
        CUSTOMER_LINKMAN.put("备注", "remarks");

        // 销量上报
        SALES_REPORT = new HashMap<>(20);
        SALES_REPORT.put("客户编码", "customerSn");
        SALES_REPORT.put("客户名称", "customerName");
        SALES_REPORT.put("销售日期", "reportDate");
        SALES_REPORT.put("上报人", "reportMan");
        SALES_REPORT.put("商品销售类型", "salesGoodsType");
        SALES_REPORT.put("商品规格编码", "modelSn");
        SALES_REPORT.put("商品名称", "goodsName");
        SALES_REPORT.put("商品规格名称", "modelName");
        SALES_REPORT.put("销售单位", "goodsUnit");
        SALES_REPORT.put("销售单价", "tradePrice");
        SALES_REPORT.put("销售数量", "goodsNum");
        SALES_REPORT.put("销售金额", "amount");
        SALES_REPORT.put("商品行备注", "remarks");

        // 销售订单
        SALES_ORDER = new HashMap<>(20);
        SALES_ORDER.put("外部订单号", "externalSn");
        SALES_ORDER.put("客户编码", "customerSn");
        SALES_ORDER.put("客户名称", "customerName");
        SALES_ORDER.put("供货商编码", "supplierSn");
        SALES_ORDER.put("供货商名称", "supplierName");
        SALES_ORDER.put("下单日期", "orderTime");
        SALES_ORDER.put("业务员", "employeeName");
        SALES_ORDER.put("交货日期", "deliveryDate");
        SALES_ORDER.put("收货人名称", "linkmanName");
        SALES_ORDER.put("收货人手机", "telephone");
        SALES_ORDER.put("收货地址", "address");
        SALES_ORDER.put("订单备注", "remarks");

        SALES_ORDER.put("商品规格编码", "goodsModelSn");
        SALES_ORDER.put("商品名称", "goodsName");
        SALES_ORDER.put("商品规格名称", "modelName");
        SALES_ORDER.put("销售单位", "unit");
        SALES_ORDER.put("销售单价", "dealPrice");
        SALES_ORDER.put("销售数量", "goodsNum");
        SALES_ORDER.put("金额", "amount");
        SALES_ORDER.put("商品行备注", "detailRemarks");

    }

}
