package com.haohan.cloud.scm.api.constant.converter.supply;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.supply.PdsOfferStatusEnum;

/**
 * @author xwx
 * @date 2019/6/3
 */
public class PdsOfferStatusEnumConverterUtil implements Converter<PdsOfferStatusEnum> {
    @Override
    public PdsOfferStatusEnum convert(Object o, PdsOfferStatusEnum pdsOfferStatusEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return PdsOfferStatusEnum.getByType(o.toString());
    }
}
