/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.message.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.message.entity.WechatUserMsgevent;
import com.haohan.cloud.scm.api.message.req.WechatUserMsgeventReq;
import com.haohan.cloud.scm.api.salec.resp.OrderMonitorResp;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 微信用户消息事件内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "WechatUserMsgeventFeignService", value = ScmServiceName.SCM_BIZ_MSG)
public interface WechatUserMsgeventFeignService {


    /**
     * 通过id查询微信用户消息事件
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/WechatUserMsgevent/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 微信用户消息事件 列表信息
     * @param wechatUserMsgeventReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/WechatUserMsgevent/fetchWechatUserMsgeventPage")
    R getWechatUserMsgeventPage(@RequestBody WechatUserMsgeventReq wechatUserMsgeventReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 微信用户消息事件 列表信息
     * @param wechatUserMsgeventReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/WechatUserMsgevent/fetchWechatUserMsgeventList")
    R getWechatUserMsgeventList(@RequestBody WechatUserMsgeventReq wechatUserMsgeventReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增微信用户消息事件
     * @param wechatUserMsgevent 微信用户消息事件
     * @return R
     */
    @PostMapping("/api/feign/WechatUserMsgevent/add")
    R save(@RequestBody WechatUserMsgevent wechatUserMsgevent, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改微信用户消息事件
     * @param wechatUserMsgevent 微信用户消息事件
     * @return R
     */
    @PostMapping("/api/feign/WechatUserMsgevent/update")
    R updateById(@RequestBody WechatUserMsgevent wechatUserMsgevent, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除微信用户消息事件
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/WechatUserMsgevent/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/WechatUserMsgevent/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/WechatUserMsgevent/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param wechatUserMsgeventReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/WechatUserMsgevent/countByWechatUserMsgeventReq")
    R countByWechatUserMsgeventReq(@RequestBody WechatUserMsgeventReq wechatUserMsgeventReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param wechatUserMsgeventReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/WechatUserMsgevent/getOneByWechatUserMsgeventReq")
    R getOneByWechatUserMsgeventReq(@RequestBody WechatUserMsgeventReq wechatUserMsgeventReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param wechatUserMsgeventList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/WechatUserMsgevent/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<WechatUserMsgevent> wechatUserMsgeventList, @RequestHeader(SecurityConstants.FROM) String from);


  /**
   * 根据对象条件，查询用户最后一条位置事件记录
   *
   * @param wechatUserMsgeventReq 实体对象,可以为空
   * @return R
   */
  @PostMapping("/api/feign/WechatUserMsgevent/getLastUserMsgevent")
  R<List<OrderMonitorResp>> getLastUserMsgevent(@RequestBody WechatUserMsgeventReq wechatUserMsgeventReq, @RequestHeader(SecurityConstants.FROM) String from);


}
