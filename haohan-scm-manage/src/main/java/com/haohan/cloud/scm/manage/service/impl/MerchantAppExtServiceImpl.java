/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.manage.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.manage.entity.MerchantAppExt;
import com.haohan.cloud.scm.manage.mapper.MerchantAppExtMapper;
import com.haohan.cloud.scm.manage.service.MerchantAppExtService;
import org.springframework.stereotype.Service;

/**
 * 商家应用扩展信息
 *
 * @author haohan
 * @date 2019-05-13 17:44:27
 */
@Service
public class MerchantAppExtServiceImpl extends ServiceImpl<MerchantAppExtMapper, MerchantAppExt> implements MerchantAppExtService {

}
