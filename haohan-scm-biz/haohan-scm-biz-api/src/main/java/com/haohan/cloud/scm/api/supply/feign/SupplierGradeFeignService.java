/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.supply.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.supply.entity.SupplierGrade;
import com.haohan.cloud.scm.api.supply.req.SupplierGradeReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 供应商评级记录内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "SupplierGradeFeignService", value = ScmServiceName.SCM_BIZ_SUPPLY)
public interface SupplierGradeFeignService {


    /**
     * 通过id查询供应商评级记录
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/SupplierGrade/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 供应商评级记录 列表信息
     * @param supplierGradeReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/SupplierGrade/fetchSupplierGradePage")
    R getSupplierGradePage(@RequestBody SupplierGradeReq supplierGradeReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 供应商评级记录 列表信息
     * @param supplierGradeReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/SupplierGrade/fetchSupplierGradeList")
    R getSupplierGradeList(@RequestBody SupplierGradeReq supplierGradeReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增供应商评级记录
     * @param supplierGrade 供应商评级记录
     * @return R
     */
    @PostMapping("/api/feign/SupplierGrade/add")
    R save(@RequestBody SupplierGrade supplierGrade, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改供应商评级记录
     * @param supplierGrade 供应商评级记录
     * @return R
     */
    @PostMapping("/api/feign/SupplierGrade/update")
    R updateById(@RequestBody SupplierGrade supplierGrade, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除供应商评级记录
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/SupplierGrade/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/SupplierGrade/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/SupplierGrade/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param supplierGradeReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/SupplierGrade/countBySupplierGradeReq")
    R countBySupplierGradeReq(@RequestBody SupplierGradeReq supplierGradeReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param supplierGradeReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/SupplierGrade/getOneBySupplierGradeReq")
    R getOneBySupplierGradeReq(@RequestBody SupplierGradeReq supplierGradeReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param supplierGradeList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/SupplierGrade/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<SupplierGrade> supplierGradeList, @RequestHeader(SecurityConstants.FROM) String from);


}
