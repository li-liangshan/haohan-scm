/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.manage.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.manage.entity.PhotoGallery;
import com.haohan.cloud.scm.api.manage.entity.PhotoManage;
import com.haohan.cloud.scm.manage.mapper.PhotoManageMapper;
import com.haohan.cloud.scm.manage.service.PhotoManageService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 图片管理
 *
 * @author haohan
 * @date 2019-05-13 17:32:37
 */
@Service
public class PhotoManageServiceImpl extends ServiceImpl<PhotoManageMapper, PhotoManage> implements PhotoManageService {

    @Override
    public List<PhotoManage> findListByNum(String groupNum) {
        QueryWrapper<PhotoManage> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(PhotoManage::getGroupNum, groupNum);
        return super.list(queryWrapper);
    }

    @Override
    public boolean addPhotoManage(String groupNum, PhotoGallery photoGallery) {
        PhotoManage photoManage = new PhotoManage();
        photoManage.setGroupNum(groupNum);
        photoManage.setPhotoGalleryId(photoGallery.getId());
        photoManage.setPicUrl(photoGallery.getPicUrl());
        photoManage.setPicName(photoGallery.getPicName());
        return super.save(photoManage);
    }
}
