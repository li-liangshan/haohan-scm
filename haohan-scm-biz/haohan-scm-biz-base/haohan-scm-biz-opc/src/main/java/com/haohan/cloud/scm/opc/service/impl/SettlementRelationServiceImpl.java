/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.opc.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.opc.entity.SettlementRelation;
import com.haohan.cloud.scm.opc.mapper.SettlementRelationMapper;
import com.haohan.cloud.scm.opc.service.SettlementRelationService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 结算单账单关系表
 *
 * @author haohan
 * @date 2019-09-18 17:36:02
 */
@Service
public class SettlementRelationServiceImpl extends ServiceImpl<SettlementRelationMapper, SettlementRelation> implements SettlementRelationService {

    /**
     * 根据 结算记录编号/账单编号 删除关联关系
     * @param settlementSn
     * @param paymentSn
     * @return
     */
    @Override
    public boolean delete(String settlementSn, String paymentSn) {
        boolean settlementFlag = StrUtil.isNotBlank(settlementSn);
        boolean paymentFlag = StrUtil.isNotBlank(paymentSn);
        if(!settlementFlag && !paymentFlag){
            return false;
        }
        QueryWrapper<SettlementRelation> query = new QueryWrapper<>();
        query.lambda()
                .eq(settlementFlag, SettlementRelation::getSettlementSn, settlementSn)
                .eq(paymentFlag, SettlementRelation::getPaymentSn, paymentSn);
        return super.remove(query);
    }

    /**
     * 查询 同一结算单的账单关系
     * @param settlementSn
     * @return
     */
    @Override
    public List<SettlementRelation> fetchListBySn(String settlementSn) {
        QueryWrapper<SettlementRelation> query = new QueryWrapper<>();
        query.lambda()
                .eq(SettlementRelation::getSettlementSn, settlementSn);
        List<SettlementRelation> list = super.list(query);
        return CollUtil.isEmpty(list) ? new ArrayList<>() : list;
    }
}
