package com.haohan.cloud.scm.goods.core;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.goods.dto.GoodsModelDTO;
import com.haohan.cloud.scm.api.goods.entity.GoodsCategory;
import com.haohan.cloud.scm.api.goods.entity.GoodsModel;
import com.haohan.cloud.scm.api.goods.req.OffGoodsByCateReq;

import java.util.List;

public interface IScmGoodsService {

//    /**
//     * 查询商品信息列表
//     * @return
//     */
//    List<GoodsInfoDTO> getGoodsInfoDTO();

//    /**
//     * 查询商品信息
//     * @param req
//     * @return
//     */
//    List<GoodsInfoDTO> queryGoodsInfoList(GoodsInfoReq req);
//
//    /**
//     * 创建goods信息表
//     * @param list
//     * @return
//     */
//    List<GoodsInfoDTO> createGoodsInfoDTO(List<Goods> list);

//    /**
//     * 查询商品轮播图
//     * @param goodsSn
//     * @return
//     */
//    List<String> getSliderImage(String goodsSn);
//
//    /**
//     * 上传轮播图
//     * @param req
//     * @return
//     */
//    Boolean uploadSliderImages(UploadGoodsImageReq req);

    /**
     * 查询库存预警
     * @param page
     * @param goodsModel
     * @return
     */
    IPage queryStorageWarn(Page page, GoodsModel goodsModel);

    /**
     * 查询商品规格信息列表
     * @param model
     * @return
     */
    List<GoodsModelDTO> queryGoodsModelDTOList(GoodsModel model);


    /**
     * 改变商品分类状态
     * @param cate
     * @return
     */
    Boolean changeCateStatus(GoodsCategory cate);

    /**
     * 按分类下架商品
     * @param req
     * @return
     */
    Boolean offGoodsByCate(OffGoodsByCateReq req);
}
